**第一次転勤対戦(R.W.I)**
====

**Overview**
====

*2018/6月に完成した作品です。*
*[日本ゲーム大賞 2018](http://awards.cesa.or.jp/)、[U-22 プログラミング・コンテスト2018](https://u22procon.com/)*
*<br>に向けて10secondsというチームで制作したゲームです。*
*  [Application Framework](Application%20Framework) - 第一次転勤対戦(R.W.I) を制作したフレームワークです。
*  [Execution Data](Execution%20Data) - 第一次転勤対戦(R.W.I) の実行データです。
*  [Develop](Develop) - 第一次転勤対戦(R.W.I) の開発データです。
*  [DirectX SDK Installer](DirectX%20SDK%20Installer) - DirectX SDK の インストールプログラムです。

**Demo**
====
*  [YouTube](https://youtu.be/7vR78iRANlQ)

**Requirement**
====

*  [IDE (Integrated Development Environment)](https://my.visualstudio.com/Downloads?q=visual%20studio%202015&pgroup=) - visual studio 2015
*  [End-User Runtime](https://www.microsoft.com/ja-jp/download/details.aspx?id=34429) - DirectX 9.0c End-User Runtime

##

![VisualStudio2015](Asset/VisualStudio2015.png)![c++](Asset/C__.png)![DirectX9SDK](Asset/DirectX9SDK.png)