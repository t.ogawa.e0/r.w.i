//==========================================================================
// CGameEnd[GameEnd.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "GameEnd.h"
#include "Screen.h"
#include "resource_link.hpp"

//==========================================================================
// 初期化
bool CGameEnd::Init(void)
{
    return false;
}

//==========================================================================
// 解放
void CGameEnd::Uninit(void)
{
}

//==========================================================================
// 更新
void CGameEnd::Update(void)
{
}

//==========================================================================
// 描画
void CGameEnd::Draw(void)
{
}

void CGameEnd::EndKey(EGameEndKey key)
{
    this->m_key = key;
    this->Save();
    CScreen::screenchange(CScreen::scenelist_t::Result);
}

void CGameEnd::Load(void)
{
    std::string strFilename = "";

    strFilename += resource_link::data::system_file;
    strFilename += "/";
    strFilename += resource_link::data::system_file_result;

    // フォルダ生成
    if (_mkdir(resource_link::data::system_file) == 0) {}

    FILE *pFile = fopen(strFilename.c_str(), "rb");

    if (pFile)
    {
        fread(&this->m_key, sizeof(this->m_key), 1, pFile);
        fclose(pFile);
    }
}

void CGameEnd::Save(void)
{
    std::string strFilename = "";

    strFilename += resource_link::data::system_file;
    strFilename += "/";
    strFilename += resource_link::data::system_file_result;

    // フォルダ生成
    if (_mkdir(resource_link::data::system_file) == 0) {}
    FILE *pFile = fopen(strFilename.c_str(), "wb");
    if (pFile)
    {
        fwrite(&this->m_key, sizeof(this->m_key), 1, pFile); // データ数の書き込み
        fclose(pFile);
    }
}
