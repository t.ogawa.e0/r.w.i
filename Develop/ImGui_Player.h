//==========================================================================
// プレイヤー[ImGui_Player.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _ImGui_Player_H_
#define _ImGui_Player_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "PlayerParam.h"
#include "Character.h"

//==========================================================================
//
// class  : CImGui_Player
// Content: ImGuiデバッグプレイヤー
//
//==========================================================================
class CImGui_Player : public CObject, public CCharacter
{
public:
    CImGui_Player()
	{
		this->m_regionkey = false;
		this->m_regionkey2 = false;
		this->boolreset();
        this->m_WorldTime = nullptr;
	}
	~CImGui_Player() {}
	// 初期化
	bool Init(void)override;

	// 解放
	void Uninit(void)override;

	// 更新
	void Update(void)override;

	// 描画
	void Draw(void)override;
private:
	// プレイヤーの行動
	void action(void);

	// 雇用
	void employment(void);

	// 解雇
	void fire(void);

	// 転勤
	void transferred(void);

	// 賃金
	void wage(void);

	// ただのbool初期化
	void boolreset(void);
private:
    bool m_menu[4] = { false }; // m_menu
    bool m_region[4] = { false }; // m_region
    bool m_region2[4] = { false }; // m_region
    bool m_regionkey = false; // 操作キー
    bool m_regionkey2 = false; // 操作キー
    void * m_WorldTime; // 時間へのアクセスルート
};

#endif // !_ImGui_Player_H_
