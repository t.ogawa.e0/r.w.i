//==========================================================================
// 社員生成システム[EmployeeSystem.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _EmployeeSystem_H_
#define _EmployeeSystem_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "SceneChange.h"

//==========================================================================
//
// class  : CEmployeeSystem
// Content: 社員生成システム
//
//==========================================================================
class CEmployeeSystem : public CBaseScene
{
public:
    CEmployeeSystem();
    ~CEmployeeSystem();
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
private:
};

#endif // !_EmployeeSystem_H_
