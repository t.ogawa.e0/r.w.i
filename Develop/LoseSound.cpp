#include "LoseSound.h"
#include "resource_list.h"


CLoseSound::CLoseSound()
{
    this->m_key = false;
}


CLoseSound::~CLoseSound()
{
}

bool CLoseSound::Init(void)
{
    this->XAudio()->Init(CXAudio2::SoundLabel(RESOURCE_GameCon_lose_wav, 0, 0.5f));
	return false;
}

void CLoseSound::Uninit(void)
{
}

void CLoseSound::Update(void)
{
    if (this->m_key == false)
    {
        this->XAudio()->Play(0);
        this->m_key = true;
    }
}

void CLoseSound::Draw(void)
{
}
