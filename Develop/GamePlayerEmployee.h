//==========================================================================
// プレイヤーの社員データ管理[GamePlayerEmployee.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "EmployeeManager.h"

//==========================================================================
//
// class  : CGamePlayerEmployee
// Content: プレイヤーの社員データ管理
//
//==========================================================================
class CGamePlayerEmployee : public CObject, public CEmployeeLink
{
public:
    CGamePlayerEmployee() :CObject(CObject::ID::Default) {
        this->m_camera = nullptr;
        this->m_EmployeeManager = nullptr;
        this->m_EmployeeParam = nullptr;
        this->m_WorldTime = nullptr;
        this->m_player = nullptr;
        this->m_field_pt = nullptr;
        this->m_LookEmployeeParam = nullptr;
        this->m_OperationExplanation = nullptr;
        this->m_Gacha_UI_Draw = nullptr;
        this->m_start_script = nullptr;
        this->m_TransferAnimation = nullptr;
        this->m_lookID = 0;
        this->m_id = 0;
        this->SetType(Type::Game_GamePlayerEmployee);
    }
    ~CGamePlayerEmployee() {
        this->m_task.clear();
        this->m_end_data_id.clear();
    }
    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;

    // 処理taskの取得
    const std::list<CCEmployeeTask> * GetTaskData(void) {
        return &this->m_task;
    }

    // 処理終了タスクのID取得
    // 取得した場合-1を入れてください
    std::list<int> & GetEndTaskID(void) {
        return this->m_end_data_id;
    }

    // 今見ているデータ
    EmployeeParam * lookData(void) {
        return this->m_LookEmployeeParam;
    }
private:
    // 雇用
    void Employment(void);

    // 解雇
    void Fire(void);

    // 転勤
    void Transferred(int _select);

    // 転勤予定
    void TransferredPlans(EmployeeParam * _this, int _select);

    // 操作2
    void Action(void);

    // 初期セットアップ
    void FastCreate(void);

    // ImGuiでのデバッグ
    void ImGuiDebug(void);

    // 最短経路探索
    CCEmployeeTask RootSearch(int _select);

    // ストライキ
    void Strike(void);

    // データの移動
    void Move(void);
private:
    void * m_camera;
    void * m_EmployeeManager;
    void * m_player; // プレイヤーへのアクセス
    void * m_field_pt; // フィールドへのアクセス
    void * m_WorldTime; // ワールドタイム
    void * m_OperationExplanation; // 操作制御
    void * m_Gacha_UI_Draw; // ガチャUI
    void * m_start_script; // スタート制御
    void * m_TransferAnimation;
    int m_lookID; // 見ているID
    int m_id; // 転勤元ID
    int m_dataIDManager; // データのID
    std::list<CCEmployeeTask>m_task; // 処理task
    std::list<int>m_end_data_id; // 処理終了_ID
    EmployeeParam * m_LookEmployeeParam; // 見ている社員データ
    EmployeeParam * m_EmployeeParam; // 社員データ
};

