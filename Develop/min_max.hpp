//==========================================================================
// min max[min_max.hpp]
// author: tatsuya ogawa
//==========================================================================
#ifndef __min_max_H_
#define __min_max_H_
//==========================================================================
//
// class  : C_min_max
// Content: m¦\
//
//==========================================================================
template <typename _data>
class C_min_max
{
public:
    C_min_max()
    {
        this->m_min = (float)0;
        this->m_max = (float)0;
    }
    C_min_max(const float min_, const float max_, const _data data_)
    {
        this->m_min = min_;
        this->m_max = max_;
        this->m_data = data_;
    }
    ~C_min_max() {}

    // ίθlΝΜmin
    float set(const float min_, const float Input, const _data data_)
    {
        this->m_min = min_;
        this->m_max = this->m_min + Input;
        this->m_data = data_;
        return this->m_max + (float)0.1f;
    }
    // »θ
    bool search(float Input)
    {
        if (this->m_min <= Input&&Input <= this->m_max)
        {
            return true;
        }
        return false;
    }
    void set_min(const float Input) { this->m_min = Input; }
    float get_min(void) { return this->m_min; }
    void set_max(const float Input) { this->m_max = Input; }
    float get_max(void) { return this->m_max; }
    void set_data(const _data & Input) { this->m_data = Input; }
    _data get_data(void) { return this->m_data; }
private:
    float m_min; // ΝΝΜζͺ
    float m_max; // ΝΝΜIνθ
    _data m_data; // f[^
};

#endif // !__min_max_H_
