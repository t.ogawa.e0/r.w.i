//==========================================================================
// スクリーン[Screen.cpp]
// author: tatuya ogawa
//==========================================================================
#include "Screen.h"

//==========================================================================
// 実体
//==========================================================================
bool CScreen::m_screenchange;
CScreen::scenelist_t CScreen::m_name; // name

//==========================================================================
// 初期化
bool CScreen::Init(HINSTANCE hInstance, HWND hWnd)
{
    srand((unsigned)time(nullptr));
    if (CDeviceManager::GetDXDevice()->CreateDevice()) { return true; }
    if (CDeviceManager::Init(CDeviceManager::DeviceInitList::keyboard, hInstance, hWnd)) { return true; }
    if (CDeviceManager::Init(CDeviceManager::DeviceInitList::Mouse, hInstance, hWnd)) { return true; }
    if (CDeviceManager::Init(CDeviceManager::DeviceInitList::XAudio2, hInstance, hWnd)) { return true; }

    this->m_change = false;
    this->m_count = 0;
    this->m_start = false;
    this->m_debugkey = false;
    this->m_name = scenelist_t::NOME;
    this->m_screenchange = false;

    this->m_temp.New_(this->m_fade);
    this->m_temp.New_(this->m_load);

    if (this->m_fade->Init()) { return true; }

    this->m_ImGui.Init(hWnd, CDeviceManager::GetDXDevice()->GetD3DDevice());

    return this->m_load->Init();
}

//==========================================================================
// 解放
void CScreen::Uninit(void)
{
    this->m_scene.Uninit();
    this->startdatarelease();
    this->m_ImGui.Uninit();
    this->m_temp.Delete_(this->m_fade);
    this->m_temp.Delete_(this->m_load);

    CDeviceManager::Release();
}

//==========================================================================
// 更新処理
bool CScreen::Update(void)
{
    this->m_ImGui.NewFrame();
    this->m_ImGui.Update();
    this->m_ImGui.NewWindow("デバッグウィンドウ", true);

    this->DebugSceneChange();

    if (this->startinit()) { return true; }

    if (this->m_bstartinitdata == false)
    {
        if (this->screeninit()) { return true; }

        CDeviceManager::Update();
        this->m_fade->Update();
        this->fade();
        this->SetFillmode();
        this->SetFittering();

        // ロードが終わっているとき
        if (this->m_change == true)
        {
            if (!this->Updatestop())
            {
                this->m_scene.Update();
            }
        }
        else
        {
            this->m_load->Update();
        }
    }

    this->m_ImGui.EndWindow();
    this->m_ImGui.EndFrame();

    return false;
}

//==========================================================================
// 描画処理
void CScreen::Draw(void)
{
    LPDIRECT3DDEVICE9 pDevice = CDeviceManager::GetDXDevice()->GetD3DDevice();	//デバイス渡し	
    D3DPRESENT_PARAMETERS pd3dpp = CDeviceManager::GetDXDevice()->Getd3dpp();

    //Direct3Dによる描画の開始
    if (CDeviceManager::GetDXDevice()->DrawBegin())
    {
        this->Fillmode(pDevice);
        this->Fittering(pDevice);

        // 画面切り替えが終わっているとき
        if (this->m_change == true)
        {
            this->m_scene.Draw();
            this->m_fade->Draw();
        }
        else
        {
            this->m_load->Draw();
            this->startdraw();
        }
        this->EndFillmode(pDevice);
        this->m_ImGui.Draw();
    }
    this->m_ImGui.DeviceReset(CDeviceManager::GetDXDevice()->DrawEnd(), pDevice, &pd3dpp);
}

//==========================================================================
// シーン変更キー
void CScreen::screenchange(scenelist_t Name)
{
    m_screenchange = true;
    m_name = Name;
}

//==========================================================================
// スクリーンの切り替え
void CScreen::change(scenelist_t Name)
{
    // すべての初期化
    this->m_scene.ChangeScene(Name);
}

//==========================================================================
// 最初の初期化
bool CScreen::startinit(void)
{
    // 最初に読み込むデータがある場合のみ有効
    if (this->m_bstartdraw)
    {
        // この間にセット

        // この間にセット
        this->m_bstartinitdata = false;
        this->m_bstartdraw = false;
    }

    return false;
}

//==========================================================================
// 最初の初期化時の描画
void CScreen::startdraw(void)
{
    // 最初に読み込むデータがある場合のみ有効
    if (this->m_bstartinitdata)
    {
        this->m_bstartdraw = true;
    }
}

//==========================================================================
// 最初の初期化データの破棄
void CScreen::startdatarelease(void)
{
}

//==========================================================================
// スクリーンの初期化
bool CScreen::screeninit(void)
{
    //==========================================================================
    // デバッグ用
#if defined(_DEBUG) || defined(DEBUG)
    if (this->m_count == 30 && this->m_change == false && this->m_start == true && this->m_debugkey == true)
    {
        this->change(scenelist_t::Default);
        this->m_debugkey = false;
        this->m_initializer = false;
    }
    //==========================================================================
    // 最初の画面
    if (this->m_count == 30 && this->m_change == false && this->m_start == false)
    {
        this->m_start = true;
        this->change(scenelist_t::Default);
        this->m_initializer = false;
    }
#else
    //==========================================================================
    // 最初の画面
    if (this->m_count == 30 && this->m_change == false && this->m_start == false)
    {
        this->m_start = true;
        this->change(scenelist_t::Home);
        this->m_initializer = false;
    }
#endif
    //==========================================================================
    // シーン変更
    if (this->m_count == 30 && this->m_change == false && this->m_name != scenelist_t::NOME)
    {
        this->m_start = true;
        this->change(this->m_name);
        this->m_name = scenelist_t::NOME;
        this->m_initializer = false;
    }

    //==========================================================================
    // 初期化中
    if (30 <= this->m_count && this->m_change == false && this->m_initializer != true)
    {
        // 全インスタンスの初期化終了判定
        this->m_initializer = this->m_scene.Init();
        // 全てのインスタンスの初期化が終了した際に、ロードを終えるための情報を与える
        if (this->m_initializer == true)
        {
            this->m_end_count = this->m_count;
            this->m_end_count += 30;
        }
    }

    //==========================================================================
    // フェード終了判定
    if (this->m_count == this->m_end_count && this->m_change == false && this->m_initializer == true)
    {
        this->m_fade->Out();
        this->m_change = true;
    }

    // ロードカウンタ
    if (this->m_change == false)
    {
        this->m_count++;
    }

    return false;
}

//==========================================================================
// フェード
void CScreen::fade(void)
{
    // フェードイン
    if (this->m_screenchange)
    {
        if (!this->m_fade->GetDraw())
        {
            this->m_fade->In();
        }
    }

    // フェードが終わりスクリーンがロード画面ではない場合
    if (this->m_fade->FeadInEnd() && this->m_change == true)
    {
        this->m_screenchange = false;
        this->m_change = false;
        this->m_count = 0;
    }
}

//==========================================================================
// ワイヤーフレームに切り替える
void CScreen::SetFillmode(void)
{
#if defined(_DEBUG) || defined(DEBUG)
    const char * pchar[] = { "false","true", };
    if (this->m_ImGui.MenuItem(this->m_ImGui.CreateText("ワイヤーフレームに切り替える : %s", pchar[this->m_bFillmode]).c_str()))
    {
        this->m_temp.Bool_(this->m_bFillmode);
    }
#endif
}

//==========================================================================
// 描画モード
void CScreen::Fillmode(LPDIRECT3DDEVICE9 pDevice)
{
#if defined(_DEBUG) || defined(DEBUG)
    if (this->m_bFillmode)
    {
        this->SetRenderWIREFRAME(pDevice);
    }
    else
    {
        this->SetRenderSOLID(pDevice);
    }
#else
    this->m_temp.Decoy_(pDevice);
#endif
}

//==========================================================================
// 描画モード
void CScreen::EndFillmode(LPDIRECT3DDEVICE9 pDevice)
{
#if defined(_DEBUG) || defined(DEBUG)
    if (this->m_bFillmode)
    {
        this->SetRenderSOLID(pDevice);
    }
#else
    this->m_temp.Decoy_(pDevice);
#endif
}

//==========================================================================
// フィルタリングのセット
void CScreen::SetFittering(void)
{
#if defined(_DEBUG) || defined(DEBUG)
    const char * pchar[] = { "false","true", };
    if (this->m_ImGui.MenuItem(this->m_ImGui.CreateText("フィルタリングの変更 : %s", pchar[this->m_bFittering]).c_str()))
    {
        this->m_temp.Bool_(this->m_bFittering);
    }
#endif
}

//==========================================================================
// フィルタリング
void CScreen::Fittering(LPDIRECT3DDEVICE9 pDevice)
{
#if defined(_DEBUG) || defined(DEBUG)
    if (this->m_bFittering)
    {
        this->SamplerFitteringGraphical(pDevice);
    }
    else
    {
        this->SamplerFitteringLINEAR(pDevice);
    }
#else
    this->m_temp.Decoy_(pDevice);
#endif
}

//==========================================================================
// 更新の停止
bool CScreen::Updatestop(void)
{
#if defined(_DEBUG) || defined(DEBUG)
    const char * pchar[] = { "false","true", };
    if (this->m_ImGui.MenuItem(this->m_ImGui.CreateText("更新の停止 : %s", pchar[this->m_bUpdatestop]).c_str()))
    {
        this->m_temp.Bool_(this->m_bUpdatestop);
    }
#endif
    return this->m_bUpdatestop;
}


void CScreen::DebugSceneChange(void)
{
    this->m_ImGui.NewWindow("シーン管理", true);
    if (this->m_ImGui.MenuItem("タイトル"))
    {
        this->screenchange(scenelist_t::Title);
    }
    if (this->m_ImGui.MenuItem("ホーム"))
    {
        this->screenchange(scenelist_t::Home);
    }
    if (this->m_ImGui.MenuItem("ゲーム"))
    {
        this->screenchange(scenelist_t::Game);
    }
    if (this->m_ImGui.MenuItem("リザルト"))
    {
        this->screenchange(scenelist_t::Result);
    }
    if (this->m_ImGui.MenuItem("マップエディタ"))
    {
        this->screenchange(scenelist_t::Editor);
    }
    if (this->m_ImGui.MenuItem("パラメータの動きの確認"))
    {
        this->screenchange(scenelist_t::DataCheck);
    }
    if (this->m_ImGui.MenuItem("社員ガチャ"))
    {
        this->screenchange(scenelist_t::EmployeeSystem);
    }
    if (this->m_ImGui.MenuItem("BGM/SEの確認"))
    {
        this->screenchange(scenelist_t::Music);
    }
    if (this->m_ImGui.MenuItem("終了"))
    {
        DestroyWindow(CDeviceManager::GetDXDevice()->GetHwnd());
    }
    this->m_ImGui.EndWindow();
}