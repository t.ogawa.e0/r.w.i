//==========================================================================
// ボード[Board.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Board.h"
#include "GameCamera.h"
#include "TitleData.h"
#include "LoadGameData.h"

CBoard::CBoard() : CObject(ID::Billboard)
{
    this->m_camera = nullptr;
    this->m_stage = StageData_ID::begin;
}


CBoard::~CBoard()
{
}

//==========================================================================
// 初期化
bool CBoard::Init(void)
{
    // カメラオブジェクト取得
    this->m_camera = this->GetObjects(ID::Camera, Type::Game_Camera);

    // ステージデータ読み取り
    this->m_stage = CTitleData::LoadStageID();

    // ステージテクスチャ読み込み
    switch (this->m_stage)
    {
    case CStage::EID::Wabisabi:
        this->Wabisabi();
        break;
    case CStage::EID::U_S_B:
        this->U_S_B();
        break;
    case CStage::EID::Waneha:
        this->Waneha();
        break;
    case CStage::EID::Kankari:
        this->Kankari();
        break;
    default:
        break;
    }

    // オブジェクトのインスタンス生成
    for (int i = 0; i < CLoadGameData::Size(); i++)
    {
        auto itr = this->m_data.find(CLoadGameData::GetData(i).m_ObjName);
        if (itr != this->m_data.end())
        {
            auto * p_obj = this->_3DObject()->Create();

            p_obj->Init(itr->second);
            p_obj->Scale(1);
            p_obj->SetMatInfoPos(*CLoadGameData::GetData(i).m_pos.GetMatInfoPos());

            if (CLoadGameData::GetData(i).m_head_office_player == true)
            {
                p_obj->MoveY(7);
            }
            else
            {
                p_obj->MoveY(3);
            }
        }
    }

    for (int i = 0; i < this->_3DObject()->Size(); i++)
    {
        auto * p_obj = this->_3DObject()->Get(i);
        this->_3DBillboard()->ObjectInput(p_obj);
    }

    return false;
}

//==========================================================================
// 解放
void CBoard::Uninit(void)
{
    this->m_data.clear();
}

//==========================================================================
// 更新
void CBoard::Update(void)
{
    D3DXMATRIX * p_vew = nullptr;

    // カメラオブジェクト再取得
    if (this->m_camera == nullptr)
    {
        this->m_camera = this->GetObjects(ID::Camera, Type::Game_Camera);
    }

    auto * p_camera = (CGameCamera*)this->m_camera;

    if (p_camera != nullptr)
    {
        p_vew = p_camera->GetMtxView();
    }

    this->_3DBillboard()->Update(p_vew);
}

//==========================================================================
// 描画
void CBoard::Draw(void)
{
    this->_3DBillboard()->Draw();
}

void CBoard::Wabisabi(void)
{
    const char * p_texlist[] = {
        RESOURCE_Board_Bon_DDS,
        RESOURCE_Board_Yoshino_DDS,
        RESOURCE_Board_Syouku_DDS,
        RESOURCE_Board_Shinano_DDS,
        RESOURCE_Board_Hakkai_DDS,
        RESOURCE_Board_Hikawa_DDS,
        RESOURCE_Board_Kokushi_DDS,
        RESOURCE_Board_NIto_DDS,
    };

    //==========================================================================
    // ここからシシャテボード表示判定用の設定
    //==========================================================================
    this->m_data["ポン"] = (int)JapanList::ポン;
    this->m_data["ヨシノ"] = (int)JapanList::ヨシノ;
    this->m_data["ショウク"] = (int)JapanList::ショウク;
    this->m_data["シナノ"] = (int)JapanList::シナノ;
    this->m_data["ハッカイ"] = (int)JapanList::ハッカイ;
    this->m_data["ヒカフ"] = (int)JapanList::ヒカフ;
    this->m_data["コクシ"] = (int)JapanList::コクシ;
    this->m_data["ニト"] = (int)JapanList::ニト;

    // ビルボード設置
    this->_3DBillboard()->Init(p_texlist, this->Helper()->Sizeof_(p_texlist), CBillboard::PlateList::Vertical);
}

void CBoard::U_S_B(void)
{
    const char * p_texlist[] = {
        RESOURCE_Board_Bimusu_DDS,
        RESOURCE_Board_Jakku_DDS,
        RESOURCE_Board_Tomateli_DDS,
        RESOURCE_Board_Toruma_DDS,
        RESOURCE_Board_Barenta_DDS,
        RESOURCE_Board_Wazesu_DDS,
        RESOURCE_Board_Woka_DDS,
        RESOURCE_Board_Hosu_DDS,
        RESOURCE_Board_Henesu_DDS,
        RESOURCE_Board_Hapa_DDS,
    };
    //==========================================================================
    // ここからシシャテボード表示判定用の設定
    //==========================================================================
    this->m_data["ビームス"] = (int)USAList::ビームス;
    this->m_data["ジャック"] = (int)USAList::ジャック;
    this->m_data["トマティ"] = (int)USAList::トマティ;
    this->m_data["トルマ"] = (int)USAList::トルマ;
    this->m_data["ベレンタイン"] = (int)USAList::ベレンタイン;
    this->m_data["ローゼス"] = (int)USAList::ローゼス;
    this->m_data["ウォーカー"] = (int)USAList::ウォーカー;
    this->m_data["ホース"] = (int)USAList::ホース;
    this->m_data["ヘネス"] = (int)USAList::ヘネス;
    this->m_data["ハーパー"] = (int)USAList::ハーパー;

    // ビルボード設置
    this->_3DBillboard()->Init(p_texlist, this->Helper()->Sizeof_(p_texlist), CBillboard::PlateList::Vertical);
}

void CBoard::Waneha(void)
{
    const char * p_texlist[] = {
        RESOURCE_Board_Soruteli_DDS,
        RESOURCE_Board_Jinikku_DDS,
        RESOURCE_Board_Myuru_DDS,
        RESOURCE_Board_Stein_DDS,
    };
    //==========================================================================
    // ここからシシャテボード表示判定用の設定
    //==========================================================================
    this->m_data["ソルティ"] = (int)HawaiiList::ソルティ;
    this->m_data["ジニック"] = (int)HawaiiList::ジニック;
    this->m_data["ミェール"] = (int)HawaiiList::ミェール;
    this->m_data["ヌテイン"] = (int)HawaiiList::ヌテイン;

    // ビルボード設置
    this->_3DBillboard()->Init(p_texlist, this->Helper()->Sizeof_(p_texlist), CBillboard::PlateList::Vertical);
}

void CBoard::Kankari(void)
{
    const char * p_texlist[] = {
        RESOURCE_Board_Tankare_DDS,
        RESOURCE_Board_Bonbei_DDS,
        RESOURCE_Board_Sirokko_DDS,
        RESOURCE_Board_Hiru_DDS,
        RESOURCE_Board_Godon_DDS,
        RESOURCE_Board_Girubi_DDS,
        RESOURCE_Board_fita_DDS,
    };
    //==========================================================================
    // ここからシシャテボード表示判定用の設定
    //==========================================================================
    this->m_data["タンカレー"] = (int)AustraliaList::タンカレー;
    this->m_data["ボンベイ"] = (int)AustraliaList::ボンベイ;
    this->m_data["シロッコ"] = (int)AustraliaList::シロッコ;
    this->m_data["ヒル"] = (int)AustraliaList::ヒル;
    this->m_data["ゴードン"] = (int)AustraliaList::ゴードン;
    this->m_data["ギルビー"] = (int)AustraliaList::ギルビー;
    this->m_data["フィーター"] = (int)AustraliaList::フィーター;

    // ビルボード設置
    this->_3DBillboard()->Init(p_texlist, this->Helper()->Sizeof_(p_texlist), CBillboard::PlateList::Vertical);
}
