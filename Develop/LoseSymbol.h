#ifndef _LoseSymbol_H_
#define _LoseSymbol_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CLoseSymbol
// Content: 敗北文字
//
//==========================================================================
class CLoseSymbol : public CObject
{
public:
	CLoseSymbol() :CObject(CObject::ID::Polygon2D) {
        this->m_reminnn = nullptr;
    }
	~CLoseSymbol() {}
	// 初期化
	bool Init(void)override;

	// 解放
	void Uninit(void)override;

	// 更新
	void Update(void)override;

	// 描画
	void Draw(void)override;
private:
	CColor<int>m_color; // 色
	CVector2<float>m_endpos; //終点
    void * m_reminnn;
};

#endif // !_WinSymbol_H_#pragma once
#pragma once
