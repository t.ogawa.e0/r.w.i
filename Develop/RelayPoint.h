//==========================================================================
// 中継地点[RelayPoint.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _RelayPoint_H_
#define _RelayPoint_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include <list>

//==========================================================================
//
// class  : CRelayPoint
// Content: 中継地点クラス
//
//==========================================================================
class CRelayPoint : public CObject
{
public:
    CRelayPoint() : CObject(CObject::ID::Xmodel) {
        this->SetType(CObject::Type::Game_RelayPoint);
    }
    ~CRelayPoint() {
        this->Uninit();
    }
    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;

    // セット
    // prev = 前 
    // next = 次
    void Set(const C3DObject& prev, const C3DObject& next);

    // リセット
    void Reset(void);
private:
    void SetPos(const C3DObject& prev, const C3DObject& next, float fDistance);
    // 解放
    void release(void) {
        this->Reset();
    }
private:
};

#endif // !_RelayPoint_H_
