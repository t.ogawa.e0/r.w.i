//==========================================================================
// ゲームイベント:バブル崩壊[BubbleCollapseEvent.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _BubbleCollapseEvent_H_
#define _BubbleCollapseEvent_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "GameEvent.h"

//==========================================================================
//
// class  : CBubbleCollapse
// Content: バブル崩壊
//
//==========================================================================
class CBubbleCollapse : public CEventManager
{
public:
    CBubbleCollapse()
    {
        this->m_Name = "バブル崩壊";
    }
    ~CBubbleCollapse()
    {
        this->m_target.clear();
        this->m_Name.clear();
    }

    // 初期化
    void Init(void);

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;

    // UI更新
    void UpdateUI(void)override;

    // テキスト
    void text(std::string strEventName)override;
private:

};


#endif // !_BubbleCollapseEvent_H_
