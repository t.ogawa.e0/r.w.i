//==========================================================================
// 警告[Warning.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Warning.h"
#include "resource_list.h"
#include "Screen.h"


CWarning::CWarning() : CObject(ID::Polygon2D)
{
    this->SetType(Type::Title_Warning);
    this->m_key = false;
}


CWarning::~CWarning()
{
}

//==========================================================================
// 初期化
bool CWarning::Init(void)
{
    auto * p_obj = this->_2DObject()->Create();

    this->_2DPolygon()->ObjectInput(p_obj);

    this->m_start_time.Init(1, 30);
    this->m_time.Init(1, 60);
    this->m_color = 255;
    this->m_color.a = 0;

    return this->_2DPolygon()->Init(RESOURCE_coution_DDS, true);
}

//==========================================================================
// 解放
void CWarning::Uninit(void)
{
}

//==========================================================================
// 更新
void CWarning::Update(void)
{
    if (this->m_key == true)
    {
        if (this->m_start_time.Countdown())
        {
            if (this->m_color.a != 255)
            {
                this->m_color.a += 5;
                if (255<this->m_color.a)
                {
                    this->m_color.a = 255;
                }
            }
            // 制御カウンタが終了している
            if (this->m_time.Countdown())
            {
                CScreen::screenchange(CScreen::scenelist_t::Title);
            }
        }
    }

    this->_2DObject()->Get(0)->SetColor(this->m_color);
    this->_2DPolygon()->Update();
}

//==========================================================================
// 描画
void CWarning::Draw(void)
{
    this->_2DPolygon()->Draw();
}

//==========================================================================
// 処理の開始
void CWarning::Start(void)
{
    this->m_key = true;
}
