//==========================================================================
// Game_Start[Game_Start.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Game_Start.h"
#include "TitleData.h"
#include "resource_list.h"

template<> struct enum_system::calculation<CGame_Start::E_Start_Count> : std::true_type {};
template<> struct enum_system::calculation<CGame_Start::E_Start_Talk> : std::true_type {};

//==========================================================================
// 初期化
bool CGame_Start::Init(void)
{
    //==========================================================================
    // カウンタオブジェクトの生成
    for (auto i = E_Start_Count::begin; i < E_Start_Count::end; ++i)
    {
        auto * p_obj = this->_2DObject()->Create();
        p_obj->Init(0, 1, 4, 4);
        p_obj->SetCentralCoordinatesMood(true);
        p_obj->SetPos({ this->GetWinSize().m_Width *2.0f ,this->GetWinSize().m_Height / 2.0f });
        this->_2DPolygon()->ObjectInput(p_obj);
        this->m_time.Create()->Init(1, 60);
    }
    this->m_TimeEnd.Init(this->m_time.Size() * this->m_time.Size(), 60);

    // テクスチャ読み込み 共通
    if (this->_2DPolygon()->Init(RESOURCE_Main_Screen_Count_Serihu_DDS, true))
    {
        return true;
    }
    
    // カウンタオブジェクトの生成
    //==========================================================================

    //==========================================================================
    // セリフオブジェクトの生成

    for (auto i = E_Start_Talk::begin; i < E_Start_Talk::end; ++i)
    {
        auto * p_obj = this->_2DObject()->Create();
        p_obj->Init(1, 1, 5, 1);
        p_obj->SetCentralCoordinatesMood(true);
        p_obj->SetPos({ this->GetWinSize().m_Width *2.0f ,this->GetWinSize().m_Height / 1.15f });
        this->_2DPolygon()->ObjectInput(p_obj);
    }

    switch (CTitleData::LoadEnemyLevel())
    {
    case CTitleData::Enemy_Level_ID::begin:
        break;
    case CTitleData::Enemy_Level_ID::TESHI:
        return this->_2DPolygon()->Init(RESOURCE_Serifu_Hoshi1_DDS, true);
        break;
    case CTitleData::Enemy_Level_ID::CUROUS:
        return this->_2DPolygon()->Init(RESOURCE_Serifu_Hoshi2_DDS, true);
        break;
    case CTitleData::Enemy_Level_ID::YOSHIDAYA:
        return this->_2DPolygon()->Init(RESOURCE_Serifu_Hoshi3_DDS, true);
        break;
    case CTitleData::Enemy_Level_ID::KENG:
        return this->_2DPolygon()->Init(RESOURCE_Serifu_Hoshi4_DDS, true);
        break;
    case CTitleData::Enemy_Level_ID::OZAKIGUMI:
        return this->_2DPolygon()->Init(RESOURCE_Serifu_Hoshi5_DDS, true);
        break;
    case CTitleData::Enemy_Level_ID::end:
        break;
    default:
        break;
    }


    return false;
}

//==========================================================================
// 解放
void CGame_Start::Uninit(void)
{
}

//==========================================================================
// 更新
void CGame_Start::Update(void)
{
    if (this->_2DObject()->Size() != 0 && this->_2DObject()->Size() != 0)
    {
        CVector4<float>v_pos = CVector4<float>((this->GetWinSize().m_Width / 2.0f), this->GetWinSize().m_Height / 2.0f);
        float f_speed = this->_Hit()->Distance(v_pos, *this->_2DObject()->Get(this->m_script)->GetPos())*0.1f;
        this->_2DObject()->Get(this->m_script)->SetXPlus(-f_speed);
        this->_2DObject()->Get(this->m_script + (int)E_Start_Talk::begin)->SetXPlus(-f_speed);

        // 現在処理中
        this->m_time.Get(this->m_script)->Countdown();
        if (this->m_script != (this->_2DObject()->Size() / 2) - 1)
        {
            if (this->m_time.Get(this->m_script)->GetComma() == 0 && this->m_time.Get(this->m_script)->GetTime() == 0)
            {
                // 処理対象の変更
                this->m_script++;
            }
        }

        for (int i = 0; i < this->m_time.Size(); i++)
        {
            auto * p_obj = this->_2DObject()->Get(i);
            p_obj->SetAnimationCount(i);

            auto * p_obj_2 = this->_2DObject()->Get(i + (int)E_Start_Talk::begin);
            p_obj_2->SetAnimationCount(i);

            // 処理終了オブジェクトの移動
            if (this->m_time.Get(i)->GetComma() == 0 && this->m_time.Get(i)->GetTime() == 0)
            {
                v_pos = CVector4<float>(0.0f - (this->GetWinSize().m_Width), this->GetWinSize().m_Height / 2.0f);
                f_speed = this->_Hit()->Distance(v_pos, *p_obj->GetPos())*0.05f;
                p_obj->SetXPlus(-f_speed);
                p_obj_2->SetXPlus(-f_speed);
                if (i == this->m_script)
                {
                    this->m_key = true;
                }
            }
        }
    }

    // カウンタ情報破棄
    if (this->m_TimeEnd.GetComma() == 0 && this->m_TimeEnd.GetTime() == 0 && this->_2DObject()->Size() != 0)
    {
        this->_2DPolygon()->ObjectRelease();
        this->_2DObject()->Release();
        this->m_time.Release();
        this->m_script = 0;
    }

    this->m_TimeEnd.Countdown();
    this->_2DPolygon()->Update();
}

//==========================================================================
// 描画
void CGame_Start::Draw(void)
{
    this->_2DPolygon()->Draw();
}
