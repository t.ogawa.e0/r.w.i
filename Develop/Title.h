//==========================================================================
// タイトル[Title.h]
// author: yoji watanabe
//==========================================================================
#ifndef _Title_H_
#define _Title_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "SceneChange.h"

//==========================================================================
//
// class  : CTitle
// Content: タイトル
//
//==========================================================================
class CTitle : public CBaseScene
{
public:
    CTitle();
    ~CTitle();
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;

private:
};

#endif // !_Title_H_
