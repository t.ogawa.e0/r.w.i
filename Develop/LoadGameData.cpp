//==========================================================================
// ゲームデータの読み込み[LoadGameData.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "LoadGameData.h"

//==========================================================================
// 実体
//==========================================================================
CObjectVectorManager<CFieldData> CLoadGameData::m_GameData; // データケース
StageData_ID CLoadGameData::m_SerectID; // 選択されたデータ

//==========================================================================
// 読み込み関数
void CLoadGameData::DataLoad(StageData_ID ID)
{
    CStageData game_data;
    FILE *pFile = fopen(game_data.Get_Pass_BIN(ID), "rb");

    m_SerectID = ID;

    if ((int)m_GameData.Size() != 0)
    {
        Release();
    }

    if (pFile)
    {
        // 終わりまで回す
        int nsize = 0; // サイズの取得
        fread(&nsize, sizeof(nsize), 1, pFile);

        // 取得したデータ数回す
        for (int i = 0; i < nsize; i++)
        {
            CFieldData data; // データ格納
            CFieldData *pdata = nullptr; // コピー先
            char cBuf[512] = { 0 }; // バフ

            // データの読み取り
            fread(&data.m_pos, sizeof(data.m_pos), 1, pFile);
            fread(&data.m_param, sizeof(data.m_param), 1, pFile);
            fread(cBuf, sizeof(cBuf), 1, pFile);
            fread(&data.m_head_office_enemy, sizeof(data.m_head_office_enemy), 1, pFile);
            fread(&data.m_head_office_player, sizeof(data.m_head_office_player), 1, pFile);

            // インスタンス生成
            pdata = m_GameData.Create();

            // データの移植
            pdata->m_pos.Init(data.m_pos.getindex());
            pdata->m_pos.SetLockAt(*data.m_pos.GetLockAt());
            pdata->m_pos.SetLockEye(*data.m_pos.GetLockEye());
            pdata->m_pos.SetLockUp(*data.m_pos.GetLockUp());
            pdata->m_pos.SetMatInfoPos(*data.m_pos.GetMatInfoPos());
            pdata->m_pos.SetMatInfoRot(*data.m_pos.GetMatInfoRot());
            pdata->m_pos.SetMatInfoSca(*data.m_pos.GetMatInfoSca());
            pdata->m_pos.SetVecFront(*data.m_pos.GetVecFront());
            pdata->m_pos.SetVecRight(*data.m_pos.GetVecRight());
            pdata->m_pos.SetVecUp(*data.m_pos.GetVecUp());
            pdata->m_pos.SetMatrixType(data.m_pos.GetMatrixType());
            pdata->m_param = data.m_param;
            pdata->m_ObjName = cBuf;
            pdata->m_head_office_enemy = data.m_head_office_enemy;
            pdata->m_head_office_player = data.m_head_office_player;
        }
        fclose(pFile);
    }
}

//==========================================================================
// 解放
void CLoadGameData::Release(void)
{
    m_GameData.Release();
}