//==========================================================================
// GameObject[GameObject.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _GameObject_H_
#define _GameObject_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "FieldEditor.h"

//==========================================================================
//
// class  : CGameObject
// Content: ゲームのオブジェクト
//
//==========================================================================
class CGameObject : public CObject
{
public:
    CGameObject() :CObject(CObject::ID::Xmodel) {}
    ~CGameObject() {
        this->m_CObjectData.Release();
        this->m_CObjectMapData.Release();
        this->m_RelayPoint = nullptr;
        this->SetType(Type::Game_Object);
    }
    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;

    // フィールドオブジェクトの取得
    CObjectVectorManager<C3DObject> * GetObjectPos(void) {
        return &this->m_CObjectMapData;
    }
private:
    CObjectVectorManager<CFieldData>m_CObjectData; // フィールド管理コンテナ
    CObjectVectorManager<C3DObject>m_CObjectMapData; // フィールド管理コンテナ
    void * m_RelayPoint; // 中継地点オブジェクトへのアクセスルート
};

#endif // !_GameObject_H_
