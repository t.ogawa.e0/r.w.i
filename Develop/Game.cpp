//==========================================================================
// ゲーム[Game.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Game.h"
#include "Stage.h"
#include "TitleData.h"

#include "GameCamera.h"
#include "Character.h"
#include "Worldtime.h"
#include "Player.h"
#include "Enemy.h"
#include "GameEvent.h"
#include "Subsidiary.h"
#include "LoadGameData.h"
#include "GameObject.h"
#include "GameUIScore.h"
#include "Game_Company_Date.h"
#include "RelayPoint.h"
#include "MovingPerson.h"
#include "Game_Syokuin.h"
#include "EmployeeManager.h"
#include "GamePlayerEmployee.h"
#include "GameEnemyEmployee.h"
#include "OperationExplanation.h"
#include "Gacha_UI_Draw.h"
#include "Game_Skydome.h"
#include "Game_Ocean.h"
#include "Game_Start.h"
#include "GameEnd.h"
#include "GameManager.h"
#include "Game_Strike.h"
#include "Game_Cursor.h"
#include "Money.h"
#include "Game_Sound.h"
#include "CharacterAnimation.h"
#include "Game_ICO.h"
#include "NamePlate.h"
#include "Board.h"
#include "Sinful_Image.h"

#include "resource_link.hpp"

CGame::CGame()
{
    this->m_stage = StageData_ID::begin;

    CGameCamera * camera;
    CWorldTime * worldtime;
    CPlayer * player;
    CEnemy * enemy;
    CGameEvent * pevent;
    CSubsidiary * sub;
    CGameObject * gameobj;
    CGameUIScore * ScoreUI;
    CGame_Company_Date * Game_Company_Date;
    CGame_Syokuin * Game_Syokuin;
    CRelayPoint * RelayPoint;
    CMovingPerson * MovingPerson;
    CEmployeeManager * EmployeeManager;
    CGamePlayerEmployee * GamePlayerEmployee;
    CGameEnemyEmployee * GameEnemyEmployee;
    COperationExplanation * OperationExplanation;
    CGacha_UI_Draw * Gacha_UI_Draw;
    CGame_Skydome * Game_Skydome;
    CGame_Ocean * Game_Ocean;
    CGame_Start * Game_Start;
    CGameEnd * GameEnd;
    CGame_Manager * Game_Manager;
    CGame_Cursor * Game_Cursor;
    CGame_Sound * Game_Sound;
    CGame_ICO * Game_ICO;
    CNamePlate * NamePlate;
    CSinful_Image * Sinful_Image;
    CCharacterAnimation * CharacterAnimation;
    CGame_Strike * Game_Strike;
    CBoard * Board;
    CMoney * Money; // 怪しい

    this->Load();

    //==========================================================================
    // データの読み込み
    CLoadGameData::DataLoad(this->m_stage);

    //==========================================================================
    // オブジェクト登録

    //==========================================================================
    // システム
    CObject::NewObject(worldtime);
    CObject::NewObject(camera);
    CObject::NewObject(sub);
    CObject::NewObject(player);
    CObject::NewObject(enemy);
    CObject::NewObject(EmployeeManager);
    CObject::NewObject(GamePlayerEmployee);
    CObject::NewObject(GameEnemyEmployee);
    CObject::NewObject(Game_Sound, this->m_stage);

    //==========================================================================
    // 3D
    CObject::NewObject(Game_Skydome);
    CObject::NewObject(Game_Ocean);
    CObject::NewObject(Game_Cursor);
    CObject::NewObject(RelayPoint);
    CObject::NewObject(MovingPerson);
    CObject::NewObject(gameobj);
    CObject::NewObject(Board);

    //==========================================================================
    // 2D
    CObject::NewObject(ScoreUI);
    CObject::NewObject(pevent);
    CObject::NewObject(Game_Company_Date);
    CObject::NewObject(Game_Syokuin);
    CObject::NewObject(OperationExplanation);
    CObject::NewObject(Game_ICO);
    CObject::NewObject(NamePlate);
    CObject::NewObject(Money);
    CObject::NewObject(CharacterAnimation);
    CObject::NewObject(Gacha_UI_Draw);
    CObject::NewObject(Game_Strike);
    CObject::NewObject(Sinful_Image);

    //==========================================================================
    // 特殊システム
    CObject::NewObject(Game_Start);
    CObject::NewObject(GameEnd);
    CObject::NewObject(Game_Manager);
}

CGame::~CGame()
{
}

//==========================================================================
// 初期化
bool CGame::Init(void)
{
    return CObject::InitAll();
}

//==========================================================================
// 解放
void CGame::Uninit(void)
{
    CObject::ReleaseAll();
    CCharacter::ReleaseAll();
    CLoadGameData::Release();
}

//==========================================================================
// 更新
void CGame::Update(void)
{
    CObject::UpdateAll();
    if (this->m_ImGui.NewTreeNode("次回のフィールド選択", false))
    {
        if (this->m_ImGui.Checkbox(this->m_stage_data.Get_Pass_BIN(StageData_ID::Wabisabi), &this->m_bool4.m_1))
        {
            this->m_stage = StageData_ID::Wabisabi;
            this->m_bool4 = CBool4_ss();
            this->m_bool4.m_1 = true;
            this->Save();
        }
        if (this->m_ImGui.Checkbox(this->m_stage_data.Get_Pass_BIN(StageData_ID::U_S_B), &this->m_bool4.m_2))
        {
            this->m_stage = StageData_ID::U_S_B;
            this->m_bool4 = CBool4_ss();
            this->m_bool4.m_2 = true;
            this->Save();
        }
        if (this->m_ImGui.Checkbox(this->m_stage_data.Get_Pass_BIN(StageData_ID::Waneha), &this->m_bool4.m_3))
        {
            this->m_stage = StageData_ID::Waneha;
            this->m_bool4 = CBool4_ss();
            this->m_bool4.m_3 = true;
            this->Save();
        }
        if (this->m_ImGui.Checkbox(this->m_stage_data.Get_Pass_BIN(StageData_ID::Kankari), &this->m_bool4.m_4))
        {
            this->m_stage = StageData_ID::Kankari;
            this->m_bool4 = CBool4_ss();
            this->m_bool4.m_4 = true;
            this->Save();
        }
        this->m_ImGui.EndTreeNode();
    }
}

//==========================================================================
// 描画
void CGame::Draw(void)
{
    CObject::DrawAll();
}

void CGame::Load(void)
{
    this->m_stage = CTitleData::LoadStageID();
    if (this->m_stage == Title_Stage_ID::begin)
    {
        this->m_stage = StageData_ID::Wabisabi;
        this->Save();

    }

    switch (this->m_stage)
    {
    case CStage::EID::Wabisabi:
        this->m_bool4.m_1 = true;
        break;
    case CStage::EID::U_S_B:
        this->m_bool4.m_2 = true;
        break;
    case CStage::EID::Waneha:
        this->m_bool4.m_3 = true;
        break;
    case CStage::EID::Kankari:
        this->m_bool4.m_4 = true;
        break;
    default:
        break;
    }
}

void CGame::Save(void)
{
    std::string strFilename = "";

    strFilename += resource_link::data::system_file;
    strFilename += "/";
    strFilename += resource_link::data::system_file_stage;

    // フォルダ生成
    if (_mkdir(resource_link::data::system_file) == 0) {}
    FILE *pFile = fopen(strFilename.c_str(), "wb");
    if (pFile)
    {
        fwrite(&this->m_stage, sizeof(this->m_stage), 1, pFile); // データ数の書き込み
        fclose(pFile);
    }
}
