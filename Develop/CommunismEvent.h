//==========================================================================
// ゲームイベント:共産主義[CommunismEvent.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _CommunismEvent_H_
#define _CommunismEvent_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "GameEvent.h"

//==========================================================================
//
// class  : CCommunism
// Content: 共産主義
//
//==========================================================================
class CCommunism : public CEventManager
{
public:
    CCommunism()
    {
        this->m_Name = "共産主義";
        this->m_Enemy_income = 0;
        this->m_Enemy_income_default = 0;
        this->m_Player_income = 0;
        this->m_Player_income_default = 0;
    }
    ~CCommunism()
    {
        this->m_target.clear();
        this->m_Name.clear();
    }

    // 初期化
    void Init(void);

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;

    // UI更新
    void UpdateUI(void)override;

    // テキスト
    void text(std::string strEventName)override;
private:
    int m_Player_income;
    int m_Player_income_default;
    int m_Enemy_income;
    int m_Enemy_income_default;
};

#endif // !_CommunismEvent_H_
