//==========================================================================
// プレイヤー[Player.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Player.h"
#include "Enemy.h"
#include "worldtime.h"
#include "Subsidiary.h"
#include "GameCamera.h"
#include "MovingPerson.h"
#include "GamePlayerEmployee.h"
#include "OperationExplanation.h"
#include "Game_Start.h"
#include "Game_Strike.h"
#include "resource_link.hpp"

//==========================================================================
// 初期化
bool CPlayer::Init(void)
{
    this->SetCharacterType(CharList::Player);
    this->m_psub = CSubsidiary::Get(CharList::Player);

    // mainデータ初期化
    this->m_param = CPlayerParam(20 * this->m_psub->GetMaxNemployee(), this->m_psub->GetNumSub(), 20);

    // 総従業員に加算
    this->m_param.m_total_employee = this->m_psub->GetMaxNemployee();

    this->XInput()->Init(1);

    // オブジェクトの先行呼び出し
    this->m_WorldTime = this->GetObjects(CObject::ID::Default, CObject::Type::Game_WorldTime);
    this->m_Camera = this->GetObjects(CObject::ID::Camera, CObject::Type::Game_Camera);
    this->m_GamePlayerEmployee = this->GetObjects(CObject::ID::Default, CObject::Type::Game_GamePlayerEmployee);
    this->m_OperationExplanation = this->GetObjects(CObject::ID::Polygon2D, CObject::Type::Game_OperationExplanation);
    this->m_start_script = this->GetObjects(ID::Polygon2D, Type::Game_Start_Object);
    this->m_strike__ = this->GetObjects(ID::Polygon2D, Type::Game_Strike);

    return false;
}

//==========================================================================
// 解放
void CPlayer::Uninit(void)
{
}

//==========================================================================
// 更新
void CPlayer::Update(void)
{
    if (this->m_WorldTime == nullptr)
    {
        this->m_WorldTime = this->GetObjects(CObject::ID::Default, CObject::Type::Game_WorldTime);
    }

    if (this->m_Camera == nullptr)
    {
        this->m_Camera = this->GetObjects(CObject::ID::Camera, CObject::Type::Game_Camera);
    }

    if (this->m_GamePlayerEmployee == nullptr)
    {
        this->m_GamePlayerEmployee = this->GetObjects(CObject::ID::Default, CObject::Type::Game_GamePlayerEmployee);
    }

    if (this->m_OperationExplanation == nullptr)
    {
        this->m_OperationExplanation = this->GetObjects(CObject::ID::Polygon2D, CObject::Type::Game_OperationExplanation);
    }

    if (this->m_start_script == nullptr)
    {
        this->m_start_script = this->GetObjects(ID::Polygon2D, Type::Game_Start_Object);
    }

    if (this->m_strike__ == nullptr)
    {
        this->m_strike__ = this->GetObjects(ID::Polygon2D, Type::Game_Strike);
    }

    auto * p_start = (CGame_Start*)this->m_start_script;

    // オブジェクトが存在するとき
    if (p_start != nullptr)
    {
        if (p_start->Start())
        {
            auto*pTime = (CWorldTime*)this->m_WorldTime;

            // オブジェクトが存在するとき
            if (pTime != nullptr)
            {
                // ワールド時間の更新が入ったときに処理が発生する。
                this->UpdateParam(pTime->getturns());

                // 不満度の処理
                this->Dissatisfied(pTime->getturns2()->GetTime());
            }

            // プレイヤーの行動
            this->action();

            // 社員一揆の処理
            if (this->Strike())
            {
                auto * p_strike = (CGame_Strike*)this->m_strike__;

                // オブジェクトが存在するとき
                if (p_strike != nullptr)
                {
                    p_strike->Create();
                }
            }

            // テキストセット
            this->Text("プレイヤーの情報");

            // データの補正
            this->Correction();

            this->XInput()->Update();
        }
    }
}

//==========================================================================
// 描画
void CPlayer::Draw(void)
{
}

//==========================================================================
// プレイヤーの行動
void CPlayer::action(void)
{
    this->employment();
    this->fire();
    this->transferred();
    this->wage();
}

//==========================================================================
// 雇用
void CPlayer::employment(void)
{
    auto * pcam = (CGameCamera*)this->m_Camera;
    auto * p_oe_key = (COperationExplanation*)this->m_OperationExplanation;

    // オブジェクトが存在するとき
    if (pcam != nullptr&&p_oe_key != nullptr)
    {
        // ここで採用人数を加算しています。
        if (this->XInput()->Trigger(CXInput::EButton::Y, 0) && p_oe_key->Key_Syokuin() == true)
        {
            this->m_param.m_asset -= resource_link::game_::n_cost;
            this->Employment(this->m_psub->GetSub(pcam->GetSerectID()), 1);
        }
    }
}

//==========================================================================
// 解雇
void CPlayer::fire(void)
{
    auto * pcam = (CGameCamera*)this->m_Camera;
    auto * p_oe_key = (COperationExplanation*)this->m_OperationExplanation;

    // オブジェクトが存在するとき
    if (pcam != nullptr&&p_oe_key != nullptr)
    {
        // 人数分削る
        if (this->XInput()->Trigger(CXInput::EButton::A, 0) && p_oe_key->Key_Syokuin() == true)
        {
            if (this->m_psub->GetSub(pcam->GetSerectID())->m_employee_sub != 0)
            {
                this->Fire(this->m_psub->GetSub(pcam->GetSerectID()), 1);
            }
        }
    }
}

//==========================================================================
// 転勤
void CPlayer::transferred(void)
{
    auto * pcam = (CGameCamera*)this->m_Camera;
    auto * p_oe_key = (COperationExplanation*)this->m_OperationExplanation;

    // オブジェクトが存在するとき
    if (pcam != nullptr&&p_oe_key != nullptr)
    {
        // 転勤地決定
        if (this->XInput()->Trigger(CXInput::EButton::X, 0))
        {
            if (this->m_old_serect_id == -1)
            {
                this->m_old_serect_id = pcam->GetSerectID();
            }
            if (this->m_old_serect_id != pcam->GetSerectID())
            {
                if (this->m_psub->GetSub(this->m_old_serect_id)->m_employee_sub != 0 && p_oe_key->Key_Transfer() == true)
                {
                    this->Transferred(this->m_psub->GetSub(this->m_old_serect_id), this->m_psub->GetSub(pcam->GetSerectID()), this->m_psub, 1);
                    this->m_old_serect_id = -1;
                    return;
                }
            }
        }
    }
}

//==========================================================================
// 賃金
void CPlayer::wage(void)
{
    auto * p_oe_key = (COperationExplanation*)this->m_OperationExplanation;

    // オブジェクトが存在するとき
    if (p_oe_key != nullptr)
    {
        if (this->XInput()->Trigger(CXInput::EButton::DPAD_UP, 0) && p_oe_key->Key_Default_Company())
        {
            this->m_param.m_wage_sub++;
        }
        if (this->XInput()->Trigger(CXInput::EButton::DPAD_DOWN, 0) && p_oe_key->Key_Default_Company())
        {
            if (this->m_param.m_wage_sub != 20)
            {
                this->m_param.m_wage_sub--;
            }
        }
    }
}

//==========================================================================
// データの補正
void CPlayer::Correction(void)
{
    auto*pTime = (CWorldTime*)this->m_WorldTime;
    auto*p_GamePlayerEmployee = (CGamePlayerEmployee*)this->m_GamePlayerEmployee;

    // オブジェクトが存在するとき
    if (pTime != nullptr&&p_GamePlayerEmployee != nullptr)
    {
        // データの補正
        if ((pTime->getturns().m_current != pTime->getturns().m_old))
        {
            int nbuf = 0; // 合計の取得

                          // プレイヤーの社員取得
            auto * p_emp = p_GamePlayerEmployee->GetEmployeeData();

            // 子会社へのアクセス
            for (int i = 0; i < this->m_psub->GetNumSub(); i++)
            {
                auto * p_sub_data = this->m_psub->GetSub(i);

                // idと一致するデータの検出
                auto itr = p_emp->find(p_sub_data->m_id);

                // 社員人数の補正
                if (itr != p_emp->end())
                {
                    p_sub_data->m_employee_sub = (int)itr->second.size();
                    p_sub_data->m_employee = (int)itr->second.size();
                    nbuf += (int)itr->second.size();
                }
            }

            // 総社員数の補正
            this->m_param.m_total_employee = nbuf + p_GamePlayerEmployee->GetTaskData()->size();
        }
    }
}
