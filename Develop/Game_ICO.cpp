//==========================================================================
// Game_ICO[Game_ICO.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Game_ICO.h"
#include "Player.h"
#include "Enemy.h"
#include "resource_list.h"
#include "TitleData.h"

CGame_ICO::CGame_ICO() : CObject(ID::Polygon2D)
{
    this->m_player = nullptr;
    this->m_enemy = nullptr;
}


CGame_ICO::~CGame_ICO()
{
}

//==========================================================================
// 初期化
bool CGame_ICO::Init(void)
{
	C2DObject * pPos = nullptr;
	CVector4<float> vpos;

    //==========================================================================
    // アイコン
	const char * pICO_UI[] = {
		RESOURCE_ICO_Hoshi1_DDS,
		RESOURCE_ICO_Hoshi2_DDS,
		RESOURCE_ICO_Hoshi3_DDS,
		RESOURCE_ICO_Hoshi4_DDS,
		RESOURCE_ICO_Hoshi5_DDS,
		RESOURCE_ICO_Player_DDS,
	};

	//プレイヤー
	this->_2DPolygon()->Init(pICO_UI[5], true);
	pPos = this->_2DObject()->Create();
	this->_2DPolygon()->ObjectInput(pPos);
	this->_2DPolygon()->SetTexScale(0, 1.45f);
	this->_2DObject()->Get((int)ICO_UI_List::ICO_Player)->Init((int)ICO_UI_List::ICO_Player, 1, 3, 3);
	this->_2DObject()->Get((int)ICO_UI_List::ICO_Player)->SetAnimationCount(0);
	vpos.x = (float)this->_2DPolygon()->GetTexSize(0)->w * 0.68f;
	vpos.y = (float)this->_2DPolygon()->GetTexSize(0)->h * 0.55f;
	this->_2DObject()->Get((int)ICO_UI_List::ICO_Player)->SetCentralCoordinatesMood(true);
	this->_2DObject()->Get((int)ICO_UI_List::ICO_Player)->SetPos(vpos);

	//エネミー
	this->_2DPolygon()->Init(pICO_UI[0 + (int)CTitleData::LoadEnemyLevel()], true);
	pPos = this->_2DObject()->Create();
	this->_2DPolygon()->ObjectInput(pPos);
	this->_2DPolygon()->SetTexScale(1, 1.45f);
	this->_2DObject()->Get((int)ICO_UI_List::ICO_Enemy)->Init((int)ICO_UI_List::ICO_Enemy, 1, 3, 3);
	this->_2DObject()->Get((int)ICO_UI_List::ICO_Enemy)->SetAnimationCount(0);
	vpos.x = (float)this->_2DPolygon()->GetTexSize(1)->w * 2.71f;
	vpos.y = (float)this->_2DPolygon()->GetTexSize(1)->h * 0.55f;
	this->_2DObject()->Get((int)ICO_UI_List::ICO_Enemy)->SetCentralCoordinatesMood(true);
	this->_2DObject()->Get((int)ICO_UI_List::ICO_Enemy)->SetPos(vpos);

    this->m_player = this->GetObjects(ID::Default, Type::Game_Player);
    this->m_enemy = this->GetObjects(ID::Default, Type::Game_Enemy);
    return false;
}

//==========================================================================
// 解放
void CGame_ICO::Uninit(void)
{
}

//==========================================================================
// 更新
void CGame_ICO::Update(void)
{
    if (this->m_player == nullptr)
    {
        this->m_player = this->GetObjects(CObject::ID::Default, CObject::Type::Game_Player);
    }
    if (this->m_enemy == nullptr)
    {
        this->m_enemy = this->GetObjects(CObject::ID::Default, CObject::Type::Game_Enemy);
    }

    if (this->m_player != nullptr&&this->m_enemy != nullptr)
    {
        this->AssetComparison();
    }

    this->_2DPolygon()->Update();
}

//==========================================================================
// 描画
void CGame_ICO::Draw(void)
{
    this->_2DPolygon()->Draw();
}

//==========================================================================
// 資産の比較
void CGame_ICO::AssetComparison(void)
{
    auto*p_player = (CPlayer*)this->m_player;
    auto*p_enemy = (CEnemy*)this->m_enemy;

	if (p_player->GetAsset() > p_enemy->GetAsset())
	{
		this->_2DObject()->Get((int)ICO_UI_List::ICO_Player)->SetAnimationCount(1);
		this->_2DObject()->Get((int)ICO_UI_List::ICO_Enemy)->SetAnimationCount(2);
	}
	else if (p_player->GetAsset() < p_enemy->GetAsset())
	{
		this->_2DObject()->Get((int)ICO_UI_List::ICO_Player)->SetAnimationCount(2);
		this->_2DObject()->Get((int)ICO_UI_List::ICO_Enemy)->SetAnimationCount(1);
	}
	else
	{
		this->_2DObject()->Get((int)ICO_UI_List::ICO_Player)->SetAnimationCount(0);
		this->_2DObject()->Get((int)ICO_UI_List::ICO_Enemy)->SetAnimationCount(0);
	}
}
