//==========================================================================
// Enemy[Enemy.h]
// author: yoji watanabe
//==========================================================================
#ifndef _Enemy_H_
#define _Enemy_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "PlayerParam.h"
#include "Character.h"
#include "TitleData.h"

//==========================================================================
//
// class  : Enemy
// Content: エネミー
//
//==========================================================================
class CEnemy : public CObject, public CCharacter
{
private:
    // 各種操作
    enum class actionID
    {
        NONE = -1,//何もしない
        EMPLOYMENT = 0,
        FIRE,
        TRANSFERRED,
        WAGE,
    };
public:
    CEnemy() :CObject(CObject::ID::Default) {
        this->m_WorldTime = nullptr;
        this->m_GameEnemyEmployee = nullptr;
        this->m_start_script = nullptr;
        this->m_targetNum = 0;
        this->m_sourceTargetNum = 0;
        this->m_numemployment = 0;
        this->SetType(Type::Game_Enemy);
    }
    ~CEnemy() {}
    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;

    int GetAsset(void) {
        return this->m_param.m_asset;
    }
private:
    void action(const CGameManager::TURNS_t & InputTime);

    //行動の決定
    void selectTarget(void);

    // データの補正
    void Correction(void);
private:
    int m_NumSub;//支社数
    int m_headNum;//本社ID
    void * m_GameEnemyEmployee; // エネミーの社員へのアクセス権
    void * m_WorldTime; // 時間へのアクセスルート
    void * m_start_script; // スタート制御

                           //	AI用変数
    CTitleData::Enemy_Level_ID m_enemyLevel; //エネミーレベル
    const int m_maxWage = 25;//最大賃金
    const int m_minWage = 20;//最小賃金
    int m_Term;//任期
    actionID m_actionID[2];	//操作種別(雇用して移動する場合だけタスク処理なので一応配列で確保
    int m_targetNum;		//操作ターゲットナンバー
    int m_sourceTargetNum;	//移動元ターゲットナンバー
    int m_numemployment;	//操作人数
    int m_numEmployee[20];	//各行動が即終了したものとして社員配置数を管理する
    int m_turn; // 行動までのターン

};

#endif // !_Enemy_H_
