//==========================================================================
// 社員描画[Employee.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _GameEmployee_H_
#define _GameEmployee_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "EmployeeParamSystem.h"

//==========================================================================
//
// class  : CEmployeeGatya
// Content: 社員描画
//
//==========================================================================
class CGameEmployee : public CObject, private CEmployeeParamSystem
{
private:
    enum class TexList {
        MainScreen_Syokuin_Rirekisyo, // シャインデータ表示用背景ID
        MainScreen_Syokuin_Date_tab, // タブID
        MainScreen_Syokuni_Name, // 名前テクスチャID
        MainScreen_syoumeisyashin, // アイコンID
        MainScreen_Syokuin_Rank_skill, // skillレアリティID
        MainScreen_Syokuin_Rank_mental, // mentalレアリティID
        MainScreen_Syokuin_Rank_cost, // costレアリティID
        MainScreen_Syokuin_Tokuchou, // 特徴ID
        MainScreen_Syokuin_Type, // タイプID
        MainScreen_Syokuin_Type_Partner_true, // パートナーがいない
        MainScreen_Syokuin_Type_Partner_false, // パートナーがいる
    };
public:
    CGameEmployee() : CObject(CObject::ID::Polygon2D) {
        this->m_system_bool = false;
        this->m_EmergenceKey = false;
        this->m_partner_drawkey = false;
    }
    ~CGameEmployee() {
        this->Uninit();
    }
    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;
    // 表示open
    void Game_Syokuin_Data_Open(void) {
        this->m_EmergenceKey = true;
    }

    // 表示clause
    void Game_Syokuin_Data_Clause(void) {
        this->m_EmergenceKey = false;
    }

    const CVector4<float> & GetPosMaster(void) {
        return this->m_master_pos;
    }
private:
    // パラメーターのセット
    void SetParameter(void);

    // デバッグウィンドウ
    void DebugWindow(void);

    // 出現/終了
    // 戻り値 移動が完全に終了した際にtrue
    bool Emergence(void);

    // UIの移動
    bool UIMove(float fLimit, float fSpeed);

    //==========================================================================
    // 設計用仮システム
    //==========================================================================
    bool system_(void) {
        if (this->ImGui()->Checkbox("社員表示枠生成モード", &this->m_system_bool)) {
            this->create();
        }
        return this->m_system_bool;
    }
    bool system__(void) {
        return this->m_system_bool;
    }
    void create(void);
    void system_draw(void);
    //==========================================================================
    // 設計用仮システム
    //==========================================================================
private:
    //==========================================================================
    // 設計用仮システム
    //==========================================================================
    CObjectVectorManager<CVector4<float>>m_MasterPos; // 親座標
    bool m_EmergenceKey; // 出現操作キー
    bool m_partner_drawkey; // パートナー判定強制
    //==========================================================================
    // 設計用仮システム
    //==========================================================================

    CObjectVectorManager<CParam> m_param; // パラメータ生成
    bool m_system_bool;
    CVector4<float>m_master_pos;
};
#endif // !_GameEmployee_H_
