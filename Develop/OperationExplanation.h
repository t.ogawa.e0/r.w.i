//==========================================================================
// 操作説明/制御[OperationExplanation.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : COperationExplanation
// Content: 操作説明/制御
//
//==========================================================================
class COperationExplanation : public CObject
{
private:
    // 操作ID
    enum class E_Key
    {
        Company, // シシャ
        Employee, // シャイン
        Transfer, // 転勤
    };
public:
    COperationExplanation() :CObject(ID::Polygon2D) {
        this->SetType(Type::Game_OperationExplanation);
        this->m_control_tab_key = E_Key::Company;
        this->m_control_menu_key = E_Key::Company;
        this->m_control_syokuin = nullptr;
        this->m_control_company = nullptr;
        this->m_start_script = nullptr;
        this->m_Game_Sound = nullptr;
        this->m_camera = nullptr;
        this->m_lock = false;
        this->m_old_serect_id = -1;
    }
    ~COperationExplanation() {}

    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;

    // デフォルト/シシャキー
    bool Key_Default_Company(void) {
        if (this->m_control_menu_key == E_Key::Company) {
            return true;
        }
        return false;
    }
    // シャイン操作キー
    bool Key_Syokuin(void) {
        if (this->m_control_menu_key == E_Key::Employee) {
            return true;
        }
        return false;
    }
    // 転勤キー
    bool Key_Transfer(void) {
        if (this->m_control_menu_key == E_Key::Transfer) {
            return true;
        }
        return false;
    }
private:
    E_Key m_control_tab_key; // タブコントロール
    E_Key m_control_menu_key; // タブコントロール
    void * m_control_syokuin; // シャインデータ
    void * m_control_company; // シシャデータ
    void * m_PlayerEmployee;
    void * m_start_script; // スタート制御
    void * m_Game_Sound;// soundへのアクセス権
    void * m_camera; // カメラへのアクセスルート
    int m_old_serect_id; // 選んでいたID
    bool m_lock;
};
