#pragma once
#include "dxlib.h"

class CWinSound : public CObject
{
public:
	CWinSound();
	~CWinSound();
	// 初期化
	bool Init(void)override;

	// 解放
	void Uninit(void)override;

	// 更新
	void Update(void)override;

	// 描画
	void Draw(void)override;
private:
    bool m_key;
};

