#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "EmployeeParamSystem.h"
#include "Gacha_UI_Draw.h"

class CCharacterAnimation : public CObject
{
private:
    enum class E_Update
    {
        Transferred, // 転勤
        Fire, // 解雇
    };
private:
    class character
    {
    public:
        character() {
            this->m_release_key = false;
            this->m_update_lock = false;
        }
        ~character() {
            this->m_2dobj.Release();
        }
    public:
        EmployeeParam * m_data;
        CObjectVectorManager<C2DObject> m_2dobj;
        bool m_release_key;
        bool m_update_lock;
    };
    class data
    {
    public:
        data() {
            this->m_stop_time.Init(1, 30);
            this->m_end_time.Init(1, 30);
            this->m_char_data = nullptr;
        }
        ~data() {}
    public:
        character * m_char_data;
        CVector4<float> m_start_pos; // 開始座標
        CVector4<float> m_stop_pos; // 一時停止座標
        CVector4<float> m_end_pos; // 終了座標
        CTimer m_stop_time; // 制御時間
        CTimer m_end_time; // 制御時間
        C2DObject m_pos; // オブジェクト
        E_Update m_type;
    };
public:
    CCharacterAnimation();
    ~CCharacterAnimation();
    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;

    // 解雇データ生成
    void CreateFire(EmployeeParam * _data = nullptr);

    // 転勤データ生成
    void CreateTransferred(EmployeeParam * _data = nullptr);

    // データ選択
    void SerectData(EmployeeParam * _data);

    // 描画オブジェクトのセット
    void SetDrawObject(C2DPolygon * Input);
private:
    // 辞令アニメーション
    void Animation(void);
    // アニメーションの固定化
    void AnimationLock(character * InputChar);
    // 社員アニメーション
    void Animation2(void);
    // ペーパー破棄
    void PaperRelease(void);
private:
    CObjectVectorManager<data> m_obj; // 辞令オブジェクト
    CObjectVectorManager<character> m_character; // シャインオブジェクト
    C2DPolygon * m_poly; // 描画オブジェクト
    CVector4<float>m_start_pos;
    CVector4<float>m_stop_pos;
    void * m_start_script; // スタート制御
    void * m_Sinful_Image; // 罪イメージ
    void * m_sound;
    bool m_sound_lick;
};

