//==========================================================================
// 国の処理[CreateWorldObj.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _CreateWorldObj_H_
#define _CreateWorldObj_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "FieldEditor.h"
#include "Stage.h"

//==========================================================================
//
// class  : CCreateWorldObj
// Content: ワールドオブジェクト生成
//
//==========================================================================
class CCreateWorldObj : public CObject, public CFieldEditor
{
public:
    CCreateWorldObj() : CObject(CObject::ID::Xmodel) {}
    ~CCreateWorldObj() { }
    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;

    // 読み込み
    void Load(StageData_ID Input);
private:
    CStageData m_stage_data;
};

#endif // !_CreateWorldObj_H_
