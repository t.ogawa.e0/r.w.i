//==========================================================================
// Game_Start[Game_Start.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "Stage.h"

//==========================================================================
//
// class  : CGame_Start
// Content: CGame_Start
//
//==========================================================================
class CGame_Start : public CObject
{
public:
    enum class E_Start_Count
    {
        begin = 0,
        count_3 = begin,
        count_2,
        count_1,
        end,
    };
    enum class E_Start_Talk
    {
        begin = (int)E_Start_Count::end,
        talk_3 = begin,
        talk_2,
        talk_1,
        end,
    };
public:
    CGame_Start() : CObject(ID::Polygon2D) {
        this->SetType(Type::Game_Start_Object);
        this->m_key = false;
        this->m_script = 0;
    }
    ~CGame_Start() {
        this->m_time.Release();
    }
    // ������
    bool Init(void)override;
    // ���
    void Uninit(void)override;
    // �X�V
    void Update(void)override;
    // �`��
    void Draw(void)override;
    // �J�n true
    bool Start(void) {
        return this->m_key;
    }
    // �Q�[����~
    void GameStop(void) {
        this->m_key = false;
    }
    // �Q�[����~
    void GameStart(void) {
        this->m_key = true;
    }
private:
    bool m_key; // �X�^�[�g�̐���
    CObjectVectorManager<CTimer>m_time;
    CTimer m_TimeEnd;
    int m_script;
};
