//==========================================================================
// ���S[CLogo.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CLogo
// Content: ���S
//
//==========================================================================
class CLogo : public CObject
{
public:
    CLogo();
    ~CLogo();

    // ������
    bool Init(void)override;

    // ���
    void Uninit(void)override;

    // �X�V
    void Update(void)override;

    // �`��
    void Draw(void)override;
private:
    CTimer m_time;
    CColor<int> m_color;
    void * m_warning;
};

