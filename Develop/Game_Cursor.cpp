//==========================================================================
// カーソル[CGame_Cursor.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Game_Cursor.h"
#include "GameCamera.h"
#include "resource_list.h"

CGame_Cursor::CGame_Cursor() :CObject(CObject::ID::Billboard)
{
    this->SetType(Type::Game_Cursor);
    this->m_game_camera = nullptr;
}

CGame_Cursor::~CGame_Cursor()
{
}

//==========================================================================
// 初期化
bool CGame_Cursor::Init(void)
{
    auto * p_obj = this->_3DObject()->Create();

    p_obj->Init(0);

    p_obj->MoveY(1.0f);

    p_obj->Scale(3);

    p_obj->SetMatrixType(C3DObject::EMatrixType::NotVector);

    this->_3DBillboard()->ObjectInput(p_obj);

    this->m_game_camera = this->GetObjects(ID::Camera, Type::Game_Camera);

    return this->_3DBillboard()->Init(RESOURCE_MainScreen_Cursor_DDS, CBillboard::PlateList::Up);
}

//==========================================================================
// 解放
void CGame_Cursor::Uninit(void)
{
}

//==========================================================================
// 更新
void CGame_Cursor::Update(void)
{
    if (this->m_game_camera == nullptr)
    {
        this->m_game_camera = this->GetObjects(ID::Camera, Type::Game_Camera);
    }

    // オブジェクトが存在するとき
    if (this->m_game_camera != nullptr)
    {
        auto * p_camera = (CGameCamera*)this->m_game_camera;

        this->SetPos(p_camera->_3DObject()->Get(0));
    }

    auto * p_obj = this->_3DObject()->Get(0);

    p_obj->RotX(0.05f);

    this->_3DBillboard()->Update(nullptr);
}

//==========================================================================
// 描画
void CGame_Cursor::Draw(void)
{
    this->_3DBillboard()->Draw();
}

//==========================================================================
// 座標セット
void CGame_Cursor::SetPos(C3DObject * _this)
{
    auto * p_obj = this->_3DObject()->Get(0);

    p_obj->SetMatInfoPos(*_this->GetMatInfoPos());
}
