//==========================================================================
// ゲームイベント:共産主義[CommunismEvent.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "CommunismEvent.h"

//==========================================================================
// 初期化
void CCommunism::Init(void)
{
    CSubsidiaryManager * pchar = nullptr;
    CSubsidiaryParam * ptarget = nullptr;

    // プレイヤーのデータを取り出す
    pchar = CSubsidiary::Get(CharList::Player);

    // 子会社の数だけ回す
    for (int i = 0; i < pchar->GetNumSub(); i++)
    {
        // 売り上げの加算
        ptarget = pchar->GetSub(i);
        this->m_Player_income += ptarget->m_income;
        this->m_Player_income_default += ptarget->m_income_default;
    }
    // 全体の売り上げの平均をだす
    this->m_Player_income = (int)(this->m_Player_income / pchar->GetNumSub());
    this->m_Player_income_default = (int)(this->m_Player_income_default / pchar->GetNumSub());

    // エネミーのデータを取り出す
    pchar = CSubsidiary::Get(CharList::Enemy);

    // 子会社の数だけまわす
    for (int i = 0; i < pchar->GetNumSub(); i++)
    {
        // 売り上げの加算
        ptarget = pchar->GetSub(i);
        this->m_Enemy_income += ptarget->m_income;
        this->m_Enemy_income_default += ptarget->m_income_default;
    }
    // 全体の売り上げの平均をだす
    this->m_Enemy_income = (int)(this->m_Enemy_income / pchar->GetNumSub());
    this->m_Enemy_income_default = (int)(this->m_Enemy_income_default / pchar->GetNumSub());
}

//==========================================================================
// 解放
void CCommunism::Uninit(void)
{
    this->m_target.clear();
    this->m_Name.clear();
}

//==========================================================================
// 更新
void CCommunism::Update(void)
{
    CSubsidiaryManager * pchar = nullptr;
    CSubsidiaryParam * ptarget = nullptr;

    // プレイヤーのデータを取り出す
    pchar = CSubsidiary::Get(CharList::Player);
    for (int i = 0; i < pchar->GetNumSub(); i++)
    {
        // 全エリアに売り上げの平均を移す
        ptarget = pchar->GetSub(i);
        ptarget->m_income_default = this->m_Player_income_default;
        ptarget->m_income = this->m_Player_income;
    }

    // エネミーのデータを取り出す
    pchar = CSubsidiary::Get(CharList::Enemy);
    for (int i = 0; i < pchar->GetNumSub(); i++)
    {
        // 全エリアに売り上げの平均を入れる
        ptarget = pchar->GetSub(i);
        ptarget->m_income_default = this->m_Enemy_income_default;
        ptarget->m_income = this->m_Enemy_income;
    }

    this->m_endkey = true;
}

//==========================================================================
// 描画
void CCommunism::Draw(void)
{
}

//==========================================================================
// UI更新
void CCommunism::UpdateUI(void) 
{
    // 親オブジェクトへのアクセス
    auto * p_obj = (CMobeEventObject*)this->m_master_obj;

    // オブジェクトの座標設定
    this->m_ObjectPos->SetPos(*p_obj->Get()->GetPos());
    this->m_NameObject->SetPos(*p_obj->Get()->GetPos());
}

//==========================================================================
// テキスト
void CCommunism::text(std::string strEventName)
{
    std::string strEvent = strEventName + this->m_Name;
    if (this->m_Imgui.NewMenu(strEvent.c_str(), true))
    {
        this->m_Imgui.Text("発動中");
        this->m_Imgui.Text("次、全エリアの自社売り上げが同じになります");
        this->m_Imgui.EndMenu();
    }
}
