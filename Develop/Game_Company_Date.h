//==========================================================================
// シシャデータの表示[Game_Company_Date.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _Game_Company_Date_H_
#define _Game_Company_Date_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "Character.h"

//==========================================================================
//
// class  : CGame_Company_Date
// Content: シシャデータの表示
//
//==========================================================================
class CGame_Company_Date : public CObject
{
private:
    enum class TexList {
        MainScreen_Company_Date, // シシャデータ表示用背景
        MainScreen_Company_Date_tab, // タブ
        CompanyText, // シシャ名
        honsya_mark, // 本社マーク
    };

    enum class Namber {
        NumberTexture1, // 数字テクスチャ(万)
        NumberTexture2, // 数字テクスチャ(ニン)
        NumberTexture3, // 数字テクスチャ(シェア率)
    };

    // アニメーションキー
    enum class JapanList {
        ポン,
        ヨシノ,
        ショウク,
        シナノ,
        ハッカイ,
        ヒカフ,
        コクシ,
        ニト,
        end,
    };
    // アニメーションキー
    enum class USAList {
        ビームス,
        ジャック,
        トマティ,
        トルマ,
        ベレンタイン,
        ローゼス,
        ウォーカー,
        ホース,
        ヘネス,
        ハーパー,
        end,
    };
    // アニメーションキー
    enum class HawaiiList {
        ソルティ,
        ミェール,
        ヌテイン,
        ジニック,
        end,
    };
    // アニメーションキー
    enum class AustraliaList {
        タンカレー,
        ボンベイ,
        シロッコ,
        ヒル,
        ゴードン,
        ギルビー,
        フィーター,
        end,
    };
private:
    // シシャテクスチャ切り替え用
    class CDataList
    {
    public:
        CDataList() {
            this->m_CompanyName = "";
            this->m_texID = -1;
        }
        ~CDataList() {
            this->m_CompanyName.clear();
        }

        void Set(const std::string & Name, int texID) {
            this->m_CompanyName = Name;
            this->m_texID = texID;
        }

        int GetTexID(void) {
            return this->m_texID;
        }

        std::string & GetTag(void) {
            return this->m_CompanyName;
        }
    private:
        std::string m_CompanyName; // シシャ名
        int m_texID;
    };
public:
    CGame_Company_Date() :CObject(CObject::ID::Polygon2D) {
        this->SetType(Type::Game_Company_Date);
        this->m_OLDSerectID = -1;
        this->m_EmergenceKey = false;
        this->m_anum_key = 0;
        this->m_Camera = nullptr;
    }
    ~CGame_Company_Date() {}
    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;

    // 表示open
    void Game_Company_Data_Open(void) {
        this->m_EmergenceKey = true;
    }

    // 表示clause
    void Game_Company_Data_Clause(void) {
        this->m_EmergenceKey = false;
    }
private:
    // デバッグウィンドウ
    void DebugWindow(void);

    // 日本
    bool Japan_Init(int & OutAnim);

    // USA
    bool USA_Init(int & OutAnim);

    // ハワイ
    bool Hawaii_Init(int & OutAnim);

    // オーストラリア
    bool Australia_Init(int & OutAnim);

    // シシャ名テクスチャの切り替え
    void CompanyNameIDUpdate(void);

    // 出現/終了
    // 戻り値 移動が完全に終了した際にtrue
    bool Emergence(void);

    // UIの移動
    bool UIMove(float fLimit, float fSpeed);
private:
    CObjectVectorManager<CDataList>m_Data;
    CObjectVectorManager<CVector4<float>>m_MasterPos; // 親座標
    int m_OLDSerectID; // 古い選択ID
    bool m_EmergenceKey; // 出現操作キー
    void * m_Camera; // カメラへのアクセスルート
    int m_anum_key;
};

#endif // !_Game_Company_Date_H_
