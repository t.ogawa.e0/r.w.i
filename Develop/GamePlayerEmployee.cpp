//==========================================================================
// プレイヤーの社員データ管理[GamePlayerEmployee.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "GamePlayerEmployee.h"
#include "LoadGameData.h"
#include "GameCamera.h"
#include "Player.h"
#include "LoadGameData.h"
#include "Worldtime.h"
#include "OperationExplanation.h"
#include "Gacha_UI_Draw.h"
#include "Game_Start.h"
#include "CharacterAnimation.h"

//==========================================================================
// 初期化
bool CGamePlayerEmployee::Init(void)
{
    this->FastCreate();
    this->m_camera = this->GetObjects(ID::Camera, Type::Game_Camera);
    this->m_player = this->GetObjects(ID::Default, Type::Game_Player);
    this->m_WorldTime = this->GetObjects(CObject::ID::Default, CObject::Type::Game_WorldTime);
    this->m_OperationExplanation = this->GetObjects(CObject::ID::Polygon2D, CObject::Type::Game_OperationExplanation);
    this->m_Gacha_UI_Draw = this->GetObjects(CObject::ID::Polygon2D, CObject::Type::Game_GachaUI);
    this->m_start_script = this->GetObjects(ID::Polygon2D, Type::Game_Start_Object);
    this->m_TransferAnimation = this->GetObjects(ID::Polygon2D, Type::Game_CharacterAnimation);

    this->XInput()->Init(1);

    return false;
}

//==========================================================================
// 解放
void CGamePlayerEmployee::Uninit(void)
{
}

//==========================================================================
// 更新
void CGamePlayerEmployee::Update(void)
{
    if (this->m_camera == nullptr)
    {
        this->m_camera = this->GetObjects(ID::Camera, Type::Game_Camera);
    }

    if (this->m_player == nullptr)
    {
        this->m_player = this->GetObjects(ID::Default, Type::Game_Player);
    }

    if (this->m_WorldTime == nullptr)
    {
        this->m_WorldTime = this->GetObjects(CObject::ID::Default, CObject::Type::Game_WorldTime);
    }

    if (this->m_OperationExplanation == nullptr)
    {
        this->m_OperationExplanation = this->GetObjects(CObject::ID::Polygon2D, CObject::Type::Game_OperationExplanation);
    }

    if (this->m_Gacha_UI_Draw == nullptr)
    {
        this->m_Gacha_UI_Draw = this->GetObjects(CObject::ID::Polygon2D, CObject::Type::Game_GachaUI);
    }

    if (this->m_start_script == nullptr)
    {
        this->m_start_script = this->GetObjects(ID::Polygon2D, Type::Game_Start_Object);
    }
    if (this->m_TransferAnimation == nullptr)
    {
        this->m_TransferAnimation = this->GetObjects(ID::Polygon2D, Type::Game_CharacterAnimation);
    }

    auto * p_start = (CGame_Start*)this->m_start_script;

    // オブジェクトが存在するとき
    if (p_start != nullptr)
    {
        if (p_start->Start())
        {
            // ImGuidebug
            this->ImGuiDebug();

            // 操作2
            this->Action();

            this->Move();

            // ストライキ
            this->Strike();

            this->XInput()->Update();
        }
    }
}

//==========================================================================
// 描画
void CGamePlayerEmployee::Draw(void)
{

}

//==========================================================================
// 雇用
void CGamePlayerEmployee::Employment(void)
{
    auto * _player = (CPlayer*)this->m_player;

    // オブジェクトが存在するとき
    if (_player != nullptr)
    {
        auto * p_player = _player->GetCubsidiaryData();
        auto * p_employee_data = (CEmployeeManager*)this->m_EmployeeManager;

        // オブジェクトが存在するとき
        if (p_player != nullptr&&p_employee_data != nullptr)
        {
            // 本社探索
            for (int i = 0; i < p_player->GetNumSub(); i++)
            {
                if (p_player->GetSub(i)->m_head_office_player == true)
                {
                    auto itr = this->m_data.find(p_player->GetSub(i)->m_id);
                    // 探索に成功したとき
                    if (itr != this->m_data.end())
                    {
                        auto * p_emp = p_employee_data->Create();
                        auto * p_Gacha_UI_Draw = (CGacha_UI_Draw*)this->m_Gacha_UI_Draw;
                        itr->second.emplace_back(p_emp);
                        p_Gacha_UI_Draw->Create(p_emp);
                        //this->m_lookID = (int)itr->second.size() - 1;
                    }
                }
            }
        }
    }

    //auto * p_came = (CGameCamera*)this->m_camera;
    //auto * p_employee_data = (CEmployeeManager*)this->m_EmployeeManager;

    //auto itr = this->m_data.find(p_came->GetSerectID());
    //// 探索に成功したとき
    //if (itr != this->m_data.end())
    //{
    //    auto * p_emp = p_employee_data->Create();
    //    auto * p_Gacha_UI_Draw = (CGacha_UI_Draw*)this->m_Gacha_UI_Draw;
    //    itr->second.emplace_back(p_emp);
    //    p_Gacha_UI_Draw->Create(p_emp, CGacha_UI_Draw::UIKey::Input);
    //    this->m_lookID = (int)itr->second.size() - 1;
    //}
}

//==========================================================================
// 解雇
void CGamePlayerEmployee::Fire(void)
{
    auto * p_came = (CGameCamera*)this->m_camera;
    auto * p_traobj = (CCharacterAnimation*)this->m_TransferAnimation;

    // オブジェクトが存在するとき
    if (p_came != nullptr)
    {
        // データの補正
        auto itr = this->m_data.find(p_came->GetSerectID());

        // 探索に成功したとき
        if (itr != this->m_data.end())
        {
            for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); ++itr2)
            {
                if ((*itr2) == this->m_LookEmployeeParam)
                {
                    if (p_traobj != nullptr)
                    {
                        p_traobj->SerectData((*itr2));
                        p_traobj->CreateFire((*itr2));
                    }
                    itr->second.erase(itr2);
                    break;
                }
            }
        }
    }
}

//==========================================================================
// 転勤
void CGamePlayerEmployee::Transferred(int _select)
{
    auto * p_Gacha_UI_Draw = (CGacha_UI_Draw*)this->m_Gacha_UI_Draw;
    auto * p_traobj = (CCharacterAnimation*)this->m_TransferAnimation;

    // オブジェクトが存在するとき
    if (p_Gacha_UI_Draw != nullptr)
    {
        if (this->m_EmployeeParam != nullptr&&this->m_id != _select)
        {
            // データの補正
            auto itr = this->m_data.find(this->m_id);

            // 探索に成功したとき
            if (itr != this->m_data.end())
            {
                for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); ++itr2)
                {
                    if ((*itr2) == this->m_EmployeeParam)
                    {
                        if (p_traobj != nullptr)
                        {
                            p_traobj->CreateTransferred((*itr2));
                        }

                        itr->second.erase(itr2);
                        break;
                    }
                }
                // 移動経路の登録
                this->m_task.emplace_back(this->RootSearch(_select));
                this->m_EmployeeParam = nullptr;
                this->m_lookID = 0;
            }
        }
    }
}

//==========================================================================
// 転勤予定
void CGamePlayerEmployee::TransferredPlans(EmployeeParam * _this, int _select)
{
    // コントローラーチェック
    this->m_EmployeeParam = _this;
    this->m_id = _select;
}

//==========================================================================
// 操作2
void CGamePlayerEmployee::Action(void)
{
    auto * p_came = (CGameCamera*)this->m_camera;
    auto * p_oe_key = (COperationExplanation*)this->m_OperationExplanation;
    auto * p_traobj = (CCharacterAnimation*)this->m_TransferAnimation;

    // オブジェクトが存在するとき
    if (p_came != nullptr&&p_oe_key != nullptr)
    {
        if (this->XInput()->Check(0))
        {
            // ここで採用人数を加算しています。
            if (this->XInput()->Trigger(CXInput::EButton::Y, 0) && p_oe_key->Key_Syokuin() == true)
            {
                this->Employment();
            }

            // 人数分削る
            if (this->XInput()->Trigger(CXInput::EButton::A, 0) && p_oe_key->Key_Syokuin() == true)
            {
                this->Fire();
            }

            // 転勤地決定
            if (this->XInput()->Trigger(CXInput::EButton::X, 0))
            {
                // 移動対象を移動
                if (this->m_id != p_came->GetSerectID() && p_oe_key->Key_Transfer() == true)
                {
                    this->Transferred(p_came->GetSerectID());
                }

                // 転勤対象取得
                if (this->m_EmployeeParam == nullptr&&this->m_LookEmployeeParam != nullptr&& p_oe_key->Key_Syokuin() == true)
                {
                    // ここで移動対象をセットする
                    this->TransferredPlans(this->m_LookEmployeeParam, p_came->GetSerectID());
                    if (p_traobj != nullptr)
                    {
                        p_traobj->SerectData(this->m_LookEmployeeParam);
                    }
                    return;
                }
            }

            // 左
            if (this->XInput()->Trigger(CXInput::EButton::DPAD_LEFT, 0) && p_oe_key->Key_Syokuin() == true)
            {
                this->m_lookID--;
            }

            // 右
            if (this->XInput()->Trigger(CXInput::EButton::DPAD_RIGHT, 0) && p_oe_key->Key_Syokuin() == true)
            {
                this->m_lookID++;
            }
        }

        // データの補正
        auto itr = this->m_data.find(p_came->GetSerectID());

        // 探索に成功したとき
        if (itr != this->m_data.end())
        {
            // 0以下になったら後ろに
            if (this->m_lookID < 0)
            {
                this->m_lookID = (int)(itr->second.size() - 1);
            }

            // 最大を超えたら前に
            if ((int)(itr->second.size()) <= this->m_lookID)
            {
                this->m_lookID = 0;
            }

            // 今見ているところ
            int nserectID = 0;
            this->m_LookEmployeeParam = nullptr;
            for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); ++itr2)
            {
                if (nserectID == this->m_lookID)
                {
                    this->m_LookEmployeeParam = (*itr2);
                }
                nserectID++;
            }
        }
    }
}

//==========================================================================
// 初期セットアップ
void CGamePlayerEmployee::FastCreate(void)
{
    this->m_EmployeeManager = this->GetObjects(ID::Default, Type::Game_EmployeeManager);
    auto * p_employee_data = (CEmployeeManager*)this->m_EmployeeManager;

    // オブジェクトが存在するとき
    if (p_employee_data != nullptr)
    {
        // 読み込まれたデータより生成
        for (int i = 0; i < CLoadGameData::Size(); i++)
        {
            auto * p_data = &CLoadGameData::GetData(i);

            // 各シシャ毎の社員数だけ回す
            for (int s = 0; s < p_data->m_param.m_employee; s++)
            {
                this->m_data[p_data->m_param.m_id].emplace_back(p_employee_data->Create());
            }
        }
    }
}

//==========================================================================
// ImGuiでのデバッグ
void CGamePlayerEmployee::ImGuiDebug(void)
{
    auto * p_came = (CGameCamera*)this->m_camera;
    auto * p_emm = (CEmployeeManager*)this->m_EmployeeManager;

    // オブジェクトが存在するとき
    if (p_came != nullptr&&p_emm != nullptr)
    {
        if (this->ImGui()->NewMenu("社員データ"))
        {
            if (this->ImGui()->NewMenu("社員データ表示"))
            {
                auto std_list = this->m_data.find(p_came->GetSerectID());

                // 探索に成功したとき
                if (std_list != this->m_data.end())
                {
                    for (auto itr = std_list->second.begin(); itr != std_list->second.end(); ++itr)
                    {
                        p_emm->SetDebugText((*itr));
                        if (this->ImGui()->Button("[%x]転勤", (*itr)))
                        {
                            this->TransferredPlans((*itr), p_came->GetSerectID());
                        }
                        this->ImGui()->Separator();
                    }
                }
                this->ImGui()->EndMenu();
            }
            if (this->ImGui()->NewMenu("転勤予定社員"))
            {
                p_emm->SetDebugText(this->m_EmployeeParam);
                this->ImGui()->EndMenu();
            }
            if (this->ImGui()->NewMenu("今見ているシャイン"))
            {
                p_emm->SetDebugText(this->m_LookEmployeeParam);
                this->ImGui()->EndMenu();
            }
            if (this->ImGui()->Button("今見ているシシャに転勤"))
            {
                this->Transferred(p_came->GetSerectID());
            }
            if (this->ImGui()->Button("転勤のキャンセル"))
            {
                this->m_EmployeeParam = nullptr;
            }
            this->ImGui()->EndMenu();
        }
    }
}

//==========================================================================
// 最短経路探索
CCEmployeeTask CGamePlayerEmployee::RootSearch(int _select)
{
    auto * _player = (CPlayer*)this->m_player;
    std::vector<int>std_root; // ルート
    std::unordered_map<int, std::vector<void*>>root_pt_case; // ルートポインタ
    std::vector<void*>root_pt; // ルート

    auto * p_player = _player->GetCubsidiaryData();

    // 中継ポインタ探索 next
    int root = 0; // 初期ルート
    for (int search = this->m_id;;)
    {
        // ルートアドレスの登録
        root_pt.emplace_back(p_player->GetSub(search));

        root++; // ルートカウンタ

                // ID一致時
        if (_select == search)
        {
            root--;
            break;
        }

        // 一致せずに終了IDが出た場合
        if (search == -1)
        {
            root = 999999;
            break;
        }

        // IDの探索
        search = p_player->GetSub(search)->m_next;
    }

    // ルート情報のセット
    std_root.emplace_back(root);
    root_pt_case[root] = root_pt;
    root_pt.clear();

    // 中継ポインタ探索 
    // 中継ポインタ探索 prev
    root = 0;
    for (int search = this->m_id;;)
    {
        // ルートアドレスの登録
        root_pt.emplace_back(p_player->GetSub(search));

        root++; // ルートカウンタ

                // ID一致時
        if (_select == search)
        {
            root--;
            break;
        }

        // 一致せずに終了IDが出た場合
        if (search == -1)
        {
            root = 999999;
            break;
        }

        // IDの探索
        search = p_player->GetSub(search)->m_prev;
    }

    // ルート情報のセット
    std_root.emplace_back(root);
    root_pt_case[root] = root_pt;
    root_pt.clear();

    // 最短経路検出
    int turn = min(std_root[0], std_root[1]);

    this->m_dataIDManager++;

    // タスクデータの生成
    return CCEmployeeTask(this->m_EmployeeParam, root_pt_case[turn], this->m_dataIDManager, this->m_id, _select, turn);
}

//==========================================================================
// ストライキ
void CGamePlayerEmployee::Strike(void)
{
    auto * _player = (CPlayer*)this->m_player;

    if (_player != nullptr)
    {
        if (_player->GetStrikeKey() == true)
        {
            for (auto itr = this->m_data.begin(); itr != this->m_data.end(); ++itr)
            {
                if (itr->second.size() != 0)
                {
                    int n_count = 0;
                    std::unordered_map<int, int> id_b;
                    std::uniform_int_distribution<int> strike_rand = std::uniform_int_distribution<int>(0, itr->second.size() - 1);

                    // リタイアテーブル生成
                    for (int i = 0; i < (int)((itr->second.size() / 2) + 0.5f); i++)
                    {
                        int n_rand = strike_rand(this->m_mt);
                        id_b[n_rand] = n_rand;
                    }

                    // ストライキ対象の検索
                    for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); )
                    {
                        // ストライキ対象検出
                        auto itr3 = id_b.find(n_count);
                        if (itr3 != id_b.end())
                        {
                            itr2 = itr->second.erase(itr2);
                        }
                        else
                        {
                            ++itr2;
                        }
                        n_count++;
                    }
                    id_b.clear();
                }
            }
        }
    }
}

//==========================================================================
// データの移動
void CGamePlayerEmployee::Move(void)
{
    auto*pTime = (CWorldTime*)this->m_WorldTime;

    if (pTime != nullptr)
    {
        if (pTime->getturns().m_current != pTime->getturns().m_old)
        {
            for (auto itr = this->m_task.begin(); itr != this->m_task.end(); )
            {
                itr->m_now++;

                // 移動ターンと一致した際に移動終了
                if (itr->m_turn < itr->m_now)
                {
                    // データの補正
                    auto itr2 = this->m_data.find(itr->m_id_next);

                    // 探索に成功したとき
                    if (itr2 != this->m_data.end())
                    {
                        itr2->second.emplace_back(itr->m_EmployeeParam);
                    }

                    // 処理終了IDの登録
                    this->m_end_data_id.push_back(itr->m_data_id);

                    // 仮のイテレータ
                    itr = this->m_task.erase(itr);
                }
                else
                {
                    ++itr;
                }
            }
        }
    }

    // 終了IDの破棄
    for (auto itr = this->m_end_data_id.begin(); itr != this->m_end_data_id.end(); )
    {
        // 終了キー検出時
        if ((*itr) == -1)
        {
            // 仮のイテレータ
            itr = this->m_end_data_id.erase(itr);
        }
        else
        {
            ++itr;
        }
    }
}
