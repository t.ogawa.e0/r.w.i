//==========================================================================
// ゲームイベント:油田が見つかった[OilFieldsWereFoundEvent.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _OilFieldsWereFoundEvent_H_
#define _OilFieldsWereFoundEvent_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "GameEvent.h"

//==========================================================================
//
// class  : COilFieldsWereFound
// Content: 油田が見つかった
//
//==========================================================================
class COilFieldsWereFound : public CEventManager
{
public:
    COilFieldsWereFound()
    {
        this->m_Name = "油田が見つかった";

    }
    ~COilFieldsWereFound()
    {
        this->m_target.clear();
        this->m_Name.clear();
    }

    // 初期化
    void Init(std::mt19937 & InputMT);

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;

    // UI更新
    void UpdateUI(void)override;

    // テキスト
    void text(std::string strEventName)override;
private:

};

#endif // !_OilFieldsWereFoundEvent_H_
