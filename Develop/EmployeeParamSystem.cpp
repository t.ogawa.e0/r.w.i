//==========================================================================
// EmployeeParamSystem[EmployeeParamSystem.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "EmployeeParamSystem.h"

//==========================================================================
// タイプの生成
void CEmployeeParamSystem::CreateType(CParam * _this)
{
    for (;_this->GetTypeID()->id == -1;)
    {
        float n_rand = this->m_type_rand(this->m_mt);

        // タイプの確立表を回す
        for (auto itr = this->m_type_table.begin(); itr != this->m_type_table.end(); ++itr)
        {
            // 確立探索
            if (itr->search(n_rand))
            {
                auto itr_ = this->m_type.find(itr->get_data());

                // 探索に成功したとき
                if (itr_ != this->m_type.end())
                {
                    _this->SetTypeID(CData_<std::string>(itr_->second, itr_->first));
                    break;
                }
            }
        }
    }
}

//==========================================================================
// スキルの生成
void CEmployeeParamSystem::CreateAge(CParam * _this)
{
    for (;_this->GetAge()->id == -1;)
    {
        int n_rand = this->m_age_rand(this->m_mt);

        // レアリティの探索
        auto itr_ = this->m_age.find(n_rand);

        // 探索に成功したとき
        if (itr_ != this->m_age.end())
        {
            _this->SetAge(CData_<std::string>(itr_->first, itr_->second));
        }
    }
}

//==========================================================================
// 国籍の生成
void CEmployeeParamSystem::CreateCountry(CParam * _this)
{
    for (;_this->GetCountry()->id == -1;)
    {
        int n_rand = this->m_country_rand(this->m_mt);

        // レアリティの探索
        auto itr_ = this->m_country.find(n_rand);

        // 探索に成功したとき
        if (itr_ != this->m_country.end())
        {
            _this->SetCountry(CData_<std::string>(itr_->first, itr_->second));
        }
    }
}

//==========================================================================
// メンタルの生成
void CEmployeeParamSystem::CreatePersonality(CParam * _this)
{
    for (;_this->GetPersonality()->id == -1;)
    {
        int n_rand = this->m_personality_rand(this->m_mt);

        // レアリティの探索
        auto itr_ = this->m_personality.find(n_rand);

        // 探索に成功したとき
        if (itr_ != this->m_personality.end())
        {
            _this->SetPersonality(CData_<std::string>(itr_->first, itr_->second));
        }
    }
}

//==========================================================================
// 特徴の生成
void CEmployeeParamSystem::CreateCha(CParam * _this)
{
    float f_rand = 0.0f;

    for (;_this->GetChaID()->id == -1;)
    {
        f_rand = this->m_cha_rand(this->m_mt);

        // タイプの確立表を回す
        for (auto itr = this->m_cha_table.begin(); itr != this->m_cha_table.end(); ++itr)
        {
            // 確立探索
            if (itr->search(f_rand))
            {
                auto itr_ = this->m_cha.find(itr->get_data());

                // 探索に成功したとき
                if (itr_ != this->m_cha.end())
                {
                    _this->SetChaID(CData_ <std::string>(itr_->second, itr_->first));
                }

                break;
            }
        }
    }
}

//==========================================================================
// パートナーの生成
void CEmployeeParamSystem::CreatePartner(CParam * _this)
{
    float f_rand = 0.0f;

    for (;_this->GetPartner()->id == -1;)
    {
        f_rand = this->m_partner_rand(this->m_mt);

        // レアリティの確立表を回す
        for (auto itr = this->m_partner_table.begin(); itr != this->m_partner_table.end(); ++itr)
        {
            // 確立探索
            if (itr->search(f_rand))
            {
                auto itr_ = this->m_partner.find(itr->get_data());

                // 探索に成功したとき
                if (itr_ != this->m_partner.end())
                {
                    _this->SetPartner(CData_<std::string>((int)itr_->second, itr_->first));
                }

                break;
            }
        }
    }
}

//==========================================================================
// アイコン生成
void CEmployeeParamSystem::CreateICO(CParam * _this)
{
    int nID = this->m_name_rand(this->m_mt);
    _this->SetIcoFrameID(this->m_ico_rand(this->m_mt));
    _this->SetName(CData_ <std::string>(nID, this->m_name[nID]));
}

//==========================================================================
// デバッグ用
void CEmployeeParamSystem::SetText(CParam * _this)
{
    if (_this != nullptr)
    {
        this->m_ImGui.Text("名前 : %s", _this->GetName()->data.c_str());
        this->m_ImGui.Text("%s", _this->GetPartner()->data.c_str());
        this->m_ImGui.Text("タイプ : %s", _this->GetTypeID()->data.c_str());
        this->m_ImGui.Text("特徴 : %s", _this->GetChaID()->data.c_str());
        this->m_ImGui.Text("年齢 : %s", _this->GetAge()->data.c_str());
        this->m_ImGui.Text("国籍 : %s", _this->GetCountry()->data.c_str());
        this->m_ImGui.Text("性格 : %s", _this->GetPersonality()->data.c_str());
        this->m_ImGui.Text("アイコンID : %d", _this->GetIcoFrameID());
    }
}
