//==========================================================================
// メニュー[Home.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _Home_H_
#define _Home_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "SceneChange.h"

//==========================================================================
//
// class  : CHome
// Content: メニュー
//
//==========================================================================
class CHome : public CBaseScene
{
public:
    CHome();
    ~CHome();
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
private:

};

#endif // !_Home_H_
