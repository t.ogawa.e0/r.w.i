//==========================================================================
// �܂ˁ[[Money.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Money.h"
#include "Worldtime.h"
#include "Player.h"
#include "Game_Start.h"
#include "resource_list.h"

CMoney::CMoney() :CObject(ID::Polygon2D)
{
    this->m_player = nullptr;
    this->m_WorldTime = nullptr;
    this->m_start_script = nullptr;
}


CMoney::~CMoney()
{
}

//==========================================================================
// ������
bool CMoney::Init(void)
{
    this->m_WorldTime = this->GetObjects(CObject::ID::Default, CObject::Type::Game_WorldTime);
    this->m_player = this->GetObjects(ID::Default, Type::Game_Player);
    this->m_start_script = this->GetObjects(ID::Polygon2D, Type::Game_Start_Object);


    if (this->_2DPolygon()->Init(RESOURCE_Okane_Up_Re02_DDS, true))
    {
        return true;
    }
    if (this->_2DPolygon()->Init(RESOURCE_Okane_Down_Re02_DDS, true))
    {
        return true;
    }

    return false;
}

//==========================================================================
// ���
void CMoney::Uninit(void)
{
}

//==========================================================================
// �X�V
void CMoney::Update(void)
{
    if (this->m_WorldTime == nullptr)
    {
        this->m_WorldTime = this->GetObjects(CObject::ID::Default, CObject::Type::Game_WorldTime);
    }
    if (this->m_player == nullptr)
    {
        this->m_player = this->GetObjects(ID::Default, Type::Game_Player);
    }
    if (this->m_start_script == nullptr)
    {
        this->m_start_script = this->GetObjects(ID::Polygon2D, Type::Game_Start_Object);
    }

    if (this->m_start_script != nullptr)
    {
        auto*pTime = (CWorldTime*)this->m_WorldTime;
        auto*p_player = (CPlayer*)this->m_player;
        auto * p_start = (CGame_Start*)this->m_start_script;

        if (p_start->Start())
        {
            if (p_player != nullptr&&pTime != nullptr)
            {
                if (pTime->getturns().m_current != pTime->getturns().m_old)
                {
                    if (p_player->GetAsset()!= p_player->GetOldAsset())
                    {
                        // ���Y�A�Q���A�Q��
                        if (p_player->GetAsset_Min_Max() == true)
                        {
                            this->Create(0);
                        }
                        // ���Y�T�Q���T�Q��
                        else if (p_player->GetAsset_Min_Max() == false)
                        {
                            this->Create(1);
                        }
                    }
                }
            }
        }

        // �^�C�}�[����
        for (int i = 0; i < this->_2DObject()->Size(); i++)
        {
            auto * p_obj = this->_2DObject()->Get(i);

            if (p_obj->GetPattanNum())
            {
                this->_2DPolygon()->ObjectDelete(p_obj);
                this->_2DObject()->PinpointRelease(p_obj);
                break;
            }
        }
    }

    this->_2DPolygon()->Update();
}

//==========================================================================
// �`��
void CMoney::Draw(void)
{
    this->_2DPolygon()->Draw();
}

//==========================================================================
// �G�t�F�N�g����
void CMoney::Create(int id)
{
    auto * p_obj = this->_2DObject()->Create();

    p_obj->Init(id, 2, 30, 10);
    p_obj->SetCentralCoordinatesMood(true);
    p_obj->SetPos({ this->GetWinSize().m_Width / 2.0f ,this->GetWinSize().m_Height / 2.0f });
    p_obj->Scale(1.2f);

    this->_2DPolygon()->ObjectInput(p_obj);
}
