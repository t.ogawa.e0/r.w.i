//==========================================================================
// タイトルサウンド[Title_Sound.cpp]
// author: yoji watanabe
//==========================================================================
#include "Title_Sound.h"
#include "resource_list.h"

CTitle_Sound::CTitle_Sound()
{
    this->m_key = false;
}

CTitle_Sound::~CTitle_Sound()
{
}

//==========================================================================
// 初期化
bool CTitle_Sound::Init(void)
{
	this->XAudio()->Init(CXAudio2::SoundLabel(RESOURCE_GameCon_title_wav, -1, 0.5f));
	this->XAudio()->Init(CXAudio2::SoundLabel(RESOURCE_決定_wav, 0, 1.0f));
	this->XAudio()->Init(CXAudio2::SoundLabel(RESOURCE_移動_wav, 0, 1.0f));

	return false;
}

//==========================================================================
// 解放
void CTitle_Sound::Uninit(void)
{
}

//==========================================================================
// 更新
void CTitle_Sound::Update(void)
{
    if (this->m_key == false)
    {
        this->PlayBGM();
        this->m_key = true;
    }
}

//==========================================================================
// 描画
void CTitle_Sound::Draw(void)
{
}

//==========================================================================
// タイトルBGM
void CTitle_Sound::PlayBGM(void)
{
	this->XAudio()->Play((int)SoundList::Title_BGM);
}

//==========================================================================
// 決定SE
void CTitle_Sound::PlayDecide_se(void)
{
	this->XAudio()->Play((int)SoundList::Decide_SE);
}

//==========================================================================
// 移動SE
void CTitle_Sound::PlayMove_se(void)
{
	this->XAudio()->Play((int)SoundList::Move_SE);
}