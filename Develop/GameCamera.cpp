//==========================================================================
// GameCamera[GameCamera.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "GameCamera.h"
#include "LoadGameData.h"

//==========================================================================
// 初期化
bool CGameCamera::Init(void)
{
    D3DXVECTOR3 Eye = D3DXVECTOR3(0.0f, 6.0f, 8.0f); // 注視点
    D3DXVECTOR3 At = D3DXVECTOR3(0.0f, 4.0f, 0.0f); // カメラ座標
    this->Camera()->Init(Eye, At);

    // 地上を這いずり回るオブジェクト(ニート)
    this->_3DObject()->Create()->Init(0);

    // 地上と空中を行き来する本体(クロネコヤマト)
    this->_3DObject()->Create()->Init(0);

    // 高みの見物をするオブジェクト(VIP)
    this->_3DObject()->Create()->Init(0);

    for (int i = 0; i < this->_3DObject()->Size(); i++)
    {
        this->_3DObject()->Get(i)->MoveY(1);
    }

    // オブジェクトのインスタンス生成
    for (int i = 0; i < CLoadGameData::Size(); i++)
    {
        CFieldData * fie = this->m_CObjectData.Create();
        *fie = CLoadGameData::GetData(i);
    }

    // デフォルトの距離を記録/制御数値とする
    this->m_vew_pos = this->m_min_limit_pos = this->Camera()->DistanceFromView(0.0f);

    this->XInput()->Init(1);

    this->_3DObject()->Get((int)E_Type::VIPtan)->MoveZ(-30.0f);

    // 初期選択エリアを本社に固定
    for (int i = 0; i < this->m_CObjectData.Size(); i++)
    {
        if (this->m_CObjectData.Get(i)->m_head_office_player)
        {
            this->m_ObjectSerect = this->m_CObjectData.Get(i)->m_param.m_id;
            break;
        }
    }

    return false;
}

//==========================================================================
// 解放
void CGameCamera::Uninit(void)
{
    this->m_CObjectData.Release();
}

//==========================================================================
// 更新
void CGameCamera::Update(void)
{
    this->MoveSerect();
    this->Move();
    this->Zoom_IN_OUT();
    this->XInput()->Update();
    this->Camera()->Update(this->GetWinSize().m_Width, this->GetWinSize().m_Height, this->GetDirectX9Device());
}

//==========================================================================
// 描画
void CGameCamera::Draw(void)
{
}

//==========================================================================
// カメラの移動処理
void CGameCamera::MoveSerect(void)
{
    if (this->XInput()->Check(0))
    {
        if (this->XInput()->Trigger(CXInput::EButton::LEFT_LB, 0))
        {
            this->m_ObjectSerect = this->m_CObjectData.Get(this->m_ObjectSerect)->m_param.m_prev;
            if (this->m_ObjectSerect == -1)
            {
                this->m_ObjectSerect = this->m_CObjectData.Size() - 1;
            }
        }

        if (this->XInput()->Trigger(CXInput::EButton::RIGHT_RB, 0))
        {
            this->m_ObjectSerect = this->m_CObjectData.Get(this->m_ObjectSerect)->m_param.m_next;
            if (this->m_ObjectSerect == -1)
            {
                this->m_ObjectSerect = 0;
            }
        }
    }

    if (this->Dinput_Keyboard() != nullptr)
    {
        if (this->Dinput_Keyboard()->Trigger(CKeyboard::KeyList::KEY_A))
        {
            this->m_ObjectSerect = this->m_CObjectData.Get(this->m_ObjectSerect)->m_param.m_prev;
            if (this->m_ObjectSerect == -1)
            {
                this->m_ObjectSerect = this->m_CObjectData.Size() - 1;
            }
        }

        if (this->Dinput_Keyboard()->Trigger(CKeyboard::KeyList::KEY_D))
        {
            this->m_ObjectSerect = this->m_CObjectData.Get(this->m_ObjectSerect)->m_param.m_next;
            if (this->m_ObjectSerect == -1)
            {
                this->m_ObjectSerect = 0;
            }
        }
    }
}

//==========================================================================
// カメラの移動処理
void CGameCamera::Move(void)
{
    // 地上を這いずり回る
    if (!this->_Hit()->Ball(*this->_3DObject()->Get((int)E_Type::Neet), this->m_CObjectData.Get(this->m_ObjectSerect)->m_pos, 1.1f))
    {
        for (int i = 0; i < 10; i++)
        {
            this->_3DObject()->Get((int)E_Type::Neet)->LockOn(this->m_CObjectData.Get(this->m_ObjectSerect)->m_pos, 0.6f);
        }
        float powor = this->_Hit()->Distance(*this->_3DObject()->Get((int)E_Type::Neet), this->m_CObjectData.Get(this->m_ObjectSerect)->m_pos)*0.5f;
        this->_3DObject()->Get((int)E_Type::Neet)->MoveZ(powor);
    }
    else
    {
        D3DXVECTOR3 vpos = *this->m_CObjectData.Get(this->m_ObjectSerect)->m_pos.GetMatInfoPos();
        vpos.y = this->_3DObject()->Get((int)E_Type::Neet)->GetMatInfoPos()->y;
        this->_3DObject()->Get((int)E_Type::Neet)->SetMatInfoPos(vpos);
    }

    this->Camera()->RotViewX(0.001f);
}

//==========================================================================
// ズーム/IN/OUT
void CGameCamera::Zoom_IN_OUT(void)
{
    // 同時押しをした際にズームイン/ズームアウト
    if (this->XInput()->LT(0) || this->XInput()->RT(0))
    {
        if (this->m_LT_RT == false)
        {
            // 切り替え
            this->Helper()->Bool_(this->m_key_zoom);
            this->m_LT_RT = true;
        }
    }
    else
    {
        this->m_LT_RT = false;
    }

    // ズームアウト
    if (this->m_key_zoom == true)
    {
        // 空中オブジェクト
        float f_seed = this->_Hit()->Distance(*this->_3DObject()->Get((int)E_Type::VIPtan), *this->_3DObject()->Get((int)E_Type::Kuroneko));
   
        for (int i = 0; i < 10; i++)
        {
            this->_3DObject()->Get((int)E_Type::Kuroneko)->LockOn(*this->_3DObject()->Get((int)E_Type::VIPtan), 0.6f);
        }
        this->_3DObject()->Get((int)E_Type::Kuroneko)->MoveZ(f_seed*0.05f);
    }

    // ズームイン
    if (this->m_key_zoom == false)
    {
        float f_seed = this->_Hit()->Distance(*this->_3DObject()->Get((int)E_Type::Neet), *this->_3DObject()->Get((int)E_Type::Kuroneko));

        for (int i = 0; i < 10; i++)
        {
            this->_3DObject()->Get((int)E_Type::Kuroneko)->LockOn(*this->_3DObject()->Get((int)E_Type::Neet), 0.6f);
        }
        this->_3DObject()->Get((int)E_Type::Kuroneko)->MoveZ(f_seed*0.05f);
    }

    this->Camera()->SetAt(*this->_3DObject()->Get((int)E_Type::Neet)->GetMatInfoPos());
    this->Camera()->SetEye(*this->_3DObject()->Get((int)E_Type::Kuroneko)->GetMatInfoPos());

}
