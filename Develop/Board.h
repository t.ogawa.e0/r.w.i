//==========================================================================
// ボード[Board.h]
// author: tatsuya ogawa
//==========================================================================

#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "Stage.h"
#include "FieldEditor.h"

//==========================================================================
//
// class  : CBoard
// Content: ボード
//
//==========================================================================
class CBoard : public CObject
{
private:
    // アニメーションキー
    enum class JapanList {
        ポン,
        ヨシノ,
        ショウク,
        シナノ,
        ハッカイ,
        ヒカフ,
        コクシ,
        ニト,
        end,
    };
    // アニメーションキー
    enum class USAList {
        ビームス,
        ジャック,
        トマティ,
        トルマ,
        ベレンタイン,
        ローゼス,
        ウォーカー,
        ホース,
        ヘネス,
        ハーパー,
        end,
    };
    // アニメーションキー
    enum class HawaiiList {
        ソルティ,
        ジニック,
        ミェール,
        ヌテイン,
        end,
    };
    // アニメーションキー
    enum class AustraliaList {
        タンカレー,
        ボンベイ,
        シロッコ,
        ヒル,
        ゴードン,
        ギルビー,
        フィーター,
        end,
    };
public:
    CBoard();
    ~CBoard();
    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;
private:
    void Wabisabi(void);
    void U_S_B(void);
    void Waneha(void);
    void Kankari(void);
private:
    void * m_camera;
    std::unordered_map<std::string, int>m_data;
    StageData_ID m_stage;
};

