//==========================================================================
// カーソル[CGame_Cursor.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CGame_Cursor
// Content: カーソル
//
//==========================================================================
class CGame_Cursor : public CObject
{
public:
    CGame_Cursor();
    ~CGame_Cursor();

    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;

    // 座標セット
    void SetPos(C3DObject * _this);
private:
    void * m_game_camera;
};

