#include "CharacterAnimation.h"
#include "Game_Start.h"
#include "Sinful_Image.h"
#include "Game_Sound.h"
#include "resource_list.h"


CCharacterAnimation::CCharacterAnimation() : CObject(ID::Polygon2D)
{
    this->SetType(Type::Game_CharacterAnimation);
    this->m_poly = nullptr;
    this->m_Sinful_Image = nullptr;
    this->m_start_script = nullptr;
    this->m_sound = nullptr;
    this->m_sound_lick = false;
}


CCharacterAnimation::~CCharacterAnimation()
{
}

//==========================================================================
// 初期化
bool CCharacterAnimation::Init(void)
{
    //==========================================================================
    // 座標の補正用テクスチャの読み取り
    //==========================================================================
    C2DPolygon * p_display = nullptr;
    this->Helper()->New_(p_display);

    if (p_display->Init(RESOURCE_Rirekisyo_display_DDS, true))
    {
        return true;
    }

    // マスター座標の記録
    this->m_start_pos = CVector4<float>((float)p_display->GetTexSize(0)->w, (float)p_display->GetTexSize(0)->h);

    p_display->Release();
    this->Helper()->Delete_(p_display);

    //==========================================================================
    // 座標の補正用テクスチャの読み取り 2
    //==========================================================================
    this->Helper()->New_(p_display);

    if (p_display->Init(RESOURCE_TransferAnimation_display_DDS, true))
    {
        return true;
    }

    // マスター座標の記録
    this->m_stop_pos = CVector4<float>((float)p_display->GetTexSize(0)->w*0.98f, (float)p_display->GetTexSize(0)->h);

    p_display->Release();
    this->Helper()->Delete_(p_display);

    //==========================================================================
    // 辞令テクスチャ
    //==========================================================================

    this->m_stop_pos.y = this->m_start_pos.y;

    if (this->_2DPolygon()->Init(RESOURCE_MainScreen_Tenkin_DDS, true))
    {
        return true;
    }

    if (this->_2DPolygon()->Init(RESOURCE_MainScreen_Kaiko_DDS, true))
    {
        return true;
    }

    this->m_start_script = this->GetObjects(ID::Polygon2D, Type::Game_Start_Object);
    this->m_Sinful_Image = this->GetObjects(ID::Polygon2D, Type::Game_Sinful_Image);

    return false;
}

//==========================================================================
// 解放
void CCharacterAnimation::Uninit(void)
{
    this->m_obj.Release();
    this->m_character.Release();
}

//==========================================================================
// 更新
void CCharacterAnimation::Update(void)
{
    if (this->m_start_script == nullptr)
    {
        this->m_start_script = this->GetObjects(ID::Polygon2D, Type::Game_Start_Object);
    }
    if (this->m_Sinful_Image == nullptr)
    {
        this->m_Sinful_Image = this->GetObjects(ID::Polygon2D, Type::Game_Sinful_Image);
    }
    if (this->m_sound == nullptr)
    {
        this->m_sound = this->GetObjects(ID::Default, Type::Game_Game_Sound);
    }
    
    if (this->ImGui()->MenuItem("辞令"))
    {
        this->CreateTransferred();
    }

    if (this->ImGui()->MenuItem("解雇通知"))
    {
        this->CreateFire();
    }

    auto * p_start = (CGame_Start*)this->m_start_script;

    if (p_start != nullptr)
    {
        if (p_start->Start())
        {
            this->Animation2();

            this->Animation();

            this->PaperRelease();
        }
    }

    this->_2DPolygon()->Update();
}

//==========================================================================
// 描画
void CCharacterAnimation::Draw(void)
{
    this->_2DPolygon()->Draw();
}

//==========================================================================
// 解雇データ生成
void CCharacterAnimation::CreateFire(EmployeeParam * _data)
{
    auto * p_obj = this->m_obj.Create();
    auto * p_tex = this->_2DPolygon()->GetTexSize(0);
    auto * p_sound = (CGame_Sound*)this->m_sound;

    //==========================================================================
    // 制御座標の設定
    //==========================================================================
    p_obj->m_pos.Init((int)E_Update::Fire);
    p_obj->m_pos.SetCentralCoordinatesMood(true);
    p_obj->m_start_pos = CVector4<float>(-(float)p_tex->w, this->GetWinSize().m_Height - (p_tex->h / 2.0f));
    p_obj->m_stop_pos = CVector4<float>((this->GetWinSize().m_Width / 2.0f), this->GetWinSize().m_Height - (p_tex->h / 2.0f));
    p_obj->m_end_pos = CVector4<float>((this->GetWinSize().m_Width / 2.0f), this->GetWinSize().m_Height + (p_tex->h / 2.0f));
    p_obj->m_pos.SetPos(p_obj->m_start_pos);
    p_obj->m_type = E_Update::Fire;

    if (p_sound != nullptr)
    {
        p_sound->Sinful_SE();
    }

    if (_data != nullptr)
    {
        for (int i = 0; i < this->m_character.Size(); i++)
        {
            if (this->m_character.Get(i)->m_data == _data)
            {
                p_obj->m_char_data = this->m_character.Get(i);
                break;
            }
        }
    }

    this->_2DPolygon()->ObjectInput(p_obj->m_pos);
}

//==========================================================================
// 転勤データ生成
void CCharacterAnimation::CreateTransferred(EmployeeParam * _data)
{
    auto * p_obj = this->m_obj.Create();
    auto * p_tex = this->_2DPolygon()->GetTexSize(0);
    auto * p_sound = (CGame_Sound*)this->m_sound;

    //==========================================================================
    // 制御座標の設定
    //==========================================================================
    p_obj->m_pos.Init((int)E_Update::Transferred);
    p_obj->m_pos.SetCentralCoordinatesMood(true);
    p_obj->m_start_pos = CVector4<float>((this->GetWinSize().m_Width / 2.0f), this->GetWinSize().m_Height + (p_tex->h / 2.0f));
    p_obj->m_stop_pos = CVector4<float>((this->GetWinSize().m_Width / 2.0f), this->GetWinSize().m_Height - (p_tex->h / 2.0f));
    p_obj->m_end_pos = CVector4<float>(-(float)p_tex->w, this->GetWinSize().m_Height - (p_tex->h / 2.0f));
    p_obj->m_pos.SetPos(p_obj->m_start_pos);
    p_obj->m_type = E_Update::Transferred;

    if (p_sound != nullptr)
    {
        p_sound->Sinful_SE();
    }

    if (_data != nullptr)
    {
        for (int i = 0; i < this->m_character.Size(); i++)
        {
            if (this->m_character.Get(i)->m_data == _data)
            {
                p_obj->m_char_data = this->m_character.Get(i);
                break;
            }
        }
    }

    this->_2DPolygon()->ObjectInput(p_obj->m_pos);
}

//==========================================================================
// データ選択
void CCharacterAnimation::SerectData(EmployeeParam * _data)
{
    if (_data == nullptr)
    {
        return;
    }
    auto * p_char = this->m_character.Create();
    auto * p_sound = (CGame_Sound*)this->m_sound;

    if (p_sound != nullptr)
    {
        p_sound->tab_se();
    }

    //==========================================================================
    // 宣言
    //==========================================================================
    CVector4<float>vpos4;
    CVector2<float>vpos2;
    C2DObject * pos = nullptr;

    //==========================================================================
    // シシャデータ表示用背景(共通)
    //==========================================================================
    p_char->m_data = _data;

    // 2Dオブジェクト生成
    pos = p_char->m_2dobj.Create();

    // 初期化
    pos->Init((int)E_CharTexList::MainScreen_Syokuin_Rirekisyo);

    // 表示座標の設定
    vpos4 = CVector4<float>(
        (float)this->GetWinSize().m_Width - (float)this->m_poly->GetTexSize((int)E_CharTexList::MainScreen_Syokuin_Rirekisyo)->w,
        (float)this->m_poly->GetTexSize((int)E_CharTexList::Begin)->h
        );

    // 表示座標の登録
    pos->SetPos(vpos4);

    //==========================================================================
    // ここまでが背景(共通)
    //==========================================================================

    //==========================================================================
    // 名前オブジェクト ここから
    //==========================================================================

    // 2Dオブジェクト生成
    pos = p_char->m_2dobj.Create();

    // 初期化
    pos->Init((int)E_CharTexList::MainScreen_Syokuni_Name, 1, 50, 5);

    /*
    表示座標の設定
    ※定数禁止！
    必ずウィンドウサイズから座標を生成しよう！

    this->GetWinSize(); // ウィンドウサイズ取得関数
    */

    vpos4 = CVector4<float>(this->GetWinSize().m_Width / 2.0f * 1.52f, this->GetWinSize().m_Height / 2.0f * 0.615f);

    // 表示座標の登録
    pos->SetPos(vpos4);

    //==========================================================================
    // 名前オブジェクト ここまで
    //==========================================================================

    //==========================================================================
    // アイコンオブジェクト ここから
    //==========================================================================

    // 2Dオブジェクト生成
    pos = p_char->m_2dobj.Create();

    // 初期化
    pos->Init((int)E_CharTexList::MainScreen_syoumeisyashin, 1, 19, 5);

    /*
    表示座標の設定
    ※定数禁止！
    必ずウィンドウサイズから座標を生成しよう！

    this->GetWinSize(); // ウィンドウサイズ取得関数
    */
    vpos4 = CVector4<float>(((this->GetWinSize().m_Width / 2.0f) * 1.52f) + 2, ((this->GetWinSize().m_Height / 2.0f) * 0.7f) + 8);

    // 表示座標の登録
    pos->SetPos(vpos4);

    //==========================================================================
    // アイコンオブジェクト ここまで
    //==========================================================================

    //==========================================================================
    // 年齢オブジェクト ここから
    //==========================================================================

    // 2Dオブジェクト生成
    pos = p_char->m_2dobj.Create();

    // 初期化
    pos->Init((int)E_CharTexList::MainScreen_Syokuin_Rank_skill, 1, 13, 2);

    /*
    表示座標の設定
    ※定数禁止！
    必ずウィンドウサイズから座標を生成しよう！

    this->GetWinSize(); // ウィンドウサイズ取得関数
    */
    vpos4 = CVector4<float>(this->GetWinSize().m_Width / 2.0f * 1.80f, this->GetWinSize().m_Height / 2.0f * 0.76f);

    // 表示座標の登録
    pos->SetPos(vpos4);

    //==========================================================================
    // 年齢オブジェクト ここまで
    //==========================================================================

    //==========================================================================
    // 国籍オブジェクト ここから
    //==========================================================================

    // 2Dオブジェクト生成
    pos = p_char->m_2dobj.Create();

    // 初期化
    pos->Init((int)E_CharTexList::MainScreen_Syokuin_Rank_mental, 1, 8, 1);

    /*
    表示座標の設定
    ※定数禁止！
    必ずウィンドウサイズから座標を生成しよう！

    this->GetWinSize(); // ウィンドウサイズ取得関数
    */
    vpos4 = CVector4<float>(this->GetWinSize().m_Width / 2.0f * 1.80f, this->GetWinSize().m_Height / 2.0f * 0.865f);

    // 表示座標の登録
    pos->SetPos(vpos4);

    //==========================================================================
    // 国籍オブジェクト ここまで
    //==========================================================================

    //==========================================================================
    // 性格オブジェクト ここから
    //==========================================================================

    // 2Dオブジェクト生成
    pos = p_char->m_2dobj.Create();

    // 初期化
    pos->Init((int)E_CharTexList::MainScreen_Syokuin_Rank_cost, 1, 9, 1);

    /*
    表示座標の設定
    ※定数禁止！
    必ずウィンドウサイズから座標を生成しよう！

    this->GetWinSize(); // ウィンドウサイズ取得関数
    */
    vpos4 = CVector4<float>(this->GetWinSize().m_Width / 2.0f * 1.80f, this->GetWinSize().m_Height / 2.0f * 0.965f);

    // 表示座標の登録
    pos->SetPos(vpos4);

    //==========================================================================
    // 性格オブジェクト ここまで
    //==========================================================================

    //==========================================================================
    // 特徴オブジェクト ここから
    //==========================================================================

    // 2Dオブジェクト生成
    pos = p_char->m_2dobj.Create();

    // 初期化
    pos->Init((int)E_CharTexList::MainScreen_Syokuin_Tokuchou, 1, 8, 1);

    /*
    表示座標の設定
    ※定数禁止！
    必ずウィンドウサイズから座標を生成しよう！

    this->GetWinSize(); // ウィンドウサイズ取得関数
    */
    vpos4 = CVector4<float>(this->GetWinSize().m_Width / 2.0f * 1.71f, this->GetWinSize().m_Height / 2.0f * 1.055f);

    // 表示座標の登録
    pos->SetPos(vpos4);

    //==========================================================================
    // 特徴オブジェクト ここまで
    //==========================================================================

    //==========================================================================
    // タイプオブジェクト ここから
    //==========================================================================

    // 2Dオブジェクト生成
    pos = p_char->m_2dobj.Create();

    // 初期化
    pos->Init((int)E_CharTexList::MainScreen_Syokuin_Type, 1, 4, 4);

    /*
    表示座標の設定
    ※定数禁止！
    必ずウィンドウサイズから座標を生成しよう！

    this->GetWinSize(); // ウィンドウサイズ取得関数
    */
    vpos4 = CVector4<float>(this->GetWinSize().m_Width / 2.0f * 1.49f, this->GetWinSize().m_Height / 2.0f * 0.95f);

    // 表示座標の登録
    pos->SetPos(vpos4);

    //==========================================================================
    // タイプオブジェクト ここまで
    //==========================================================================

    //==========================================================================
    // パートナーがいるブジェクト ここから
    //==========================================================================

    // 2Dオブジェクト生成
    pos = p_char->m_2dobj.Create();

    // 初期化
    pos->Init((int)E_CharTexList::MainScreen_Syokuin_Type_Partner_true, 1, 4, 4);

    /*
    表示座標の設定
    ※定数禁止！
    必ずウィンドウサイズから座標を生成しよう！

    this->GetWinSize(); // ウィンドウサイズ取得関数
    */
    vpos4 = CVector4<float>(this->GetWinSize().m_Width / 2.0f * 1.72f, this->GetWinSize().m_Height / 2.0f * 0.57f);

    // 表示座標の登録
    pos->SetPos(vpos4);

    //==========================================================================
    // パートナーがいるブジェクト ここまで
    //==========================================================================

    //==========================================================================
    // パートナーがいないブジェクト ここから
    //==========================================================================

    // 2Dオブジェクト生成
    pos = p_char->m_2dobj.Create();

    // 初期化
    pos->Init((int)E_CharTexList::MainScreen_Syokuin_Type_Partner_false, 1, 4, 4);

    /*
    表示座標の設定
    ※定数禁止！
    必ずウィンドウサイズから座標を生成しよう！

    this->GetWinSize(); // ウィンドウサイズ取得関数
    */

    vpos4 = CVector4<float>(this->GetWinSize().m_Width / 2.0f * 1.85f, this->GetWinSize().m_Height / 2.0f * 0.57f);

    // 表示座標の登録
    pos->SetPos(vpos4);

    //==========================================================================
    // パートナーがいないオブジェクト ここまで
    //==========================================================================

    auto p_obj_pos = p_char->m_2dobj.Get((int)E_CharTexList::MainScreen_Syokuin_Rirekisyo)->GetPos();
    vpos4 = this->m_start_pos;
    vpos4.x = p_obj_pos->x;

    // 親オブジェクトと終点オブジェクトとの誤差の検出
    float f_powor = this->_Hit()->Distance(*p_obj_pos, vpos4);

    // 全objectに誤差の加算
    for (int i = 0; i < p_char->m_2dobj.Size(); i++)
    {
        auto * p_obj = p_char->m_2dobj.Get(i);
        p_obj->SetYPlus(f_powor);
    }

    // オブジェクトの登録
    for (int i = 0; i < p_char->m_2dobj.Size(); i++)
    {
        this->m_poly->ObjectInput(p_char->m_2dobj.Get(i));
    }
}

//==========================================================================
// 描画オブジェクトのセット
void CCharacterAnimation::SetDrawObject(C2DPolygon * Input)
{
    this->m_poly = Input;
}

//==========================================================================
// 辞令アニメーション
void CCharacterAnimation::Animation(void)
{
    // 全オブジェクトの処理
    for (int i = 0; i < this->m_obj.Size(); i++)
    {
        auto * p_obj = this->m_obj.Get(i);
        if (p_obj->m_type == E_Update::Transferred)
        {
            // 停止時間が0ではないとき
            if (!p_obj->m_stop_time.Countdown())
            {
                // 座標の修正
                p_obj->m_pos.SetX(p_obj->m_stop_pos.x);

                // 速度の割り出し
                float f_speed = this->_Hit()->Distance(*p_obj->m_pos.GetPos(), p_obj->m_stop_pos)*0.2f;

                // y軸に移動
                p_obj->m_pos.SetYPlus(-f_speed);
            }
            else
            {
                auto * p_sound = (CGame_Sound*)this->m_sound;
                if (this->m_sound_lick == false)
                {
                    if (p_sound != nullptr)
                    {
                        p_sound->tab_se();
                    }
                    this->m_sound_lick = true;
                }
                // 座標の修正
                p_obj->m_pos.SetY(p_obj->m_end_pos.y);

                // 速度の割り出し
                float f_speed = this->_Hit()->Distance(*p_obj->m_pos.GetPos(), p_obj->m_end_pos)*0.2f;

                // y軸に移動
                p_obj->m_pos.SetXPlus(-f_speed);

                if (p_obj->m_char_data != nullptr)
                {
                    p_obj->m_char_data->m_update_lock = true;

                    for (int i_2 = 0; i_2 < p_obj->m_char_data->m_2dobj.Size(); i_2++)
                    {
                        p_obj->m_char_data->m_2dobj.Get(i_2)->SetXPlus(-f_speed);
                    }
                }

                // カウンタ
                p_obj->m_end_time.Countdown();
            }
        }
        if (p_obj->m_type == E_Update::Fire)
        {
            // 停止時間が0ではないとき
            if (!p_obj->m_stop_time.Countdown())
            {
                // 座標の修正
                p_obj->m_pos.SetY(p_obj->m_stop_pos.y);

                // 速度の割り出し
                float f_speed = this->_Hit()->Distance(*p_obj->m_pos.GetPos(), p_obj->m_stop_pos)*0.2f;

                // x軸に移動
                p_obj->m_pos.SetXPlus(f_speed);
            }
            else
            {
                auto * p_sound = (CGame_Sound*)this->m_sound;
                if (this->m_sound_lick == false)
                {
                    if (p_sound != nullptr)
                    {
                        p_sound->tab_se();
                    }
                    this->m_sound_lick = true;
                }
                // 座標の修正
                p_obj->m_pos.SetX(p_obj->m_end_pos.x);

                // 速度の割り出し
                float f_speed = this->_Hit()->Distance(*p_obj->m_pos.GetPos(), p_obj->m_end_pos)*0.2f;

                // y軸に移動
                p_obj->m_pos.SetYPlus(f_speed);

                if (p_obj->m_char_data != nullptr)
                {
                    p_obj->m_char_data->m_update_lock = true;
                    for (int i_2 = 0; i_2 < p_obj->m_char_data->m_2dobj.Size(); i_2++)
                    {
                        p_obj->m_char_data->m_2dobj.Get(i_2)->SetYPlus(f_speed);
                    }
                }

                // カウンタ
                p_obj->m_end_time.Countdown();
            }
        }
    }
}

//==========================================================================
// アニメーションの固定化
void CCharacterAnimation::AnimationLock(character * InputChar)
{
    /* 描画オブジェクトに対して必要な情報を与える処理 */
    C2DObject * p_obj = nullptr; // 2Dオブジェクト

    /* 名前の固定化 */
    p_obj = InputChar->m_2dobj.Get((int)E_CharTexList::MainScreen_Syokuni_Name);
    p_obj->SetAnimationCount(InputChar->m_data->GetName()->id);

    /* アイコンの固定化 */
    p_obj = InputChar->m_2dobj.Get((int)E_CharTexList::MainScreen_syoumeisyashin);
    p_obj->SetAnimationCount(InputChar->m_data->GetIcoFrameID());

    /* スキルランクの固定化 */
    p_obj = InputChar->m_2dobj.Get((int)E_CharTexList::MainScreen_Syokuin_Rank_skill);
    p_obj->SetAnimationCount(InputChar->m_data->GetAge()->id);

    /* メンタルの固定化 */
    p_obj = InputChar->m_2dobj.Get((int)E_CharTexList::MainScreen_Syokuin_Rank_mental);
    p_obj->SetAnimationCount(InputChar->m_data->GetCountry()->id);

    /* コストの固定化 */
    p_obj = InputChar->m_2dobj.Get((int)E_CharTexList::MainScreen_Syokuin_Rank_cost);
    p_obj->SetAnimationCount(InputChar->m_data->GetPersonality()->id);

    /* 特徴の固定化 */
    p_obj = InputChar->m_2dobj.Get((int)E_CharTexList::MainScreen_Syokuin_Tokuchou);
    p_obj->SetAnimationCount(InputChar->m_data->GetChaID()->id);

    /* タイプの固定化 */
    p_obj = InputChar->m_2dobj.Get((int)E_CharTexList::MainScreen_Syokuin_Type);
    p_obj->SetAnimationCount(InputChar->m_data->GetTypeID()->id);

    /* 既婚か独身かの固定化 */
    // 既婚
    if (InputChar->m_data->GetPartner()->id == 1)
    {
        // frameの固定化
        InputChar->m_2dobj.Get((int)E_CharTexList::MainScreen_Syokuin_Type_Partner_false)->SetColor({ 255,255,255,0 });
        p_obj = InputChar->m_2dobj.Get((int)E_CharTexList::MainScreen_Syokuin_Type_Partner_true);
        p_obj->SetAnimationCount(3);
        p_obj->SetColor({ 255,255,255,255 });
    }
    // 未婚
    else if (InputChar->m_data->GetPartner()->id == 0)
    {
        // frameの固定化
        InputChar->m_2dobj.Get((int)E_CharTexList::MainScreen_Syokuin_Type_Partner_true)->SetColor({ 255,255,255,0 });
        p_obj = InputChar->m_2dobj.Get((int)E_CharTexList::MainScreen_Syokuin_Type_Partner_false);
        p_obj->SetAnimationCount(3);
        p_obj->SetColor({ 255,255,255,255 });
    }
}

//==========================================================================
// 社員アニメーション
void CCharacterAnimation::Animation2(void)
{
    // 転勤シャイン数(基本的に一人)
    for (int i = 0; i < this->m_character.Size(); i++)
    {
        auto* p_obj = this->m_character.Get(i);

        if (p_obj->m_update_lock == false)
        {
            auto * p_pos = p_obj->m_2dobj.Get((int)E_CharTexList::MainScreen_Syokuin_Rirekisyo)->GetPos();

            float f_speed = this->_Hit()->Distance(*p_pos, this->m_stop_pos)*0.1f;

            for (int i_2 = 0; i_2 < p_obj->m_2dobj.Size(); i_2++)
            {
                auto * p_char = p_obj->m_2dobj.Get(i_2);
                p_char->SetXPlus(-f_speed);
            }
        }
        this->AnimationLock(p_obj);
    }

    for (int i = 0; i < this->m_character.Size(); i++)
    {
        auto* p_obj = this->m_character.Get(i);

        if (p_obj->m_release_key)
        {
            for (int i_2 = 0; i_2 < p_obj->m_2dobj.Size(); i_2++)
            {
                this->m_poly->ObjectDelete(p_obj->m_2dobj.Get(i_2));
            }
            p_obj->m_2dobj.Release();
            this->m_character.PinpointRelease(p_obj);
            return;
        }
    }
}

//==========================================================================
// ペーパー破棄
void CCharacterAnimation::PaperRelease(void)
{
    auto * p_Sinful_Image = (CSinful_Image*)this->m_Sinful_Image;
    // オブジェクトの解除及び破棄
    for (int i = 0; i < this->m_obj.Size(); i++)
    {
        auto * p_obj = this->m_obj.Get(i);

        if (p_obj->m_end_time.GetTime() == 0 && p_obj->m_end_time.GetComma() == 0)
        {
            if (p_obj->m_char_data != nullptr)
            {
                p_obj->m_char_data->m_release_key = true;
            }
            if (p_Sinful_Image != nullptr)
            {
                if (p_obj->m_type == E_Update::Transferred)
                {
                    p_Sinful_Image->Create();
                }
            }
            this->_2DPolygon()->ObjectDelete(p_obj->m_pos);
            this->m_obj.PinpointRelease(p_obj);
            break;
        }
    }
}
