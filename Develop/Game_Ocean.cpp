//==========================================================================
//  海[Game_Ocean.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Game_Ocean.h"
#include "GameCamera.h"
#include "resource_list.h"

//==========================================================================
// 初期化
bool CGame_Ocean::Init(void)
{
    auto * p_obj = this->_3DObject()->Create();

    this->m_camera = this->GetObjects(ID::Camera, Type::Game_Camera);

    p_obj->Init(0);

    p_obj->SetMatrixType(C3DObject::EMatrixType::Vector1);

    this->_3DMesh()->ObjectInput(p_obj);
    return this->_3DMesh()->Init(RESOURCE_suimen_DDS, 200, 200);
}

//==========================================================================
// 解放
void CGame_Ocean::Uninit(void)
{
}

//==========================================================================
// 更新
void CGame_Ocean::Update(void)
{
    if (this->m_camera == nullptr)
    {
        this->m_camera = this->GetObjects(ID::Camera, Type::Game_Camera);
    }

    // オブジェクトが存在するとき
    if (this->m_camera != nullptr)
    {
        auto * p_camera = (CGameCamera*)this->m_camera;
        auto * _obj = p_camera->_3DObject()->Get((int)CGameCamera::E_Type::Kuroneko);

        this->_3DObject()->Get(0)->SetMatInfoPos(*_obj->GetMatInfoPos());
        this->_3DObject()->Get(0)->MoveY(-0.6f);
        this->_3DObject()->Get(0)->RotX(0.003f);
    }

    this->_3DMesh()->Update();
}

//==========================================================================
// 描画
void CGame_Ocean::Draw(void)
{
    this->_3DMesh()->Draw();
}
