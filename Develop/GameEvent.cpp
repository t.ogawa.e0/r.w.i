//==========================================================================
// ゲームイベント[GameEvent.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "GameEvent.h"
#include "Worldtime.h"
#include "BubbleCollapseEvent.h"
#include "BubbleEconomyEvent.h"
#include "NirinPicHoldingEvent.h"
#include "CommunismEvent.h"
#include "NewCompanyEvent.h"
#include "ShinkansenOpeningEvent.h"
#include "TheBirthOfPopularAnimalsEvent.h"
#include "CapitalCityChangeEvent.h"
#include "MinimumWageEvent.h"
#include "AnimeHolyGroundEvent.h"
#include "OilFieldsWereFoundEvent.h"

#include "LoadGameData.h"
#include "Game_Start.h"
#include "resource_link.hpp"

//==========================================================================
// 初期化
bool CGameEvent::Init(void)
{
	std::random_device rnd; // 非決定的な乱数生成器を生成
	std::mt19937 mt(rnd());  //  メルセンヌ・ツイスタの32ビット版、引数は初期シード値
    CProbabilityTable<float> table;
	float fNextMin = 0.0f;

    const char * TexList[] = {
        RESOURCE_MainScreen_Newspaper_DDS,
        RESOURCE_Ibent_03_CV_DDS,
        RESOURCE_Ibent_04_CV_DDS,
        RESOURCE_Ibent_01_CV_DDS,
        RESOURCE_Ibent_10_DDS,
        RESOURCE_Ibent_06_CV_DDS,
        RESOURCE_Ibent_09_CV_DDS,
        RESOURCE_Ibent_05_DDS,
        RESOURCE_Ibent_07_CV_DDS,
        RESOURCE_Ibent_08_CV_DDS,
    };

	// バブル崩壊
	fNextMin = table.Set(fNextMin, fNextMin, CEventManager::List::BubbleCollapse);
	this->m_Table.emplace_back(table);

	// バブル景気
	fNextMin = table.Set(1.0f, 5.7f, CEventManager::List::BubbleEconomy);
	this->m_Table.emplace_back(table);

	// ニリンピック開催決定
	fNextMin = table.Set(fNextMin, 5.0f, CEventManager::List::NirinPicHolding);
	this->m_Table.emplace_back(table);

	// 共産主義 
	fNextMin = table.Set(fNextMin, 0.3f, CEventManager::List::Communism);
	this->m_Table.emplace_back(table);

	// 新企業
	fNextMin = table.Set(fNextMin, 10.0f, CEventManager::List::NewCompany);
	this->m_Table.emplace_back(table);

	//// 新幹線開通 
	//fNextMin = table.Set(fNextMin, 1.0f, CEventManager::List::ShinkansenOpening);
	//this->m_Table.emplace_back(table);

	// 人気動物の誕生 
	fNextMin = table.Set(fNextMin, 23.0f, CEventManager::List::TheBirthOfPopularAnimals);
	this->m_Table.emplace_back(table);

	// 首都の場所が変わった
	fNextMin = table.Set(fNextMin, 3.0f, CEventManager::List::CapitalCityChange);
	this->m_Table.emplace_back(table);

	//// 最低賃金
	//fNextMin = table.Set(fNextMin, 10.0f, CEventManager::List::MinimumWage);
	//this->m_Table.emplace_back(table);

	// アニメの聖地！
	fNextMin = table.Set(fNextMin, 25.0f, CEventManager::List::AnimeHolyGround);
	this->m_Table.emplace_back(table);

	// 油田が見つかった！
	fNextMin = table.Set(fNextMin, 3.0f, CEventManager::List::OilFieldsWereFound);
	this->m_Table.emplace_back(table);

	this->m_EventSearch = std::uniform_real_distribution<float>(1.0f, fNextMin);
	this->m_rand = std::uniform_int_distribution<int>(1, 100);
	this->m_mt = mt;
	this->m_Evetable = CProbabilityTable<int>(1, 30); //30%

    for (int i = 0; i < this->Helper()->Sizeof_(TexList); i++)
    {
        if (this->_2DPolygon()->Init(TexList[i], true))
        {
            return true;
        }
    }

    switch (CLoadGameData::GetSerectData())
    {
    case StageData_ID::Wabisabi:
        if (this->Japan_Init()) { return true; }
        break;
    case StageData_ID::U_S_B:
        if (this->USA_Init()) { return true; }
        break;
    case StageData_ID::Waneha:
        if (this->Hawaii_Init()) { return true; }
        break;
    case StageData_ID::Kankari:
        if (this->Australia_Init()) { return true; }
        break;
    default:
        break;
    }

    // オブジェクトの先行呼び出し
    this->m_WorldTime = this->GetObjects(CObject::ID::Default, CObject::Type::Game_WorldTime);
    this->m_start_script = this->GetObjects(ID::Polygon2D, Type::Game_Start_Object);

    for (int i = 0; i < this->_2DPolygon()->GetNumTex(); i++)
    {
        this->_2DPolygon()->SetTexScale(i, 0.7f);
    }

	return false;
}

//==========================================================================
// 解放
void CGameEvent::Uninit(void)
{
	this->m_Table.clear();
	this->ReleaseTask();
    this->m_Data.Release();
}

//==========================================================================
// 更新
void CGameEvent::Update(void)
{
    if (this->m_WorldTime == nullptr)
    {
        this->m_WorldTime = this->GetObjects(CObject::ID::Default, CObject::Type::Game_WorldTime);
    }

    if (this->m_start_script == nullptr)
    {
        this->m_start_script = this->GetObjects(ID::Polygon2D, Type::Game_Start_Object);
    }

    auto * p_start = (CGame_Start*)this->m_start_script;

    if (p_start != nullptr&&this->m_WorldTime != nullptr)
    {
        if (p_start->Start())
        {
            auto*pTime = (CWorldTime*)this->m_WorldTime;

            // ワールド時間の更新が入ったときに処理が発生する。
            if (pTime->getturns().m_current != pTime->getturns().m_old&&pTime != nullptr)
            {
                this->m_Script.Update();

                // イベント背景表示終了処理
                for (int i = 0; i < this->m_EventPos.Size(); i++)
                {
                    CMobeEventObject * even = this->m_EventPos.Get(i);

                    // イベント背景表示終わり
                    if (even->Key2() == true)
                    {
                        even->End();
                    }

                    // イベントオブジェクト表示終了用(よろしくない)
                    for (auto itr = this->m_task.begin(); itr != this->m_task.end(); ++itr)
                    {
                        this->_2DPolygon()->ObjectDelete((*itr)->GetObjectData());
                        this->_2DPolygon()->ObjectDelete((*itr)->GetAreaName());
                    }
                }

                // イテレータで最初から最後まで検索
                for (auto itr = this->m_task.begin(); itr != this->m_task.end(); )
                {
                    // 各イベント更新処理
                    (*itr)->Update();

                    // 処理が終了
                    if ((*itr)->end())
                    {
                        this->_2DPolygon()->ObjectDelete((*itr)->GetObjectData());
                        this->_2DPolygon()->ObjectDelete((*itr)->GetAreaName());

                        // 終了したイベントの解放
                        (*itr)->Uninit();

                        // タスクの破棄
                        this->Helper()->Delete_((*itr));
                        (*itr) = nullptr;

                        // 仮のイテレータを渡す
                        itr = this->m_task.erase(itr);
                    }
                    else
                    {
                        ++itr;
                    }
                }

                // イベント生成
                this->create(this->m_rand(this->m_mt));
            }

            // イベント表示処理
            for (int i = 0; i < this->m_EventPos.Size(); i++)
            {
                this->m_EventPos.Get(i)->Update();
            }

            // IMGUI
            if (this->ImGui()->NewMenu("発生しているイベント一覧"))
            {
                this->text();
                this->ImGui()->EndMenu();
            }

            // イベント終了処理
            for (int i = 0; i < this->m_EventPos.Size(); i++)
            {
                CMobeEventObject * even = this->m_EventPos.Get(i);
                if (even->EndKey() == true)
                {
                    this->_2DPolygon()->ObjectDelete(even->Get());
                    this->m_EventPos.PinpointRelease(i);
                    break;
                }
            }

            // イベントUIの移動更新
            for (auto itr = this->m_task.begin(); itr != this->m_task.end(); ++itr)
            {
                (*itr)->UpdateUI();
            }

            this->EventForcedActivation();
        }
    }

    // UI更新
    this->_2DPolygon()->Update();
}

//==========================================================================
// 描画
void CGameEvent::Draw(void)
{
    this->_2DPolygon()->Draw();

    for (auto itr = this->m_task.begin(); itr != this->m_task.end(); ++itr)
    {
        (*itr)->Draw();
    }
}

//==========================================================================
// イベント生成
void CGameEvent::create(int Input)
{
	// イベントが発生した場合
	if (this->m_Evetable.Search(Input))
	{
		// 乱数の生成
		float frand = this->m_EventSearch(this->m_mt);

		// イテレータで最初から最後まで検索
		for (auto itr = this->m_Table.begin(); itr != this->m_Table.end(); ++itr)
		{
			// 生成された乱数にヒットするイベントの検索
			if ((*itr).Search(frand) == true)
			{
				// イベントID
				this->set((*itr).GetID());
				break;
			}
		}
	}
}

//==========================================================================
// イベントセット
void CGameEvent::set(CEventManager::List Input)
{
	// イベントIDに対応しているイベントの生成

    // バブル景気のイベントを検索し破棄
    for (auto itr = this->m_task.begin(); itr != this->m_task.end(); ++itr)
    {
        if ((*itr)->GetEventID() == CEventManager::List::BubbleEconomy)
        {
            this->m_task.erase(itr);
            // 強制バブル崩壊
            if (this->m_Script.Input(Input, 2147483647))
            {
                this->EventInitialize(CEventManager::List::BubbleCollapse);
            }
            break;
        }
    }

	switch (Input)
	{
	case CEventManager::List::BubbleEconomy:
	{
        if (this->m_Script.Input(Input, 2147483647))
        {
            this->EventInitialize(Input);
        }
    }
    break;
	case CEventManager::List::BubbleCollapse:
	break;
	case CEventManager::List::NirinPicHolding:
	{
        if (this->m_Script.Input(Input, 24))
        {
            this->EventInitialize(Input);
        }
	}
	break;
	case CEventManager::List::Communism:
	{
        if (this->m_Script.Input(Input, 2147483647))
        {
            this->EventInitialize(Input);
        }
	}
	break;
	case CEventManager::List::NewCompany:
	{
        if (this->m_Script.Input(Input, 6))
        {
            this->EventInitialize(Input);
        }
	}
	break;
    //case CEventManager::List::ShinkansenOpening:
    //{
    //       if (this->m_Script.Input(Input, 2147483647))
    //       {
    //           this->EventInitialize(Input);
    //       }
    //}
    //break;
    case CEventManager::List::TheBirthOfPopularAnimals:
	{
        if (this->m_Script.Input(Input, 3))
        {
            this->EventInitialize(Input);
        }
	}
	break;
	case CEventManager::List::CapitalCityChange:
	{
        if (this->m_Script.Input(Input, 12))
        {
            this->EventInitialize(Input);
        }
	}
    break;
    //case CEventManager::List::MinimumWage:
    //{
    //       if (this->m_Script.Input(Input, 3))
    //       {
    //           this->EventInitialize(Input);
    //       }
    //}
    //break;
	case CEventManager::List::AnimeHolyGround:
	{
        if (this->m_Script.Input(Input, 3))
        {
            this->EventInitialize(Input);
        }
	}
	break;
	case CEventManager::List::OilFieldsWereFound:
	{
        if (this->m_Script.Input(Input, 24))
        {
            this->EventInitialize(Input);
        }
	}
	break;
	default:
		break;
	}
}

//==========================================================================
// イベントタスクの解放
void CGameEvent::ReleaseTask(void)
{
    // イテレータで最初から最後まで検索
    for (auto itr = this->m_task.begin(); itr != this->m_task.end(); ++itr)
    {
        (*itr)->Uninit();
        // イテレータを破棄し、仮のイテレータを返す
        this->Helper()->Delete_((*itr));
    }
    this->m_task.clear();
}

//==========================================================================
// テキストセット
void CGameEvent::text(void)
{
	// イテレータで最初から最後まで検索
    int EventID = 0;
	for (auto itr = this->m_task.begin(); itr != this->m_task.end(); ++itr)
	{
		(*itr)->text(this->ImGui()->CreateText("%d : ", EventID));
        EventID++;
	}
}

//==========================================================================
// 移動開始
void CGameEvent::Move(void)
{
}

//==========================================================================
// インスタンスの生成
CMobeEventObject * CGameEvent::CreateInstance(void)
{
    CTexvec<int>*texsize = nullptr;
    CVector4<float>vpos1;
    CVector4<float>vpos2;

    // インスタンスの生成
    CMobeEventObject *pos = this->m_EventPos.Create();
    pos->Init(0); // イベント画像

    // テクスチャサイズ取得
    texsize = this->_2DPolygon()->GetTexSize(pos->ObjectID());

    // 止める座標
    vpos2 = CVector4<float>(0.0f, (float)this->GetWinSize().m_Height - texsize->h);

    // 始まり
    vpos1 = CVector4<float>(-(float)texsize->w, (float)this->GetWinSize().m_Height - texsize->h);

    // 座標のセット
    pos->SetPos(vpos1, vpos2);

    // オブジェクトにセット
    this->_2DPolygon()->ObjectInput(pos->Get());
    pos->Start();

    return pos;
}

//==========================================================================
// イベントのイニシャライザ
void CGameEvent::EventInitialize(CEventManager::List Input)
{
    auto * p_obj = this->CreateInstance();
    switch (Input)
    {
    case CEventManager::List::none:
        break;
    case CEventManager::List::BubbleEconomy:
    {
        CBubbleEconomy *peve;
        this->Helper()->New_(peve);
        peve->InputEventID(Input);
        peve->SetMasterPos(p_obj);
        peve->Init();
        this->m_task.emplace_back(peve);
        this->_2DPolygon()->ObjectInput(peve->GetObjectData());
    }
    break;
    case CEventManager::List::BubbleCollapse:
    {
        CBubbleCollapse *peve;
        this->Helper()->New_(peve);
        peve->InputEventID(Input);
        peve->SetMasterPos(p_obj);
        peve->Init();
        this->m_task.emplace_back(peve);
        this->_2DPolygon()->ObjectInput(peve->GetObjectData());
    }
    break;
    case CEventManager::List::NirinPicHolding:
    {
        CNirinPicHolding *peve;
        this->Helper()->New_(peve);
        peve->InputEventID(Input);
        peve->SetMasterPos(p_obj);
        peve->Init(this->m_mt);
        this->m_task.emplace_back(peve);
        this->_2DPolygon()->ObjectInput(peve->GetObjectData());

        for (int i = 0; i < this->m_Data.Size(); i++)
        {
            // ターゲットの探索
            auto * dat = this->m_Data.Get(i);

            // tag一致時
            if (dat->GetTag() == peve->GetTargetName())
            {
                peve->InputAreaName(dat->GetTexID());
            }
        }
        this->_2DPolygon()->ObjectInput(peve->GetAreaName());
    }
    break;
    case CEventManager::List::Communism:
    {
        CCommunism *peve;
        this->Helper()->New_(peve);
        peve->InputEventID(Input);
        peve->SetMasterPos(p_obj);
        peve->Init();
        this->m_task.emplace_back(peve);
        this->_2DPolygon()->ObjectInput(peve->GetObjectData());
    }
    break;
    case CEventManager::List::NewCompany:
    {
        CNewCompany *peve;
        this->Helper()->New_(peve);
        peve->InputEventID(Input);
        peve->SetMasterPos(p_obj);
        peve->Init(this->m_mt);
        this->m_task.emplace_back(peve);
        this->_2DPolygon()->ObjectInput(peve->GetObjectData());

        for (int i = 0; i < this->m_Data.Size(); i++)
        {
            // ターゲットの探索
            auto * dat = this->m_Data.Get(i);

            // tag一致時
            if (dat->GetTag() == peve->GetTargetName())
            {
                peve->InputAreaName(dat->GetTexID());
            }
        }
        this->_2DPolygon()->ObjectInput(peve->GetAreaName());
    }
    break;
    //case CEventManager::List::ShinkansenOpening:
    //{
    //    CShinkansenOpening *peve;
    //    this->Helper()->New_(peve);
    //    peve->InputEventID(Input);
    //    peve->SetMasterPos(p_obj);
    //    peve->Init();
    //    this->m_task.emplace_back(peve);
    //    this->_2DPolygon()->ObjectInput(peve->GetObjectData());
    //}
    //break;
    case CEventManager::List::TheBirthOfPopularAnimals:
    {
        CTheBirthOfPopularAnimals *peve;
        this->Helper()->New_(peve);
        peve->InputEventID(Input);
        peve->SetMasterPos(p_obj);
        peve->Init(this->m_mt);
        this->m_task.emplace_back(peve);
        this->_2DPolygon()->ObjectInput(peve->GetObjectData());

        for (int i = 0; i < this->m_Data.Size(); i++)
        {
            // ターゲットの探索
            auto * dat = this->m_Data.Get(i);

            // tag一致時
            if (dat->GetTag() == peve->GetTargetName())
            {
                peve->InputAreaName(dat->GetTexID());
            }
        }
        this->_2DPolygon()->ObjectInput(peve->GetAreaName());
    }
    break;
    case CEventManager::List::CapitalCityChange:
    {
        CCapitalCityChange *peve;
        this->Helper()->New_(peve);
        peve->InputEventID(Input);
        peve->SetMasterPos(p_obj);
        peve->Init(&this->m_mt);
        this->m_task.emplace_back(peve);
        this->_2DPolygon()->ObjectInput(peve->GetObjectData());
    }
    break;
    //case CEventManager::List::MinimumWage:
    //{
    //    CMinimumWage *peve;
    //    this->Helper()->New_(peve);
    //    peve->InputEventID(Input);
    //    peve->SetMasterPos(p_obj);
    //    peve->Init();
    //    this->m_task.emplace_back(peve);
    //    this->_2DPolygon()->ObjectInput(peve->GetObjectData());
    //}
    //break;
    case CEventManager::List::AnimeHolyGround:
    {
        CAnimeHolyGround *peve;
        this->Helper()->New_(peve);
        peve->InputEventID(Input);
        peve->SetMasterPos(p_obj);
        peve->Init(this->m_mt);
        this->m_task.emplace_back(peve);
        this->_2DPolygon()->ObjectInput(peve->GetObjectData());

        for (int i = 0; i < this->m_Data.Size(); i++)
        {
            // ターゲットの探索
            auto * dat = this->m_Data.Get(i);

            // tag一致時
            if (dat->GetTag() == peve->GetTargetName())
            {
                peve->InputAreaName(dat->GetTexID());
            }
        }
        this->_2DPolygon()->ObjectInput(peve->GetAreaName());
    }
    break;
    case CEventManager::List::OilFieldsWereFound:
    {
        COilFieldsWereFound *peve;
        this->Helper()->New_(peve);
        peve->InputEventID(Input);
        peve->SetMasterPos(p_obj);
        peve->Init(this->m_mt);
        this->m_task.emplace_back(peve);
        this->_2DPolygon()->ObjectInput(peve->GetObjectData());

        for (int i = 0; i < this->m_Data.Size(); i++)
        {
            // ターゲットの探索
            auto * dat = this->m_Data.Get(i);

            // tag一致時
            if (dat->GetTag() == peve->GetTargetName())
            {
                peve->InputAreaName(dat->GetTexID());
            }
        }
        this->_2DPolygon()->ObjectInput(peve->GetAreaName());
    }
    break;
    default:
        break;
    }
}

//==========================================================================
// イベント強制発動
void CGameEvent::EventForcedActivation(void)
{
    if (this->ImGui()->NewMenu("イベント強制発動リスト", true))
    {
        if (this->ImGui()->MenuItem("バブル景気"))
        {
            this->m_Script.Input(CEventManager::List::BubbleEconomy, 2147483647);
            this->EventInitialize(CEventManager::List::BubbleEconomy);
        }
        if (this->ImGui()->MenuItem("バブル崩壊"))
        {
            this->m_Script.Input(CEventManager::List::BubbleCollapse, 2147483647);
            this->EventInitialize(CEventManager::List::BubbleCollapse);
        }
        if (this->ImGui()->MenuItem("ニリンピック開催"))
        {
            this->m_Script.Input(CEventManager::List::NirinPicHolding, 24);
            this->EventInitialize(CEventManager::List::NirinPicHolding);
        }
        if (this->ImGui()->MenuItem("共産主義"))
        {
            this->m_Script.Input(CEventManager::List::Communism, 2147483647);
            this->EventInitialize(CEventManager::List::Communism);
        }
        if (this->ImGui()->MenuItem("新企業"))
        {
            this->m_Script.Input(CEventManager::List::NewCompany, 6);
            this->EventInitialize(CEventManager::List::NewCompany);
        }
        //if (this->ImGui()->MenuItem("新幹線開通 !!!"))
        //{
        //    this->m_Script.Input(CEventManager::List::ShinkansenOpening, 2147483647);
        //    this->EventInitialize(CEventManager::List::ShinkansenOpening);
        //}
        if (this->ImGui()->MenuItem("人気のある動物の誕生"))
        {
            this->m_Script.Input(CEventManager::List::TheBirthOfPopularAnimals, 3);
            this->EventInitialize(CEventManager::List::TheBirthOfPopularAnimals);
        }
        if (this->ImGui()->MenuItem("首都の場所が変わった"))
        {
            this->m_Script.Input(CEventManager::List::CapitalCityChange, 12);
            this->EventInitialize(CEventManager::List::CapitalCityChange);
        }
        //if (this->ImGui()->MenuItem("最低賃金"))
        //{
        //    this->m_Script.Input(CEventManager::List::MinimumWage, 3);
        //    this->EventInitialize(CEventManager::List::MinimumWage);
        //}
        if (this->ImGui()->MenuItem("アニメの聖地"))
        {
            this->m_Script.Input(CEventManager::List::AnimeHolyGround, 3);
            this->EventInitialize(CEventManager::List::AnimeHolyGround);
        }
        if (this->ImGui()->MenuItem("油田が見つかった"))
        {
            this->m_Script.Input(CEventManager::List::OilFieldsWereFound, 24);
            this->EventInitialize(CEventManager::List::OilFieldsWereFound);
        }
        this->ImGui()->EndMenu();
    }
}

// 日本
bool CGameEvent::Japan_Init(void)
{
    const char * TexList[] = {
        RESOURCE_Bon_DDS,
        RESOURCE_Hakkai_DDS,
        RESOURCE_Hikawa_DDS,
        RESOURCE_Kokushi_DDS,
        RESOURCE_Nito_DDS,
        RESOURCE_Shinano_DDS,
        RESOURCE_Syouku_DDS,
        RESOURCE_Yoshino_DDS,
    };

    for (int i = 0; i < this->Helper()->Sizeof_(TexList); i++)
    {
        if (this->_2DPolygon()->Init(TexList[i], true))
        {
            return true;
        }
    }
    //==========================================================================
    // ここからシシャテキスト表示判定用の設定
    //==========================================================================

    this->m_Data.Create()->Set("シナノ", (int)CEventManager::JapanList::シナノ);
    this->m_Data.Create()->Set("ヨシノ", (int)CEventManager::JapanList::ヨシノ);
    this->m_Data.Create()->Set("ハッカイ", (int)CEventManager::JapanList::ハッカイ);
    this->m_Data.Create()->Set("コクシ", (int)CEventManager::JapanList::コクシ);
    this->m_Data.Create()->Set("ヒカフ", (int)CEventManager::JapanList::ヒカフ);
    this->m_Data.Create()->Set("ニト", (int)CEventManager::JapanList::ニト);
    this->m_Data.Create()->Set("ショウク", (int)CEventManager::JapanList::ショウク);
    this->m_Data.Create()->Set("ポン", (int)CEventManager::JapanList::ポン);

    return false;
}

// USA
bool CGameEvent::USA_Init(void)
{
    const char * TexList[] = {
        RESOURCE_Barenta_DDS,
        RESOURCE_Bimusu_DDS,
        RESOURCE_Hapa_DDS,
        RESOURCE_Henesu_DDS,
        RESOURCE_Hosu_DDS,
        RESOURCE_Jakku_DDS,
        RESOURCE_Tomateli_DDS,
        RESOURCE_Toruma_DDS,
        RESOURCE_Uloka_DDS,
        RESOURCE_Wazesu_DDS,
    };

    for (int i = 0; i < this->Helper()->Sizeof_(TexList); i++)
    {
        if (this->_2DPolygon()->Init(TexList[i], true))
        {
            return true;
        }
    }

    //==========================================================================
    // ここからシシャテキスト表示判定用の設定
    //==========================================================================

    this->m_Data.Create()->Set("ビームス", (int)CEventManager::USAList::ビームス);
    this->m_Data.Create()->Set("ジャック", (int)CEventManager::USAList::ジャック);
    this->m_Data.Create()->Set("トマティ", (int)CEventManager::USAList::トマティ);
    this->m_Data.Create()->Set("トルマ", (int)CEventManager::USAList::トルマ);
    this->m_Data.Create()->Set("ベレンタイン", (int)CEventManager::USAList::ベレンタイン);
    this->m_Data.Create()->Set("ローゼス", (int)CEventManager::USAList::ローゼス);
    this->m_Data.Create()->Set("ウォーカー", (int)CEventManager::USAList::ウォーカー);
    this->m_Data.Create()->Set("ホース", (int)CEventManager::USAList::ホース);
    this->m_Data.Create()->Set("ヘネス", (int)CEventManager::USAList::ヘネス);
    this->m_Data.Create()->Set("ハーパー", (int)CEventManager::USAList::ハーパー);

    return false;
}

// ハワイ
bool CGameEvent::Hawaii_Init(void)
{
    const char * TexList[] = {
        RESOURCE_Jinikku_DDS,
        RESOURCE_Myuru_DDS,
        RESOURCE_Soruteli_DDS,
        RESOURCE_Sutein_DDS,
    };

    for (int i = 0; i < this->Helper()->Sizeof_(TexList); i++)
    {
        if (this->_2DPolygon()->Init(TexList[i], true))
        {
            return true;
        }
    }

    //==========================================================================
    // ここからシシャテキスト表示判定用の設定
    //==========================================================================
    this->m_Data.Create()->Set("ソルティ", (int)CEventManager::HawaiiList::ソルティ);
    this->m_Data.Create()->Set("ジニック", (int)CEventManager::HawaiiList::ジニック);
    this->m_Data.Create()->Set("ミェール", (int)CEventManager::HawaiiList::ミェール);
    this->m_Data.Create()->Set("ヌテイン", (int)CEventManager::HawaiiList::ヌテイン);
    return false;
}

// オーストラリア
bool CGameEvent::Australia_Init(void)
{
    const char * TexList[] = {
        RESOURCE_Bonbei_DDS,
        RESOURCE_Fita_DDS,
        RESOURCE_Girubi_DDS,
        RESOURCE_Godon_DDS,
        RESOURCE_Hiru_DDS,
        RESOURCE_Shirokko_DDS,
        RESOURCE_Tankare_DDS,
    };

    for (int i = 0; i < this->Helper()->Sizeof_(TexList); i++)
    {
        if (this->_2DPolygon()->Init(TexList[i], true))
        {
            return true;
        }
    }

    //==========================================================================
    // ここからシシャテキスト表示判定用の設定
    //==========================================================================
    this->m_Data.Create()->Set("タンカレー", (int)CEventManager::AustraliaList::タンカレー);
    this->m_Data.Create()->Set("ボンベイ", (int)CEventManager::AustraliaList::ボンベイ);
    this->m_Data.Create()->Set("シロッコ", (int)CEventManager::AustraliaList::シロッコ);
    this->m_Data.Create()->Set("ヒル", (int)CEventManager::AustraliaList::ヒル);
    this->m_Data.Create()->Set("ゴードン", (int)CEventManager::AustraliaList::ゴードン);
    this->m_Data.Create()->Set("ギルビー", (int)CEventManager::AustraliaList::ギルビー);
    this->m_Data.Create()->Set("フィーター", (int)CEventManager::AustraliaList::フィーター);

    return false;
}
