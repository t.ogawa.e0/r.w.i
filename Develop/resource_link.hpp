#pragma once

#include <codecvt> // c++ 文字コード変換
#include <cstdlib> // c++ 数値変換
#include <locale> // c++ 文字の判定
#include <system_error> // c++ OSエラー
#include <vector> // c++ 動的構造体
#include <list> // c++ 双方向list
#include <map> // c++ 平衡二分木 ソート機能付き
#include <unordered_map> // c++ 平衡二分木 ソート機能無し
#include <iomanip> // c++ 時間
#include <string> // c++ char
#include <random> // c++ rand
#include <fstream> // c++ file

#include "resource_list.h"

#define _RESOURCE_BEGIN namespace resource_link {
#define _RESOURCE_END }
#define _RESOURCE resource_link::
#define _RESOURCE_ ::resource_link::

_RESOURCE_BEGIN
namespace data {
    constexpr char * system_file = "resource/system";
    constexpr char * system_file_stage = "stage.bin";
    constexpr char * system_file_result = "result.bin";
    constexpr char * system_file_select = "select.bin";
    constexpr char * system_file_log = "log.bin";
}
_RESOURCE_END

_RESOURCE_BEGIN
namespace game_ {
    constexpr int n_cost = 50; // 雇用コスト
    constexpr float f_relay_point_buff = 3.0f;
}
_RESOURCE_END
