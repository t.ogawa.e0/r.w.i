//==========================================================================
// 社員データ管理マネージャー[EmployeeManager.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "EmployeeManager.h"

//==========================================================================
// 初期化
bool CEmployeeManager::Init(void)
{
    return false;
}

//==========================================================================
// 解放
void CEmployeeManager::Uninit(void)
{
    for (auto itr = this->m_data.begin(); itr != this->m_data.end(); ++itr)
    {
        this->Helper()->Delete_((*itr));
    }
    this->m_data.clear();
}

//==========================================================================
// 更新
void CEmployeeManager::Update(void)
{
}

//==========================================================================
// 描画
void CEmployeeManager::Draw(void)
{
}

//==========================================================================
// 生成
EmployeeParam * CEmployeeManager::Create(void)
{
    EmployeeParam * p_data;

    // メモリの確保
    this->Helper()->New_(p_data);

    // タイプの生成
    this->CreateType(p_data);

    // スキルの設定
    this->CreateAge(p_data);

    // メンタルの生成
    this->CreateCountry(p_data);

    // コストの生成
    this->CreatePersonality(p_data);

    // 特徴の生成
    this->CreateCha(p_data);

    // パートナー判定
    this->CreatePartner(p_data);

    // アイコンID
    this->CreateICO(p_data);

    this->m_data.push_back(p_data);

    return p_data;
}

// デバッグテキスト
void CEmployeeManager::SetDebugText(EmployeeParam *_this)
{
    this->SetText(_this);
}
