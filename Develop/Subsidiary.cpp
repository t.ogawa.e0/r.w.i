//==========================================================================
// 子会社の処理[Subsidiary.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Game.h"
#include "DataCheck.h"
#include "Subsidiary.h"
#include "worldtime.h"
#include "LoadGameData.h"
#include "Game_Start.h"

//==========================================================================
// 実体化
//==========================================================================
std::vector<CSubsidiaryManager*> CSubsidiary::m_play_sub;

//==========================================================================
// 移動
void CSubsidiaryManager::SetTransfer(CSubsidiaryParam * Input, int num, int relay_point)
{
    CTransfer *m_this = nullptr;
    m_this = new CTransfer(Input, num, relay_point);
    this->m_TransferData.emplace_back(m_this);
}

//==========================================================================
// 移動の更新
void CSubsidiaryManager::UpdateTransfer(void)
{
    // イテレータで最初から最後まで検索
    for (auto itr = this->m_TransferData.begin(); itr != this->m_TransferData.end();)
    {
        // 中継地点を減らす
        (*itr)->m_relay_point--;

        // 中継地点が0の時
        if ((*itr)->m_relay_point == 0)
        {
            // 移動してきた人数の加算
            (*itr)->m_this->m_employee = (*itr)->m_this->m_employee + (*itr)->m_num;
            (*itr)->m_this->m_employee_sub = (*itr)->m_this->m_employee_sub + (*itr)->m_num;

            // イテレータを破棄し、仮のイテレータを返す
            delete (*itr);
            itr = this->m_TransferData.erase(itr);
        }
        else
        {
            ++itr; // イテレータの更新
        }
    }
}

//==========================================================================
// 移動の解放
void CSubsidiaryManager::ReleaseTransfer(void)
{
    // イテレータで最初から最後まで検索
    for (auto itr = this->m_TransferData.begin(); itr != this->m_TransferData.end(); ++itr)
    {
        // 解放処理
        delete (*itr);
        (*itr) = nullptr;
    }

    // リストのデータを破棄
    this->m_TransferData.clear();
}

//==========================================================================
// 自社の総社員数のゲッター
int CSubsidiaryManager::GetMaxNemployee(void)
{
    int nNemployee = 0; // 社員数記録

    // 社数回し社員をカウント
    // イテレータで最初から最後まで検索
    for (int i = 0; i < this->m_subparam.Size(); i++)
    {
        CSubsidiaryParam * par = this->m_subparam.Get(i);
        nNemployee += par->m_employee;
    }

    // イテレータで最初から最後まで検索
    for (auto itr = this->m_TransferData.begin(); itr != this->m_TransferData.end(); ++itr)
    {
        // 移動している人たちが行方不明にならないための移動人数の加算処理
        nNemployee += (*itr)->m_num;
    }

    return nNemployee;
}

//==========================================================================
// 利益
int CSubsidiaryManager::GetAsset(void)
{
    // 利益の格納
    int nAsset = 0;

    // 社数回し利益の計算
    // イテレータで最初から最後まで検索
    for (int i = 0; i < this->m_subparam.Size(); i++)
    {
        CSubsidiaryParam * par = this->m_subparam.Get(i);
        nAsset += par->m_profit;
    }

    return nAsset;
}

//==========================================================================
// テキスト
void CSubsidiaryManager::Text(void)
{
    this->m_imgui.NewWindow(this->m_playname.c_str(), true);

    // 社数分デバッグ用ウィンドウに書き込む
    // イテレータで最初から最後まで検索
    for (int i = 0; i < this->m_subparam.Size(); i++)
    {
        CSubsidiaryParam * par = this->m_subparam.Get(i);
        if (this->m_imgui.NewTreeNode(par->m_strName.c_str(), true))
        {
            this->m_imgui.Text("社員数 : %d/人", par->m_employee_sub);
            this->m_imgui.Text("シェア率 : %.2f/％", par->m_stock_price);
            this->m_imgui.Text("売上 : %d/ゴールド", par->m_income);
            this->m_imgui.Text("利益 : %d/ゴールド", par->m_profit);
            this->m_imgui.EndTreeNode();
        }
    }
    this->m_imgui.EndWindow();
}

//==========================================================================
// 子会社生成
// pRegion 地域
// pRegionProfit 賃金
// pRegionEmployee 社員数
// pcapital_city 首都判定
// num データ数
// tag_ キャラタグ
void CSubsidiaryManager::Create(CFieldData * FieldData, int num, CharList tag_)
{
    // 初期化
    for (int i = 0; i < num; i++)
    {
        this->Create(FieldData[i], tag_);
    }
}

//==========================================================================
// 子会社生成
// pRegion 地域
// pRegionProfit 賃金
// pRegionEmployee 社員数
// pcapital_city 首都判定
// tag_ キャラタグ
void CSubsidiaryManager::Create(CFieldData & FieldData, CharList tag_)
{
    CSubsidiaryParam * pSub = nullptr;

    this->m_tag = tag_;

    // 子会社データの確保
    pSub = this->m_subparam.Create();

    *pSub = CSubsidiaryParam(FieldData);

    pSub->Settag(FieldData.m_ObjName);

    pSub->Update(pSub);
}

//==========================================================================
// 初期化
bool CSubsidiary::Init(void)
{
    CFieldData * FieldData = nullptr;

    // 読み込んだデータのサイズの取得
    this->m_numsub = CLoadGameData::Size();

    // インスタンス生成
    this->Helper()->New_(FieldData, this->m_numsub);

    // データ複製
    for (int i = 0; i < this->m_numsub; i++)
    {
        FieldData[i] = CLoadGameData::GetData(i);
    }

    // プレイヤーのデータを全て登録
    CSubsidiaryManager *player = nullptr;
    this->Helper()->New_(player);
    player->Create(FieldData, this->m_numsub, CharList::Player);
    player->SetTag("プレイヤー");
    this->m_play_sub.emplace_back(player);

    // エネミーのデータを全て登録
    CSubsidiaryManager *enemy = nullptr;
    this->Helper()->New_(enemy);
    enemy->Create(FieldData, this->m_numsub, CharList::Enemy);
    enemy->SetTag("エネミー");
    this->m_play_sub.emplace_back(enemy);

    // インスタンスの破棄
    this->Helper()->Delete_(FieldData);

    // オブジェクトの先行呼び出し
    this->m_WorldTime = (CWorldTime*)this->GetObjects(CObject::ID::Default, CObject::Type::Game_WorldTime);
    this->m_start_script = this->GetObjects(ID::Polygon2D, Type::Game_Start_Object);

    return false;
}

//==========================================================================
// 解放
void CSubsidiary::Uninit(void)
{
    // イテレータで最初から最後まで検索
    for (auto itr = this->m_play_sub.begin(); itr != this->m_play_sub.end(); ++itr)
    {
        (*itr)->Release();
        this->Helper()->Delete_((*itr));
    }
    this->m_play_sub.clear();
}

//==========================================================================
// 更新
void CSubsidiary::Update(void)
{
    if (this->m_WorldTime == nullptr)
    {
        this->m_WorldTime = (CWorldTime*)this->GetObjects(CObject::ID::Default, CObject::Type::Game_WorldTime);
    }

    if (this->m_start_script == nullptr)
    {
        this->m_start_script = this->GetObjects(ID::Polygon2D, Type::Game_Start_Object);
    }

    auto * p_start = (CGame_Start*)this->m_start_script;

    // オブジェクトが存在するとき
    if (p_start != nullptr&&this->m_WorldTime != nullptr)
    {
        if (p_start->Start())
        {
            auto*pTime = (CWorldTime*)this->m_WorldTime;

            // ワールド時間の更新が入ったときに処理が発生する。
            if (pTime->getturns().m_current != pTime->getturns().m_old&&pTime != nullptr)
            {
                // 子会社の更新
                for (int i = 0; i < this->m_numsub; i++)
                {
                    CSubsidiaryParam *player = this->m_play_sub[(int)CharList::Player]->GetSub(i);
                    CSubsidiaryParam *enemy = this->m_play_sub[(int)CharList::Enemy]->GetSub(i);

                    // 子会社の処理
                    player->Update(enemy);
                    enemy->Update(player);
                }
            }

            // イテレータで最初から最後まで検索
            for (auto itr = this->m_play_sub.begin(); itr != this->m_play_sub.end(); ++itr)
            {
                // ワールド時間の更新が入ったときに処理が発生する。
                if (pTime->getturns().m_current != pTime->getturns().m_old)
                {
                    (*itr)->UpdateTransfer();
                }
                (*itr)->Text();
            }
        }
    }
}

//==========================================================================
// 描画
void CSubsidiary::Draw(void)
{
}
