//==========================================================================
// スカイドーム[Game_Skydome.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CGame_Skydome
// Content: スカイドーム
//
//=========================================================================
class CGame_Skydome : public CObject
{
public:
    CGame_Skydome() :CObject(ID::Sphere) {
        this->m_camera = nullptr;
        ZeroMemory(&caps, sizeof(D3DCAPS9));    //初期化
        colorr = 255;
    }
    ~CGame_Skydome() {}
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
private:
    void * m_camera; // カメラへのアクセスルート
    D3DCAPS9 caps;
    CColor<int>colorr;
};
