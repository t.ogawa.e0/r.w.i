//==========================================================================
// ミュージック[Music.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _Music_H_
#define _Music_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "SceneChange.h"

//==========================================================================
//
// class  : CMusic
// Content: ミュージック
//
//==========================================================================
class CMusic : public CBaseScene
{
public:
    CMusic();
    ~CMusic();
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
private:
    CXAudio2 m_music; // 音楽
    CObjectVectorManager<std::string> m_stdSoundnameList; // サウンド名リスト
    CImGui_Dx9 m_imgui; // ImGui
};

#endif // !_Music_H_
