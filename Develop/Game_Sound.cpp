//==========================================================================
// ゲームのサウンド[Game_Sound.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Game_Sound.h"
#include "Worldtime.h"
#include "resource_list.h"

CGame_Sound::CGame_Sound()
{
    this->m_stage = StageData_ID::begin;
    this->m_WorldTime = nullptr;
    this->m_bgm_key = false;
    this->SetType(Type::Game_Game_Sound);
}

CGame_Sound::CGame_Sound(StageData_ID serect)
{
    this->m_stage = serect;
    this->m_WorldTime = nullptr;
    this->m_bgm_key = false;
    this->SetType(Type::Game_Game_Sound);

}


CGame_Sound::~CGame_Sound()
{
}

//==========================================================================
// 初期化
bool CGame_Sound::Init(void)
{
    switch (this->m_stage)
    {
    case CStage::EID::Wabisabi:
        this->XAudio()->Init(CXAudio2::SoundLabel(RESOURCE_GameCon_JPnew_wav, -1, 1.0f));
        break;
    case CStage::EID::U_S_B:
        this->XAudio()->Init(CXAudio2::SoundLabel(RESOURCE_GameCon_America_wav, -1, 1.0f));
        break;
    case CStage::EID::Waneha:
        this->XAudio()->Init(CXAudio2::SoundLabel(RESOURCE_GameCon_Hawaii_wav, -1, 1.0f));
        break;
    case CStage::EID::Kankari:
        this->XAudio()->Init(CXAudio2::SoundLabel(RESOURCE_GameCon_Raria_wav, -1, 1.0f));
        break;
    default:
        break;
    }
    this->XAudio()->Init(CXAudio2::SoundLabel(RESOURCE_R_引き出し_wav, 0, 1.0f));
    this->XAudio()->Init(CXAudio2::SoundLabel(RESOURCE_ターン_wav, 0, 1.0f));
    this->XAudio()->Init(CXAudio2::SoundLabel(RESOURCE_R_ページ_wav, 0, 1.0f));
    this->XAudio()->Init(CXAudio2::SoundLabel(RESOURCE_ストライキ_wav, 0, 1.0f));

    this->m_WorldTime = this->GetObjects(CObject::ID::Default, CObject::Type::Game_WorldTime);
    return false;
}

//==========================================================================
// 解放
void CGame_Sound::Uninit(void)
{

}

//==========================================================================
// 更新
void CGame_Sound::Update(void)
{
    if (this->m_WorldTime == nullptr)
    {
        this->m_WorldTime = this->GetObjects(CObject::ID::Default, CObject::Type::Game_WorldTime);
    }

    // オブジェクトが存在するとき
    if (this->m_WorldTime != nullptr)
    {
        this->bgm();
        this->turn_se();
    }
}

//==========================================================================
// 描画
void CGame_Sound::Draw(void)
{
}

//==========================================================================
// ステージBGM(自動化)
void CGame_Sound::bgm(void)
{
    auto*pTime = (CWorldTime*)this->m_WorldTime;
    if (pTime->getturns().m_current != pTime->getturns().m_old&&this->m_bgm_key == false)
    {
        this->XAudio()->Play((int)EList::Stage_BGM);
        this->m_bgm_key = true;
    }
}

//==========================================================================
// ターンSE
void CGame_Sound::turn_se(void)
{
    auto*pTime = (CWorldTime*)this->m_WorldTime;
    if (pTime->getturns().m_current != pTime->getturns().m_old)
    {
        this->XAudio()->Play((int)EList::Turn_SE);
    }
}

//==========================================================================
// 移動SE
void CGame_Sound::tab_se(void)
{
    this->XAudio()->Play((int)EList::tab_change_SE);
}

void CGame_Sound::Sinful_SE(void)
{
    this->XAudio()->Play((int)EList::Sinful_SE);
}

void CGame_Sound::voice(void)
{
    this->XAudio()->Play((int)EList::Storiki_voice);
}
