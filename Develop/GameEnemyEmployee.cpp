//==========================================================================
// エネミーの社員データ管理[GameEnemyEmployee.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "GameEnemyEmployee.h"
#include "LoadGameData.h"
#include "GameCamera.h"
#include "Enemy.h"
#include "LoadGameData.h"
#include "Worldtime.h"
#include "Game_Start.h"

//==========================================================================
// 初期化
bool CGameEnemyEmployee::Init(void)
{
    this->FastCreate();
    this->m_enemy = this->GetObjects(ID::Default, Type::Game_Enemy);
    this->m_WorldTime = this->GetObjects(CObject::ID::Default, CObject::Type::Game_WorldTime);
    this->m_start_script = this->GetObjects(ID::Polygon2D, Type::Game_Start_Object);

    return false;
}

//==========================================================================
// 解放
void CGameEnemyEmployee::Uninit(void)
{
}

//==========================================================================
// 更新
void CGameEnemyEmployee::Update(void)
{
    if (this->m_enemy == nullptr)
    {
        this->m_enemy = this->GetObjects(ID::Default, Type::Game_Enemy);
    }

    if (this->m_WorldTime == nullptr)
    {
        this->m_WorldTime = this->GetObjects(CObject::ID::Default, CObject::Type::Game_WorldTime);
    }

    if (this->m_start_script == nullptr)
    {
        this->m_start_script = this->GetObjects(ID::Polygon2D, Type::Game_Start_Object);
    }

    // オブジェクトが存在するとき
    if (this->m_start_script != nullptr)
    {
        auto * p_start = (CGame_Start*)this->m_start_script;

        if (p_start->Start())
        {
            this->Move();

            // ストライキ
            this->Strike();
        }
    }
}

//==========================================================================
// 描画
void CGameEnemyEmployee::Draw(void)
{
}

//==========================================================================
// 雇用
// _select シシャID
void CGameEnemyEmployee::Employment(int _select)
{
    _select;
    auto * p_employee_data = (CEmployeeManager*)this->m_EmployeeManager;
    auto * _enemy = (CEnemy*)this->m_enemy;
    auto * p_enemy = _enemy->GetCubsidiaryData();

    // オブジェクトが存在するとき
    if (p_employee_data != nullptr&&_enemy != nullptr)
    {
        // 本社探索
        for (int i = 0; i < p_enemy->GetNumSub(); i++)
        {
            if (p_enemy->GetSub(i)->m_head_office_enemy == true)
            {
                auto itr = this->m_data.find(p_enemy->GetSub(i)->m_id);
                // 探索に成功したとき
                if (itr != this->m_data.end())
                {
                    itr->second.emplace_back(p_employee_data->Create());
                }
            }
        }
    }

    //auto * p_employee_data = (CEmployeeManager*)this->m_EmployeeManager;

    //// データの補正
    //auto itr = this->m_data.find(_select);

    //// 探索に成功したとき
    //if (itr != this->m_data.end())
    //{
    //    itr->second.emplace_back(p_employee_data->Create());
    //}
}

//==========================================================================
// 解雇
// 解雇人数 NumEmployment
void CGameEnemyEmployee::Fire(int _select)
{
    // データの補正
    auto itr = this->m_data.find(_select);

    // 探索に成功したとき
    if (itr != this->m_data.end())
    {
        for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); ++itr2)
        {
            if ((*itr2) == this->m_LookEmployeeParam)
            {
                itr->second.erase(itr2);
                this->m_LookEmployeeParam = nullptr;
                break;
            }
        }
    }
}

//==========================================================================
// 転勤
void CGameEnemyEmployee::Transferred(int _select, int _next)
{
    this->m_id = _select;
    if (this->m_LookEmployeeParam != nullptr&&this->m_id != _next)
    {
        // データの補正
        auto itr = this->m_data.find(this->m_id);

        // 探索に成功したとき
        if (itr != this->m_data.end())
        {
            for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); ++itr2)
            {
                if ((*itr2) == this->m_LookEmployeeParam)
                {
                    itr->second.erase(itr2);
                    break;
                }
            }
            // 移動経路の登録
            this->m_task.emplace_back(this->RootSearch(_next));
            this->m_LookEmployeeParam = nullptr;
        }
    }
}

//==========================================================================
// ストライキ
void CGameEnemyEmployee::Strike(void)
{
    auto * _enemy = (CEnemy*)this->m_enemy;

    // オブジェクトが存在するとき
    if (_enemy != nullptr)
    {
        if (_enemy->GetStrikeKey() == true)
        {
            for (auto itr = this->m_data.begin(); itr != this->m_data.end(); ++itr)
            {
                if (itr->second.size() != 0)
                {
                    int n_count = 0;
                    std::unordered_map<int, int> id_b;
                    std::uniform_int_distribution<int> strike_rand = std::uniform_int_distribution<int>(0, itr->second.size() - 1);

                    // リタイアテーブル生成
                    for (int i = 0; i < (int)((itr->second.size() / 2) + 0.5f); i++)
                    {
                        int n_rand = strike_rand(this->m_mt);
                        id_b[n_rand] = n_rand;
                    }

                    // ストライキ対象の検索
                    for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); )
                    {
                        // ストライキ対象検出
                        auto itr3 = id_b.find(n_count);
                        if (itr3 != id_b.end())
                        {
                            itr2 = itr->second.erase(itr2);
                        }
                        else
                        {
                            ++itr2;
                        }
                        n_count++;
                    }
                    id_b.clear();
                }
            }
        }
    }
}

//==========================================================================
// 社員を選択
// _select シシャID
// ランダムで選択されます
void CGameEnemyEmployee::to_see(int _select)
{
    this->m_LookEmployeeParam = nullptr;
    // データの補正
    auto itr = this->m_data.find(_select);

    // 探索に成功したとき
    if (itr != this->m_data.end())
    {
        // 範囲の一様乱数
        std::uniform_int_distribution<int> std_rand = std::uniform_int_distribution<int>(0, itr->second.size() - 1);
        int n_serect = std_rand(this->m_mt);

        int serect = 0;
        for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); ++itr2)
        {
            if (serect == n_serect)
            {
                this->m_LookEmployeeParam = (*itr2);
                break;
            }
            serect++;
        }
    }
}

//==========================================================================
// 初期セットアップ
void CGameEnemyEmployee::FastCreate(void)
{
    this->m_EmployeeManager = this->GetObjects(ID::Default, Type::Game_EmployeeManager);
    auto * p_employee_data = (CEmployeeManager*)this->m_EmployeeManager;

    // オブジェクトが存在するとき
    if (p_employee_data != nullptr)
    {
        // 読み込まれたデータより生成
        for (int i = 0; i < CLoadGameData::Size(); i++)
        {
            auto * p_data = &CLoadGameData::GetData(i);

            // 各シシャ毎の社員数だけ回す
            for (int s = 0; s < p_data->m_param.m_employee; s++)
            {
                this->m_data[p_data->m_param.m_id].emplace_back(p_employee_data->Create());
            }
        }
    }
}

//==========================================================================
// 最短経路探索
CCEmployeeTask CGameEnemyEmployee::RootSearch(int _select)
{
    auto * _enemy = (CEnemy*)this->m_enemy;
    std::vector<int>std_root; // ルート
    std::unordered_map<int, std::vector<void*>>root_pt_case; // ルートポインタ
    std::vector<void*>root_pt; // ルート
    auto * p_enemy = _enemy->GetCubsidiaryData();

    // 中継ポインタ探索 next
    int root = 0; // 初期ルート
    for (int search = this->m_id;;)
    {
        // ルートアドレスの登録
        root_pt.emplace_back(p_enemy->GetSub(search));

        root++; // ルートカウンタ

                // ID一致時
        if (_select == search)
        {
            root--;
            break;
        }

        // 一致せずに終了IDが出た場合
        if (search == -1)
        {
            root = 999999;
            break;
        }

        // IDの探索
        search = p_enemy->GetSub(search)->m_next;
    }

    // ルート情報のセット
    std_root.emplace_back(root);
    root_pt_case[root] = root_pt;
    root_pt.clear();

    // 中継ポインタ探索 
    // 中継ポインタ探索 prev
    root = 0;
    for (int search = this->m_id;;)
    {
        // ルートアドレスの登録
        root_pt.emplace_back(p_enemy->GetSub(search));

        root++; // ルートカウンタ

                // ID一致時
        if (_select == search)
        {
            root--;
            break;
        }

        // 一致せずに終了IDが出た場合
        if (search == -1)
        {
            root = 999999;
            break;
        }

        // IDの探索
        search = p_enemy->GetSub(search)->m_prev;
    }

    // ルート情報のセット
    std_root.emplace_back(root);
    root_pt_case[root] = root_pt;
    root_pt.clear();

    // 最短経路検出
    int turn = min(std_root[0], std_root[1]);

    this->m_dataIDManager++;

    // タスクデータの生成
    return CCEmployeeTask(this->m_LookEmployeeParam, root_pt_case[turn], this->m_dataIDManager, this->m_id, _select, turn);
}

//==========================================================================
// データの移動
void CGameEnemyEmployee::Move(void)
{
    auto*pTime = (CWorldTime*)this->m_WorldTime;

    // オブジェクトが存在するとき
    if (pTime != nullptr)
    {
        if (pTime->getturns().m_current != pTime->getturns().m_old)
        {
            for (auto itr = this->m_task.begin(); itr != this->m_task.end(); )
            {
                itr->m_now++;

                // 移動ターンと一致した際に移動終了
                if (itr->m_turn < itr->m_now)
                {
                    // データの補正
                    auto itr2 = this->m_data.find(itr->m_id_next);

                    // 探索に成功したとき
                    if (itr2 != this->m_data.end())
                    {
                        itr2->second.emplace_back(itr->m_EmployeeParam);
                    }

                    // 仮のイテレータ
                    itr = this->m_task.erase(itr);
                }
                else
                {
                    ++itr;
                }
            }
        }
    }
}
