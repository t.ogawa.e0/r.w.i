//==========================================================================
// ゲームイベント:バブル景気[BubbleEconomyEvent.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _BubbleEconomyEvent_H_
#define _BubbleEconomyEvent_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "GameEvent.h"

//==========================================================================
//
// class  : CBubbleEconomy
// Content: バブル景気
//
//==========================================================================
class CBubbleEconomy : public CEventManager
{
public:
    CBubbleEconomy()
    {
        this->m_Name = "バブル景気";
    }
    ~CBubbleEconomy()
    {
        this->m_target.clear();
        this->m_Name.clear();
    }

    // 初期化
    void Init(void);

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;

    // UI更新
    void UpdateUI(void)override;

    // テキスト
    void text(std::string strEventName)override;
private:
};

#endif // !_BubbleEconomyEvent_H_
