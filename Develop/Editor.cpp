//==========================================================================
// エディタ[Editor.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Editor.h"
#include "resource_list.h"

//==========================================================================
// 実体化
//==========================================================================

CEditor::CEditor()
{
    this->m_Createworld = nullptr;
    this->m_CreateSubsidiary = nullptr;
    this->m_CreateWorldObj = nullptr;
    this->m_RelayPoint = nullptr;
    this->m_OldCSV = "";

    CLight light; // ライト
    light.Init({ 0, -1, 1 });

    this->m_CSV.CSVSetData(RESOURCE_Country_csv);

    // オブジェクトの登録
    CObject::NewObject(this->m_Createworld);
    CObject::NewObject(this->m_CreateWorldObj);
    CObject::NewObject(this->m_CreateSubsidiary);
    CObject::NewObject(this->m_RelayPoint);
}

CEditor::~CEditor()
{
    this->m_OldCSV.clear();
}

//==========================================================================
// 初期化
bool CEditor::Init(void)
{
    return CObject::InitAll();
}

//==========================================================================
// 解放
void CEditor::Uninit(void)
{
    CObject::ReleaseAll();
    this->m_CSV.CSVReleaseData();
}

//==========================================================================
// 更新
void CEditor::Update(void)
{
    this->m_ImGui.NewWindow("メニュー", true, ImGuiWindowFlags_::ImGuiWindowFlags_MenuBar);

    CObject::UpdateAll();

    // メニュー
    this->FileOperation();
    this->Menu();
    this->m_ImGui.EndWindow();
}

//==========================================================================
// 描画
void CEditor::Draw(void)
{
    CObject::DrawAll();
}

//==========================================================================
// ファイル操作
void CEditor::FileOperation(void)
{
    // メニューバーの設定
    if (this->m_ImGui.NewMenuBar())
    {
        // menuの追加
        if (this->m_ImGui.NewMenu("ファイル"))
        {
            // menuの追加
            if (this->m_ImGui.NewMenu("保存"))
            {
                int ncount = 0; // id
                for (auto itr = this->m_CSV.m_input.begin(); itr != this->m_CSV.m_input.end(); ++itr)
                {
                    // ボタンの生成
                    if (this->m_ImGui.MenuItem((*itr).c_str()))
                    {
                        // 読み込んだデータと保存先が一致した時のみ処理
                        if (this->m_OldCSV == (*itr))
                        {
                            this->m_CreateSubsidiary->Save((StageData_ID)ncount);
                        }
                    }
                    ncount++;
                }
                this->m_ImGui.EndMenu();
            }
            if (this->m_ImGui.NewMenu("読み込み")) 
            {
                int ncount = 0; // id
                for (auto itr = this->m_CSV.m_input.begin(); itr != this->m_CSV.m_input.end(); ++itr)
                {
                    // ボタン
                    if (this->m_ImGui.MenuItem((*itr).c_str()))
                    {
                        this->m_CreateSubsidiary->Load((StageData_ID)ncount);
                        this->m_CreateWorldObj->Load((StageData_ID)ncount);
                        this->m_OldCSV = (*itr); // 読み込みデータの記憶
                    }
                    ncount++;
                }
                this->m_ImGui.EndMenu();
            }

            this->m_ImGui.EndMenu();
        }
        this->m_ImGui.EndMenuBar();
    }
}

//==========================================================================
// メニュー
void CEditor::Menu(void)
{
    this->m_CreateSubsidiary->Activation(true);
    this->m_CreateWorldObj->Activation(false);

    //if (this->m_ImGui.Button("オブジェクトの設置"))
    //{
    //    if (this->m_CreateSubsidiary->GetActivation() == false)
    //    {
    //        this->m_CreateSubsidiary->Activation(true);
    //        this->m_CreateWorldObj->Activation(false);
    //    }
    //    else
    //    {
    //        this->m_CreateSubsidiary->Activation(false);
    //    }
    //}
    //if (this->m_ImGui.Button("国の設置"))
    //{
    //    if (this->m_CreateWorldObj->GetActivation() == false)
    //    {
    //        this->m_CreateSubsidiary->Activation(false);
    //        this->m_CreateWorldObj->Activation(true);
    //    }
    //    else
    //    {
    //        this->m_CreateWorldObj->Activation(false);
    //    }
    //}
}