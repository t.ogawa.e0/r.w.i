#include "TitleUI.h"
#include "InterruptToTitle.h"
#include"Screen.h"
#include "resource_list.h"

// 初期化
bool CTitleUI::Init(void)
{
	this->XInput()->Init(1);
	
	//データロードテスト用
	//フルロード
	//CTitleData::LoadData();
	//個別ロード
	//m_Term = CTitleData::LoadTerm();
	//m_EnemyLevel = CTitleData::LoadEnemyLevel();
	//m_StageID = CTitleData::LoadStageID();

	m_MapLevel = ConvertStageIDToMapLevel(m_StageID);

    const char * pUIList[] = {
        RESOURCE_Title_Re_DDS,
        RESOURCE_title_logo_02_DDS,
        RESOURCE_TitleScreen_Push_DDS,
        RESOURCE_TitleScreen_Data_DDS,
        RESOURCE_TitleScreen_End_DDS,
        RESOURCE_TitleScreen_Select_DDS,
        RESOURCE_TitleScreen_Decide_DDS,
        RESOURCE_TitleScreen_Level_DDS,
        RESOURCE_TitleScreen_map_DDS,
        RESOURCE_TitleScreen_Tutorial_DDS,
	};
	int datasize = (int)this->Helper()->Sizeof_(pUIList);
	CVector4<float>vpos;

	if (this->_2DPolygon()->Init(pUIList, datasize, true))
	{
		return true;
	}

	for (int i = 0; i < datasize; i++)
	{
		this->_2DPolygon()->ObjectInput(this->_2DObject()->Create());
		if (i == (int)TitleUIList::Title_Anim)
		{
			this->_2DObject()->Get(i)->Init(i, 2, 60, 6);
            this->_2DObject()->Get(i)->SetYPlus(-2.0f);
		}
		else if(i == (int)TitleUIList::Title_Level)
		{
			this->_2DObject()->Get(i)->Init(i, 1, 5, 1);
		}
		else if(i == (int)TitleUIList::Title_Map)
		{
			this->_2DObject()->Get(i)->Init(i, 1, 4, 2);
		}
		else
		{
			this->_2DObject()->Get(i)->Init(i);
		}
	}

	this->_2DPolygon()->AnimationCountInit(&this->m_animcount);

	//背景アニメーション設定
    this->_2DPolygon()->SetTexScale((int)TitleUIList::Title_Anim, 2.3f);
    //this->_2DPolygon()->SetTexSize((int)TitleUIList::Title_Anim, this->GetWinSize().m_Width * 6, this->GetWinSize().m_Height * 10);
    //   vpos.x = (float)this->GetWinSize().m_Width;
    //   vpos.y = (float)this->GetWinSize().m_Height;
    //this->_2DObject()->Get((int)TitleUIList::Title_Anim)->SetPos(vpos);

    //ロゴ設定
	vpos.x = (float)this->GetWinSize().m_Width / 2;
	vpos.y = (float)this->GetWinSize().m_Height / 2;
	this->_2DObject()->Get((int)TitleUIList::Title_Logo)->SetCentralCoordinatesMood(true);
	this->_2DObject()->Get((int)TitleUIList::Title_Logo)->SetPos(vpos);

	//push表示
	vpos.x = (float)this->GetWinSize().m_Width / 2;
	vpos.y = (float)this->GetWinSize().m_Height / 2 + this->_2DPolygon()->GetTexSize((int)TitleUIList::Title_Logo)->h * 0.4f;
	this->_2DObject()->Get((int)TitleUIList::Title_Push)->SetCentralCoordinatesMood(true);
	this->_2DObject()->Get((int)TitleUIList::Title_Push)->SetPos(vpos);

	//データウィンドウ表示
	vpos.x = (float)this->GetWinSize().m_Width + this->_2DPolygon()->GetTexSize((int)TitleUIList::Title_Data)->w / 2.0f;
	vpos.y = this->_2DPolygon()->GetTexSize((int)TitleUIList::Title_Data)->h / 2.0f + 50.0f;
	this->_2DObject()->Get((int)TitleUIList::Title_Data)->SetCentralCoordinatesMood(true);
	this->_2DObject()->Get((int)TitleUIList::Title_Data)->SetPos(vpos);

	//終了選択表示
	vpos.x = this->_2DObject()->Get((int)TitleUIList::Title_Data)->GetPos()->x;
	vpos.y = this->_2DPolygon()->GetTexSize((int)TitleUIList::Title_Data)->h + this->_2DPolygon()->GetTexSize((int)TitleUIList::Title_End)->h * 3.0f;
	this->_2DObject()->Get((int)TitleUIList::Title_End)->SetCentralCoordinatesMood(true);
	this->_2DObject()->Get((int)TitleUIList::Title_End)->SetPos(vpos);
	
	//セレクトカーソル表示
	vpos.x = (float)this->GetWinSize().m_Width + this->_2DPolygon()->GetTexSize((int)TitleUIList::Title_Data)->w * 0.66f;
	vpos.y = this->_2DPolygon()->GetTexSize((int)TitleUIList::Title_Data)->h * 0.20f + 50.0f;
	this->_2DObject()->Get((int)TitleUIList::Title_Select)->SetCentralCoordinatesMood(true);
	this->_2DObject()->Get((int)TitleUIList::Title_Select)->SetPos(vpos);

	//ハンコ表示
	vpos.x = (float)this->GetWinSize().m_Width - this->_2DPolygon()->GetTexSize((int)TitleUIList::Title_Decide)->w  * 0.73f;
	vpos.y = this->_2DPolygon()->GetTexSize((int)TitleUIList::Title_Data)->h - this->_2DPolygon()->GetTexSize((int)TitleUIList::Title_Decide)->h * 0.6f +50.0f;
	this->_2DObject()->Get((int)TitleUIList::Title_Decide)->SetCentralCoordinatesMood(true);
	this->_2DObject()->Get((int)TitleUIList::Title_Decide)->SetPos(vpos);
	
	//レベル設定表示
	this->_2DObject()->Get((int)TitleUIList::Title_Level)->SetAnimationCount(4 - (int)m_EnemyLevel);
	vpos.x = (float)this->GetWinSize().m_Width + this->_2DPolygon()->GetTexSize((int)TitleUIList::Title_Data)->w  * 0.66f;
	vpos.y = this->_2DPolygon()->GetTexSize((int)TitleUIList::Title_Data)->h * 0.36f + 50.0f;
	this->_2DObject()->Get((int)TitleUIList::Title_Level)->SetCentralCoordinatesMood(true);
	this->_2DObject()->Get((int)TitleUIList::Title_Level)->SetPos(vpos);

	//Map設定表示
	this->_2DObject()->Get((int)TitleUIList::Title_Map)->SetAnimationCount(m_MapLevel);
	vpos.x = (float)this->GetWinSize().m_Width + this->_2DPolygon()->GetTexSize((int)TitleUIList::Title_Data)->w  * 0.64f;
	vpos.y = this->_2DPolygon()->GetTexSize((int)TitleUIList::Title_Data)->h * 0.58f + 50.0f;
	this->_2DObject()->Get((int)TitleUIList::Title_Map)->SetCentralCoordinatesMood(true);
	this->_2DObject()->Get((int)TitleUIList::Title_Map)->SetPos(vpos);

	//任期設定表示
    this->_2DNumber()->Init(RESOURCE_MainScreen_Newspaper_Numbers_DDS, true);
    vpos.x = (float)this->GetWinSize().m_Width + this->_2DPolygon()->GetTexSize((int)TitleUIList::Title_Data)->w * 0.55f;
	vpos.y = this->_2DPolygon()->GetTexSize((int)TitleUIList::Title_Data)->h * 0.14f + 50.0f;
	this->_2DNumber()->Set(0, 2, false, false, { vpos.x,vpos.y });
	this->_2DNumber()->SetAnim(0, 1, 10, 10);

	//チュートリアル選択表示
	vpos.x = this->_2DObject()->Get((int)TitleUIList::Title_Data)->GetPos()->x;
	vpos.y = this->_2DPolygon()->GetTexSize((int)TitleUIList::Title_Data)->h + +this->_2DPolygon()->GetTexSize((int)TitleUIList::Title_End)->h * 1.7f;
	this->_2DObject()->Get((int)TitleUIList::Title_Tutorial)->SetCentralCoordinatesMood(true);
	this->_2DObject()->Get((int)TitleUIList::Title_Tutorial)->SetPos(vpos);

    this->m_teitle_Interrupt = this->GetObjects(CObject::ID::Polygon2D, CObject::Type::Title_InterruptToTitle);

	return false;
};

// 解放
void CTitleUI::Uninit(void)
{
};

// 更新
void CTitleUI::Update(void)
{
    this->m_teitle_Interrupt = this->GetObjects(CObject::ID::Polygon2D, CObject::Type::Title_InterruptToTitle);

    if (m_teitle_Interrupt ==nullptr)
    {
        this->m_teitle_Interrupt = this->GetObjects(CObject::ID::Polygon2D, CObject::Type::Title_InterruptToTitle);
    }

    auto * p_title_sclipt = (CInterruptToTitle*)m_teitle_Interrupt;

    if (!p_title_sclipt->TitleControl())
    {
        if (m_mode == modeList::modeTutorial)
        {
            //止める処理
            p_title_sclipt->StopTitle();
        }

        if (m_mode != modeList::modeEnd)
        {
            this->UpdateMode();
        }

        //アニメーション関係更新
        this->_2DPolygon()->GetPattanNum(*this->_2DObject()->Get((int)TitleUIList::Title_Anim), &this->m_animcount);
        this->m_animcount++;
        this->_2DObject()->Get((int)TitleUIList::Title_Anim)->SetAnimationCount(this->m_animcount);
        this->m_FlashCount++;

        //選択データ更新
        this->_2DNumber()->Update(0, m_Term);
        this->_2DObject()->Get((int)TitleUIList::Title_Level)->SetAnimationCount(4 - (int)m_EnemyLevel);
        this->_2DObject()->Get((int)TitleUIList::Title_Map)->SetAnimationCount(m_MapLevel);

        CColor<int> color(255, 255, 255, 255);
        //セレクトカーソル点滅
        if (m_mode == modeList::modeDecide || m_mode == modeList::modeExit || m_mode == modeList::modeTutorial || m_mode == modeList::modeEnd)
        {
            color.a = 0;
        }
        else
        {
            color.a = 200 + (int)(55.0f * cosf(m_FlashCount / 10.0f));
        }
        this->_2DObject()->Get((int)TitleUIList::Title_Select)->SetColor(color);

        //Push表示点滅
        if (m_mode != modeList::Default)
        {
            color.a = 0;
        }
        else
        {
            color.a = 200 + (int)(55.0f * cosf(m_FlashCount / 7.5f));
        }
        this->_2DObject()->Get((int)TitleUIList::Title_Push)->SetColor(color);

        //ハンコ点滅
        if (m_mode != modeList::modeDecide)
        {
            color.a = 0;
        }
        else
        {
            color.a = 200 + (int)(55.0f * cosf(m_FlashCount / 10.0f));
        }
        this->_2DObject()->Get((int)TitleUIList::Title_Decide)->SetColor(color);

        //チュートリアル点滅
        if (m_mode != modeList::modeTutorial)
        {
            color.a = 255;
        }
        else
        {
            color.a = 200 + (int)(55.0f * cosf(m_FlashCount / 7.5f));
        }
        this->_2DObject()->Get((int)TitleUIList::Title_Tutorial)->SetColor(color);

        //Exit点滅
        if (m_mode != modeList::modeExit)
        {
            color.a = 255;
        }
        else
        {
            color.a = 200 + (int)(55.0f * cosf(m_FlashCount / 7.5f));
        }
        this->_2DObject()->Get((int)TitleUIList::Title_End)->SetColor(color);

        CVector4<float>vpos;

        switch (m_mode)
        {
        case CTitleUI::modeList::Default:
            break;
        case CTitleUI::modeList::modeChange:
            this->_2DObject()->Get((int)TitleUIList::Title_Logo)->SetXPlus(-this->GetWinSize().m_Width / 200.0f);
            this->_2DObject()->Get((int)TitleUIList::Title_Data)->SetXPlus(-this->_2DPolygon()->GetTexSize((int)TitleUIList::Title_Data)->w / 31.0f);
            this->_2DObject()->Get((int)TitleUIList::Title_End)->SetXPlus(-this->_2DPolygon()->GetTexSize((int)TitleUIList::Title_Data)->w / 31.0f);
            this->_2DObject()->Get((int)TitleUIList::Title_Tutorial)->SetXPlus(-this->_2DPolygon()->GetTexSize((int)TitleUIList::Title_Data)->w / 31.0f);
            this->_2DObject()->Get((int)TitleUIList::Title_Select)->SetXPlus(-this->_2DPolygon()->GetTexSize((int)TitleUIList::Title_Data)->w / 31.0f);
            this->_2DObject()->Get((int)TitleUIList::Title_Level)->SetXPlus(-this->_2DPolygon()->GetTexSize((int)TitleUIList::Title_Data)->w / 31.0f);
            this->_2DObject()->Get((int)TitleUIList::Title_Map)->SetXPlus(-this->_2DPolygon()->GetTexSize((int)TitleUIList::Title_Map)->w / 31.0f);
            this->_2DNumber()->MoveXPos(0, -this->_2DPolygon()->GetTexSize((int)TitleUIList::Title_Data)->w / 31.0f);
            break;
        case CTitleUI::modeList::modeSelectYear:
            vpos.y = this->_2DPolygon()->GetTexSize((int)TitleUIList::Title_Data)->h * 0.20f + 50.0f;
            this->_2DObject()->Get((int)TitleUIList::Title_Select)->SetY(vpos.y);
            break;
        case CTitleUI::modeList::modeSelectEnemy:
            vpos.y = this->_2DPolygon()->GetTexSize((int)TitleUIList::Title_Data)->h * 0.36f + 50.0f;
            this->_2DObject()->Get((int)TitleUIList::Title_Select)->SetY(vpos.y);
            break;
        case CTitleUI::modeList::modeSelectMap:
            vpos.y = this->_2DPolygon()->GetTexSize((int)TitleUIList::Title_Data)->h * 0.57f + 50.0f;
            this->_2DObject()->Get((int)TitleUIList::Title_Select)->SetY(vpos.y);
            break;
        default:
            break;
        }

        this->XInput()->Update();

        this->_2DPolygon()->Update();
    }
};

// 描画
void CTitleUI::Draw(void)
{
    auto * p_title_sclipt = (CInterruptToTitle*)m_teitle_Interrupt;

    if (!p_title_sclipt->TitleControl())
    {
        this->_2DPolygon()->Draw();
        this->_2DNumber()->Draw();
    }
};

//モード変更・選択データ更新処理
void CTitleUI::UpdateMode(void)
{
	if(this->XInput()->Check(0))
	{ 		
		switch (m_mode)
		{
		case CTitleUI::modeList::Default:
			if (this->XInput()->Trigger(CXInput::EButton::A, 0)
				|| this->XInput()->Trigger(CXInput::EButton::B, 0)
				|| this->XInput()->Trigger(CXInput::EButton::X, 0)
				|| this->XInput()->Trigger(CXInput::EButton::Y, 0)
				|| this->XInput()->Trigger(CXInput::EButton::START, 0)
				|| this->XInput()->Trigger(CXInput::EButton::BACK, 0)
				|| this->XInput()->Trigger(CXInput::EButton::RIGHT_RB, 0)
				|| this->XInput()->RT(0)
				|| this->XInput()->Trigger(CXInput::EButton::LEFT_LB, 0)
				|| this->XInput()->LT(0)
				)
			{
				m_mode = CTitleUI::modeList::modeChange;
				this->m_pSoundObj->PlayDecide_se();
			}
			break;
		case CTitleUI::modeList::modeChange:
			m_count++;
			if (m_count > 30)
			{
				m_mode = CTitleUI::modeList::modeSelectYear;
			}
			break;
		case CTitleUI::modeList::modeSelectYear:
			if (this->XInput()->Trigger(CXInput::EButton::DPAD_DOWN, 0))
			{
				m_mode = CTitleUI::modeList::modeSelectEnemy;
				this->m_pSoundObj->PlayMove_se();
			}
			else if (this->XInput()->Trigger(CXInput::EButton::DPAD_RIGHT, 0))
			{
				m_Term += 1;
				if (m_Term > 99)
				{
					m_Term = 1;
				}
				this->m_pSoundObj->PlayMove_se();
			}
			else if (this->XInput()->Trigger(CXInput::EButton::DPAD_LEFT, 0))
			{
				m_Term -= 1;
				if (m_Term < 1)
				{
					m_Term = 99;
				}
				this->m_pSoundObj->PlayMove_se();
			}
			break;
		case CTitleUI::modeList::modeSelectEnemy:
			if (this->XInput()->Trigger(CXInput::EButton::DPAD_UP, 0))
			{
				m_mode = CTitleUI::modeList::modeSelectYear;
				this->m_pSoundObj->PlayMove_se();
			}
			else if (this->XInput()->Trigger(CXInput::EButton::DPAD_DOWN, 0))
			{
				m_mode = CTitleUI::modeList::modeSelectMap;
				this->m_pSoundObj->PlayMove_se();
			}
			else if (this->XInput()->Trigger(CXInput::EButton::DPAD_RIGHT, 0))
			{
				m_EnemyLevel = (CTitleData::Enemy_Level_ID)((int)m_EnemyLevel + 1);
				if (m_EnemyLevel == CTitleData::Enemy_Level_ID::end)
				{
					m_EnemyLevel = CTitleData::Enemy_Level_ID::TESHI;
				}
				this->m_pSoundObj->PlayMove_se();
			}
			else if (this->XInput()->Trigger(CXInput::EButton::DPAD_LEFT, 0))
			{
				m_EnemyLevel = (CTitleData::Enemy_Level_ID)((int)m_EnemyLevel - 1);
				if (m_EnemyLevel == CTitleData::Enemy_Level_ID::begin)
				{
					m_EnemyLevel = CTitleData::Enemy_Level_ID::OZAKIGUMI;
				}
				this->m_pSoundObj->PlayMove_se();
			}
			break;
		case CTitleUI::modeList::modeSelectMap:
			if (this->XInput()->Trigger(CXInput::EButton::DPAD_DOWN, 0))
			{
				m_mode = CTitleUI::modeList::modeDecide;
				this->m_pSoundObj->PlayMove_se();
			}
			else if (this->XInput()->Trigger(CXInput::EButton::DPAD_UP, 0))
			{
				m_mode = CTitleUI::modeList::modeSelectEnemy;
				this->m_pSoundObj->PlayMove_se();
			}
			else if (this->XInput()->Trigger(CXInput::EButton::DPAD_RIGHT, 0))
			{
				m_MapLevel += 1;
				if (m_MapLevel > 3)
				{
					m_MapLevel = 0;
				}
				this->m_pSoundObj->PlayMove_se();
			}
			else if (this->XInput()->Trigger(CXInput::EButton::DPAD_LEFT, 0))
			{
				m_MapLevel -= 1;
				if (m_MapLevel < 0)
				{
					m_MapLevel = 3;
				}
				this->m_pSoundObj->PlayMove_se();
			}
			break;
		case CTitleUI::modeList::modeDecide:
			if (this->XInput()->Trigger(CXInput::EButton::A, 0)
				|| this->XInput()->Trigger(CXInput::EButton::B, 0)
				|| this->XInput()->Trigger(CXInput::EButton::X, 0)
				|| this->XInput()->Trigger(CXInput::EButton::Y, 0)
				|| this->XInput()->Trigger(CXInput::EButton::START, 0)
			)
			{
				//ハンコ確定
				CColor<int> color(255, 255, 255, 255);
				this->_2DObject()->Get((int)TitleUIList::Title_Decide)->SetColor(color);

				this->m_pSoundObj->PlayDecide_se();

				m_mode = modeList::modeEnd;

				//データセーブ
				m_StageID = ConvertMapLevelToStageID(m_MapLevel);
				CTitleData::SavaData();

				//シーン変更処理挿入
				CScreen::screenchange(CScreen::scenelist_t::Game);
			}
			else if (this->XInput()->Trigger(CXInput::EButton::DPAD_UP, 0))
			{
				m_mode = CTitleUI::modeList::modeSelectMap;
				this->m_pSoundObj->PlayMove_se();
			}
			else if (this->XInput()->Trigger(CXInput::EButton::DPAD_DOWN, 0))
			{
				m_mode = CTitleUI::modeList::modeTutorial;
				this->m_pSoundObj->PlayMove_se();
			}
			break;
		case CTitleUI::modeList::modeTutorial:
			if (this->XInput()->Trigger(CXInput::EButton::DPAD_UP, 0))
			{
				m_mode = CTitleUI::modeList::modeDecide;
				this->m_pSoundObj->PlayMove_se();
			}
			else if (this->XInput()->Trigger(CXInput::EButton::DPAD_DOWN, 0))
			{
				m_mode = CTitleUI::modeList::modeExit;
				this->m_pSoundObj->PlayMove_se();
			}
			break;
		case CTitleUI::modeList::modeExit:
			if (this->XInput()->Trigger(CXInput::EButton::A, 0)
				|| this->XInput()->Trigger(CXInput::EButton::B, 0)
				|| this->XInput()->Trigger(CXInput::EButton::X, 0)
				|| this->XInput()->Trigger(CXInput::EButton::Y, 0)
				|| this->XInput()->Trigger(CXInput::EButton::START, 0)
			)
			{
				//終了処理
				DestroyWindow(CDeviceManager::GetDXDevice()->GetHwnd());
			}
			else if (this->XInput()->Trigger(CXInput::EButton::DPAD_UP, 0))
			{
				m_mode = CTitleUI::modeList::modeTutorial;
				this->m_pSoundObj->PlayMove_se();
			}
			break;
		default:
			break;

		}
	}
};