//==========================================================================
// ゲームマネージャー[GameManager.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _GameManager_H_
#define _GameManager_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "TitleData.h"

//==========================================================================
//
// class  : CGameManager
// Content: ゲームマネージャー
//
//==========================================================================
class CGameManager
{
private:
    class CTurns
    {
    public:
        CTurns() {
            this->m_current = 1;
            this->m_old = 0;
        }
        ~CTurns() {}
	public:
		int m_current; // 現在
		int m_old; // 古い
	};
public:
    CGameManager() {
        this->m_time.Init(0, 0);
        this->m_turntime = 10;
    }
	~CGameManager() {}

	// 更新処理
	void Update();

	// ターンのゲッター
	CTurns getturns(void) { return this->m_turns; }

	// 時間のゲッター
	CTimer * getime(void) { return &this->m_time; }

    // 1ターンにかかる時間
    int turntime(void) { return m_turntime; }
private:
	// ターン処理
	int Updateturn(void);
private:
	CTurns m_turns; // ターン
	CTimer m_time; // タイム
    int m_turntime; // 1ターンにかかる時間
public:
	typedef CTurns TURNS_t; // ターン
};

//==========================================================================
//
// class  : CGameManager
// Content: ゲームマネージャー
//
//==========================================================================
class CGame_Manager : public CObject
{
public:
    CGame_Manager() : CObject(ID::Default) {
        this->SetType(Type::Game_Manager);
        this->m_player = nullptr;
        this->m_enemy = nullptr;
        this->m_world_time = nullptr;
        this->m_end_script = nullptr;
        this->m_Limit_turn = CTitleData::LoadTerm() * 12; // 一年で固定
    }
    ~CGame_Manager() {}
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
private:
    void * m_player; // プレーヤーへのアクセスルート
    void * m_enemy; // エネミーへのアクセスルート
    void * m_world_time; // 世界時間へのアクセスルート
    void * m_end_script; // 終了操作
    int m_Limit_turn;
};

#endif // !_GameManager_H_
