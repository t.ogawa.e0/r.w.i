//==========================================================================
// ImGuiデバッグプレイヤー[ImGui_Player.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "ImGui_Player.h"
#include "ImGui_Enemy.h"
#include "worldtime.h"
#include "Subsidiary.h"

//==========================================================================
// 初期化
bool CImGui_Player::Init(void)
{
	this->SetCharacterType(CharList::Player);
	this->m_psub = CSubsidiary::Get(CharList::Player);

	// mainデータ初期化
    this->m_param = CPlayerParam(20 * this->m_psub->GetMaxNemployee(), this->m_psub->GetNumSub(), 20);

	// 総従業員に加算
	this->m_param.m_total_employee = this->m_psub->GetMaxNemployee();

    // オブジェクトの先行呼び出し
    this->m_WorldTime = (CWorldTime*)this->GetObjects(CObject::ID::Default, CObject::Type::Game_WorldTime);

	return false;
}

//==========================================================================
// 解放
void CImGui_Player::Uninit(void)
{
}

//==========================================================================
// 更新
void CImGui_Player::Update(void)
{
    if (this->m_WorldTime == nullptr)
    {
        this->m_WorldTime = (CWorldTime*)this->GetObjects(CObject::ID::Default, CObject::Type::Game_WorldTime);
    }

    auto*pTime = (CWorldTime*)this->m_WorldTime;

    if (pTime != nullptr)
    {
        // ワールド時間の更新が入ったときに処理が発生する。
        this->UpdateParam(pTime->getturns());

        // 不満度の処理
        this->Dissatisfied(pTime->getturns2()->GetTime());
    }
	// プレイヤーの行動
	this->action();

	// 社員一揆の処理
	this->Strike();

	// テキストセット
	this->Text("プレイヤーの情報");
}

//==========================================================================
// 描画
void CImGui_Player::Draw(void)
{
}

//==========================================================================
// プレイヤーの行動
void CImGui_Player::action(void)
{
    this->m_imgui.NewWindow("プレイヤー の操作", true);
	if (this->m_imgui.Button("雇用"))
	{
		this->boolreset();
		this->m_menu[0] = true;
	}
	if (this->m_imgui.Button("解雇"))
	{
		this->boolreset();
		this->m_menu[1] = true;
	}
	if (this->m_imgui.Button("転勤"))
	{
		this->boolreset();
		this->m_menu[2] = true;
	}
	if (this->m_imgui.Button("賃金"))
	{
		this->boolreset();
		this->m_menu[3] = true;
	}
	this->employment();
	this->fire();
	this->transferred();
	this->wage();
	this->m_imgui.EndWindow();
}

//==========================================================================
// 雇用
void CImGui_Player::employment(void)
{
	static int numemployment = 0;

    if (this->m_menu[0])
    {
		this->m_imgui.Text("エリア選択");
		for (int i = 0; i < this->m_psub->GetNumSub(); i++)
		{
			
			CSubsidiaryParam * psubsid = this->m_psub->GetSub(i);
			if (this->m_imgui.Button(psubsid->m_strName.c_str()))
			{
				numemployment = 0;
				this->m_regionkey = false;
				this->m_region[0] = this->m_region[1] = this->m_region[2] = this->m_region[3] = false;
				this->m_region[i] = true;
			}

			if (this->m_region[i])
			{
                this->m_imgui.Separator();
                this->m_imgui.Text("現在の社員数 : %d/人", psubsid->m_employee_sub);
                this->m_imgui.Separator();
                this->m_imgui.Text("採用人数を決めてください。");
                this->m_imgui.SliderInt("採用人数", &numemployment, 1, 100);

				if (this->m_imgui.Button("採用！"))
				{
					// ここで採用人数を加算しています。
					// 視覚情報に加算
					this->Employment(psubsid, numemployment);
				}
                this->m_imgui.Separator();
			}
		}
        this->m_imgui.Separator();
	}
}

//==========================================================================
// 解雇
void CImGui_Player::fire(void)
{
	static int numemployment = 0;
	if (this->m_menu[1])
	{
		this->m_imgui.Text("エリア選択");
		for (int i = 0; i < this->m_psub->GetNumSub(); i++)
		{
			CSubsidiaryParam * psubsid = this->m_psub->GetSub(i);
			if (this->m_imgui.Button(psubsid->m_strName.c_str()))
			{
				numemployment = 0;
				this->m_regionkey = false;
				this->m_region[0] = this->m_region[1] = this->m_region[2] = this->m_region[3] = false;
				this->m_region[i] = true;
			}

			if (this->m_region[i])
			{
                this->m_imgui.Separator();
                this->m_imgui.Text("現在の社員数 : %d/人", psubsid->m_employee_sub);
                this->m_imgui.Separator();
                this->m_imgui.Text("解雇人数を決めてください。");
                this->m_imgui.SliderInt("解雇人数", &numemployment, 0, psubsid->m_employee_sub);
				if (this->m_imgui.Button("fire!!"))
				{
					// 人数分削る
					this->Fire(psubsid, numemployment);
				}
                this->m_imgui.Separator();
			}
		}
        this->m_imgui.Separator();
	}
}

//==========================================================================
// 転勤
void CImGui_Player::transferred(void)
{
	CSubsidiaryParam * pthis_ = nullptr;
	CSubsidiaryParam * pthis2_ = nullptr;
	static int numemployment = 0;

	if (this->m_menu[2])
	{
		this->m_imgui.Text("エリア選択");
		for (int i = 0; i < this->m_psub->GetNumSub(); i++)
		{
			CSubsidiaryParam * psubsid = this->m_psub->GetSub(i);
			if (this->m_imgui.Button(psubsid->m_strName.c_str()))
			{
				this->m_regionkey = false;
				this->m_region[0] = this->m_region[1] = this->m_region[2] = this->m_region[3] = false;
				this->m_region[i] = true;
			}

			if (this->m_region[i])
			{
				pthis_ = psubsid; // アドレス渡し
                this->m_imgui.Separator();
                this->m_imgui.Text("現在の社員数 : %d/人", pthis_->m_employee_sub);
                this->m_imgui.Text("転勤先を選択してください。");

				for (int s = 0; s < this->m_psub->GetNumSub(); s++)
				{
					CSubsidiaryParam * psubsid2 = this->m_psub->GetSub(s);
					if (i != s)
					{
						if (this->m_imgui.Button(this->m_imgui.CreateText(" > %s", psubsid2->m_strName.c_str()).c_str()))
						{
							numemployment = 0;
							this->m_region2[0] = this->m_region2[1] = this->m_region2[2] = this->m_region2[3] = false;
							this->m_region2[s] = true;
						}

						if (this->m_region2[s])
						{
							pthis2_ = psubsid2; // アドレス渡し
                            this->m_imgui.SliderInt("転勤人数", &numemployment, 0, pthis_->m_employee_sub);
						}
					}
				}

				if (pthis_ != nullptr&&pthis2_ != nullptr)
				{
					if (this->m_imgui.Button(this->m_imgui.CreateText("%s から %s へ転勤", pthis_->m_strName.c_str(), pthis2_->m_strName.c_str()).c_str()))
					{
						// 転勤処理
                        this->Transferred(pthis_, pthis2_, this->m_psub, numemployment);
                        numemployment = 0;
					}
				}
                this->m_imgui.Separator();
			}
		}
        this->m_imgui.Separator();
	}
}

//==========================================================================
// 賃金
void CImGui_Player::wage(void)
{
	if (this->m_menu[3])
	{
		// ここで賃金の操作をしています。
        this->m_imgui.SliderInt("賃金の操作", &this->m_param.m_wage_sub, 0, 200);
        this->m_imgui.Separator();
	}
}

//==========================================================================
// ただのbool初期化
void CImGui_Player::boolreset(void)
{
	this->m_region[0] = this->m_region[1] = this->m_region[2] = this->m_region[3] = false;
	this->m_region2[0] = this->m_region2[1] = this->m_region2[2] = this->m_region2[3] = false;
	this->m_menu[0] = this->m_menu[1] = this->m_menu[2] = this->m_menu[3] = false;
}
