//==========================================================================
// ゲームデータの読み込み[LoadGameData.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _LoadGameData_H_
#define _LoadGameData_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "FieldEditor.h"
#include "Stage.h"

//==========================================================================
//
// class  : CLoadGameData
// Content: ゲームデータの読み込み
//
//==========================================================================
class CLoadGameData
{
public:
    CLoadGameData() {}
    ~CLoadGameData() {}

    // 読み込み関数
    static void DataLoad(StageData_ID ID);

    // 解放
    static void Release(void);

    // サイズの取得
    static int Size(void) { return (int)m_GameData.Size(); }

    // データのゲッター
    static const CFieldData& GetData(int label) { return *m_GameData.Get(label); }

    static StageData_ID GetSerectData(void) {
        return m_SerectID;
    }
private:
     static CObjectVectorManager<CFieldData>m_GameData; // データケース
     static StageData_ID m_SerectID; // 選択されたデータ
};

#endif // !_LoadGameData_H_
