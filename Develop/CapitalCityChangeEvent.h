//==========================================================================
// ゲームイベント:首都の場所が変わった[CapitalCityChangeEvent.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _CapitalCityChangeEvent_H_
#define _CapitalCityChangeEvent_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "GameEvent.h"

//==========================================================================
//
// class  : CCapitalCityChange
// Content: 首都の場所が変わった
//
//==========================================================================
class CCapitalCityChange : public CEventManager
{
public:
    CCapitalCityChange()
    {
        std::random_device rnd; // 非決定的な乱数生成器を生成
        std::mt19937 mt(rnd());  //  メルセンヌ・ツイスタの32ビット版、引数は初期シード値

        this->m_Name = "首都の場所が変わった";
        this->m_mt = nullptr;
        this->m_rand = std::uniform_int_distribution<int>(0, 0);

    }
    ~CCapitalCityChange()
    {
        this->m_target.clear();
        this->m_Name.clear();
    }

    // 初期化
    void Init(std::mt19937 * InputMT);

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;

    // UI更新
    void UpdateUI(void)override;

    // テキスト
    void text(std::string strEventName)override;
private:
    // 入れ替え
    bool change(CSubsidiaryManager * pchar, int capitalID);
private:
    std::mt19937 * m_mt;
    std::uniform_int_distribution<int> m_rand; // 範囲の一様乱数
};

#endif // !_CapitalCityChangeEvent_H_
