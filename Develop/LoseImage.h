#ifndef _LoseImage_H_
#define _LoseImage_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CLoseImage
// Content: �s�k���
//
//==========================================================================
class CLoseImage : public CObject
{
public:
	CLoseImage() :CObject(CObject::ID::Polygon2D) {}
	~CLoseImage() {}
	// ������
	bool Init(void)override;

	// ���
	void Uninit(void)override;

	// �X�V
	void Update(void)override;

	// �`��
	void Draw(void)override;
private:
};

#endif // !_WinImage_H_#pragma once
