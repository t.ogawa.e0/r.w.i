//==========================================================================
// ゲームイベント:バブル景気[BubbleEconomyEvent.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "BubbleEconomyEvent.h"

//==========================================================================
// 初期化
void CBubbleEconomy::Init(void)
{
    CSubsidiaryManager * pchar = nullptr;
    CSubsidiaryParam * ptarget = nullptr;

    // プレイヤーのデータを取り出す
    pchar = CSubsidiary::Get(CharList::Player);
    for (int i = 0; i < pchar->GetNumSub(); i++)
    {
        ptarget = pchar->GetSub(i);
        this->m_target.push_back(ptarget);
    }

    // エネミーのデータを取り出す
    pchar = CSubsidiary::Get(CharList::Enemy);
    for (int i = 0; i < pchar->GetNumSub(); i++)
    {
        ptarget = pchar->GetSub(i);
        this->m_target.push_back(ptarget);
    }
}

//==========================================================================
// 解放
void CBubbleEconomy::Uninit(void)
{
    this->m_target.clear();
    this->m_Name.clear();
    this->m_targetName.clear();
}

//==========================================================================
// 更新
void CBubbleEconomy::Update(void)
{
    // イテレータで最初から最後まで検索
    for (auto itr = this->m_target.begin(); itr != this->m_target.end(); ++itr)
    {
        // 売り上げに加算
        (*itr)->m_income += 100;
    }
}

//==========================================================================
// 描画
void CBubbleEconomy::Draw(void)
{
}

//==========================================================================
// UI更新
void CBubbleEconomy::UpdateUI(void)
{
    // 親オブジェクトへのアクセス
    auto * p_obj = (CMobeEventObject*)this->m_master_obj;

    // オブジェクトの座標設定
    this->m_ObjectPos->SetPos(*p_obj->Get()->GetPos());
    this->m_NameObject->SetPos(*p_obj->Get()->GetPos());
}

//==========================================================================
// テキスト
void CBubbleEconomy::text(std::string strEventName)
{
    std::string strEvent = strEventName + this->m_Name;
    if (this->m_Imgui.NewMenu(strEvent.c_str(), true))
    {
        this->m_Imgui.Text("発動中");
        this->m_Imgui.Text("全エリアの売上が＋100されていきます");
        this->m_Imgui.EndMenu();
    }
}
