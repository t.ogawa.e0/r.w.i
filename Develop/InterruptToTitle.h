#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

class CInterruptToTitle : public CObject
{
public:
    CInterruptToTitle();
    ~CInterruptToTitle();
    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;

    // タイトルを止める処理
    bool StopTitle(void);

    // タイトル処理の再開
    bool TitleControl(void);
private:
    bool m_key;
    bool m_key2;
    std::unordered_map<int, std::string>m_link;
    int m_look_id;
};

