//==========================================================================
// NamePlate[NamePlate.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CNamePlate
// Content: CNamePlate
//
//==========================================================================
class CNamePlate : public CObject
{
public:
    CNamePlate();
    ~CNamePlate();
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
};

