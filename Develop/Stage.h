#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "resource_list.h"

// ステージID
class CStage
{
public:
    // ステージID
    enum class EID
    {
        begin = -1,
        Wabisabi, // わびさび
        U_S_B, // U.S.B
        Waneha, // ワネハー
        Kankari, // カンカリー
        end,
    };
public:
    CStage() {
        this->m_SerectID = EID::begin;
    }
    ~CStage() {}

    // セット
    void Set(int _id) {
        this->m_SerectID = (EID)_id;
    }

    // 取得
    EID Get(void) {
        return this->m_SerectID;
    }
private:
    EID m_SerectID; // ステージID
};

//==========================================================================
// 演算子をリンク
//==========================================================================
template<> struct enum_system::calculation<CStage::EID> : std::true_type {};

//==========================================================================
// 定義
//==========================================================================
using StageData_ID = CStage::EID;

class CStageData
{
public:
    CStageData() {}
    ~CStageData() {}

    // ゲームマップオブジェクトのファイルパス
    // 失敗時 null
    const char * Get_Pass_MAP_X(StageData_ID _ID) {
        switch (_ID)
        {
        case StageData_ID::Wabisabi:
            return RESOURCE_japan_3d_map_x;
            break;
        case StageData_ID::U_S_B:
            return RESOURCE_USA_map_x;
            break;
        case StageData_ID::Waneha:
            return RESOURCE_Hawaii_map_x;
            break;
        case StageData_ID::Kankari:
            return RESOURCE_Aus_map_x;
            break;
        default:
            return nullptr;
            break;
        }
    }

    // ゲームデータのファイルパス
    // 失敗時 null
    const char * Get_Pass_BIN(StageData_ID _ID) {
        switch (_ID)
        {
        case StageData_ID::Wabisabi:
            return RESOURCE_Japan_bin;
            break;
        case StageData_ID::U_S_B:
            return RESOURCE_USA_bin;
            break;
        case StageData_ID::Waneha:
            return RESOURCE_Hawaii_bin;
            break;
        case StageData_ID::Kankari:
            return RESOURCE_Australia_bin;
            break;
        default:
            return nullptr;
            break;
        }
    }

    // CSVデータのファイルパス
    // 失敗時 null
    const char * Get_Pass_CSV(StageData_ID _ID) {
        switch (_ID)
        {
        case StageData_ID::Wabisabi:
            return RESOURCE_Japan_csv;
            break;
        case StageData_ID::U_S_B:
            return RESOURCE_USA_csv;
            break;
        case StageData_ID::Waneha:
            return RESOURCE_Hawaii_csv;
            break;
        case StageData_ID::Kankari:
            return RESOURCE_Australia_csv;
            break;
        default:
            return nullptr;
            break;
        }
    }
private:
};
