//==========================================================================
// シシャデータの表示[Game_Company_Date.h]
// author: tatsuya ogawa
//==========================================================================
#include "Game_Company_Date.h"
#include "CharList.h"
#include "Subsidiary.h"
#include "GameCamera.h"
#include "LoadGameData.h"
#include "Game_Syokuin.h"
#include"resource_list.h"

template<> struct enum_system::calculation<CGame_Company_Date::AustraliaList> : std::true_type {};
template<> struct enum_system::calculation<CGame_Company_Date::HawaiiList> : std::true_type {};
template<> struct enum_system::calculation<CGame_Company_Date::JapanList> : std::true_type {};
template<> struct enum_system::calculation<CGame_Company_Date::USAList> : std::true_type {};

//==========================================================================
// 初期化
bool CGame_Company_Date::Init(void)
{
    //==========================================================================
    // 宣言
    //==========================================================================
    CVector4<float>vpos4;
    CVector2<float>vpos2;
    C2DObject * pos = nullptr;

    //==========================================================================
    // シシャデータ表示用背景(共通)
    //==========================================================================

    if (this->_2DPolygon()->Init(RESOURCE_MainScreen_Company_Date_DDS, true))
    {
        return true;
    }

    // テクスチャの読み込み
    if (this->_2DPolygon()->Init(RESOURCE_MainScreen_Company_Date_tab_DDS, true))
    {
        return true;
    }

    // 2Dオブジェクト生成
    pos = this->_2DObject()->Create();

    // 初期化
    pos->Init((int)TexList::MainScreen_Company_Date);

    // 親オブジェクトの初期座標指定
    *this->m_MasterPos.Create() = CVector4<float>((float)this->GetWinSize().m_Width, 0);

    // 親オブジェクトの終点座標指定
    *this->m_MasterPos.Create() = CVector4<float>((float)this->GetWinSize().m_Width - this->_2DPolygon()->GetTexSize(pos->getindex())->w, 0);

    // 表示座標の設定
    vpos4 = CVector4<float>(this->m_MasterPos.Get(1)->x, 0);

    // 表示座標の登録
    pos->SetPos(vpos4);

    // オブジェクトの登録
    this->_2DPolygon()->ObjectInput(pos);

    //==========================================================================
    // シシャデータ表示用タブ(共通)
    //==========================================================================

    // 2Dオブジェクト生成
    pos = this->_2DObject()->Create();

    // 初期化
    pos->Init((int)TexList::MainScreen_Company_Date_tab);

    // 表示座標の設定
    vpos4 = CVector4<float>(vpos4.x - this->_2DPolygon()->GetTexSize(pos->getindex())->w, 0);

    // 表示座標の登録
    pos->SetPos(vpos4);

    // オブジェクトの登録
    this->_2DPolygon()->ObjectInput(pos);

    //==========================================================================
    // ここまでが背景(共通)
    //==========================================================================

    //==========================================================================
    // ここから数字の設定(共通)
    //==========================================================================

    //==========================================================================
    // 1回目 始まり(万の設定)
    /* テクスチャの読み込み */

    this->_2DNumber()->Init(RESOURCE_MainScreen_Normal_Numbers2_DDS, true);
    this->_2DNumber()->SetTexScale((int)Namber::NumberTexture1, 0.5f);

    /* 位置設定 */
    vpos2 = CVector2<float>();
    vpos2.x = (float)this->GetWinSize().m_Width - this->_2DNumber()->GetTexSize((int)Namber::NumberTexture1)->w * 1.3f;
    vpos2.y = (float)this->_2DNumber()->GetTexSize((int)Namber::NumberTexture1)->h * 2.9f;

    /* 数字表示の設定 */
    this->_2DNumber()->Set((int)Namber::NumberTexture1, 4, false, false, vpos2);

    /* アニメーションの設定 */
    this->_2DNumber()->SetAnim((int)Namber::NumberTexture1, 1, 10, 10);

    // 1回目 終わり
    //==========================================================================

    //==========================================================================
    // 2回目 始まり(ニンの設定)

    this->_2DNumber()->Init(RESOURCE_MainScreen_Normal_Numbers2_DDS, true);
    this->_2DNumber()->SetTexScale((int)Namber::NumberTexture2, 0.5f);

    vpos2 = CVector2<float>();
    vpos2.x = (float)this->GetWinSize().m_Width - this->_2DNumber()->GetTexSize((int)Namber::NumberTexture2)->w * 1.3f;
    vpos2.y = (float)this->_2DNumber()->GetTexSize((int)Namber::NumberTexture2)->h * 5.1f;

    /* 数字表示の設定 */
    this->_2DNumber()->Set((int)Namber::NumberTexture2, 3, false, false, vpos2);

    /* アニメーションの設定 */
    this->_2DNumber()->SetAnim((int)Namber::NumberTexture2, 1, 10, 10);

    // 2回目 終わり
    //==========================================================================

    //==========================================================================
    // 3回目 始まり(シェア率の設定)
    this->_2DNumber()->Init(RESOURCE_MainScreen_Normal_Numbers2_DDS, true);
    this->_2DNumber()->SetTexScale((int)Namber::NumberTexture3, 1.5f);
    vpos2 = CVector2<float>();
    vpos2.x = (float)this->GetWinSize().m_Width - this->_2DNumber()->GetTexSize((int)Namber::NumberTexture3)->w * 0.45f;
    vpos2.y = (float)this->_2DNumber()->GetTexSize((int)Namber::NumberTexture3)->h * 2.9f;

    /* 数字表示の設定 */
    this->_2DNumber()->Set((int)Namber::NumberTexture3, 3, false, false, vpos2);

    /* アニメーションの設定 */
    this->_2DNumber()->SetAnim((int)Namber::NumberTexture3, 1, 10, 10);

    // 3回目 終わり
    //==========================================================================

    //==========================================================================
    // ここまで数字の設定(共通)
    //==========================================================================

    //==========================================================================
    // ここから各国のシシャテキスト
    //==========================================================================

    int n_anim_limit = 0;
    switch (CLoadGameData::GetSerectData())
    {
    case StageData_ID::Wabisabi:
        if (this->Japan_Init(n_anim_limit)) { return true; }
        break;
    case StageData_ID::U_S_B:
        if (this->USA_Init(n_anim_limit)) { return true; }
        break;
    case StageData_ID::Waneha:
        if (this->Hawaii_Init(n_anim_limit)) { return true; }
        break;
    case StageData_ID::Kankari:
        if (this->Australia_Init(n_anim_limit)) { return true; }
        break;
    default:
        break;
    }

    //==========================================================================
    // ここまで各国のシシャテキスト
    //==========================================================================

    //==========================================================================
    // シシャテキスト用オブジェクト設定
    //==========================================================================
    /* 2Dオブジェクト生成 */
    pos = this->_2DObject()->Create();

    /* ここに初期化 */
    pos->Init((int)TexList::CompanyText, 1, n_anim_limit, 1);

    // 表示座標の設定
    auto * p_texsize = this->_2DPolygon()->GetTexSize(pos->getindex());

    // 表示座標の登録
    pos->SetPos(CVector4<float>((float)(vpos4.x + (p_texsize->w)), (float)this->GetWinSize().m_Height *0.015f));

    // オブジェクトの登録
    this->_2DPolygon()->ObjectInput(pos);

    //==========================================================================
    // シシャテキスト用オブジェクト設定終わり
    //==========================================================================

    //==========================================================================
    // シシャテキスト用オブジェクト設定
    //==========================================================================

    if (this->_2DPolygon()->Init(RESOURCE_MainScreen_Company_HonsyaMark_DDS, true))
    {
        return true;
    }

    /* 2Dオブジェクト生成 */
    pos = this->_2DObject()->Create();

    /* ここに初期化 */
    pos->Init((int)TexList::honsya_mark);

    // 表示座標の設定

    // 表示座標の登録
    pos->SetPos(CVector4<float>(vpos4.x*1.05f, this->GetWinSize().m_Height*0.25f));

    pos->Angle(-0.8f);

    //==========================================================================
    // シシャテキスト用オブジェクト設定終わり
    //==========================================================================

    //==========================================================================
    // シシャUIグループの初期値配置
    //==========================================================================
    for (;this->Emergence() != true;);

    //==========================================================================
    // 座標の補正用テクスチャの読み取り
    //==========================================================================
    C2DPolygon * p_display = nullptr;
    this->Helper()->New_(p_display);

    if (p_display->Init(RESOURCE_Company_display_DDS, true))
    {
        return true;
    }

    //==========================================================================
    // 移動対象の終点を設
    auto p_obj_pos = this->_2DObject()->Get((int)TexList::MainScreen_Company_Date)->GetPos();
    vpos4 = CVector4<float>(p_obj_pos->x, (float)p_display->GetTexSize(0)->h);

    // 親オブジェクトと終点オブジェクトとの誤差の検出
    float f_powor = this->_Hit()->Distance(*this->_2DObject()->Get((int)TexList::MainScreen_Company_Date)->GetPos(), vpos4);

    // 全objectに誤差の加算
    for (int i = 0; i < this->_2DObject()->Size(); i++)
    {
        auto * p_obj = this->_2DObject()->Get(i);
        p_obj->SetYPlus(f_powor);
    }

    this->_2DNumber()->MoveYPos((int)Namber::NumberTexture1, f_powor);
    this->_2DNumber()->MoveYPos((int)Namber::NumberTexture2, f_powor);
    this->_2DNumber()->MoveYPos((int)Namber::NumberTexture3, f_powor);

    // マスターオブジェクト座標の修正
    for (int i = 0; i < this->m_MasterPos.Size(); i++)
    {
        this->m_MasterPos.Get(i)->y = vpos4.y;
    }

    p_display->Release();
    this->Helper()->Delete_(p_display);

    this->m_Camera = this->GetObjects(CObject::ID::Camera, CObject::Type::Game_Camera);

    return false;
}

//==========================================================================
// 解放
void CGame_Company_Date::Uninit(void)
{
    this->m_MasterPos.Release();
    /* 何も書かなくてよい */
}

//==========================================================================
// 更新
void CGame_Company_Date::Update(void)
{
    if (this->m_Camera == nullptr)
    {
        this->m_Camera = this->GetObjects(CObject::ID::Camera, CObject::Type::Game_Camera);
    }

    auto*pcamera = (CGameCamera*)this->m_Camera;

    /* プレイヤーのシシャデータの取り出し */
    CSubsidiaryManager * pSub = CSubsidiary::Get(CCharList::CharList::Player);

    // シシャ名テクスチャの切り替え
    this->CompanyNameIDUpdate();

    /* デバッグウィンドウ */
    this->DebugWindow();

    /* 全オブジェクト一括移動 */
    this->Emergence();

    /* 登録済みオブジェクトの更新 */
    this->_2DPolygon()->Update();

    /* 万 */
    this->_2DNumber()->Update((int)Namber::NumberTexture1, pSub->GetSub(pcamera->GetSerectID())->m_income);

    /* ニン */
    this->_2DNumber()->Update((int)Namber::NumberTexture2, pSub->GetSub(pcamera->GetSerectID())->m_employee_sub);

    /* シェア率 0〜100％ */
    this->_2DNumber()->Update((int)Namber::NumberTexture3, (int)(pSub->GetSub(pcamera->GetSerectID())->m_stock_price * 100));
}

//==========================================================================
// 描画
void CGame_Company_Date::Draw(void)
{
    /* 登録済みオブジェクトの描画 */
    this->_2DPolygon()->Draw();
    this->_2DNumber()->Draw();
}

//==========================================================================
// デバッグウィンドウ
void CGame_Company_Date::DebugWindow(void)
{
    auto*pTime = (CGameCamera*)this->m_Camera;

    CSubsidiaryManager * pSub = CSubsidiary::Get(CCharList::CharList::Player);
    this->ImGui()->NewWindow("今見ているシシャデータ<デバッグ>", true);

    // オブジェクトが存在するとき
    if (pTime != nullptr)
    {
        this->ImGui()->Text("シシャ名 : %s", pSub->GetSub(pTime->GetSerectID())->m_strName.c_str());
        this->ImGui()->Text("%d万", pSub->GetSub(pTime->GetSerectID())->m_income);
        this->ImGui()->Text("%dニン", pSub->GetSub(pTime->GetSerectID())->m_employee_sub);
        this->ImGui()->Text("%.2f％", pSub->GetSub(pTime->GetSerectID())->m_stock_price * 100);
    }
    this->ImGui()->EndWindow();

    if (this->ImGui()->MenuItem("シシャデータopen"))
    {
        this->Game_Company_Data_Open();
    }
    if (this->ImGui()->MenuItem("シシャデータclause"))
    {
        this->Game_Company_Data_Clause();
    }
}

//==========================================================================
// 日本
bool CGame_Company_Date::Japan_Init(int & OutAnim)
{
    //==========================================================================
    // ここからシシャテキスト読み込み
    //==========================================================================

    /* 全テクスチャ読み込み */
    if (this->_2DPolygon()->Init(RESOURCE_color_MainScreen_Company_name_jpn_DDS, true))
    {
        return true;
    }

    //==========================================================================
    // ここまでがシシャテキスト読み込み
    //==========================================================================

    //==========================================================================
    // ここからシシャテキスト表示判定用の設定
    //==========================================================================

    this->m_Data.Create()->Set("シナノ", (int)JapanList::シナノ);
    this->m_Data.Create()->Set("ヨシノ", (int)JapanList::ヨシノ);
    this->m_Data.Create()->Set("ハッカイ", (int)JapanList::ハッカイ);
    this->m_Data.Create()->Set("コクシ", (int)JapanList::コクシ);
    this->m_Data.Create()->Set("ヒカフ", (int)JapanList::ヒカフ);
    this->m_Data.Create()->Set("ニト", (int)JapanList::ニト);
    this->m_Data.Create()->Set("ショウク", (int)JapanList::ショウク);
    this->m_Data.Create()->Set("ポン", (int)JapanList::ポン);

    OutAnim = (int)JapanList::end;

    return false;
}

//==========================================================================
// USA
bool CGame_Company_Date::USA_Init(int & OutAnim)
{
    //==========================================================================
    // ここからシシャテキスト読み込み
    //==========================================================================

    /* 全テクスチャ読み込み */
    if (this->_2DPolygon()->Init(RESOURCE_color_MainScreen_Company_name_USA_DDS, true))
    {
        return true;
    }

    //==========================================================================
    // ここまでがシシャテキスト読み込み
    //==========================================================================

    //==========================================================================
    // ここからシシャテキスト表示判定用の設定
    //==========================================================================

    this->m_Data.Create()->Set("ビームス", (int)USAList::ビームス);
    this->m_Data.Create()->Set("ジャック", (int)USAList::ジャック);
    this->m_Data.Create()->Set("トマティ", (int)USAList::トマティ);
    this->m_Data.Create()->Set("トルマ", (int)USAList::トルマ);
    this->m_Data.Create()->Set("ベレンタイン", (int)USAList::ベレンタイン);
    this->m_Data.Create()->Set("ローゼス", (int)USAList::ローゼス);
    this->m_Data.Create()->Set("ウォーカー", (int)USAList::ウォーカー);
    this->m_Data.Create()->Set("ホース", (int)USAList::ホース);
    this->m_Data.Create()->Set("ヘネス", (int)USAList::ヘネス);
    this->m_Data.Create()->Set("ハーパー", (int)USAList::ハーパー);

    OutAnim = (int)USAList::end;

    return false;
}

//==========================================================================
// ハワイ
bool CGame_Company_Date::Hawaii_Init(int & OutAnim)
{
    //==========================================================================
    // ここからシシャテキスト読み込み
    //==========================================================================

    /* 全テクスチャ読み込み */
    if (this->_2DPolygon()->Init(RESOURCE_color_MainScreen_Company_name_hawaii_DDS, true))
    {
        return true;
    }

    //==========================================================================
    // ここまでがシシャテキスト読み込み
    //==========================================================================

    //==========================================================================
    // ここからシシャテキスト表示判定用の設定
    //==========================================================================
    this->m_Data.Create()->Set("ソルティ", (int)HawaiiList::ソルティ);
    this->m_Data.Create()->Set("ミェール", (int)HawaiiList::ミェール);
    this->m_Data.Create()->Set("ヌテイン", (int)HawaiiList::ヌテイン);
    this->m_Data.Create()->Set("ジニック", (int)HawaiiList::ジニック);

    OutAnim = (int)HawaiiList::end;

    return false;
}

//==========================================================================
// オーストラリア
bool CGame_Company_Date::Australia_Init(int & OutAnim)
{
    //==========================================================================
    // ここからシシャテキスト読み込み
    //==========================================================================

    /* 全テクスチャ読み込み */
    if (this->_2DPolygon()->Init(RESOURCE_color_MainScreen_Company_name_aust_DDS, true))
    {
        return true;
    }

    //==========================================================================
    // ここまでがシシャテキスト読み込み
    //==========================================================================

    //==========================================================================
    // ここからシシャテキスト表示判定用の設定
    //==========================================================================
    this->m_Data.Create()->Set("タンカレー", (int)AustraliaList::タンカレー);
    this->m_Data.Create()->Set("ボンベイ", (int)AustraliaList::ボンベイ);
    this->m_Data.Create()->Set("シロッコ", (int)AustraliaList::シロッコ);
    this->m_Data.Create()->Set("ヒル", (int)AustraliaList::ヒル);
    this->m_Data.Create()->Set("ゴードン", (int)AustraliaList::ゴードン);
    this->m_Data.Create()->Set("ギルビー", (int)AustraliaList::ギルビー);
    this->m_Data.Create()->Set("フィーター", (int)AustraliaList::フィーター);

    OutAnim = (int)AustraliaList::end;

    return false;
}

//==========================================================================
// シシャ名テクスチャの切り替え
void CGame_Company_Date::CompanyNameIDUpdate(void)
{
    auto*p_camera = (CGameCamera*)this->m_Camera;

    // オブジェクトが存在するとき
    if (p_camera != nullptr)
    {
        C2DObject * pos = this->_2DObject()->Get((int)TexList::CompanyText);

        // セレクトIDが一致しないときに更新
        if (this->m_OLDSerectID != p_camera->GetSerectID())
        {
            CSubsidiaryManager * sub = CSubsidiary::Get(CCharList::CharList::Player);
            CDataList * dat = nullptr;
            std::string& strtag = sub->GetSub(p_camera->GetSerectID())->m_strName;

            if (sub->GetSub(p_camera->GetSerectID())->m_head_office_player == true)
            {
                this->_2DPolygon()->ObjectInput(this->_2DObject()->Get((int)TexList::honsya_mark));
            }
            else if (sub->GetSub(p_camera->GetSerectID())->m_head_office_player == false)
            {
                this->_2DPolygon()->ObjectDelete(this->_2DObject()->Get((int)TexList::honsya_mark));
            }

            for (int i = 0; i < this->m_Data.Size(); i++)
            {
                // ターゲットの探索
                dat = this->m_Data.Get(i);

                // tag一致時
                if (dat->GetTag() == strtag)
                {
                    // テクスチャIDを教える
                    this->m_anum_key = dat->GetTexID();

                    // セレクトIDの更新
                    this->m_OLDSerectID = p_camera->GetSerectID();
                    break;
                }
            }
        }
        pos->SetAnimationCount(this->m_anum_key);
    }
}

//==========================================================================
// 出現/終了
bool CGame_Company_Date::Emergence(void)
{
    auto * pPos = this->_2DObject()->Get((int)TexList::MainScreen_Company_Date);
    float fSpeed = 0.0f;

    // 表示終了時
    if (this->m_EmergenceKey == false)
    {
        auto * pLimitPos = this->m_MasterPos.Get(0);
        fSpeed = this->_Hit()->Distance(*pPos->GetPos(), *pLimitPos)*0.2f;

        // 初期座標まで移送
        return this->UIMove(fSpeed, fSpeed);
    }
    // 表示開始時
    else if (this->m_EmergenceKey == true)
    {
        auto * pLimitPos = this->m_MasterPos.Get(1);
        fSpeed = this->_Hit()->Distance(*pPos->GetPos(), *pLimitPos)*0.2f;

        // 初期座標まで移送
        return this->UIMove(fSpeed, -fSpeed);
    }
    return false;
}

//==========================================================================
// UIの移動
bool CGame_Company_Date::UIMove(float fLimit, float fSpeed)
{
    // 初期座標まで移送
    if (0.01f <= fLimit)
    {
        for (int i = 0; i < this->_2DObject()->Size(); i++)
        {
            auto * pMoveObject = this->_2DObject()->Get(i);
            pMoveObject->SetXPlus(fSpeed);
        }
        /* 万 */
        this->_2DNumber()->MoveXPos((int)Namber::NumberTexture1, fSpeed);

        /* ニン */
        this->_2DNumber()->MoveXPos((int)Namber::NumberTexture2, fSpeed);

        /* シェア率 0〜100％ */
        this->_2DNumber()->MoveXPos((int)Namber::NumberTexture3, fSpeed);

    }
    // 初期座標一致時、又は超えた場合
    else
    {
        return true;
    }
    return false;
}
