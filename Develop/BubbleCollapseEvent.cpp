//==========================================================================
// ゲームイベント:バブル崩壊[BubbleCollapseEvent.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "BubbleCollapseEvent.h"

//==========================================================================
// 初期化
void CBubbleCollapse::Init(void)
{
    CSubsidiaryManager * pchar = nullptr;
    CSubsidiaryParam * ptarget = nullptr;

    // プレイヤーのデータを取り出す
    pchar = CSubsidiary::Get(CharList::Player);
    for (int i = 0; i < pchar->GetNumSub(); i++)
    {
        ptarget = pchar->GetSub(i);
        this->m_target.push_back(ptarget);
    }

    // エネミーのデータを取り出す
    pchar = CSubsidiary::Get(CharList::Enemy);
    for (int i = 0; i < pchar->GetNumSub(); i++)
    {
        ptarget = pchar->GetSub(i);
        this->m_target.push_back(ptarget);
    }
}

//==========================================================================
// 解放
void CBubbleCollapse::Uninit(void)
{
    this->m_target.clear();
    this->m_Name.clear();
}

//==========================================================================
// 更新
void CBubbleCollapse::Update(void)
{
    // イテレータで最初から最後まで検索
    for (auto itr = this->m_target.begin(); itr != this->m_target.end(); ++itr)
    {
        // デフォルトの売り上げの0.8%に売り上げを下げる
        (*itr)->m_income = (int)((*itr)->m_income_default*0.8f);

        // デフォルトも同じに
        (*itr)->m_income_default = (*itr)->m_income;
    }
    this->m_endkey = true;
}

//==========================================================================
// 描画
void CBubbleCollapse::Draw(void)
{
}

//==========================================================================
// UI更新
void CBubbleCollapse::UpdateUI(void)
{
    // 親オブジェクトへのアクセス
    auto * p_obj = (CMobeEventObject*)this->m_master_obj;

    // オブジェクトの座標設定
    this->m_ObjectPos->SetPos(*p_obj->Get()->GetPos());
    this->m_NameObject->SetPos(*p_obj->Get()->GetPos());
}

//==========================================================================
// テキスト
void CBubbleCollapse::text(std::string strEventName)
{
    std::string strEvent = strEventName + this->m_Name;
    if (this->m_Imgui.NewMenu(strEvent.c_str(), true))
    {
        this->m_Imgui.Text("発動中");
        this->m_Imgui.Text("次、全エリアの売上げが80％に落ちます");
        this->m_Imgui.EndMenu();
    }
}
