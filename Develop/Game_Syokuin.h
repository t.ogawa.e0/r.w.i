//==========================================================================
// Game_Syokuin[Game_Syokuin.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _Game_Syokuin_H_
#define _Game_Syokuin_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CGame_Syokuin
// Content: CGame_Syokuin
//
//==========================================================================
class CGame_Syokuin : public CObject
{
public:
    enum class TexList {
        MainScreen_Syokuin_Rirekisyo, // シャインデータ表示用背景
        MainScreen_Syokuin_Date_tab, // タブ
        MainScreen_Syokuni_Name, // 名前テクスチャID
        MainScreen_syoumeisyashin, // アイコンID
        MainScreen_Syokuin_Rank_skill, // skillレアリティID
        MainScreen_Syokuin_Rank_mental, // mentalレアリティID
        MainScreen_Syokuin_Rank_cost, // costレアリティID
        MainScreen_Syokuin_Tokuchou, // 特徴ID
        MainScreen_Syokuin_Type, // タイプID
        MainScreen_Syokuin_Type_Partner_true, // パートナーがいない
        MainScreen_Syokuin_Type_Partner_false, // パートナーがいる
    };
public:
    CGame_Syokuin() :CObject(CObject::ID::Polygon2D) {
        this->SetType(Type::Game_Syokuin);
        this->m_EmergenceKey = false;
        this->m_draw_target = nullptr;
    }
    ~CGame_Syokuin() {}
    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;

    // 表示open
    void Game_Syokuin_Data_Open(void) {
        this->m_EmergenceKey = true;
    }

    // 表示clause
    void Game_Syokuin_Data_Clause(void) {
        this->m_EmergenceKey = false;
    }

    // 表示キー
    bool Get_Syokuin_Data_Key(void) {
        return this->m_EmergenceKey;
    }
private:
    // パラメーターのセット
    void SetParameter(void);

    // デバッグウィンドウ
    void DebugWindow(void);

    // 出現/終了
    // 戻り値 移動が完全に終了した際にtrue
    bool Emergence(void);

    // UIの移動
    bool UIMove(float fLimit, float fSpeed);
private:
    void * m_draw_target;
    CObjectVectorManager<CVector4<float>>m_MasterPos; // 親座標
    bool m_EmergenceKey; // 出現操作キー
};

#endif // !_Game_Syokuin_H_
