//==========================================================================
// 子会社のパタメータ[SubsidiaryParam.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "SubsidiaryParam.h"

//==========================================================================
// 更新
// ptarget 比較対象のデータ
void CSubsidiaryParam::Update(const CSubsidiaryParam * ptarget)
{
    this->m_stock_price = (float)this->m_employee / (this->m_employee + ptarget->m_employee);
    if (this->m_stock_price <= 0.0f)
    {
        this->m_stock_price = 0.0f;
    }
	this->m_profit = (int)((this->m_income*this->m_stock_price) + 0.5f);
    if (this->m_profit <= 0)
    {
        this->m_profit = 0;
    }
	this->m_employee = this->m_employee_sub;
    if (this->m_employee_sub <= 0)
    {
        this->m_employee_sub = 0;
        this->m_employee = 0;
    }
}
