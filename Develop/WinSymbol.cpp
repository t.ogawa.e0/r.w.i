//==========================================================================
#include "WinSymbol.h"
#include "resource_list.h"
#include "ResultReminiscence.h"

//==========================================================================
// 初期化
bool CWinSymbol::Init(void)
{
	const char * pUIList[] = {
		RESOURCE_Win_Chousei_DDS
	};
	int datasize = (int)this->Helper()->Sizeof_(pUIList);
	CVector4<float>vpos;
	

	// インスタンス生成

	for (int i = 0; i < datasize; i++)
	{
		if (this->_2DPolygon()->Init(pUIList[i], true))
		{
			return true;
		}
		this->_2DPolygon()->ObjectInput(this->_2DObject()->Create());
		this->_2DObject()->Get(i)->Init(i);
	}

	this->_2DPolygon()->SetTexSize(0, this->GetWinSize().m_Width, this->GetWinSize().m_Height);
	this->_2DObject()->Get(0)->SetCentralCoordinatesMood(true);
	vpos[0].x = (float)this->GetWinSize().m_Width;
	vpos[0].y = 0.0f;
	this->_2DObject()->Get(0)->Scale(1.03f);
	this->_2DObject()->Get(0)->SetPos(vpos);

	this->m_color = 255;
	this->m_color.a = 0;
	this->_2DObject()->Get(0)->SetColor(m_color);
	
	auto * p_tex = this->_2DPolygon()->GetTexSize(0);
	m_endpos = CVector2<float>(((float)p_tex->w/4.5f), (float)GetWinSize().m_Height - ((float)p_tex->h/4.0f));

    this->m_reminnn = this->GetObjects(CObject::ID::Polygon2D, CObject::Type::Result_ResultReminiscence);

	return false;
}

//==========================================================================
// 解放
void CWinSymbol::Uninit(void)
{
}

//==========================================================================
// 更新
void CWinSymbol::Update(void)
{
    if (this->m_reminnn)
    {
        this->m_reminnn = this->GetObjects(CObject::ID::Polygon2D, CObject::Type::Result_ResultReminiscence);
    }

	auto * p_obj = this->_2DObject()->Get(0);
		
	if (m_endpos.x<p_obj->GetPos()->x)
	{
		
		p_obj->SetX(p_obj->GetPos()->x - GetWinSize().m_Width/245.0f);
		if (p_obj->GetPos()->x <= m_endpos.x)
		{
			p_obj->SetX(m_endpos.x);
		}
		
	}
	if (m_endpos.y>p_obj->GetPos()->y)
	{
		p_obj->SetY(p_obj->GetPos()->y + GetWinSize().m_Height/255.0f);
		if (p_obj->GetPos()->y >= m_endpos.y)
		{
			p_obj->SetY(m_endpos.y);
		}
	}
	if (p_obj->GetScale() > 0.5f)
	{
		p_obj->Scale(-0.008f);
	}
    else
    {
        auto * p_remsisjfi = (CResultReminiscence*)this->m_reminnn;
        p_remsisjfi->Start();
    }
	if(m_color.a<255)
	{
		m_color.a +=2;
		if (m_color.a > 255)
		{
			m_color.a = 255;
		}
		p_obj->SetColor(m_color);
		
	}
	this->_2DPolygon()->Update();
}

//==========================================================================
// 描画
void CWinSymbol::Draw(void)
{
	this->_2DPolygon()->Draw();
}