//==========================================================================
// ゲームイベント:ニリンピック開催[NirinPicHoldingEvent.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _NirinPicHoldingEvent_H_
#define _NirinPicHoldingEvent_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "GameEvent.h"

//==========================================================================
//
// class  : CNirinPicHolding
// Content: ニリンピック開催
//
//==========================================================================
class CNirinPicHolding : public CEventManager
{
public:
    CNirinPicHolding()
    {
        this->m_Name = "ニリンピック開催";
        this->m_bonus = 0;
        this->m_NextTurn = 12;
        this->m_key = false;
    }
    ~CNirinPicHolding()
    {
        this->m_target.clear();
        this->m_Name.clear();
        this->m_targetName.clear();
    }

    // 初期化
    void Init(std::mt19937 & InputMT);

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;

    // UI更新
    void UpdateUI(void)override;

    // テキスト
    void text(std::string strEventName)override;
private:
    int m_NextTurn;
    int m_bonus; // 加算売り上げ
    bool m_key; // 処理ロック
};

#endif // !_NirinPicHoldingEvent_H_
