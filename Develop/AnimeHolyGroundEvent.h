//==========================================================================
// ゲームイベント:アニメの聖地[AnimeHolyGroundEvent.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _AnimeHolyGroundEvent_H_
#define _AnimeHolyGroundEvent_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "GameEvent.h"

//==========================================================================
//
// class  : CAnimeHolyGround
// Content: アニメの聖地
//
//==========================================================================
class CAnimeHolyGround : public CEventManager
{
public:
    CAnimeHolyGround()
    {
        this->m_Name = "アニメの聖地";

    }
    ~CAnimeHolyGround()
    {
        this->m_target.clear();
        this->m_Name.clear();
    }

    // 初期化
    void Init(std::mt19937 & InputMT);

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;

    // UI更新
    void UpdateUI(void)override;

    // テキスト
    void text(std::string strEventName)override;
private:

};

#endif // !_AnimeHolyGroundEvent_H_
