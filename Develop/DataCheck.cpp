//==========================================================================
// データチェック[DataCheck.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "DataCheck.h"

#include "Character.h"
#include "Worldtime.h"
#include "ImGui_Player.h"
#include "ImGui_Enemy.h"
#include "GameEvent.h"
#include "Subsidiary.h"
#include "LoadGameData.h"

CDataCheck::CDataCheck()
{
    CLoadGameData::DataLoad(StageData_ID::Wabisabi);
    //==========================================================================
    // 宣言
    CSubsidiary * sub;
    CWorldTime * worldtime;
    CImGui_Player * player;
    CImGui_Enemy * enemy;
    CGameEvent * pevent;

    //==========================================================================
    // オブジェクト登録
    if (CLoadGameData::Size() != 0)
    {
        CObject::NewObject(worldtime);
        CObject::NewObject(sub);
        CObject::NewObject(player);
        CObject::NewObject(enemy);
        CObject::NewObject(pevent);
    }
    else if (CLoadGameData::Size() == 0)
    {
        CDeviceManager::GetDXDevice()->ErrorMessage("読み込めるステージデータがありませんでした。\n 開発用のため わびさび.bin で固定化されています。");
    }
}

CDataCheck::~CDataCheck()
{
}

//==========================================================================
// 初期化
bool CDataCheck::Init(void)
{
    return CObject::InitAll();
}

//==========================================================================
// 解放
void CDataCheck::Uninit(void)
{
    CObject::ReleaseAll();
    CCharacter::ReleaseAll();
    CLoadGameData::Release();
}

//==========================================================================
// 更新
void CDataCheck::Update(void)
{
    this->m_ImGui.NewWindow("データチェックデバッグウィンドウ", true);
    CObject::UpdateAll();
    this->m_ImGui.EndWindow();
}

//==========================================================================
// 描画
void CDataCheck::Draw(void)
{
    CObject::DrawAll();
}
