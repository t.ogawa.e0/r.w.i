//==========================================================================
// データチェック[DataCheck.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _DataCheck_H_
#define _DataCheck_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "SceneChange.h"

//==========================================================================
//
// class  : CDataCheck
// Content: データチェック
//
//==========================================================================
class CDataCheck : public CBaseScene
{
public:
    CDataCheck();
    ~CDataCheck();
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
private:
    CImGui_Dx9 m_ImGui; // ImGui
};

#endif // !_DataCheck_H_
