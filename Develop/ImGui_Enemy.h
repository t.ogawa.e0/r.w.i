//==========================================================================
// ImGuiデバッグエネミー[ImGui_Enemy.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _ImGui_Enemy_H_
#define _ImGui_Enemy_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "PlayerParam.h"
#include "Character.h"

//==========================================================================
//
// class  : CImGui_Enemy
// Content: エネミー
//
//==========================================================================
class CImGui_Enemy : public CObject, public CCharacter
{
private:
    // 各種操作
    enum class actionID
    {
        NONE = -1,//何もしない
        EMPLOYMENT = 0,
        FIRE,
        TRANSFERRED,
        WAGE,
    };
public:
    CImGui_Enemy() {
        this->m_WorldTime = nullptr;
    }
    ~CImGui_Enemy() {}
    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;
private:
    // AI用関数
    //行動を行う：とりあえずプレイヤーと同じ形にしました
    void action(const CGameManager::TURNS_t & InputTime);

    //行動の決定
    void selectTarget(void);
private:
    //	AI用変数
    actionID m_actionID[2];	//操作種別(雇用して移動する場合だけタスク処理なので一応配列で確保
    int m_targetNum;		//操作ターゲットナンバー
    int m_sourceTargetNum;	//移動元ターゲットナンバー
    int m_numemployment;	//操作人数
    int m_waitCounter[20];	//移動関係処理に関わったエリアを評価外にするためのカウンター
    void * m_WorldTime; // 時間へのアクセスルート
};

#endif // !_ImGui_Enemy_H_
