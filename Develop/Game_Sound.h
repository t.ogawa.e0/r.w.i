//==========================================================================
// ゲームのサウンド[Game_Sound.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "Stage.h"

//==========================================================================
//
// class  : CGame_Sound
// Content: ゲームのサウンド
//
//==========================================================================
class CGame_Sound : public CObject
{
private:
    enum class EList
    {
        Stage_BGM,
        Turn_SE,
        tab_change_SE,
        Sinful_SE,
        Storiki_voice,
    };
public:
    CGame_Sound();
    CGame_Sound(StageData_ID serect);
    ~CGame_Sound();
    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;

    // タブSE
    void tab_se(void);

    // 罪深いSE
    void Sinful_SE(void);

    // ボイス
    void voice(void);
private:
    // ステージBGM(自動化)
    void bgm(void);

    // ターンSE
    void turn_se(void);

private:
    StageData_ID m_stage;
    void * m_WorldTime;
    bool m_bgm_key;
};

