//==========================================================================
// プレイヤー[Player.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _Player_H_
#define _Player_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "PlayerParam.h"
#include "Character.h"

//==========================================================================
//
// class  : CPlayer
// Content: プレイヤー
//
//==========================================================================
class CPlayer : public CObject, public CCharacter
{
public:
    CPlayer() :CObject(CObject::ID::Default) {
        this->m_WorldTime = nullptr;
        this->m_Camera = nullptr;
        this->m_strike__ = nullptr;
        this->m_GamePlayerEmployee = nullptr;
        this->m_OperationExplanation = nullptr;
        this->m_start_script = nullptr;
        this->SetType(Type::Game_Player);
        this->m_old_serect_id = -1;
    }
    ~CPlayer() {
        this->m_task.clear();
    }
    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;
    
    // 大きい場合はtrue/小さい場合false
    bool GetAsset_Min_Max(void) {
        if (this->m_param.m_asset <this->m_param.m_old_asset)
        {
            return false;
        }
        else if (this->m_param.m_old_asset <this->m_param.m_asset)
        {
            return true;
        }
        return true;
    }

    int GetAsset(void) {
        return this->m_param.m_asset;
    }
    int GetOldAsset(void) {
        return this->m_param.m_old_asset;
    }
private:
    // プレイヤーの行動
    void action(void);

    // 雇用
    void employment(void);

    // 解雇
    void fire(void);

    // 転勤
    void transferred(void);

    // 賃金
    void wage(void);

    // データの補正
    void Correction(void);
private:
    std::list<CSubsidiaryParam*>m_task; // 処理task
    void * m_WorldTime; // 時間へのアクセスルート
    void * m_Camera; // カメラへのアクセスルート
    void * m_GamePlayerEmployee; // プレイヤーの社員へのアクセス権
    void * m_OperationExplanation; // 操作制御
    void * m_start_script; // スタート制御
    void * m_strike__; // ストライキ演出
    int m_old_serect_id;
};

#endif // !_Player_H_
