//==========================================================================
// メニュー[Home.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Home.h"
#include "Logo.h"
#include "Warning.h"

CHome::CHome()
{
    // オブジェクトの宣言
    CLogo * logo;
    CWarning * Warning;

    // オブジェクトの登録
    CObject::NewObject(logo);
    CObject::NewObject(Warning);
}

CHome::~CHome()
{
}

//==========================================================================
// 初期化
bool CHome::Init(void)
{
    // オブジェクトの初期化
    return CObject::InitAll();
}

//==========================================================================
// 解放
void CHome::Uninit(void)
{
    CObject::ReleaseAll();
}

//==========================================================================
// 更新
void CHome::Update(void)
{
    CObject::UpdateAll();
}

//==========================================================================
// 描画
void CHome::Draw(void)
{
    CObject::DrawAll();
}