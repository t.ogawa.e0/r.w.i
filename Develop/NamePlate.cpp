//==========================================================================
// NamePlate[NamePlate.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "NamePlate.h"
#include "resource_list.h"
#include "TitleData.h"

CNamePlate::CNamePlate() : CObject(ID::Polygon2D)
{
}


CNamePlate::~CNamePlate()
{
}

//==========================================================================
// 初期化
bool CNamePlate::Init(void)
{
	CVector4<float> vpos;

	this->_2DPolygon()->Init(RESOURCE_TitleScreen_LOGO_DDS, true);
	this->_2DObject()->Create();
	this->_2DPolygon()->ObjectInput(this->_2DObject()->Get(0));
	this->_2DPolygon()->SetTexScale(0, 1.1f);
	this->_2DObject()->Get(0)->Init(0, 1, 5, 1);
	this->_2DObject()->Get(0)->SetAnimationCount(4 - (int)CTitleData::LoadEnemyLevel());
	vpos.x = (float)this->_2DPolygon()->GetTexSize(0)->w * 6.32f;
	vpos.y = (float)this->_2DPolygon()->GetTexSize(0)->h * 0.141f;
	this->_2DObject()->Get(0)->SetCentralCoordinatesMood(true);
	this->_2DObject()->Get(0)->SetPos(vpos);
   
    return false;
}

//==========================================================================
// 解放
void CNamePlate::Uninit(void)
{
}

//==========================================================================
// 更新
void CNamePlate::Update(void)
{
    this->_2DPolygon()->Update();
}

//==========================================================================
// 描画
void CNamePlate::Draw(void)
{
    this->_2DPolygon()->Draw();
}
