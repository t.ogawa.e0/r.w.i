//==========================================================================
// ゲームイベント:新幹線開通[ShinkansenOpeningEvent.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _ShinkansenOpening_H_
#define _ShinkansenOpening_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "GameEvent.h"

//==========================================================================
//
// class  : CShinkansenOpening
// Content: 新幹線開通
//
//==========================================================================
class CShinkansenOpening : public CEventManager
{
public:
    CShinkansenOpening()
    {
        this->m_Name = "新幹線開通";
    }
    ~CShinkansenOpening()
    {
        this->m_target.clear();
        this->m_Name.clear();
    }

    // 初期化
    void Init(void);

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;

    // UI更新
    void UpdateUI(void)override;

    // テキスト
    void text(std::string strEventName)override;
private:
};

#endif // !_ShinkansenOpening_H_
