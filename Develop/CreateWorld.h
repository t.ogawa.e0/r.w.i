//==========================================================================
// ワールド生成[CreateWorld.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _CreateWorld_H_
#define _CreateWorld_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "FieldEditor.h"

//==========================================================================
//
// class  : CCreateWorld
// Content: ワールド
//
//==========================================================================
class CCreateWorld : public CObject, public CFieldEditor
{
public:
    typedef CMouse MOUSE_t;
public:
    CCreateWorld() : CObject(CObject::ID::Grid) {
        this->m_gridsize = 0; 
        this->m_pos.Init(0);
    }
	~CCreateWorld() {

    }
	// 初期化
	bool Init(void)override;

	// 解放
	void Uninit(void)override;

	// 更新
	void Update(void)override;

	// 描画
	void Draw(void)override;

    // 移動
    void Move(void);
private:
    // カメラ
    void Camera(void);
private:
    int m_gridsize; // グリッドのサイズ
};

#endif // !_CreateWorld_H_
