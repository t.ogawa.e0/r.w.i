//==========================================================================
// スコア表示UI[GameUIScore.h]
// author: yoji watanabe
//==========================================================================
#ifndef _GameUIScore_H_
#define _GameUIScore_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

class CGameUIScore : public CObject
{
private:
    enum class EUIList {
        Main_Screen_MainUI_BG, // メインUI背景
		//タイマー表示UI群
		Main_Screen_Timer0,
		Main_Screen_Timer1,
		Main_Screen_Timer2,
		Main_Screen_Timer3,
		Main_Screen_Timer4,
		Main_Screen_Timer5,
		Main_Screen_Timer6,
		Main_Screen_Timer7,
		Main_Screen_Timer8,
		Main_Screen_Timer9,
		Main_Screen_Timer10,
		Main_Screen_Timer11,
		Main_Screen_Timer12,
		Main_Screen_Timer13,
		Main_Screen_Timer14,
		Main_Screen_Timer15,
		Main_Screen_Timer16,
		Main_Screen_Timer17,
		Main_Screen_Timer18,
		Main_Screen_Timer19,
		Main_Screen_Timer20,
		Main_Screen_Timer21,
		Main_Screen_Timer22,
		Main_Screen_Timer23,
		Main_Screen_Timer24,
		Main_Screen_Timer25,
		Main_Screen_Timer26,
		Main_Screen_Timer27,
		Main_Screen_Timer28,
		Main_Screen_Timer29,
		Main_Screen_Timer30,
		Main_Screen_Timer31,
		Main_Screen_Timer32,
		Main_Screen_Timer33,
		Main_Screen_Timer34,
		Main_Screen_Timer35,
		Main_Screen_Timer36,
		Main_Screen_Timer37,
		Main_Screen_Timer38,
		Main_Screen_Timer39,
		Main_Screen_Timer40,
		Main_Screen_Timer41,
		Main_Screen_Timer42,
		Main_Screen_Timer43,
		Main_Screen_Timer44,
		Main_Screen_Timer45,
		Main_Screen_Timer46,
		Main_Screen_Timer47,
		Main_Screen_Timer48,
		Main_Screen_Timer49,
		Main_Screen_Timer50,
		Main_Screen_Timer51,
		Main_Screen_Timer52,
		Main_Screen_Timer53,
		Main_Screen_Timer54,
		Main_Screen_Timer55,
		Main_Screen_Timer56,
		Main_Screen_Timer57,
		Main_Screen_Timer58,
		Main_Screen_Timer59,
		Main_Screen_Timer60,
		Main_Screen_Timer61,
		Main_Screen_Timer62,
		Main_Screen_Timer63,
		Main_Screen_Timer64,
		Main_Screen_Timer65,
		Main_Screen_Timer66,
		Main_Screen_Timer67,
		Main_Screen_Timer68,
		Main_Screen_Timer69,
		Main_Screen_Timer70,
		Main_Screen_Timer71,
		Main_Screen_Timer72,
		Main_Screen_Timer73,
		Main_Screen_Timer74,
		Main_Screen_Timer75,
		Main_Screen_Timer76,
		Main_Screen_Timer77,
		Main_Screen_Timer78,
		Main_Screen_Timer79,
		Main_Screen_Timer80,
		Main_Screen_Timer81,
		Main_Screen_Timer82,
		Main_Screen_Timer83,
		Main_Screen_Timer84,
		Main_Screen_Timer85,
		Main_Screen_Timer86,
		Main_Screen_Timer87,
		Main_Screen_Timer88,
		Main_Screen_Timer89,
		Main_Screen_MainUI, // メインUI
		MainScreen_Dissatisfied_Indication, //不満度表示
    };
    enum class ENumber {
        Numbers1, // 数字
        Numbers2, // 数字
        NumYear, //年
        NumMonth, //月
        NumEmployee, //雇用人数
        NumSalary, // プレイヤースコア
    };
public:
    CGameUIScore() :CObject(CObject::ID::Polygon2D) {
        this->m_WorldTime = nullptr;
    }
    ~CGameUIScore() {}
    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;
private:
    void * m_WorldTime; // 時間へのアクセスルート
    CVector4<float>m_Dissatisfied_Indication_start_pos; // 不満度バーの始点
    CVector4<float>m_Dissatisfied_Indication_end_pos; // 不満度バーの終点
};

#endif // !_GameUIScore_H_
