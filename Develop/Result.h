//==========================================================================
// リザルト[Result.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _Result_H_
#define _Result_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "SceneChange.h"
#include "GameEnd.h"

//==========================================================================
//
// class  : CResult
// Content: リザルト
//
//==========================================================================
class CResult : public CBaseScene
{
public:
    CResult();
    ~CResult();
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
private:
    void Load(void);
private:
    EGameEndKey m_key;
};

#endif // !_Result_H_
