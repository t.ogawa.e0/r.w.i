//==========================================================================
// Game_Syokuin[Game_Syokuin.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Game_Syokuin.h"
#include "GamePlayerEmployee.h"
#include "resource_list.h"

//==========================================================================
// 初期化
bool CGame_Syokuin::Init(void)
{
    //==========================================================================
    // 宣言
    //==========================================================================
    CVector4<float>vpos4;
    CVector2<float>vpos2;
    C2DObject * pos = nullptr;

    //==========================================================================
    // シャインデータ表示用背景(共通)
    //==========================================================================

    if (this->_2DPolygon()->Init(RESOURCE_MainScreen_Syokuin_Rirekisyo_DDS, true))
    {
        return true;
    }

    // テクスチャの読み込み
    if (this->_2DPolygon()->Init(RESOURCE_MainScreen_Syokuin_Date_tab_DDS, true))
    {
        return true;
    }

    // 2Dオブジェクト生成
    pos = this->_2DObject()->Create();

    // 初期化
    pos->Init((int)TexList::MainScreen_Syokuin_Rirekisyo);

    // 親オブジェクトの初期座標指定
    *this->m_MasterPos.Create() = CVector4<float>((float)this->GetWinSize().m_Width, (float)this->_2DPolygon()->GetTexSize((int)TexList::MainScreen_Syokuin_Date_tab)->h);

    // 親オブジェクトの終点座標指定
    *this->m_MasterPos.Create() = CVector4<float>((float)this->GetWinSize().m_Width - this->_2DPolygon()->GetTexSize(pos->getindex())->w, (float)this->_2DPolygon()->GetTexSize((int)TexList::MainScreen_Syokuin_Date_tab)->h);

    // 表示座標の設定
    vpos4 = CVector4<float>(this->m_MasterPos.Get(1)->x, this->m_MasterPos.Get(1)->y);

    // 表示座標の登録
    pos->SetPos(vpos4);

    // オブジェクトの登録
    this->_2DPolygon()->ObjectInput(pos);

    //==========================================================================
    // シャインデータ表示用タブ(共通)
    //==========================================================================

    // 2Dオブジェクト生成
    pos = this->_2DObject()->Create();

    // 初期化
    pos->Init((int)TexList::MainScreen_Syokuin_Date_tab);

    // 表示座標の設定
    vpos4 = CVector4<float>(vpos4.x - this->_2DPolygon()->GetTexSize(pos->getindex())->w, (float)this->_2DPolygon()->GetTexSize(pos->getindex())->h);

    // 表示座標の登録
    pos->SetPos(vpos4);

    // オブジェクトの登録
    this->_2DPolygon()->ObjectInput(pos);

    //==========================================================================
    // ここまでが背景(共通)
    //==========================================================================

    //==========================================================================
    // 名前オブジェクト ここから
    //==========================================================================

    // テクスチャの読み込み
    if (this->_2DPolygon()->Init(RESOURCE_MainScreen_Syokuni_Name_DDS, true))
    {
        return true;
    }

    // 2Dオブジェクト生成
    pos = this->_2DObject()->Create();

    // 初期化
    pos->Init((int)TexList::MainScreen_Syokuni_Name, 1, 50, 5);

    /*
    表示座標の設定
    ※定数禁止！
    必ずウィンドウサイズから座標を生成しよう！

    this->GetWinSize(); // ウィンドウサイズ取得関数
    */

    vpos4 = CVector4<float>(this->GetWinSize().m_Width / 2.0f * 1.52f, this->GetWinSize().m_Height / 2.0f * 0.315f);

    // 表示座標の登録
    pos->SetPos(vpos4);

    // オブジェクトの登録
    this->_2DPolygon()->ObjectInput(pos);

    //==========================================================================
    // 名前オブジェクト ここまで
    //==========================================================================

    //==========================================================================
    // アイコンオブジェクト ここから
    //==========================================================================

    // テクスチャの読み込み
    if (this->_2DPolygon()->Init(RESOURCE_MainScreen_syoumeisyashin_DDS, true))
    {
        return true;
    }

    // 2Dオブジェクト生成
    pos = this->_2DObject()->Create();

    // 初期化
    pos->Init((int)TexList::MainScreen_syoumeisyashin, 1, 19, 5);

    /*
    表示座標の設定
    ※定数禁止！
    必ずウィンドウサイズから座標を生成しよう！

    this->GetWinSize(); // ウィンドウサイズ取得関数
    */
    vpos4 = CVector4<float>(((this->GetWinSize().m_Width / 2.0f) * 1.52f) + 2, ((this->GetWinSize().m_Height / 2.0f) * 0.4f) + 8);

    // 表示座標の登録
    pos->SetPos(vpos4);

    // オブジェクトの登録
    this->_2DPolygon()->ObjectInput(pos);

    //==========================================================================
    // アイコンオブジェクト ここまで
    //==========================================================================

    //==========================================================================
    // 年齢オブジェクト ここから
    //==========================================================================

    // テクスチャの読み込み
    if (this->_2DPolygon()->Init(RESOURCE_MainScreen_Syokuin_Nenrei_DDS, true))
    {
        return true;
    }

    // 2Dオブジェクト生成
    pos = this->_2DObject()->Create();

    // 初期化
    pos->Init((int)TexList::MainScreen_Syokuin_Rank_skill, 1, 13, 2);

    /*
    表示座標の設定
    ※定数禁止！
    必ずウィンドウサイズから座標を生成しよう！

    this->GetWinSize(); // ウィンドウサイズ取得関数
    */
    vpos4 = CVector4<float>(this->GetWinSize().m_Width / 2.0f * 1.80f, this->GetWinSize().m_Height / 2.0f * 0.46f);


    // -0.05/ +0.02 
    // 表示座標の登録
    pos->SetPos(vpos4);

    // オブジェクトの登録
    this->_2DPolygon()->ObjectInput(pos);

    //==========================================================================
    // 年齢オブジェクト ここまで
    //==========================================================================

    //==========================================================================
    // 国籍オブジェクト ここから
    //==========================================================================

    // テクスチャの読み込み
    if (this->_2DPolygon()->Init(RESOURCE_MainScreen_Syokuin_Kokuseki_DDS, true))
    {
        return true;
    }

    // 2Dオブジェクト生成
    pos = this->_2DObject()->Create();

    // 初期化
    pos->Init((int)TexList::MainScreen_Syokuin_Rank_mental, 1, 8, 1);

    /*
    表示座標の設定
    ※定数禁止！
    必ずウィンドウサイズから座標を生成しよう！

    this->GetWinSize(); // ウィンドウサイズ取得関数
    */
    vpos4 = CVector4<float>(this->GetWinSize().m_Width / 2.0f * 1.80f, this->GetWinSize().m_Height / 2.0f * 0.565f);

    // 表示座標の登録
    pos->SetPos(vpos4);

    // オブジェクトの登録
    this->_2DPolygon()->ObjectInput(pos);

    //==========================================================================
    // 国籍オブジェクト ここまで
    //==========================================================================

    //==========================================================================
    // 性格オブジェクト ここから
    //==========================================================================

    // テクスチャの読み込み
    if (this->_2DPolygon()->Init(RESOURCE_MainScreen_Syokuin_Seikaku_DDS, true))
    {
        return true;
    }

    // 2Dオブジェクト生成
    pos = this->_2DObject()->Create();

    // 初期化
    pos->Init((int)TexList::MainScreen_Syokuin_Rank_cost, 1, 9, 1);

    /*
    表示座標の設定
    ※定数禁止！
    必ずウィンドウサイズから座標を生成しよう！

    this->GetWinSize(); // ウィンドウサイズ取得関数
    */
    vpos4 = CVector4<float>(this->GetWinSize().m_Width / 2.0f * 1.80f, this->GetWinSize().m_Height / 2.0f * 0.665f);

    // 表示座標の登録
    pos->SetPos(vpos4);

    // オブジェクトの登録
    this->_2DPolygon()->ObjectInput(pos);

    //==========================================================================
    // 性格オブジェクト ここまで
    //==========================================================================

    //==========================================================================
    // 特徴オブジェクト ここから
    //==========================================================================

    // テクスチャの読み込み
    if (this->_2DPolygon()->Init(RESOURCE_MainScreen_Syokuin_Tokuchou_DDS, true))
    {
        return true;
    }

    // 2Dオブジェクト生成
    pos = this->_2DObject()->Create();

    // 初期化
    pos->Init((int)TexList::MainScreen_Syokuin_Tokuchou, 1, 8, 1);

    /*
    表示座標の設定
    ※定数禁止！
    必ずウィンドウサイズから座標を生成しよう！

    this->GetWinSize(); // ウィンドウサイズ取得関数
    */
    vpos4 = CVector4<float>(this->GetWinSize().m_Width / 2.0f * 1.71f, this->GetWinSize().m_Height / 2.0f * 0.755f);

    // 表示座標の登録
    pos->SetPos(vpos4);

    // オブジェクトの登録
    this->_2DPolygon()->ObjectInput(pos);

    //==========================================================================
    // 特徴オブジェクト ここまで
    //==========================================================================

    //==========================================================================
    // タイプオブジェクト ここから
    //==========================================================================

    // テクスチャの読み込み
    if (this->_2DPolygon()->Init(RESOURCE_MainScreen_Syokuin_Type_DDS, true))
    {
        return true;
    }

    // 2Dオブジェクト生成
    pos = this->_2DObject()->Create();

    // 初期化
    pos->Init((int)TexList::MainScreen_Syokuin_Type, 1, 4, 4);

    /*
    表示座標の設定
    ※定数禁止！
    必ずウィンドウサイズから座標を生成しよう！

    this->GetWinSize(); // ウィンドウサイズ取得関数
    */
    vpos4 = CVector4<float>(this->GetWinSize().m_Width / 2.0f * 1.49f, this->GetWinSize().m_Height / 2.0f * 0.65f);

    // 表示座標の登録
    pos->SetPos(vpos4);

    // オブジェクトの登録
    this->_2DPolygon()->ObjectInput(pos);

    //==========================================================================
    // タイプオブジェクト ここまで
    //==========================================================================

    //==========================================================================
    // パートナーがいるブジェクト ここから
    //==========================================================================

    // テクスチャの読み込み
    if (this->_2DPolygon()->Init(RESOURCE_MainScreen_Syokuin_Type_DDS, true))
    {
        return true;
    }

    // 2Dオブジェクト生成
    pos = this->_2DObject()->Create();

    // 初期化
    pos->Init((int)TexList::MainScreen_Syokuin_Type_Partner_true, 1, 4, 4);

    /*
    表示座標の設定
    ※定数禁止！
    必ずウィンドウサイズから座標を生成しよう！

    this->GetWinSize(); // ウィンドウサイズ取得関数
    */
    vpos4 = CVector4<float>(this->GetWinSize().m_Width / 2.0f * 1.72f, this->GetWinSize().m_Height / 2.0f * 0.27f);

    // 表示座標の登録
    pos->SetPos(vpos4);

    //==========================================================================
    // パートナーがいるブジェクト ここまで
    //==========================================================================

    //==========================================================================
    // パートナーがいないブジェクト ここから
    //==========================================================================

    // テクスチャの読み込み
    if (this->_2DPolygon()->Init(RESOURCE_MainScreen_Syokuin_Type_DDS, true))
    {
        return true;
    }

    // 2Dオブジェクト生成
    pos = this->_2DObject()->Create();

    // 初期化
    pos->Init((int)TexList::MainScreen_Syokuin_Type_Partner_false, 1, 4, 4);

    /*
    表示座標の設定
    ※定数禁止！
    必ずウィンドウサイズから座標を生成しよう！

    this->GetWinSize(); // ウィンドウサイズ取得関数
    */

    vpos4 = CVector4<float>(this->GetWinSize().m_Width / 2.0f * 1.85f, this->GetWinSize().m_Height / 2.0f * 0.27f);

    // 表示座標の登録
    pos->SetPos(vpos4);

    //==========================================================================
    // パートナーがいないオブジェクト ここまで
    //==========================================================================

    //==========================================================================
    // シシャUIグループの初期値配置
    //==========================================================================
    for (;this->Emergence() != true;);

    //==========================================================================
    // 座標の補正用テクスチャの読み取り
    //==========================================================================
    C2DPolygon * p_display = nullptr;
    this->Helper()->New_(p_display);

    if (p_display->Init(RESOURCE_Rirekisyo_display_DDS, true))
    {
        return true;
    }

    //==========================================================================
    // 移動対象の終点を設
    auto p_obj_pos = this->_2DObject()->Get((int)TexList::MainScreen_Syokuin_Rirekisyo)->GetPos();
    vpos4 = CVector4<float>(p_obj_pos->x, (float)p_display->GetTexSize(0)->h);

    // 親オブジェクトと終点オブジェクトとの誤差の検出
    float f_powor = this->_Hit()->Distance(*p_obj_pos, vpos4);

    // 全objectに誤差の加算
    for (int i = 0; i < this->_2DObject()->Size(); i++)
    {
        auto * p_obj = this->_2DObject()->Get(i);
        p_obj->SetYPlus(f_powor);
    }

    // マスターオブジェクト座標の修正
    for (int i = 0; i < this->m_MasterPos.Size(); i++)
    {
        this->m_MasterPos.Get(i)->y = vpos4.y;
    }

    p_display->Release();
    this->Helper()->Delete_(p_display);

    return false;
}

//==========================================================================
// 解放
void CGame_Syokuin::Uninit(void)
{
    this->m_MasterPos.Release();
}

//==========================================================================
// 更新
void CGame_Syokuin::Update(void)
{
    if (this->m_draw_target == nullptr)
    {
        this->m_draw_target = this->GetObjects(CObject::ID::Default, CObject::Type::Game_GamePlayerEmployee);
    }

    // オブジェクトが存在するとき
    if (this->m_draw_target != nullptr)
    {

        /* パラメーターのセット */
        this->SetParameter();

        /* デバッグウィンドウ */
        this->DebugWindow();

        /* 全オブジェクト一括移動 */
        this->Emergence();
    }

    /* 登録済みオブジェクトの更新 */
    this->_2DPolygon()->Update();
}

//==========================================================================
// 描画
void CGame_Syokuin::Draw(void)
{
    /* 登録済みオブジェクトの描画 */
    this->_2DPolygon()->Draw();
}

//==========================================================================
// パラメーターのセット
void CGame_Syokuin::SetParameter(void)
{
    /* 描画オブジェクトに対して必要な情報を与える処理 */
    C2DObject * p_obj = nullptr; // 2Dオブジェクト
    auto * p_data = (CGamePlayerEmployee*)this->m_draw_target;
    auto * p_param = p_data->lookData(); // 描画対象のデータ

    // 更新前にオブジェクトを破棄
    this->_2DPolygon()->ObjectDelete(this->_2DObject()->Get((int)TexList::MainScreen_Syokuni_Name));
    this->_2DPolygon()->ObjectDelete(this->_2DObject()->Get((int)TexList::MainScreen_syoumeisyashin));
    this->_2DPolygon()->ObjectDelete(this->_2DObject()->Get((int)TexList::MainScreen_Syokuin_Rank_skill));
    this->_2DPolygon()->ObjectDelete(this->_2DObject()->Get((int)TexList::MainScreen_Syokuin_Rank_mental));
    this->_2DPolygon()->ObjectDelete(this->_2DObject()->Get((int)TexList::MainScreen_Syokuin_Rank_cost));
    this->_2DPolygon()->ObjectDelete(this->_2DObject()->Get((int)TexList::MainScreen_Syokuin_Tokuchou));
    this->_2DPolygon()->ObjectDelete(this->_2DObject()->Get((int)TexList::MainScreen_Syokuin_Type));
    this->_2DPolygon()->ObjectDelete(this->_2DObject()->Get((int)TexList::MainScreen_Syokuin_Type_Partner_true));
    this->_2DPolygon()->ObjectDelete(this->_2DObject()->Get((int)TexList::MainScreen_Syokuin_Type_Partner_false));

    if (p_param != nullptr)
    {
        /* 名前の固定化 */
        p_obj = this->_2DObject()->Get((int)TexList::MainScreen_Syokuni_Name);
        p_obj->SetAnimationCount(p_param->GetName()->id);
        this->_2DPolygon()->ObjectInput(p_obj);

        /* アイコンの固定化 */
        p_obj = this->_2DObject()->Get((int)TexList::MainScreen_syoumeisyashin);
        p_obj->SetAnimationCount(p_param->GetIcoFrameID());
        this->_2DPolygon()->ObjectInput(p_obj);

        /* スキルランクの固定化 */
        p_obj = this->_2DObject()->Get((int)TexList::MainScreen_Syokuin_Rank_skill);
        p_obj->SetAnimationCount(p_param->GetAge()->id);
        this->_2DPolygon()->ObjectInput(p_obj);

        /* メンタルの固定化 */
        p_obj = this->_2DObject()->Get((int)TexList::MainScreen_Syokuin_Rank_mental);
        p_obj->SetAnimationCount(p_param->GetCountry()->id);
        this->_2DPolygon()->ObjectInput(p_obj);

        /* コストの固定化 */
        p_obj = this->_2DObject()->Get((int)TexList::MainScreen_Syokuin_Rank_cost);
        p_obj->SetAnimationCount(p_param->GetPersonality()->id);
        this->_2DPolygon()->ObjectInput(p_obj);

        /* 特徴の固定化 */
        p_obj = this->_2DObject()->Get((int)TexList::MainScreen_Syokuin_Tokuchou);
        p_obj->SetAnimationCount(p_param->GetChaID()->id);
        this->_2DPolygon()->ObjectInput(p_obj);

        /* タイプの固定化 */
        p_obj = this->_2DObject()->Get((int)TexList::MainScreen_Syokuin_Type);
        p_obj->SetAnimationCount(p_param->GetTypeID()->id);
        this->_2DPolygon()->ObjectInput(p_obj);

        /* 既婚か独身かの固定化 */
        // 既婚
        if (p_param->GetPartner()->id == 1)
        {
            // frameの固定化
            p_obj = this->_2DObject()->Get((int)TexList::MainScreen_Syokuin_Type_Partner_true);
            p_obj->SetAnimationCount(3);
            this->_2DPolygon()->ObjectInput(p_obj);
        }
        // 未婚
        else if (p_param->GetPartner()->id == 0)
        {
            // frameの固定化
            p_obj = this->_2DObject()->Get((int)TexList::MainScreen_Syokuin_Type_Partner_false);
            p_obj->SetAnimationCount(3);
            this->_2DPolygon()->ObjectInput(p_obj);
        }
    }
}

//==========================================================================
// デバッグウィンドウ
void CGame_Syokuin::DebugWindow(void)
{
    if (this->ImGui()->MenuItem("シャインデータopen"))
    {
        this->Game_Syokuin_Data_Open();
    }
    if (this->ImGui()->MenuItem("シャインデータclause"))
    {
        this->Game_Syokuin_Data_Clause();
    }
}

//==========================================================================
// 出現/終了
// 戻り値 移動が完全に終了した際にtrue
bool CGame_Syokuin::Emergence(void)
{
    auto * pPos = this->_2DObject()->Get((int)TexList::MainScreen_Syokuin_Rirekisyo);
    float fSpeed = 0.0f;

    // 表示終了時
    if (this->m_EmergenceKey == false)
    {
        auto * pLimitPos = this->m_MasterPos.Get(0);
        fSpeed = this->_Hit()->Distance(*pPos->GetPos(), *pLimitPos)*0.2f;

        // 初期座標まで移送
        return this->UIMove(fSpeed, fSpeed);
    }
    // 表示開始時
    else if (this->m_EmergenceKey == true)
    {
        auto * pLimitPos = this->m_MasterPos.Get(1);
        fSpeed = this->_Hit()->Distance(*pPos->GetPos(), *pLimitPos)*0.2f;

        // 初期座標まで移送
        return this->UIMove(fSpeed, -fSpeed);
    }
    return false;
}

//==========================================================================
// UIの移動
bool CGame_Syokuin::UIMove(float fLimit, float fSpeed)
{
    // 初期座標まで移送
    if (0.01f <= fLimit)
    {
        for (int i = 0; i < this->_2DObject()->Size(); i++)
        {
            auto * pMoveObject = this->_2DObject()->Get(i);
            pMoveObject->SetXPlus(fSpeed);
        }
    }
    // 初期座標一致時、又は超えた場合
    else
    {
        return true;
    }
    return false;
}
