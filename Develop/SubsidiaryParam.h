//==========================================================================
// 子会社のパタメータ[SubsidiaryParam.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _SubsidiaryParam_H_
#define _SubsidiaryParam_H_

//==========================================================================
// include
//==========================================================================
#include <string>
#include "dxlib.h"
#include "FieldEditor.h"

//==========================================================================
//
// class  : CSubsidiaryParam
// Content: 子会社のパラメーター
//
//==========================================================================
class CSubsidiaryParam : public CRegionalData
{
public:
    CSubsidiaryParam()
    {
        this->m_employee_sub = 0;
        this->m_stock_price = 0.0f;
        this->m_income_default = 0;
        this->m_profit = 0;
        this->m_strName = "";
        this->m_textID = 0;
        this->m_income = 0;
        this->m_employee = 0;
        this->m_capital_city = false;
        this->m_prev = -1;
        this->m_next = -1;
        this->m_id = -1;
        this->m_pos.Init(0);
    }

    // employee 従業員
    // profit 売上
    // capital_city 首都判定
    CSubsidiaryParam(CFieldData & Data)
    {
        this->m_profit = 0;
        this->m_strName = "";
        this->m_textID = Data.m_param.m_textID;
        this->m_income = Data.m_param.m_income;
        this->m_employee = Data.m_param.m_employee;
        this->m_capital_city = Data.m_param.m_capital_city;
        this->m_prev = Data.m_param.m_prev;
        this->m_next = Data.m_param.m_next;
        this->m_id = Data.m_param.m_id;
        this->m_employee_sub = Data.m_param.m_employee;
        this->m_stock_price = 0.0f;
        this->m_income_default = Data.m_param.m_income;
        this->m_pos = Data.m_pos;
        this->m_head_office_player = Data.m_head_office_player;
        this->m_head_office_enemy = Data.m_head_office_enemy;
    }
    ~CSubsidiaryParam()
    {
        this->m_strName.clear();
    }

    // 更新
    // ptarget 比較対象のデータ
    void Update(const CSubsidiaryParam * ptarget);

    // タグのセット
    void Settag(const std::string & Inout) { this->m_strName = Inout; }
public:
    int m_employee_sub; // 視覚情報の社員数
    float m_stock_price; // シェア率
    int m_income_default; // デフォルトの売り上げ
    int m_profit; // 利益
    std::string m_strName; // 首都の名前
    C3DObject m_pos; // 座標
    bool m_head_office_player; // プレイヤーの本社(true)(警告:生データ)
    bool m_head_office_enemy; // エネミーの本社(true)(警告:生データ)

};

#endif // !_SubsidiaryParam_H_
