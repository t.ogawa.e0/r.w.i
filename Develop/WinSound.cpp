#include "WinSound.h"
#include "resource_list.h"


CWinSound::CWinSound()
{
    this->m_key = false;
}


CWinSound::~CWinSound()
{
}

bool CWinSound::Init(void)
{
    this->XAudio()->Init(CXAudio2::SoundLabel(RESOURCE_GameCon_win_wav, 0, 1.0f));
	return false;
}

void CWinSound::Uninit(void)
{
}

void CWinSound::Update(void)
{
    if (this->m_key == false)
    {
        this->XAudio()->Play(0);
        this->m_key = true;
    }
}

void CWinSound::Draw(void)
{
}



