#ifndef _WinImage_H_
#define _WinImage_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CWinImage
// Content: 勝利画面
//
//==========================================================================
class CWinImage : public CObject
{
public:
	CWinImage() :CObject(CObject::ID::Polygon2D) {}
	~CWinImage() {}
	// 初期化
	bool Init(void)override;

	// 解放
	void Uninit(void)override;

	// 更新
	void Update(void)override;

	// 描画
	void Draw(void)override;
private:
};

#endif // !_WinImage_H_