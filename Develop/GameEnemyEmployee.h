//==========================================================================
// エネミーの社員データ管理[GameEnemyEmployee.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "EmployeeManager.h"

//==========================================================================
//
// class  : CGameEnemyEmployee
// Content: エネミーの社員データ管理
//
//==========================================================================
class CGameEnemyEmployee : public CObject, public CEmployeeLink
{
public:
    CGameEnemyEmployee() :CObject(CObject::ID::Default) {
        this->m_LookEmployeeParam = nullptr;
        this->m_EmployeeManager = nullptr;
        this->m_WorldTime = nullptr;
        this->m_enemy = nullptr;
        this->m_field_pt = nullptr;
        this->m_start_script = nullptr;
        this->m_lookID = 0;
        this->m_id = 0;
        this->SetType(Type::Game_GameEnemyEmployee);
    }
    ~CGameEnemyEmployee() {
        this->m_task.clear();
    }
    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;

    // 処理taskの取得
    const std::list<CCEmployeeTask> * GetTaskData(void) {
        return &this->m_task;
    }

    // 雇用
    // _select シシャID
    void Employment(int _select);

    // 解雇
    // _select シシャID
    void Fire(int _select);

    // 転勤
    // _select シシャID
    void Transferred(int _select, int _next);

    // ストライキ
    void Strike(void);


    // 社員を選択
    // _select シシャID
    // ランダムで選択されます
    void to_see(int _select);
private:
    // 初期セットアップ
    void FastCreate(void);

    // 最短経路探索
    // _select シシャID
    CCEmployeeTask RootSearch(int _select);

    // データの移動
    void Move(void);
private:
    void * m_EmployeeManager; // 社員データ管理マネージャー
    void * m_enemy; // エネミーへのアクセス
    void * m_field_pt; // フィールドへのアクセス
    void * m_WorldTime; // ワールドタイム
    void * m_start_script; // スタート制御
    int m_lookID; // 見ているID
    int m_id; // 転勤元ID
    int m_dataIDManager; // データのID
    std::list<CCEmployeeTask>m_task; // 処理task
    EmployeeParam * m_LookEmployeeParam; // 見ている社員データ
};

// ※ プレーヤーの処理全コピ。
// 見る対象が違うので複製