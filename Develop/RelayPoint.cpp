//==========================================================================
// 中継地点[RelayPoint.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "RelayPoint.h"
#include "resource_link.hpp"

//==========================================================================
// 初期化
bool CRelayPoint::Init(void)
{
    return this->_3DXmodel()->Init(RESOURCE_keiro_x, CXmodel::LightMoad::System);
}

//==========================================================================
// 解放
void CRelayPoint::Uninit(void)
{
    this->release();
}

//==========================================================================
// 更新
void CRelayPoint::Update(void)
{
    this->_3DXmodel()->Update();
}

//==========================================================================
// 描画
void CRelayPoint::Draw(void)
{
    this->_3DXmodel()->Draw();
}

//==========================================================================
// セット
// prev = 前 
// next = 次
void CRelayPoint::Set(const C3DObject& prev, const C3DObject& next)
{
    // 距離の計算
    float fDistance = this->_Hit()->Distance(prev, next);
    int n_limit = (int)((fDistance + 0.5f)*resource_link::game_::f_relay_point_buff);
    float f_powor = (float)1 / (float)n_limit;

    for (int i = 1; i <= n_limit; i++)
    {
        this->SetPos(prev, next, fDistance*(i*f_powor));
    }
}

void CRelayPoint::SetPos(const C3DObject& prev, const C3DObject& next, float fDistance)
{
    auto * vp = this->_3DObject()->Create();

    // データの登録
    this->_3DXmodel()->ObjectInput(vp);

    // 初期化
    vp->Init(0);

    // 座標のセット
    vp->SetMatInfoPos(*prev.GetMatInfoPos());

    // 向きを変える
    for (int i = 0; i < 20; i++)
    {
        vp->LockOn(next, 1.0f);
    }

    // 離れている距離を速度として入力
    vp->MoveZ(fDistance);
}

//==========================================================================
// リセット
void CRelayPoint::Reset(void)
{
    this->_3DObject()->Release();
    this->_3DXmodel()->ObjectRelease();
}
