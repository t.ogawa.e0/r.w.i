//==========================================================================
// ゲームイベント:最低賃金[MinimumWageEvent.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "MinimumWageEvent.h"

//==========================================================================
// 初期化
void CMinimumWage::Init(void) 
{	
}

//==========================================================================
// 解放
void CMinimumWage::Uninit(void)
{
    this->m_target.clear();
    this->m_Name.clear();
}

//==========================================================================
// 更新
void CMinimumWage::Update(void) 
{ 
    this->m_endkey = true;
}

//==========================================================================
// 描画
void CMinimumWage::Draw(void) 
{
}

//==========================================================================
// UI更新
void CMinimumWage::UpdateUI(void) 
{
    // 親オブジェクトへのアクセス
    auto * p_obj = (CMobeEventObject*)this->m_master_obj;

    // オブジェクトの座標設定
    this->m_ObjectPos->SetPos(*p_obj->Get()->GetPos());
    this->m_NameObject->SetPos(*p_obj->Get()->GetPos());
}

//==========================================================================
// テキスト
void CMinimumWage::text(std::string strEventName)
{
    std::string strEvent = strEventName + this->m_Name;
    if (this->m_Imgui.NewMenu(strEvent.c_str(), true))
    {
        this->m_Imgui.Text("発動中");
        this->m_Imgui.Text("未実装です");
        this->m_Imgui.EndMenu();
    }
}