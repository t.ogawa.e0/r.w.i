//==========================================================================
// スカイドーム[Game_Skydome.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Game_Skydome.h"
#include "GameCamera.h"
#include "resource_list.h"

//=========================================================================
// 初期化
bool CGame_Skydome::Init(void)
{
    auto *pObje = this->_3DObject()->Create();

    pObje->Init(0);
    pObje->Scale(-100);

    this->_3DSphere()->ObjectInput(pObje);

    this->m_camera = this->GetObjects(ID::Camera, Type::Game_Camera);

    this->GetDirectX9Device()->GetDeviceCaps(&caps);

    //フォグの設定
    FLOAT StartPos = 0;  //開始位置
    FLOAT EndPos = 99; //終了位置
    this->GetDirectX9Device()->SetRenderState(D3DRS_FOGENABLE, TRUE); //フォグ：ON
    this->GetDirectX9Device()->SetRenderState(D3DRS_FOGCOLOR, colorr.get()); //白色で不透明
    this->GetDirectX9Device()->SetRenderState(D3DRS_FOGVERTEXMODE, D3DFOG_NONE);      //頂点モード
    this->GetDirectX9Device()->SetRenderState(D3DRS_FOGTABLEMODE, D3DFOG_LINEAR);     //テーブルモード
    this->GetDirectX9Device()->SetRenderState(D3DRS_FOGSTART, *(DWORD*)(&StartPos)); //開始位置
    this->GetDirectX9Device()->SetRenderState(D3DRS_FOGEND, *(DWORD*)(&EndPos));     //終了位置

    return this->_3DSphere()->Init(RESOURCE_sky_DDS, 30);
}

//=========================================================================
// 解放
void CGame_Skydome::Uninit(void)
{
    this->GetDirectX9Device()->SetRenderState(D3DRS_FOGENABLE, FALSE); //フォグ：ON
}

//=========================================================================
// 描画
void CGame_Skydome::Update(void)
{
    if (this->m_camera == nullptr)
    {
        this->m_camera = this->GetObjects(ID::Camera, Type::Game_Camera);
    }

    auto * p_camera = (CGameCamera*)this->m_camera;
    auto * _obj = p_camera->_3DObject()->Get((int)CGameCamera::E_Type::Kuroneko);

    this->_3DObject()->Get(0)->SetMatInfoPos(*_obj->GetMatInfoPos());

    this->_3DObject()->Get(0)->RotX(0.0013f);

    this->_3DSphere()->Update();
}

//=========================================================================
// 描画
void CGame_Skydome::Draw(void)
{
    this->_3DSphere()->Draw();
}
