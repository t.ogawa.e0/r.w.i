//==========================================================================
// 回想[ResultReminiscence.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CResultReminiscence
// Content: 回想
//
//==========================================================================
class CResultReminiscence : public CObject
{
public:
    CResultReminiscence();
    ~CResultReminiscence();
    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;
    
    // 開始
    void Start(void);
private:
    // 読み込み
    void Load(void);
private:
    std::unordered_map<int, std::string>m_sin_list; // 罪リスト
    std::list<int>m_log; // ログ
    bool m_key;
    CColor<int>m_color;
};

