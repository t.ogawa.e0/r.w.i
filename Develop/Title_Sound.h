//==========================================================================
// タイトル画面サウンド[Title_Sound.h]
// author: yoji watanabe
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CTitle_Sound
// Content: タイトル画面サウンド
//
//==========================================================================
class CTitle_Sound : public CObject
{
private:
	enum class SoundList
	{
		Title_BGM,
		Decide_SE,
		Move_SE,
	};
public:
	CTitle_Sound();
	~CTitle_Sound();

	// 初期化
	bool Init(void)override;

	// 解放
	void Uninit(void)override;

	// 更新
	void Update(void)override;

	// 描画
	void Draw(void)override;

	// タイトルBGM
	void PlayBGM(void);

	// 決定SE
	void PlayDecide_se(void);

	// カーソル移動SE
	void PlayMove_se(void);
private:
    bool m_key;
};