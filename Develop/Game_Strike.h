//==========================================================================
// ストライキ[Game_Strike.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CGame_Strike
// Content: ストライキ
//
//==========================================================================
class CGame_Strike : public CObject
{
private:
    class CDATA
    {
    public:
        CDATA() {
            this->m_obj_main.Init(0);
            this->m_obj_effect.Init(1, 2, 10, 5);
            this->m_obj_font.Init(2);
            this->m_time.Init(1, 30);
            this->m_end_time.Init(1, 30);
            this->m_endkey = false;
        }
        ~CDATA() {}

    public:
        C2DObject m_obj_main;
        C2DObject m_obj_effect;
        C2DObject m_obj_font;
        CVector4<float>m_stop_pos;
        CVector4<float>m_end_pos;
        CTimer m_time; // 表示カウンタ
        CTimer m_end_time; // 表示カウンタ
        bool m_endkey; // 終了判定
    };
public:
    CGame_Strike();
    ~CGame_Strike();
    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;

    // 生成
    void Create(void);
private:
    // 座標初期化
    void initializer(void);
private:
    CObjectVectorManager<CDATA> m_data;
    void * m_start_script; // スタート制御
    void * m_sound; //罪深いサウンド
};


