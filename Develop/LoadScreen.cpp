//==========================================================================
// ロードスクリーン[LoadScreen.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "LoadScreen.h"

//==========================================================================
// 初期化
bool CLoadScreen::Init(void)
{
    CTexvec<int> vwinsize = CTexvec<int>(0, 0, this->GetWinSize().m_Width, this->GetWinSize().m_Height);
    CVector4<float> vpos = CVector4<float>(0, 0, 0, 0);
    CTexvec<int> *ptexsize = nullptr;

    // テクスチャの確保
    if (this->_2DPolygon()->Init()) { return true; }
    if (this->_2DPolygon()->Init("resource/texture/LoadTex.DDS")) { return true; }
    if (this->_2DPolygon()->Init("resource/texture/NowLoading.DDS")) { return true; }

    //==========================================================================
    // 背景
    this->_2DPolygon()->SetTexSize(0, vwinsize.w, vwinsize.h);
    this->_2DObject()->Create()->Init(0);
    this->_2DObject()->Get(0)->SetColor(0, 0, 0, 255);
    this->_2DPolygon()->ObjectInput(this->_2DObject()->Get(0));

    //==========================================================================
    // リング
    ptexsize = this->_2DPolygon()->GetTexSize(1);
    CTexvec<float> texsize = CTexvec<float>(0, 0, (float)ptexsize->w, (float)ptexsize->h);
    texsize *= 0.1f;
    *ptexsize = CTexvec<int>(0, 0, (int)texsize.w, (int)texsize.h);

    vpos = CVector4<float>((float)vwinsize.w - (ptexsize->w*0.5f), (float)vwinsize.h - (ptexsize->h*0.5f), 0, 0);
    this->_2DObject()->Create()->Init(1);
    this->_2DObject()->Get(1)->SetCentralCoordinatesMood(true);
    this->_2DObject()->Get(1)->SetColor(255, 255, 0, 255);
    this->_2DObject()->Get(1)->SetPos(vpos);
    this->_2DPolygon()->ObjectInput(this->_2DObject()->Get(1));

    //==========================================================================
    // ロードフォント
    ptexsize = this->_2DPolygon()->GetTexSize(2);
    texsize = CTexvec<float>(0, 0, (float)ptexsize->w, (float)ptexsize->h);
    texsize *= 0.1f;
    *ptexsize = CTexvec<int>(0, 0, (int)texsize.w, (int)texsize.h);

    this->m_paramload.m_a = 255;
    this->m_paramload.m_Change = false;
    this->m_paramload.m_a = 100;

    this->_2DObject()->Create()->Init(2);
    this->_2DObject()->Get(2)->SetCentralCoordinatesMood(true);
    this->_2DObject()->Get(2)->SetColor(255, 255, 0, this->m_paramload.m_a);
    this->_2DObject()->Get(2)->SetPos(vpos);
    this->_2DPolygon()->ObjectInput(this->_2DObject()->Get(2));

    return false;
}

//==========================================================================
// 解放
void CLoadScreen::Uninit(void)
{
    this->_2DPolygon()->Release();
    this->_2DObject()->Release();
}

//==========================================================================
// 更新
void CLoadScreen::Update(void)
{
    // 現在時刻の取得
    this->m_NewTime = timeGetTime();

    // 前回との差を取得
    DWORD ___time = this->m_NewTime - this->m_OldTime;

    //時間渡し
    this->m_OldTime = this->m_NewTime;

    // 差を回転力に変換
    this->_2DObject()->Get(1)->Angle(___time*0.0025f);

    this->Change((int)(___time*0.5f));
    this->_2DObject()->Get(2)->SetColor(255, 255, 0, this->m_paramload.m_a);

    this->_2DPolygon()->Update();
}

//==========================================================================
// 描画
void CLoadScreen::Draw(void)
{
    LPDIRECT3DDEVICE9 pDevice = this->GetDirectX9Device();

    // 画質を高画質にする
    pDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR); // 小さくなった時に白枠
    pDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR); // 常に白枠
    pDevice->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_NONE); // 元のサイズより小さい時綺麗にする

                                                                  // Zバッファの無効
    pDevice->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);

    this->_2DPolygon()->Draw();

    // Zバッファの有効化
    pDevice->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);

    // 通常の画質に戻す
    pDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR); // 小さくなった時に白枠
    pDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR); // 常に白枠
    pDevice->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR); // 元のサイズより小さい時綺麗にする}
}

//==========================================================================
// αチェンジ
void CLoadScreen::Change(int Speed)
{
    // チェンジフラグ
    if (this->m_paramload.m_Change)
    {
        this->m_paramload.m_a -= Speed;
        if (this->m_paramload.m_a <= 0)
        {
            this->m_paramload.m_a = 0;
            this->m_paramload.m_Change = false;
        }
    }
    else
    {
        this->m_paramload.m_a += Speed;
        if (255 <= this->m_paramload.m_a)
        {
            this->m_paramload.m_a = 255;
            this->m_paramload.m_Change = true;
        }
    }
}
