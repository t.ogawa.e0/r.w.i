//==========================================================================
// ガチャUI[Gacha_UI_Draw.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Gacha_UI_Draw.h"
#include "Game_Syokuin.h"
#include "CharacterAnimation.h"
#include "resource_list.h"

template<> struct enum_system::calculation<CGacha_UI_Draw::TexList> : std::true_type {};

//==========================================================================
// 初期化
bool CGacha_UI_Draw::Init(void)
{
    const char * c_texlist[] = {
        RESOURCE_MainScreen_Syokuin_Rirekisyo_Gacha_DDS,
        RESOURCE_MainScreen_Syokuni_Name_DDS,
        RESOURCE_MainScreen_syoumeisyashin_DDS,
        RESOURCE_MainScreen_Syokuin_Nenrei_DDS,
        RESOURCE_MainScreen_Syokuin_Kokuseki_DDS,
        RESOURCE_MainScreen_Syokuin_Seikaku_DDS,
        RESOURCE_MainScreen_Syokuin_Tokuchou_DDS,
        RESOURCE_MainScreen_Syokuin_Type_DDS,
        RESOURCE_MainScreen_Syokuin_Type_DDS,
        RESOURCE_MainScreen_Syokuin_Type_DDS,
    };
    
    if (this->_2DPolygon()->Init(c_texlist, this->Helper()->Sizeof_(c_texlist), true))
    {
        return true;
    }

    this->m_EmployeeGatya = this->GetObjects(ID::Polygon2D, Type::Game_Syokuin);
    this->m_TransferAnimation = this->GetObjects(ID::Polygon2D, Type::Game_CharacterAnimation);

    // オブジェクトの取得に成功
    if (this->m_TransferAnimation != nullptr)
    {
        auto * p_traobj = (CCharacterAnimation*)this->m_TransferAnimation;
        p_traobj->SetDrawObject(this->_2DPolygon());
    }

    return false;
}

//==========================================================================
// 解放
void CGacha_UI_Draw::Uninit(void)
{
    this->m_GachaObject.Release();
}

//==========================================================================
// 更新
void CGacha_UI_Draw::Update(void)
{
    if (this->m_EmployeeGatya == nullptr)
    {
        this->m_EmployeeGatya = this->GetObjects(ID::Polygon2D, Type::Game_Syokuin);
    }

    if (this->m_TransferAnimation == nullptr)
    {
        this->m_TransferAnimation = this->GetObjects(ID::Polygon2D, Type::Game_CharacterAnimation);
        // オブジェクトの取得に成功
        if (this->m_TransferAnimation != nullptr)
        {
            auto * p_traobj = (CCharacterAnimation*)this->m_TransferAnimation;
            p_traobj->SetDrawObject(this->_2DPolygon());
        }
    }

    for (int i = 0; i < this->m_GachaObject.Size(); i++)
    {
        this->m_GachaObject.Get(i)->SetParameter(this->_2DPolygon());
    }

    // オブジェクトが存在するとき
    if (this->m_EmployeeGatya != nullptr)
    {
        // 破棄判定
        for (int i_s = 0; i_s < this->m_GachaObject.Size(); i_s++)
        {
            auto * p_obj = this->m_GachaObject.Get(i_s);
            if (p_obj->m_tim_delete.GetComma() == 0)
            {
                for (int i = 0; i < p_obj->GetGachaUI()->Size(); i++)
                {
                    this->_2DPolygon()->ObjectDelete(p_obj->GetGachaUI()->Get(i));
                }
                this->m_GachaObject.PinpointRelease(i_s);
                break;
            }
        }
    }

    this->_2DPolygon()->Update();
}

//==========================================================================
// 描画
void CGacha_UI_Draw::Draw(void)
{
    this->_2DPolygon()->Draw();
}

//==========================================================================
// 生成
void CGacha_UI_Draw::Create(EmployeeParam * _data)
{
    auto * p_obj = this->m_GachaObject.Create();

    p_obj->initializer(this->GetWinSize(), this->_2DPolygon(), _data);

    for (int i = 0; i < p_obj->GetGachaUI()->Size(); i++)
    {
        this->_2DPolygon()->ObjectInput(p_obj->GetGachaUI()->Get(i));
    }

    p_obj->InputPos();
}

void CGacha_UI_Draw::CUI::initializer(CDirectXDevice::CWindowsSize<int> _winsize, C2DPolygon * p_poly, EmployeeParam * _data)
{
    //==========================================================================
    // 宣言
    //==========================================================================
    CVector4<float>vpos4;
    CVector2<float>vpos2;
    C2DObject * pos = nullptr;

    this->m_winsize = CVector2<int>(_winsize.m_Width, _winsize.m_Height);

    //==========================================================================
    // シシャデータ表示用背景(共通)
    //==========================================================================

    this->m_data = _data;

    // 2Dオブジェクト生成
    pos = this->m_2DObject.Create();

    // 初期化
    pos->Init((int)TexList::MainScreen_Syokuin_Rirekisyo);

    // 親オブジェクトの終点座標指定
    this->m_MasterTexSize = *p_poly->GetTexSize((int)TexList::MainScreen_Syokuin_Rirekisyo);
    this->m_MasterPos = CVector4<float>((float)this->m_winsize.x - (float)this->m_MasterTexSize.w, (float)p_poly->GetTexSize((int)TexList::Begin)->h);

    // 表示座標の設定
    vpos4 = CVector4<float>(this->m_MasterPos.x, this->m_MasterPos.y);

    // 表示座標の登録
    pos->SetPos(vpos4);

    //==========================================================================
    // ここまでが背景(共通)
    //==========================================================================

    //==========================================================================
    // 名前オブジェクト ここから
    //==========================================================================

    // 2Dオブジェクト生成
    pos = this->m_2DObject.Create();

    // 初期化
    pos->Init((int)TexList::MainScreen_Syokuni_Name, 1, 50, 5);

    /*
    表示座標の設定
    ※定数禁止！
    必ずウィンドウサイズから座標を生成しよう！

    this->GetWinSize(); // ウィンドウサイズ取得関数
    */

    vpos4 = CVector4<float>(this->m_winsize.x / 2.0f * 1.52f, this->m_winsize.y / 2.0f * 0.615f);

    // 表示座標の登録
    pos->SetPos(vpos4);

    //==========================================================================
    // 名前オブジェクト ここまで
    //==========================================================================

    //==========================================================================
    // アイコンオブジェクト ここから
    //==========================================================================

    // 2Dオブジェクト生成
    pos = this->m_2DObject.Create();

    // 初期化
    pos->Init((int)TexList::MainScreen_syoumeisyashin, 1, 19, 5);

    /*
    表示座標の設定
    ※定数禁止！
    必ずウィンドウサイズから座標を生成しよう！

    this->GetWinSize(); // ウィンドウサイズ取得関数
    */
    vpos4 = CVector4<float>(((this->m_winsize.x / 2.0f) * 1.52f) + 2, ((this->m_winsize.y / 2.0f) * 0.7f) + 8);

    // 表示座標の登録
    pos->SetPos(vpos4);

    //==========================================================================
    // アイコンオブジェクト ここまで
    //==========================================================================

    //==========================================================================
    // 年齢オブジェクト ここから
    //==========================================================================

    // 2Dオブジェクト生成
    pos = this->m_2DObject.Create();

    // 初期化
    pos->Init((int)TexList::MainScreen_Syokuin_Rank_skill, 1, 13, 2);

    /*
    表示座標の設定
    ※定数禁止！
    必ずウィンドウサイズから座標を生成しよう！

    this->GetWinSize(); // ウィンドウサイズ取得関数
    */
    vpos4 = CVector4<float>(this->m_winsize.x / 2.0f * 1.80f, this->m_winsize.y / 2.0f * 0.76f);

    // 表示座標の登録
    pos->SetPos(vpos4);

    //==========================================================================
    // 年齢オブジェクト ここまで
    //==========================================================================

    //==========================================================================
    // 国籍オブジェクト ここから
    //==========================================================================

    // 2Dオブジェクト生成
    pos = this->m_2DObject.Create();

    // 初期化
    pos->Init((int)TexList::MainScreen_Syokuin_Rank_mental, 1, 8, 1);

    /*
    表示座標の設定
    ※定数禁止！
    必ずウィンドウサイズから座標を生成しよう！

    this->GetWinSize(); // ウィンドウサイズ取得関数
    */
    vpos4 = CVector4<float>(this->m_winsize.x / 2.0f * 1.80f, this->m_winsize.y / 2.0f * 0.865f);

    // 表示座標の登録
    pos->SetPos(vpos4);

    //==========================================================================
    // 国籍オブジェクト ここまで
    //==========================================================================

    //==========================================================================
    // 性格オブジェクト ここから
    //==========================================================================

    // 2Dオブジェクト生成
    pos = this->m_2DObject.Create();

    // 初期化
    pos->Init((int)TexList::MainScreen_Syokuin_Rank_cost, 1, 9, 1);

    /*
    表示座標の設定
    ※定数禁止！
    必ずウィンドウサイズから座標を生成しよう！

    this->GetWinSize(); // ウィンドウサイズ取得関数
    */
    vpos4 = CVector4<float>(this->m_winsize.x / 2.0f * 1.80f, this->m_winsize.y / 2.0f * 0.965f);

    // 表示座標の登録
    pos->SetPos(vpos4);

    //==========================================================================
    // 性格オブジェクト ここまで
    //==========================================================================

    //==========================================================================
    // 特徴オブジェクト ここから
    //==========================================================================

    // 2Dオブジェクト生成
    pos = this->m_2DObject.Create();

    // 初期化
    pos->Init((int)TexList::MainScreen_Syokuin_Tokuchou, 1, 8, 1);

    /*
    表示座標の設定
    ※定数禁止！
    必ずウィンドウサイズから座標を生成しよう！

    this->GetWinSize(); // ウィンドウサイズ取得関数
    */
    vpos4 = CVector4<float>(this->m_winsize.x / 2.0f * 1.71f, this->m_winsize.y / 2.0f * 1.055f);

    // 表示座標の登録
    pos->SetPos(vpos4);

    //==========================================================================
    // 特徴オブジェクト ここまで
    //==========================================================================

    //==========================================================================
    // タイプオブジェクト ここから
    //==========================================================================

    // 2Dオブジェクト生成
    pos = this->m_2DObject.Create();

    // 初期化
    pos->Init((int)TexList::MainScreen_Syokuin_Type, 1, 4, 4);

    /*
    表示座標の設定
    ※定数禁止！
    必ずウィンドウサイズから座標を生成しよう！

    this->GetWinSize(); // ウィンドウサイズ取得関数
    */
    vpos4 = CVector4<float>(this->m_winsize.x / 2.0f * 1.49f, this->m_winsize.y / 2.0f * 0.95f);

    // 表示座標の登録
    pos->SetPos(vpos4);

    //==========================================================================
    // タイプオブジェクト ここまで
    //==========================================================================

    //==========================================================================
    // パートナーがいるブジェクト ここから
    //==========================================================================

    // 2Dオブジェクト生成
    pos = this->m_2DObject.Create();

    // 初期化
    pos->Init((int)TexList::MainScreen_Syokuin_Type_Partner_true, 1, 4, 4);

    /*
    表示座標の設定
    ※定数禁止！
    必ずウィンドウサイズから座標を生成しよう！

    this->GetWinSize(); // ウィンドウサイズ取得関数
    */
    vpos4 = CVector4<float>(this->m_winsize.x / 2.0f * 1.72f, this->m_winsize.y / 2.0f * 0.57f);

    // 表示座標の登録
    pos->SetPos(vpos4);

    //==========================================================================
    // パートナーがいるブジェクト ここまで
    //==========================================================================

    //==========================================================================
    // パートナーがいないブジェクト ここから
    //==========================================================================

    // 2Dオブジェクト生成
    pos = this->m_2DObject.Create();

    // 初期化
    pos->Init((int)TexList::MainScreen_Syokuin_Type_Partner_false, 1, 4, 4);

    /*
    表示座標の設定
    ※定数禁止！
    必ずウィンドウサイズから座標を生成しよう！

    this->GetWinSize(); // ウィンドウサイズ取得関数
    */

    vpos4 = CVector4<float>(this->m_winsize.x / 2.0f * 1.85f, this->m_winsize.y / 2.0f * 0.57f);

    // 表示座標の登録
    pos->SetPos(vpos4);

    //==========================================================================
    // パートナーがいないオブジェクト ここまで
    //==========================================================================

}

void CGacha_UI_Draw::CUI::SetParameter(C2DPolygon * p_poly)
{
    /* 描画オブジェクトに対して必要な情報を与える処理 */
    C2DObject * p_obj = nullptr; // 2Dオブジェクト

    // 更新前にオブジェクトを破棄
    for (TexList i = TexList::Begin; i < TexList::End; ++i)
    {
        p_poly->ObjectDelete(this->m_2DObject.Get((int)i));
    }

    /* 名前の固定化 */
    p_obj = this->m_2DObject.Get((int)TexList::MainScreen_Syokuin_Rirekisyo);
    p_poly->ObjectInput(p_obj);

    /* 名前の固定化 */
    p_obj = this->m_2DObject.Get((int)TexList::MainScreen_Syokuni_Name);
    p_obj->SetAnimationCount(this->m_data->GetName()->id);
    p_poly->ObjectInput(p_obj);

    /* アイコンの固定化 */
    p_obj = this->m_2DObject.Get((int)TexList::MainScreen_syoumeisyashin);
    p_obj->SetAnimationCount(this->m_data->GetIcoFrameID());
    p_poly->ObjectInput(p_obj);

    /* スキルランクの固定化 */
    p_obj = this->m_2DObject.Get((int)TexList::MainScreen_Syokuin_Rank_skill);
    p_obj->SetAnimationCount(this->m_data->GetAge()->id);
    p_poly->ObjectInput(p_obj);

    /* メンタルの固定化 */
    p_obj = this->m_2DObject.Get((int)TexList::MainScreen_Syokuin_Rank_mental);
    p_obj->SetAnimationCount(this->m_data->GetCountry()->id);
    p_poly->ObjectInput(p_obj);

    /* コストの固定化 */
    p_obj = this->m_2DObject.Get((int)TexList::MainScreen_Syokuin_Rank_cost);
    p_obj->SetAnimationCount(this->m_data->GetPersonality()->id);
    p_poly->ObjectInput(p_obj);

    /* 特徴の固定化 */
    p_obj = this->m_2DObject.Get((int)TexList::MainScreen_Syokuin_Tokuchou);
    p_obj->SetAnimationCount(this->m_data->GetChaID()->id);
    p_poly->ObjectInput(p_obj);

    /* タイプの固定化 */
    p_obj = this->m_2DObject.Get((int)TexList::MainScreen_Syokuin_Type);
    p_obj->SetAnimationCount(this->m_data->GetTypeID()->id);
    p_poly->ObjectInput(p_obj);

    /* 既婚か独身かの固定化 */
    // 既婚
    if (this->m_data->GetPartner()->id == 1)
    {
        // frameの固定化
        p_obj = this->m_2DObject.Get((int)TexList::MainScreen_Syokuin_Type_Partner_true);
        p_obj->SetAnimationCount(3);
        p_poly->ObjectInput(p_obj);
    }
    // 未婚
    else if (this->m_data->GetPartner()->id == 0)
    {
        // frameの固定化
        p_obj = this->m_2DObject.Get((int)TexList::MainScreen_Syokuin_Type_Partner_false);
        p_obj->SetAnimationCount(3);
        p_poly->ObjectInput(p_obj);
    }

    this->Move();
    this->m_tim.Countdown();
}

// 雇用時の座標
void CGacha_UI_Draw::CUI::InputPos(void)
{
    CVector4<float> vois = CVector4<float>((float)this->m_MasterTexSize.w, 0);
    for (TexList i = TexList::Begin; i < TexList::End; ++i)
    {
        this->m_2DObject.Get((int)i)->SetPosPlus(vois);
    }

    auto p_vpos = *this->m_2DObject.Get((int)TexList::MainScreen_Syokuin_Rirekisyo)->GetPos();
    CVector4<float> vec1 = CVector4<float>(0.0f, (float)this->m_winsize.y);
    vec1.y = p_vpos.y;
    float f_speed = this->m_hit.Distance(p_vpos, vec1) + (float)this->m_MasterTexSize.w;

    for (TexList i = TexList::Begin; i < TexList::End; ++i)
    {
        this->m_2DObject.Get((int)i)->SetXPlus(-f_speed);
    }
}

// 解雇時の座標
void CGacha_UI_Draw::CUI::OutPos(CObjectVectorManager<C2DObject> * Input)
{
    for (int i = 0, s = 0; i < Input->Size(); i++)
    {
        if (i != (int)CGame_Syokuin::TexList::MainScreen_Syokuin_Date_tab)
        {
            auto * vec1 = Input->Get(i)->GetPos();
            this->m_2DObject.Get(s)->SetPos(*vec1);
            s++;
        }
    }
}

// 移動
void CGacha_UI_Draw::CUI::Move(void)
{
    if (this->m_tim.GetComma() != 0)
    {
        auto p_vpos = *this->m_2DObject.Get((int)TexList::MainScreen_Syokuin_Rirekisyo)->GetPos();
        CVector4<float> vec1 = CVector4<float>((float)(this->m_winsize.x / 2.0f) - ((this->m_MasterTexSize.w / 2.0f)), (float)this->m_winsize.y);
        vec1.y = p_vpos.y;
        float f_speed = this->m_hit.Distance(p_vpos, vec1)*0.2f;

        for (TexList i = TexList::Begin; i < TexList::End; ++i)
        {
            this->m_2DObject.Get((int)i)->SetXPlus(f_speed);
        }
    }

    if (this->m_tim.GetComma() == 0)
    {
        auto p_vpos = *this->m_2DObject.Get((int)TexList::MainScreen_Syokuin_Rirekisyo)->GetPos();
        CVector4<float> vec1 = CVector4<float>((float)(this->m_winsize.x), (float)this->m_winsize.y);
        vec1.y = p_vpos.y;
        float f_speed = this->m_hit.Distance(p_vpos, vec1)*0.1f;

        for (TexList i = TexList::Begin; i < TexList::End; ++i)
        {
            this->m_2DObject.Get((int)i)->SetXPlus(f_speed);
        }
        this->m_tim_delete.Countdown();
    }
}
