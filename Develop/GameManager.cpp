//==========================================================================
// ゲームマネージャー[GameManager.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "GameManager.h"
#include "Player.h"
#include "Enemy.h"
#include "GameEnd.h"
#include "Worldtime.h"

//==========================================================================
// 更新処理
void CGameManager::Update()
{
	this->Updateturn();
	this->m_time.Count();
}

//==========================================================================
// ターン処理
int CGameManager::Updateturn(void)
{
	this->m_turns.m_old = this->m_turns.m_current;
	this->m_turns.m_current = (int)(this->m_time.GetTime() / this->m_turntime);
	this->m_turns.m_current++;
    return this->m_turns.m_current;
}

//==========================================================================
// 初期化
bool CGame_Manager::Init(void)
{
    this->m_player = this->GetObjects(ID::Default, Type::Game_Player);
    this->m_enemy = this->GetObjects(ID::Default, Type::Game_Enemy);
    this->m_end_script = this->GetObjects(ID::Default, Type::Game_End);
    this->m_world_time = this->GetObjects(ID::Default, Type::Game_WorldTime);
    return false;
}

//==========================================================================
// 解放
void CGame_Manager::Uninit(void)
{
}

//==========================================================================
// 更新
void CGame_Manager::Update(void)
{
    if (this->m_player == nullptr)
    {
        this->m_player = this->GetObjects(ID::Default, Type::Game_Player);
    }
    if (this->m_enemy == nullptr)
    {
        this->m_enemy = this->GetObjects(ID::Default, Type::Game_Enemy);
    }
    if (this->m_end_script == nullptr)
    {
        this->m_end_script = this->GetObjects(ID::Default, Type::Game_End);
    }
    if (this->m_world_time == nullptr)
    {
        this->m_world_time = this->GetObjects(ID::Default, Type::Game_WorldTime);
    }

    // オブジェクトが存在するとき
    if (this->m_player != nullptr&&this->m_enemy != nullptr&&this->m_end_script != nullptr&&this->m_world_time != nullptr)
    {
        auto * p_player = (CPlayer*)this->m_player;
        auto * p_enemyr = (CEnemy*)this->m_enemy;
        auto * p_game_end = (CGameEnd*)this->m_end_script;
        auto * p_world_time = (CWorldTime*)this->m_world_time;

        // プレイヤーが破産
        if (p_player->GetAsset() <= 0)
        {
            p_player->GetPlayerParam().m_asset = 0;
            p_game_end->EndKey(EGameEndKey::LOSE);
        }
        // エネミーが破産
        if (p_enemyr->GetAsset() <= 0)
        {
            p_enemyr->GetPlayerParam().m_asset = 0;
            p_game_end->EndKey(EGameEndKey::WIN);
        }

        // プレイ可能ターンを超えた場合
        if (this->m_Limit_turn < p_world_time->getturns().m_current)
        {
            // エネミーが稼いだ
            if (p_player->GetAsset() < p_enemyr->GetAsset())
            {
                p_game_end->EndKey(EGameEndKey::LOSE);
            }
            // プレイヤーが稼いだ
            if (p_enemyr->GetAsset() < p_player->GetAsset())
            {
                p_game_end->EndKey(EGameEndKey::WIN);
            }
        }
    }
}

//==========================================================================
// 描画
void CGame_Manager::Draw(void)
{
}
