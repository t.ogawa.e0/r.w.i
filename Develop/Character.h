//==========================================================================
// ゲームキャラクター継承用[Character.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _Character_H_
#define _Character_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "PlayerParam.h"
#include "SubsidiaryParam.h"
#include "Subsidiary.h"
#include "CharList.h"
#include "GameManager.h"
#include <vector>
#include <string>

//==========================================================================
//
// class  : CCharacter
// Content: キャラクターデータ継承用
//
//==========================================================================
class CCharacter : public CCharList
{
protected:
    CCharacter() {
        this->m_charID = (CharList)-1;
        this->m_charcase.emplace_back(this);
        this->m_psub = nullptr;
        this->m_strike_key = false;
	}
	virtual ~CCharacter() { this->m_charcase.clear(); }

	// 継承初期化
	virtual bool Init(void) = 0;

	// 継承解放
	virtual void Uninit(void) = 0;

	// 継承更新
	virtual void Update(void) = 0;

	// 継承描画
	virtual void Draw(void) = 0;

	// キャラのタイプセット
	void SetCharacterType(CharList type) { this->m_charID = type; }

	// タイプのゲッター
	CharList GetCharacterType(void) { return this->m_charID; }

	// テキスト設定
	void Text(const std::string & Inout);

	// 社員一揆の処理
	bool Strike(void);

	// 不満度の処理
	// ntime 時間
	void Dissatisfied(int ntime);

	// パラメーターの更新
	// InputTime タイマー
	void UpdateParam(const CGameManager::TURNS_t & InputTime);

	// 雇用
	// pSub 見ている場所
	// NumEmployment 人数 
	void Employment(CSubsidiaryParam * pSub, int NumEmployment);

	// 解雇
	// pSub 見ている場所
	// NumEmployment 人数 
	void Fire(CSubsidiaryParam * pSub, int NumEmployment);

	// 転勤
	// pNow 今の場所
	// pNext 移動先
	// NumEmployment 人数
    // 戻り値 : true ならnextルート、falseならprev
    bool Transferred(CSubsidiaryParam * pNow, CSubsidiaryParam * pNext, CSubsidiaryManager * InputPlayeData, int NumEmployment);

	// 賃金 未実装 
	void Wage(void);
private:
    // ルート探索
    int RootSearch(CSubsidiaryParam * pNow, CSubsidiaryParam * pNext, CSubsidiaryManager * InputPlayeData, bool & pOutNextFlag);
public:
    // ストライキ判定の取得
    bool GetStrikeKey(void) {
        return this->m_strike_key;
    }

    CPlayerParam & GetPlayerParam(void) { return this->m_param; }

	// キャラクターのゲッター
	static CCharacter* Get(CharList Input) { return m_charcase[(int)Input]; }

	// 解放
	static void ReleaseAll(void) { m_charcase.clear(); }

    // 子会社へのアクセスルート
    CSubsidiaryManager * GetCubsidiaryData(void) {
        return this->m_psub;
    }
protected:
	CPlayerParam m_param; // データの管理
	CTemplates m_temp; // template
	CImGui_Dx9 m_imgui; // imgui
	CSubsidiaryManager * m_psub; // 子会社へのアクセスルート
    bool m_strike_key;
private:
	static std::vector<CCharacter*> m_charcase; // キャラクターの登録
	int m_oldtime; // 古い時間
    CharList m_charID; // キャラID
};

#endif // !_Character_H_
