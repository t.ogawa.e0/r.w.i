//==========================================================================
// Enemy[Enemy.cpp]
// author: yoji watanabe
//==========================================================================
#include "Player.h"
#include "Enemy.h"
#include "worldtime.h"
#include "Subsidiary.h"
#include "GameEnemyEmployee.h"
#include "Game_Start.h"
#include "resource_link.hpp"

constexpr int __limit_turn = 5; // 待ち時間最大時間

//==========================================================================
// 初期化
bool CEnemy::Init(void)
{
    this->SetCharacterType(CharList::Enemy);
    this->m_psub = CSubsidiary::Get(CharList::Enemy);

    // mainデータ初期化
    this->m_param = CPlayerParam(20 * this->m_psub->GetMaxNemployee(), this->m_psub->GetNumSub(), this->m_minWage);

    // 総従業員に加算
    this->m_param.m_total_employee = this->m_psub->GetMaxNemployee();

    this->m_actionID[0] = CEnemy::actionID::NONE;
    this->m_actionID[1] = CEnemy::actionID::NONE;

    // オブジェクトの先行呼び出し
    this->m_WorldTime = (CWorldTime*)this->GetObjects(CObject::ID::Default, CObject::Type::Game_WorldTime);

    this->m_GameEnemyEmployee = this->GetObjects(CObject::ID::Default, CObject::Type::Game_GameEnemyEmployee);
    this->m_start_script = this->GetObjects(ID::Polygon2D, Type::Game_Start_Object);

    //レベル・任期ロード
    this->m_enemyLevel = CTitleData::LoadEnemyLevel();
    this->m_Term = CTitleData::LoadTerm();

    // 子会社数取得
    m_NumSub = this->m_psub->GetNumSub();

    // 本社番号取得
    for (int i = 0; i < m_NumSub; i++)
    {
        if (this->m_psub->GetSub(i)->m_head_office_enemy == true)
        {
            m_headNum = i;
            break;
        }
    }

    //配置人数初期化
    for (int i = 0; i < m_NumSub; i++)
    {
        this->m_numEmployee[i] = this->m_psub->GetSub(i)->m_employee;
    }
    m_numemployment = 1;

    this->m_turn = 0;

    return false;
}

//==========================================================================
// 解放
void CEnemy::Uninit(void)
{
}

//==========================================================================
// 更新
void CEnemy::Update(void)
{
    if (this->m_WorldTime == nullptr)
    {
        this->m_WorldTime = (CWorldTime*)this->GetObjects(CObject::ID::Default, CObject::Type::Game_WorldTime);
    }

    if (this->m_GameEnemyEmployee == nullptr)
    {
        this->m_GameEnemyEmployee = this->GetObjects(CObject::ID::Default, CObject::Type::Game_GameEnemyEmployee);
    }

    if (this->m_start_script == nullptr)
    {
        this->m_start_script = this->GetObjects(ID::Polygon2D, Type::Game_Start_Object);
    }

    auto * p_start = (CGame_Start*)this->m_start_script;

    // オブジェクトが存在するとき
    if (p_start != nullptr)
    {
        if (p_start->Start())
        {
            auto*pTime = (CWorldTime*)this->m_WorldTime;

            // オブジェクトが存在するとき
            if (pTime != nullptr)
            {
                // ワールド時間の更新が入ったときに処理が発生する。
                this->UpdateParam(pTime->getturns());

                // 不満度の処理
                this->Dissatisfied(pTime->getturns2()->GetTime());

                if (pTime->getturns().m_current != pTime->getturns().m_old)
                {
                    // エネミーの行動
                    if (__limit_turn <= this->m_turn)
                    {
                        this->action(pTime->getturns());
                        this->m_turn = 0;
                    }
                    else
                    {
                        this->m_turn++;
                    }
                }
            }

            // 社員一揆の処理
            this->Strike();

            // テキストセット
            this->Text("エネミーの情報");

            // データの補正
            this->Correction();
        }
    }
}

//==========================================================================
// 描画
void CEnemy::Draw(void)
{
}

// エネミーの行動
void CEnemy::action(const CGameManager::TURNS_t & InputTime)
{
    if (InputTime.m_current != InputTime.m_old)
    {
        auto*p_GameEnemyEmployee = (CGameEnemyEmployee*)this->m_GameEnemyEmployee;

        //レベル分の行動を行う
        for (int actionCount = 0; actionCount < (int)m_enemyLevel + 1; actionCount++)
        {
            //思考関数呼び出し
            this->selectTarget();

            switch (this->m_actionID[0])
            {
            case actionID::NONE:
                break;

            case actionID::FIRE:
                this->Fire(this->m_psub->GetSub(this->m_targetNum), this->m_numemployment);
                this->m_numEmployee[m_targetNum] -= m_numemployment;

                // 社員パラメータの社員を解雇
                for (int i = 0; i < this->m_numemployment; i++)
                {
                    // オブジェクトが存在するとき
                    if (p_GameEnemyEmployee != nullptr)
                    {
                        p_GameEnemyEmployee->to_see(this->m_targetNum);
                        p_GameEnemyEmployee->Fire(this->m_targetNum);
                    }
                }
                break;

            case actionID::EMPLOYMENT:
                this->Employment(this->m_psub->GetSub(m_headNum), this->m_numemployment);
                this->m_numEmployee[m_headNum] += m_numemployment;

                // 社員パラメータの社員を採用
                for (int i = 0; i < this->m_numemployment; i++)
                {
                    this->m_param.m_asset -= resource_link::game_::n_cost;
                    // オブジェクトが存在するとき
                    if (p_GameEnemyEmployee != nullptr)
                    {
                        p_GameEnemyEmployee->Employment(m_headNum);
                    }
                }
                break;

            case actionID::TRANSFERRED:
                if (this->m_param.m_dissatisfied + 5 >= this->m_maxWage)
                {
                    break;
                }
                else if (this->m_param.m_dissatisfied + 5 >= this->m_minWage)
                {
                    this->m_param.m_wage = this->m_param.m_dissatisfied + 6;
                    this->m_param.m_wage_sub = this->m_param.m_dissatisfied + 6;
                }
                m_numEmployee[m_targetNum] += m_numemployment;
                m_numEmployee[m_sourceTargetNum] -= m_numemployment;

                this->Transferred(this->m_psub->GetSub(this->m_sourceTargetNum), this->m_psub->GetSub(this->m_targetNum), this->m_psub, this->m_numemployment);

                // 社員パラメータの社員を転勤
                for (int i = 0; i < this->m_numemployment; i++)
                {
                    // オブジェクトが存在するとき
                    if (p_GameEnemyEmployee != nullptr)
                    {
                        p_GameEnemyEmployee->to_see(this->m_sourceTargetNum);
                        p_GameEnemyEmployee->Transferred(this->m_sourceTargetNum, this->m_targetNum);
                    }
                }
                break;

            case actionID::WAGE:
                break;

            default:
                break;
            }
            //タスク更新
            this->m_actionID[0] = this->m_actionID[1];
            this->m_actionID[1] = CEnemy::actionID::NONE;
        }
        //賃金を不満度+1に戻す
        if (this->m_param.m_dissatisfied >= this->m_minWage)
        {
            this->m_param.m_wage = this->m_param.m_dissatisfied + 1;
            this->m_param.m_wage_sub = this->m_param.m_dissatisfied + 1;
        }
        else
        {
            this->m_param.m_wage_sub = m_minWage;
            this->m_param.m_wage = m_minWage;
        }
    }
}

void CEnemy::selectTarget(void)
{
    //次アクションが登録されている = リターン
    if (this->m_actionID[0] != CEnemy::actionID::NONE)
    {
        return;
    }

    //最適人数の計算
    std::vector<int> optimumNumEmployee(m_NumSub);//エリアごとの最適人数
    int totalOptimumNumEmployee = 0;			//全体での最適人数
    for (int i = 0; i < m_NumSub; i++)
    {
        //エリア売り上げ÷２÷ベース賃金が閾値
        optimumNumEmployee[i] = this->m_psub->GetSub(i)->m_income / this->m_minWage / 2;
        totalOptimumNumEmployee += optimumNumEmployee[i];
    }

    //解雇判断
    //解雇すべきケース：「全体最適人数*1.2＜社員数」かつ「不満度が9(min賃金/2-1)以下」
    //少しは無理して抱える（イベント発生なども考慮
    //ターゲット：最もオーバーしているエリア
    //人数：1
    if ((totalOptimumNumEmployee * 1.2f < this->m_param.m_total_employee) && (this->m_param.m_dissatisfied < this->m_minWage / 2))
    {
        this->m_targetNum = -1;
        int overNum = 0; //最大オーバー人数

        for (int i = 0; i < m_NumSub; i++)
        {
            if (this->m_numEmployee[i] - optimumNumEmployee[i] > overNum)
            {
                overNum = this->m_numEmployee[i] - optimumNumEmployee[i];
                this->m_targetNum = i;
            }
        }

        this->m_actionID[0] = actionID::FIRE;
        return;
    }

    //増員ターゲット判断
    //ウェイト中じゃないエリアで最もロス指標(不足人数×エリア売り上げ)が大きい
    this->m_targetNum = -1;
    int loss = 0; //ロス指標
    for (int i = 0; i < m_NumSub; i++)
    {
        if ((optimumNumEmployee[i] - this->m_numEmployee[i]) * this->m_psub->GetSub(i)->m_income > loss)
        {
            this->m_targetNum = i;
            loss = (optimumNumEmployee[i] - this->m_numEmployee[i]) * this->m_psub->GetSub(i)->m_income;
        }
    }
    //ターゲット無しで操作なし
    if (m_targetNum == -1)
    {
        this->m_actionID[0] = actionID::NONE;
        return;
    }

    //移動元判断
    //理想人数を最もオーバーしているエリアを対象
    this->m_sourceTargetNum = -1;
    int overNum = 0; //最大オーバー人数
    for (int i = 0; i < m_NumSub; i++)
    {
        if (this->m_numEmployee[i] - optimumNumEmployee[i] > overNum)
        {
            overNum = this->m_numEmployee[i] - optimumNumEmployee[i];
            this->m_sourceTargetNum = i;
        }
    }

    //どこもオーバーしていなかったら雇用処理→移動処理
    if (m_sourceTargetNum == -1)
    {
        this->m_actionID[0] = actionID::EMPLOYMENT;

        //タゲが本社以外なら次ターン行動に移動を設定
        if (this->m_targetNum != m_headNum)
        {
            this->m_sourceTargetNum = m_headNum;
            this->m_actionID[1] = actionID::TRANSFERRED;
        }
        return;
    }
    else
    {
        this->m_actionID[0] = actionID::TRANSFERRED;
        return;
    }
}

//==========================================================================
// データの補正
void CEnemy::Correction(void)
{
    auto*pTime = (CWorldTime*)this->m_WorldTime;
    auto*p_GameEnemyrEmployee = (CGameEnemyEmployee*)this->m_GameEnemyEmployee;

    // データの補正
    if (pTime->getturns().m_current != pTime->getturns().m_old)
    {
        int nbuf = 0; // 合計の取得
                      // プレイヤーの社員取得
        auto * p_emp = p_GameEnemyrEmployee->GetEmployeeData();

        this->m_psub->GetMaxNemployee();

        // 子会社へのアクセス
        for (int i = 0; i < this->m_psub->GetNumSub(); i++)
        {
            auto * p_sub_data = this->m_psub->GetSub(i);

            // idと一致するデータの検出
            auto itr = p_emp->find(p_sub_data->m_id);

            // 社員人数の補正
            if (itr != p_emp->end())
            {
                p_sub_data->m_employee_sub = (int)itr->second.size();
                p_sub_data->m_employee = (int)itr->second.size();
                nbuf += (int)itr->second.size();
            }
        }

        // 総社員数の補正
        this->m_param.m_total_employee = nbuf + p_GameEnemyrEmployee->GetTaskData()->size();
    }
}
