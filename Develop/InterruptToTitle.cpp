#include "InterruptToTitle.h"
#include "resource_list.h"


CInterruptToTitle::CInterruptToTitle() : CObject(ID::Polygon2D)
{
    this->SetType(Type::Title_InterruptToTitle);
    this->m_key = false;
    this->m_key2 = false;
    this->m_look_id = 0;
}


CInterruptToTitle::~CInterruptToTitle()
{
}

bool CInterruptToTitle::Init(void)
{
    this->XInput()->Init(1);

    int ____cnt = 0;

    this->m_link[____cnt] = RESOURCE_Tutorial01_DDS;
    ____cnt++;
    this->m_link[____cnt] = RESOURCE_Tutorial02_DDS;
    ____cnt++;
    this->m_link[____cnt] = RESOURCE_Tutorial03_DDS;
    ____cnt++;
    this->m_link[____cnt] = RESOURCE_Tutorial04_DDS;
    ____cnt++;
    this->m_link[____cnt] = RESOURCE_Tutorial05_DDS;
    ____cnt++;
    this->m_link[____cnt] = RESOURCE_Tutorial06_DDS;
    ____cnt++;
    this->m_link[____cnt] = RESOURCE_Tutorial07_DDS;
    ____cnt++;
    this->m_link[____cnt] = RESOURCE_Tutorial08_DDS;
    ____cnt++;
    this->m_link[____cnt] = RESOURCE_Tutorial09_DDS;
    ____cnt++;
    this->m_link[____cnt] = RESOURCE_Tutorial10_DDS;
    ____cnt++;
    this->m_link[____cnt] = RESOURCE_Tutorial11_DDS;
    ____cnt++;
    this->m_link[____cnt] = RESOURCE_Tutorial12_DDS;
    ____cnt++;
    this->m_link[____cnt] = RESOURCE_Tutorial13_DDS;
    ____cnt++;
    this->m_link[____cnt] = RESOURCE_Tutorial14_DDS;
    ____cnt++;
    this->m_link[____cnt] = RESOURCE_Tutorial15_DDS;

    this->_2DObject()->Create()->Init(0);

    this->XAudio()->Init({ RESOURCE_ターン_wav,0,1.0f });

    return false;
}

void CInterruptToTitle::Uninit(void)
{
    this->m_link.clear();
}

void CInterruptToTitle::Update(void)
{
    if (this->m_key == true)
    {
        if (this->XInput()->Trigger(CXInput::EButton::B, 0) && this->m_key2 == false)
        {
            this->m_look_id = 0;
            this->_2DPolygon()->Release();
            this->m_key = false;
            this->m_key2 = false;
            return;
        }
        else if (!this->XInput()->Trigger(CXInput::EButton::B, 0))
        {
            this->m_key2 = false;
        }
        // 左
        if (this->XInput()->Trigger(CXInput::EButton::DPAD_LEFT, 0))
        {
            if (this->m_look_id != 0)
            {
                this->XAudio()->Play(0);
                this->m_look_id--;
                this->_2DPolygon()->Release();
                this->_2DPolygon()->Init(this->m_link[this->m_look_id].c_str(), true);
                this->_2DPolygon()->ObjectInput(this->_2DObject()->Get(0));
            }
        }

        // 右
        if (this->XInput()->Trigger(CXInput::EButton::DPAD_RIGHT, 0))
        {
            if (this->m_look_id != (int)this->m_link.size() - 1)
            {
                this->XAudio()->Play(0);
                this->m_look_id++;
                this->_2DPolygon()->Release();
                this->_2DPolygon()->Init(this->m_link[this->m_look_id].c_str(), true);
                this->_2DPolygon()->ObjectInput(this->_2DObject()->Get(0));
            }
        }
        this->XInput()->Update();
    }
    this->_2DPolygon()->Update();
}

void CInterruptToTitle::Draw(void)
{
    this->_2DPolygon()->Draw();
}

// タイトルを止める処理
bool CInterruptToTitle::StopTitle(void)
{
    this->XInput()->Update();
    if (this->XInput()->Trigger(CXInput::EButton::B, 0) ||
        this->XInput()->Trigger(CXInput::EButton::A, 0) ||
        this->XInput()->Trigger(CXInput::EButton::X, 0) ||
        this->XInput()->Trigger(CXInput::EButton::Y, 0)) 
    {
        if (m_key==false)
        {
            this->m_key = true;
            this->m_key2 = true;
            this->_2DPolygon()->Release();
            this->_2DPolygon()->Init(this->m_link[this->m_look_id].c_str(), true);
            this->_2DPolygon()->ObjectInput(this->_2DObject()->Get(0));
        }
        return true;
    }
    return false;
}

// タイトル処理の再開
bool CInterruptToTitle::TitleControl(void)
{
    return this->m_key;
}
