//==========================================================================
// ロゴ[CLogo.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Logo.h"
#include "Warning.h"
#include "resource_list.h"

CLogo::CLogo() : CObject(ID::Polygon2D)
{
}


CLogo::~CLogo()
{
}

//==========================================================================
// 初期化
bool CLogo::Init(void)
{
    auto * p_obj = this->_2DObject()->Create();

    this->_2DPolygon()->ObjectInput(p_obj);
    
    p_obj->SetCentralCoordinatesMood(true);
    p_obj->SetPos(this->GetWinSize().m_Width / 2.0f, this->GetWinSize().m_Height / 2.0f);

    this->m_time.Init(1, 60);
    this->m_color = 255;

    this->m_warning = this->GetObjects(ID::Polygon2D, Type::Title_Warning);

    return this->_2DPolygon()->Init(RESOURCE_logo2_DDS, true);
}

//==========================================================================
// 解放
void CLogo::Uninit(void)
{
}

//==========================================================================
// 更新
void CLogo::Update(void)
{
    if (this->m_warning == nullptr)
    {
        this->m_warning = this->GetObjects(ID::Polygon2D, Type::Title_Warning);
    }

    // 制御カウンタが終了している
    if (this->m_time.Countdown())
    {
        if (this->m_color.a != 0)
        {
            this->m_color.a -= 5;
            if (this->m_color.a<=0)
            {
                this->m_color.a = 0;
                auto * p_waning = (CWarning*)this->m_warning;
                p_waning->Start();
            }
            this->_2DObject()->Get(0)->SetColor(this->m_color);
        }
    }

    this->_2DPolygon()->Update();
}

//==========================================================================
// 描画
void CLogo::Draw(void)
{
    this->_2DPolygon()->Draw();
}
