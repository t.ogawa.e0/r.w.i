//==========================================================================
// ゲームイベント:人気動物の誕生[TheBirthOfPopularAnimalsEvent.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _TheBirthOfPopularAnimals_H_
#define _TheBirthOfPopularAnimals_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "GameEvent.h"

//==========================================================================
//
// class  : CTheBirthOfPopularAnimals
// Content: 人気動物の誕生
//
//==========================================================================
class CTheBirthOfPopularAnimals : public CEventManager
{
public:
    CTheBirthOfPopularAnimals()
    {
        this->m_Name = "人気動物の誕生";
    }
    ~CTheBirthOfPopularAnimals()
    {
        this->m_target.clear();
        this->m_Name.clear();
    }

    // 初期化
    void Init(std::mt19937 & InputMT);

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;

    // UI更新
    void UpdateUI(void)override;

    // テキスト
    void text(std::string strEventName)override;
private:
};

#endif // !_TheBirthOfPopularAnimals_H_
