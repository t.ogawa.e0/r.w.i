//==========================================================================
// ボイコット[Game_Strike.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Game_Strike.h"
#include "Game_Start.h"
#include "Game_Sound.h"
#include "resource_list.h"

CGame_Strike::CGame_Strike() :CObject(ID::Polygon2D)
{
    this->SetType(Type::Game_Strike);
    this->m_start_script = nullptr;
    this->m_sound = nullptr;
}

CGame_Strike::~CGame_Strike()
{
    this->m_data.Release();
}

//==========================================================================
// 初期化
bool CGame_Strike::Init(void)
{
    //==========================================================================
    // ストライキ演出テクスチャ
    if (this->_2DPolygon()->Init(RESOURCE_strike_DDS, true))
    {
        return true;
    }

    //==========================================================================
    // ストライキ演出テクスチャ
    if (this->_2DPolygon()->Init(RESOURCE_Syuutyuusen_DDS, true))
    {
        return true;
    }

    this->_2DPolygon()->SetTexScale(1, 2.0f);

    //==========================================================================
    // ストライキフォント
    if (this->_2DPolygon()->Init(RESOURCE_MainScreen_st_DDS, true))
    {
        return true;
    }

    this->_2DPolygon()->SetTexScale(2, 0.5f);

    this->m_start_script = this->GetObjects(ID::Polygon2D, Type::Game_Start_Object);
    this->m_sound = this->GetObjects(ID::Default, Type::Game_Game_Sound);

    return false;
}

//==========================================================================
// 解放
void CGame_Strike::Uninit(void)
{
}

//==========================================================================
// 更新
void CGame_Strike::Update(void)
{
    if (this->m_start_script == nullptr)
    {
        this->m_start_script = this->GetObjects(ID::Polygon2D, Type::Game_Start_Object);
    }
    if (this->m_sound == nullptr)
    {
        this->m_sound = this->GetObjects(ID::Default, Type::Game_Game_Sound);
    }

    // オブジェクトが存在するとき
    if (this->m_start_script != nullptr)
    {
        auto * p_start = (CGame_Start*)this->m_start_script;

        for (int i = 0; i < this->m_data.Size(); i++)
        {
            float f_speed = 0.0f;
            auto * p_obj = this->m_data.Get(i);

            // タイマーが終わっていない
            if (p_obj->m_time.GetComma() != 0 && p_obj->m_time.GetTime() != 0)
            {
                // 速度計算
                f_speed = this->_Hit()->Distance(*p_obj->m_obj_main.GetPos(), p_obj->m_stop_pos)*0.2f;

                p_obj->m_obj_main.SetXPlus(f_speed);

                // 速度計算
                f_speed = this->_Hit()->Distance(*p_obj->m_obj_effect.GetPos(), p_obj->m_stop_pos)*0.2f;
                p_obj->m_obj_effect.SetXPlus(f_speed);

                // 速度計算
                p_obj->m_obj_font.SetXPlus(f_speed);

                p_start->GameStop();
            }
            else if (p_obj->m_time.GetComma() == 0 && p_obj->m_time.GetTime() == 0)
            {
                // 速度計算
                f_speed = this->_Hit()->Distance(*p_obj->m_obj_main.GetPos(), p_obj->m_end_pos)*0.2f;

                p_obj->m_obj_main.SetXPlus(f_speed);

                // 速度計算
                f_speed = this->_Hit()->Distance(*p_obj->m_obj_effect.GetPos(), p_obj->m_end_pos)*0.2f;
                p_obj->m_obj_effect.SetXPlus(f_speed);

                // 速度計算
                p_obj->m_obj_font.SetXPlus(f_speed);

                p_obj->m_end_time.Countdown();
                p_start->GameStart();
            }

            if (p_obj->m_end_time.GetComma() == 0 && p_obj->m_end_time.GetTime() == 0)
            {
                p_obj->m_endkey = true;
            }

            // 表示カウンタ
            p_obj->m_time.Countdown();
        }

        // 解放
        for (int i = 0; i < this->m_data.Size(); i++)
        {
            auto * p_obj = this->m_data.Get(i);

            if (p_obj != nullptr)
            {
                if (p_obj->m_endkey)
                {
                    this->_2DPolygon()->ObjectDelete(p_obj->m_obj_main);
                    this->_2DPolygon()->ObjectDelete(p_obj->m_obj_effect);
                    this->_2DPolygon()->ObjectDelete(p_obj->m_obj_font);
                    this->m_data.PinpointRelease(p_obj);
                }
            }
        }

        if (this->ImGui()->MenuItem("ストライキ"))
        {
            this->Create();
        }
    }

    this->_2DPolygon()->Update();
}

//==========================================================================
// 描画
void CGame_Strike::Draw(void)
{
    this->_2DPolygon()->Draw();
}

//==========================================================================
// 生成
void CGame_Strike::Create(void)
{
    this->initializer();
}

// 座標初期化
void CGame_Strike::initializer(void)
{
    CVector4<float>v_pos = CVector4<float>(-(float)this->GetWinSize().m_Width, 0);
    auto * p_obj = this->m_data.Create();
    auto * p_sound = (CGame_Sound*)this->m_sound;

    // オブジェクトの登録
    this->_2DPolygon()->ObjectInput(p_obj->m_obj_main);
    this->_2DPolygon()->ObjectInput(p_obj->m_obj_effect);
    this->_2DPolygon()->ObjectInput(p_obj->m_obj_font);

    // 座標の初期化
    p_obj->m_obj_main.SetPos(v_pos);
    p_obj->m_obj_effect.SetPos(v_pos);
    p_obj->m_obj_font.SetPos(v_pos);

    v_pos = CVector4<float>(-(float)this->GetWinSize().m_Width/2.0f, (float)this->GetWinSize().m_Height / 2.0f);
    p_obj->m_obj_font.SetPos(v_pos);

    p_obj->m_end_pos = CVector4<float>((float)this->GetWinSize().m_Width, 0);

    p_sound->voice();
}
