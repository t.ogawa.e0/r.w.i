//==========================================================================
#include "LoseSymbol.h"
#include "ResultReminiscence.h"
#include "resource_list.h"

//==========================================================================
// 初期化
bool CLoseSymbol::Init(void)
{
	const char * pUIList[] = {
        RESOURCE_Lose_Chousei_DDS,
	};
	int datasize = (int)this->Helper()->Sizeof_(pUIList);
	CVector4<float>vpos;

	// インスタンス生成

	for (int i = 0; i < datasize; i++)
	{
		if (this->_2DPolygon()->Init(pUIList[i], true))
		{
			return true;
		}
		this->_2DPolygon()->ObjectInput(this->_2DObject()->Create());
		this->_2DObject()->Get(i)->Init(i);
	}

	this->_2DPolygon()->SetTexSize(0, this->GetWinSize().m_Width, this->GetWinSize().m_Height);

	vpos.x = 0.0f;
	vpos.y = 0.0f;
	this->_2DObject()->Get(0)->Scale(-0.5f);
	this->_2DObject()->Get(0)->SetPos(vpos);

	this->m_color = 255;
	this->m_color.a = 0;
	this->_2DObject()->Get(0)->SetColor(m_color);

	m_endpos = CVector2<float>(0.0f, (float)GetWinSize().m_Height);

    this->m_reminnn = this->GetObjects(CObject::ID::Polygon2D, CObject::Type::Result_ResultReminiscence);

	return false;
}

//==========================================================================
// 解放
void CLoseSymbol::Uninit(void)
{
}

//==========================================================================
// 更新
void CLoseSymbol::Update(void)
{
    if (this->m_reminnn)
    {
        this->m_reminnn = this->GetObjects(CObject::ID::Polygon2D, CObject::Type::Result_ResultReminiscence);
    }

	auto * p_obj = this->_2DObject()->Get(0);
	

	if (p_obj->GetPos()->y <= m_endpos.y)
	{
		p_obj->SetY(p_obj->GetPos()->y + (float)this->GetWinSize().m_Height/100.0f);
	}
    else
    {
        auto * p_remsisjfi = (CResultReminiscence*)this->m_reminnn;
        p_remsisjfi->Start();
    }
	
	if (m_color.a < 255)
	{
		m_color.a +=2;
		if (m_color.a > 255)
		{
			m_color.a = 255;
		}
		p_obj->SetColor(m_color);
	}

	this->_2DPolygon()->Update();
}

//==========================================================================
// 描画
void CLoseSymbol::Draw(void)
{
	this->_2DPolygon()->Draw();
}