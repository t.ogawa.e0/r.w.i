//==========================================================================
// Game_ICO[Game_ICO.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CGame_ICO
// Content: CGame_ICO
//
//==========================================================================
class CGame_ICO : public CObject
{
private:
	enum class ICO_UI_List {
		ICO_Player,
		ICO_Enemy,
	};

public:
    CGame_ICO();
    ~CGame_ICO();
    // ������
    bool Init(void)override;
    // ���
    void Uninit(void)override;
    // �X�V
    void Update(void)override;
    // �`��
    void Draw(void)override;
private:
    // ���Y�̔�r
    void AssetComparison(void);
private:
    void * m_player;
    void * m_enemy;
};

