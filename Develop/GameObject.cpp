//==========================================================================
// GameObject[GameObject.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "GameObject.h"
#include "LoadGameData.h"
#include "RelayPoint.h"

//==========================================================================
// 初期化
bool CGameObject::Init(void)
{
    CStageData stage_data;
    
    const char * pObjectList[] = {
        stage_data.Get_Pass_MAP_X(CLoadGameData::GetSerectData()),
        RESOURCE_build_x,
        RESOURCE_Sub_build_x,
    };

    C3DObject* obj = this->m_CObjectMapData.Create();
    obj->Init(0);
    obj->RotX(ANGLE_t::_ANGLE_180);
    obj->Scale(4.0f);
    this->_3DXmodel()->ObjectInput(obj);

    // 描画素材の登録
    for (int i = 0; i < this->Helper()->Sizeof_(pObjectList); i++)
    {
        if (this->_3DXmodel()->Init(pObjectList[i], CXmodel::LightMoad::System))
        {
            this->m_CObjectData.Release();
            this->m_CObjectMapData.Release();
            return true;
        }
    }

    // オブジェクトのインスタンス生成
    for (int i = 0; i < CLoadGameData::Size(); i++)
    {
        CFieldData * fie = this->m_CObjectData.Create();
        *fie = CLoadGameData::GetData(i);


        if (fie->m_head_office_player == true)
        {
            fie->m_pos.SetIndex(1);
        }
        else if (fie->m_head_office_player == false)
        {
            fie->m_pos.SetIndex(2);
        }
        this->_3DXmodel()->ObjectInput(fie->m_pos);
    }


    this->m_RelayPoint = this->GetObjects(CObject::ID::Xmodel, CObject::Type::Game_RelayPoint);

    // オブジェクトが存在するとき
    if (this->m_RelayPoint != nullptr)
    {
        auto * point = (CRelayPoint*)this->m_RelayPoint;
        // ルートをつなぐ
        for (int i = 0; i < this->m_CObjectData.Size(); i++)
        {
            if (this->m_CObjectData.Get(i)->m_param.m_next != -1)
            {
                point->Set(this->m_CObjectData.Get(i)->m_pos, this->m_CObjectData.Get(this->m_CObjectData.Get(i)->m_param.m_next)->m_pos);
            }
        }
    }

    return false;
}

//==========================================================================
// 解放
void CGameObject::Uninit(void)
{
    this->m_CObjectData.Release();
    this->m_CObjectMapData.Release();
}

//==========================================================================
// 更新
void CGameObject::Update(void)
{
    // オブジェクトセットされていないとき
    if (this->m_RelayPoint == nullptr)
    {
        this->m_RelayPoint = this->GetObjects(CObject::ID::Xmodel, CObject::Type::Game_RelayPoint);

        auto * point = (CRelayPoint*)this->m_RelayPoint;

        // オブジェクトが存在するとき
        if (point != nullptr)
        {
            // ルートをつなぐ
            for (int i = 0; i < this->m_CObjectData.Size(); i++)
            {
                if (this->m_CObjectData.Get(i)->m_param.m_next != -1)
                {
                    point->Set(this->m_CObjectData.Get(i)->m_pos, this->m_CObjectData.Get(this->m_CObjectData.Get(i)->m_param.m_next)->m_pos);
                }
            }
        }
    }

    this->_3DXmodel()->Update();
}

//==========================================================================
// 描画
void CGameObject::Draw(void)
{
    this->_3DXmodel()->Draw();
}