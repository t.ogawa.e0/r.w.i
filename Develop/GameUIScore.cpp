//==========================================================================
// スコア表示UI[GameUIScore.cpp]
// author: yoji watanabe
//==========================================================================
#include "GameUIScore.h"
#include "Character.h"
#include "Worldtime.h"
#include "resource_list.h"

//==========================================================================
// 初期化
bool CGameUIScore::Init(void)
{
    C2DObject * pPos = nullptr;
    CVector4<float> vpos4;
    CVector2<float> vpos2;
    CTexvec<int> * vTexSize = nullptr;
	CColor<int> color(255, 255, 255, 255);

    //==========================================================================
    // テクスチャの読み込み
    //==========================================================================
    const char * pMainUI[] = {
        RESOURCE_Main_Screen_MainUI_BG_DDS,
		RESOURCE_Main_Screen_Gage_DDS,//00
		RESOURCE_Main_Screen_Gage_DDS,//01
		RESOURCE_Main_Screen_Gage_DDS,//02
		RESOURCE_Main_Screen_Gage_DDS,//03
		RESOURCE_Main_Screen_Gage_DDS,//04
		RESOURCE_Main_Screen_Gage_DDS,//05
		RESOURCE_Main_Screen_Gage_DDS,//06
		RESOURCE_Main_Screen_Gage_DDS,//07
		RESOURCE_Main_Screen_Gage_DDS,//08
		RESOURCE_Main_Screen_Gage_DDS,//09
		RESOURCE_Main_Screen_Gage_DDS,//10
		RESOURCE_Main_Screen_Gage_DDS,//11
		RESOURCE_Main_Screen_Gage_DDS,//12
		RESOURCE_Main_Screen_Gage_DDS,//13
		RESOURCE_Main_Screen_Gage_DDS,//14
		RESOURCE_Main_Screen_Gage_DDS,//15
		RESOURCE_Main_Screen_Gage_DDS,//16
		RESOURCE_Main_Screen_Gage_DDS,//17
		RESOURCE_Main_Screen_Gage_DDS,//18
		RESOURCE_Main_Screen_Gage_DDS,//19
		RESOURCE_Main_Screen_Gage_DDS,//20
		RESOURCE_Main_Screen_Gage_DDS,//21
		RESOURCE_Main_Screen_Gage_DDS,//22
		RESOURCE_Main_Screen_Gage_DDS,//23
		RESOURCE_Main_Screen_Gage_DDS,//24
		RESOURCE_Main_Screen_Gage_DDS,//25
		RESOURCE_Main_Screen_Gage_DDS,//26
		RESOURCE_Main_Screen_Gage_DDS,//27
		RESOURCE_Main_Screen_Gage_DDS,//28
		RESOURCE_Main_Screen_Gage_DDS,//29
		RESOURCE_Main_Screen_Gage_DDS,//30
		RESOURCE_Main_Screen_Gage_DDS,//31
		RESOURCE_Main_Screen_Gage_DDS,//32
		RESOURCE_Main_Screen_Gage_DDS,//33
		RESOURCE_Main_Screen_Gage_DDS,//34
		RESOURCE_Main_Screen_Gage_DDS,//35
		RESOURCE_Main_Screen_Gage_DDS,//36
		RESOURCE_Main_Screen_Gage_DDS,//37
		RESOURCE_Main_Screen_Gage_DDS,//38
		RESOURCE_Main_Screen_Gage_DDS,//39
		RESOURCE_Main_Screen_Gage_DDS,//40
		RESOURCE_Main_Screen_Gage_DDS,//41
		RESOURCE_Main_Screen_Gage_DDS,//42
		RESOURCE_Main_Screen_Gage_DDS,//43
		RESOURCE_Main_Screen_Gage_DDS,//44
		RESOURCE_Main_Screen_Gage_DDS,//45
		RESOURCE_Main_Screen_Gage_DDS,//46
		RESOURCE_Main_Screen_Gage_DDS,//47
		RESOURCE_Main_Screen_Gage_DDS,//48
		RESOURCE_Main_Screen_Gage_DDS,//49
		RESOURCE_Main_Screen_Gage_DDS,//50
		RESOURCE_Main_Screen_Gage_DDS,//51
		RESOURCE_Main_Screen_Gage_DDS,//52
		RESOURCE_Main_Screen_Gage_DDS,//53
		RESOURCE_Main_Screen_Gage_DDS,//54
		RESOURCE_Main_Screen_Gage_DDS,//55
		RESOURCE_Main_Screen_Gage_DDS,//56
		RESOURCE_Main_Screen_Gage_DDS,//57
		RESOURCE_Main_Screen_Gage_DDS,//58
		RESOURCE_Main_Screen_Gage_DDS,//59
		RESOURCE_Main_Screen_Gage_DDS,//60
		RESOURCE_Main_Screen_Gage_DDS,//61
		RESOURCE_Main_Screen_Gage_DDS,//62
		RESOURCE_Main_Screen_Gage_DDS,//63
		RESOURCE_Main_Screen_Gage_DDS,//64
		RESOURCE_Main_Screen_Gage_DDS,//65
		RESOURCE_Main_Screen_Gage_DDS,//66
		RESOURCE_Main_Screen_Gage_DDS,//67
		RESOURCE_Main_Screen_Gage_DDS,//68
		RESOURCE_Main_Screen_Gage_DDS,//69
		RESOURCE_Main_Screen_Gage_DDS,//70
		RESOURCE_Main_Screen_Gage_DDS,//71
		RESOURCE_Main_Screen_Gage_DDS,//72
		RESOURCE_Main_Screen_Gage_DDS,//73
		RESOURCE_Main_Screen_Gage_DDS,//74
		RESOURCE_Main_Screen_Gage_DDS,//75
		RESOURCE_Main_Screen_Gage_DDS,//76
		RESOURCE_Main_Screen_Gage_DDS,//77
		RESOURCE_Main_Screen_Gage_DDS,//78
		RESOURCE_Main_Screen_Gage_DDS,//79
		RESOURCE_Main_Screen_Gage_DDS,//80
		RESOURCE_Main_Screen_Gage_DDS,//81
		RESOURCE_Main_Screen_Gage_DDS,//82
		RESOURCE_Main_Screen_Gage_DDS,//83
		RESOURCE_Main_Screen_Gage_DDS,//84
		RESOURCE_Main_Screen_Gage_DDS,//85
		RESOURCE_Main_Screen_Gage_DDS,//86
		RESOURCE_Main_Screen_Gage_DDS,//87
		RESOURCE_Main_Screen_Gage_DDS,//88
		RESOURCE_Main_Screen_Gage_DDS,//89
		RESOURCE_Main_Screen_MainUI_DDS,
		RESOURCE_Main_Screen_MainUI_Gauge_DDS,
    };

    const char * pNumbersUI[] = {
        RESOURCE_MainScreen_Normal_Numbers_DDS,
        RESOURCE_MainScreen_Normal_Numbers_DDS,
        RESOURCE_MainScreen_Newspaper_Numbers_DDS,
        RESOURCE_MainScreen_Newspaper_Numbers_DDS,
        RESOURCE_MainScreen_Normal_Numbers_DDS,
        RESOURCE_MainScreen_Normal_Numbers_DDS,
    };


    for (int i = 0; i < this->Helper()->Sizeof_(pMainUI); i++)
    {
        if (this->_2DPolygon()->Init(pMainUI[i], true))
        {
            return true;
        }
        pPos = this->_2DObject()->Create();
        this->_2DPolygon()->ObjectInput(pPos);
        pPos->Init(i);
    }

    for (int i = 0; i < this->Helper()->Sizeof_(pNumbersUI); i++)
    {
        if (this->_2DNumber()->Init(pNumbersUI[i], true))
        {
            return true;
        }
    }

    //==========================================================================
    // 読み込みここまで
    //==========================================================================

	//==========================================================================
	// タイムゲージ
	//==========================================================================
	for (int i = (int)EUIList::Main_Screen_Timer0; i <= (int)EUIList::Main_Screen_Timer89; i++)
	{
		
		vpos4.x = (float)this->_2DPolygon()->GetTexSize(i)->w * 6.0f;
		vpos4.y = (float)this->_2DPolygon()->GetTexSize(i)->h * 0.565f;
		this->_2DObject()->Get(i)->SetCentralCoordinatesMood(true);
		this->_2DObject()->Get(i)->SetPos(vpos4);

		this->_2DObject()->Get(i)->Angle(D3DX_PI / 45.0f * (i-1));
		
		color.a = 0;
		this->_2DObject()->Get(i)->SetColor(color);
	}


    //==========================================================================
    // プレイヤー資産
    //==========================================================================
    vTexSize = this->_2DNumber()->GetTexSize((int)ENumber::Numbers1);
    vpos2 = CVector2<float>((this->GetWinSize().m_Width / 2.0f) - vTexSize->w*0.9f, vTexSize->h *0.6f);
    this->_2DNumber()->Set((int)ENumber::Numbers1, 5, false, false, vpos2);
    this->_2DNumber()->SetAnim((int)ENumber::Numbers1, 1, 10, 10);

    //==========================================================================
    // エネミー資産
    //==========================================================================
    vTexSize = this->_2DNumber()->GetTexSize((int)ENumber::Numbers2);
    vpos2 = CVector2<float>((this->GetWinSize().m_Width / 2.0f) + vTexSize->w *0.4f, vTexSize->h *0.6f);
    this->_2DNumber()->Set((int)ENumber::Numbers2, 5, false, false, vpos2);
    this->_2DNumber()->SetAnim((int)ENumber::Numbers2, 1, 10, 10);

    //==========================================================================
    //年
    //==========================================================================
    this->_2DNumber()->SetTexScale((int)ENumber::NumYear, 0.5f);
    vTexSize = this->_2DNumber()->GetTexSize((int)ENumber::NumYear);
    vpos2 = CVector2<float>((this->GetWinSize().m_Width / 2.0f) - (vTexSize->w / 10.0f)*1.5f, vTexSize->h*1.2f);
    this->_2DNumber()->Set((int)ENumber::NumYear, 2, false, false, vpos2);
    this->_2DNumber()->SetAnim((int)ENumber::NumYear, 1, 10, 10);

    //==========================================================================
    //月
    //==========================================================================
    this->_2DNumber()->SetTexScale((int)ENumber::NumMonth, 0.5f);
    vTexSize = this->_2DNumber()->GetTexSize((int)ENumber::NumMonth);
    vpos2 = CVector2<float>((this->GetWinSize().m_Width / 2.0f) - (vTexSize->w / 10.0f)*1.5f, vTexSize->h*2.2f);
    this->_2DNumber()->Set((int)ENumber::NumMonth, 2, false, false, vpos2);
    this->_2DNumber()->SetAnim((int)ENumber::NumMonth, 1, 10, 10);

    //==========================================================================
    //雇用人数
    //==========================================================================
    vTexSize = this->_2DNumber()->GetTexSize((int)ENumber::NumEmployee);
    vpos2 = CVector2<float>((vTexSize->w / 10.0f)*2.2f, vTexSize->h*1.25f);
    this->_2DNumber()->Set((int)ENumber::NumEmployee, 3, false, false, vpos2);
    this->_2DNumber()->SetAnim((int)ENumber::NumEmployee, 1, 10, 10);

    //==========================================================================
    // 賃金
    //==========================================================================
    vTexSize = this->_2DNumber()->GetTexSize((int)ENumber::NumSalary);
    vpos2 = CVector2<float>((vTexSize->w / 10.0f)*2.2f, vTexSize->h*2.40f);
    this->_2DNumber()->Set((int)ENumber::NumSalary, 3, false, false, vpos2);
    this->_2DNumber()->SetAnim((int)ENumber::NumSalary, 1, 10, 10);

    //==========================================================================
    // 不満度バー
    //==========================================================================
    vTexSize = this->_2DPolygon()->GetTexSize((int)EUIList::MainScreen_Dissatisfied_Indication);
    this->m_Dissatisfied_Indication_start_pos = CVector4<float>(this->GetWinSize().m_Width / 2.27f, this->GetWinSize().m_Height*0.215f);
    this->m_Dissatisfied_Indication_end_pos = CVector4<float>(this->GetWinSize().m_Width / 1.5f, this->GetWinSize().m_Height*0.215f);
	pPos->SetCentralCoordinatesMood(true);
    pPos->SetPos(this->m_Dissatisfied_Indication_start_pos);

    //==========================================================================
    // オブジェクトの先行呼び出し
    this->m_WorldTime = (CWorldTime*)this->GetObjects(CObject::ID::Default, CObject::Type::Game_WorldTime);

    return false;
}

//==========================================================================
// 解放
void CGameUIScore::Uninit(void)
{
}

//==========================================================================
// 更新
void CGameUIScore::Update(void)
{
    if (this->m_WorldTime == nullptr)
    {
        this->m_WorldTime = (CWorldTime*)this->GetObjects(CObject::ID::Default, CObject::Type::Game_WorldTime);
    }

    auto*pTime = (CWorldTime*)this->m_WorldTime;

	//==========================================================================
	// タイムゲージ
	//==========================================================================
	CColor<int> color(255, 255, 255, 255);
	float TurnTime = float(pTime->getturns2()->GetTime() % 10) + pTime->getturns2()->GetComma() / 60.0f;
	int Num = int(90 * TurnTime / 10.0f);
	for (int i = (int)EUIList::Main_Screen_Timer0; i <= (int)EUIList::Main_Screen_Timer89; i++)
	{
		if (i - (int)EUIList::Main_Screen_Timer0 < Num)
		{
			color.a = 255;
		}
		else
		{
			color.a = 0;
		}
		this->_2DObject()->Get(i)->SetColor(color);
	}

    //==========================================================================
    // 不満度バーの処理 (ここの処理の調節をお願いします)
    //==========================================================================

    // 不満度のオブジェクト取得
    auto * p_pos = this->_2DObject()->Get((int)EUIList::MainScreen_Dissatisfied_Indication);

    // テクスチャサイズの取得?
    auto * p_tex = this->_2DPolygon()->GetTexSize((int)EUIList::MainScreen_Dissatisfied_Indication);

    // 座標の取得
    auto vPos = *p_pos->GetPos();

    //this->m_Dissatisfied_Indication_start_pos; // これは不満度の始点
    //this->m_Dissatisfied_Indication_end_pos; // これは不満度の終点
	float gageWidth = this->m_Dissatisfied_Indication_end_pos.x - this->m_Dissatisfied_Indication_start_pos.x;

	vPos.x = this->m_Dissatisfied_Indication_start_pos.x + (gageWidth * CCharacter::Get(CCharacter::CharList::Player)->GetPlayerParam().m_dissatisfied / CCharacter::Get(CCharacter::CharList::Player)->GetPlayerParam().m_wage);
	p_pos->SetPos(vPos);
    // 以前の処理
    //vPos = *this->_2DObject()->Get((int)EUIList::MainScreen_Dissatisfied)->GetPos();
    //pPos = this->_2DObject()->Get((int)EUIList::MainScreen_Dissatisfied_Indication);
    //vTexSize = this->_2DPolygon()->GetTexSize((int)EUIList::MainScreen_Dissatisfied_Indication);
    //vPos.y += vTexSize->h * 0.7f;
    //vPos.x += -(vTexSize->w * 4.8f) + vTexSize->w * 14.5f *CCharacter::Get(CCharacter::CharList::Player)->GetPlayerParam().m_dissatisfied / CCharacter::Get(CCharacter::CharList::Player)->GetPlayerParam().m_wage;
    //pPos->SetPos(vPos);

    //==========================================================================
    // プレイヤーとエネミーの資産更新
    this->_2DNumber()->Update((int)ENumber::Numbers1, CCharacter::Get(CCharacter::CharList::Player)->GetPlayerParam().m_asset);
    this->_2DNumber()->Update((int)ENumber::Numbers2, CCharacter::Get(CCharacter::CharList::Enemy)->GetPlayerParam().m_asset);

    //==========================================================================
    // オブジェクトが存在するとき
    if (pTime != nullptr)
    {
        this->_2DNumber()->Update((int)ENumber::NumYear, pTime->getturns().m_current / 12 + 1);
        this->_2DNumber()->Update((int)ENumber::NumMonth, pTime->getturns().m_current % 12 + 1);
    }

    //==========================================================================
    // 社員数と賃金
    this->_2DNumber()->Update((int)ENumber::NumEmployee, CCharacter::Get(CCharacter::CharList::Player)->GetPlayerParam().m_total_employee);
    this->_2DNumber()->Update((int)ENumber::NumSalary, CCharacter::Get(CCharacter::CharList::Player)->GetPlayerParam().m_wage);

    //==========================================================================
    // 未使用回避デコイ
    //==========================================================================
    this->Helper()->Decoy_(*p_pos);
    this->Helper()->Decoy_(*p_tex);
    this->Helper()->Decoy_(vPos);

    this->_2DPolygon()->Update();
}

//==========================================================================
// 描画
void CGameUIScore::Draw(void)
{
    this->_2DPolygon()->Draw();
    this->_2DNumber()->Draw();
}