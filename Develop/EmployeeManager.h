//==========================================================================
// 社員データ管理マネージャー[EmployeeManager.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "EmployeeParamSystem.h"

//==========================================================================
//
// class  : CEmployeeManager
// Content: 社員データ管理マネージャー
//
//==========================================================================
class CEmployeeManager : public CObject, private CEmployeeParamSystem
{
public:
    CEmployeeManager() :CObject(CObject::ID::Default) {
        this->SetType(Type::Game_EmployeeManager);
    }
    ~CEmployeeManager() { }
    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;

    // 生成
    EmployeeParam * Create(void);

    // デバッグテキスト
    void SetDebugText(EmployeeParam *_this);
private:
    std::list<EmployeeParam*>m_data;
};

//==========================================================================
//
// class  : CEmployeeLink
// Content: 社員管理部
//
//==========================================================================
class CEmployeeLink
{
public:
    CEmployeeLink() {
        std::random_device rnd; // 非決定的な乱数生成器を生成
        std::mt19937 mt(rnd());  //  メルセンヌ・ツイスタの32ビット版、引数は初期シード値
        this->m_mt = mt;
        this->m_strike_update = false;
    }
    virtual ~CEmployeeLink() {
        this->m_data.clear();
    }

    const std::unordered_map<int, std::list<EmployeeParam*>> * GetEmployeeData(void) {
        return &this->m_data;
    }

    bool GetStrikeKey(void) {
        return this->m_strike_update;
    }

    void SetStrikeKeyReset(void) {
        this->m_strike_update = false;
    }

    // 初期セットアップ
    virtual void FastCreate(void) = 0;
protected:
    std::unordered_map<int, std::list<EmployeeParam*>> m_data; // 管理部
    std::mt19937 m_mt; // 乱数
    bool m_strike_update;
};

//==========================================================================
//
// class  : CCEmployeeTask
// Content: 処理task
//
//==========================================================================
class CCEmployeeTask
{
public:
    CCEmployeeTask() {
        this->m_EmployeeParam = nullptr;
        this->m_id_old = 0;
        this->m_id_next = 0;
        this->m_turn = 0;
        this->m_now = 0;
        this->m_data_id = 0;
        this->m_root_pt.clear();
    }
    CCEmployeeTask(EmployeeParam * p_m, std::vector<void*>_loot, int _data_id, int id_old, int id_next, int turn) {
        this->m_EmployeeParam = p_m;
        this->m_id_old = id_old;
        this->m_id_next = id_next;
        this->m_turn = turn;
        this->m_now = 0;
        this->m_data_id = _data_id;
        this->m_root_pt = _loot;
    }
    ~CCEmployeeTask() {
        this->m_EmployeeParam = nullptr;
        this->m_id_old = 0;
        this->m_id_next = 0;
        this->m_turn = 0;
        this->m_now = 0;
        this->m_data_id = 0;
        this->m_root_pt.clear();
    }

public:
    EmployeeParam * m_EmployeeParam; // 社員データ
    std::vector<void*>m_root_pt; // ルートポインタ(CSubsidiaryParam*)
    int m_id_old; // 転勤元ID
    int m_id_next; // 転勤先ID
    int m_now; // 経過ターン
    int m_turn; // 移動にかかる時間
    int m_data_id; // データのID
};