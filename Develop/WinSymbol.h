#ifndef _WinSymbol_H_
#define _WinSymbol_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CWinSymbol
// Content: 勝利文字
//
//==========================================================================
class CWinSymbol : public CObject
{
public:
	CWinSymbol() :CObject(CObject::ID::Polygon2D) {
        this->m_reminnn = nullptr;
    }
	~CWinSymbol() {}
	// 初期化
	bool Init(void)override;

	// 解放
	void Uninit(void)override;

	// 更新
	void Update(void)override;

	// 描画
	void Draw(void)override;
private:
	CColor<int>m_color; // 色
	CVector2<float>m_endpos; //終点
    void * m_reminnn;
};

#endif // !_WinSymbol_H_#pragma once
