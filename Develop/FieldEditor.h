//==========================================================================
// フィールドエディタ[FieldEditor.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _FieldEditor_H_
#define _FieldEditor_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include <string>
#include <vector>
#include <list>
#include <fstream>

//==========================================================================
//
// class  : CMinMax
// Content: ミンマックス
//
//==========================================================================
class CMinMax
{
public:
    CMinMax() {
        this->m_max = this->m_min = 0;
    }
    CMinMax(int min_, int max_) {
        this->m_max = max_;
        this->m_min = min_;
    }
    int min_(void) { return this->m_min; }
    int max_(void) { return this->m_max; }
private:
    int m_min;
    int m_max;
};

//==========================================================================
//
// class  : CRegionalList
// Content: 地域のリスト
//
//==========================================================================
class CRegionalList
{
protected:
    CRegionalList() {
        this->m_NameList.emplace_back("未設定"); // none
    }
    virtual ~CRegionalList() {
        this->RegionalRelease();
    }
    // 地域の登録
    void RegionalSet(const std::string & Input) {
        this->m_NameList.emplace_back(Input);
    }

    // 地域の解放
    void RegionalRelease(void) {
        for (auto itr = this->m_NameList.begin(); itr != this->m_NameList.end(); ++itr)
        {
            (*itr).clear();
        }
        this->m_NameList.clear();
    }

    // 地域reset
    void RegionalReset(void) {
        this->RegionalRelease();
        this->m_NameList.emplace_back("未設定"); // none
    }
protected:
    std::vector<std::string>m_NameList; // 地域ID
};

//==========================================================================
//
// class  : CRegionalData
// Content: 会社のデータ
//
//==========================================================================
class CRegionalData
{
public:
    CRegionalData() {
        this->m_textID = 0;
        this->m_income = 0;
        this->m_employee = 0;
        this->m_capital_city = false;
        this->m_prev = -1;
        this->m_next = -1;
        this->m_id = -1;
    }
    ~CRegionalData() {}
public:
    int m_income; // 売上(警告:生データ)
    int m_employee; // 社員数(警告:生データ)
    int m_textID; // テキスト(警告:生データ)
    bool m_capital_city; // 首都判定(警告:生データ)
    int m_prev; // 前の配列番号 データがない場合-1(警告:生データ)
    int m_next; // 次の配列番号 データがない場合-1(警告:生データ)
    int m_id; // 配列番号(警告:生データ)
};

//==========================================================================
//
// class  : CFieldData
// Content: 会社のデータ
//
//==========================================================================
class CFieldData
{
public:
    CFieldData() {
        this->m_pos.Init(0);
        this->m_ObjName = "";
        this->m_head_office_player = false;
        this->m_head_office_enemy = false;
        this->m_key = false;
    }
    ~CFieldData() {
        this->Release();
    }

    // 解放
    void Release(void) {
        this->m_pos.Release();
        this->m_ObjName.clear();
    }
public:
    CRegionalData m_param; // データ(生データ)
    C3DObject m_pos; // 座標
    std::string m_ObjName; // オブジェクト名
    bool m_head_office_player; // プレイヤーの本社(true)(警告:生データ)
    bool m_head_office_enemy; // エネミーの本社(true)(警告:生データ)
    bool m_key; // 鍵
};

//==========================================================================
//
// class  : CLoadCSV
// Content: CSV読み込み
//
//==========================================================================
class CLoadCSV
{
public:
    CLoadCSV() {}
    ~CLoadCSV() {
        this->CSVReleaseData();
    }

    // CSVデータ読み込み
    void CSVSetData(const std::string & Input) {
        std::ifstream ifs(Input); // c++によるファイル読み込み
        std::string str; // 格納string

        // ファイルが開いたとき
        if (!ifs.fail())
        {
            // 終わりまで回す
            for (;std::getline(ifs, str);) {
                this->m_input.emplace_back(str);
            }
        }
        ifs.close();
    }

    // CSVデータ開放
    void CSVReleaseData(void) {
        for (auto itr = this->m_input.begin(); itr != this->m_input.end(); ++itr)
        {
            (*itr).clear();
        }
        this->m_input.clear();
    }
public:
    std::list<std::string> m_input; // string型の格納
};

//==========================================================================
//
// class  : CFieldEditor
// Content: エディタ用クラス
//
//==========================================================================
class CFieldEditor : public CRegionalList
{
protected:
    // bool4
    class CBool4
    {
    public:
        CBool4() {
            this->reset();
        }
        ~CBool4() {}
        void reset(void) {
            this->b1 = this->b2 = this->b3 = this->b4 = false;
        }
    public:
        bool b1, b2, b3, b4;
    };
protected:
    CFieldEditor() {
        this->m_ActivationKey = false;
        this->m_data = nullptr;
        this->m_Edit = nullptr;
    }
    virtual ~CFieldEditor() {
        this->Release();
    }

public:
    // ビューのゲッター
    D3DXMATRIX * GetVew(void) { return this->m_Vew->CreateView(); }

    // 解放処理
    void Release(void) {
        this->m_pos.Release();
        this->m_ObjePoly.Release();
        this->m_VewCamera.Release();
        this->m_bool4_1.reset();
        this->m_bool4_2.reset();
        this->m_bool4_3.reset();
        this->m_bool4_4.reset();
        this->m_DataCase.Release();
    }

    // 有効化
    void Activation(bool Input) { this->m_ActivationKey = Input; }

    // アクティブキーのゲッター
    bool GetActivation(void) { return this->m_ActivationKey; }
protected:
    CImGui_Dx9 m_ImGui; // imgui
    CCamera *m_Vew;
    CCamera m_VewCamera;
    C3DObject m_pos; // 座標
    CXmodel m_ObjePoly; // ポリゴン
    CObjectVectorManager<CFieldData>m_DataCase;// データケース
    CBool4 m_bool4_1; // bool4
    CBool4 m_bool4_2; // bool4
    CBool4 m_bool4_3; // bool4
    CBool4 m_bool4_4; // bool4
    CFieldData *m_data; // データ
    CFieldData *m_Edit; // エディット用
    bool m_ActivationKey; // アクティブ判定キー
};

#endif // !_FieldEditor_H_
