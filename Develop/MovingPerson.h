//==========================================================================
// 動く人[MovingPerson.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _MovingPerson_H_
#define _MovingPerson_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "Subsidiary.h"
#include "SubsidiaryParam.h"
#include "FieldEditor.h"

//==========================================================================
//
// class  : CMovingPerson
// Content: 動く人
//
//==========================================================================
class CMovingPerson : public CObject
{
private:
    class CHuman
    {
    public:
        CHuman() {
            this->m_obj = nullptr;
            this->m_speed = 0.0f;
            this->m_old_turn = -1;
        }
        CHuman(C3DObject * _obj, float _speed, int _turn) {
            this->m_obj = _obj;
            this->m_speed = _speed;
            this->m_old_turn = _turn;
        }
        ~CHuman() {}
    public:
        C3DObject * m_obj; // オブジェクトパス
        float m_speed; // 速度
        int m_old_turn; // 古いターン
    };
public:
    CMovingPerson() :CObject(CObject::ID::Xmodel) {
        this->SetType(CObject::Type::Game_Human);
        this->m_WorldTime = nullptr;
        this->m_Employee = nullptr;
        this->m_start_script = nullptr;
    }
    ~CMovingPerson() {
        this->m_inputlist.clear();
        this->Uninit();
    }
    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;
private:
    CObjectVectorManager<CFieldData>m_CObjectData; // フィールド管理コンテナ
    void * m_WorldTime; // 時間へのアクセスルート
    void * m_Employee; // 移動オブジェクトへのアクセスルート
    void * m_start_script; // スタート制御
    std::unordered_map<int, CHuman> m_inputlist; // 生成済みオブジェクトの記録
};



/* 5秒で移動したい

移動したい距離を移動したい時間で割ると移動速度がでる。
*/

#endif // !_MovingPerson_H_
