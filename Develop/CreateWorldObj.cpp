//==========================================================================
// 国の処理[CreateWorldObj.h]
// author: tatsuya ogawa
//==========================================================================
#include "CreateWorldObj.h"

//==========================================================================
// 定数宣言
//==========================================================================
constexpr float fPowor = 0.5f;

//==========================================================================
// 初期化
bool CCreateWorldObj::Init(void)
{
    return false;
}

//==========================================================================
// 解放
void CCreateWorldObj::Uninit(void)
{
    this->Release();
}

//==========================================================================
// 更新
void CCreateWorldObj::Update(void)
{
    // アクティブな時のみ有効
    if (this->m_ActivationKey)
    {
        this->ImGui()->NewWindow("国の編集", true);
        float fpowor = 0.0f;
        this->ImGui()->SliderFloat("スケール", &fpowor, -fPowor, fPowor);
        this->m_pos.Scale(fpowor);

        const D3DXVECTOR3 * pos = this->m_pos.GetMatInfoPos();
        const D3DXVECTOR3 * rot = this->m_pos.GetVecFront();
        const D3DXVECTOR3 * sca = this->m_pos.GetMatInfoSca();

        this->ImGui()->Text("X = %.2f, Y = %.2f , Z = %.2f", pos->x, pos->y, pos->z);
        this->ImGui()->Text("回転");
        this->ImGui()->Text("X = %.2f, Y = %.2f , Z = %.2f", rot->x, rot->y, rot->z);
        this->ImGui()->Text("サイズ");
        this->ImGui()->Text("X = %.2f, Y = %.2f , Z = %.2f", sca->x, sca->y, sca->z);

        this->ImGui()->EndWindow();
    }
    this->m_ObjePoly.Update();
}

//==========================================================================
// 描画
void CCreateWorldObj::Draw(void)
{
    this->m_ObjePoly.Draw();
}

//==========================================================================
// 読み込み
void CCreateWorldObj::Load(StageData_ID Input)
{
    this->Release();
    this->m_pos.Init(0);
    this->m_pos.RotX(ANGLE_t::_ANGLE_180);
    this->m_pos.Scale(4.0f);
    this->m_ObjePoly.Init(this->m_stage_data.Get_Pass_MAP_X(Input), CXmodel::LightMoad::System);
    this->m_ObjePoly.ObjectInput(this->m_pos);
}