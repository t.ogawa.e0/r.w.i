//==========================================================================
// ゲームイベント:首都の場所が変わった[CapitalCityChangeEvent.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "CapitalCityChangeEvent.h"

//==========================================================================
// 初期化
void CCapitalCityChange::Init(std::mt19937 * InputMT)
{
    this->m_mt = InputMT;
    CSubsidiaryManager * pchar = nullptr;

    pchar = CSubsidiary::Get(CharList::Player);

    // 乱数の範囲指定
    this->m_rand = std::uniform_int_distribution<int>(0, pchar->GetNumSub() - 1);
}

//==========================================================================
// 解放
void CCapitalCityChange::Uninit(void)
{
    this->m_target.clear();
    this->m_Name.clear();
}

//==========================================================================
// 更新
void CCapitalCityChange::Update(void)
{
    CSubsidiaryManager * pchar = nullptr;

    // 乱数生成
    int capitalID = this->m_rand(*this->m_mt);

    // 対象エリアの入れ替え
    pchar = CSubsidiary::Get(CharList::Player);
    if (this->change(pchar, capitalID)) { this->m_endkey = true; }
    pchar = CSubsidiary::Get(CharList::Enemy);
    if (this->change(pchar, capitalID)) { this->m_endkey = true; }
}

//==========================================================================
// 描画
void CCapitalCityChange::Draw(void) 
{
}

//==========================================================================
// UI更新
void CCapitalCityChange::UpdateUI(void) 
{
    // 親オブジェクトへのアクセス
    auto * p_obj = (CMobeEventObject*)this->m_master_obj;

    // オブジェクトの座標設定
    this->m_ObjectPos->SetPos(*p_obj->Get()->GetPos());
    this->m_NameObject->SetPos(*p_obj->Get()->GetPos());
}

//==========================================================================
// テキスト
void CCapitalCityChange::text(std::string strEventName)
{
    std::string strEvent = strEventName + this->m_Name;
    if (this->m_Imgui.NewMenu(strEvent.c_str(), true))
    {
        this->m_Imgui.Text("発動中");
        this->m_Imgui.Text("次、首都が変わります");
        this->m_Imgui.EndMenu();
    }
}

//==========================================================================
// 入れ替え
bool CCapitalCityChange::change(CSubsidiaryManager * pchar, int capitalID)
{
    CSubsidiaryParam * ptarget1 = nullptr;

    ptarget1 = pchar->GetSub(capitalID);
    if (ptarget1->m_capital_city == false)
    {
        // 次の首都に必要なデータを取り出す
        CSubsidiaryParam backup_target1;
        backup_target1.m_capital_city = ptarget1->m_capital_city;
        backup_target1.m_income = ptarget1->m_income;
        backup_target1.m_income_default = ptarget1->m_income_default;
        backup_target1.m_profit = ptarget1->m_profit;

        CSubsidiaryParam * ptarget2 = nullptr;
        // 首都を検索
        for (int i = 0; i < pchar->GetNumSub(); i++)
        {
            ptarget2 = pchar->GetSub(i);
            if (ptarget2->m_capital_city == true)
            {
                break;
            }
        }

        // 現在の首都のデータを取り出す
        CSubsidiaryParam backup_target2;
        backup_target2.m_capital_city = ptarget2->m_capital_city;
        backup_target2.m_income = ptarget2->m_income;
        backup_target2.m_income_default = ptarget2->m_income_default;
        backup_target2.m_profit = ptarget2->m_profit;

        // 首都でなくなる場所に首都ではないデータを移す
        ptarget2->m_capital_city = backup_target1.m_capital_city;
        ptarget2->m_income = backup_target1.m_income;
        ptarget2->m_income_default = backup_target1.m_income_default;
        ptarget2->m_profit = backup_target1.m_profit;

        // 首都になる場所に首都データを移す
        ptarget1->m_capital_city = backup_target2.m_capital_city;
        ptarget1->m_income = backup_target2.m_income;
        ptarget1->m_income_default = backup_target2.m_income_default;
        ptarget1->m_profit = backup_target2.m_profit;
        return true;
    }
    return false;
}
