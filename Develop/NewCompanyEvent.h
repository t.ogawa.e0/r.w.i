//==========================================================================
// ゲームイベント:新企業[NewCompanyEvent.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _NewCompanyEvent_H_
#define _NewCompanyEvent_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "GameEvent.h"

//==========================================================================
//
// class  : CNewCompany
// Content: 新企業
//
//==========================================================================
class CNewCompany : public CEventManager
{
public:
    CNewCompany()
    {
        this->m_Name = "新企業";
    }
    ~CNewCompany()
    {
        this->m_target.clear();
        this->m_Name.clear();
    }

    // 初期化
    void Init(std::mt19937 & InputMT);

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;

    // UI更新
    void UpdateUI(void)override;

    // テキスト
    void text(std::string strEventName)override;
private:
};

#endif // !_NewCompanyEvent_H_
