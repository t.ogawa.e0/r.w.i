//==========================================================================
// タイトル画面UI[TitleUI.h]
// author: yoji watanabe
//==========================================================================
#ifndef _TitleUI_H_
#define _TitleUI_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include"TitleData.h"
#include"Title_Sound.h"

//==========================================================================
//
// class  : CTitleUI
// Content: タイトル画面UI
//
//==========================================================================
class CTitleUI : public CObject ,public CTitleData
{
public:
	// 操作モード
	enum class modeList
	{
		Default = 0,
		modeChange,
		modeSelectYear,
		modeSelectEnemy,
		modeSelectMap,
		modeDecide,
		modeTutorial,
		modeExit,
		modeEnd,
	};
	//UIリスト
	enum class TitleUIList {
        Title_Anim,
		Title_Logo,
		Title_Push,
		Title_Data,
		Title_End,
		Title_Select,
		Title_Decide,
		Title_Level,
		Title_Map,
		Title_Tutorial,
	};

	CTitleUI() :CObject(CObject::ID::Polygon2D) { 
        m_mode = CTitleUI::modeList::Default; 
        m_count = 0; 
        m_FlashCount = 0; 
    };
	~CTitleUI() {};
	
	// 初期化
	bool Init(void)override;

	// 解放
	void Uninit(void)override;

	// 更新
	void Update(void)override;

	// 描画
	void Draw(void)override;

	//Updateに組み込む
	void UpdateMode(void);

	static Title_Stage_ID ConvertMapLevelToStageID(int mapLevel)
	{
		switch (mapLevel)
		{
		case 0:
			return Title_Stage_ID::Waneha;
			break;
		case 1:
			return Title_Stage_ID::Kankari;
			break;
		case 2:
			return Title_Stage_ID::U_S_B;
			break;
		case 3:
			return Title_Stage_ID::Wabisabi;
			break;
		default:
			return Title_Stage_ID::Waneha;
			break;
		}
	};

	static int ConvertStageIDToMapLevel(Title_Stage_ID StageID)
	{
		switch (StageID)
		{
		case Title_Stage_ID::Waneha:
			return 0;
			break;
		case  Title_Stage_ID::Kankari:
			return 1;
			break;
		case Title_Stage_ID::U_S_B:
			return 2;
			break;
		case Title_Stage_ID::Wabisabi:
			return 3;
			break;
		default:
			return 0;
			break;
		}
	};

	void SetSoundObj(CTitle_Sound* soundObj) { m_pSoundObj = soundObj; };

private:
	CTitleUI::modeList m_mode;	//現在のモード
	int m_count; //モード更新用カウント
	int m_animcount = 0;
	int m_FlashCount = 0;
	//ステージ選択操作用変数
	int m_MapLevel;
	CTitle_Sound* m_pSoundObj;//サウンドへのアクセスルート
    void * m_teitle_Interrupt;
};

#endif // !_TitleUI_H_