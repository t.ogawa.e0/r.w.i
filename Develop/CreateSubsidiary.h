//==========================================================================
// 各地域の子会社生成[CreateSubsidiary.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _CreateSubsidiary_H_
#define _CreateSubsidiary_H_

//==========================================================================
// include
//==========================================================================
#include <string>
#include "dxlib.h"
#include "FieldEditor.h"
#include "Stage.h"

//==========================================================================
//
// class  : CCreateSubsidiary
// Content: 各地域の子会社生成
//
//==========================================================================
class CCreateSubsidiary : public CObject, public CFieldEditor
{
public:
    // フロート7
    class CFloatCase
    {
    public:
        CFloatCase() {
            f1 = f2 = f3 = f4 = f5 = f6 = f7 = 0.0f;
        }
        void reset(void) {
            f1 = f2 = f3 = f4 = f5 = f6 = f7 = 0.0f;
        }
    public:
        float f1, f2, f3, f4, f5, f6, f7;
    };
public:
    CCreateSubsidiary() : CObject(CObject::ID::Xmodel) {
        this->m_RelayPoint = nullptr;
    }
    ~CCreateSubsidiary() {}

    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;

    // 読み込み
    void Load(StageData_ID Input);

    // セーブ
    void Save(StageData_ID Input);
private:
    // 読み込み
    void Load(const std::string Input);

    // セーブ
    void Save(const std::string Input);

    // 座標のセット
    void SetPos(bool bopen, CFieldData * Input);

    // X軸の移動
    float XAxisMovement(CFieldData * Input);

    // Y軸の移動
    float YAxisMovement(CFieldData * Input);

    // Z軸の移動
    float ZAxisMovement(CFieldData * Input);

    // X軸回転
    float XAxisRot(CFieldData * Input);

    // Y軸回転
    float YAxisRot(CFieldData * Input);

    // スケーリング
    float Scaling(CFieldData * Input);

    // パラメータのセット
    void SetRegionalData(void);

    // 生成
    void Create(void);

    // エディト
    void Edit(void);

    // パラメータのセット
    void EditRegionalData(void);

    // ナンバリング
    std::string Numbering(CFieldData * Input, int num);

    // リンクシステム
    void LinkSystem(void);

    // データの破棄
    void Delete(void);

    // 中継中継地点をセット
    void SetRelayPoint(void);
private:
    CMinMax m_LimitRegionEmployee; // 採用人数の制限
    CMinMax m_LimitRegionProfit; // 給料の制限
    CLoadCSV m_CSV; // csv
    void * m_RelayPoint; // 中継地点オブジェクトへのアクセスルート
    CStageData m_stage_data;
};

#endif // !_CreateSubsidiary_H_
