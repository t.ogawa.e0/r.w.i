//==========================================================================
// 制作用シーン[make.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _make_h_
#define _make_h_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "SceneChange.h"

//==========================================================================
//
// class  : CMake
// Content: 制作用
//
//==========================================================================
class CMake : public CBaseScene
{
public:
    CMake();
    ~CMake();
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
private:
};

#endif // !_make_H_
