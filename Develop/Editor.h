//==========================================================================
// エディタ[Editor.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _Editor_H_
#define _Editor_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "SceneChange.h"
#include "FieldEditor.h"
#include "CreateWorld.h"
#include "CreateSubsidiary.h"
#include "CreateWorldObj.h"
#include "RelayPoint.h"

//==========================================================================
//
// class  : CEditor
// Content: エディタ
//
//==========================================================================
class CEditor : public CBaseScene
{
public:
    CEditor();
    ~CEditor();
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
private:
    // ファイル操作
    void FileOperation(void);

    // メニュー
    void Menu(void);
private:
    CCreateWorld *m_Createworld;
    CCreateSubsidiary * m_CreateSubsidiary;
    CCreateWorldObj * m_CreateWorldObj;
    CRelayPoint * m_RelayPoint;

    CImGui_Dx9 m_ImGui; // imgui

    CLoadCSV m_CSV; // csv
    std::string m_OldCSV; // 現在編集中のところ
};

#endif // !_Editor_H_
