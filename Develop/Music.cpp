//==========================================================================
// ミュージック[Music.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Music.h"
#include "resource_list.h"

CMusic::CMusic()
{
    CObjectVectorManager<CXAudio2::SoundLabel> Label;
    FILE *fp;
    char cText1[1024] = { 0 };
    char cText2[1024] = { 0 };
    int ret = 0;
    int nCount = 0;
    int NumData = 0;
    std::string strSoundName = RESOURCE_SoundList_csv; // csvファイル名

                                                       // csvを開く
    fp = fopen(strSoundName.c_str(), "r");
    if (fp)
    {
        // 行数計算
        for (;;)
        {
            cText1[0] = 0;
            ret = fscanf(fp, "%s", &cText1);
            if (ret == EOF) { break; }
            nCount++;
        }
        fclose(fp);

        // データ数の登録
        NumData = nCount - 1;

        if (NumData != 0)
        {
            // csvを開く
            fp = fopen(strSoundName.c_str(), "r");
            if (fp)
            {
                // 説明行の読み込み
                fscanf(fp, "%s", cText1);

                // メインの読み込み
                for (int i = 0; i < NumData; i++)
                {
                    CXAudio2::SoundLabel *lab = Label.Create();
                    cText1[0] = 0;
                    cText2[0] = 0;

                    fscanf(fp, "%[^,],%[^,],%d,%f", &cText1, &cText2, &lab->m_CntLoop, &lab->m_Volume);
                    lab->m_strName = cText1;
                    lab->m_strName += cText2;

                    // サウンドの登録
                    *this->m_stdSoundnameList.Create() = cText2;

                    // ファイルをパスの修正 \n排除
                    std::string filename;
                    for (int s = 1; s < (int)lab->m_strName.size(); s++)
                    {
                        filename += lab->m_strName[s];
                    }
                    // 再登録
                    lab->m_strName = filename;
                }
                fclose(fp);
            }
        }
    }
    else
    {
        CDeviceManager::GetDXDevice()->ErrorMessage("ファイルが読み込めませんでした >> %s \n サーバーから再度ダウンロードしてください。", strSoundName.c_str());
        Label.Release();
    }

    for (int i = 0; i < Label.Size(); i++)
    {
        CXAudio2::SoundLabel *lab = Label.Get(i);
        if (FAILED(this->m_music.Init(*lab)))
        {
            this->m_music.Release();
            this->m_stdSoundnameList.Release();
        }
    }

    Label.Release();
}

CMusic::~CMusic()
{
    this->Uninit();
}

//==========================================================================
// 初期化
bool CMusic::Init(void)
{
    // オブジェクトの初期化
    return CObject::InitAll();
}

//==========================================================================
// 解放
void CMusic::Uninit(void)
{
    this->m_stdSoundnameList.Release();
    this->m_music.Release();
    CObject::ReleaseAll();
}

//==========================================================================
// 更新
void CMusic::Update(void)
{
    this->m_imgui.NewWindow("サウンドリスト", true);
    if (this->m_stdSoundnameList.Size() == 0)
    {
        this->m_imgui.Text("No Data");
    }

    for (int i = 0; i < this->m_stdSoundnameList.Size(); i++)
    {
        std::string * strName = this->m_stdSoundnameList.Get(i);

        if (this->m_imgui.NewMenu(strName->c_str()))
        {
            if (this->m_imgui.MenuItem("再生"))
            {
                this->m_music.Play(i);
            }
            if (this->m_imgui.MenuItem("停止"))
            {
                this->m_music.Stop(i);
            }
            float fVolume = this->m_music.GetVolume(i);
            this->m_imgui.SliderFloat("音量", &fVolume, 0.0f, 1.0f);
            this->m_music.SetVolume(i, fVolume);
            this->m_imgui.EndMenu();
        }
    }

    CObject::UpdateAll();
    this->m_imgui.EndWindow();
}

//==========================================================================
// 描画
void CMusic::Draw(void)
{
    CObject::DrawAll();
}