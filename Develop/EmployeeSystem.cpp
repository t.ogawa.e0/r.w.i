#include "EmployeeSystem.h"
#include "EmployeeGatya.h"
#include "GameEmployee.h"

CEmployeeSystem::CEmployeeSystem()
{
    CEmployeeGatya * EmployeeGatya;
    CGameEmployee * GameEmployee;

    CObject::NewObject(EmployeeGatya);
    CObject::NewObject(GameEmployee);
}

CEmployeeSystem::~CEmployeeSystem()
{
}

bool CEmployeeSystem::Init(void)
{
    return CObject::InitAll();
}

void CEmployeeSystem::Uninit(void)
{
    CObject::ReleaseAll();
}

void CEmployeeSystem::Update(void)
{
    CObject::UpdateAll();
}

void CEmployeeSystem::Draw(void)
{
    CObject::DrawAll();
}
