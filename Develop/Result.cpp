//==========================================================================
// リザルト[Result.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Result.h"
#include "WinImage.h"
#include "WinSymbol.h"
#include "WinSound.h"
#include "LoseImage.h"
#include "LoseSymbol.h"
#include "LoseSound.h"
#include "ResultReminiscence.h"
#include "resource_link.hpp"

CResult::CResult()
{
    this->m_key = EGameEndKey::LOSE;

    CWinImage *win;
    CWinSymbol *winS;
    CWinSound *winSound;
    CLoseImage *lose;
    CLoseSymbol *loseS;
    CLoseSound *loseSound;
    CResultReminiscence * ResultReminiscence;

    this->Load();

    // オブジェクトの登録

    if (this->m_key == EGameEndKey::LOSE)
    {
        CObject::NewObject(lose);
        CObject::NewObject(loseS);
        CObject::NewObject(loseSound);
    }
    else if (this->m_key == EGameEndKey::WIN)
    {
        CObject::NewObject(win);
        CObject::NewObject(winS);
        CObject::NewObject(winSound);
    }

    CObject::NewObject(ResultReminiscence);
}

CResult::~CResult()
{
}

//==========================================================================
// 初期化
bool CResult::Init(void)
{
    // オブジェクトの初期化
    return CObject::InitAll();
}

//==========================================================================
// 解放
void CResult::Uninit(void)
{
    CObject::ReleaseAll();
}

//==========================================================================
// 更新
void CResult::Update(void)
{
    CObject::UpdateAll();
}

//==========================================================================
// 描画
void CResult::Draw(void)
{
    CObject::DrawAll();
}

void CResult::Load(void)
{
    // オブジェクトの宣言
    std::string strFilename = "";

    strFilename += resource_link::data::system_file;
    strFilename += "/";
    strFilename += resource_link::data::system_file_result;

    // フォルダ生成
    if (_mkdir(resource_link::data::system_file) == 0) {}

    FILE *pFile = fopen(strFilename.c_str(), "rb");

    if (pFile)
    {
        fread(&this->m_key, sizeof(this->m_key), 1, pFile);
        fclose(pFile);
    }
}
