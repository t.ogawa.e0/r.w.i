//==========================================================================
// テストプログラム[test.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "test.h"

CTest::CTest()
{
    C2DData *p2dobj;
    C2DAnimData *p2danimobj;
    C3DGridData *p3dgrid;
    C3DCubeData *p3dcube;
    C3DBillboard *p3dbill;
    C3DMesh *p3dmesh;
    C3DSphere *p3dsphere;
    C3DXmodel *p3dxmodel;
    C3DText *pdxtext;
    C3DXInput * p3dxinput;
    C3DCamera * pcamera;

    CLight light; // ライト

    CObject::NewObject(p3dxinput);
    CObject::NewObject(p3dgrid);
    CObject::NewObject(p3dmesh);
    CObject::NewObject(p3dcube);
    CObject::NewObject(p3dsphere);
    CObject::NewObject(p3dxmodel);
    CObject::NewObject(p3dbill);
    CObject::NewObject(p2dobj);
    CObject::NewObject(p2danimobj);
    CObject::NewObject(pdxtext);
    CObject::NewObject(pcamera);

    light.Init({ 0, -1, 1 });
}

CTest::~CTest()
{
}

//==========================================================================
//
// class  : CTest
// Content: テスト用
//
//==========================================================================
bool CTest::Init(void)
{
    return CObject::InitAll();
}

void CTest::Uninit(void)
{
    CObject::ReleaseAll();
}

void CTest::Update(void)
{
    CObject::UpdateAll();
}

void CTest::Draw(void)
{
    CObject::DrawAll();
}

//==========================================================================
//
// class  : C2DData
// Content: 2D描画
//
//==========================================================================

//==========================================================================
// 初期化
bool C2DData::Init(void)
{
    // 複数のテクスチャを読み込ませたいときの書き方
    const char * ptexcase[] =
    {
        "resource/texture/test/10550007_d1e41729.DDS",
        "resource/texture/test/10550007_d1e41729.DDS",
    };
    ptexcase; // Initの引数にすると一気に初期化ができる。

              // 初期化
    this->_2DObject()->Create();
    this->_2DObject()->Get(0)->Init(0);

    // オブジェクトの登録
    this->_2DPolygon()->ObjectInput(this->_2DObject()->Get(0));

    // 2D画像の読み込み
    return this->_2DPolygon()->Init("resource/texture/test/10550007_d1e41729.DDS", true);
}

//==========================================================================
// 解放
void C2DData::Uninit(void)
{
}

//==========================================================================
// 更新
void C2DData::Update(void)
{
    if (this->Dinput_Keyboard()->Trigger(CKeyboard::KeyList::KEY_0))
    {
        this->_2DPolygon()->ObjectDelete(this->_2DObject()->Get(0));
    }
    if (this->Dinput_Keyboard()->Trigger(CKeyboard::KeyList::KEY_9))
    {
        this->_2DPolygon()->ObjectInput(this->_2DObject()->Get(0));
    }

    // 必ず呼ぶこと
    this->_2DPolygon()->Update();
}

//==========================================================================
// 描画
void C2DData::Draw(void)
{
    // 2Dの描画
    this->_2DPolygon()->Draw();
}

//==========================================================================
//
// class  : C2DAnimData
// Content: 2Dアニメーション描画
//
//==========================================================================

//==========================================================================
// 初期化
bool C2DAnimData::Init(void)
{
    // 複数のテクスチャを読み込ませたいときの書き方
    const char * ptexcase[] =
    {
        "resource/texture/test/10550007d1e41729.DDS",
        "resource/texture/test/10550007d1e41729.DDS",
    };
    ptexcase; // Initの引数にすると一気に初期化ができる。

              // 2Dアニメーション用の初期化
    this->_2DObject()->Create();
    this->_2DObject()->Get(0)->Init(0, 5, 12, 5);

    // アニメーション用のカウンタの初期化
    this->_2DPolygon()->AnimationCountInit(&this->m_animcount);

    // オブジェクトの登録
    this->_2DPolygon()->ObjectInput(this->_2DObject()->Get(0));

    // 2D画像の読み込み
    return this->_2DPolygon()->Init("resource/texture/test/10550007d1e41729.DDS", true);
}

//==========================================================================
// 解放
void C2DAnimData::Uninit(void)
{
}

//==========================================================================
// 更新
void C2DAnimData::Update(void)
{
    // アニメーションの終了と同時にカウンタを初期化するための関数
    this->_2DPolygon()->GetPattanNum(*this->_2DObject()->Get(0), &this->m_animcount);

    // アニメーションカウンタはここで回す
    this->m_animcount++;

    // アニメーションさせる場合カウンタをセットする
    this->_2DObject()->Get(0)->SetAnimationCount(this->m_animcount);

    // 必ず呼ぶこと
    this->_2DPolygon()->Update();
}

//==========================================================================
// 描画
void C2DAnimData::Draw(void)
{
    // 2D描画
    this->_2DPolygon()->Draw();
}

//==========================================================================
//
// class  : C3DGridData
// Content: グリッド描画
//
//==========================================================================

//==========================================================================
// 初期化
bool C3DGridData::Init(void)
{
    // グリッドの初期化 引数はグリッドのサイズ
    this->_3DGrid()->Init(20);

    return false;
}

//==========================================================================
// 解放
void C3DGridData::Uninit(void)
{
}

//==========================================================================
// 更新
void C3DGridData::Update(void)
{
}

//==========================================================================
// 描画
void C3DGridData::Draw(void)
{
    // グリッドの描画
    this->_3DGrid()->Draw();
}

//==========================================================================
//
// class  : C3DCubeData
// Content: キューブの描画
//
//==========================================================================

//==========================================================================
// 初期化
bool C3DCubeData::Init(void)
{
    // 複数のテクスチャを読み込ませたいときの書き方
    const char * ptexcase[] =
    {
        "resource/texture/test/19949738_sfaees.DDS",
        "resource/texture/test/97465168_hsar.DDS",
    };
    ptexcase; // Initの引数にすると一気に初期化ができる。

    for (int i = 0; i < this->Helper()->Sizeof_(ptexcase); i++)
    {
        this->_3DObject()->Create();
        this->_3DObject()->Get(i)->Init(i);
    }

    this->_3DObject()->Get(0)->MoveX(-1);
    this->_3DObject()->Get(1)->MoveX(1);

    // オブジェクトの登録
    this->_3DCube()->ObjectInput(this->_3DObject()->Get(0));
    this->_3DCube()->ObjectInput(this->_3DObject()->Get(1));

    // ダブルポインタ使用時の初期化
    // this->m_Temp.Sizeof_を使用し、要素数を渡します
    return this->_3DCube()->Init(ptexcase, this->Helper()->Sizeof_(ptexcase));
}

//==========================================================================
// 解放
void C3DCubeData::Uninit(void)
{
}

//==========================================================================
// 更新
void C3DCubeData::Update(void)
{
    // 回転させるための値を入れています
    const D3DXVECTOR3 *rot1 = this->_3DObject()->Get(0)->GetMatInfoRot();
    const D3DXVECTOR3 *rot2 = this->_3DObject()->Get(1)->GetMatInfoRot();

    this->_3DObject()->Get(0)->SetMatInfoRot({ rot1->x + 0.025f,rot1->y,rot1->z + 0.025f });
    this->_3DObject()->Get(1)->SetMatInfoRot({ rot2->x - 0.025f,rot2->y,rot2->z - 0.025f });

    // 必ず呼ぶ
    this->_3DCube()->Update();
}

//==========================================================================
// 描画
void C3DCubeData::Draw(void)
{
    // 座標が二つ存在するのでforで回します。
    // this->m_Temp.Sizeof_とは、その変数の要素数を出してくれる関数です。
    for (int i = 0; i < this->_3DObject()->Size(); i++)
    {
        // 向きベクトルの描画
        this->_3DObject()->Get(i)->Draw(10, this->GetDirectX9Device());
    }

    // キューブの描画
    this->_3DCube()->Draw();
}

//==========================================================================
//
// class  : C3DBillboard
// Content: ビルボードの描画
//
//==========================================================================

//==========================================================================
// 初期化
bool C3DBillboard::Init(void)
{
    // 複数のテクスチャを読み込ませたいときの書き方
    const char * ptexcase[] =
    {
        "resource/texture/test/qPbkxvY4_400x400.DDS",
        "resource/texture/test/qPbkxvY4_400x400.DDS",
    };
    ptexcase; // Initの引数にすると一気に初期化ができる。

              // アニメーションに使う変数を初期化します。
    this->_3DBillboard()->AnimationCountInit(&this->m_animcount);

    // 座標の初期化です
    this->_3DObject()->Create();
    this->_3DObject()->Get(0)->Init(0);
    this->_3DObject()->Get(0)->MoveX(-2);
    this->_3DObject()->Get(0)->MoveY(5);
    this->_3DObject()->Get(0)->Scale(1);

    // 座標の初期化です
    this->_3DObject()->Create();
    this->_3DObject()->Get(1)->Init(1);
    this->_3DObject()->Get(1)->MoveX(2);
    this->_3DObject()->Get(1)->MoveY(5);
    this->_3DObject()->Get(1)->Scale(1);

    // ダブルポインタ使用時の初期化コメントアウトを外せば有効化されます
    //if (this->m_bill.Init(ptexcase, this->m_Temp.Sizeof_(ptexcase), CBillboard::PlateList::Vertical))
    //{
    //	return true;
    //}

    // 中はリスト構造なのでシングルポインタで小分けして初期化も可能！！
    if (this->_3DBillboard()->Init("resource/texture/test/qPbkxvY4_400x400.DDS", CBillboard::PlateList::Vertical))
    {
        return true;
    }

    // 中はリスト構造なのでシングルポインタで小分けして初期化も可能！！
    // なんとアニメーションの初期化まで可能！！
    if (this->_3DBillboard()->Init("resource/texture/test/12250000efa.DDS", 5, 12, 5, CBillboard::PlateList::Vertical))
    {
        return true;
    }

    // オブジェクトの登録
    this->_3DBillboard()->ObjectInput(this->_3DObject()->Get(1));
    this->_3DBillboard()->ObjectInput(this->_3DObject()->Get(0));

    return false;
}

//==========================================================================
// 解放
void C3DBillboard::Uninit(void)
{
}

//==========================================================================
// 更新
void C3DBillboard::Update(void)
{
    // ビルボードアニメーション用の変数を引数に入れることにより、アニメーションを有効にします。
    this->_3DBillboard()->UpdateAnimation(this->m_animcount);

    // 必ず呼ぶこと 
    this->_3DBillboard()->Update(this->GetObjects(CObject::ID::Camera, CObject::Type::Game_Camera)->GetMtxView(), &this->m_animcount);
}

//==========================================================================
// 描画
void C3DBillboard::Draw(void)
{
    // 座標が二つ存在するのでforで回します。
    // this->m_Temp.Sizeof_とは、その変数の要素数を出してくれる関数です。
    for (int i = 0; i < this->_3DObject()->Size(); i++)
    {
        // 向きベクトルの描画
        this->_3DObject()->Get(i)->Draw(10, this->GetDirectX9Device());
    }

    // ビルボードの描画
    this->_3DBillboard()->Draw(false);
}

//==========================================================================
//
// class  : C3DMesh
// Content: メッシュの描画
//
//==========================================================================

//==========================================================================
// 初期化
bool C3DMesh::Init(void)
{
    // 座標が二つ存在するのでforで回します。
    // this->m_Temp.Sizeof_とは、その変数の要素数を出してくれる関数です。
    for (int i = 0; i < 2; i++)
    {
        this->_3DObject()->Create();
        this->_3DObject()->Get(i)->Init(0);
        this->_3DObject()->Get(i)->Scale(3);
    }

    // 回転させるための値を入れています

    float f90 = ANGLE_t::_ANGLE_090;
    const D3DXVECTOR3 rot1 = { f90 ,f90 ,f90 };
    const D3DXVECTOR3 rot2 = { f90 ,f90 ,-f90 };

    this->_3DObject()->Get(0)->SetMatInfoRot(rot1);
    this->_3DObject()->Get(1)->SetMatInfoRot(rot2);

    // オブジェクトの登録
    this->_3DMesh()->ObjectInput(this->_3DObject()->Get(0));
    this->_3DMesh()->ObjectInput(this->_3DObject()->Get(1));

    // メッシュの初期化
    return this->_3DMesh()->Init("resource/texture/test/stand_ssr2.DDS", 10, 2);
}

//==========================================================================
// 解放
void C3DMesh::Uninit(void)
{
}

//==========================================================================
// 更新
void C3DMesh::Update(void)
{
    // 必ず呼ぶ
    this->_3DMesh()->Update();
}

//==========================================================================
// 描画
void C3DMesh::Draw(void)
{
    // 座標が二つ存在するのでforで回します。
    // this->m_Temp.Sizeof_とは、その変数の要素数を出してくれる関数です。
    for (int i = 0; i < this->_3DObject()->Size(); i++)
    {
        // 向きベクトルの描画
        this->_3DObject()->Get(i)->Draw(20, this->GetDirectX9Device());
    }

    // メッシュの描画
    this->_3DMesh()->Draw();
}

//==========================================================================
//
// class  : C3DSphere
// Content: 球体の描画
//
//==========================================================================

//==========================================================================
// 初期化
bool C3DSphere::Init(void)
{
    // 座標の初期化 入れている数字は使用する球体の番号です
    C3DObject * pos = this->_3DObject()->Create();
    pos->Init(0);

    // サイズの加算を行っています。マイナスの値を入れることにより天球になります。
    pos->Scale(-30);

    // オブジェクトの登録
    this->_3DSphere()->ObjectInput(pos);

    // 球体の初期化です。
    return this->_3DSphere()->Init("resource/texture/test/BS02s.DDS", 18);
}

//==========================================================================
// 解放
void C3DSphere::Uninit(void)
{
}

//==========================================================================
// 更新
void C3DSphere::Update(void)
{
    // 必ず呼ぶ
    this->_3DSphere()->Update();
}

//==========================================================================
// 描画
void C3DSphere::Draw(void)
{
    C3DObject * pos = this->_3DObject()->Get(0);

    // 向きベクトルの描画
    pos->Draw(30, this->GetDirectX9Device());

    // 球体の描画
    this->_3DSphere()->Draw();
}

//==========================================================================
//
// class  : C3DXmodel
// Content: Xモデルの描画
//
//==========================================================================

//==========================================================================
// 初期化
bool C3DXmodel::Init(void)
{
    // 座標の初期化 入れている数字は使用するモデルの番号です
    C3DObject * pos = this->_3DObject()->Create();
    pos->Init(0);

    // サイズを加算してます
    pos->Scale(1);

    // オブジェクトの登録
    this->_3DXmodel()->ObjectInput(pos);

    this->XInput()->Init(1);

    // モデルの初期化です、ファイルパスとライティングの種類の指定を行います。
    return this->_3DXmodel()->Init("resource/3DModel/Alicia/Alicia_solid.x", CXmodel::LightMoad::Material);
}

//==========================================================================
// 解放
void C3DXmodel::Uninit(void)
{
}

//==========================================================================
// 更新
void C3DXmodel::Update(void)
{
    // コントローラーのチェック
    if (this->XInput()->Check(0))
    {
        D3DXVECTOR3 vecRight;

        if (this->XInput()->AnalogL(0, &vecRight))
        {
            C3DObject * pos = this->_3DObject()->Get(0);
            pos->RotX(vecRight, 0.5f);
            pos->MoveZ(0.05f);

            D3DXVECTOR3 vpos = *pos->GetMatInfoPos();
            this->GetObjects(CObject::ID::Camera, CObject::Type::Game_Camera)->SetCameraLockOn(vpos);
        }
    }

    // 必ず呼ぶ
    this->_3DXmodel()->Update();
    this->XInput()->Update();
}

//==========================================================================
// 描画
void C3DXmodel::Draw(void)
{
    C3DObject * pos = this->_3DObject()->Get(0);

    // 向きベクトルの描画
    pos->Draw(30, this->GetDirectX9Device());

    // ｘモデルの描画
    this->_3DXmodel()->Draw();
}

//==========================================================================
//
// class  : C3DText
// Content: テキストの描画
//
//==========================================================================

//==========================================================================
// 初期化
bool C3DText::Init(void)
{
    this->_2DText()->Init(this->GetWinSize().m_Width, this->GetWinSize().m_Height, "resource/font/meiryo/meiryo.ttc");
    return false;
}

//==========================================================================
// 解放
void C3DText::Uninit(void)
{
}

//==========================================================================
// 更新
void C3DText::Update(void)
{
}

//==========================================================================
// 描画
void C3DText::Draw(void)
{
    this->_2DText()->textnew();
    this->_2DText()->text("テ");
    this->_2DText()->text("キ");
    this->_2DText()->text("ス");
    this->_2DText()->text("ト");
    this->_2DText()->textend();
}

//==========================================================================
//
// class  : C3DCamera
// Content: カメラの設定
//
//==========================================================================

//==========================================================================
// 初期化
bool C3DCamera::Init(void)
{
    D3DXVECTOR3 Eye = D3DXVECTOR3(0.0f, 3.0f, -10.0f); // 注視点
    D3DXVECTOR3 At = D3DXVECTOR3(0.0f, 3.0f, 0.0f); // カメラ座標

    this->Camera()->Init(Eye, At);

    this->XInput()->Init(1);

    return false;
}

//==========================================================================
// 解放
void C3DCamera::Uninit(void)
{
}

//==========================================================================
// 更新
void C3DCamera::Update(void)
{
    // コントローラーのチェック
    if (this->XInput()->Check(0) == true)
    {
        D3DXVECTOR3 vecRight;
        if (this->XInput()->AnalogR(0, &vecRight))
        {
            this->Camera()->RotViewX(vecRight.x*0.05f);
            this->Camera()->RotViewY(vecRight.z*0.02f);
        }
    }
    else if (this->Dinput_Keyboard()->Press(CKeyboard::KeyList::KEY_UP))
    {
        this->Camera()->DistanceFromView(0.05f);
    }
    else if (this->Dinput_Keyboard()->Press(CKeyboard::KeyList::KEY_DOWN))
    {
        this->Camera()->DistanceFromView(-0.05f);
    }

    if (this->Dinput_Keyboard()->Press(CKeyboard::KeyList::KEY_LEFT))
    {
        this->Camera()->RotViewX(0.05f);
    }
    else if (this->Dinput_Keyboard()->Press(CKeyboard::KeyList::KEY_RIGHT))
    {
        this->Camera()->RotViewX(-0.05f);
    }
    else
    {
        //this->m_camera.RotViewX(0.0025f);
    }
    this->XInput()->Update();
    this->Camera()->Update(this->GetWinSize().m_Width, this->GetWinSize().m_Height, this->GetDirectX9Device());
}

//==========================================================================
// 描画
void C3DCamera::Draw(void)
{
}

//==========================================================================
// 初期化
bool C3DXInput::Init(void)
{
    this->XInput()->Init(1);
    return false;
}

//==========================================================================
// 解放
void C3DXInput::Uninit(void)
{
}

//==========================================================================
// 更新
void C3DXInput::Update(void)
{
    static const char * pcbool[] = { "false","true" };

    this->ImGui()->NewWindow("Xboxのコントローラーのテスト", true);
    // コントローラーのチェック
    if (this->XInput()->Check(0))
    {
        this->ImGui()->Text("プレス");
        this->ImGui()->Text("Aボタン : %s", pcbool[this->XInput()->Press(CXInput::EButton::A, 0)]);
        this->ImGui()->Text("Bボタン : %s", pcbool[this->XInput()->Press(CXInput::EButton::B, 0)]);
        this->ImGui()->Text("Xボタン : %s", pcbool[this->XInput()->Press(CXInput::EButton::X, 0)]);
        this->ImGui()->Text("Yボタン : %s", pcbool[this->XInput()->Press(CXInput::EButton::Y, 0)]);

        this->ImGui()->Text("STARTボタン : %s", pcbool[this->XInput()->Press(CXInput::EButton::START, 0)]);
        this->ImGui()->Text("BACKボタン : %s", pcbool[this->XInput()->Press(CXInput::EButton::BACK, 0)]);

        this->ImGui()->Text("十字ボタン 左 : %s", pcbool[this->XInput()->Press(CXInput::EButton::DPAD_LEFT, 0)]);
        this->ImGui()->Text("十字ボタン 右 : %s", pcbool[this->XInput()->Press(CXInput::EButton::DPAD_RIGHT, 0)]);
        this->ImGui()->Text("十字ボタン 上 : %s", pcbool[this->XInput()->Press(CXInput::EButton::DPAD_UP, 0)]);
        this->ImGui()->Text("十字ボタン 下 : %s", pcbool[this->XInput()->Press(CXInput::EButton::DPAD_DOWN, 0)]);

        this->ImGui()->Text("LBボタン : %s", pcbool[this->XInput()->Press(CXInput::EButton::LEFT_LB, 0)]);
        this->ImGui()->Text("LTボタン : %s", pcbool[this->XInput()->LT(0)]);
        this->ImGui()->Text("RBボタン : %s", pcbool[this->XInput()->Press(CXInput::EButton::RIGHT_RB, 0)]);
        this->ImGui()->Text("RTボタン : %s", pcbool[this->XInput()->RT(0)]);

        this->ImGui()->Text("左アナログスティックのボタン : %s", pcbool[this->XInput()->Press(CXInput::EButton::LEFT_THUMB, 0)]);
        this->ImGui()->Text("右アナログスティックのボタン : %s", pcbool[this->XInput()->Press(CXInput::EButton::RIGHT_THUMB, 0)]);

        D3DXVECTOR3 AnalogLKey;
        D3DXVECTOR3 AnalogRKey;
        this->ImGui()->Text("左アナログスティックを押しているか : %s", pcbool[this->XInput()->AnalogL(0, &AnalogLKey)]);
        this->ImGui()->Text(" > ベクトル情報 : x = %.2f , y = %.2f , z = %.2f", AnalogLKey.x, AnalogLKey.y, AnalogLKey.z);
        this->ImGui()->Text(" > ベクトル情報 : x = %d , y = %d", this->XInput()->GetState(0)->Gamepad.sThumbLX, this->XInput()->GetState(0)->Gamepad.sThumbLY);
        this->ImGui()->Text("右アナログスティックを押しているか : %s", pcbool[this->XInput()->AnalogR(0, &AnalogRKey)]);
        this->ImGui()->Text(" > ベクトル情報 : x = %.2f , y = %.2f , z = %.2f", AnalogRKey.x, AnalogRKey.y, AnalogRKey.z);
        this->ImGui()->Text(" > ベクトル情報 : x = %d , y = %d", this->XInput()->GetState(0)->Gamepad.sThumbRX, this->XInput()->GetState(0)->Gamepad.sThumbRY);

        this->ImGui()->Text("左アナログトリガー 上 : %s", pcbool[this->XInput()->AnalogLTrigger(CXInput::EAnalog::UP, 0)]);
        this->ImGui()->Text("左アナログトリガー 下 : %s", pcbool[this->XInput()->AnalogLTrigger(CXInput::EAnalog::DOWN, 0)]);
        this->ImGui()->Text("左アナログトリガー 左 : %s", pcbool[this->XInput()->AnalogLTrigger(CXInput::EAnalog::LEFT, 0)]);
        this->ImGui()->Text("左アナログトリガー 右 : %s", pcbool[this->XInput()->AnalogLTrigger(CXInput::EAnalog::RIGHT, 0)]);

        this->ImGui()->Text("右アナログトリガー 上 : %s", pcbool[this->XInput()->AnalogRTrigger(CXInput::EAnalog::UP, 0)]);
        this->ImGui()->Text("右アナログトリガー 下 : %s", pcbool[this->XInput()->AnalogRTrigger(CXInput::EAnalog::DOWN, 0)]);
        this->ImGui()->Text("右アナログトリガー 左 : %s", pcbool[this->XInput()->AnalogRTrigger(CXInput::EAnalog::LEFT, 0)]);
        this->ImGui()->Text("右アナログトリガー 右 : %s", pcbool[this->XInput()->AnalogRTrigger(CXInput::EAnalog::RIGHT, 0)]);
    }
    else
    {
        this->ImGui()->Text("Xboxのコントローラーがありません。");
    }
    this->XInput()->Update();
    this->ImGui()->EndWindow();
}

//==========================================================================
// 描画
void C3DXInput::Draw(void)
{
}
