//==========================================================================
// プレイヤーのパラメーター[PlayerParam.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _SubsidiaryParam_h_
#define _SubsidiaryParam_h_

//==========================================================================
// include
//==========================================================================
#include <list>

//==========================================================================
//
// class  : CPlayerParam
// Content: プレイヤーのパラメーター
//
//==========================================================================
class CPlayerParam
{
public:
	CPlayerParam()
	{
        this->m_asset = 0;
        this->m_old_asset = 0;
		this->m_total_employee = 0;
		this->m_subsidiary = 0;
		this->m_wage = 0;
		this->m_wage_sub = 0;
		this->m_total_wage = 0;
		this->m_dissatisfied = 0;
        //this->m_dissatisfied_limit = 9999999;
	}

	// asset 資産
	// subsidiary 子会社数
	// wage 賃金
	CPlayerParam(int asset, int subsidiary, int wage)
	{
        this->m_old_asset = this->m_asset = asset;
        this->m_total_employee = 0;
		this->m_subsidiary = subsidiary;
		this->m_wage = wage;
		this->m_wage_sub = wage;
		this->m_total_wage = 0;
        this->m_dissatisfied = 0;
        //this->m_dissatisfied_limit = 9999999;
	}
	~CPlayerParam() {}

	// 更新
	void Update(void);

	// 不満度が最大になった！！
	bool DissatisfiedHit(void);
public:
	int m_asset; // 資産(自動化)
    int m_old_asset; // 前回の資産(自動化)
	int m_total_employee; // 総従業員(自動化)
	int m_subsidiary; // 子会社数(自動化)
	int m_wage; // 賃金
	int m_wage_sub; // 視覚情報の賃金(操作用)
	int m_total_wage; // 総従賃金(自動化)
    int m_dissatisfied; // 不満度(自動化)
    //int m_dissatisfied_limit; // 不満度(操作用)
};

#endif // !_SubsidiaryParam_h_
