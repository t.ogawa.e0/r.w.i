//==========================================================================
// ゲームキャラクター継承用[Character.h]
// author: tatsuya ogawa
//==========================================================================
#include "Character.h"

//==========================================================================
// 実体化
//==========================================================================
std::vector<CCharacter*> CCharacter::m_charcase;

//==========================================================================
// テキスト設定
void CCharacter::Text(const std::string & Inout)
{
    if (this->m_imgui.NewMenu(Inout.c_str(), true))
    {
        this->m_imgui.Text("資産 : %d/ゴールド", this->m_param.m_asset);
        this->m_imgui.Text("総従業員 : %d/人", this->m_param.m_total_employee);
        this->m_imgui.Text("子会社数 : %d/社", this->m_param.m_subsidiary);
        this->m_imgui.Text("賃金 : %d/ゴールド", this->m_param.m_wage);
        this->m_imgui.Text("総従賃金 : %d/ゴールド", this->m_param.m_total_wage);
        this->m_imgui.Text("社員の不満度 : %d/%d", this->m_param.m_dissatisfied, this->m_param.m_wage);
        this->m_imgui.EndMenu();
    }
}

//==========================================================================
// 社員一揆の処理
bool CCharacter::Strike(void)
{
    // ストライキ判定の無効
    this->m_strike_key = false;

	// 不満度が最大値と一致、超えた場合処理が発生
	if (this->m_param.DissatisfiedHit())
	{
		// 社数分
		for (int i = 0; i < this->m_psub->GetNumSub(); i++)
		{
			// アドレス渡し
			CSubsidiaryParam * psubsid = this->m_psub->GetSub(i);
			// 視覚情報と内部データの人数を半分にする
			psubsid->m_employee_sub = psubsid->m_employee = (int)((psubsid->m_employee / 2) + 0.5f);
		}
        this->m_strike_key = true;
        return true;
	}
    return false;
}

//==========================================================================
// 不満度の処理
void CCharacter::Dissatisfied(int ntime)
{
	// 一秒経過ごとに不満度が減少
	if (this->m_oldtime != ntime)
	{
        // 不満度が0でない限り処理を続ける。
        if (0 < this->m_param.m_dissatisfied)
        {
            this->m_param.m_dissatisfied--;
        }
        else if (this->m_param.m_dissatisfied > 0)
        {
            this->m_param.m_dissatisfied = 0;
        }
		// 時間更新
		this->m_oldtime = ntime;
	}
}

//==========================================================================
// パラメーターの更新
void CCharacter::UpdateParam(const CGameManager::TURNS_t & InputTime)
{
	// ワールド時間の更新が入ったときに処理が発生する。
	if (InputTime.m_current != InputTime.m_old)
	{
        this->m_param.m_old_asset = this->m_param.m_asset;

		// 資産に利益の加算
        this->m_param.m_asset = this->m_param.m_asset + this->m_psub->GetAsset();

        if (this->m_param.m_asset <= 0)
        {
            this->m_param.m_asset = 0;
        }

		// 総従業員に加算
		this->m_param.m_total_employee = this->m_psub->GetMaxNemployee();

        if (this->m_param.m_total_employee <= 0)
        {
            this->m_param.m_total_employee = 0;
        }
		// 情報の更新
		this->m_param.Update();
	}
}

//==========================================================================
// 雇用
void CCharacter::Employment(CSubsidiaryParam * pSub, int NumEmployment)
{
	// ここで採用人数を加算しています。
	// 視覚情報に加算
	pSub->m_employee_sub = pSub->m_employee_sub + NumEmployment;
}

//==========================================================================
// 解雇
void CCharacter::Fire(CSubsidiaryParam * pSub, int NumEmployment)
{
	// テスト用なので解雇時の不満度上昇率は、5になっています。
	// 人数分加算。
    
	this->m_param.m_dissatisfied += (int)(this->m_param.m_wage_sub / 2);

	// 視覚情報から解雇人数を引いています。
	pSub->m_employee_sub = pSub->m_employee_sub - NumEmployment;
}

//==========================================================================
// 転勤
// pNow 今の場所
// pNext 移動先
bool CCharacter::Transferred(CSubsidiaryParam * pNow, CSubsidiaryParam * pNext, CSubsidiaryManager * InputPlayeData, int NumEmployment)
{
    bool ppNextTag = false;

    // 転勤時の不満度はテスト用で2に固定化されています。
    this->m_param.m_dissatisfied += (5 * NumEmployment);

    // 転勤元から転勤させる人数を引きます。
    //pNow->m_employee_sub = pNow->m_employee_sub - NumEmployment;

    // 転勤先の情報と、転勤人数、移動にかかるターンを処理スタックに登録します。
    this->m_psub->SetTransfer(pNext, NumEmployment, this->RootSearch(pNow, pNext, InputPlayeData, ppNextTag));
    pNow;
    pNext;
    InputPlayeData;
    NumEmployment;

    return ppNextTag;
}

//==========================================================================
// ルート探索
int CCharacter::RootSearch(CSubsidiaryParam * pNow, CSubsidiaryParam * pNext, CSubsidiaryManager * InputPlayeData, bool & pOutNextFlag)
{
    // 中継ポインタ探索 next
    int root = 1; // 初期ルート
    for (int search = pNow->m_id;;) 
    {
        // IDの探索
        search = InputPlayeData->GetSub(search)->m_next;

        // ID一致時
        if (pNext->m_id == search)
        {
            pOutNextFlag = true;
            break;
        }

        // 一致せずに終了IDが出た場合
        if (search == -1)
        {
            root = -1;
            break;
        }
        root++; // ルートカウンタ
    }

    // 中継ポインタ探索 nextが失敗時
    if (root == -1)
    {
        // 中継ポインタ探索 prev
        root = 1;
        for (int search = pNow->m_id;;)
        {
            // IDの探索
            search = InputPlayeData->GetSub(search)->m_prev;

            // ID一致時
            if (pNext->m_id == search)
            {
                pOutNextFlag = false;
                break;
            }

            // 一致せずに終了IDが出た場合
            if (search == -1)
            {
                root = -1;
                break;
            }
            root++; // ルートカウンタ
        }
    }

    return root;
}
