//==========================================================================
// 罪深いイメージ[Sinful_Image.h]
// author: tatsuya ogawa
//==========================================================================
#include "Sinful_Image.h"
#include "Game_Start.h"
#include "resource_list.h"
#include "resource_link.hpp"

CSinful_Image::CSinful_Image() : CObject(ID::Polygon2D)
{
    std::random_device rnd; // 非決定的な乱数生成器を生成
    std::mt19937 mt(rnd());  //  メルセンヌ・ツイスタの32ビット版、引数は初期シード値
    this->m_mt = mt; // シード値を記録
    this->SetType(Type::Game_Sinful_Image);
    this->m_sinful_sound = nullptr;
    this->m_start_script = nullptr;
}


CSinful_Image::~CSinful_Image()
{
}

//==========================================================================
// 初期化
bool CSinful_Image::Init(void)
{
    int __container = 0;
    this->m_sin_list[__container] = RESOURCE_TenkinCutin_001_DDS;
    __container++;
    this->m_sin_list[__container] = RESOURCE_TenkinCutin_002_DDS;
    __container++;
    this->m_sin_list[__container] = RESOURCE_TenkinCutin_003_DDS;
    __container++;
    this->m_sin_list[__container] = RESOURCE_TenkinCutin_004_DDS;
    __container++;
    this->m_sin_list[__container] = RESOURCE_TenkinCutin_005_DDS;
    __container++;
    this->m_sin_list[__container] = RESOURCE_TenkinCutin_006_DDS;
    __container++;
    this->m_sin_list[__container] = RESOURCE_TenkinCutin_007_DDS;
    __container++;
    this->m_sin_list[__container] = RESOURCE_TenkinCutin_008_DDS;
    __container++;
    this->m_sin_list[__container] = RESOURCE_TenkinCutin_009_DDS;
    __container++;
    this->m_sin_list[__container] = RESOURCE_TenkinCutin_010_DDS;
    __container++;
    this->m_sin_list[__container] = RESOURCE_TenkinCutin_011_DDS;
    __container++;
    this->m_sin_list[__container] = RESOURCE_TenkinCutin_012_DDS;
    __container++;
    this->m_sin_list[__container] = RESOURCE_TenkinCutin_013_DDS;
    __container++;
    this->m_sin_list[__container] = RESOURCE_TenkinCutin_014_DDS;
    __container++;
    this->m_sin_list[__container] = RESOURCE_TenkinCutin_015_DDS;
    __container++;
    this->m_sin_list[__container] = RESOURCE_TenkinCutin_016_DDS;
    __container++;
    this->m_sin_list[__container] = RESOURCE_TenkinCutin_017_DDS;
    __container++;
    this->m_sin_list[__container] = RESOURCE_TenkinCutin_018_DDS;
    __container++;
    this->m_sin_list[__container] = RESOURCE_TenkinCutin_019_DDS;

    // 乱数範囲
    this->m_rand = std::uniform_int_distribution<int>(0, __container);

    for (int i = 0; i < __container + 1; i++)
    {
        auto itr = this->m_sin_list.find(i);

        // パス情報が存在するか判定
        if (itr != this->m_sin_list.end())
        {
            // 存在するパスの読み込み
            if (this->_2DPolygon()->Init(itr->second.c_str(), true))
            {
                return true;
            }
        }
    }

    this->m_start_script = this->GetObjects(ID::Polygon2D, Type::Game_Start_Object);
    this->m_sinful_sound = this->GetObjects(ID::Default, Type::Game_Game_Sound);

    return false;
}

//==========================================================================
// 解放
void CSinful_Image::Uninit(void)
{
    this->Save();
    this->m_sin_list.clear();
    this->m_data.clear();
    this->m_log.clear();
}

//==========================================================================
// 更新
void CSinful_Image::Update(void)
{
    if (this->m_start_script == nullptr)
    {
        this->m_start_script = this->GetObjects(ID::Polygon2D, Type::Game_Start_Object);
    }    
    if (this->m_sinful_sound == nullptr)
    {
        this->m_sinful_sound = this->GetObjects(ID::Default, Type::Game_Game_Sound);
    }

    auto * p_start = (CGame_Start*)this->m_start_script;

    if (p_start != nullptr)
    {
        if (this->ImGui()->MenuItem("転勤カットイン") && p_start->Start() == true)
        {
            this->Create();
        }

        // オブジェクトの自動解放
        for (auto itr = this->m_data.begin(); itr != this->m_data.end(); )
        {
            // 終了キーが有効なとき
            if (itr->EndTime() == true)
            {
                // オブジェクトの登録解除
                this->_2DPolygon()->ObjectDelete(itr->GetObjects());

                // 対象メモリを解放し、仮のイテレータを返す
                itr = this->m_data.erase(itr);
            }
            else
            {
                // イテレータカウンタ
                ++itr;
            }
        }

        // オブジェクト個々の更新
        for (auto itr = this->m_data.begin(); itr != this->m_data.end(); ++itr)
        {
            itr->Update();
        }
    }

    // 描画情報生成
    this->_2DPolygon()->Update();
}

//==========================================================================
// 描画
void CSinful_Image::Draw(void)
{
    // 描画
    this->_2DPolygon()->Draw();
}

//==========================================================================
// 生成
void CSinful_Image::Create(void)
{
    CVector2<float> win_size((float)this->GetWinSize().m_Width, (float)this->GetWinSize().m_Height);

    // テクスチャパスの取り出し
    auto itr = this->m_sin_list.find(this->m_rand(this->m_mt));

    // パス情報が存在するか判定
    if (itr != this->m_sin_list.end())
    {
        // インスタンス生成
        this->m_data.emplace_back(itr->first, this->_2DPolygon(), win_size);

        this->m_log.push_back(itr->first);
    }
}

void CSinful_Image::Save(void)
{
    std::string strFilename = "";

    strFilename += resource_link::data::system_file;
    strFilename += "/";
    strFilename += resource_link::data::system_file_log;

    // フォルダ生成
    if (_mkdir(resource_link::data::system_file) == 0) {}
    FILE *pFile = fopen(strFilename.c_str(), "wb");
    if (pFile)
    {
        int n_size = (int)this->m_log.size();
        fwrite(&n_size, sizeof(n_size), 1, pFile); // データ数の書き込み
        for (auto itr = this->m_log.begin(); itr != this->m_log.end(); ++itr)
        {
            fwrite(&(*itr), sizeof((*itr)), 1, pFile); // データの書き込み
        }
        fclose(pFile);
    }
}

CSinful_Image::Sinful_Image_Data::Sinful_Image_Data()
{
    this->m_obj.Init(0);
    this->m_stop_time.Init(0, 30);
    this->m_end_time.Init(0, 30);
    this->m_color = 255;
    this->m_color.a = 0;
}

CSinful_Image::Sinful_Image_Data::Sinful_Image_Data(int _tex_id, C2DPolygon * Input, const CVector2<float>&v_winsize)
{
    CVector4<float> v_default({ v_winsize.x,v_winsize.y });

    this->m_obj.Init(_tex_id);
    this->m_stop_time.Init(1, 30);
    this->m_end_time.Init(1, 30);

    // 座標設定
    this->m_start_pos = CVector4<float>(v_winsize.x, 0.0f);
    this->m_stop_pos = CVector4<float>(0.0f, 0.0f);
    this->m_end_pos = CVector4<float>(-v_winsize.x, 0.0f);
    this->m_color = 255;
    this->m_color.a = 0;
    this->m_obj.SetPos(this->m_start_pos);

    Input->ObjectInput(this->m_obj);
}

CSinful_Image::Sinful_Image_Data::~Sinful_Image_Data()
{
}

//==========================================================================
// 終了キーが有効かどうか
bool CSinful_Image::Sinful_Image_Data::EndTime(void)
{
    if (this->m_end_time.GetComma() == 0 && this->m_end_time.GetTime() == 0)
    {
        return true;
    }
    return false;
}

//==========================================================================
// 更新
void CSinful_Image::Sinful_Image_Data::Update(void)
{
    float f_speed = 0.0f;
    // 停止時間ではないとき
    if (this->m_stop_time.Countdown() == false)
    {
        // オブジェクトの移動時間の生成
        f_speed = this->m_hit.Distance(this->m_stop_pos, *this->m_obj.GetPos())*0.1f;

        if (this->m_color.a != 255)
        {
            this->m_color.a += 10;
            if (255<this->m_color.a)
            {
                this->m_color.a = 255;
            }
        }
    }
    // 一時停止タイマーが0の時
    else
    {
        // オブジェクトの移動時間の生成
        f_speed = this->m_hit.Distance(this->m_end_pos, *this->m_obj.GetPos())*0.1f;

        if (0 != this->m_color.a)
        {
            this->m_color.a -= 10;
            if (this->m_color.a < 0)
            {
                this->m_color.a = 0;
            }
        }

        // 終了タイマーカウント
        this->m_end_time.Countdown();
    }

    // 移動速度の加算
    this->m_obj.SetColor(this->m_color);
    this->m_obj.SetXPlus(-f_speed);
}

//==========================================================================
// オブジェクトの取得
C2DObject & CSinful_Image::Sinful_Image_Data::GetObjects(void)
{
    return this->m_obj;
}
