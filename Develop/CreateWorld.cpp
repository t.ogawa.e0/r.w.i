//==========================================================================
// ワールド生成[CreateWorld.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "CreateWorld.h"

//==========================================================================
// 初期化
bool CCreateWorld::Init(void)
{
    this->m_gridsize = 20;
	this->_3DGrid()->Init(this->m_gridsize);
	this->m_VewCamera.Init();
    this->m_Vew = &this->m_VewCamera;
    this->m_pos.Init(0);

    return false;
}

//==========================================================================
// 解放
void CCreateWorld::Uninit(void)
{
    this->m_VewCamera.Release();
}

//==========================================================================
// 更新
void CCreateWorld::Update(void)
{
    if (this->ImGui()->NewMenu("グリッド"))
    {
        if (this->ImGui()->SliderInt("グリッドのサイズ変更", &this->m_gridsize, 1, 100))
        {
            this->_3DGrid()->Release();
            this->_3DGrid()->Init(this->m_gridsize);
        }
        this->ImGui()->EndMenu();
    }
    this->Camera();
    this->m_Vew = &this->m_VewCamera;
    this->m_VewCamera.Update(this->GetWinSize().m_Width, this->GetWinSize().m_Height, this->GetDirectX9Device());
}

//==========================================================================
// 描画
void CCreateWorld::Draw(void)
{
    this->_3DGrid()->Draw();
    this->m_pos.Draw(20, this->GetDirectX9Device());
}

//==========================================================================
// 移動
void CCreateWorld::Move(void)
{

}

//==========================================================================
// カメラ
void CCreateWorld::Camera(void)
{
    // ホイールを押したときの処理
    if (this->Dinput_Mouse()->Press(MOUSE_t::ButtonKey::Wheel))
    {
        this->m_VewCamera.MoveX(-(float)CDeviceManager::GetMouse()->Speed().m_lX*0.01f);
        this->m_VewCamera.MoveY((float)CDeviceManager::GetMouse()->Speed().m_lY*0.01f);
    }

    // 右クリックの時の処理
    if (this->Dinput_Mouse()->Press(MOUSE_t::ButtonKey::Right))
    {
        this->m_VewCamera.RotViewX((float)this->Dinput_Mouse()->Speed().m_lX*0.0025f);
        this->m_VewCamera.RotViewY((float)this->Dinput_Mouse()->Speed().m_lY*0.0025f);
        this->m_VewCamera.RotCameraX((float)this->Dinput_Mouse()->Speed().m_lX*0.0025f);
        this->m_VewCamera.RotCameraY((float)this->Dinput_Mouse()->Speed().m_lY*0.0025f);
        this->m_pos.RotX((float)this->Dinput_Mouse()->Speed().m_lX*0.005f);
    }

    // マウスの移動速度を加算
    if (this->m_VewCamera.DistanceFromView((float)this->Dinput_Mouse()->Speed().m_lZ*0.0025f)<8.0f)
    {
        this->m_VewCamera.DistanceFromView(-(float)this->Dinput_Mouse()->Speed().m_lZ*0.0025f);
    }

    // 視点の移動
    this->m_pos.SetMatInfoPos(this->m_VewCamera.GetVECTOR(CCamera::VectorList::VAT));
}
