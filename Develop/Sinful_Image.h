//==========================================================================
// 罪深いイメージ[Sinful_Image.h]
// author: tatsuya ogawa
//==========================================================================

#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CSinful_Image
// Content: 罪深いイメージ
//
//==========================================================================
class CSinful_Image : public CObject
{
private:
    class Sinful_Image_Data {
    public:
        Sinful_Image_Data();
        Sinful_Image_Data(int _tex_id, C2DPolygon * Input, const CVector2<float>&v_winsize);
        ~Sinful_Image_Data();

        // 終了キーが有効かどうか
        bool EndTime(void);

        // 更新
        void Update(void);

        // オブジェクトの取得
        C2DObject & GetObjects(void);
    private:
        C2DObject m_obj; // オブジェクト
        CTimer m_stop_time; // 停止時間制御タイマー
        CTimer m_end_time; // 終了制御タイマー
        CVector4<float>m_start_pos; // 開始地点
        CVector4<float>m_stop_pos; // 一時停止地点
        CVector4<float>m_end_pos; // 終了地点
        CColor<int>m_color; // 色
        CHitDetermination m_hit; // 当たり判定
    };
public:
    CSinful_Image();
    ~CSinful_Image();
    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;

    // 生成
    void Create(void);
private:
    void Save(void);
private:
    std::unordered_map<int, std::string>m_sin_list; // 罪リスト
    std::mt19937 m_mt; // 乱数
    std::uniform_int_distribution<int> m_rand; // 範囲の一様乱数
    std::list<Sinful_Image_Data>m_data; // データ
    void * m_start_script;
    std::list<int>m_log; // ログ
    void * m_sinful_sound; //罪深いサウンド
};

