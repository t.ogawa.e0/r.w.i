//==========================================================================
// デバイスマネージャー[DeviceManager.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _DeviceManager_H_
#define _DeviceManager_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "ObjectVectorManager.h"
#include "DXDevice.h"
#include "dinput_manager.h"
#include "dinput_mouse.h"
#include "dinput_keyboard.h"
#include "dinput_controller.h"
#include "XAudio2.h"

//==========================================================================
//
// class  : CDeviceManager
// Content: DeviceManager
//
//==========================================================================
class CDeviceManager
{
public:
    enum class DeviceInitList
    {
        Mouse, // マウス
        keyboard, // キーボード
        Controller, // コントローラー
        DXDevice, // DirectX9デバイス
        XAudio2, // XAudio2Device
        MAXDevice, // デバイス数
    };
public:
    CDeviceManager() {}
    ~CDeviceManager() {}

    // 初期化
    static bool Init(DeviceInitList Serect, HINSTANCE hInstance, HWND hWnd);

    // 解放
    static void Release(void);

    // 更新
    static void Update(void);

    // DirectInputマウス
    static CMouse * GetMouse(void) { return m_Mouse; }
    // DirectInputキーボード
    static CKeyboard * GetKeyboard(void) { return m_Keyboard; }
    // DirectInputコントローラー
    static CController * GetController(void) { return m_Controller; }
    // DirectX9デバイス
    static CDirectXDevice * GetDXDevice(void) { return m_DXDevice; }
private:
    static CObjectVectorManager<DeviceInitList> m_InitList; // 初期化終了情報の格納
    static CMouse * m_Mouse; // DirectInputマウス
    static CKeyboard * m_Keyboard; // DirectInputキーボード
    static CController * m_Controller; // DirectInputコントローラー
    static CDirectXDevice * m_DXDevice; // DirectX9デバイス
    static CXAudio2Device * m_XAudio2; // XAudio2Device
};

#endif // !_DeviceManager_H_
