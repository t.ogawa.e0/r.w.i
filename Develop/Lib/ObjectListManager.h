//==========================================================================
// ObjectListManager[ObjectListManager.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _ObjectListManager_H_
#define _ObjectListManager_H_

//==========================================================================
// include
//==========================================================================
#include <list>

//==========================================================================
//
// class  : CObjectListManager
// Content: ObjectListManager
//
//==========================================================================
template<typename _Ty>
class CObjectListManager
{
private:
    // コピー禁止 (C++11)
    CObjectListManager(const CObjectListManager &) = delete;
    CObjectListManager &operator=(const CObjectListManager &) = delete;
public:
    CObjectListManager() {
        this->m_route_list = nullptr;
        this->m_route_size = 0;
    }
    ~CObjectListManager() {
        this->Release();
    }

    // 解放
    void Release(void) {
        for (auto itr = this->m_list.begin(); itr != this->m_list.end(); ++itr) {
            this->Delete((*itr));
        }
        this->m_list.clear();

        for (auto itr = this->m_list_sub.begin(); itr != this->m_list_sub.end(); ++itr) {
            this->Delete((*itr));
        }
        this->m_list_sub.clear();
        this->RouteDelete();
    }

    // インスタンスの生成
    // 戻り値 : インスタンスのアドレス
    _Ty * Create(void) {
        _Ty * Object = nullptr;
        this->m_list.emplace_back(this->New(Object));
        return Object;
    }

    // 生成したデータの読み込み
    void LoadData(void) {
        int countor = 0;
        this->RouteNew((int)this->m_list.size());
        for (auto itr = this->m_list.begin(); itr != this->m_list.end(); ++itr) {
            this->RouteSet((*itr), countor);
        }
    }

    // 指定したアドレスの管理権限をメインメモリから破棄
    // サブメモリに管理権限を移します。
    // 失敗時 nullが返ります
    _Ty * GetDelete(_Ty * Object) {
        if (Object != nullptr) {
            bool hit = false; // 管理範囲外のデータが来た場合スルーするためのフラグ
            // 指定アドレスの管理権限を破棄する
            for (auto itr = this->m_list.begin(); itr != this->m_list.end(); ++itr) {
                if ((*itr) == Object) {
                    hit = true; // 管理範囲内の場合
                    this->m_list.erase(itr);
                    break;
                }
            }
            if (hit)
            {
                // サブメモリに管理権限を持たせる
                this->m_list_sub.emplace_back(Object);
                this->LoadData();
            }
        }
        return Object; // 戻り値として返す
    }

    // 指定した領域の管理権限をメインメモリから破棄
    // サブメモリに管理権限を移します。
    // 失敗時 nullが返ります
    _Ty * GetDelete(int label) {
        return this->GetDelete(this->Get(label));
    }

    // メインメモリに管理権限を戻す
    // GetDelete() で取得したデータを入れてください。
    void PushBack(_Ty * Object) {
        if (Object != nullptr) {
            // サブメモリから管理権限を破棄
            bool hit = false; // 管理範囲外のデータが来た場合スルーするためのフラグ
            for (auto itr = this->m_list_sub.begin(); itr != this->m_list_sub.end(); ++itr) {
                if ((*itr) == Object) {
                    hit = true; // ヒット
                    this->m_list_sub.erase(itr);
                    break;
                }
            }
            if (hit)
            {
                // メインメモリに管理権限を持たせる
                this->m_list.emplace_back(Object);
                this->LoadData();
            }
        }
    }

    // 特定のObjectの破棄
    // 破棄するオブジェクトのアドレスを入れてください
    void PinpointRelease(_Ty * Object) {
        if (Object != nullptr) {
            // メインメモリ
            for (auto itr = this->m_list.begin(); itr != this->m_list.end(); ++itr) {
                if ((*itr) == Object) {
                    this->Delete((*itr));
                    this->m_list.erase(itr);
                    this->LoadData();
                    return;
                }
            }
            // サブメモリ
            for (auto itr = this->m_list_sub.begin(); itr != this->m_list_sub.end(); ++itr) {
                if ((*itr) == Object) {
                    this->Delete((*itr));
                    this->m_list_sub.erase(itr);
                    return;
                }
            }
        }
    }

    // 特定のObjectの破棄
    // 破棄する領域を入れてください
    void PinpointRelease(int label) {
        this->PinpointRelease(this->Get(label));
    }

    // 管理しているObject数
    int Size(void) {
        return (int)this->m_route_size;
    }

    // Objectのゲッター
    // 失敗時 null
    _Ty * Get(int label) {
        // メモリ領域内の時
        if (0 <= label&&label<this->Size()) {
            return this->m_route_list[label];
        }
        return nullptr;
    }
private:
    // インスタンス生成
    // アドレスが返ります
    _Ty * New(_Ty *& Object) {
        Object = new _Ty;
        return Object;
    }

    // インスタンスの破棄
    // nullが返ります
    _Ty * Delete(_Ty *& Object) {
        // 破棄メモリがある時
        if (Object != nullptr) {
            delete Object;
            Object = nullptr;
        }
        return Object;
    }

    // アクセスルート生成
    void RouteNew(int size) {
        if (size != 0) {
            this->RouteDelete();
            this->m_route_size = size;
            this->m_route_list = new _Ty*[size];
            for (int i = 0; i < size; i++) {
                this->m_route_list[i] = nullptr;
            }
        }
    }

    // アクセスルートのセット
    void RouteSet(_Ty * Object, int & countor) {
        this->m_route_list[countor] = Object;
        countor++;
    }

    // アクセスルートの破棄
    void RouteDelete(void) {
        // 破棄メモリがある時
        if (this->m_route_list != nullptr) {
            delete[] this->m_route_list;
            this->m_route_list = nullptr;
            this->m_route_size = 0;
        }
    }
private:
    std::list<_Ty*>m_list; // メインメモリ
    std::list<_Ty*>m_list_sub; // サブメモリ
    _Ty**m_route_list; // アクセスルート
    int m_route_size; // ルートのサイズ
};

#endif // !_ObjectListManager_H_
