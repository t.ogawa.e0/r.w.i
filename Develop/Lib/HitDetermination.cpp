//==========================================================================
// 当たり判定[HitDetermination.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "HitDetermination.h"

//==========================================================================
// 定数定義
////==========================================================================
constexpr float fNull = 0.0f;

//==========================================================================
// 球当たり判定 当たった場合true
bool CHitDetermination::Ball(const C3DObject & TargetA, const C3DObject & TargetB, float Scale)
{
	if (this->Distance(TargetA, TargetB)<Scale) { return true; }

	return false;
}

//==========================================================================
// シンプルな当たり判定 当たった場合true
bool CHitDetermination::Simple(const C3DObject & TargetA, const C3DObject & TargetB, float Scale)
{
	const D3DXVECTOR3 * TargetAPos = TargetA.GetMatInfoPos();
	const D3DXVECTOR3 * TargetBPos = TargetB.GetMatInfoPos();

	if (this->Sinple2(TargetAPos->z, TargetBPos->z, fNull, Scale)) { return true; }
	if (this->Sinple2(TargetAPos->x, TargetBPos->x, fNull, Scale)) { return true; }

	return false;
}

//==========================================================================
// 距離の算出
// C2DObject or C3DObject
float CHitDetermination::Distance(const C3DObject & TargetA, const C3DObject & TargetB)
{
    const D3DXVECTOR3 * TargetAPos = TargetA.GetMatInfoPos();
    const D3DXVECTOR3 * TargetBPos = TargetB.GetMatInfoPos();

	float fDistance = powf
	(
		((TargetBPos->x) - (TargetAPos->x))*
		((TargetBPos->x) - (TargetAPos->x)) +
		((TargetBPos->y) - (TargetAPos->y))*
		((TargetBPos->y) - (TargetAPos->y)) +
		((TargetBPos->z) - (TargetAPos->z))*
		((TargetBPos->z) - (TargetAPos->z)),
		0.5f
	);

	return fDistance;
}

//==========================================================================
// 距離の算出
// C2DObject or C3DObject
float CHitDetermination::Distance(const C2DObject & TargetA, const C2DObject & TargetB)
{
    const CVector4<float> * TargetAPos = TargetA.GetPos();
    const CVector4<float> * TargetBPos = TargetB.GetPos();

    float fDistance = powf
    (
        ((TargetBPos->x) - (TargetAPos->x))*
        ((TargetBPos->x) - (TargetAPos->x)) +
        ((TargetBPos->y) - (TargetAPos->y))*
        ((TargetBPos->y) - (TargetAPos->y)) +
        ((TargetBPos->z) - (TargetAPos->z))*
        ((TargetBPos->z) - (TargetAPos->z)),
        0.5f
    );

    return fDistance;
}

//==========================================================================
// 距離の算出
// CVector4<float>
float CHitDetermination::Distance(const CVector4<float>& TargetA, const CVector4<float>& TargetB)
{
    float fDistance = powf
    (
        ((TargetB->x) - (TargetA->x))*
        ((TargetB->x) - (TargetA->x)) +
        ((TargetB->y) - (TargetA->y))*
        ((TargetB->y) - (TargetA->y)) +
        ((TargetB->z) - (TargetA->z))*
        ((TargetB->z) - (TargetA->z)),
        0.5f
    );

    return fDistance;
}

//==========================================================================
// シンプルな当たり判定の内部
bool CHitDetermination::Sinple2(const float & TargetA, const float & TargetB, const float & TargetC, float Scale)
{
	if (TargetC < TargetA)
	{
		if (TargetC < TargetB)
		{
			if (TargetB < TargetA*Scale)
			{
				return true;
			}
		}
	}
	else if (TargetA < TargetC)
	{
		if (TargetB < TargetC)
		{
			if (TargetA*Scale < TargetB)
			{
				return true;
			}
		}
	}

	return false;
}
