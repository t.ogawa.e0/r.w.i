//==========================================================================
// デバイスマネージャー[DeviceManager.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "DeviceManager.h"
#include "Template.h"

//==========================================================================
// 実体
//==========================================================================
CObjectVectorManager<CDeviceManager::DeviceInitList> CDeviceManager::m_InitList; // 初期化終了情報の格納
CMouse * CDeviceManager::m_Mouse = nullptr; // DirectInputマウス
CKeyboard * CDeviceManager::m_Keyboard = nullptr; // DirectInputキーボード
CController * CDeviceManager::m_Controller = nullptr; // DirectInputコントローラー
CDirectXDevice * CDeviceManager::m_DXDevice = nullptr; // DirectX9デバイス
CXAudio2Device * CDeviceManager::m_XAudio2 = nullptr; // XAudio2Device

//==========================================================================
// 初期化
bool CDeviceManager::Init(DeviceInitList Serect, HINSTANCE hInstance, HWND hWnd)
{
    CTemplates temp; // テンプレート

    // 重複生成回避用
    for (int i = 0; i < m_InitList.Size(); i++)
    {
        if (*m_InitList.Get(i) == Serect)
        {
            return false;
        }
    }

    // 未初期化の場合初期化作業を行う
    switch (Serect)
    {
    case CDeviceManager::DeviceInitList::Mouse:
        *m_InitList.Create() = Serect;
        CDirectInputManager::NewDirectInput(m_Mouse);
        m_Mouse->Init(hInstance, hWnd);
        break;
    case CDeviceManager::DeviceInitList::keyboard:
        *m_InitList.Create() = Serect;
        CDirectInputManager::NewDirectInput(m_Keyboard);
        m_Keyboard->Init(hInstance, hWnd);
        break;
    case CDeviceManager::DeviceInitList::Controller:
        *m_InitList.Create() = Serect;
        CDirectInputManager::NewDirectInput(m_Controller);
        m_Controller->Init(hInstance, hWnd);
        break;
    case CDeviceManager::DeviceInitList::DXDevice:
        *m_InitList.Create() = Serect;
        temp.New_(m_DXDevice);
        return m_DXDevice->Init();
        break;
    case CDeviceManager::DeviceInitList::XAudio2:
        *m_InitList.Create() = Serect;
        temp.New_(m_XAudio2);
        m_XAudio2->Init(hWnd);
        break;
    default:
        break;
    }
    return false;
}

//==========================================================================
// 解放
void CDeviceManager::Release(void)
{
    CTemplates temp; // テンプレート

    CDirectInputManager::ReleaseALL();
    temp.Delete_(m_DXDevice);
    temp.Delete_(m_XAudio2);
    m_InitList.Release();
}

//==========================================================================
// 更新
void CDeviceManager::Update(void)
{
    CDirectInputManager::UpdateAll();
}
