//==========================================================================
// char convert
// author: tatsuya ogawa
//==========================================================================
#include "CharConvert.h"

//==========================================================================
// C
// string -> wstring
std::wstring CCharConvert::multi_to_wide_capi(std::string const& src)
{
    std::size_t converted{};
    std::vector<wchar_t> dest(src.size(), L'\0');
    if (::_mbstowcs_s_l(&converted, dest.data(), dest.size(), src.data(), _TRUNCATE, ::_create_locale(LC_ALL, "jpn")) != 0) {
        throw std::system_error{ errno, std::system_category() };
    }
    return std::wstring(dest.begin(), dest.end());
}

//==========================================================================
// Windows API
// string -> wstring
std::wstring CCharConvert::multi_to_wide_winapi(std::string const& src)
{
    auto const dest_size = ::MultiByteToWideChar(CP_ACP, 0U, src.data(), -1, nullptr, 0U);
    std::vector<wchar_t> dest(dest_size, L'\0');
    if (::MultiByteToWideChar(CP_ACP, 0U, src.data(), -1, dest.data(), dest.size()) == 0) {
        throw std::system_error{ static_cast<int>(::GetLastError()), std::system_category() };
    }
    return std::wstring(dest.begin(), dest.end());
}

//==========================================================================
// C/C++
// UTF-8 -> Shift_JIS
std::string CCharConvert::wide_to_multi_capi(std::wstring const& src)
{
    std::size_t converted{};
    std::vector<char> dest(src.size() * sizeof(wchar_t) + 1, '\0');
    if (::_wcstombs_s_l(&converted, dest.data(), dest.size(), src.data(), _TRUNCATE, ::_create_locale(LC_ALL, "jpn")) != 0) {
        throw std::system_error{ errno, std::system_category() };
    }
    return std::string(dest.begin(), dest.end());
}

//==========================================================================
// Windows API
// UTF-8 -> Shift_JIS
std::string CCharConvert::wide_to_multi_winapi(std::wstring const& src)
{
    auto const dest_size = ::WideCharToMultiByte(CP_ACP, 0U, src.data(), -1, nullptr, 0, nullptr, nullptr);
    std::vector<char> dest(dest_size, '\0');
    if (::WideCharToMultiByte(CP_ACP, 0U, src.data(), -1, dest.data(), dest.size(), nullptr, nullptr) == 0) {
        throw std::system_error{ static_cast<int>(::GetLastError()), std::system_category() };
    }
    return std::string(dest.begin(), dest.end());
}

//==========================================================================
// C++
// wstring -> string
std::string CCharConvert::wide_to_utf8_cppapi(std::wstring const& src)
{
    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
    return converter.to_bytes(src);
}

//==========================================================================
// Windows API
// wstring -> string
std::string CCharConvert::wide_to_utf8_winapi(std::wstring const& src)
{
    auto const dest_size = ::WideCharToMultiByte(CP_UTF8, 0U, src.data(), -1, nullptr, 0, nullptr, nullptr);
    std::vector<char> dest(dest_size, '\0');
    if (::WideCharToMultiByte(CP_UTF8, 0U, src.data(), -1, dest.data(), dest.size(), nullptr, nullptr) == 0) {
        throw std::system_error{ static_cast<int>(::GetLastError()), std::system_category() };
    }
    return std::string(dest.begin(), dest.end());
}

//==========================================================================
// C++
// string -> wstring
std::wstring CCharConvert::utf8_to_wide_cppapi(std::string const& src)
{
    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
    return converter.from_bytes(src);
}

//==========================================================================
// Windows API
// string -> wstring
std::wstring CCharConvert::utf8_to_wide_winapi(std::string const& src)
{
    auto const dest_size = ::MultiByteToWideChar(CP_UTF8, 0U, src.data(), -1, nullptr, 0U);
    std::vector<wchar_t> dest(dest_size, L'\0');
    if (::MultiByteToWideChar(CP_UTF8, 0U, src.data(), -1, dest.data(), dest.size()) == 0) {
        throw std::system_error{ static_cast<int>(::GetLastError()), std::system_category() };
    }
    return std::wstring(dest.begin(), dest.end());
}

//==========================================================================
// C/C++
// Shift_JIS -> UTF-8
std::string CCharConvert::multi_to_utf8_cppapi(std::string const& src)
{
    auto const wide = this->multi_to_wide_capi(src);
    return this->wide_to_utf8_cppapi(wide);
}

//==========================================================================
// Windows API
// Shift_JIS -> UTF-8
std::string CCharConvert::multi_to_utf8_winapi(std::string const& src)
{
    auto const wide = this->multi_to_wide_winapi(src);
    return this->wide_to_utf8_winapi(wide);
}

//==========================================================================
// C/C++
// UTF-8 -> Shift_JIS
std::string CCharConvert::utf8_to_multi_cppapi(std::string const& src)
{
    auto const wide = this->utf8_to_wide_cppapi(src);
    return this->wide_to_multi_capi(wide);
}

//==========================================================================
// Windows API
// UTF-8 -> Shift_JIS
std::string CCharConvert::utf8_to_multi_winapi(std::string const& src)
{
    auto const wide = this->utf8_to_wide_winapi(src);
    return this->wide_to_multi_winapi(wide);
}
