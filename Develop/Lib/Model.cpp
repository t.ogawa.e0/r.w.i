//==========================================================================
// モデルデータ[Model.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Model.h"

//==========================================================================
// 初期化
// Input = 使用するテクスチャのパス
// mode = マテリアルのライティングの設定
bool CXmodel::Init(const char * Input, LightMoad mode)
{
	LPDIRECT3DDEVICE9 pDevice = CDeviceManager::GetDXDevice()->GetD3DDevice();
	CXdata * pxm = nullptr;
	bool bkey = false;

	// データの重複判定
    for (int i = 0; i < this->m_data.Size(); i++)
    {
        pxm = this->m_data.Get(i);

        // 重複しているとき
        if (!strcmp(pxm->gettag(), Input))
        {
            bkey = true;
            break;
        }
    }

	// 重複していないとき
	if (bkey == false)
	{
        pxm = this->m_data.Create();
        pxm->settag(Input);

		// modelの読み込み
		if (FAILED(pxm->load(mode, pDevice)))
		{
			return true;
		}
	}

    CXdata * pis = this->m_data_path.Create();
    pis->path(pxm);

	return false;
}

//==========================================================================
// 初期化
// Input = 使用するテクスチャのパス ダブルポインタに対応
// num = 要素数
// mode = マテリアルのライティングの設定
bool CXmodel::Init(const char ** Input, int num, LightMoad mode)
{
	for (int i = 0; i < num; i++)
	{
		if (this->Init(Input[i], mode))
		{
			return true;
		}
	}
	return false;
}

//==========================================================================
// 更新
void CXmodel::Update(void)
{
    // 登録済みオブジェクトのデータの更新
    for (auto itr = this->m_ObjectData.begin(); itr != this->m_ObjectData.end(); ++itr)
    {
        // 行列がデフォルトの指定の時
        if (itr->m_3dobj->GetMatrixType() == C3DObject::EMatrixType::Default)
        {
            itr->m_3dobj->SetMatrixType(C3DObject::EMatrixType::Vector1);
        }

        // 行列の生成
        itr->m_3dobj->CreateMtxWorld(itr->MtxWorld);
    }
}

//==========================================================================
// 解放
void CXmodel::Release(void)
{
    this->m_data.Release();
    this->m_data_path.Release();
    this->ObjectRelease();
}

//==========================================================================
// 描画
void CXmodel::Draw(void)
{
    if (this->m_data_path.Size() != 0 && this->m_ObjectData.size() != 0)
    {
        LPDIRECT3DDEVICE9 pDevice = CDeviceManager::GetDXDevice()->GetD3DDevice();

        // 登録済みオブジェクトのデータを元に描画
        for (auto itr = this->m_ObjectData.begin(); itr != this->m_ObjectData.end(); ++itr)
        {
            CXdata * pXdata = this->m_data_path.Get(itr->m_3dobj->getindex());

            // 行列のセット
            pDevice->SetTransform(D3DTS_WORLD, &itr->MtxWorld);

            // 描画
            pXdata->draw(pDevice);
        }
    }
}

//==========================================================================
// tagのセット
void CXmodel::CXdata::settag(const char * ptag)
{
	if (ptag != nullptr)
	{
		this->m_strName = ptag;
	}
	else
	{
		this->m_strName = "";
	}
}

//==========================================================================
// コピー
void CXmodel::CXdata::path(CXdata * pinp)
{
	this->m_MaterialMood = pinp->m_MaterialMood;
	this->m_Mesh = pinp->m_Mesh;
	this->m_NumMaterial = pinp->m_NumMaterial;
	this->m_MaterialMesh = pinp->m_MaterialMesh;
    this->m_texture = pinp->m_texture;
    this->m_strName = pinp->m_strName;
	this->m_origin = false;
}

//==========================================================================
// 読み込み
HRESULT CXmodel::CXdata::load(LightMoad mode, LPDIRECT3DDEVICE9 pDevice)
{
	LPD3DXBUFFER pAdjacency = nullptr; // 隣接情報
	LPD3DXMESH pTempMesh = nullptr; // テンプレートメッシュ
	LPD3DXBUFFER pMaterialBuffer = nullptr; // マテリアルバッファ
	D3DVERTEXELEMENT9 pElements[MAXD3DDECLLENGTH + 1];
	HRESULT hr = (HRESULT)0;
	LPSTR lhr = nullptr;

	// マテリアルのモードを記録
	this->m_MaterialMood = mode;

	// モデルの読み込み
	hr = this->read(&pAdjacency, &pMaterialBuffer, pDevice);
	if (FAILED(hr))
	{
        CDeviceManager::GetDXDevice()->ErrorMessage("Xデータが読み込めませんでした。\n %s", this->m_strName.c_str());
		return hr;
	}
	
	// 最適化
	hr = this->optimisation(pAdjacency);
	if (FAILED(hr))
	{
        CDeviceManager::GetDXDevice()->ErrorMessage("Xデータの最適化に失敗しました。\n %s", this->m_strName.c_str());
		return hr;
	}

	// 頂点の宣言
	hr = this->declaration(pElements);
	if (FAILED(hr))
	{
        CDeviceManager::GetDXDevice()->ErrorMessage("頂点の宣言に失敗しました。\n %s", this->m_strName.c_str());
		return hr;
	}

	// 複製
	hr = this->replication(pElements, &pTempMesh, pDevice);
	if (FAILED(hr))
	{
        CDeviceManager::GetDXDevice()->ErrorMessage("複製に失敗しました。\n %s", this->m_strName.c_str());
		return hr;
	}

	// テクスチャの読み込み
	lhr = this->loadtexture((LPD3DXMATERIAL)pMaterialBuffer->GetBufferPointer());
	if (lhr != nullptr)
	{
        CDeviceManager::GetDXDevice()->ErrorMessage("読み込みに失敗しました\n model : %s \n texture : %s ", this->m_strName.c_str(), lhr);
		return (HRESULT)-1;
	}

	// マテリアルの設定
	this->materialsetting((LPD3DXMATERIAL)pMaterialBuffer->GetBufferPointer());

	this->m_temp.Release_(pMaterialBuffer);
	this->m_temp.Release_(this->m_Mesh);
	this->m_temp.Release_(pAdjacency);
	this->m_Mesh = pTempMesh;
	this->m_origin = true;

	return hr;
}

//==========================================================================
// 解放
void CXmodel::CXdata::Release(void)
{
	if (this->m_origin)
	{
        this->m_temp.Delete_(this->m_MaterialMesh);
        this->m_temp.Delete_(this->m_texture);
		this->m_temp.Release_(this->m_Mesh);
	}
	this->m_strName.clear();
}

//==========================================================================
// 描画
void CXmodel::CXdata::draw(LPDIRECT3DDEVICE9 pDevice)
{
	pDevice->SetFVF(this->m_Mesh->GetFVF());
	for (int i = 0; i < (int)this->m_NumMaterial; i++)
	{
		// マテリアル情報をセット
		pDevice->SetMaterial(&this->m_MaterialMesh[i]);
		// テクスチャが存在するときにセット
		pDevice->SetTexture(0, nullptr);
		// テクスチャ情報をセット
		pDevice->SetTexture(0, this->m_texture->get(i)->gettex());
		// メッシュを描画
		this->m_Mesh->DrawSubset(i);
	}
}

//==========================================================================
// 読み込み
HRESULT CXmodel::CXdata::read(LPD3DXBUFFER * pAdjacency, LPD3DXBUFFER * pMaterialBuffer, LPDIRECT3DDEVICE9 pDevice)
{
	return D3DXLoadMeshFromX(this->m_strName.c_str(), D3DXMESH_SYSTEMMEM, pDevice, pAdjacency, pMaterialBuffer, nullptr, &this->m_NumMaterial, &this->m_Mesh);
}

//==========================================================================
// 最適化
HRESULT CXmodel::CXdata::optimisation(LPD3DXBUFFER pAdjacency)
{
	return this->m_Mesh->OptimizeInplace(D3DXMESHOPT_COMPACT | D3DXMESHOPT_ATTRSORT | D3DXMESHOPT_VERTEXCACHE, (DWORD*)pAdjacency->GetBufferPointer(), nullptr, nullptr, nullptr);
}

//==========================================================================
// 頂点の宣言
HRESULT CXmodel::CXdata::declaration(D3DVERTEXELEMENT9 *pElements)
{
	return this->m_Mesh->GetDeclaration(pElements);
}

//==========================================================================
// 複製
HRESULT CXmodel::CXdata::replication(D3DVERTEXELEMENT9 *pElements, LPD3DXMESH * pTempMesh, LPDIRECT3DDEVICE9 pDevice)
{
	return this->m_Mesh->CloneMesh(D3DXMESH_MANAGED | D3DXMESH_WRITEONLY, pElements, pDevice, pTempMesh);
}

//==========================================================================
// テクスチャの読み込み
const LPSTR CXmodel::CXdata::loadtexture(const LPD3DXMATERIAL Input)
{
    this->m_temp.New_(this->m_texture);

	// マテリアルの数だけ
	for (int i = 0; i < (int)this->m_NumMaterial; i++)
	{
		D3DXMATERIAL * pmat = &Input[i];

		// 使用しているテクスチャがあれば読み込む
		if (pmat->pTextureFilename != nullptr &&lstrlen(pmat->pTextureFilename) > 0)
		{
			if (this->m_texture->init(pmat->pTextureFilename)) { return pmat->pTextureFilename; }
		}
		else
		{
			this->m_texture->init(nullptr);
		}
	}

	// material数が0のとき
	if ((int)this->m_NumMaterial == 0)
	{
		this->m_texture->init(nullptr);
	}

	return nullptr;
}

//==========================================================================
// マテリアルの設定
void CXmodel::CXdata::materialsetting(const LPD3DXMATERIAL Input)
{
	// メッシュの数だけ確保
	this->m_temp.New_(this->m_MaterialMesh, (int)this->m_NumMaterial);

	for (int i = 0; i < (int)this->m_NumMaterial; i++)
	{
		switch (this->m_MaterialMood)
		{
		case CXmodel::LightMoad::System:
			this->materialsetting_system(&this->m_MaterialMesh[i], &Input[i].MatD3D);
			break;
		case CXmodel::LightMoad::Material:
			this->materialsetting_material(&this->m_MaterialMesh[i], &Input[i].MatD3D);
			break;
		default:
			break;
		}
	}
}

//==========================================================================
// マテリアルの設定 システム設定
void CXmodel::CXdata::materialsetting_system(D3DMATERIAL9 * pMatMesh, const D3DMATERIAL9 * Input)
{
    this->materialsetting_material(pMatMesh, Input);

    pMatMesh->Specular.r = 1.0f; // スペキュラー
    pMatMesh->Specular.g = 1.0f; // スペキュラー
    pMatMesh->Specular.b = 1.0f; // スペキュラー
    pMatMesh->Specular.a = 1.0f; // スペキュラー

    pMatMesh->Emissive.r = 1.0f; // 放射性
    pMatMesh->Emissive.g = 1.0f; // 放射性
    pMatMesh->Emissive.b = 1.0f; // 放射性
    pMatMesh->Emissive.a = 1.0f; // 放射性

    pMatMesh->Diffuse.r = 1.0f; // 光源
    pMatMesh->Diffuse.g = 1.0f; // 光源
    pMatMesh->Diffuse.b = 1.0f; // 光源
    pMatMesh->Diffuse.a = 1.0f; // 光源

    pMatMesh->Ambient.r = 0.7f; // アンビエントライト
    pMatMesh->Ambient.g = 0.7f; // アンビエントライト
    pMatMesh->Ambient.b = 0.7f; // アンビエントライト
    pMatMesh->Ambient.a = 1.0f; // アンビエントライト
}

//==========================================================================
// マテリアルの設定 マテリアル素材設定
void CXmodel::CXdata::materialsetting_material(D3DMATERIAL9 * pMatMesh, const D3DMATERIAL9 * Input)
{
	ZeroMemory(pMatMesh, sizeof(D3DMATERIAL9));
	*pMatMesh = *Input;
}
