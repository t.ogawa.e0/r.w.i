//==========================================================================
// アスペクト比[AspectRatio.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _AspectRatio_H_
#define _AspectRatio_H_

//==========================================================================
// include
//==========================================================================
#include <vector>
#include "DataType.h"

//==========================================================================
//
// class  : CAspectRatio
// Content: アスペクト比
//
//==========================================================================
class CAspectRatio
{
private:
    // コピー禁止 (C++11)
    CAspectRatio(const CAspectRatio &) = delete;
    CAspectRatio &operator=(const CAspectRatio &) = delete;
public:
    typedef CVector2<int> data_t;
public:
    struct List {
        data_t size; // ウィンドウサイズ
        data_t asp; // アスペクト比
    };
public:
    CAspectRatio() {}
    ~CAspectRatio() {
        this->m_AspectRatio.clear();
        this->m_AspectRatioALL.clear();
    }

    // アスペクト比チェック
    // Window_W = ウィンドウ幅
    // Window_H = ウィンドウ高さ
    // アスペクト比指定 aspX : aspY (16:9)
    // 戻り値 指定のアスペクト比ならtrueが返ります
    bool Search(const data_t & win, const data_t & asps) {
        int n = 0;
        data_t size(win);
        data_t asp;
        List list;

        if ((n = this->GCD(size)) > 0) {
            asp.x = size.x / n;
            asp.y = size.y / n;
        }

        // データの登録
        list.size = data_t(win);
        list.asp = asp;
        this->m_AspectRatioALL.emplace_back(list);

        // 指定するアスペクト比ならそのウィンドウサイズの登録
        if (asps.x == asp.x&&asps.y == asp.y) {
            this->m_AspectRatio.emplace_back(list);
            return true;
        }
        return false;
    }

    // 指定したアスペクト比と一致したデータ数
    int Size(void) {
        return this->m_AspectRatio.size();
    }

    // 処理したデータ数
    int AllSize(void) {
        return this->m_AspectRatioALL.size();
    }

    // 指定したアスペクト比のデータのみ取得
    List & Get(int label) {
        return this->m_AspectRatio[label];
    }

    // 処理したデータの結果を取得
    List & AllDataGet(int label) {
        return this->m_AspectRatioALL[label];
    }
private:
    // 最大公約数を求める
    int GCD(data_t size) {
        int tmp = 0;

        // xよりもyの値が大きければ，tmp変数を使って入れ替える
        if (size.x < size.y) {
            tmp = size.x;
            size.x = size.y;
            size.y = tmp;
        }

        // yが自然数でなければ-1を返す
        if (size.y <= 0) {
            return -1;
        }

        // xをyで割った余りが0であれば，それが最大公約数
        if (size.x % size.y == 0) {
            return size.y;
        }

        // 再帰関数（上のif文に引っかかるまでGCDを繰り返す）
        return GCD(data_t(size.y, size.x % size.y));
    }
private:
    std::vector<List> m_AspectRatio; // ヒットしたデータの格納
    std::vector<List> m_AspectRatioALL; // データの格納
};

#endif // !_AspectRatio_H_
