//==========================================================================
// モデルデータ[Model.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _Model_H_
#define _Model_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "3DObject.h"
#include "Template.h"
#include "DeviceManager.h"
#include "TextureLoader.h"
#include "ObjectInput.h"
#include "ObjectVectorManager.h"

//==========================================================================
//
// class  : CXmodel
// Content: Xモデル 
//
//==========================================================================
class CXmodel : public CObjectInput
{
private:
    // コピー禁止 (C++11)
    CXmodel(const CXmodel &) = delete;
    CXmodel &operator=(const CXmodel &) = delete;
public:
	// マテリアルライトの設定
	enum class LightMoad
	{
		System, // システム設定
		Material // マテリアル設定
	};
private:
	//==========================================================================
	//
	// class  : CXdata
	// Content: Xデータ管理 
	//
	//==========================================================================
	class CXdata
	{
	public:
        CXdata() {
            this->m_MaterialMood = LightMoad::System;
            this->m_Mesh = nullptr;
            this->m_NumMaterial = (DWORD)0;
            this->m_MaterialMesh = nullptr;
            this->m_texture = nullptr;
            this->m_strName = "";
            this->m_origin = false;
        }
		~CXdata() {
            this->Release();
        }

		// tagのセット
		void settag(const char * ptag);
		// tagの取得
		const char * gettag(void) { return this->m_strName.c_str(); }

        // 複製データ
        void path(CXdata * pinp);

		// 読み込み
		HRESULT load(LightMoad mode, LPDIRECT3DDEVICE9 pDevice);
		// 解放
		void Release(void);
		// 描画
		void draw(LPDIRECT3DDEVICE9 pDevice);
	private:
		// 読み込み
		HRESULT read(LPD3DXBUFFER * pAdjacency, LPD3DXBUFFER * pMaterialBuffer, LPDIRECT3DDEVICE9 pDevice);
		// 最適化
		HRESULT optimisation(LPD3DXBUFFER pAdjacency);
		// 頂点の宣言
		HRESULT declaration(D3DVERTEXELEMENT9 *pElements);
		// 複製
		HRESULT replication(D3DVERTEXELEMENT9 *pElements, LPD3DXMESH * pTempMesh, LPDIRECT3DDEVICE9 pDevice);
		// テクスチャの読み込み
		const LPSTR loadtexture(const LPD3DXMATERIAL Input);
		// マテリアルの設定
		void materialsetting(const LPD3DXMATERIAL Input);
		// マテリアルの設定 システム設定
		void materialsetting_system(D3DMATERIAL9 *pMatMesh, const D3DMATERIAL9 * Input);
		// マテリアルの設定 マテリアル素材設定
		void materialsetting_material(D3DMATERIAL9 *pMatMesh, const D3DMATERIAL9 *Input);
	private:
		LightMoad m_MaterialMood; // マテリアルのモード
		LPD3DXMESH m_Mesh; // メッシュデータ
		DWORD m_NumMaterial; // マテリアル数
		D3DMATERIAL9 *m_MaterialMesh; // マテリアル情報
        CTextureLoader *m_texture; // テクスチャの格納
		std::string m_strName; // タグ
		bool m_origin; // オリジナルデータ判定
		CTemplates m_temp;
	};
public:
	CXmodel()
	{
		this->m_Device = nullptr;
	}
	~CXmodel() {
        this->Release();
    }
	// 初期化
	// Input = 使用するテクスチャのパス
	// mode = マテリアルのライティングの設定
	bool Init(const char * Input, LightMoad mode = LightMoad::System);

	// 初期化
	// Input = 使用するテクスチャのパス ダブルポインタに対応
	// num = 要素数
	// mode = マテリアルのライティングの設定
	bool Init(const char ** Input, int num, LightMoad mode = LightMoad::System);

    // 更新
    void Update(void);

	// 解放
	void Release(void);

    // 描画
    void Draw(void);

	// モデルデータのパラメーターゲッター
	// Input = (C3DObject) 
	CXdata* GetMdelDataParam(C3DObject & Input) { return this->m_data_path.Get(Input.getindex()); }

	// デバイスのセット
	void SetDevice(const LPDIRECT3DDEVICE9 Input) { this->m_Device = Input; }
private:
    CObjectVectorManager<CXdata> m_data; // モデルデータ管理
    CObjectVectorManager<CXdata> m_data_path; // モデルデータ管理
	LPDIRECT3DDEVICE9 m_Device; // デバイス
};

#endif // !_Model_H_
