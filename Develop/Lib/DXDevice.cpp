//==========================================================================
// デバイス[DXDevice.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "DXDevice.h"

//==========================================================================
// 初期化 失敗時true
bool CDirectXDevice::Init(void)
{
    //Direct3Dオブジェクトの作成
    this->m_pD3D = Direct3DCreate9(D3D_SDK_VERSION);
    if (this->m_pD3D == nullptr)
    {
        return true;
    }
    return false;
}


//==========================================================================
// ウィンドウモードの生成
bool CDirectXDevice::CreateWindowMode(const CAspectRatio::data_t & data, bool Mode)
{
    int nCount = 0;
    CWindowsSize<float> w1percent = CWindowsSize<float>(0.0f, 0.0f);
    CWindowsSize<float> wErrorpercent = CWindowsSize<float>(0.0f, 0.0f);
    D3DDISPLAYMODE d3dspMode;
    int num = 0;

    ZeroMemory(&this->m_d3dpp, sizeof(this->m_d3dpp));

    // 現在のディスプレイモードを取得
    if (FAILED(this->m_pD3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &this->m_d3dpm)))
    {
        return true;
    }

    // 何種類ディスプレイモードあるかを調べる
    num = this->m_pD3D->GetAdapterModeCount(D3DADAPTER_DEFAULT, this->m_d3dpm.Format);

    // ディスプレイモードを調べる
    for (int i = 0; i < num; i++)
    {
        if (FAILED(this->m_pD3D->EnumAdapterModes(D3DADAPTER_DEFAULT, this->m_d3dpm.Format, i, &d3dspMode)))
        {
            this->ErrorMessage("ディスプレイモードの検索に失敗しました");
            return true;
        }

        // ディスプレイモードを記憶
        this->m_d3dspMode.emplace_back(d3dspMode);
    }

    // 最高のバッファを登録
    this->m_d3dpm = this->m_d3dspMode[this->m_d3dspMode.size() - 1];

    if (Mode == false)
    {
        this->m_d3dpm.Width = data.x;
        this->m_d3dpm.Height = data.y;
        this->m_d3dpp.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
        this->m_d3dpp.Windowed = true; // ウィンドウモード
    }
    else if (Mode == true)
    {
        //フルスクリーンのとき、リフレッシュレートを変えられる
        this->m_d3dpp.FullScreen_RefreshRateInHz = this->m_d3dpm.RefreshRate;
        this->m_d3dpp.Windowed = false; // ウィンドウモード
    }

    // 推奨ウィンドウサイズの1%のウィンドウサイズを算出
    w1percent.m_Width = (float)((float)1920 / 100.0f);
    w1percent.m_Height = (float)((float)1080 / 100.0f);
    nCount = 0;

    // 動作環境の最大解像度になるまで繰り返す
    for (;;)
    {
        wErrorpercent.m_Width += w1percent.m_Width;
        wErrorpercent.m_Height += w1percent.m_Height;
        nCount++;
        if (this->m_d3dpm.Width <= wErrorpercent.m_Width)
        {
            break;
        }
        if (this->m_d3dpm.Height <= wErrorpercent.m_Height)
        {
            break;
        }
    }

    // 補正値算出
    this->m_screenbuffscale = nCount *0.01f;

    //デバイスのプレゼンテーションパラメータ(デバイスの設定値)
    this->m_d3dpp.BackBufferWidth = this->m_d3dpm.Width;		//バックバッファの幅
    this->m_d3dpp.BackBufferHeight = this->m_d3dpm.Height;	//バックバッファの高さ
    this->m_d3dpp.BackBufferFormat = this->m_d3dpm.Format;    //バックバッファフォーマット
    this->m_d3dpp.BackBufferCount = 1;					//バッファの数
    this->m_d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;		//垂直？
    this->m_d3dpp.EnableAutoDepthStencil = TRUE;			//3Dの描画に必要
    this->m_d3dpp.AutoDepthStencilFormat = D3DFMT_D16;	// 16Bit Zバッファ作成
    this->m_d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_DEFAULT;

    // ウィンドウサイズ確定
    this->m_WindowsSize.m_Width = this->m_d3dpm.Width;
    this->m_WindowsSize.m_Height = this->m_d3dpm.Height;

    return false;
}

//==========================================================================
// デバイスの生成
bool CDirectXDevice::CreateDevice(void)
{
    //デバイスオブジェクトを生成
    //[デバイス作成制御]<描画>と<頂点処理>
    if (FAILED(this->m_pD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, this->m_hwnd, D3DCREATE_HARDWARE_VERTEXPROCESSING, &this->m_d3dpp, &this->m_pD3DDevice)))
    {
        this->ErrorMessage("デバイスの生成に失敗しました。");
        return true;
    }

    // ビューポートの設定
    D3DVIEWPORT9 vp;
    vp.X = 0;
    vp.Y = 0;
    vp.Width = this->m_d3dpp.BackBufferWidth;
    vp.Height = this->m_d3dpp.BackBufferHeight;
    vp.MinZ = 0.0f;
    vp.MaxZ = 1.0f;

    if (FAILED(this->m_pD3DDevice->SetViewport(&vp)))
    {
        this->ErrorMessage("ビューポートの設定に失敗しました。");
        return true;
    }

    // レンダーステートの設定(α値の設定)
    this->m_pD3DDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);				// αブレンドを許可
    this->m_pD3DDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);		// αソースカラーの設定
    this->m_pD3DDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);	// α

                                                                                // テクスチャステージの設定
    this->m_pD3DDevice->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
    this->m_pD3DDevice->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE);
    this->m_pD3DDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);

    // サンプラーステートの設定(UV値を変えると増えるようになる)
    // WRAP...		反復する
    // CLAMP...　	引き伸ばす
    // MIRROR...　	鏡状態
    this->m_pD3DDevice->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
    this->m_pD3DDevice->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);
    this->m_pD3DDevice->SetSamplerState(0, D3DSAMP_ADDRESSW, D3DTADDRESS_WRAP);

    // フィルタリング
    this->m_pD3DDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR); // 小さくなった時に白枠
    this->m_pD3DDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR); // 常に白枠
    this->m_pD3DDevice->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR); // 元のサイズより小さい時綺麗にする

    return false;
}

//==========================================================================
// 解放
void CDirectXDevice::Release(void)
{
	//デバイスの開放
	if (this->m_pD3DDevice != nullptr)
	{
        this->m_pD3DDevice->Release();
        this->m_pD3DDevice = nullptr;
	}

	//Direct3Dオブジェクトの開放
	if (this->m_pD3D != nullptr)
	{
        this->m_pD3D->Release();
        this->m_pD3D = nullptr;
	}

    this->m_d3dspMode.clear();
}

//==========================================================================
// 描画開始
bool CDirectXDevice::DrawBegin(void)
{
    this->m_pD3DDevice->Clear(0, nullptr, (D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER), D3DCOLOR_RGBA(0, 0, 0, 255), 1.0f, 0);

    //Direct3Dによる描画の開始
    if (SUCCEEDED(this->m_pD3DDevice->BeginScene()))
    {
        return true;
    }
    return false;
}

//==========================================================================
// 描画終了
HRESULT CDirectXDevice::DrawEnd(const RECT* SourceRect, const RECT* DestRect, HWND hDestWindowOverride, const RGNDATA* DirtyRegion)
{
    //Direct3Dによる描画の終了
    this->m_pD3DDevice->EndScene();
    return this->m_pD3DDevice->Present(SourceRect, DestRect, hDestWindowOverride, DirtyRegion);
}