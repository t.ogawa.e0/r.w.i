//==========================================================================
// テクスチャローダー[TextureLoader.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _TextureLoader_H_
#define _TextureLoader_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <string>
#include "DataType.h"
#include "DeviceManager.h"
#include "ObjectVectorManager.h"

//==========================================================================
//
// class  : CTextureLoader
// Content: 2Dテクスチャローダー
//
//==========================================================================
class CTextureLoader
{
private:
    // コピー禁止 (C++11)
    CTextureLoader(const CTextureLoader &) = delete;
    CTextureLoader &operator=(const CTextureLoader &) = delete;
public:
	//==========================================================================
	//
	// class  : CTexture
	// Content: テクスチャの管理
	//
	//==========================================================================
	class CTexture
	{
	public:
		CTexture()
		{
			this->m_texture = nullptr;
			this->m_texparam = 0;
			this->m_mastersize = 0;
            this->m_original = false;
		}
		~CTexture() {
            this->Release();
        }

		// サイズのリセット
		void resetsize(void);
		// サイズのセット
		void setsize(CTexvec<int> Input);
		// サイズの取得
		CTexvec<int> *getsize(void) { return &this->m_texparam; }
		// 読み込み
		HRESULT load(void);
		// 解放
		void Release(void);
		// tagのセット
		void settag(const char * ptag);
		// tagの取得
		const char * gettag(void) { return this->m_strName.c_str(); }
		// 同じテクスチャのパスが入ったらtrueが返る
		bool check(const char * Input);
		// リソースの取得
		const LPDIRECT3DTEXTURE9 gettex(void) { return this->m_texture; }
		// 複製データ
		void path(CTexture * pinp);
	private:
		LPDIRECT3DTEXTURE9 m_texture; // テクスチャポインタ
		CTexvec<int> m_texparam; // テクスチャのサイズ
		CTexvec<int> m_mastersize; // マスターサイズ
		std::string m_strName; // タグ
        bool m_original; // オリジナル
	};
public:
	CTextureLoader() {}
	~CTextureLoader() { this->Release(); }

	// 読み込み
	bool init(const char ** Input, int Num);
	// 読み込み
	bool init(const char * Input);
	// 解放
	void Release(void);
	// データのゲッター
	CTexture * get(int nID);
	// データ数
	int size(void) { return this->m_path.Size(); }
private:
	// 生成
	CTexture * create(CTexture *& pinp, const char * Input);
private:
    CObjectVectorManager<CTexture> m_texture; // 生データの管理
    CObjectVectorManager<CTexture> m_path; // 複製データの管理
};

#endif // !_TextureLoader_H_
