//==========================================================================
// セットレンダー[SetRender.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _SetRender_H_
#define _SetRender_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>

//==========================================================================
//
// class  : CSetRender
// Content: セットレンダーリスト
//
//==========================================================================
class CSetRender
{
protected:
	CSetRender() {}
	~CSetRender() {}

	// 減算処理
    static void SetRenderREVSUBTRACT(LPDIRECT3DDEVICE9 pDevice);

	// 半透明処理
    static void SetRenderADD(LPDIRECT3DDEVICE9 pDevice);

	// アルファテスト
    static void SetRenderALPHAREF_START(LPDIRECT3DDEVICE9 pDevice, int Power);

	// アルファテスト
    static void SetRenderALPHAREF_END(LPDIRECT3DDEVICE9 pDevice);

	// 加算合成
    static void SetRenderSUB(LPDIRECT3DDEVICE9 pDevice);

	// 描画時にZバッファを参照するか否か
    static void SetRenderZENABLE_START(LPDIRECT3DDEVICE9 pDevice);

	// 描画時にZバッファを参照するか否か
    static void SetRenderZENABLE_END(LPDIRECT3DDEVICE9 pDevice);

	// Zバッファを描画するか否か
    static void SetRenderZWRITEENABLE_START(LPDIRECT3DDEVICE9 pDevice);

	// Zバッファを描画するか否か
    static void SetRenderZWRITEENABLE_END(LPDIRECT3DDEVICE9 pDevice);

	// ワイヤフレームで描画します
    static void SetRenderWIREFRAME(LPDIRECT3DDEVICE9 pDevice);

	// 塗りつぶします
    static void SetRenderSOLID(LPDIRECT3DDEVICE9 pDevice);
};

#endif // !_SetRender_H_
