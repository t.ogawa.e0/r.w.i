//==========================================================================
// ダイレクインプットマネージャー[dinput_manager.h]
// author: tatsuya ogawa
//==========================================================================
#include "dinput_manager.h"

//==========================================================================
// 実体
//==========================================================================
std::list<CDirectInputManager*> CDirectInputManager::m_DirectInput; // オブジェクト格納

//==========================================================================
// 登録済みオブジェクトの更新
void CDirectInputManager::UpdateAll(void)
{
    for (auto itr = m_DirectInput.begin(); itr != m_DirectInput.end(); ++itr)
    {
        (*itr)->Update();
    }
}

//==========================================================================
// 登録済みデバイスの破棄
void CDirectInputManager::ReleaseALL(void)
{
    for (auto itr = m_DirectInput.begin(); itr != m_DirectInput.end(); ++itr)
    {
        delete (*itr);
    }
    m_DirectInput.clear();
}