//==========================================================================
// 2Dポリゴン[2DPolygon.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "2DPolygon.h"

//==========================================================================
// 初期化 失敗時true
// Input = 使用するテクスチャのパス ダブルポインタに対応
// NumData = 要素数
// AutoTextureResize = テクスチャのサイズ自動調節機能 trueで有効化
bool C2DPolygon::Init(const char ** Input, int NumData, bool AutoTextureResize)
{
	for (int i = 0; i < NumData; i++)
	{
		if (this->Init(Input[i], AutoTextureResize))
		{
			return true;
		}
	}

	return false;
}

//==========================================================================
// 初期化 失敗時true
// Input = 使用するテクスチャのパス
// AutoTextureResize = テクスチャのサイズ自動調節機能 trueで有効化
bool C2DPolygon::Init(const char * Input, bool AutoTextureResize)
{
	// テクスチャの格納
	if (this->m_texture.init(Input))
	{
		return true;
	}

	// バッファが登録されていないとき
	if (!this->m_Lock)
	{
		if (this->CreateVertexBuffer(sizeof(VERTEX_3) * 4, D3DUSAGE_WRITEONLY, FVF_VERTEX_3, D3DPOOL_MANAGED, &this->m_pVertexBuffer, nullptr))
		{
			return true;
		}
		this->m_Lock = true;
	}

    // 画像サイズ自動調節
    if (AutoTextureResize)
    {
        float scale = CDeviceManager::GetDXDevice()->screenbuffscale();
        CTexvec<int> vtex = CTexvec<int>(0, 0, 0, 0);
        Tex_t * ptex = this->m_texture.get(this->m_texture.size() - 1);
        ptex->resetsize();
        vtex = CTexvec<int>(0, 0, (int)((ptex->getsize()->w*scale) + 0.5f), (int)((ptex->getsize()->h*scale) + 0.5f));
        ptex->setsize(vtex);
    }

	return false;
}

//==========================================================================
// 更新
void C2DPolygon::Update(void)
{
    if (this->m_texture.size() != 0 && this->m_ObjectData.size() != 0)
    {
        // 登録済みオブジェクトのデータの更新
        for (auto itr = this->m_ObjectData.begin(); itr != this->m_ObjectData.end(); ++itr)
        {
            Tex_t * ptex = this->m_texture.get(itr->m_2dobj->getindex());

            // バッファのロック
            itr->m_2dobj->CreateVertexAngle(itr->vertex_3, *ptex->getsize());
            itr->m_2dobj->CreateVertex(itr->vertex_3, *ptex->getsize());
        }
    }
}

//==========================================================================
// 解放
void C2DPolygon::Release(void)
{
    this->m_temp.Release_(this->m_pVertexBuffer);
    this->m_texture.Release();
    this->m_Lock = false;
    this->ObjectRelease();
}

//==========================================================================
// 描画
// ADD = 加算合成の有無
void C2DPolygon::Draw(bool ADD)
{
    if (this->m_texture.size() != 0 && this->m_ObjectData.size() != 0)
    {
        LPDIRECT3DDEVICE9 pDevice = CDeviceManager::GetDXDevice()->GetD3DDevice();

        // FVFの設定
        pDevice->SetFVF(this->FVF_VERTEX_3);

        // 加算合成
        if (ADD == true)
        {
            this->SetRenderZWRITEENABLE_START(pDevice);
            this->SetRenderSUB(pDevice);
        }

        // バッファのセット
        VERTEX_3* pPseudo = nullptr;

        // 登録済みオブジェクトのデータを元に描画
        for (auto itr = this->m_ObjectData.begin(); itr != this->m_ObjectData.end(); ++itr)
        {
            // テクスチャの取得
            Tex_t * ptex = this->m_texture.get(itr->m_2dobj->getindex());

            this->m_pVertexBuffer->Lock(0, sizeof(VERTEX_3) * 4, (void**)&pPseudo, D3DLOCK_DISCARD);
            pPseudo[0].pos = itr->vertex_3[0].pos;
            pPseudo[1].pos = itr->vertex_3[1].pos;
            pPseudo[2].pos = itr->vertex_3[2].pos;
            pPseudo[3].pos = itr->vertex_3[3].pos;

            pPseudo[0].color = itr->vertex_3[0].color;
            pPseudo[1].color = itr->vertex_3[1].color;
            pPseudo[2].color = itr->vertex_3[2].color;
            pPseudo[3].color = itr->vertex_3[3].color;

            pPseudo[0].Tex = itr->vertex_3[0].Tex;
            pPseudo[1].Tex = itr->vertex_3[1].Tex;
            pPseudo[2].Tex = itr->vertex_3[2].Tex;
            pPseudo[3].Tex = itr->vertex_3[3].Tex;
            this->m_pVertexBuffer->Unlock(); // ロック解除

            pDevice->SetStreamSource(0, this->m_pVertexBuffer, 0, sizeof(VERTEX_3));

            pDevice->SetTexture(0, nullptr);
            pDevice->SetTexture(0, ptex->gettex());
            pDevice->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);
        }

        if (ADD == true)
        {
            this->SetRenderZWRITEENABLE_END(pDevice);
            this->SetRenderADD(pDevice);
        }
    }
}

//==========================================================================
// テクスチャのスケール変更
// index = テクスチャの番号
// scale = 入力した値が加算されます
void C2DPolygon::SetTexScale(int index, float scale)
{
    if (scale != 1.0f)
    {
        Tex_t * ptex = this->m_texture.get(index);
        if (ptex != nullptr)
        {
            CTexvec<int> vtex = CTexvec<int>(0, 0, (int)((ptex->getsize()->w*scale) + 0.5f), (int)((ptex->getsize()->h*scale) + 0.5f));

            ptex->setsize(vtex);
        }
    }
}

//==========================================================================
// アニメーション切り替わり時の判定 切り替え時true 
// Input = アニメーションの初期化をしたC2DObject
// AnimationCount = アニメーションカウンタ
bool C2DPolygon::GetPattanNum(C2DObject & Input, int * AnimationCount)
{
	if (AnimationCount != nullptr)
	{
		// フレームが一致したとき
		if ((*AnimationCount) == (Input.GetAnimParam()->Frame*Input.GetAnimParam()->Pattern) - 1)
		{
			this->AnimationCountInit(AnimationCount);
			this->AnimationCountInit(&Input.GetAnimParam()->Count);
			return true;
		}
	}
	return false;
}

//==========================================================================
// アニメーション切り替わり時の判定 切り替え時true 
// Input = アニメーションの初期化をしたC2DObject
bool C2DPolygon::GetPattanNum(C2DObject & Input)
{
	if (Input.GetAnimParam()->Key)
	{
		// フレームが一致したとき
		if (Input.GetAnimParam()->Count == (Input.GetAnimParam()->Frame*Input.GetAnimParam()->Pattern) - 1)
		{
			this->AnimationCountInit(&Input.GetAnimParam()->Count);
			return true;
		}
	}
	return false;
}

//==========================================================================
// アニメーション用のカウンタの初期化
// AnimationCount = アニメーションカウンタ
int * C2DPolygon::AnimationCountInit(int * AnimationCount)
{
	if (AnimationCount != nullptr)
	{
		(*AnimationCount) = -1;
	}
	return AnimationCount;
}
