//==========================================================================
// 影[Shadow.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _Shadow_H_
#define _Shadow_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "Rectangle.h"
#include "Vertex3D.h"
#include "SetRender.h"
#include "3DObject.h"
#include "Template.h"
#include "DataType.h"
#include "TextureLoader.h"
#include "Create.h"
#include "DeviceManager.h"
#include "ObjectInput.h"

//==========================================================================
//
// class  : CShadow
// Content: 影
//
//==========================================================================
class CShadow : private CRectangle, private VERTEX_3D, private CSetRender, private CCreate, public CObjectInput
{
private:
    // コピー禁止 (C++11)
    CShadow(const CShadow &) = delete;
    CShadow &operator=(const CShadow &) = delete;
public:
    CShadow() {
        this->m_pIndexBuffer = nullptr;
        this->m_pVertexBuffer = nullptr;
        this->m_pPseudo = nullptr;
        this->m_Lock = false;
        this->m_Device = nullptr;
    }
    ~CShadow() {
        this->Release();
    }

	// 初期化 失敗時true
	// Input = 使用するテクスチャのパス
	bool Init(const char * Input);

    // 更新
    void Update(void);

	// 解放
	void Release(void);

    // 描画
    void Draw(void);

	// デバイスのセット
	void SetDevice(const LPDIRECT3DDEVICE9 Input) { this->m_Device = Input; }
private:
	// 頂点バッファの生成
	void VertexBuffer(VERTEX_4 * Output, CUv<float> * uv);
private:
	VERTEX_4* m_pPseudo;// 頂点バッファのロック
	CTextureLoader m_texture; // テクスチャの格納
	LPDIRECT3DVERTEXBUFFER9 m_pVertexBuffer;	// バッファ
	LPDIRECT3DINDEXBUFFER9 m_pIndexBuffer; // インデックスバッファ
	CTemplates m_temp;
	bool m_Lock;
	LPDIRECT3DDEVICE9 m_Device; // デバイス
};

#endif // !_Shadow_H_
