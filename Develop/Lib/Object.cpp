//==========================================================================
// オブジェクト管理[Object.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "DeviceManager.h"
#include "Object.h"

//==========================================================================
// 実体
//==========================================================================
std::list<CObject*> CObject::m_object[(int)ID::MAX]; // オブジェクト格納

//==========================================================================
// 登録済みオブジェクトの初期化
bool CObject::InitAll(void)
{
    int max_data = 0; // 最大インスタンス数
    int now_data = 0; // 初期化済みデータ数

    // インスタンス数の取得
    for (int i = (int)ID::Default; i < (int)ID::MAX; i++)
    {
        max_data += m_object[i].size();
    }

    // 初期化
    for (int i = (int)ID::Default; i < (int)ID::MAX; i++)
    {
        for (auto itr = m_object[i].begin(); itr != m_object[i].end(); ++itr)
        {
            // 初期化カウンタを回す
            now_data++;

            // 初期化済みではないとき
            if ((*itr)->m_initializer != true)
            {
                (*itr)->Init();
                (*itr)->m_initializer = true;
                i = (int)ID::MAX;
                break;
            }
        }
    }

    // 初期化が終了
    if (max_data == now_data)
    {
        return true;
    }

    return false;
}

//==========================================================================
// 登録済みオブジェクトの解放
void CObject::ReleaseAll(void)
{
    for (int i = (int)ID::Default; i < (int)ID::MAX; i++)
    {
        for (auto itr = m_object[i].begin(); itr != m_object[i].end(); ++itr)
        {
            (*itr)->Uninit();
            delete (*itr);
        }
        m_object[i].clear();
    }
}

//==========================================================================
// 登録済みオブジェクトの更新
void CObject::UpdateAll(void)
{
    for (int i = (int)ID::Default; i < (int)ID::MAX; i++)
    {
        for (auto itr = m_object[i].begin(); itr != m_object[i].end(); ++itr)
        {
            (*itr)->Update();
        }
    }
}

//==========================================================================
// 登録済みオブジェクトの描画
void CObject::DrawAll(void)
{
    for (int i = (int)ID::Default; i < (int)ID::MAX; i++)
    {
        DrawPreparation((ID)i);
        for (auto itr = m_object[i].begin(); itr != m_object[i].end(); ++itr)
        {
            (*itr)->Draw();
        }
        DrawPostProcessing((ID)i);
    }
}

//==========================================================================
// 描画の準備
void CObject::DrawPreparation(ID ObjectID)
{
    LPDIRECT3DDEVICE9 pDevice = GetDirectX9Device();
    switch (ObjectID)
    {
    case CObject::ID::Default:
        // ライト設定
        break;
    case CObject::ID::Grid:
        // ライト設定
        pDevice->SetRenderState(D3DRS_LIGHTING, D3DZB_FALSE);
        break;
    case CObject::ID::Field:
        break;
    case CObject::ID::Cube:
        // ライト設定
        pDevice->SetRenderState(D3DRS_LIGHTING, D3DZB_TRUE);
        break;
    case CObject::ID::Mesh:
        // ライト設定
        pDevice->SetRenderState(D3DRS_LIGHTING, D3DZB_FALSE);

        //アルファ画像によるブレンド
        SetRenderALPHAREF_START(pDevice, 100);
        break;
    case CObject::ID::Shadow:
        // ライト設定
        pDevice->SetRenderState(D3DRS_LIGHTING, D3DZB_FALSE);

        // 減算処理
        SetRenderREVSUBTRACT(pDevice);
        break;
    case CObject::ID::Sphere:
        // パンチ抜き
        SetRenderALPHAREF_START(pDevice, 100);
        pDevice->SetRenderState(D3DRS_WRAP0, D3DWRAPCOORD_0);
        pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW); // 反時計回りの面を消去
        pDevice->SetRenderState(D3DRS_LIGHTING, D3DZB_FALSE); // ライティング
        pDevice->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);
        break;
    case CObject::ID::Xmodel:
        // ライト設定
        pDevice->SetRenderState(D3DRS_LIGHTING, D3DZB_TRUE);
        break;
    case CObject::ID::Billboard:
        // ライト設定
        pDevice->SetRenderState(D3DRS_LIGHTING, D3DZB_FALSE);

        // パンチ抜き
        SetRenderALPHAREF_START(pDevice, 100);
        break;
    case CObject::ID::Polygon2D:
        // 画質を高画質にする
        SamplerFitteringGraphical(pDevice);

        // Zバッファの無効
        SetRenderZENABLE_START(pDevice);
        break;
    case CObject::ID::Text:
        break;
    case CObject::ID::MAX:
        break;
    default:
        break;
    }
}

//==========================================================================
// 描画の後処理
void CObject::DrawPostProcessing(ID ObjectID)
{
    LPDIRECT3DDEVICE9 pDevice = GetDirectX9Device();
    switch (ObjectID)
    {
    case CObject::ID::Default:
        break;
    case CObject::ID::Grid:
        break;
    case CObject::ID::Field:
        break;
    case CObject::ID::Cube:
        break;
    case CObject::ID::Mesh:
        SetRenderALPHAREF_END(pDevice);
        break;
    case CObject::ID::Shadow:
        // 戻す
        SetRenderADD(pDevice);
        break;
    case CObject::ID::Sphere:
        SetRenderALPHAREF_END(pDevice);
        pDevice->SetRenderState(D3DRS_WRAP0, 0);
        break;
    case CObject::ID::Xmodel:
        break;
    case CObject::ID::Billboard:
        // パンチ抜き終了
        SetRenderALPHAREF_END(pDevice);
        break;
    case CObject::ID::Polygon2D:
        // Zバッファの有効化
        SetRenderZENABLE_END(pDevice);

        // 通常の画質に戻す
        SamplerFitteringLINEAR(pDevice);
        break;
    case CObject::ID::Text:
        break;
    case CObject::ID::MAX:
        break;
    default:
        break;
    }
}

//==========================================================================
// オブジェクトの取得
CObject * CObject::GetObjects(const ID ObjectID, const Type ObjectType)
{
    if ((ObjectID < ID::Default&&ID::MAX <= ObjectID) || (ObjectID == ID::SpecialBlock))
    {
        return nullptr;
    }

    for (auto itr = m_object[(int)ObjectID].begin(); itr != m_object[(int)ObjectID].end(); ++itr)
    {
        if ((*itr)->m_Type == ObjectType)
        {
            return (*itr);
        }
    }
    return nullptr;
}