//==========================================================================
// 数字処理[Number.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _Number_H_
#define _Number_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <string>
#include "2DPolygon.h"
#include "2DObject.h"
#include "DataType.h"
#include "ObjectVectorManager.h"

//==========================================================================
//
// class  : CNumber
// Content: 数字
//
//==========================================================================
class CNumber
{
private:
    // データリスト
    class CList
    {
    public:
        CList()
        {
            this->m_Digit = 0;
            this->m_Max = 0;
            this->m_Zero = 0;
            this->m_LeftAlignment = 0;

        };
        CList(int digit, int _max, bool zero, bool leftAlignment)
        {
            this->m_Digit = digit;
            this->m_Max = _max;
            this->m_Zero = zero;
            this->m_LeftAlignment = leftAlignment;
        };
        ~CList() {}
    public:
        int m_Digit; // 桁
        int m_Max; // 上限
        bool m_Zero; // ゼロ描画判定
        bool m_LeftAlignment; // 左寄せ判定
    };

    class CParam {
    public:
        CParam() {
            this->m_2DPos.Init(0);
            this->count = 0;
        }
        ~CParam() {}
    public:
        C2DObject m_2DPos;
        int count;
    };

    class CData {
    public:
        CData() {
            this->index = this->Frame = this->Pattern = this->Direction = 0;
            this->m_list = new CList;
            this->m_param = new CObjectVectorManager<CParam>;
            this->m_masterv2pos = new CVector2<float>;
        }
        ~CData() {
            delete this->m_list;
            delete this->m_masterv2pos;
            delete this->m_param;
        }
    public:
        int index, Frame, Pattern, Direction;
        CList *m_list;
        CObjectVectorManager<CParam>*m_param;
        CVector2<float> *m_masterv2pos;
    };
public:
    CNumber() {
        this->m_poly = new C2DPolygon;
    }
    ~CNumber() {
        this->Release();
    }

    // 初期化
    // AutoTextureResize = テクスチャのサイズ自動調節機能 trueで有効化
    bool Init(const std::string strTexName, bool AutoTextureResize = false);

    // Digit 桁
    // LeftAlignment 左寄せするかしないか
    // Zero ゼロ描画判定
    // 始点座標v2pos x,y
    void Set(int index, int Digit, bool LeftAlignment, bool Zero, const CVector2<float> v2pos);

    // アニメーション用初期化
    // index = テクスチャ番号
    // Frame = 更新フレーム 
    // Pattern = アニメーション数
    // Direction = 横一列のアニメーション数
    void SetAnim(int index, int Frame, int Pattern, int Direction);

    // 解放
    void Release(void);

    void SetPos(int index, const CVector2<float> v2pos) {
        *this->m_Data.Get(index)->m_masterv2pos = v2pos;
    }

    void MoveXPos(int index, float speed) {
        this->m_Data.Get(index)->m_masterv2pos->x += speed;
    }

    void MoveYPos(int index, float speed) {
        this->m_Data.Get(index)->m_masterv2pos->y += speed;
    }

    // テクスチャサイズ
    CTexvec<int> * GetTexSize(int Index) {
        return this->m_poly->GetTexSize(Index);
    }

    // テクスチャの枚数
    int GetNumTex(void) { return this->m_poly->GetNumTex(); }

    // テクスチャのサイズ変更
    // index = テクスチャの番号
    // Widht Height = 幅と高さ
    void SetTexSize(int index, int Widht, int Height) {
        this->m_poly->SetTexSize(index, Widht, Height);
    }

    // テクスチャのスケール変更
    // index = テクスチャの番号
    // scale = 入力した値が加算されます
    void SetTexScale(int index, float scale) {
        this->m_poly->SetTexScale(index, scale);
    }

    // テクスチャのサイズリセット
    // index = テクスチャの番号
    void ResetTexSize(int index) {
        this->m_poly->ResetTexSize(index);
    }

    // 更新
    CVector2<float> Update(int index, int SetNumber = 0);

    // 描画
    void Draw(bool ADD = false);
private:
    // max
    template <typename T> inline T Max(T a, T b);

    // min
    template <typename T> inline T Min(T a, T b);
private:
    CObjectVectorManager<CData>m_Data;
    C2DPolygon * m_poly;
};

//==========================================================================
// max
template<typename T>
inline T CNumber::Max(T a, T b)
{
    return (((a) > (b)) ? (a) : (b));
}

//==========================================================================
// min
template<typename T>
inline T CNumber::Min(T a, T b)
{
    return (((a) < (b)) ? (a) : (b));
}

#endif // !_Number_H_
