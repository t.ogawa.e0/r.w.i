//==========================================================================
// ダイレクトインプットのマウス[dinput_mouse.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _dinput_mouse_h_ 
#define _dinput_mouse_h_

//==========================================================================
// include
//==========================================================================
#include "dinput_manager.h"

//==========================================================================
//
// class  : CMouse
// Content: マウス
//
//==========================================================================
class CMouse : public CDirectInput, public CDirectInputManager
{
private:
    // コピー禁止 (C++11)
    CMouse(const CMouse &) = delete;
    CMouse &operator=(const CMouse &) = delete;
private:
    class CSpeed
    {
    public:
        CSpeed() {
            this->m_lX = (LONG)0;
            this->m_lY = (LONG)0;
            this->m_lZ = (LONG)0;
        }
        ~CSpeed() {}
    public:
        LONG m_lX; // X軸への速度
        LONG m_lY; // Y軸への速度
        LONG m_lZ; // Z軸への速度
    };
public:
    // 有効ボタン
    enum class ButtonKey
    {
        Left = 0,	// 左クリック
        Right,	// 右クリック
        Wheel,	// ホイール
        MAX,
    };
public:
    CMouse() {}
    ~CMouse() {}

    // 初期化
    bool Init(HINSTANCE hInstance, HWND hWnd);

    // 更新
    void Update(void)override;

    // Button Cast(int)
    ButtonKey KeyCast(int cast) { return (ButtonKey)cast; };

    // プレス
    bool Press(ButtonKey key);

    // トリガー
    bool Trigger(ButtonKey key);

    // リピート
    bool Repeat(ButtonKey key);

    // リリ−ス
    bool Release(ButtonKey key);

    // マウスの速度
    CSpeed Speed(void);

    // カーソルの座標
    POINT WIN32Cursor(void);

    // 左クリック
    SHORT WIN32LeftClick(void);

    // 右クリック
    SHORT WIN32RightClick(void);

    // マウスホイールホールド
    SHORT WIN32WheelHold(void);
private:
    static BOOL CALLBACK EnumAxesCallback(const DIDEVICEOBJECTINSTANCE *pdidoi, void *pContext);
private: // DirectInput
    DIMOUSESTATE2 m_State; // 入力情報ワーク
    BYTE m_StateTrigger[(int)sizeof(DIMOUSESTATE2::rgbButtons)];	// トリガー情報ワーク
    BYTE m_StateRelease[(int)sizeof(DIMOUSESTATE2::rgbButtons)]; // リリース情報ワーク
    BYTE m_StateRepeat[(int)sizeof(DIMOUSESTATE2::rgbButtons)]; // リピート情報ワーク
    int m_StateRepeatCnt[(int)sizeof(DIMOUSESTATE2::rgbButtons)]; // リピートカウンタ
private: // WindowsAPI
    POINT m_mousePos;
    HWND m_hWnd;
};

#endif // !_dinput_mouse_h_
