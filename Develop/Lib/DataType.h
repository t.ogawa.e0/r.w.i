//==========================================================================
// データクラス[DataType.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _DataType_H_
#define _DataType_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <cstddef> 
#include <iostream>

//==========================================================================
// Declaration
//==========================================================================

//==========================================================================
//
// class  : CUv
// Content : CUv
// how to use : CUv<float>,CUv<int>,CUv<double>...
//
//==========================================================================
template<typename types>
class CUv // UV
{
public:
	CUv() {
		this->u0 = (types)0;
		this->v0 = (types)0;
		this->u1 = (types)0;
		this->v1 = (types)0;
	}
    CUv(types _u0, types _v0, types _u1, types _v1) {
        this->u0 = _u0;
        this->v0 = _v0;
        this->u1 = _u1;
        this->v1 = _v1;
    }
	~CUv() {}

	// Member Selection
	const CUv* operator->(void) const noexcept;
	// Member Selection
	CUv* operator->(void) noexcept;

	// Compare
	bool operator<(const CUv&);
	// Compare
	bool operator>(const CUv&);
	// Compare
	bool operator<=(const CUv&);
	// Compare
	bool operator>=(const CUv&);
	// Equality Compare
	bool operator == (const CUv&) const;
	// Equality Compare
	bool operator != (const CUv&) const;

	// Compare
	bool operator<(const types);
	// Compare
	bool operator>(const types);
	// Compare
	bool operator<=(const types);
	// Compare
	bool operator>=(const types);
	// Equality Compare
	bool operator == (const types) const;
	// Equality Compare
	bool operator != (const types) const;

	// Unary Negation/Plus
	CUv operator + (void) const;
	// Unary Negation/Plus
	CUv operator - (void) const;

	// Prefix Increment / Decremrnt
	CUv &operator ++(void);
	// Postfix Increment / Decrement
	CUv operator ++(int);
	// Prefix Increment / Decremrnt
	CUv &operator --(void);
	// Postfix Increment / Decrement
	CUv operator --(int);

	CUv operator +(const CUv &);
	CUv operator -(const CUv &);
	CUv operator *(const CUv &);
	CUv operator /(const CUv &);

	CUv &operator =(const CUv &);
	CUv &operator +=(const CUv &);
	CUv &operator -=(const CUv &);
	CUv &operator *=(const CUv &);
	CUv &operator /=(const CUv &);

	CUv operator + (const types) const;
	CUv operator - (const types) const;
	CUv operator * (const types) const;
	CUv operator / (const types) const;

	CUv &operator =(const types);
	CUv &operator +=(const types);
	CUv &operator -=(const types);
	CUv &operator *=(const types);
	CUv &operator /=(const types);

	// Array Subscript
	const CUv& operator[](std::size_t) const&;
	// Array Subscript
	CUv& operator[](std::size_t) &;
	// Array Subscript
	CUv operator[](std::size_t) && ;
public:
	types u0;
	types v0;
	types u1;
	types v1;
};

//==========================================================================
//
// class  : CTexvec
// Content : CTexvec
// how to use : CTexvec<float>,CTexvec<int>,CTexvec<double>...
//
//==========================================================================
template<typename types>
class CTexvec
{
public:
    CTexvec() {
		this->x = (types)0;
		this->y = (types)0;
		this->w = (types)0;
		this->h = (types)0;
	}
    CTexvec(types _x, types _y, types _w, types _h) {
        this->x = _x;
        this->y = _y;
        this->w = _w;
        this->h = _h;
    }
	~CTexvec(){}

	// Member Selection
	const CTexvec* operator->(void) const noexcept;
	// Member Selection
	CTexvec* operator->(void) noexcept;

	// Compare
	bool operator<(const CTexvec&);
	// Compare
	bool operator>(const CTexvec&);
	// Compare
	bool operator<=(const CTexvec&);
	// Compare
	bool operator>=(const CTexvec&);
	// Equality Compare
	bool operator == (const CTexvec&) const;
	// Equality Compare
	bool operator != (const CTexvec&) const;

	// Compare
	bool operator<(const types);
	// Compare
	bool operator>(const types);
	// Compare
	bool operator<=(const types);
	// Compare
	bool operator>=(const types);
	// Equality Compare
	bool operator == (const types) const;
	// Equality Compare
	bool operator != (const types) const;

	// Unary Negation/Plus
	CTexvec operator + (void) const;
	// Unary Negation/Plus
	CTexvec operator - (void) const;

	// Prefix Increment / Decremrnt
	CTexvec &operator ++(void);
	// Postfix Increment / Decrement
	CTexvec operator ++(int);
	// Prefix Increment / Decremrnt
	CTexvec &operator --(void);
	// Postfix Increment / Decrement
	CTexvec operator --(int);

	CTexvec operator +(const CTexvec &);
	CTexvec operator -(const CTexvec &);
	CTexvec operator *(const CTexvec &);
	CTexvec operator /(const CTexvec &);

	CTexvec &operator =(const CTexvec &);
	CTexvec &operator +=(const CTexvec &);
	CTexvec &operator -=(const CTexvec &);
	CTexvec &operator *=(const CTexvec &);
	CTexvec &operator /=(const CTexvec &);

	CTexvec operator + (const types) const;
	CTexvec operator - (const types) const;
	CTexvec operator * (const types) const;
	CTexvec operator / (const types) const;

	CTexvec &operator =(const types);
	CTexvec &operator +=(const types);
	CTexvec &operator -=(const types);
	CTexvec &operator *=(const types);
	CTexvec &operator /=(const types);

	// Array Subscript
	const CTexvec& operator[](std::size_t) const&;
	// Array Subscript
	CTexvec& operator[](std::size_t) &;
	// Array Subscript
	CTexvec operator[](std::size_t) && ;
public:
	types x;
	types y;
	types w;
	types h;
};

//==========================================================================
//
// class  : CColor
// Content : CColor
// how to use : CColor<float>,CColor<int>,CColor<double>...
//
//==========================================================================
template<typename types>
class CColor
{
public:
    CColor() {
        this->r = (types)0;
        this->g = (types)0;
        this->b = (types)0;
        this->a = (types)0;
    };
	CColor(types _r, types _g, types _b, types _a) {
		this->r = _r;
		this->g = _g;
		this->b = _b;
		this->a = _a;
	}
	~CColor() {}

	const D3DCOLOR get(void) const { return D3DCOLOR_RGBA((int)this->r, (int)this->g, (int)this->b, (int)this->a); }

	// Member Selection
	const CColor* operator->(void) const noexcept;
	// Member Selection
	CColor* operator->(void) noexcept;

	// Compare
	bool operator<(const CColor&);
	// Compare
	bool operator>(const CColor&);
	// Compare
	bool operator<=(const CColor&);
	// Compare
	bool operator>=(const CColor&);
	// Equality Compare
	bool operator == (const CColor&) const;
	// Equality Compare
	bool operator != (const CColor&) const;

	// Compare
	bool operator<(const types);
	// Compare
	bool operator>(const types);
	// Compare
	bool operator<=(const types);
	// Compare
	bool operator>=(const types);
	// Equality Compare
	bool operator == (const types) const;
	// Equality Compare
	bool operator != (const types) const;

	// Unary Negation/Plus
	CColor operator + (void) const;
	// Unary Negation/Plus
	CColor operator - (void) const;

	// Prefix Increment / Decremrnt
	CColor &operator ++(void);
	// Postfix Increment / Decrement
	CColor operator ++(int);
	// Prefix Increment / Decremrnt
	CColor &operator --(void);
	// Postfix Increment / Decrement
	CColor operator --(int);

	CColor operator +(const CColor &);
	CColor operator -(const CColor &);
	CColor operator *(const CColor &);
	CColor operator /(const CColor &);

	CColor &operator =(const CColor &);
	CColor &operator +=(const CColor &);
	CColor &operator -=(const CColor &);
	CColor &operator *=(const CColor &);
	CColor &operator /=(const CColor &);

	CColor operator + (const types) const;
	CColor operator - (const types) const;
	CColor operator * (const types) const;
	CColor operator / (const types) const;

	CColor &operator =(const types);
	CColor &operator +=(const types);
	CColor &operator -=(const types);
	CColor &operator *=(const types);
	CColor &operator /=(const types);

	// Array Subscript
	const CColor& operator[](std::size_t) const&;
	// Array Subscript
	CColor& operator[](std::size_t) &;
	// Array Subscript
	CColor operator[](std::size_t) && ;
public:
	types r;
	types g;
	types b;
	types a;
};

//==========================================================================
//
// class  : CVector4
// Content : CVector4
// how to use : CVector4<float>,CVector4<int>,CVector4<double>...
//
//==========================================================================
template<typename types>
class CVector4
{
public:
    CVector4() {
        this->x = (types)0;
        this->y = (types)0;
        this->z = (types)0;
        this->w = (types)0;
    }
    CVector4(types _x, types _y) {
        this->x = _x;
        this->y = _y;
        this->z = (types)0;
        this->w = (types)0;
    }
    CVector4(types _x, types _y, types _z) {
        this->x = _x;
        this->y = _y;
        this->z = _z;
        this->w = (types)0;
    }
    CVector4(types _x, types _y, types _z, types _w) {
        this->x = _x;
        this->y = _y;
        this->z = _z;
        this->w = _w;
    }
    ~CVector4() {}

    // Member Selection
    const CVector4* operator->(void) const noexcept;
    // Member Selection
    CVector4* operator->(void) noexcept;

    // Compare
    bool operator<(const CVector4&);
    // Compare
    bool operator>(const CVector4&);
    // Compare
    bool operator<=(const CVector4&);
    // Compare
    bool operator>=(const CVector4&);
    // Equality Compare
    bool operator == (const CVector4&) const;
    // Equality Compare
    bool operator != (const CVector4&) const;

    // Compare
    bool operator<(const types);
    // Compare
    bool operator>(const types);
    // Compare
    bool operator<=(const types);
    // Compare
    bool operator>=(const types);
    // Equality Compare
    bool operator == (const types) const;
    // Equality Compare
    bool operator != (const types) const;

    // Unary Negation/Plus
    CVector4 operator + (void) const;
    // Unary Negation/Plus
    CVector4 operator - (void) const;

    // Prefix Increment / Decremrnt
    CVector4 &operator ++(void);
    // Postfix Increment / Decrement
    CVector4 operator ++(int);
    // Prefix Increment / Decremrnt
    CVector4 &operator --(void);
    // Postfix Increment / Decrement
    CVector4 operator --(int);

    CVector4 operator +(const CVector4 &);
    CVector4 operator -(const CVector4 &);
    CVector4 operator *(const CVector4 &);
    CVector4 operator /(const CVector4 &);

    CVector4 &operator =(const CVector4 &);
    CVector4 &operator +=(const CVector4 &);
    CVector4 &operator -=(const CVector4 &);
    CVector4 &operator *=(const CVector4 &);
    CVector4 &operator /=(const CVector4 &);

    CVector4 operator + (const types) const;
    CVector4 operator - (const types) const;
    CVector4 operator * (const types) const;
    CVector4 operator / (const types) const;

    CVector4 &operator =(const types);
    CVector4 &operator +=(const types);
    CVector4 &operator -=(const types);
    CVector4 &operator *=(const types);
    CVector4 &operator /=(const types);

    // Array Subscript
    const CVector4& operator[](std::size_t n) const&;
    // Array Subscript
    CVector4& operator[](std::size_t) &;
    // Array Subscript
    CVector4 operator[](std::size_t) && ;

    // D3DXVECTOR2 << CVector
    D3DXVECTOR2 D3DXVECTOR2_CAST(void) { return D3DXVECTOR2((float)this->x, (float)this->y); }
    // D3DXVECTOR3 << CVector
    D3DXVECTOR3 D3DXVECTOR3_CAST(void) { return D3DXVECTOR3((float)this->x, (float)this->y, (float)this->z); }
    // D3DXVECTOR4 << CVector
    D3DXVECTOR4 D3DXVECTOR4_CAST(void) { return D3DXVECTOR4((float)this->x, (float)this->y, (float)this->z, (float)this->w); }
public:
    types x;
    types y;
    types z;
    types w;
};

//==========================================================================
//
// class  : CVector2
// Content : CVector2
// how to use : CVector2<float>,CVector2<int>,CVector2<double>...
//
//==========================================================================
template<typename types>
class CVector2
{
public:
    CVector2() {
        this->x = (types)0;
        this->y = (types)0;
    }
    CVector2(types _x, types _y) {
        this->x = _x;
        this->y = _y;
    }
    ~CVector2() {}

    // Member Selection
    const CVector2* operator->(void) const noexcept;
    // Member Selection
    CVector2* operator->(void) noexcept;

    // Compare
    bool operator<(const CVector2&);
    // Compare
    bool operator>(const CVector2&);
    // Compare
    bool operator<=(const CVector2&);
    // Compare
    bool operator>=(const CVector2&);
    // Equality Compare
    bool operator == (const CVector2&) const;
    // Equality Compare
    bool operator != (const CVector2&) const;

    // Compare
    bool operator<(const types);
    // Compare
    bool operator>(const types);
    // Compare
    bool operator<=(const types);
    // Compare
    bool operator>=(const types);
    // Equality Compare
    bool operator == (const types) const;
    // Equality Compare
    bool operator != (const types) const;

    // Unary Negation/Plus
    CVector2 operator + (void) const;
    // Unary Negation/Plus
    CVector2 operator - (void) const;

    // Prefix Increment / Decremrnt
    CVector2 &operator ++(void);
    // Postfix Increment / Decrement
    CVector2 operator ++(int);
    // Prefix Increment / Decremrnt
    CVector2 &operator --(void);
    // Postfix Increment / Decrement
    CVector2 operator --(int);

    CVector2 operator +(const CVector2 &);
    CVector2 operator -(const CVector2 &);
    CVector2 operator *(const CVector2 &);
    CVector2 operator /(const CVector2 &);

    CVector2 &operator =(const CVector2 &);
    CVector2 &operator +=(const CVector2 &);
    CVector2 &operator -=(const CVector2 &);
    CVector2 &operator *=(const CVector2 &);
    CVector2 &operator /=(const CVector2 &);

    CVector2 operator + (const types) const;
    CVector2 operator - (const types) const;
    CVector2 operator * (const types) const;
    CVector2 operator / (const types) const;

    CVector2 &operator =(const types);
    CVector2 &operator +=(const types);
    CVector2 &operator -=(const types);
    CVector2 &operator *=(const types);
    CVector2 &operator /=(const types);

    // Array Subscript
    const CVector2& operator[](std::size_t n) const&;
    // Array Subscript
    CVector2& operator[](std::size_t) &;
    // Array Subscript
    CVector2 operator[](std::size_t) && ;
public:
    types x;
    types y;
};

//==========================================================================
//
// class  : CVector3
// Content : CVector3
// how to use : CVector3<float>,CVector3<int>,CVector3<double>...
//
//==========================================================================
template<typename types>
class CVector3
{
public:
    CVector3() {
        this->x = (types)0;
        this->y = (types)0;
        this->z = (types)0;
    }
    CVector3(types _x, types _y) {
        this->x = _x;
        this->y = _y;
        this->z = (types)0;
    }
    CVector3(types _x, types _y, types _z) {
        this->x = _x;
        this->y = _y;
        this->z = _z;
    }
    ~CVector3() {}

    // Member Selection
    const CVector3* operator->(void) const noexcept;
    // Member Selection
    CVector3* operator->(void) noexcept;

    // Compare
    bool operator<(const CVector3&);
    // Compare
    bool operator>(const CVector3&);
    // Compare
    bool operator<=(const CVector3&);
    // Compare
    bool operator>=(const CVector3&);
    // Equality Compare
    bool operator == (const CVector3&) const;
    // Equality Compare
    bool operator != (const CVector3&) const;

    // Compare
    bool operator<(const types);
    // Compare
    bool operator>(const types);
    // Compare
    bool operator<=(const types);
    // Compare
    bool operator>=(const types);
    // Equality Compare
    bool operator == (const types) const;
    // Equality Compare
    bool operator != (const types) const;

    // Unary Negation/Plus
    CVector3 operator + (void) const;
    // Unary Negation/Plus
    CVector3 operator - (void) const;

    // Prefix Increment / Decremrnt
    CVector3 &operator ++(void);
    // Postfix Increment / Decrement
    CVector3 operator ++(int);
    // Prefix Increment / Decremrnt
    CVector3 &operator --(void);
    // Postfix Increment / Decrement
    CVector3 operator --(int);

    CVector3 operator +(const CVector3 &);
    CVector3 operator -(const CVector3 &);
    CVector3 operator *(const CVector3 &);
    CVector3 operator /(const CVector3 &);

    CVector3 &operator =(const CVector3 &);
    CVector3 &operator +=(const CVector3 &);
    CVector3 &operator -=(const CVector3 &);
    CVector3 &operator *=(const CVector3 &);
    CVector3 &operator /=(const CVector3 &);

    CVector3 operator + (const types) const;
    CVector3 operator - (const types) const;
    CVector3 operator * (const types) const;
    CVector3 operator / (const types) const;

    CVector3 &operator =(const types);
    CVector3 &operator +=(const types);
    CVector3 &operator -=(const types);
    CVector3 &operator *=(const types);
    CVector3 &operator /=(const types);

    // Array Subscript
    const CVector3& operator[](std::size_t n) const&;
    // Array Subscript
    CVector3& operator[](std::size_t) &;
    // Array Subscript
    CVector3 operator[](std::size_t) && ;
public:
    types x;
    types y;
    types z;
};

//==========================================================================
//
// class  : CMaterial
// Content : CMaterial
// how to use : CMaterial<float>,CMaterial<int>,CMaterial<double>...
//
//==========================================================================
template<typename types>
class CMaterial
{
public:
    CMaterial() {
        this->Ambient = CColor<types>((types)0);
        this->Diffuse = CColor<types>((types)0);
        this->Specular = CColor<types>((types)0);
        this->Emission = CColor<types>((types)0);
        this->Shininess = (types)0;
    }
    ~CMaterial() {}

    // Member Selection
    const CMaterial* operator->(void) const noexcept;

    // Member Selection
    CMaterial* operator->(void) noexcept;

    CMaterial &operator =(const CMaterial &);

    // Array Subscript
    const CMaterial& operator[](std::size_t n) const&;
    // Array Subscript
    CMaterial& operator[](std::size_t) &;
    // Array Subscript
    CMaterial operator[](std::size_t) && ;
public:
    CColor<types> Ambient;
    CColor<types> Diffuse;
    CColor<types> Specular;
    CColor<types> Emission;
    types Shininess;
};

//==========================================================================
//
// class  : CVertex_3D
// Content : CVertex_3D
// how to use : CVertex_3D<float>,CVertex_3D<int>,CVertex_3D<double>...
//
//==========================================================================
template<typename types>
class CVertex_3D
{
public:
    CVertex_3D() {
        this->TexturePos = CVector2<types>((types)0);
        this->Normal = CVector3<types>((types)0);
        this->Position = CVector3<types>((types)0);
        this->Diffuse = CColor<types>((types)0);
    }
    ~CVertex_3D() {}

    // Member Selection
    const CVertex_3D* operator->(void) const noexcept;

    // Member Selection
    CVertex_3D* operator->(void) noexcept;

    CVertex_3D &operator =(const CVertex_3D &);

    // Array Subscript
    const CVertex_3D& operator[](std::size_t n) const&;
    // Array Subscript
    CVertex_3D& operator[](std::size_t) &;
    // Array Subscript
    CVertex_3D operator[](std::size_t) && ;
public:
    CVector2<types> TexturePos;
    CColor<types> Diffuse;
    CVector3<types> Normal;
    CVector3<types> Position;
};

//==========================================================================
// Definition
//==========================================================================

//==========================================================================
//
// class  : CUv
// Content : CUv
// how to use : CUv<float>,CUv<int>,CUv<double>...
//
//==========================================================================

// Member Selection
template<typename types>
inline const CUv<types> * CUv<types>::operator->(void) const noexcept
{
	return this;
}

// Member Selection
template<typename types>
inline CUv<types> * CUv<types>::operator->(void) noexcept
{
	return this;
}

// Compare
template<typename types>
inline bool CUv<types>::operator<(const CUv & v)
{
	if (this->u0 < v.u0 && this->v0 < v.v0 && this->u1 < v.u1 && this->v1 < v.v1)
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CUv<types>::operator>(const CUv & v)
{
	if (this->u0 > v.u0 && this->v0 > v.v0 && this->u1 > v.u1 && this->v1 > v.v1)
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CUv<types>::operator<=(const CUv & v)
{
	if ((this->u0 <= v.u0) || (this->v0 <= v.v0) || (this->u1 <= v.u1) || (this->v1 <= v.v1))
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CUv<types>::operator>=(const CUv & v)
{
	if ((this->u0 >= v.u0) || (this->v0 >= v.v0) || (this->u1 >= v.u1) || (this->v1 >= v.v1))
	{
		return true;
	}
	return false;
}

// Equality Compare
template<typename types>
inline bool CUv<types>::operator==(const CUv & v) const
{
	if ((this->u0 == v.u0) || (this->v0 == v.v0) || (this->u1 == v.u1) || (this->v1 == v.v1))
	{
		return true;
	}
	return false;
}

// Equality Compare
template<typename types>
inline bool CUv<types>::operator!=(const CUv & v) const
{
	if ((this->u0 != v.u0) || (this->v0 != v.v0) || (this->u1 != v.u1) || (this->v1 != v.v1))
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CUv<types>::operator<(const types t)
{
	if (this->u0 < t && this->v0 < t && this->u1 < t && this->v1 < t)
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CUv<types>::operator>(const types t)
{
	if (this->u0 > t && this->v0 > t && this->u1 > t && this->v1 > t)
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CUv<types>::operator<=(const types t)
{
	if ((this->u0 <= t) || (this->v0 <= t) || (this->u1 <= t) || (this->v1 <= t))
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CUv<types>::operator>=(const types t)
{
	if ((this->u0 >= t) || (this->v0 >= t) || (this->u1 >= t) || (this->v1 >= t))
	{
		return true;
	}
	return false;
}

// Equality Compare
template<typename types>
inline bool CUv<types>::operator==(const types t) const
{
	if ((this->u0 == t) || (this->v0 == t) || (this->u1 == t) || (this->v1 == t))
	{
		return true;
	}
	return false;
}

// Equality Compare
template<typename types>
inline bool CUv<types>::operator!=(const types t) const
{
	if ((this->u0 != t) || (this->v0 != t) || (this->u1 != t) || (this->v1 != t))
	{
		return true;
	}
	return false;
}

// Unary Negation/Plus
template<typename types>
inline CUv<types> CUv<types>::operator+(void) const
{
	return *this;
}

// Unary Negation/Plus
template<typename types>
inline CUv<types> CUv<types>::operator-(void) const
{
	return *this;
}

// Postfix Increment / Decrement
template<typename types>
inline CUv<types> & CUv<types>::operator++(void)
{
	this->u0++;
	this->v0++;
	this->u1++;
	this->v1++;
	return *this;
}

// Postfix Increment / Decrement
template<typename types>
inline CUv<types> CUv<types>::operator++(int)
{
	CUv<types> out = *this;
	this->u0++;
	this->v0++;
	this->u1++;
	this->v1++;
	return out;
}

// Postfix Increment / Decrement
template<typename types>
inline CUv<types> & CUv<types>::operator--(void)
{
	this->u0--;
	this->v0--;
	this->u1--;
	this->v1--;
	return *this;
}

// Postfix Increment / Decrement
template<typename types>
inline CUv<types> CUv<types>::operator--(int)
{
	CUv<types> out = *this;
	this->u0--;
	this->v0--;
	this->u1--;
	this->v1--;
	return out;
}

template<typename types>
inline CUv<types> CUv<types>::operator+(const CUv & v)
{
	CUv<types> out;
	out.u0 = this->u0 + v.u0;
	out.v0 = this->v0 + v.v0;
	out.u1 = this->u1 + v.u1;
	out.v1 = this->v1 + v.v1;
	return out;
}

template<typename types>
inline CUv<types> CUv<types>::operator-(const CUv & v)
{
	CUv<types> out;
	out.u0 = this->u0 - v.u0;
	out.v0 = this->v0 - v.v0;
	out.u1 = this->u1 - v.u1;
	out.v1 = this->v1 - v.v1;
	return out;
}

template<typename types>
inline CUv<types> CUv<types>::operator*(const CUv & v)
{
	CUv<types> out;
	out.u0 = this->u0 * v.u0;
	out.v0 = this->v0 * v.v0;
	out.u1 = this->u1 * v.u1;
	out.v1 = this->v1 * v.v1;
	return out;
}

template<typename types>
inline CUv<types> CUv<types>::operator/(const CUv & v)
{
	CUv<types> out;
	out.u0 = this->u0 / v.u0;
	out.v0 = this->v0 / v.v0;
	out.u1 = this->u1 / v.u1;
	out.v1 = this->v1 / v.v1;
	return out;
}

template<typename types>
inline CUv<types> & CUv<types>::operator=(const CUv & v)
{
	this->u0 = v.u0;
	this->v0 = v.v0;
	this->u1 = v.u1;
	this->v1 = v.v1;
	return *this;
}

template<typename types>
inline CUv<types> & CUv<types>::operator+=(const CUv & v)
{
	this->u0 += v.u0;
	this->v0 += v.v0;
	this->u1 += v.u1;
	this->v1 += v.v1;
	return *this;
}

template<typename types>
inline CUv<types> & CUv<types>::operator-=(const CUv & v)
{
	this->u0 -= v.u0;
	this->v0 -= v.v0;
	this->u1 -= v.u1;
	this->v1 -= v.v1;
	return *this;
}

template<typename types>
inline CUv<types> & CUv<types>::operator*=(const CUv & v)
{
	this->u0 *= v.u0;
	this->v0 *= v.v0;
	this->u1 *= v.u1;
	this->v1 *= v.v1;
	return *this;
}

template<typename types>
inline CUv<types> & CUv<types>::operator/=(const CUv & v)
{
	this->u0 /= v.u0;
	this->v0 /= v.v0;
	this->u1 /= v.u1;
	this->v1 /= v.v1;
	return *this;
}

template<typename types>
inline CUv<types> CUv<types>::operator+(const types t) const
{
	return CUv(this->u0 + t, this->v0 + t, this->u1 + t, this->v1 + t);
}

template<typename types>
inline CUv<types> CUv<types>::operator-(const types t) const
{
	return CUv(this->u0 - t, this->v0 - t, this->u1 - t, this->v1 - t);
}

template<typename types>
inline CUv<types> CUv<types>::operator*(const types t) const
{
	return CUv(this->u0 * t, this->v0 * t, this->u1 * t, this->v1 * t);
}

template<typename types>
inline CUv<types> CUv<types>::operator/(const types t) const
{
	types tInv = (types)1;
	tInv = tInv / t;
	return CUv(this->u0 * tInv, this->v0 * tInv, this->u1 * tInv, this->v1 * tInv);
}

template<typename types>
inline CUv<types> & CUv<types>::operator=(const types t)
{
	this->u0 = t;
	this->v0 = t;
	this->u1 = t;
	this->v1 = t;
	return *this;
}

template<typename types>
inline CUv<types> & CUv<types>::operator+=(const types t)
{
	this->u0 += t;
	this->v0 += t;
	this->u1 += t;
	this->v1 += t;
	return *this;
}

template<typename types>
inline CUv<types> & CUv<types>::operator-=(const types t)
{
	this->u0 -= t;
	this->v0 -= t;
	this->u1 -= t;
	this->v1 -= t;
	return *this;
}

template<typename types>
inline CUv<types> & CUv<types>::operator*=(const types t)
{
	this->u0 *= t;
	this->v0 *= t;
	this->u1 *= t;
	this->v1 *= t;
	return *this;
}

template<typename types>
inline CUv<types> & CUv<types>::operator/=(const types t)
{
	this->u0 /= t;
	this->v0 /= t;
	this->u1 /= t;
	this->v1 /= t;
	return *this;
}

// Array Subscript
template<typename types>
inline const CUv<types> & CUv<types>::operator[](std::size_t n) const &
{
	return this[n];
}

// Array Subscript
template<typename types>
inline CUv<types> & CUv<types>::operator[](std::size_t n) &
{
	return this[n];
}

// Array Subscript
template<typename types>
inline CUv<types> CUv<types>::operator[](std::size_t n) &&
{
	return std::move(this[n]);
}

//==========================================================================
//
// class  : CTexvec
// Content : CTexvec
// how to use : CTexvec<float>,CTexvec<int>,CTexvec<double>...
//
//==========================================================================

// Member Selection
template<typename types>
inline const CTexvec<types> * CTexvec<types>::operator->(void) const noexcept
{
	return this;
}

// Member Selection
template<typename types>
inline CTexvec<types> * CTexvec<types>::operator->(void) noexcept
{
	return this;
}

// Compare
template<typename types>
inline bool CTexvec<types>::operator<(const CTexvec & v)
{
	if (this->x < v.x && this->y < v.y && this->w < v.w && this->h < v.h)
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CTexvec<types>::operator>(const CTexvec & v)
{
	if (this->x > v.x && this->y > v.y && this->w > v.w && this->h > v.h)
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CTexvec<types>::operator<=(const CTexvec & v)
{
	if (this->x <= v.x && this->y <= v.y && this->w <= v.w && this->h <= v.h)
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CTexvec<types>::operator>=(const CTexvec & v)
{
	if (this->x >= v.x && this->y >= v.y && this->w >= v.w && this->h >= v.h)
	{
		return true;
	}
	return false;
}

// Equality Compare
template<typename types>
inline bool CTexvec<types>::operator==(const CTexvec & v) const
{
	if ((this->x == v.x) || (this->y == v.y) || (this->w == v.w) || (this->h == v.h))
	{
		return true;
	}
	return false;
}

// Equality Compare
template<typename types>
inline bool CTexvec<types>::operator!=(const CTexvec & v) const
{
	if ((this->x != v.x) || (this->y != v.y) || (this->w != v.w) || (this->h != v.h))
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CTexvec<types>::operator<(const types t)
{
	if (this->x < t && this->y < t && this->w < t && this->h < t)
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CTexvec<types>::operator>(const types t)
{
	if (this->x > t && this->y > t && this->w > t && this->h > t)
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CTexvec<types>::operator<=(const types t)
{
	if (this->x <= t && this->y <= t && this->w <= t && this->h <= t)
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CTexvec<types>::operator>=(const types t)
{
	if (this->x >= t && this->y >= t && this->w >= t && this->h >= t)
	{
		return true;
	}
	return false;
}

// Equality Compare
template<typename types>
inline bool CTexvec<types>::operator==(const types t) const
{
	if ((this->x == t) || (this->y == t) || (this->w == t) || (this->h == t))
	{
		return true;
	}
	return false;
}

// Equality Compare
template<typename types>
inline bool CTexvec<types>::operator!=(const types t) const
{
	if ((this->x != t) || (this->y != t) || (this->w != t) || (this->h != t))
	{
		return true;
	}
	return false;
}

// Unary Negation/Plus
template<typename types>
inline CTexvec<types> CTexvec<types>::operator+(void) const
{
	return *this;
}

// Unary Negation/Plus
template<typename types>
inline CTexvec<types> CTexvec<types>::operator-(void) const
{
	return *this;
}

// Postfix Increment / Decrement
template<typename types>
inline CTexvec<types> & CTexvec<types>::operator++(void)
{
	this->x++;
	this->y++;
	this->w++;
	this->h++;
	return *this;
}

// Postfix Increment / Decrement
template<typename types>
inline CTexvec<types> CTexvec<types>::operator++(int)
{
	CTexvec<types> out = *this;
	this->x++;
	this->y++;
	this->w++;
	this->h++;
	return out;
}

// Postfix Increment / Decrement
template<typename types>
inline CTexvec<types> & CTexvec<types>::operator--(void)
{
	this->x--;
	this->y--;
	this->w--;
	this->h--;
	return *this;
}

// Postfix Increment / Decrement
template<typename types>
inline CTexvec<types> CTexvec<types>::operator--(int)
{
	CTexvec<types> out = *this;
	this->x--;
	this->y--;
	this->w--;
	this->h--;
	return out;
}

template<typename types>
inline CTexvec<types> CTexvec<types>::operator+(const CTexvec & v)
{
	CTexvec<types> out;
	out.x = this->x + v.x;
	out.y = this->y + v.y;
	out.w = this->w + v.w;
	out.h = this->h + v.h;
	return out;
}

template<typename types>
inline CTexvec<types> CTexvec<types>::operator-(const CTexvec & v)
{
	CTexvec<types> out;
	out.x = this->x - v.x;
	out.y = this->y - v.y;
	out.w = this->w - v.w;
	out.h = this->h - v.h;
	return out;
}

template<typename types>
inline CTexvec<types> CTexvec<types>::operator*(const CTexvec & v)
{
	CTexvec<types> out;
	out.x = this->x * v.x;
	out.y = this->y * v.y;
	out.w = this->w * v.w;
	out.h = this->h * v.h;
	return out;
}

template<typename types>
inline CTexvec<types> CTexvec<types>::operator/(const CTexvec & v)
{
	CTexvec<types> out;
	out.x = this->x / v.x;
	out.y = this->y / v.y;
	out.w = this->w / v.w;
	out.h = this->h / v.h;
	return out;
}

template<typename types>
inline CTexvec<types> & CTexvec<types>::operator=(const CTexvec & v)
{
	this->x = v.x;
	this->y = v.y;
	this->w = v.w;
	this->h = v.h;
	return *this;
}

template<typename types>
inline CTexvec<types> & CTexvec<types>::operator+=(const CTexvec & v)
{
	this->x += v.x;
	this->y += v.y;
	this->w += v.w;
	this->h += v.h;
	return *this;
}

template<typename types>
inline CTexvec<types> & CTexvec<types>::operator-=(const CTexvec & v)
{
	this->x -= v.x;
	this->y -= v.y;
	this->w -= v.w;
	this->h -= v.h;
	return *this;
}

template<typename types>
inline CTexvec<types> & CTexvec<types>::operator*=(const CTexvec & v)
{
	this->x *= v.x;
	this->y *= v.y;
	this->w *= v.w;
	this->h *= v.h;
	return *this;
}

template<typename types>
inline CTexvec<types> & CTexvec<types>::operator/=(const CTexvec & v)
{
	this->x /= v.x;
	this->y /= v.y;
	this->w /= v.w;
	this->h /= v.h;
	return *this;
}

template<typename types>
inline CTexvec<types> CTexvec<types>::operator+(const types t) const
{
	return CTexvec(this->x + t, this->y + t, this->w + t, this->h + t);
}

template<typename types>
inline CTexvec<types> CTexvec<types>::operator-(const types t) const
{
	return CTexvec(this->x - t, this->y - t, this->w - t, this->h - t);
}

template<typename types>
inline CTexvec<types> CTexvec<types>::operator*(const types t) const
{
	return CTexvec(this->x * t, this->y * t, this->w * t, this->h * t);
}

template<typename types>
inline CTexvec<types> CTexvec<types>::operator/(const types t) const
{
	types tInv = (types)1;
	tInv = tInv / t;
	return CTexvec(this->x * tInv, this->y * tInv, this->w * tInv, this->h * tInv);
}

template<typename types>
inline CTexvec<types> & CTexvec<types>::operator=(const types t)
{
	this->x = t;
	this->y = t;
	this->w = t;
	this->h = t;
	return *this;
}

template<typename types>
inline CTexvec<types> & CTexvec<types>::operator+=(const types t)
{
	this->x += t;
	this->y += t;
	this->w += t;
	this->h += t;
	return *this;
}

template<typename types>
inline CTexvec<types> & CTexvec<types>::operator-=(const types t)
{
	this->x -= t;
	this->y -= t;
	this->w -= t;
	this->h -= t;
	return *this;
}

template<typename types>
inline CTexvec<types> & CTexvec<types>::operator*=(const types t)
{
	this->x *= t;
	this->y *= t;
	this->w *= t;
	this->h *= t;
	return *this;
}

template<typename types>
inline CTexvec<types> & CTexvec<types>::operator/=(const types t)
{
	this->x /= t;
	this->y /= t;
	this->w /= t;
	this->h /= t;
	return *this;
}

// Array Subscript
template<typename types>
inline const CTexvec<types> & CTexvec<types>::operator[](std::size_t n) const &
{
	return this[n];
}

// Array Subscript
template<typename types>
inline CTexvec<types> & CTexvec<types>::operator[](std::size_t n) &
{
	return this[n];
}

// Array Subscript
template<typename types>
inline CTexvec<types> CTexvec<types>::operator[](std::size_t n) &&
{
	return std::move(this[n]);
}

//==========================================================================
//
// class  : CColor
// Content : CColor
// how to use : CColor<float>,CColor<int>,CColor<double>...
//
//==========================================================================

// Member Selection
template<typename types>
inline const CColor<types> * CColor<types>::operator->(void) const noexcept
{
	return this;
}

// Member Selection
template<typename types>
inline CColor<types> * CColor<types>::operator->(void) noexcept
{
	return this;
}

// Compare
template<typename types>
inline bool CColor<types>::operator<(const CColor & v)
{
	if (this->r < v.r && this->g < v.g && this->b < v.b && this->a < v.a)
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CColor<types>::operator>(const CColor & v)
{
	if (this->r > v.r && this->g > v.g && this->b > v.b && this->a > v.a)
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CColor<types>::operator<=(const CColor & v)
{
	if (this->r <= v.r && this->g <= v.g && this->b <= v.b && this->a <= v.a)
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CColor<types>::operator>=(const CColor & v)
{
	if (this->r >= v.r && this->g >= v.g && this->b >= v.b && this->a >= v.a)
	{
		return true;
	}
	return false;
}

// Equality Compare
template<typename types>
inline bool CColor<types>::operator==(const CColor & v) const
{
	if ((this->r == v.r) || (this->g == v.g) || (this->b == v.b) || (this->a == v.a))
	{
		return true;
	}
	return false;
}

// Equality Compare
template<typename types>
inline bool CColor<types>::operator!=(const CColor & v) const
{
	if ((this->r != v.r) || (this->g != v.g) || (this->b != v.b) || (this->a != v.a))
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CColor<types>::operator<(const types t)
{
	if (this->r < t && this->g < t && this->b < t && this->a < t)
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CColor<types>::operator>(const types t)
{
	if (this->r > t && this->g > t && this->b > t && this->a > t)
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CColor<types>::operator<=(const types t)
{
	if (this->r <= t && this->g <= t && this->b <= t && this->a <= t)
	{
		return true;
	}
	return false;
}

// Compare
template<typename types>
inline bool CColor<types>::operator>=(const types t)
{
	if (this->r >= t && this->g >= t && this->b >= t && this->a >= t)
	{
		return true;
	}
	return false;
}

// Equality Compare
template<typename types>
inline bool CColor<types>::operator==(const types t) const
{
	if ((this->r == t) || (this->g == t) || (this->b == t) || (this->a == t))
	{
		return true;
	}
	return false;
}

// Equality Compare
template<typename types>
inline bool CColor<types>::operator!=(const types t) const
{
	if ((this->r != t) || (this->g != t) || (this->b != t) || (this->a != t))
	{
		return true;
	}
	return false;
}

// Unary Negation/Plus
template<typename types>
inline CColor<types> CColor<types>::operator+(void) const
{
	return *this;
}

// Unary Negation/Plus
template<typename types>
inline CColor<types> CColor<types>::operator-(void) const
{
	return *this;
}

// Postfix Increment / Decrement
template<typename types>
inline CColor<types> & CColor<types>::operator++(void)
{
	this->r++;
	this->g++;
	this->b++;
	this->a++;
	return *this;
}

// Postfix Increment / Decrement
template<typename types>
inline CColor<types> CColor<types>::operator++(int)
{
	CColor<types> out = *this;
	this->r++;
	this->g++;
	this->b++;
	this->a++;
	return out;
}

// Postfix Increment / Decrement
template<typename types>
inline CColor<types> & CColor<types>::operator--(void)
{
	this->r--;
	this->g--;
	this->b--;
	this->a--;
	return *this;
}

// Postfix Increment / Decrement
template<typename types>
inline CColor<types> CColor<types>::operator--(int)
{
	CColor<types> out = *this;
	this->r--;
	this->g--;
	this->b--;
	this->a--;
	return out;
}

template<typename types>
inline CColor<types> CColor<types>::operator+(const CColor & v)
{
	CColor<types> out;
	out.r = this->r + v.r;
	out.g = this->g + v.g;
	out.b = this->b + v.b;
	out.a = this->a + v.a;
	return out;
}

template<typename types>
inline CColor<types> CColor<types>::operator-(const CColor & v)
{
	CColor<types> out;
	out.r = this->r - v.r;
	out.g = this->g - v.g;
	out.b = this->b - v.b;
	out.a = this->a - v.a;
	return out;
}

template<typename types>
inline CColor<types> CColor<types>::operator*(const CColor & v)
{
	CColor<types> out;
	out.r = this->r * v.r;
	out.g = this->g * v.g;
	out.b = this->b * v.b;
	out.a = this->a * v.a;
	return out;
}

template<typename types>
inline CColor<types> CColor<types>::operator/(const CColor & v)
{
	CColor<types> out;
	out.r = this->r / v.r;
	out.g = this->g / v.g;
	out.b = this->b / v.b;
	out.a = this->a / v.a;
	return out;
}

template<typename types>
inline CColor<types> & CColor<types>::operator=(const CColor & v)
{
	this->r = v.r;
	this->g = v.g;
	this->b = v.b;
	this->a = v.a;
	return *this;
}

template<typename types>
inline CColor<types> & CColor<types>::operator+=(const CColor & v)
{
	this->r += v.r;
	this->g += v.g;
	this->b += v.b;
	this->a += v.a;
	return *this;
}

template<typename types>
inline CColor<types> & CColor<types>::operator-=(const CColor & v)
{
	this->r -= v.r;
	this->g -= v.g;
	this->b -= v.b;
	this->a -= v.a;
	return *this;
}

template<typename types>
inline CColor<types> & CColor<types>::operator*=(const CColor & v)
{
	this->r *= v.r;
	this->g *= v.g;
	this->b *= v.b;
	this->a *= v.a;
	return *this;
}

template<typename types>
inline CColor<types> & CColor<types>::operator/=(const CColor & v)
{
	this->r /= v.r;
	this->g /= v.g;
	this->b /= v.b;
	this->a /= v.a;
	return *this;
}

template<typename types>
inline CColor<types> CColor<types>::operator+(const types t) const
{
	return CColor(this->r + t, this->g + t, this->b + t, this->a + t);
}

template<typename types>
inline CColor<types> CColor<types>::operator-(const types t) const
{
	return CColor(this->r - t, this->g - t, this->b - t, this->a - t);
}

template<typename types>
inline CColor<types> CColor<types>::operator*(const types t) const
{
	return CColor(this->r * t, this->g * t, this->b * t, this->a * t);
}

template<typename types>
inline CColor<types> CColor<types>::operator/(const types t) const
{
	types tInv = (types)1;
	tInv = tInv / t;
	return CColor(this->r * tInv, this->g * tInv, this->b * tInv, this->a * tInv);
}

template<typename types>
inline CColor<types> & CColor<types>::operator=(const types t)
{
	this->r = t;
	this->g = t;
	this->b = t;
	this->a = t;
	return *this;
}

template<typename types>
inline CColor<types> & CColor<types>::operator+=(const types t)
{
	this->r += t;
	this->g += t;
	this->b += t;
	this->a += t;
	return *this;
}

template<typename types>
inline CColor<types> & CColor<types>::operator-=(const types t)
{
	this->r -= t;
	this->g -= t;
	this->b -= t;
	this->a -= t;
	return *this;
}

template<typename types>
inline CColor<types> & CColor<types>::operator*=(const types t)
{
	this->r *= t;
	this->g *= t;
	this->b *= t;
	this->a *= t;
	return *this;
}

template<typename types>
inline CColor<types> & CColor<types>::operator/=(const types t)
{
	this->r /= t;
	this->g /= t;
	this->b /= t;
	this->a /= t;
	return *this;
}

// Array Subscript
template<typename types>
inline const CColor<types> & CColor<types>::operator[](std::size_t n) const &
{
	return this[n];
}

// Array Subscript
template<typename types>
inline CColor<types> & CColor<types>::operator[](std::size_t n) &
{
	return this[n];
}

// Array Subscript
template<typename types>
inline CColor<types> CColor<types>::operator[](std::size_t n) &&
{
	return std::move(this[n]);
}

//==========================================================================
//
// class  : CVector4
// Content : CVector4
// how to use : CVector4<float>,CVector4<int>,CVector4<double>...
//
//==========================================================================

// Member Selection
template<typename types>
inline const CVector4<types> * CVector4<types>::operator->(void) const noexcept
{
    return this;
}

// Member Selection
template<typename types>
inline CVector4<types> * CVector4<types>::operator->(void) noexcept
{
    return this;
}

// Compare
template<typename types>
inline bool CVector4<types>::operator<(const CVector4 & v)
{
    if (this->x < v.x && this->y < v.y && this->z < v.z && this->w < v.w)
    {
        return true;
    }
    return false;
}

// Compare
template<typename types>
inline bool CVector4<types>::operator>(const CVector4 & v)
{
    if (this->x > v.x && this->y > v.y && this->z > v.z && this->w > v.w)
    {
        return true;
    }
    return false;
}

// Compare
template<typename types>
inline bool CVector4<types>::operator<=(const CVector4 & v)
{
    if (this->x <= v.x && this->y <= v.y && this->z <= v.z && this->w <= v.w)
    {
        return true;
    }
    return false;
}

// Compare
template<typename types>
inline bool CVector4<types>::operator>=(const CVector4 & v)
{
    if (this->x >= v.x && this->y >= v.y && this->z >= v.z && this->w >= v.w)
    {
        return true;
    }
    return false;
}

// Equality Compare
template<typename types>
inline bool CVector4<types>::operator==(const CVector4 & v) const
{
    if ((this->x == v.x) || (this->y == v.y) || (this->z == v.z) || (this->w == v.w))
    {
        return true;
    }
    return false;
}

// Equality Compare
template<typename types>
inline bool CVector4<types>::operator!=(const CVector4 & v) const
{
    if ((this->x != v.x) || (this->y != v.y) || (this->z != v.z) || (this->w != v.w))
    {
        return true;
    }
    return false;
}

// Compare
template<typename types>
inline bool CVector4<types>::operator<(const types t)
{
    if (this->x < t && this->y < t && this->z < t && this->w < t)
    {
        return true;
    }
    return false;
}

// Compare
template<typename types>
inline bool CVector4<types>::operator>(const types t)
{
    if (this->x > t && this->y > t && this->z > t && this->w > t)
    {
        return true;
    }
    return false;
}

// Compare
template<typename types>
inline bool CVector4<types>::operator<=(const types t)
{
    if (this->x <= t && this->y <= t && this->z <= t && this->w <= t)
    {
        return true;
    }
    return false;
}

// Compare
template<typename types>
inline bool CVector4<types>::operator>=(const types t)
{
    if (this->x >= t && this->y >= t && this->z >= t && this->w >= t)
    {
        return true;
    }
    return false;
}

// Equality Compare
template<typename types>
inline bool CVector4<types>::operator==(const types t) const
{
    if ((this->x == t) || (this->y == t) || (this->z == t) || (this->w == t))
    {
        return true;
    }
    return false;
}

// Equality Compare
template<typename types>
inline bool CVector4<types>::operator!=(const types t) const
{
    if ((this->x != t) || (this->y != t) || (this->z != t) || (this->w != t))
    {
        return true;
    }
    return false;
}

// Unary Negation/Plus
template<typename types>
inline CVector4<types> CVector4<types>::operator+(void) const
{
    return *this;
}

// Unary Negation/Plus
template<typename types>
inline CVector4<types> CVector4<types>::operator-(void) const
{
    return *this;
}

// Postfix Increment / Decrement
template<typename types>
inline CVector4<types> & CVector4<types>::operator++(void)
{
    this->x++;
    this->y++;
    this->z++;
    this->w++;
    return *this;
}

// Postfix Increment / Decrement
template<typename types>
inline CVector4<types> CVector4<types>::operator++(int)
{
    CVector4<types> out = *this;
    this->x++;
    this->y++;
    this->z++;
    this->w++;
    return out;
}

// Postfix Increment / Decrement
template<typename types>
inline CVector4<types> & CVector4<types>::operator--(void)
{
    this->x--;
    this->y--;
    this->z--;
    this->w--;
    return *this;
}

// Postfix Increment / Decrement
template<typename types>
inline CVector4<types> CVector4<types>::operator--(int)
{
    CVector4<types> out = *this;
    this->x--;
    this->y--;
    this->z--;
    this->w--;
    return out;
}

template<typename types>
inline CVector4<types> CVector4<types>::operator+(const CVector4 & v)
{
    CVector4<types> out;
    out.x = this->x + v.x;
    out.y = this->y + v.y;
    out.z = this->z + v.z;
    out.w = this->w + v.w;
    return out;
}

template<typename types>
inline CVector4<types> CVector4<types>::operator-(const CVector4 & v)
{
    CVector4<types> out;
    out.x = this->x - v.x;
    out.y = this->y - v.y;
    out.z = this->z - v.z;
    out.w = this->w - v.w;
    return out;
}

template<typename types>
inline CVector4<types> CVector4<types>::operator*(const CVector4 & v)
{
    CVector4<types> out;
    out.x = this->x * v.x;
    out.y = this->y * v.y;
    out.z = this->z * v.z;
    out.w = this->w * v.w;
    return out;
}

template<typename types>
inline CVector4<types> CVector4<types>::operator/(const CVector4 & v)
{
    CVector4<types> out;
    out.x = this->x / v.x;
    out.y = this->y / v.y;
    out.z = this->z / v.z;
    out.w = this->w / v.w;
    return out;
}

template<typename types>
inline CVector4<types> & CVector4<types>::operator=(const CVector4 & v)
{
    this->x = v.x;
    this->y = v.y;
    this->z = v.z;
    this->w = v.w;
    return *this;
}

template<typename types>
inline CVector4<types> & CVector4<types>::operator+=(const CVector4 & v)
{
    this->x += v.x;
    this->y += v.y;
    this->z += v.z;
    this->w += v.w;
    return *this;
}

template<typename types>
inline CVector4<types> & CVector4<types>::operator-=(const CVector4 & v)
{
    this->x -= v.x;
    this->y -= v.y;
    this->z -= v.z;
    this->w -= v.w;
    return *this;
}

template<typename types>
inline CVector4<types> & CVector4<types>::operator*=(const CVector4 & v)
{
    this->x *= v.x;
    this->y *= v.y;
    this->z *= v.z;
    this->w *= v.w;
    return *this;
}

template<typename types>
inline CVector4<types> & CVector4<types>::operator/=(const CVector4 & v)
{
    this->x /= v.x;
    this->y /= v.y;
    this->z /= v.z;
    this->w /= v.w;
    return *this;
}

template<typename types>
inline CVector4<types> CVector4<types>::operator+(const types t) const
{
    return CVector4(this->x + t, this->y + t, this->z + t, this->w + t);
}

template<typename types>
inline CVector4<types> CVector4<types>::operator-(const types t) const
{
    return CVector4(this->x - t, this->y - t, this->z - t, this->w - t);
}

template<typename types>
inline CVector4<types> CVector4<types>::operator*(const types t) const
{
    return CVector4(this->x * t, this->y * t, this->z * t, this->w * t);
}

template<typename types>
inline CVector4<types> CVector4<types>::operator/(const types t) const
{
    types tInv = (types)1;
    tInv = tInv / t;
    return CVector4(this->x * tInv, this->y * tInv, this->z * tInv, this->w * tInv);
}

template<typename types>
inline CVector4<types> & CVector4<types>::operator=(const types t)
{
    this->x = t;
    this->y = t;
    this->z = t;
    this->w = t;
    return *this;
}

template<typename types>
inline CVector4<types> & CVector4<types>::operator+=(const types t)
{
    this->x += t;
    this->y += t;
    this->z += t;
    this->w += t;
    return *this;
}

template<typename types>
inline CVector4<types> & CVector4<types>::operator-=(const types t)
{
    this->x -= t;
    this->y -= t;
    this->z -= t;
    this->w -= t;
    return *this;
}

template<typename types>
inline CVector4<types> & CVector4<types>::operator*=(const types t)
{
    this->x *= t;
    this->y *= t;
    this->z *= t;
    this->w *= t;
    return *this;
}

template<typename types>
inline CVector4<types> & CVector4<types>::operator/=(const types t)
{
    this->x /= t;
    this->y /= t;
    this->z /= t;
    this->w /= t;
    return *this;
}

// Array Subscript
template<typename types>
inline const CVector4<types> & CVector4<types>::operator[](std::size_t n) const &
{
    return this[n];
}

// Array Subscript
template<typename types>
inline CVector4<types> & CVector4<types>::operator[](std::size_t n) &
{
    return this[n];
}

// Array Subscript
template<typename types>
inline CVector4<types> CVector4<types>::operator[](std::size_t n) &&
{
    return std::move(this[n]);
}

//==========================================================================
//
// class  : CVector2
// Content : CVector2
// how to use : CVector2<float>,CVector2<int>,CVector2<double>...
//
//==========================================================================

// Member Selection
template<typename types>
inline const CVector2<types> * CVector2<types>::operator->(void) const noexcept
{
    return this;
}

// Member Selection
template<typename types>
inline CVector2<types> * CVector2<types>::operator->(void) noexcept
{
    return this;
}

// Compare
template<typename types>
inline bool CVector2<types>::operator<(const CVector2 & v)
{
    if (this->x < v.x && this->y < v.y)
    {
        return true;
    }
    return false;
}

// Compare
template<typename types>
inline bool CVector2<types>::operator>(const CVector2 & v)
{
    if (this->x > v.x && this->y > v.y)
    {
        return true;
    }
    return false;
}

// Compare
template<typename types>
inline bool CVector2<types>::operator<=(const CVector2 & v)
{
    if (this->x <= v.x && this->y <= v.y)
    {
        return true;
    }
    return false;
}

// Compare
template<typename types>
inline bool CVector2<types>::operator>=(const CVector2 & v)
{
    if (this->x >= v.x && this->y >= v.y)
    {
        return true;
    }
    return false;
}

// Equality Compare
template<typename types>
inline bool CVector2<types>::operator==(const CVector2 & v) const
{
    if ((this->x == v.x) || (this->y == v.y))
    {
        return true;
    }
    return false;
}

// Equality Compare
template<typename types>
inline bool CVector2<types>::operator!=(const CVector2 & v) const
{
    if ((this->x != v.x) || (this->y != v.y))
    {
        return true;
    }
    return false;
}

// Compare
template<typename types>
inline bool CVector2<types>::operator<(const types t)
{
    if (this->x < t && this->y < t)
    {
        return true;
    }
    return false;
}

// Compare
template<typename types>
inline bool CVector2<types>::operator>(const types t)
{
    if (this->x > t && this->y > t)
    {
        return true;
    }
    return false;
}

// Compare
template<typename types>
inline bool CVector2<types>::operator<=(const types t)
{
    if (this->x <= t && this->y <= t)
    {
        return true;
    }
    return false;
}

// Compare
template<typename types>
inline bool CVector2<types>::operator>=(const types t)
{
    if (this->x >= t && this->y >= t)
    {
        return true;
    }
    return false;
}

// Equality Compare
template<typename types>
inline bool CVector2<types>::operator==(const types t) const
{
    if ((this->x == t) || (this->y == t))
    {
        return true;
    }
    return false;
}

// Equality Compare
template<typename types>
inline bool CVector2<types>::operator!=(const types t) const
{
    if ((this->x != t) || (this->y != t))
    {
        return true;
    }
    return false;
}

// Unary Negation/Plus
template<typename types>
inline CVector2<types> CVector2<types>::operator+(void) const
{
    return *this;
}

// Unary Negation/Plus
template<typename types>
inline CVector2<types> CVector2<types>::operator-(void) const
{
    return *this;
}

// Postfix Increment / Decrement
template<typename types>
inline CVector2<types> & CVector2<types>::operator++(void)
{
    this->x++;
    this->y++;
    return *this;
}

// Postfix Increment / Decrement
template<typename types>
inline CVector2<types> CVector2<types>::operator++(int)
{
    CVector2<types> out = *this;
    this->x++;
    this->y++;
    return out;
}

// Postfix Increment / Decrement
template<typename types>
inline CVector2<types> & CVector2<types>::operator--(void)
{
    this->x--;
    this->y--;
    return *this;
}

// Postfix Increment / Decrement
template<typename types>
inline CVector2<types> CVector2<types>::operator--(int)
{
    CVector2<types> out = *this;
    this->x--;
    this->y--;
    return out;
}

template<typename types>
inline CVector2<types> CVector2<types>::operator+(const CVector2 & v)
{
    CVector2<types> out;
    out.x = this->x + v.x;
    out.y = this->y + v.y;
    return out;
}

template<typename types>
inline CVector2<types> CVector2<types>::operator-(const CVector2 & v)
{
    CVector2<types> out;
    out.x = this->x - v.x;
    out.y = this->y - v.y;
    return out;
}

template<typename types>
inline CVector2<types> CVector2<types>::operator*(const CVector2 & v)
{
    CVector2<types> out;
    out.x = this->x * v.x;
    out.y = this->y * v.y;
    return out;
}

template<typename types>
inline CVector2<types> CVector2<types>::operator/(const CVector2 & v)
{
    CVector2<types> out;
    out.x = this->x / v.x;
    out.y = this->y / v.y;
    return out;
}

template<typename types>
inline CVector2<types> & CVector2<types>::operator=(const CVector2 & v)
{
    this->x = v.x;
    this->y = v.y;
    return *this;
}

template<typename types>
inline CVector2<types> & CVector2<types>::operator+=(const CVector2 & v)
{
    this->x += v.x;
    this->y += v.y;
    return *this;
}

template<typename types>
inline CVector2<types> & CVector2<types>::operator-=(const CVector2 & v)
{
    this->x -= v.x;
    this->y -= v.y;
    return *this;
}

template<typename types>
inline CVector2<types> & CVector2<types>::operator*=(const CVector2 & v)
{
    this->x *= v.x;
    this->y *= v.y;
    return *this;
}

template<typename types>
inline CVector2<types> & CVector2<types>::operator/=(const CVector2 & v)
{
    this->x /= v.x;
    this->y /= v.y;
    return *this;
}

template<typename types>
inline CVector2<types> CVector2<types>::operator+(const types t) const
{
    return CVector2(this->x + t, this->y + t);
}

template<typename types>
inline CVector2<types> CVector2<types>::operator-(const types t) const
{
    return CVector2(this->x - t, this->y - t);
}

template<typename types>
inline CVector2<types> CVector2<types>::operator*(const types t) const
{
    return CVector2(this->x * t, this->y * t);
}

template<typename types>
inline CVector2<types> CVector2<types>::operator/(const types t) const
{
    types tInv = (types)1;
    tInv = tInv / t;
    return CVector2(this->x * tInv, this->y * tInv);
}

template<typename types>
inline CVector2<types> & CVector2<types>::operator=(const types t)
{
    this->x = t;
    this->y = t;
    return *this;
}

template<typename types>
inline CVector2<types> & CVector2<types>::operator+=(const types t)
{
    this->x += t;
    this->y += t;
    return *this;
}

template<typename types>
inline CVector2<types> & CVector2<types>::operator-=(const types t)
{
    this->x -= t;
    this->y -= t;
    return *this;
}

template<typename types>
inline CVector2<types> & CVector2<types>::operator*=(const types t)
{
    this->x *= t;
    this->y *= t;
    return *this;
}

template<typename types>
inline CVector2<types> & CVector2<types>::operator/=(const types t)
{
    this->x /= t;
    this->y /= t;
    return *this;
}

// Array Subscript
template<typename types>
inline const CVector2<types> & CVector2<types>::operator[](std::size_t n) const &
{
    return this[n];
}

// Array Subscript
template<typename types>
inline CVector2<types> & CVector2<types>::operator[](std::size_t n) &
{
    return this[n];
}

// Array Subscript
template<typename types>
inline CVector2<types> CVector2<types>::operator[](std::size_t n) &&
{
    return std::move(this[n]);
}

//==========================================================================
//
// class  : CVector3
// Content : CVector3
// how to use : CVector3<float>,CVector3<int>,CVector3<double>...
//
//==========================================================================

// Member Selection
template<typename types>
inline const CVector3<types> * CVector3<types>::operator->(void) const noexcept
{
    return this;
}

// Member Selection
template<typename types>
inline CVector3<types> * CVector3<types>::operator->(void) noexcept
{
    return this;
}

// Compare
template<typename types>
inline bool CVector3<types>::operator<(const CVector3 & v)
{
    if (this->x < v.x && this->y < v.y && this->z < v.z)
    {
        return true;
    }
    return false;
}

// Compare
template<typename types>
inline bool CVector3<types>::operator>(const CVector3 & v)
{
    if (this->x > v.x && this->y > v.y && this->z > v.z)
    {
        return true;
    }
    return false;
}

// Compare
template<typename types>
inline bool CVector3<types>::operator<=(const CVector3 & v)
{
    if (this->x <= v.x && this->y <= v.y && this->z <= v.z)
    {
        return true;
    }
    return false;
}

// Compare
template<typename types>
inline bool CVector3<types>::operator>=(const CVector3 & v)
{
    if (this->x >= v.x && this->y >= v.y && this->z >= v.z)
    {
        return true;
    }
    return false;
}

// Equality Compare
template<typename types>
inline bool CVector3<types>::operator==(const CVector3 & v) const
{
    if ((this->x == v.x) || (this->y == v.y) || (this->z == v.z))
    {
        return true;
    }
    return false;
}

// Equality Compare
template<typename types>
inline bool CVector3<types>::operator!=(const CVector3 & v) const
{
    if ((this->x != v.x) || (this->y != v.y) || (this->z != v.z))
    {
        return true;
    }
    return false;
}

// Compare
template<typename types>
inline bool CVector3<types>::operator<(const types t)
{
    if (this->x < t && this->y < t && this->z < t)
    {
        return true;
    }
    return false;
}

// Compare
template<typename types>
inline bool CVector3<types>::operator>(const types t)
{
    if (this->x > t && this->y > t && this->z > t)
    {
        return true;
    }
    return false;
}

// Compare
template<typename types>
inline bool CVector3<types>::operator<=(const types t)
{
    if (this->x <= t && this->y <= t && this->z <= t)
    {
        return true;
    }
    return false;
}

// Compare
template<typename types>
inline bool CVector3<types>::operator>=(const types t)
{
    if (this->x >= t && this->y >= t && this->z >= t)
    {
        return true;
    }
    return false;
}

// Equality Compare
template<typename types>
inline bool CVector3<types>::operator==(const types t) const
{
    if ((this->x == t) || (this->y == t) || (this->z == t))
    {
        return true;
    }
    return false;
}

// Equality Compare
template<typename types>
inline bool CVector3<types>::operator!=(const types t) const
{
    if ((this->x != t) || (this->y != t) || (this->z != t))
    {
        return true;
    }
    return false;
}

// Unary Negation/Plus
template<typename types>
inline CVector3<types> CVector3<types>::operator+(void) const
{
    return *this;
}

// Unary Negation/Plus
template<typename types>
inline CVector3<types> CVector3<types>::operator-(void) const
{
    return *this;
}

// Postfix Increment / Decrement
template<typename types>
inline CVector3<types> & CVector3<types>::operator++(void)
{
    this->x++;
    this->y++;
    this->z++;
    return *this;
}

// Postfix Increment / Decrement
template<typename types>
inline CVector3<types> CVector3<types>::operator++(int)
{
    CVector3<types> out = *this;
    this->x++;
    this->y++;
    this->z++;
    return out;
}

// Postfix Increment / Decrement
template<typename types>
inline CVector3<types> & CVector3<types>::operator--(void)
{
    this->x--;
    this->y--;
    this->z--;
    return *this;
}

// Postfix Increment / Decrement
template<typename types>
inline CVector3<types> CVector3<types>::operator--(int)
{
    CVector3<types> out = *this;
    this->x--;
    this->y--;
    this->z--;
    return out;
}

template<typename types>
inline CVector3<types> CVector3<types>::operator+(const CVector3 & v)
{
    CVector3<types> out;
    out.x = this->x + v.x;
    out.y = this->y + v.y;
    out.z = this->z + v.z;
    return out;
}

template<typename types>
inline CVector3<types> CVector3<types>::operator-(const CVector3 & v)
{
    CVector3<types> out;
    out.x = this->x - v.x;
    out.y = this->y - v.y;
    out.z = this->z - v.z;
    return out;
}

template<typename types>
inline CVector3<types> CVector3<types>::operator*(const CVector3 & v)
{
    CVector3<types> out;
    out.x = this->x * v.x;
    out.y = this->y * v.y;
    out.z = this->z * v.z;
    return out;
}

template<typename types>
inline CVector3<types> CVector3<types>::operator/(const CVector3 & v)
{
    CVector3<types> out;
    out.x = this->x / v.x;
    out.y = this->y / v.y;
    out.z = this->z / v.z;
    return out;
}

template<typename types>
inline CVector3<types> & CVector3<types>::operator=(const CVector3 & v)
{
    this->x = v.x;
    this->y = v.y;
    this->z = v.z;
    return *this;
}

template<typename types>
inline CVector3<types> & CVector3<types>::operator+=(const CVector3 & v)
{
    this->x += v.x;
    this->y += v.y;
    this->z += v.z;
    return *this;
}

template<typename types>
inline CVector3<types> & CVector3<types>::operator-=(const CVector3 & v)
{
    this->x -= v.x;
    this->y -= v.y;
    this->z -= v.z;
    return *this;
}

template<typename types>
inline CVector3<types> & CVector3<types>::operator*=(const CVector3 & v)
{
    this->x *= v.x;
    this->y *= v.y;
    this->z *= v.z;
    return *this;
}

template<typename types>
inline CVector3<types> & CVector3<types>::operator/=(const CVector3 & v)
{
    this->x /= v.x;
    this->y /= v.y;
    this->z /= v.z;
    return *this;
}

template<typename types>
inline CVector3<types> CVector3<types>::operator+(const types t) const
{
    return CVector3(this->x + t, this->y + t, this->z + t);
}

template<typename types>
inline CVector3<types> CVector3<types>::operator-(const types t) const
{
    return CVector3(this->x - t, this->y - t, this->z - t);
}

template<typename types>
inline CVector3<types> CVector3<types>::operator*(const types t) const
{
    return CVector3(this->x * t, this->y * t, this->z * t);
}

template<typename types>
inline CVector3<types> CVector3<types>::operator/(const types t) const
{
    types tInv = (types)1;
    tInv = tInv / t;
    return CVector3(this->x * tInv, this->y * tInv, this->z * tInv);
}

template<typename types>
inline CVector3<types> & CVector3<types>::operator=(const types t)
{
    this->x = t;
    this->y = t;
    this->z = t;
    return *this;
}

template<typename types>
inline CVector3<types> & CVector3<types>::operator+=(const types t)
{
    this->x += t;
    this->y += t;
    this->z += t;
    return *this;
}

template<typename types>
inline CVector3<types> & CVector3<types>::operator-=(const types t)
{
    this->x -= t;
    this->y -= t;
    this->z -= t;
    return *this;
}

template<typename types>
inline CVector3<types> & CVector3<types>::operator*=(const types t)
{
    this->x *= t;
    this->y *= t;
    this->z *= t;
    return *this;
}

template<typename types>
inline CVector3<types> & CVector3<types>::operator/=(const types t)
{
    this->x /= t;
    this->y /= t;
    this->z /= t;
    return *this;
}

// Array Subscript
template<typename types>
inline const CVector3<types> & CVector3<types>::operator[](std::size_t n) const &
{
    return this[n];
}

// Array Subscript
template<typename types>
inline CVector3<types> & CVector3<types>::operator[](std::size_t n) &
{
    return this[n];
}

// Array Subscript
template<typename types>
inline CVector3<types> CVector3<types>::operator[](std::size_t n) &&
{
    return std::move(this[n]);
}

//==========================================================================
//
// class  : CMaterial
// Content : CMaterial
// how to use : CMaterial<float>,CMaterial<int>,CMaterial<double>...
//
//==========================================================================

// Member Selection
template<typename types>
inline const CMaterial<types> * CMaterial<types>::operator->(void) const noexcept
{
    return this;
}

// Member Selection
template<typename types>
inline CMaterial<types> * CMaterial<types>::operator->(void) noexcept
{
    return this;
}

template<typename types>
inline CMaterial<types> & CMaterial<types>::operator=(const CMaterial & v)
{
    this->Ambient = v.Ambient;
    this->Diffuse = v.Diffuse;
    this->Specular = v.Specular;
    this->Emission = v.Emission;
    this->Shininess = v.Shininess;
    return *this;
}

// Array Subscript
template<typename types>
inline const CMaterial<types> & CMaterial<types>::operator[](std::size_t n) const &
{
    return this[n];
}

// Array Subscript
template<typename types>
inline CMaterial<types> & CMaterial<types>::operator[](std::size_t n) &
{
    return this[n];
}

// Array Subscript
template<typename types>
inline CMaterial<types> CMaterial<types>::operator[](std::size_t n) &&
{
    return std::move(this[n]);
}

//==========================================================================
//
// class  : CVertex_3D
// Content : CVertex_3D
// how to use : CVertex_3D<float>,CVertex_3D<int>,CVertex_3D<double>...
//
//==========================================================================

// Member Selection
template<typename types>
inline const CVertex_3D<types> * CVertex_3D<types>::operator->(void) const noexcept
{
    return this;
}

// Member Selection
template<typename types>
inline CVertex_3D<types> * CVertex_3D<types>::operator->(void) noexcept
{
    return this;
}

template<typename types>
inline CVertex_3D<types> & CVertex_3D<types>::operator=(const CVertex_3D & v)
{
    this->TexturePos = v.TexturePos;
    this->Normal = v.Normal;
    this->Position = v.Position;
    this->Diffuse = v.Diffuse;
    return *this;
}

// Array Subscript
template<typename types>
inline const CVertex_3D<types> & CVertex_3D<types>::operator[](std::size_t n) const &
{
    return this[n];
}

// Array Subscript
template<typename types>
inline CVertex_3D<types> & CVertex_3D<types>::operator[](std::size_t n) &
{
    return this[n];
}

// Array Subscript
template<typename types>
inline CVertex_3D<types> CVertex_3D<types>::operator[](std::size_t n) &&
{
    return std::move(this[n]);
}

#endif // !_DataType_H_
