//==========================================================================
// サウンド[XAudio2.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _XAudio2_H_
#define _XAudio2_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <stdio.h>
#include <iomanip>
#include <string>
#include <XAudio2.h>
#include "ObjectVectorManager.h"
#include "Template.h"

//==========================================================================
//
// class  : CSoundDevice
// Content: サウンドのデバイス
//
//==========================================================================
class CXAudio2Device
{
private:
    // コピー禁止 (C++11)
    CXAudio2Device(const CXAudio2Device &) = delete;
    CXAudio2Device &operator=(const CXAudio2Device &) = delete;
public:
	CXAudio2Device() {}
	~CXAudio2Device() {
        this->Release();
    }

	// 初期化
	 HRESULT Init(HWND hWnd);

	// 解放
	 void Release(void);

	// ゲッター
     static HWND GetWnd(void) { return m_hWnd; }

	// ゲッター
     static IXAudio2 *GetAudio2(void) { return m_pXAudio2; }
private:
	// ボイスデータの破棄
	 void DestroyVoice(void);
private:
    IXAudio2MasteringVoice *m_pMasteringVoice; // マスターボイス
	static IXAudio2 *m_pXAudio2; // XAudio2オブジェクトへのインターフェイス
    static HWND m_hWnd; // ウィンドハンドル
};

//==========================================================================
//
// class  : CSound
// Content: サウンド
//
//==========================================================================
class CXAudio2
{
private:
    // コピー禁止 (C++11)
    CXAudio2(const CXAudio2 &) = delete;
    CXAudio2 &operator=(const CXAudio2 &) = delete;
private:
	//==========================================================================
	//
	// class  : CParam
	// Content: データ
	//
	//==========================================================================
	class CParam
	{
    private:
        // コピー禁止 (C++11)
        CParam(const CParam &) = delete;
        CParam &operator=(const CParam &) = delete;
	public:
		CParam()
		{
			this->m_pSourceVoice = nullptr;
			this->m_pDataAudio = nullptr;
			this->m_SizeAudio = (DWORD)0;
			this->m_CntLoop = 0;
			this->m_Volume = 1.0f;
        }
        ~CParam() {
            this->Release();
        }
    private:
        // ボイスデータの破棄
        void Release(void);
	public:
		IXAudio2SourceVoice *m_pSourceVoice; // サウンド
		BYTE *m_pDataAudio; // オーディオデーター
		DWORD m_SizeAudio; // オーディオデータサイズ
		int m_CntLoop; // ループカウント
		float m_Volume; // ボリューム
    private:
        CTemplates m_temp;
	};
	//==========================================================================
	//
	// class  : CLabel
	// Content: サウンドのリンク
	//
	//==========================================================================
	class CLabel
	{
	public:
        CLabel()
        {
            this->m_strName = "";
            this->m_CntLoop = 0;
            this->m_Volume = 1.0f;
        }
        CLabel(const std::string & name, int loop, float vol)
        {
            this->m_strName = name;
            this->m_CntLoop = loop;
            this->m_Volume = vol;
        }
		~CLabel() { this->m_strName.clear(); }
	public:
		std::string m_strName; // ファイル名
		int m_CntLoop; // ループカウント -1=ループ 0 一回
		float m_Volume; // ボリューム 最大=1.0f 無音=0.0f
	};
public:
    using SoundLabel = CLabel;
public:
    CXAudio2() {}
    ~CXAudio2() {
        this->Release();
    }

    // 初期化
    // OutLabel ラベル
    HRESULT Init(const SoundLabel & Input, int & OutLabel);

    // 初期化
    HRESULT Init(const SoundLabel & Input) {
        int OutLabel = 0;
        return this->Init(Input, OutLabel);
    }

	// 解放
	void Release(void);

    // 特定のサウンドの破棄
    void PinpointRelease(int label);

	// 再生
	HRESULT Play(int label);

    // 特定のサウンド停止
    void Stop(int label) {
        this->Stop(this->m_Sound.Get(label));
    }

	// 格納してある全てのサウンド停止
	void Stop(void);

    // 特定のサウンドのボリューム設定
    // volume 最大=1.0f
    // volume 無音=0.0f
    void SetVolume(int label, float volume);

	// 全てのサウンドのボリューム設定
	// volume 最大=1.0f
	// volume 無音=0.0f
	void SetVolume(float volume);

	// 全てのサウンドのボリューム自動設定
	void SetVolume(void);

    // 特定のサウンドのボリューム設定
    // volume 最大=1.0f
    // volume 無音=0.0f
    void SetVolume(CParam * pSound, float volume);

    // 特定のサウンドのボリュームの取得
    const float GetVolume(int label);
private:
    // 特定のサウンド停止
    void Stop(CParam * pSound);

	// チャンクのチェック
	HRESULT CheckChunk(HANDLE hFile, DWORD format, DWORD *pChunkSize, DWORD *pChunkDataPosition);

	// チャンクデータの読み込み
	HRESULT ReadChunkData(HANDLE hFile, void *pBuffer, DWORD dwBuffersize, DWORD dwBufferoffset);
private:
    CObjectVectorManager<CParam> m_Sound; // サウンドケース
    CTemplates m_temp;
};

#endif // !_XAudio2_H_
