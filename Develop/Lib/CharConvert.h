//==========================================================================
// char convert
// author: tatsuya ogawa
//==========================================================================
#ifndef _charconvert_h_
#define _charconvert_h_

//==========================================================================
// include
//==========================================================================
#include <codecvt> // c++ 文字コード変換
#include <cstdlib> // c++ 数値変換
#include <iomanip> // c++ 時間
#include <locale> // c++ 文字の判定
#include <string> // c++ char
#include <system_error> // c++ OSエラー
#include <vector> // c++ 動的構造体
#include <Windows.h>

//==========================================================================
//
// class  : CCharConvert
// Content: 文字コード変換
//
//==========================================================================
class CCharConvert
{
public:
    CCharConvert() {}
    ~CCharConvert() {}

    // C
    // string -> wstring
    std::wstring multi_to_wide_capi(std::string const& src);

    // Windows API
    // string -> wstring
    std::wstring multi_to_wide_winapi(std::string const& src);

    // C/C++
    // UTF-8 -> Shift_JIS
    std::string wide_to_multi_capi(std::wstring const& src);

    // Windows API
    // UTF-8 -> Shift_JIS
    std::string wide_to_multi_winapi(std::wstring const& src);

    // C++
    // wstring -> string
    std::string wide_to_utf8_cppapi(std::wstring const& src);

    // Windows API
    // wstring -> string
    std::string wide_to_utf8_winapi(std::wstring const& src);

    // C++
    // string -> wstring
    std::wstring utf8_to_wide_cppapi(std::string const& src);

    // Windows API
    // string -> wstring
    std::wstring utf8_to_wide_winapi(std::string const& src);

    // C/C++
    // Shift_JIS -> UTF-8
    std::string multi_to_utf8_cppapi(std::string const& src);

    // Windows API
    // Shift_JIS -> UTF-8
    std::string multi_to_utf8_winapi(std::string const& src);

    // C/C++
    // UTF-8 -> Shift_JIS
    std::string utf8_to_multi_cppapi(std::string const& src);

    // Windows API
    // UTF-8 -> Shift_JIS
    std::string utf8_to_multi_winapi(std::string const& src);
private:
};

#endif // !_charconvert_h_
