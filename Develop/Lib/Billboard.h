//==========================================================================
// ビルボード[Billboard.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _Billboard_H_
#define _Billboard_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <string>
#include "Vertex3D.h"
#include "Rectangle.h"
#include "SetRender.h"
#include "3DObject.h"
#include "DataType.h"
#include "TextureLoader.h"
#include "Create.h"
#include "DeviceManager.h"
#include "ObjectInput.h"
#include "Template.h"
#include "ObjectVectorManager.h"

//==========================================================================
//
// class  : CBillboard
// Content: ビルボード
//
//==========================================================================
class CBillboard : private VERTEX_3D, private CRectangle, private CSetRender, private CCreate, public CObjectInput
{
private:
    // コピー禁止 (C++11)
    CBillboard(const CBillboard &) = delete;
    CBillboard &operator=(const CBillboard &) = delete;
public:
	//==========================================================================
	//
	// class  : PlateList
	// Content: 板ポリの種類
	//
	//==========================================================================
	enum class PlateList
	{
		Up, // 上向き
		Vertical //　縦
	};
private:
	//==========================================================================
	//
	// class  : CAnimation
	// Content: テクスチャ情報
	//
	//==========================================================================
	class CAnimation
	{
	public:
		CAnimation() 
		{
			this->Frame = 0;
			this->Pattern = 0;
			this->Direction = 0;
			this->m_Cut = 0;
			this->m_TexSize = 0;
		};
		CAnimation(int frame, int pattern, int direction, CTexvec<float> cut, CTexvec<int> texsize)
		{
			this->Frame = frame;
			this->Pattern = pattern;
			this->Direction = direction;
			this->m_Cut = cut;
			this->m_TexSize = texsize;
		}
		~CAnimation() {}
	public:
		int Frame; // 更新タイミング
		int Pattern; // アニメーションのパターン数
		int Direction; // 一行のアニメーション数
		CTexvec<float> m_Cut; // 切り取り情報
		CTexvec<int> m_TexSize; // テクスチャのサイズ
	};

	//==========================================================================
	//
	// class  : CBuffer
	// Content: バッファ
	//
	//==========================================================================
	class CBuffer
	{
	public:
		CBuffer()
		{
			this->m_plate = (PlateList)-1;
			this->m_pIndexBuffer = nullptr;
			this->m_pVertexBuffer = nullptr;
			this->m_pPseudo = nullptr;
            this->m_origin = false;
		}
		~CBuffer() {
            this->Release();
        }
		// 解放
		void Release(void);
	public:
		LPDIRECT3DVERTEXBUFFER9 m_pVertexBuffer; // バッファ
		LPDIRECT3DINDEXBUFFER9 m_pIndexBuffer; // インデックスバッファ
		PlateList m_plate; // 板ポリの種類
		VERTEX_4* m_pPseudo;// 頂点バッファのロック
        bool m_origin; // オリジナルかどうかの判定
    private:
        CTemplates m_temp;
	};

	//==========================================================================
	//
	// class  : CAnimationParam
	// Content: アニメーション情報
	//
	//==========================================================================
	class CAnimationParam
	{
	public:
		CAnimationParam()
		{
			this->m_pAnimation = nullptr;
			this->m_uv = CUv<float>(0.0f, 0.0f, 1.0f, 1.0f);
		}
		~CAnimationParam() {
            this->Release();
        }
        // 解放
        void Release(void);
	public:
		CAnimation *m_pAnimation; // アニメーション情報
		CBuffer m_buffer; // バッファ
		CUv<float> m_uv; // UV格納
    private:
        CTemplates m_temp;
	};
public:
	//==========================================================================
	//
	// class  : CAnimationDataCase
	// Content: 複数アニメーション情報ボックス
	//
	//==========================================================================
	class CAnimationDataCase
	{
	public:
		CAnimationDataCase()
		{
			this->m_strName[0] = this->m_strName[1] = this->m_strName[2] = this->m_strName[3] = "";
			this->m_numdata = 0;
			this->m_frame = 0;
			this->m_pattern = 0;
			this->m_direction = 0;
			this->m_plate = PlateList::Up;
			this->m_centermood = false;
		}
		~CAnimationDataCase()
		{
			this->m_strName[0].clear();
			this->m_strName[1].clear();
			this->m_strName[2].clear();
			this->m_strName[3].clear();
		}
	public:
		std::string m_strName[4]; // 四方向テクスチャ格納
		int m_numdata; // データ数
		int m_frame; // 更新フレーム
		int m_pattern; // アニメーション数
		int m_direction; // 横一列のアニメーション数
		PlateList m_plate; // 板ポリの種類
		bool m_centermood; // 中心モード
	};
	//==========================================================================
	//
	// class  : CAnimationData
	// Content: 複数アニメーション登録用
	//
	//==========================================================================
	class CAnimationData {
    public:
        CAnimationDataCase m_list;
    };
public:
    CBillboard() {
        this->m_Device = nullptr;
    }
    ~CBillboard() {
        this->Release();
    }

	// 初期化 失敗時true
	// Input = 使うテクスチャのパス
	// plate = 板ポリの種類
	// bCenter = 中心座標を下の真ん中にします
	bool Init(const char* Input, PlateList plate, bool bCenter = true);

	// アニメーション用初期化 失敗時true
	// Input = 使うテクスチャのパス
	// UpdateFrame = 更新フレーム
	// Pattern = アニメーション数
	// Direction = 横一列のアニメーション数
	// plate = 板ポリの種類
	// bCenter = 中心座標を下の真ん中にします
	bool Init(const char* Input, int UpdateFrame, int Pattern, int Direction, PlateList plate, bool bCenter = true);

	// 初期化 失敗時true
	// pTex = 使うテクスチャのパス ダブルポインタ用
	// numdata = 要素数
	// plate = 板ポリの種類
	// bCenter = 中心座標を下の真ん中にします
	bool Init(const char** Input, int numdata, PlateList plate, bool bCenter = true);

	// アニメーション用初期化 失敗時true
	// Input = 使うテクスチャのパス ダブルポインタ用
	// numdata = 要素数
	// UpdateFrame = 更新フレーム
	// Pattern = アニメーション数
	// Direction = 横一列のアニメーション数
	// plate = 板ポリの種類
	// bCenter = 中心座標を下の真ん中にします
	bool Init(const char** Input, int numdata, int UpdateFrame, int Pattern, int Direction, PlateList plate, bool bCenter = true);

    // 更新
    void Update(D3DXMATRIX * MtxView, int * AnimationCount = nullptr);

	// 解放
	void Release(void);

	// アニメーション更新
	// AnimationCount = アニメーション用変数を入れるところ
	void UpdateAnimation(int & AnimationCount);

    // 描画
    // pInput = C3DObject
    void Draw(void) { this->BaseDraw(false); }

    // 描画
    // bADD = 加算合成するかの判定
    void Draw(bool bADD) { this->BaseDraw(bADD); }

	// アニメーション切り替わり時の判定 切り替え時true 
	// index = 判定路調べたい板ポリの番号
	// AnimationCount = アニメーション用変数を入れるところ
	bool GetPattanNum(int index, int * AnimationCount);

	// アニメーション用のカウンタの初期化 引数はカウンタ用変数
	// AnimationCount = アニメーション用変数を入れるところ
	int *AnimationCountInit(int * AnimationCount);

	// デバイスのセット
	void SetDevice(const LPDIRECT3DDEVICE9 Input) { this->m_Device = Input; }
private:
    // ベースとなる描画
    void BaseDraw(bool bADD);

	// 頂点バッファの生成
	void CreateVertex(CAnimationParam *ptexture, bool bCenter);

	// アニメーション情報のセット
	void AnimationPalam(CAnimationParam *ptexture, int * AnimationCount);

	// UVの生成
	void UV(CAnimationParam *ptexture);

	// 重複検索
	bool OverlapSearch(PlateList plate);

	// バッファの生成
	bool CreateBuffer(PlateList plate, bool bCenter);

	// アニメーション情報の生成
	void CreateAnimation(int UpdateFrame, int Pattern, int Direction);

    // 頂点の更新
    void VertexUpdate(CAnimationParam *ptexture, C3DObject * pInput);

    // ビルボードの種類
    void BillboardType(CAnimationParam *ptexture, C3DObject * pInput, D3DXMATRIX & MtxWorld, D3DXMATRIX * MtxView);
private:
    CObjectVectorManager<CAnimationParam> m_buffer_origin; // オリジナルバッファ
    CObjectVectorManager<CAnimationParam> m_buffer_path; // アクセス用バッファ
	CTextureLoader m_tex; // テクスチャの格納
	LPDIRECT3DDEVICE9 m_Device; // デバイス
private:
    CTemplates m_temp;
};

#endif // !_Billboard_H_
