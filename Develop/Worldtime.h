//==========================================================================
// 世界時間[Worldtime.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _Worldtime_h_
#define _Worldtime_h_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "GameManager.h"

//==========================================================================
//
// class  : CWorldTime
// Content: 世界時間
//
//==========================================================================
class CWorldTime : public CObject
{
public:
	CWorldTime() :CObject(CObject::ID::Default) {
        this->SetType(CObject::Type::Game_WorldTime);
        this->m_start_script = nullptr;
    }
	~CWorldTime() {}
	// 初期化
	bool Init(void);

	// 解放
	void Uninit(void);

	// 更新
	void Update(void);

	// 描画
	void Draw(void);

    // ターンのゲッター
    CGameManager::TURNS_t getturns(void) { return m_manage.getturns(); }

    // 1ターンにかかるターン
    int Geturntime(void) { return m_manage.turntime(); }

    // 時間のゲッター
    CTimer * getturns2(void) { return m_manage.getime(); }
private:
    CGameManager m_manage; // ゲームマネージャー
    void * m_start_script; // スタート制御
};
#endif // !_Worldtime_h_
