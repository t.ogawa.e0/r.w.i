//==========================================================================
// テストプログラム[test.h]
// author: tatsuya ogawa
//==========================================================================

#ifndef _test_h_
#define _test_h_

/*
●宣言が許可されているライブラリの機能
CVector2<型名>
CVector3<型名>
CVector4<型名>
CObjectVectorManager<型名>

c/c++/DirectXの変数などは自由です。

●禁止されていること
new/delete/malloc/free/の使用

●許可されている機能の説明
・CObjectVectorManager<型名>
インスタンスを生成したい時に、安全なインスタンスの管理を提供する機能です。
インスタンスの生成/管理/破棄の全てをやってくれるテンプレートクラスです。
内部の機能の説明は、変数名にピリオドを打てば関数一覧がでて、説明を読むことができます。
※類似した機能(CObjectListManager)が存在しますが、生成したインスタンスをアクセス領域にセットしないといけないため非推奨です。

●CObjectを継承した際に使用可能な機能の説明
※機能の呼び出しには this-> を使用してください

●オブジェクトの座標機能の説明
・this->_2DObject()/this->_3DObject()
2D/3Dオブジェクトの座標を決める機能です。
->Create()でインスタンスを生成し使用してください。
※Create()を呼び出した回数だけインスタンスが生成され中で自動的に管理されます。

●描画処理の説明
これは全ての描画機能の共通のルールです。

・オブジェクトの登録
this->描画機能()->ObjectInput()で描画したい対象(_2DObject()/_3DObject())のインスタンスを登録してください。

・特定オブジェクトの描画の終了
this->描画機能()->ObjectDelete()で描画を止めたい対象(_2DObject()/_3DObject())のインスタンスを指定してください。

・描画オブジェクト情報の破棄
this->描画機能()->ObjectRelease()で全ての描画を停止させることができます。

・描画に必要なパラメータの生成
this->描画機能()->Update()で登録済みオブジェクトから描画に必要なデータの生成を行い記録します。

・描画
this->描画機能()->Draw()で登録済みオブジェクトの一括描画を行います。

●各描画機能の説明
・this->_2DPolygon()
2Dオブジェクトの描画に必要な機能です。
this->_2DPolygon()->Init()に描画するテクスチャのパスを入れてください。
描画するにはオブジェクトの登録が必要です。

・this->_2DNumber()
2Dの数字描画に必要な機能です。
呼び出し順番を守ってください。
1:this->_2DNumber()->Init()に描画するテクスチャのパスを入れてください。
2:this->_2DNumber()->Set()数字をどのように描画するかの指定を行ってください。
3:this->_2DNumber()->SetAnim()数字のアニメーション情報を入れてください。
特殊オブジェクトのため、オブジェクトの登録は不要です。

・this->_3DBillboard()
ビルボードの描画に必要な機能です。
this->_3DBillboard()->Init()に描画するテクスチャのパスを入れてください。
描画するにはオブジェクトの登録が必要です。

・this->_3DCube()
キューブの描画に必要な機能です。
this->_3DCube()->Init()に描画するテクスチャのパスを入れてください。
描画するにはオブジェクトの登録が必要です。

・this->_3DMesh()
メッシュの描画に必要な機能です。
this->_3DMesh()->Init()に描画するテクスチャのパスを入れてください。
描画するにはオブジェクトの登録が必要です。

・this->_3DA_CircleShadow()
丸影の描画に必要な機能です。
this->_3DA_CircleShadow()->Init()に描画するテクスチャのパスを入れてください。
描画するにはオブジェクトの登録が必要です。

・this->_3DXmodel()
Xモデルの描画に必要な機能です。
this->_3DXmodel()->Init()に描画するテクスチャのパスを入れてください。
描画するにはオブジェクトの登録が必要です。

・this->_3DSphere()
球体の描画に必要な機能です。
this->_3DSphere()->Init()に描画するテクスチャのパスを入れてください。
描画するにはオブジェクトの登録が必要です。

・this->_3DGrid()
グリッド描画に必要な機能です。
描画するにはオブジェクトの登録が必要です。
特殊オブジェクトのため、オブジェクトの登録は不要です。

・this->_2DText()
テキストの描画に必要な機能です。
特殊オブジェクトのため、オブジェクトの登録は不要です。

●その他の機能説明
・this->Helper()
ヘルパー関数が入っています。

・this->ImGui()
ImGuiの機能が入っています。

・this->XInput()
コントローラーでの操作に必要な機能が入っています。

・this->XAudio()
音楽再生の機能が入っています。

・this->Camera()
カメラの機能が入っています。

2018/05/24
*/

//==========================================================================
// include
//==========================================================================
#include "dxlib.h" // 全ライブラリのincludeヘッダです。これだけincludeすれば全て使えます。
#include "SceneChange.h" // シーン遷移。シーン遷移にシーンを登録する際に使用して下さい。

//==========================================================================
//
// class  : CXInput
// Content: CXInputの使い方
//
//==========================================================================
class C3DXInput : public CObject
{
public:
    C3DXInput() :CObject(CObject::ID::Default) {}
    ~C3DXInput() {}
    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;
private:
};

//==========================================================================
//
// class  : C3DCamera
// Content: カメラの設定
//
//==========================================================================
// CObject クラスにこのクラスを認識させるための継承
class C3DCamera : public CObject
{
public:
    C3DCamera() :CObject(CObject::ID::Camera) {
        this->SetType(CObject::Type::Game_Camera);
    }
    ~C3DCamera() {}
    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;
private:
};

//==========================================================================
//
// class  : C3DText
// Content: テキストの描画
//
//==========================================================================
// CObject クラスにこのクラスを認識させるための継承
class C3DText : public CObject
{
public:
    C3DText() :CObject(CObject::ID::Text) {}
    ~C3DText() {}

    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;
private:
};

//==========================================================================
//
// class  : C3DXmodel
// Content: Xモデルの描画
//
//==========================================================================
// CObject クラスにこのクラスを認識させるための継承
class C3DXmodel : public CObject
{
public:
    C3DXmodel() :CObject(CObject::ID::Xmodel) {}
    ~C3DXmodel() {}
    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;
private:
};

//==========================================================================
//
// class  : C3DSphere
// Content: 球体の描画
//
//==========================================================================
// CObject クラスにこのクラスを認識させるための継承
class C3DSphere : public CObject
{
public:
    C3DSphere() :CObject(CObject::ID::Sphere) {}
    ~C3DSphere() {}
    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;
private:
};

//==========================================================================
//
// class  : C3DMesh
// Content: メッシュの描画
//
//==========================================================================
// CObject クラスにこのクラスを認識させるための継承
class C3DMesh : public CObject
{
public:
    C3DMesh() :CObject(CObject::ID::Mesh) {}
    ~C3DMesh() {}
    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;
private:
};

//==========================================================================
//
// class  : C3DBillboard
// Content: ビルボードの描画
//
//==========================================================================
// CObject クラスにこのクラスを認識させるための継承
class C3DBillboard : public CObject
{
public:
    C3DBillboard() :CObject(CObject::ID::Billboard) {}
    ~C3DBillboard() {}
    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;
private:
    int m_animcount; // アニメーションのカウンタ
};

//==========================================================================
//
// class  : C3DCubeData
// Content: キューブの描画
//
//==========================================================================
// CObject クラスにこのクラスを認識させるための継承
class C3DCubeData : public CObject
{
public:
    C3DCubeData() :CObject(CObject::ID::Cube) {}
    ~C3DCubeData() {}
    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;
private:
};

//==========================================================================
//
// class  : C3DGridData
// Content: グリッド描画
//
//==========================================================================
// CObject クラスにこのクラスを認識させるための継承
class C3DGridData : public CObject
{
public:
    C3DGridData() :CObject(CObject::ID::Grid) {}
    ~C3DGridData() {}
    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;
private:
};

//==========================================================================
//
// class  : C2DAnimData
// Content: 2Dアニメーション描画
//
//==========================================================================
// CObject クラスにこのクラスを認識させるための継承
class C2DAnimData : public CObject
{
public:
    C2DAnimData() :CObject(CObject::ID::Polygon2D) {}
    ~C2DAnimData() {}
    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;
private:
    int m_animcount; // アニメーションカウンタ
};

//==========================================================================
//
// class  : C2DData
// Content: 2D描画
//
//==========================================================================
// CObject クラスにこのクラスを認識させるための継承
class C2DData : public CObject
{
public:
    C2DData() :CObject(CObject::ID::Polygon2D) {}
    ~C2DData() {}
    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;
private:
};

//==========================================================================
//
// class  : CTest
// Content: テスト用
//
//==========================================================================
// CBaseScene CSceneManagerに認識させるための継承
class CTest : public CBaseScene
{
public:
    CTest();
    ~CTest();
    bool Init(void)override;
    void Uninit(void)override;
    void Update(void)override;
    void Draw(void)override;
};

#endif // !_test_h_
