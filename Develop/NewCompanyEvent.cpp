//==========================================================================
// ゲームイベント:新企業[NewCompanyEvent.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "NewCompanyEvent.h"

//==========================================================================
// 初期化
void CNewCompany::Init(std::mt19937 & InputMT)
{
    CSubsidiaryManager * pchar = nullptr;
    CSubsidiaryParam * ptarget = nullptr;

    pchar = CSubsidiary::Get(CharList::Player);

    // 範囲の一様乱数
    std::uniform_int_distribution<int> SetRand(0, pchar->GetNumSub() - 1); 

    // 乱数生成
    int subID = SetRand(InputMT);

    // 対象の地域の記録
    ptarget = pchar->GetSub(subID);
    this->m_target.push_back(ptarget);

    // エネミーのデータを取り出す
    pchar = CSubsidiary::Get(CharList::Enemy);

    // 対象の地域の記録
    ptarget = pchar->GetSub(subID);
    this->m_target.push_back(ptarget);

    // 対象エリアの名前を取り出す
    this->m_targetName = ptarget->m_strName;
}

//==========================================================================
// 解放
void CNewCompany::Uninit(void)
{
    this->m_target.clear();
    this->m_Name.clear();
}

//==========================================================================
// 更新
void CNewCompany::Update(void)
{
    // イテレータで最初から最後まで検索
    for (auto itr = this->m_target.begin(); itr != this->m_target.end(); ++itr)
    {
        // シェア率を永遠に下げる
        (*itr)->m_stock_price *= 0.7f;
        (*itr)->m_profit = (int)(((*itr)->m_income*(*itr)->m_stock_price) + 0.5f);
    }
}

//==========================================================================
// 描画
void CNewCompany::Draw(void) 
{
}

//==========================================================================
// UI更新
void CNewCompany::UpdateUI(void) 
{
    // 親オブジェクトへのアクセス
    auto * p_obj = (CMobeEventObject*)this->m_master_obj;

    // オブジェクトの座標設定
    this->m_ObjectPos->SetPos(*p_obj->Get()->GetPos());
    this->m_NameObject->SetPos(*p_obj->Get()->GetPos());
}

//==========================================================================
// テキスト
void CNewCompany::text(std::string strEventName)
{
    std::string strEvent = strEventName + this->m_Name;
    if (this->m_Imgui.NewMenu(strEvent.c_str(), true))
    {
        this->m_Imgui.Text("発動中");
        this->m_Imgui.Text("%sエリアに新企業ができました", this->m_targetName.c_str());
        this->m_Imgui.Text("%sエリアの利益が今後70％になります", this->m_targetName.c_str());
        this->m_Imgui.EndMenu();
    }
}
