//==========================================================================
// ゲームイベント:ニリンピック開催[NirinPicHoldingEvent.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "NirinPicHoldingEvent.h"

//==========================================================================
// 初期化
void CNirinPicHolding::Init(std::mt19937 & InputMT)
{
    CSubsidiaryManager * pchar = nullptr;
    CSubsidiaryParam * ptarget = nullptr;

    // プレイヤーのデータを取り出す
    pchar = CSubsidiary::Get(CharList::Player);
    std::uniform_int_distribution<int> SetRand(0, pchar->GetNumSub() - 1); // 範囲の一様乱数

                                                                           // 乱数の生成
    int subID = SetRand(InputMT);

    // 対象地域の登録
    ptarget = pchar->GetSub(subID);
    this->m_target.push_back(ptarget);

    // エネミーのデータを取り出す
    pchar = CSubsidiary::Get(CharList::Enemy);

    // 対象地域の登録
    ptarget = pchar->GetSub(subID);
    this->m_target.push_back(ptarget);

    this->m_bonus = 1200; // 売り上げ加算値を設定
    this->m_NextTurn = 12;
    this->m_key = false;

    // 対象エリアの名前を取り出す
    this->m_targetName = ptarget->m_strName;
}

//==========================================================================
// 解放
void CNirinPicHolding::Uninit(void)
{
    this->m_target.clear();
    this->m_Name.clear();
    this->m_targetName.clear();
}

//==========================================================================
// 更新
void CNirinPicHolding::Update(void)
{
    // 期間の経過
    if (this->m_NextTurn != 0)
    {
        this->m_NextTurn--;
    }

    int push_income = 0;

    // 開催後の処理
    if (this->m_bonus != 0 && this->m_key == true)
    {
        push_income = 100;
        this->m_bonus -= 100;

        // イテレータで最初から最後まで検索
        for (auto itr = this->m_target.begin(); itr != this->m_target.end(); ++itr)
        {
            (*itr)->m_income -= push_income;
        }

        if (this->m_bonus == 0)
        {
            this->m_endkey = true;
        }
    }

    // 開催時の処理
    if (this->m_NextTurn == 0 && this->m_key == false)
    {
        for (auto itr = this->m_target.begin(); itr != this->m_target.end(); ++itr)
        {
            (*itr)->m_income += this->m_bonus;
        }
        this->m_key = true;
        this->m_NextTurn = 12;
    }
}

//==========================================================================
// 描画
void CNirinPicHolding::Draw(void) 
{
}

//==========================================================================
// UI更新
void CNirinPicHolding::UpdateUI(void) 
{
    // 親オブジェクトへのアクセス
    auto * p_obj = (CMobeEventObject*)this->m_master_obj;

    // オブジェクトの座標設定
    this->m_ObjectPos->SetPos(*p_obj->Get()->GetPos());
    this->m_NameObject->SetPos(*p_obj->Get()->GetPos());
}

//==========================================================================
// テキスト
void CNirinPicHolding::text(std::string strEventName)
{
    std::string strEvent = strEventName + this->m_Name;
    if (this->m_Imgui.NewMenu(strEvent.c_str(), true))
    {
        this->m_Imgui.Text("発動中");
        if (this->m_key == false)
        {
            this->m_Imgui.Text("残り%dターン\n%sエリアでニリンピックが開催されます", this->m_NextTurn, this->m_targetName.c_str());
            this->m_Imgui.Text("開催時、%sエリアの売り上げに＋%dされます", this->m_targetName.c_str(), this->m_bonus);
        }
        else
        {
            this->m_Imgui.Text("ニリンピックが終わりました");
            this->m_Imgui.Text("あと%dターンの間、%sエリアの売り上げに−100されていきます", this->m_NextTurn, this->m_targetName.c_str());
        }
        this->m_Imgui.EndMenu();
    }
}