//==========================================================================
#include "WinImage.h"
#include "resource_list.h"

//==========================================================================
// 初期化
bool CWinImage::Init(void)
{
	const char * pUIList[] = {
        RESOURCE_win_DDS,
	};
	int datasize = (int)this->Helper()->Sizeof_(pUIList);
	CVector4<float>vpos;

	// インスタンス生成

	for (int i = 0; i < datasize; i++)
	{
		if (this->_2DPolygon()->Init(pUIList[i], true))
		{
			return true;
		}
		this->_2DPolygon()->ObjectInput(this->_2DObject()->Create());
		this->_2DObject()->Get(i)->Init(i);
	}

	this->_2DPolygon()->SetTexSize(0, this->GetWinSize().m_Width, this->GetWinSize().m_Height);

	vpos[0].x = (float)this->GetWinSize().m_Width / 2;
	vpos[0].y = (float)this->GetWinSize().m_Height / 2;
	this->_2DObject()->Get(0)->SetCentralCoordinatesMood(true);
	this->_2DObject()->Get(0)->SetPos(vpos);

	return false;
}

//==========================================================================
// 解放
void CWinImage::Uninit(void)
{
}

//==========================================================================
// 更新
void CWinImage::Update(void)
{
	//auto * p_obj = this->_2DObject()->Get(0);

	/*if (p_obj->GetScale() >= 0.9f)
	{
	p_obj->Scale(-0.01f);
	}*/
	
	this->_2DPolygon()->Update();
}

//==========================================================================
// 描画
void CWinImage::Draw(void)
{
	this->_2DPolygon()->Draw();
}