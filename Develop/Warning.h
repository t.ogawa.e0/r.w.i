//==========================================================================
// 警告[Warning.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CWarning
// Content: ワーニング
//
//==========================================================================
class CWarning : public CObject
{
public:
    CWarning();
    ~CWarning();
    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;

    // 処理の開始
    void Start(void);
private:
    CTimer m_time;
    CTimer m_start_time;
    CColor<int>m_color;
    bool m_key;
};

