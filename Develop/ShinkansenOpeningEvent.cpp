//==========================================================================
// ゲームイベント:新幹線開通[ShinkansenOpeningEvent.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "ShinkansenOpeningEvent.h"

//==========================================================================
// 初期化
void CShinkansenOpening::Init(void) 
{
}

//==========================================================================
// 解放
void CShinkansenOpening::Uninit(void)
{
    this->m_target.clear();
    this->m_Name.clear();
}

//==========================================================================
// 更新
void CShinkansenOpening::Update(void) 
{ 
    this->m_endkey = true;

    // 親オブジェクトへのアクセス
    auto * p_obj = (CMobeEventObject*)this->m_master_obj;

    // オブジェクトの座標設定
    this->m_ObjectPos->SetPos(*p_obj->Get()->GetPos());
    this->m_NameObject->SetPos(*p_obj->Get()->GetPos());
}

//==========================================================================
// 描画
void CShinkansenOpening::Draw(void) 
{
}

//==========================================================================
// UI更新
void CShinkansenOpening::UpdateUI(void) 
{
    // 親オブジェクトへのアクセス
    auto * p_obj = (CMobeEventObject*)this->m_master_obj;

    // オブジェクトの移動
    this->m_ObjectPos->SetXPlus(p_obj->GetSpeed());
}

//==========================================================================
// テキスト
void CShinkansenOpening::text(std::string strEventName)
{
    std::string strEvent = strEventName + this->m_Name;
    if (this->m_Imgui.NewMenu(strEvent.c_str(), true))
    {
        this->m_Imgui.Text("発動中");
        this->m_Imgui.Text("未実装です");
        this->m_Imgui.EndMenu();
    }
}