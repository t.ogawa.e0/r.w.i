//==========================================================================
// ゲームイベント:油田が見つかった[OilFieldsWereFoundEvent.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "OilFieldsWereFoundEvent.h"

//==========================================================================
// 初期化
void COilFieldsWereFound::Init(std::mt19937 & InputMT)
{
    CSubsidiaryManager * pchar = nullptr;
    CSubsidiaryParam * ptarget = nullptr;

    // プレイヤーのデータを取り出す
    pchar = CSubsidiary::Get(CharList::Player);

    // 範囲の一様乱数
    std::uniform_int_distribution<int> SetRand(0, pchar->GetNumSub() - 1); 

    // 乱数の生成
    int subID = SetRand(InputMT);

    // 対象エリアの登録
    ptarget = pchar->GetSub(subID);
    this->m_target.push_back(ptarget);

    // エネミーのデータを取り出す
    pchar = CSubsidiary::Get(CharList::Enemy);

    // 対象エリアの登録
    ptarget = pchar->GetSub(subID);
    this->m_target.push_back(ptarget);

    // 対象エリアの名前を取り出す
    this->m_targetName = ptarget->m_strName;
}

//==========================================================================
// 解放
void COilFieldsWereFound::Uninit(void)
{
    this->m_target.clear();
    this->m_Name.clear();
}

//==========================================================================
// 更新
void COilFieldsWereFound::Update(void)
{
    // イテレータで最初から最後まで検索
    for (auto itr = this->m_target.begin(); itr != this->m_target.end(); ++itr)
    {
        // 売り上げに加算
        (*itr)->m_income += 1000;
        (*itr)->m_income_default += 1000;
    }
    this->m_endkey = true;
}

//==========================================================================
// 描画
void COilFieldsWereFound::Draw(void)
{
}

//==========================================================================
// UI更新
void COilFieldsWereFound::UpdateUI(void)
{
    // 親オブジェクトへのアクセス
    auto * p_obj = (CMobeEventObject*)this->m_master_obj;

    // オブジェクトの座標設定
    this->m_ObjectPos->SetPos(*p_obj->Get()->GetPos());
    this->m_NameObject->SetPos(*p_obj->Get()->GetPos());
}

//==========================================================================
// テキスト
void COilFieldsWereFound::text(std::string strEventName)
{
    std::string strEvent = strEventName + this->m_Name;
    if (this->m_Imgui.NewMenu(strEvent.c_str(), true))
    {
        this->m_Imgui.Text("発動中");
        this->m_Imgui.Text("なんと！！%sエリアで油田が見つかった！！", this->m_targetName.c_str());
        this->m_Imgui.Text("次、%sエリアの売り上げが＋1000されます", this->m_targetName.c_str());
        this->m_Imgui.EndMenu();
    }
}
