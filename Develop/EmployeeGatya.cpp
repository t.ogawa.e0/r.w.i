//==========================================================================
// 社員ガチャ[EmployeeGatya.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "EmployeeGatya.h"

//==========================================================================
// 初期化
bool CEmployeeGatya::Init(void)
{
    return false;
}

//==========================================================================
// 解放
void CEmployeeGatya::Uninit(void)
{
    this->m_param.Create();
}

//==========================================================================
// 更新
void CEmployeeGatya::Update(void)
{
    // ガチャ
    this->Gatya();
}

//==========================================================================
// 描画
void CEmployeeGatya::Draw(void)
{
}

//==========================================================================
// ガチャ
void CEmployeeGatya::Gatya(void)
{
    // 階層構造
    if (this->ImGui()->NewTreeNode("社員ガチャ", false))
    {
        int nPush = 0;

        if (this->ImGui()->MenuItem("ガチャ"))
        {
            nPush = 1;
        }
        if (this->ImGui()->MenuItem("10連ガチャ"))
        {
            nPush = 10;
        }
        if (this->ImGui()->MenuItem("100連ガチャ"))
        {
            nPush = 100;
        }

        if (nPush != 0)
        {
            this->m_param.Release();

            for (int i = 0; i < nPush; i++)
            {
                auto * pinstance = this->m_param.Create();

                // タイプの生成
                this->CreateType(pinstance);

                // スキルの設定
                this->CreateAge(pinstance);

                // メンタルの生成
                this->CreateCountry(pinstance);

                // コストの生成
                this->CreatePersonality(pinstance);

                // 特徴の生成
                this->CreateCha(pinstance);

                // パートナー判定
                this->CreatePartner(pinstance);

                // アイコンID
                this->CreateICO(pinstance);
            }
        }

        // デバッグ用テキスト
        if (this->m_param.Size() != 0)
        {
            this->ImGui()->NewWindow("ガチャ結果", true);
            for (int i = 0; i < this->m_param.Size(); i++)
            {
                this->SetText(this->m_param.Get(i));
                if (i < this->m_param.Size() - 1)
                {
                    this->ImGui()->Separator();
                }
            }
            this->ImGui()->EndWindow();
        }
        this->ImGui()->EndTreeNode();
    }
}
