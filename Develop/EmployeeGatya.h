//==========================================================================
// 社員ガチャ[EmployeeGatya.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _EmployeeGatya_H_
#define _EmployeeGatya_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "EmployeeParamSystem.h"

//==========================================================================
//
// class  : CEmployeeGatya
// Content: 社員ガチャ
//
//==========================================================================
class CEmployeeGatya : public CObject, private CEmployeeParamSystem
{
public:
    CEmployeeGatya() : CObject(CObject::ID::Default) {}
    ~CEmployeeGatya() {
        this->Uninit();
    }
    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;
private:
    // ガチャ
    void Gatya(void);
private:
    CObjectVectorManager<CParam> m_param; // パラメータ生成
};

#endif // !_EmployeeGatya_H_
