//==========================================================================
// ガチャUI[Gacha_UI_Draw.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "EmployeeParamSystem.h"

//==========================================================================
//
// class  : COperationExplanation
// Content: 操作説明/制御
//
//==========================================================================
class CGacha_UI_Draw : public CObject
{
private:
    class CUI 
    {
    public:
        CUI() {
            this->m_data = nullptr;
            this->m_tim.Init(1, 60);
            this->m_tim_delete.Init(1, 60);
        }
        ~CUI() {
            this->m_2DObject.Release();
        }
        CObjectVectorManager<C2DObject>* GetGachaUI(void) {
            return &this->m_2DObject;
        }
        void initializer(CDirectXDevice::CWindowsSize<int> _winsize, C2DPolygon * p_poly, EmployeeParam * _data);

        void SetParameter(C2DPolygon * p_poly);
        // 雇用時の座標
        void InputPos(void);
        // 解雇時の座標
        void OutPos(CObjectVectorManager<C2DObject> * Input);
        // 移動
        void Move(void);
    private:
        CVector2<int> m_winsize; // ウィンドウサイズ
        CVector4<float> m_MasterPos; // 親座標
        CTexvec<int> m_MasterTexSize; // 親テクスチャサイズ
        CObjectVectorManager<C2DObject>m_2DObject;
        EmployeeParam * m_data;
        float fpeed = 0.0f;
        CHitDetermination m_hit; // コリジョン
        CTimer m_tim;
    public:
        CTimer m_tim_delete;
    };
public:
    enum class TexList {
        Begin = 0,
        MainScreen_Syokuin_Rirekisyo = Begin, // シャインデータ表示用背景
        MainScreen_Syokuni_Name, // 名前テクスチャID
        MainScreen_syoumeisyashin, // アイコンID
        MainScreen_Syokuin_Rank_skill, // skillレアリティID
        MainScreen_Syokuin_Rank_mental, // mentalレアリティID
        MainScreen_Syokuin_Rank_cost, // costレアリティID
        MainScreen_Syokuin_Tokuchou, // 特徴ID
        MainScreen_Syokuin_Type, // タイプID
        MainScreen_Syokuin_Type_Partner_true, // パートナーがいない
        MainScreen_Syokuin_Type_Partner_false, // パートナーがいる
        End, 
    };
public:
    CGacha_UI_Draw() :CObject(CObject::ID::Polygon2D) {
        this->SetType(Type::Game_GachaUI);
        this->m_EmployeeGatya = nullptr;
        this->m_TransferAnimation = nullptr;
    }
    ~CGacha_UI_Draw() {
        this->Uninit();
    }
    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;

    // 生成
    void Create(EmployeeParam * _data);
private:
    CObjectVectorManager<CUI> m_GachaObject;
    void * m_EmployeeGatya;
    void * m_TransferAnimation;
};

using E_CharTexList = CGacha_UI_Draw::TexList;