//==========================================================================
// タイトル[Title.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Title.h"
#include "TitleUI.h"
#include "Title_Sound.h"
#include "InterruptToTitle.h"

CTitle::CTitle()
{
    // オブジェクトの登録
    CTitleUI * TitleUI;
    CTitle_Sound * TitleSound;
    CInterruptToTitle * InterruptToTitle;

    // オブジェクトの宣言
    CObject::NewObject(TitleUI);
    CObject::NewObject(TitleSound);
    CObject::NewObject(InterruptToTitle);

    TitleUI->SetSoundObj(TitleSound);
}

CTitle::~CTitle()
{
}

//==========================================================================
// 初期化
bool CTitle::Init(void)
{
    // オブジェクトの初期化
    return CObject::InitAll();
}

//==========================================================================
// 解放
void CTitle::Uninit(void)
{
    CObject::ReleaseAll();
}

//==========================================================================
// 更新
void CTitle::Update(void)
{
    CObject::UpdateAll();
}

//==========================================================================
// 描画
void CTitle::Draw(void)
{
    CObject::DrawAll();
}