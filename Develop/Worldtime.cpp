//==========================================================================
// 世界時間[Worldtime.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Worldtime.h"
#include "Game_Start.h"

//==========================================================================
// 初期化
bool CWorldTime::Init(void)
{
	this->m_manage = CGameManager();
    this->m_start_script = this->GetObjects(ID::Polygon2D, Type::Game_Start_Object);
    return false;
}

//==========================================================================
// 解放
void CWorldTime::Uninit(void)
{
}

//==========================================================================
// 更新
void CWorldTime::Update(void)
{
    if (this->m_start_script == nullptr)
    {
        this->m_start_script = this->GetObjects(ID::Polygon2D, Type::Game_Start_Object);
    }

    auto * p_start = (CGame_Start*)this->m_start_script;

    // オブジェクトが存在するとき
    if (p_start != nullptr)
    {
        if (p_start->Start())
        {
            this->m_manage.Update();

            if (this->ImGui()->NewMenu("ワールド情報", true))
            {
                this->ImGui()->Text("経過時間 : %d / %2d", this->m_manage.getime()->GetTime(), this->m_manage.getime()->GetComma());
                this->ImGui()->Text("%dターン", this->m_manage.getturns().m_current);
                this->ImGui()->EndMenu();
            }
        }
    }
}

//==========================================================================
// 描画
void CWorldTime::Draw(void)
{
}
