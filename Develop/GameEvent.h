//==========================================================================
// ゲームイベント[GameEvent.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _GameEvent_H_
#define _GameEvent_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include <random>
#include <list>
#include <vector>
#include <string>
#include "SubsidiaryParam.h"
#include "Subsidiary.h"
#include "CharList.h"

//==========================================================================
//
// class  : CEventManager
// Content: イベントマネージャー
//
//==========================================================================
class CEventManager : public CCharList
{
public:
    enum class List
    {
        none = 0,
        BubbleEconomy, // バブル景気
        BubbleCollapse, // バブル崩壊
        NirinPicHolding, // ニリンピック開催
        Communism, // 共産主義
        NewCompany, // 新企業
        TheBirthOfPopularAnimals, // 人気のある動物の誕生
        CapitalCityChange, // 首都の場所が変わった
        AnimeHolyGround, // アニメの聖地
        OilFieldsWereFound, // 油田が見つかった
        end,
    };

    // アニメーションキー
    enum class JapanList {
        ポン = (int)List::end,
        ヨシノ,
        ショウク,
        シナノ,
        ハッカイ,
        ヒカフ,
        コクシ,
        ニト,
        end,
    };
    // アニメーションキー
    enum class USAList {
        ビームス = (int)List::end,
        ジャック,
        トマティ,
        トルマ,
        ベレンタイン,
        ローゼス,
        ウォーカー,
        ホース,
        ヘネス,
        ハーパー,
        end,
    };
    // アニメーションキー
    enum class HawaiiList {
        ソルティ = (int)List::end,
        ジニック,
        ミェール,
        ヌテイン,
        end,
    };
    // アニメーションキー
    enum class AustraliaList {
        タンカレー = (int)List::end,
        ボンベイ,
        シロッコ,
        ヒル,
        ゴードン,
        ギルビー,
        フィーター,
        end,
    };
public:
	CEventManager()
	{
        this->m_ObjectPos = new C2DObject;
        this->m_NameObject = new C2DObject;
		this->m_Name = "";
		this->m_targetName = "";
		this->m_endkey = false;
        this->m_IventNameE = (List)-1;
        this->m_master_obj = nullptr;
	}
	virtual ~CEventManager()
	{
        delete this->m_ObjectPos;
        delete this->m_NameObject;
		this->m_target.clear();
		this->m_targetName.clear();
		this->m_Name.clear();
	}

	// 継承解放
	virtual void Uninit(void) = 0;

	// 継承更新
	virtual void Update(void) = 0;

    // 継承UI更新
    virtual void UpdateUI(void) = 0;

	// 継承描画
	virtual void Draw(void) = 0;

	// テキスト
	virtual void text(std::string strEventName) = 0;

	bool end(void) { return this->m_endkey; }

    void InputEventID(List id) {
        this->m_IventNameE = id;
        this->m_ObjectPos->Init((int)this->m_IventNameE);
    }

    void InputAreaName(int id) {
        this->m_NameObject->Init(id);
    }

    List GetEventID(void) {
        return this->m_IventNameE;
    }

    // インスタンスのリターン
    C2DObject * GetObjectData(void) {
        return this->m_ObjectPos;
    }

    // インスタンスのリターン
    C2DObject * GetAreaName(void) {
        return this->m_NameObject;
    }


    // ウィンドウサイズの取得
    CDirectXDevice::CWindowsSize<int> GetWinSize(void) {
        return CDeviceManager::GetDXDevice()->GetWindowsSize();
    }

    // 親オブジェクトへのアクセスルート
    void SetMasterPos(void * p_master_object) {
        this->m_master_obj = p_master_object;
    }

    std::string & GetTargetName(void) {
        return this->m_targetName;
    }
protected:
	std::list<CSubsidiaryParam*> m_target;
	std::string m_Name; // イベント名
	std::string m_targetName; // 対象エリアの名前
	CImGui_Dx9 m_Imgui; // デバッグウィンドウ
	bool m_endkey; // 終了キー
    List m_IventNameE;
    void * m_master_obj; // 親オブジェクトへのアクセスルート
    C2DObject * m_ObjectPos = nullptr;
    C2DObject * m_NameObject = nullptr;
    CHitDetermination m_collision; // 当たり判定
};

//==========================================================================
//
// class  : CProbabilityTable
// Content: 確率表
//
//==========================================================================
template <typename types>
class CProbabilityTable
{
public:
	CProbabilityTable()
	{
		this->m_min = (types)0;
		this->m_max = (types)0;
		this->m_id = (CEventManager::List) - 1;
	}
	CProbabilityTable(types min_, types max_, CEventManager::List id_)
	{
		this->m_min = min_;
		this->m_max = max_;
		this->m_id = id_;
	}
	CProbabilityTable(types min_, types max_)
	{
		this->m_min = min_;
		this->m_max = max_;
		this->m_id = (CEventManager::List) - 1;
	}
	~CProbabilityTable() {}

	// 戻り値は次のmin
	types Set(types min_, types Input, CEventManager::List id_)
	{
		this->m_min = min_;
		this->m_max = this->m_min + Input;
		this->m_id = id_;
		return this->m_max + (types)0.1f;
	}
	// 判定
	bool Search(types Input)
	{
		if (this->m_min <= Input&&Input <= this->m_max)
		{
			return true;
		}
		return false;
	}
	void SetMin(types Input) { this->m_min = Input; }
	types GetMin(void) { return this->m_min; }
	void SetMax(types Input) { this->m_max = Input; }
	types GetMax(void) { return this->m_max; }
	void SetID(CEventManager::List Input) { this->m_id = Input; }
	CEventManager::List GetID(void) { return this->m_id; }
private:
	types m_min; // 範囲の先頭
	types m_max; // 範囲の終わり
	CEventManager::List m_id; // イベントID
};

//==========================================================================
//
// class  : CMobeEventObject
// Content: MobeEventObject
//
//==========================================================================
class CMobeEventObject
{
public:
    CMobeEventObject() {
        this->m_ObjID = 0;
        this->m_MoveKey1 = false;
        this->m_MoveKey2 = false;
        this->m_EndKey = false;
        this->m_lock = false;
        this->m_countor = 0;
        this->m_speed = 0.0f;
    }
    ~CMobeEventObject() {
    }
    // 初期化
    void Init(int ObjectID) {
        this->m_ObjID = ObjectID;
        this->m_pos.Init(ObjectID);
        this->m_limitpos.Init(ObjectID);
        this->m_startpos.Init(ObjectID);
    }
    // 更新
    void Update(void) {
        // 移動速度
        this->m_speed = 0.0f;

        // 出現処理
        if (this->m_MoveKey1&&this->m_lock == false)
        {
            CHitDetermination hit;
            float fDis = hit.Distance(this->m_pos, this->m_limitpos);
            this->m_speed = fDis*0.1f; // 移動速度
            this->m_pos.SetXPlus(this->m_speed);
            if (fDis<1.0f)
            {
                this->m_lock = true;
                this->m_MoveKey1 = false;
                this->m_pos.SetPos(*this->m_limitpos.GetPos());
            }
        }
        // 終了処理
        if (this->m_MoveKey2&&this->m_lock == true)
        {
            CHitDetermination hit;
            float fDis = hit.Distance(this->m_pos, this->m_startpos);
            this->m_speed = -fDis*0.1f; // 移動速度
            this->m_pos.SetXPlus(this->m_speed);
            if (fDis<1.0f)
            {
                this->m_MoveKey2 = false;
                this->m_EndKey = true;
            }
        }
    }
    // 座標セット
    void SetPos(CVector4<float>&StartVpos, CVector4<float>&EndVpos) {
        this->m_pos.SetPos(StartVpos);
        this->m_startpos.SetPos(StartVpos);
        this->m_limitpos.SetPos(EndVpos);
    }
    // 座標の所得
    C2DObject * Get(void) {
        return &this->m_pos;
    }
    // オブジェクトID
    int ObjectID(void) {
        return this->m_ObjID;
    }
    // 出現処理開始
    void Start(void) {
        this->m_MoveKey1 = true;
    }
    // 出現終わり処理
    void End(void) {
        this->m_MoveKey2 = true;
    }
    // 終了キー
    bool EndKey(void){
        return this->m_EndKey;
    }

    // 鍵2
    bool Key2(void) {
        return this->m_lock;
    }

    // start_pos
    const C2DObject * GetBeginPos(void) const {
        return &this->m_startpos;
    }

    // end_pos
    const C2DObject * GetEndPos(void) const {
        return &this->m_limitpos;
    }

    // オブジェクトの移動速度
    float GetSpeed(void) {
        return this->m_speed;
    }
private:
    int m_ObjID; // オブジェクトID
    C2DObject m_pos; // 現在座標
    C2DObject m_limitpos; // 移動先座標
    C2DObject m_startpos; // 移動先座標
    bool m_MoveKey1; // 移動判定1
    bool m_MoveKey2; // 移動判定2
    bool m_lock; // 鍵1
    bool m_EndKey; // 終了キー
    int m_countor; // カウンター
    float m_speed; // オブジェクトの移動速度X
};

class CEventScript
{
private:
    class CData {
    public:
        CData(){
            this->m_list = (CEventManager::List) - 1;
            this->m_casttime = 0;
        }
        ~CData(){}
    public:
        CEventManager::List m_list; // 実行したイベントタグ
        int m_casttime; // リキャスト時間
    };
public:
    CEventScript() {}
    ~CEventScript() {}
    bool Input(CEventManager::List name, int turn) {
        CData * pda = nullptr;
        for (int i = 0; i < this->m_script.Size(); i++)
        {
            pda = this->m_script.Get(i);
            if (pda->m_list == name)
            {
                return false;
            }
        }
        pda = this->m_script.Create();
        pda->m_casttime = turn;
        pda->m_list = name;
        return true;
    }

    void Update(void) {
        for (int i = 0; i < this->m_script.Size(); i++)
        {
            CData * pda = this->m_script.Get(i);

            pda->m_casttime--;
            if (pda->m_casttime == 0)
            {
                this->m_script.PinpointRelease(pda);
                break;
            }
        }
    }
private:
    CObjectVectorManager<CData>m_script; // 制御スクリプト
};
 
//==========================================================================
//
// class  : CGameEvent
// Content: ゲームイベント
//
//==========================================================================
class CGameEvent : private CObject
{
private:
    // シシャテクスチャ切り替え用
    class CDataList
    {
    public:
        CDataList() {
            this->m_CompanyName = "";
            this->m_texID = -1;
        }
        ~CDataList() {
            this->m_CompanyName.clear();
        }

        void Set(const std::string & Name, int texID) {
            this->m_CompanyName = Name;
            this->m_texID = texID;
        }

        int GetTexID(void) {
            return this->m_texID;
        }

        std::string & GetTag(void) {
            return this->m_CompanyName;
        }
    private:
        std::string m_CompanyName; // シシャ名
        int m_texID;
    };
public:
	CGameEvent() :CObject(CObject::ID::Polygon2D)
	{
		std::random_device rnd; // 非決定的な乱数生成器を生成
		std::mt19937 mt(rnd());  //  メルセンヌ・ツイスタの32ビット版、引数は初期シード値
		this->m_mt = mt;
		this->m_EventSearch = std::uniform_real_distribution<float>(0.0f, 0.0f);
		this->m_rand = std::uniform_int_distribution<int>(0, 0);
        this->m_start_script = nullptr;
        this->m_WorldTime = nullptr;
	}
	~CGameEvent(){}
	// 初期化
	bool Init(void)override;

	// 解放
	void Uninit(void)override;

	// 更新
	void Update(void)override;

	// 描画
	void Draw(void)override;
private:
	// イベント生成
	void create(int Input);
	// イベントセット
	void set(CEventManager::List Input);

	// イベントタスクの解放
	void ReleaseTask(void);

	// テキストセット
	void text(void);

    // 移動開始
    void Move(void);

    // インスタンスの生成
    CMobeEventObject * CreateInstance(void);

    // イベントのイニシャライザ
    void EventInitialize(CEventManager::List Input);

    // イベント強制発動
    void EventForcedActivation(void);

    // 日本
    bool Japan_Init(void);

    // USA
    bool USA_Init(void);

    // ハワイ
    bool Hawaii_Init(void);

    // オーストラリア
    bool Australia_Init(void);
private:
	std::mt19937 m_mt; // 乱数
	std::uniform_real_distribution<float> m_EventSearch; // 範囲の一様乱数
	std::uniform_int_distribution<int> m_rand; // 範囲の一様乱数
	std::list<CProbabilityTable<float>> m_Table; // 管理
	std::list<CEventManager*> m_task; // イベントタスク
	CProbabilityTable<int> m_Evetable; // 確率表
    CObjectVectorManager<CMobeEventObject>m_EventPos; // イベント座標
    CEventScript m_Script; // 制御スクリプト
    void * m_WorldTime; // 時間へのアクセスルート
    void * m_start_script; // スタート制御
    CObjectVectorManager<CDataList>m_Data;
};

#endif // !_GameEvent_H_
