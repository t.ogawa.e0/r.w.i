//==========================================================================
// まねー[Money.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CMoney
// Content: まねー
//
//==========================================================================
class CMoney : public CObject
{
public:
    CMoney();
    ~CMoney();

    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;
private:
    // エフェクト生成
    void Create(int id);
private:
    void * m_player;
    void * m_WorldTime;
    void * m_start_script;
};

