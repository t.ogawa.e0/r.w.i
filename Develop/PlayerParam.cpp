//==========================================================================
// プレイヤーのパラメーター[PlayerParam.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "PlayerParam.h"

//==========================================================================
// 更新
void CPlayerParam::Update(void)
{
	this->m_wage = this->m_wage_sub;
	this->m_total_wage = (int)(this->m_wage*this->m_total_employee);
    this->m_asset = this->m_asset - this->m_total_wage;
    if (this->m_asset <= 0)
    {
        this->m_asset = 0;
    }
}

//==========================================================================
// 不満度が最大になった！！
bool CPlayerParam::DissatisfiedHit(void)
{
	if (this->m_wage <= this->m_dissatisfied)
	{
		this->m_dissatisfied = 0;
		return true;
	}
	return false;
}
