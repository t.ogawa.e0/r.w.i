//==========================================================================
// CGameEnd[GameEnd.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

enum class EGameEndKey
{
    WIN, // 勝ち
    LOSE, // 負け
};

//==========================================================================
//
// class  : CGameEnd
// Content: CGameEnd
//
//==========================================================================
class CGameEnd : public CObject
{
public:
    CGameEnd() : CObject(ID::Default) {
        this->SetType(Type::Game_End);
        this->m_key = EGameEndKey(-1);
    }
    ~CGameEnd() {}
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;

    void EndKey(EGameEndKey key);
public:
    void Load(void);
    void Save(void);
public:
    EGameEndKey m_key; // 情報
};
