//==========================================================================
// Q[Cxg:ÅáÀà[MinimumWageEvent.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _MinimumWageEvent_H_
#define _MinimumWageEvent_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "GameEvent.h"

//==========================================================================
//
// class  : CMinimumWage
// Content: ÅáÀà
//
//==========================================================================
class CMinimumWage : public CEventManager
{
public:
    CMinimumWage()
    {
        this->m_Name = "ÅáÀà";

    }
    ~CMinimumWage()
    {
        this->m_target.clear();
        this->m_Name.clear();
    }

    // ú»
    void Init(void);

    // ðú
    void Uninit(void)override;

    // XV
    void Update(void)override;

    // `æ
    void Draw(void)override;

    // UIXV
    void UpdateUI(void)override;

    // eLXg
    void text(std::string strEventName)override;
private:

};

#endif // !_MinimumWageEvent_H_
