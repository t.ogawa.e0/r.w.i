//=============================================================================
// リソースのファイルパス
// 更新日 [2018/06/30]
// 更新時間 [09:41:33]
//=============================================================================
#pragma once

//=============================================================================
// 拡張子 [.DDS]
//=============================================================================

#ifndef RESOURCE_coution_DDS
#define RESOURCE_coution_DDS "resource/texture/coution.DDS"
#endif // !RESOURCE_coution_DDS
#ifndef RESOURCE_LoadTex_DDS
#define RESOURCE_LoadTex_DDS "resource/texture/LoadTex.DDS"
#endif // !RESOURCE_LoadTex_DDS
#ifndef RESOURCE_NowLoading_DDS
#define RESOURCE_NowLoading_DDS "resource/texture/NowLoading.DDS"
#endif // !RESOURCE_NowLoading_DDS
#ifndef RESOURCE_Okane_Down_Re02_DDS
#define RESOURCE_Okane_Down_Re02_DDS "resource/texture/Okane_Down_Re02.DDS"
#endif // !RESOURCE_Okane_Down_Re02_DDS
#ifndef RESOURCE_Okane_Up_Re02_DDS
#define RESOURCE_Okane_Up_Re02_DDS "resource/texture/Okane_Up_Re02.DDS"
#endif // !RESOURCE_Okane_Up_Re02_DDS
#ifndef RESOURCE_sky_DDS
#define RESOURCE_sky_DDS "resource/texture/sky.DDS"
#endif // !RESOURCE_sky_DDS
#ifndef RESOURCE_suimen_DDS
#define RESOURCE_suimen_DDS "resource/texture/suimen.DDS"
#endif // !RESOURCE_suimen_DDS
#ifndef RESOURCE_lose_DDS
#define RESOURCE_lose_DDS "resource/texture/result/lose.DDS"
#endif // !RESOURCE_lose_DDS
#ifndef RESOURCE_Lose_Chousei_DDS
#define RESOURCE_Lose_Chousei_DDS "resource/texture/result/Lose_Chousei.DDS"
#endif // !RESOURCE_Lose_Chousei_DDS
#ifndef RESOURCE_win_DDS
#define RESOURCE_win_DDS "resource/texture/result/win.DDS"
#endif // !RESOURCE_win_DDS
#ifndef RESOURCE_Win_Chousei_DDS
#define RESOURCE_Win_Chousei_DDS "resource/texture/result/Win_Chousei.DDS"
#endif // !RESOURCE_Win_Chousei_DDS
#ifndef RESOURCE_logo2_DDS
#define RESOURCE_logo2_DDS "resource/texture/logo/logo2.DDS"
#endif // !RESOURCE_logo2_DDS
#ifndef RESOURCE_title_logo_02_DDS
#define RESOURCE_title_logo_02_DDS "resource/texture/logo/title_logo_02.DDS"
#endif // !RESOURCE_title_logo_02_DDS
#ifndef RESOURCE_MainScreen_Cursor_DDS
#define RESOURCE_MainScreen_Cursor_DDS "resource/texture/UI/MainScreen_Cursor.DDS"
#endif // !RESOURCE_MainScreen_Cursor_DDS
#ifndef RESOURCE_MainScreen_Dissatisfied_DDS
#define RESOURCE_MainScreen_Dissatisfied_DDS "resource/texture/UI/MainScreen_Dissatisfied.DDS"
#endif // !RESOURCE_MainScreen_Dissatisfied_DDS
#ifndef RESOURCE_MainScreen_Dissatisfied_Indication_DDS
#define RESOURCE_MainScreen_Dissatisfied_Indication_DDS "resource/texture/UI/MainScreen_Dissatisfied_Indication.DDS"
#endif // !RESOURCE_MainScreen_Dissatisfied_Indication_DDS
#ifndef RESOURCE_MainScreen_Employee_DDS
#define RESOURCE_MainScreen_Employee_DDS "resource/texture/UI/MainScreen_Employee.DDS"
#endif // !RESOURCE_MainScreen_Employee_DDS
#ifndef RESOURCE_MainScreen_Newspaper_DDS
#define RESOURCE_MainScreen_Newspaper_DDS "resource/texture/UI/MainScreen_Newspaper.DDS"
#endif // !RESOURCE_MainScreen_Newspaper_DDS
#ifndef RESOURCE_MainScreen_Newspaper_Numbers_DDS
#define RESOURCE_MainScreen_Newspaper_Numbers_DDS "resource/texture/UI/MainScreen_Newspaper_Numbers.DDS"
#endif // !RESOURCE_MainScreen_Newspaper_Numbers_DDS
#ifndef RESOURCE_MainScreen_Normal_Numbers_DDS
#define RESOURCE_MainScreen_Normal_Numbers_DDS "resource/texture/UI/MainScreen_Normal_Numbers.DDS"
#endif // !RESOURCE_MainScreen_Normal_Numbers_DDS
#ifndef RESOURCE_MainScreen_Normal_Numbers2_DDS
#define RESOURCE_MainScreen_Normal_Numbers2_DDS "resource/texture/UI/MainScreen_Normal_Numbers2.DDS"
#endif // !RESOURCE_MainScreen_Normal_Numbers2_DDS
#ifndef RESOURCE_MainScreen_Salary_DDS
#define RESOURCE_MainScreen_Salary_DDS "resource/texture/UI/MainScreen_Salary.DDS"
#endif // !RESOURCE_MainScreen_Salary_DDS
#ifndef RESOURCE_MainScreen_Score_DDS
#define RESOURCE_MainScreen_Score_DDS "resource/texture/UI/MainScreen_Score.DDS"
#endif // !RESOURCE_MainScreen_Score_DDS
#ifndef RESOURCE_MainScreen_Turn_DDS
#define RESOURCE_MainScreen_Turn_DDS "resource/texture/UI/MainScreen_Turn.DDS"
#endif // !RESOURCE_MainScreen_Turn_DDS
#ifndef RESOURCE_Ibent_01_CV_DDS
#define RESOURCE_Ibent_01_CV_DDS "resource/texture/UI/Event/Ibent_01_CV.DDS"
#endif // !RESOURCE_Ibent_01_CV_DDS
#ifndef RESOURCE_Ibent_03_CV_DDS
#define RESOURCE_Ibent_03_CV_DDS "resource/texture/UI/Event/Ibent_03_CV.DDS"
#endif // !RESOURCE_Ibent_03_CV_DDS
#ifndef RESOURCE_Ibent_04_CV_DDS
#define RESOURCE_Ibent_04_CV_DDS "resource/texture/UI/Event/Ibent_04_CV.DDS"
#endif // !RESOURCE_Ibent_04_CV_DDS
#ifndef RESOURCE_Ibent_05_DDS
#define RESOURCE_Ibent_05_DDS "resource/texture/UI/Event/Ibent_05.DDS"
#endif // !RESOURCE_Ibent_05_DDS
#ifndef RESOURCE_Ibent_06_CV_DDS
#define RESOURCE_Ibent_06_CV_DDS "resource/texture/UI/Event/Ibent_06_CV.DDS"
#endif // !RESOURCE_Ibent_06_CV_DDS
#ifndef RESOURCE_Ibent_07_CV_DDS
#define RESOURCE_Ibent_07_CV_DDS "resource/texture/UI/Event/Ibent_07_CV.DDS"
#endif // !RESOURCE_Ibent_07_CV_DDS
#ifndef RESOURCE_Ibent_08_CV_DDS
#define RESOURCE_Ibent_08_CV_DDS "resource/texture/UI/Event/Ibent_08_CV.DDS"
#endif // !RESOURCE_Ibent_08_CV_DDS
#ifndef RESOURCE_Ibent_09_CV_DDS
#define RESOURCE_Ibent_09_CV_DDS "resource/texture/UI/Event/Ibent_09_CV.DDS"
#endif // !RESOURCE_Ibent_09_CV_DDS
#ifndef RESOURCE_Ibent_10_DDS
#define RESOURCE_Ibent_10_DDS "resource/texture/UI/Event/Ibent_10.DDS"
#endif // !RESOURCE_Ibent_10_DDS
#ifndef RESOURCE_Barenta_DDS
#define RESOURCE_Barenta_DDS "resource/texture/UI/Event/America/Barenta.DDS"
#endif // !RESOURCE_Barenta_DDS
#ifndef RESOURCE_Bimusu_DDS
#define RESOURCE_Bimusu_DDS "resource/texture/UI/Event/America/Bimusu.DDS"
#endif // !RESOURCE_Bimusu_DDS
#ifndef RESOURCE_Hapa_DDS
#define RESOURCE_Hapa_DDS "resource/texture/UI/Event/America/Hapa.DDS"
#endif // !RESOURCE_Hapa_DDS
#ifndef RESOURCE_Henesu_DDS
#define RESOURCE_Henesu_DDS "resource/texture/UI/Event/America/Henesu.DDS"
#endif // !RESOURCE_Henesu_DDS
#ifndef RESOURCE_Hosu_DDS
#define RESOURCE_Hosu_DDS "resource/texture/UI/Event/America/Hosu.DDS"
#endif // !RESOURCE_Hosu_DDS
#ifndef RESOURCE_Jakku_DDS
#define RESOURCE_Jakku_DDS "resource/texture/UI/Event/America/Jakku.DDS"
#endif // !RESOURCE_Jakku_DDS
#ifndef RESOURCE_Tomateli_DDS
#define RESOURCE_Tomateli_DDS "resource/texture/UI/Event/America/Tomateli.DDS"
#endif // !RESOURCE_Tomateli_DDS
#ifndef RESOURCE_Toruma_DDS
#define RESOURCE_Toruma_DDS "resource/texture/UI/Event/America/Toruma.DDS"
#endif // !RESOURCE_Toruma_DDS
#ifndef RESOURCE_Uloka_DDS
#define RESOURCE_Uloka_DDS "resource/texture/UI/Event/America/Uloka.DDS"
#endif // !RESOURCE_Uloka_DDS
#ifndef RESOURCE_Wazesu_DDS
#define RESOURCE_Wazesu_DDS "resource/texture/UI/Event/America/Wazesu.DDS"
#endif // !RESOURCE_Wazesu_DDS
#ifndef RESOURCE_Bonbei_DDS
#define RESOURCE_Bonbei_DDS "resource/texture/UI/Event/aust/Bonbei.DDS"
#endif // !RESOURCE_Bonbei_DDS
#ifndef RESOURCE_Fita_DDS
#define RESOURCE_Fita_DDS "resource/texture/UI/Event/aust/Fita.DDS"
#endif // !RESOURCE_Fita_DDS
#ifndef RESOURCE_Girubi_DDS
#define RESOURCE_Girubi_DDS "resource/texture/UI/Event/aust/Girubi.DDS"
#endif // !RESOURCE_Girubi_DDS
#ifndef RESOURCE_Godon_DDS
#define RESOURCE_Godon_DDS "resource/texture/UI/Event/aust/Godon.DDS"
#endif // !RESOURCE_Godon_DDS
#ifndef RESOURCE_Hiru_DDS
#define RESOURCE_Hiru_DDS "resource/texture/UI/Event/aust/Hiru.DDS"
#endif // !RESOURCE_Hiru_DDS
#ifndef RESOURCE_Shirokko_DDS
#define RESOURCE_Shirokko_DDS "resource/texture/UI/Event/aust/Shirokko.DDS"
#endif // !RESOURCE_Shirokko_DDS
#ifndef RESOURCE_Tankare_DDS
#define RESOURCE_Tankare_DDS "resource/texture/UI/Event/aust/Tankare.DDS"
#endif // !RESOURCE_Tankare_DDS
#ifndef RESOURCE_Jinikku_DDS
#define RESOURCE_Jinikku_DDS "resource/texture/UI/Event/Hawaii/Jinikku.DDS"
#endif // !RESOURCE_Jinikku_DDS
#ifndef RESOURCE_Myuru_DDS
#define RESOURCE_Myuru_DDS "resource/texture/UI/Event/Hawaii/Myuru.DDS"
#endif // !RESOURCE_Myuru_DDS
#ifndef RESOURCE_Soruteli_DDS
#define RESOURCE_Soruteli_DDS "resource/texture/UI/Event/Hawaii/Soruteli.DDS"
#endif // !RESOURCE_Soruteli_DDS
#ifndef RESOURCE_Sutein_DDS
#define RESOURCE_Sutein_DDS "resource/texture/UI/Event/Hawaii/Sutein.DDS"
#endif // !RESOURCE_Sutein_DDS
#ifndef RESOURCE_Bon_DDS
#define RESOURCE_Bon_DDS "resource/texture/UI/Event/japan/Bon.DDS"
#endif // !RESOURCE_Bon_DDS
#ifndef RESOURCE_Hakkai_DDS
#define RESOURCE_Hakkai_DDS "resource/texture/UI/Event/japan/Hakkai.DDS"
#endif // !RESOURCE_Hakkai_DDS
#ifndef RESOURCE_Hikawa_DDS
#define RESOURCE_Hikawa_DDS "resource/texture/UI/Event/japan/Hikawa.DDS"
#endif // !RESOURCE_Hikawa_DDS
#ifndef RESOURCE_Kokushi_DDS
#define RESOURCE_Kokushi_DDS "resource/texture/UI/Event/japan/Kokushi.DDS"
#endif // !RESOURCE_Kokushi_DDS
#ifndef RESOURCE_Nito_DDS
#define RESOURCE_Nito_DDS "resource/texture/UI/Event/japan/Nito.DDS"
#endif // !RESOURCE_Nito_DDS
#ifndef RESOURCE_Shinano_DDS
#define RESOURCE_Shinano_DDS "resource/texture/UI/Event/japan/Shinano.DDS"
#endif // !RESOURCE_Shinano_DDS
#ifndef RESOURCE_Syouku_DDS
#define RESOURCE_Syouku_DDS "resource/texture/UI/Event/japan/Syouku.DDS"
#endif // !RESOURCE_Syouku_DDS
#ifndef RESOURCE_Yoshino_DDS
#define RESOURCE_Yoshino_DDS "resource/texture/UI/Event/japan/Yoshino.DDS"
#endif // !RESOURCE_Yoshino_DDS
#ifndef RESOURCE_MainScreen_Company_Date_DDS
#define RESOURCE_MainScreen_Company_Date_DDS "resource/texture/UI/MainScreen_Company/MainScreen_Company_Date.DDS"
#endif // !RESOURCE_MainScreen_Company_Date_DDS
#ifndef RESOURCE_MainScreen_Company_Date_tab_DDS
#define RESOURCE_MainScreen_Company_Date_tab_DDS "resource/texture/UI/MainScreen_Company/MainScreen_Company_Date_tab.DDS"
#endif // !RESOURCE_MainScreen_Company_Date_tab_DDS
#ifndef RESOURCE_MainScreen_Company_HonsyaMark_DDS
#define RESOURCE_MainScreen_Company_HonsyaMark_DDS "resource/texture/UI/MainScreen_Company/MainScreen_Company_HonsyaMark.DDS"
#endif // !RESOURCE_MainScreen_Company_HonsyaMark_DDS
#ifndef RESOURCE_MainScreen_Syokuin_Date_tab_DDS
#define RESOURCE_MainScreen_Syokuin_Date_tab_DDS "resource/texture/UI/MainScreen_Syokuin/MainScreen_Syokuin_Date_tab.DDS"
#endif // !RESOURCE_MainScreen_Syokuin_Date_tab_DDS
#ifndef RESOURCE_MainScreen_Syokuin_Kokuseki_DDS
#define RESOURCE_MainScreen_Syokuin_Kokuseki_DDS "resource/texture/UI/MainScreen_Syokuin/MainScreen_Syokuin_Kokuseki.DDS"
#endif // !RESOURCE_MainScreen_Syokuin_Kokuseki_DDS
#ifndef RESOURCE_MainScreen_Syokuin_Nenrei_DDS
#define RESOURCE_MainScreen_Syokuin_Nenrei_DDS "resource/texture/UI/MainScreen_Syokuin/MainScreen_Syokuin_Nenrei.DDS"
#endif // !RESOURCE_MainScreen_Syokuin_Nenrei_DDS
#ifndef RESOURCE_MainScreen_Syokuin_Rirekisyo_DDS
#define RESOURCE_MainScreen_Syokuin_Rirekisyo_DDS "resource/texture/UI/MainScreen_Syokuin/MainScreen_Syokuin_Rirekisyo.DDS"
#endif // !RESOURCE_MainScreen_Syokuin_Rirekisyo_DDS
#ifndef RESOURCE_MainScreen_Syokuin_Rirekisyo_Gacha_DDS
#define RESOURCE_MainScreen_Syokuin_Rirekisyo_Gacha_DDS "resource/texture/UI/MainScreen_Syokuin/MainScreen_Syokuin_Rirekisyo_Gacha.DDS"
#endif // !RESOURCE_MainScreen_Syokuin_Rirekisyo_Gacha_DDS
#ifndef RESOURCE_MainScreen_Syokuin_Seikaku_DDS
#define RESOURCE_MainScreen_Syokuin_Seikaku_DDS "resource/texture/UI/MainScreen_Syokuin/MainScreen_Syokuin_Seikaku.DDS"
#endif // !RESOURCE_MainScreen_Syokuin_Seikaku_DDS
#ifndef RESOURCE_MainScreen_Syokuin_Tokuchou_DDS
#define RESOURCE_MainScreen_Syokuin_Tokuchou_DDS "resource/texture/UI/MainScreen_Syokuin/MainScreen_Syokuin_Tokuchou.DDS"
#endif // !RESOURCE_MainScreen_Syokuin_Tokuchou_DDS
#ifndef RESOURCE_MainScreen_Syokuin_Type_DDS
#define RESOURCE_MainScreen_Syokuin_Type_DDS "resource/texture/UI/MainScreen_Syokuin/MainScreen_Syokuin_Type.DDS"
#endif // !RESOURCE_MainScreen_Syokuin_Type_DDS
#ifndef RESOURCE_MainScreen_Syokuni_Name_DDS
#define RESOURCE_MainScreen_Syokuni_Name_DDS "resource/texture/UI/MainScreen_Syokuin/MainScreen_Syokuni_Name.DDS"
#endif // !RESOURCE_MainScreen_Syokuni_Name_DDS
#ifndef RESOURCE_MainScreen_st_DDS
#define RESOURCE_MainScreen_st_DDS "resource/texture/UI/Strike/MainScreen_st.DDS"
#endif // !RESOURCE_MainScreen_st_DDS
#ifndef RESOURCE_strike_DDS
#define RESOURCE_strike_DDS "resource/texture/UI/Strike/strike.DDS"
#endif // !RESOURCE_strike_DDS
#ifndef RESOURCE_Syuutyuusen_DDS
#define RESOURCE_Syuutyuusen_DDS "resource/texture/UI/Strike/Syuutyuusen.DDS"
#endif // !RESOURCE_Syuutyuusen_DDS
#ifndef RESOURCE_TitleScreen_Data_DDS
#define RESOURCE_TitleScreen_Data_DDS "resource/texture/UI/Title/TitleScreen_Data.DDS"
#endif // !RESOURCE_TitleScreen_Data_DDS
#ifndef RESOURCE_TitleScreen_Decide_DDS
#define RESOURCE_TitleScreen_Decide_DDS "resource/texture/UI/Title/TitleScreen_Decide.DDS"
#endif // !RESOURCE_TitleScreen_Decide_DDS
#ifndef RESOURCE_TitleScreen_End_DDS
#define RESOURCE_TitleScreen_End_DDS "resource/texture/UI/Title/TitleScreen_End.DDS"
#endif // !RESOURCE_TitleScreen_End_DDS
#ifndef RESOURCE_TitleScreen_Level_DDS
#define RESOURCE_TitleScreen_Level_DDS "resource/texture/UI/Title/TitleScreen_Level.DDS"
#endif // !RESOURCE_TitleScreen_Level_DDS
#ifndef RESOURCE_TitleScreen_map_DDS
#define RESOURCE_TitleScreen_map_DDS "resource/texture/UI/Title/TitleScreen_map.DDS"
#endif // !RESOURCE_TitleScreen_map_DDS
#ifndef RESOURCE_TitleScreen_Push_DDS
#define RESOURCE_TitleScreen_Push_DDS "resource/texture/UI/Title/TitleScreen_Push.DDS"
#endif // !RESOURCE_TitleScreen_Push_DDS
#ifndef RESOURCE_TitleScreen_Select_DDS
#define RESOURCE_TitleScreen_Select_DDS "resource/texture/UI/Title/TitleScreen_Select.DDS"
#endif // !RESOURCE_TitleScreen_Select_DDS
#ifndef RESOURCE_TitleScreen_Tutorial_DDS
#define RESOURCE_TitleScreen_Tutorial_DDS "resource/texture/UI/Title/TitleScreen_Tutorial.DDS"
#endif // !RESOURCE_TitleScreen_Tutorial_DDS
#ifndef RESOURCE_Title_Re_DDS
#define RESOURCE_Title_Re_DDS "resource/texture/UI/Title/Title_Re.DDS"
#endif // !RESOURCE_Title_Re_DDS
#ifndef RESOURCE_MainScreen_Operation_Company_Date_tab_DDS
#define RESOURCE_MainScreen_Operation_Company_Date_tab_DDS "resource/texture/UI/setumei_UI/MainScreen_Operation_Company_Date_tab.DDS"
#endif // !RESOURCE_MainScreen_Operation_Company_Date_tab_DDS
#ifndef RESOURCE_MainScreen_Operation_Syokuin_Date_tab_DDS
#define RESOURCE_MainScreen_Operation_Syokuin_Date_tab_DDS "resource/texture/UI/setumei_UI/MainScreen_Operation_Syokuin_Date_tab.DDS"
#endif // !RESOURCE_MainScreen_Operation_Syokuin_Date_tab_DDS
#ifndef RESOURCE_MainScreen_Operation_Tenkin_DDS
#define RESOURCE_MainScreen_Operation_Tenkin_DDS "resource/texture/UI/setumei_UI/MainScreen_Operation_Tenkin.DDS"
#endif // !RESOURCE_MainScreen_Operation_Tenkin_DDS
#ifndef RESOURCE_Aus_map_color_01_DDS
#define RESOURCE_Aus_map_color_01_DDS "resource/3DModel/Australia/Aus_map_color_01.DDS"
#endif // !RESOURCE_Aus_map_color_01_DDS
#ifndef RESOURCE_new_buillding_color_04_DDS
#define RESOURCE_new_buillding_color_04_DDS "resource/3DModel/Building/new_buillding_color_04.DDS"
#endif // !RESOURCE_new_buillding_color_04_DDS
#ifndef RESOURCE_new_buillding_color_05_DDS
#define RESOURCE_new_buillding_color_05_DDS "resource/3DModel/Building/new_buillding_color_05.DDS"
#endif // !RESOURCE_new_buillding_color_05_DDS
#ifndef RESOURCE_new_buillding_newcolor_01_DDS
#define RESOURCE_new_buillding_newcolor_01_DDS "resource/3DModel/Building/new_buillding_newcolor_01.DDS"
#endif // !RESOURCE_new_buillding_newcolor_01_DDS
#ifndef RESOURCE_new_buillding_newcolor_02_DDS
#define RESOURCE_new_buillding_newcolor_02_DDS "resource/3DModel/Building/new_buillding_newcolor_02.DDS"
#endif // !RESOURCE_new_buillding_newcolor_02_DDS
#ifndef RESOURCE_new_buillding_newcolor_03_DDS
#define RESOURCE_new_buillding_newcolor_03_DDS "resource/3DModel/Building/new_buillding_newcolor_03.DDS"
#endif // !RESOURCE_new_buillding_newcolor_03_DDS
#ifndef RESOURCE_USA_buillding_color_DDS
#define RESOURCE_USA_buillding_color_DDS "resource/3DModel/Building/USA_buillding_color.DDS"
#endif // !RESOURCE_USA_buillding_color_DDS
#ifndef RESOURCE_Hawaii_map_color_DDS
#define RESOURCE_Hawaii_map_color_DDS "resource/3DModel/Hawaii/Hawaii_map_color.DDS"
#endif // !RESOURCE_Hawaii_map_color_DDS
#ifndef RESOURCE_japan_map_color_DDS
#define RESOURCE_japan_map_color_DDS "resource/3DModel/Japan/japan_map_color.DDS"
#endif // !RESOURCE_japan_map_color_DDS
#ifndef RESOURCE_USA_map_color_02_DDS
#define RESOURCE_USA_map_color_02_DDS "resource/3DModel/USA/USA_map_color_02.DDS"
#endif // !RESOURCE_USA_map_color_02_DDS
#ifndef RESOURCE_film_DDS
#define RESOURCE_film_DDS "resource/texture/Main_Screen_v3/film.DDS"
#endif // !RESOURCE_film_DDS
#ifndef RESOURCE_ICO_Hoshi1_DDS
#define RESOURCE_ICO_Hoshi1_DDS "resource/texture/Main_Screen_v3/ICO_Hoshi1.DDS"
#endif // !RESOURCE_ICO_Hoshi1_DDS
#ifndef RESOURCE_ICO_Hoshi2_DDS
#define RESOURCE_ICO_Hoshi2_DDS "resource/texture/Main_Screen_v3/ICO_Hoshi2.DDS"
#endif // !RESOURCE_ICO_Hoshi2_DDS
#ifndef RESOURCE_ICO_Hoshi3_DDS
#define RESOURCE_ICO_Hoshi3_DDS "resource/texture/Main_Screen_v3/ICO_Hoshi3.DDS"
#endif // !RESOURCE_ICO_Hoshi3_DDS
#ifndef RESOURCE_ICO_Hoshi4_DDS
#define RESOURCE_ICO_Hoshi4_DDS "resource/texture/Main_Screen_v3/ICO_Hoshi4.DDS"
#endif // !RESOURCE_ICO_Hoshi4_DDS
#ifndef RESOURCE_ICO_Hoshi5_DDS
#define RESOURCE_ICO_Hoshi5_DDS "resource/texture/Main_Screen_v3/ICO_Hoshi5.DDS"
#endif // !RESOURCE_ICO_Hoshi5_DDS
#ifndef RESOURCE_ICO_Player_DDS
#define RESOURCE_ICO_Player_DDS "resource/texture/Main_Screen_v3/ICO_Player.DDS"
#endif // !RESOURCE_ICO_Player_DDS
#ifndef RESOURCE_MainScreen_Kaiko_DDS
#define RESOURCE_MainScreen_Kaiko_DDS "resource/texture/Main_Screen_v3/MainScreen_Kaiko.DDS"
#endif // !RESOURCE_MainScreen_Kaiko_DDS
#ifndef RESOURCE_MainScreen_syoumeisyashin_DDS
#define RESOURCE_MainScreen_syoumeisyashin_DDS "resource/texture/Main_Screen_v3/MainScreen_syoumeisyashin.DDS"
#endif // !RESOURCE_MainScreen_syoumeisyashin_DDS
#ifndef RESOURCE_MainScreen_Tenkin_DDS
#define RESOURCE_MainScreen_Tenkin_DDS "resource/texture/Main_Screen_v3/MainScreen_Tenkin.DDS"
#endif // !RESOURCE_MainScreen_Tenkin_DDS
#ifndef RESOURCE_Main_Screen_Gage_DDS
#define RESOURCE_Main_Screen_Gage_DDS "resource/texture/Main_Screen_v3/Main_Screen_Gage.DDS"
#endif // !RESOURCE_Main_Screen_Gage_DDS
#ifndef RESOURCE_Main_Screen_MainUI_DDS
#define RESOURCE_Main_Screen_MainUI_DDS "resource/texture/Main_Screen_v3/Main_Screen_MainUI.DDS"
#endif // !RESOURCE_Main_Screen_MainUI_DDS
#ifndef RESOURCE_Main_Screen_MainUI_BG_DDS
#define RESOURCE_Main_Screen_MainUI_BG_DDS "resource/texture/Main_Screen_v3/Main_Screen_MainUI_BG.DDS"
#endif // !RESOURCE_Main_Screen_MainUI_BG_DDS
#ifndef RESOURCE_Main_Screen_MainUI_Gauge_DDS
#define RESOURCE_Main_Screen_MainUI_Gauge_DDS "resource/texture/Main_Screen_v3/Main_Screen_MainUI_Gauge.DDS"
#endif // !RESOURCE_Main_Screen_MainUI_Gauge_DDS
#ifndef RESOURCE_TitleScreen_LOGO_DDS
#define RESOURCE_TitleScreen_LOGO_DDS "resource/texture/Main_Screen_v3/TitleScreen_LOGO.DDS"
#endif // !RESOURCE_TitleScreen_LOGO_DDS
#ifndef RESOURCE_Board_Bonbei_DDS
#define RESOURCE_Board_Bonbei_DDS "resource/texture/Main_Screen_v3/Board/Australia/Board_Bonbei.DDS"
#endif // !RESOURCE_Board_Bonbei_DDS
#ifndef RESOURCE_Board_fita_DDS
#define RESOURCE_Board_fita_DDS "resource/texture/Main_Screen_v3/Board/Australia/Board_fita.DDS"
#endif // !RESOURCE_Board_fita_DDS
#ifndef RESOURCE_Board_Girubi_DDS
#define RESOURCE_Board_Girubi_DDS "resource/texture/Main_Screen_v3/Board/Australia/Board_Girubi.DDS"
#endif // !RESOURCE_Board_Girubi_DDS
#ifndef RESOURCE_Board_Godon_DDS
#define RESOURCE_Board_Godon_DDS "resource/texture/Main_Screen_v3/Board/Australia/Board_Godon.DDS"
#endif // !RESOURCE_Board_Godon_DDS
#ifndef RESOURCE_Board_Hiru_DDS
#define RESOURCE_Board_Hiru_DDS "resource/texture/Main_Screen_v3/Board/Australia/Board_Hiru.DDS"
#endif // !RESOURCE_Board_Hiru_DDS
#ifndef RESOURCE_Board_Sirokko_DDS
#define RESOURCE_Board_Sirokko_DDS "resource/texture/Main_Screen_v3/Board/Australia/Board_Sirokko.DDS"
#endif // !RESOURCE_Board_Sirokko_DDS
#ifndef RESOURCE_Board_Tankare_DDS
#define RESOURCE_Board_Tankare_DDS "resource/texture/Main_Screen_v3/Board/Australia/Board_Tankare.DDS"
#endif // !RESOURCE_Board_Tankare_DDS
#ifndef RESOURCE_Board_Jinikku_DDS
#define RESOURCE_Board_Jinikku_DDS "resource/texture/Main_Screen_v3/Board/Hawaii/Board_Jinikku.DDS"
#endif // !RESOURCE_Board_Jinikku_DDS
#ifndef RESOURCE_Board_Myuru_DDS
#define RESOURCE_Board_Myuru_DDS "resource/texture/Main_Screen_v3/Board/Hawaii/Board_Myuru.DDS"
#endif // !RESOURCE_Board_Myuru_DDS
#ifndef RESOURCE_Board_Soruteli_DDS
#define RESOURCE_Board_Soruteli_DDS "resource/texture/Main_Screen_v3/Board/Hawaii/Board_Soruteli.DDS"
#endif // !RESOURCE_Board_Soruteli_DDS
#ifndef RESOURCE_Board_Stein_DDS
#define RESOURCE_Board_Stein_DDS "resource/texture/Main_Screen_v3/Board/Hawaii/Board_Stein.DDS"
#endif // !RESOURCE_Board_Stein_DDS
#ifndef RESOURCE_Board_Bon_DDS
#define RESOURCE_Board_Bon_DDS "resource/texture/Main_Screen_v3/Board/Japan/Board_Bon.DDS"
#endif // !RESOURCE_Board_Bon_DDS
#ifndef RESOURCE_Board_Hakkai_DDS
#define RESOURCE_Board_Hakkai_DDS "resource/texture/Main_Screen_v3/Board/Japan/Board_Hakkai.DDS"
#endif // !RESOURCE_Board_Hakkai_DDS
#ifndef RESOURCE_Board_Hikawa_DDS
#define RESOURCE_Board_Hikawa_DDS "resource/texture/Main_Screen_v3/Board/Japan/Board_Hikawa.DDS"
#endif // !RESOURCE_Board_Hikawa_DDS
#ifndef RESOURCE_Board_Kokushi_DDS
#define RESOURCE_Board_Kokushi_DDS "resource/texture/Main_Screen_v3/Board/Japan/Board_Kokushi.DDS"
#endif // !RESOURCE_Board_Kokushi_DDS
#ifndef RESOURCE_Board_NIto_DDS
#define RESOURCE_Board_NIto_DDS "resource/texture/Main_Screen_v3/Board/Japan/Board_NIto.DDS"
#endif // !RESOURCE_Board_NIto_DDS
#ifndef RESOURCE_Board_Shinano_DDS
#define RESOURCE_Board_Shinano_DDS "resource/texture/Main_Screen_v3/Board/Japan/Board_Shinano.DDS"
#endif // !RESOURCE_Board_Shinano_DDS
#ifndef RESOURCE_Board_Syouku_DDS
#define RESOURCE_Board_Syouku_DDS "resource/texture/Main_Screen_v3/Board/Japan/Board_Syouku.DDS"
#endif // !RESOURCE_Board_Syouku_DDS
#ifndef RESOURCE_Board_Yoshino_DDS
#define RESOURCE_Board_Yoshino_DDS "resource/texture/Main_Screen_v3/Board/Japan/Board_Yoshino.DDS"
#endif // !RESOURCE_Board_Yoshino_DDS
#ifndef RESOURCE_Board_Barenta_DDS
#define RESOURCE_Board_Barenta_DDS "resource/texture/Main_Screen_v3/Board/USA/Board_Barenta.DDS"
#endif // !RESOURCE_Board_Barenta_DDS
#ifndef RESOURCE_Board_Bimusu_DDS
#define RESOURCE_Board_Bimusu_DDS "resource/texture/Main_Screen_v3/Board/USA/Board_Bimusu.DDS"
#endif // !RESOURCE_Board_Bimusu_DDS
#ifndef RESOURCE_Board_Hapa_DDS
#define RESOURCE_Board_Hapa_DDS "resource/texture/Main_Screen_v3/Board/USA/Board_Hapa.DDS"
#endif // !RESOURCE_Board_Hapa_DDS
#ifndef RESOURCE_Board_Henesu_DDS
#define RESOURCE_Board_Henesu_DDS "resource/texture/Main_Screen_v3/Board/USA/Board_Henesu.DDS"
#endif // !RESOURCE_Board_Henesu_DDS
#ifndef RESOURCE_Board_Hosu_DDS
#define RESOURCE_Board_Hosu_DDS "resource/texture/Main_Screen_v3/Board/USA/Board_Hosu.DDS"
#endif // !RESOURCE_Board_Hosu_DDS
#ifndef RESOURCE_Board_Jakku_DDS
#define RESOURCE_Board_Jakku_DDS "resource/texture/Main_Screen_v3/Board/USA/Board_Jakku.DDS"
#endif // !RESOURCE_Board_Jakku_DDS
#ifndef RESOURCE_Board_Tomateli_DDS
#define RESOURCE_Board_Tomateli_DDS "resource/texture/Main_Screen_v3/Board/USA/Board_Tomateli.DDS"
#endif // !RESOURCE_Board_Tomateli_DDS
#ifndef RESOURCE_Board_Toruma_DDS
#define RESOURCE_Board_Toruma_DDS "resource/texture/Main_Screen_v3/Board/USA/Board_Toruma.DDS"
#endif // !RESOURCE_Board_Toruma_DDS
#ifndef RESOURCE_Board_Wazesu_DDS
#define RESOURCE_Board_Wazesu_DDS "resource/texture/Main_Screen_v3/Board/USA/Board_Wazesu.DDS"
#endif // !RESOURCE_Board_Wazesu_DDS
#ifndef RESOURCE_Board_Woka_DDS
#define RESOURCE_Board_Woka_DDS "resource/texture/Main_Screen_v3/Board/USA/Board_Woka.DDS"
#endif // !RESOURCE_Board_Woka_DDS
#ifndef RESOURCE_color_MainScreen_Company_name_aust_DDS
#define RESOURCE_color_MainScreen_Company_name_aust_DDS "resource/texture/Main_Screen_v3/Name/color_MainScreen_Company_name_aust.DDS"
#endif // !RESOURCE_color_MainScreen_Company_name_aust_DDS
#ifndef RESOURCE_color_MainScreen_Company_name_hawaii_DDS
#define RESOURCE_color_MainScreen_Company_name_hawaii_DDS "resource/texture/Main_Screen_v3/Name/color_MainScreen_Company_name_hawaii.DDS"
#endif // !RESOURCE_color_MainScreen_Company_name_hawaii_DDS
#ifndef RESOURCE_color_MainScreen_Company_name_jpn_DDS
#define RESOURCE_color_MainScreen_Company_name_jpn_DDS "resource/texture/Main_Screen_v3/Name/color_MainScreen_Company_name_jpn.DDS"
#endif // !RESOURCE_color_MainScreen_Company_name_jpn_DDS
#ifndef RESOURCE_color_MainScreen_Company_name_USA_DDS
#define RESOURCE_color_MainScreen_Company_name_USA_DDS "resource/texture/Main_Screen_v3/Name/color_MainScreen_Company_name_USA.DDS"
#endif // !RESOURCE_color_MainScreen_Company_name_USA_DDS
#ifndef RESOURCE_Main_Screen_Count_Serihu_DDS
#define RESOURCE_Main_Screen_Count_Serihu_DDS "resource/texture/Main_Screen_v3/Serifu/Main_Screen_Count_Serihu.DDS"
#endif // !RESOURCE_Main_Screen_Count_Serihu_DDS
#ifndef RESOURCE_Serifu_Hoshi1_DDS
#define RESOURCE_Serifu_Hoshi1_DDS "resource/texture/Main_Screen_v3/Serifu/Hoshi1/Serifu_Hoshi1.DDS"
#endif // !RESOURCE_Serifu_Hoshi1_DDS
#ifndef RESOURCE_Serifu_Hoshi1_1_DDS
#define RESOURCE_Serifu_Hoshi1_1_DDS "resource/texture/Main_Screen_v3/Serifu/Hoshi1/Serifu_Hoshi1_1.DDS"
#endif // !RESOURCE_Serifu_Hoshi1_1_DDS
#ifndef RESOURCE_Serifu_Hoshi1_2_DDS
#define RESOURCE_Serifu_Hoshi1_2_DDS "resource/texture/Main_Screen_v3/Serifu/Hoshi1/Serifu_Hoshi1_2.DDS"
#endif // !RESOURCE_Serifu_Hoshi1_2_DDS
#ifndef RESOURCE_Serifu_Hoshi1_3_DDS
#define RESOURCE_Serifu_Hoshi1_3_DDS "resource/texture/Main_Screen_v3/Serifu/Hoshi1/Serifu_Hoshi1_3.DDS"
#endif // !RESOURCE_Serifu_Hoshi1_3_DDS
#ifndef RESOURCE_Serifu_Hoshi1_Lose_DDS
#define RESOURCE_Serifu_Hoshi1_Lose_DDS "resource/texture/Main_Screen_v3/Serifu/Hoshi1/Serifu_Hoshi1_Lose.DDS"
#endif // !RESOURCE_Serifu_Hoshi1_Lose_DDS
#ifndef RESOURCE_Serifu_Hoshi1_Win_DDS
#define RESOURCE_Serifu_Hoshi1_Win_DDS "resource/texture/Main_Screen_v3/Serifu/Hoshi1/Serifu_Hoshi1_Win.DDS"
#endif // !RESOURCE_Serifu_Hoshi1_Win_DDS
#ifndef RESOURCE_Serifu_Hoshi2_DDS
#define RESOURCE_Serifu_Hoshi2_DDS "resource/texture/Main_Screen_v3/Serifu/Hoshi2/Serifu_Hoshi2.DDS"
#endif // !RESOURCE_Serifu_Hoshi2_DDS
#ifndef RESOURCE_Serifu_Hoshi2_1_DDS
#define RESOURCE_Serifu_Hoshi2_1_DDS "resource/texture/Main_Screen_v3/Serifu/Hoshi2/Serifu_Hoshi2_1.DDS"
#endif // !RESOURCE_Serifu_Hoshi2_1_DDS
#ifndef RESOURCE_Serifu_Hoshi2_2_DDS
#define RESOURCE_Serifu_Hoshi2_2_DDS "resource/texture/Main_Screen_v3/Serifu/Hoshi2/Serifu_Hoshi2_2.DDS"
#endif // !RESOURCE_Serifu_Hoshi2_2_DDS
#ifndef RESOURCE_Serifu_Hoshi2_3_DDS
#define RESOURCE_Serifu_Hoshi2_3_DDS "resource/texture/Main_Screen_v3/Serifu/Hoshi2/Serifu_Hoshi2_3.DDS"
#endif // !RESOURCE_Serifu_Hoshi2_3_DDS
#ifndef RESOURCE_Serifu_Hoshi2_Lose_DDS
#define RESOURCE_Serifu_Hoshi2_Lose_DDS "resource/texture/Main_Screen_v3/Serifu/Hoshi2/Serifu_Hoshi2_Lose.DDS"
#endif // !RESOURCE_Serifu_Hoshi2_Lose_DDS
#ifndef RESOURCE_Serifu_Hoshi2_Win_DDS
#define RESOURCE_Serifu_Hoshi2_Win_DDS "resource/texture/Main_Screen_v3/Serifu/Hoshi2/Serifu_Hoshi2_Win.DDS"
#endif // !RESOURCE_Serifu_Hoshi2_Win_DDS
#ifndef RESOURCE_Serifu_Hoshi3_DDS
#define RESOURCE_Serifu_Hoshi3_DDS "resource/texture/Main_Screen_v3/Serifu/Hoshi3/Serifu_Hoshi3.DDS"
#endif // !RESOURCE_Serifu_Hoshi3_DDS
#ifndef RESOURCE_Serifu_Hoshi3_1_DDS
#define RESOURCE_Serifu_Hoshi3_1_DDS "resource/texture/Main_Screen_v3/Serifu/Hoshi3/Serifu_Hoshi3_1.DDS"
#endif // !RESOURCE_Serifu_Hoshi3_1_DDS
#ifndef RESOURCE_Serifu_Hoshi3_2_DDS
#define RESOURCE_Serifu_Hoshi3_2_DDS "resource/texture/Main_Screen_v3/Serifu/Hoshi3/Serifu_Hoshi3_2.DDS"
#endif // !RESOURCE_Serifu_Hoshi3_2_DDS
#ifndef RESOURCE_Serifu_Hoshi3_3_DDS
#define RESOURCE_Serifu_Hoshi3_3_DDS "resource/texture/Main_Screen_v3/Serifu/Hoshi3/Serifu_Hoshi3_3.DDS"
#endif // !RESOURCE_Serifu_Hoshi3_3_DDS
#ifndef RESOURCE_Serifu_Hoshi3_Lose_DDS
#define RESOURCE_Serifu_Hoshi3_Lose_DDS "resource/texture/Main_Screen_v3/Serifu/Hoshi3/Serifu_Hoshi3_Lose.DDS"
#endif // !RESOURCE_Serifu_Hoshi3_Lose_DDS
#ifndef RESOURCE_Serifu_Hoshi3_Win_DDS
#define RESOURCE_Serifu_Hoshi3_Win_DDS "resource/texture/Main_Screen_v3/Serifu/Hoshi3/Serifu_Hoshi3_Win.DDS"
#endif // !RESOURCE_Serifu_Hoshi3_Win_DDS
#ifndef RESOURCE_Serifu_Hoshi4_DDS
#define RESOURCE_Serifu_Hoshi4_DDS "resource/texture/Main_Screen_v3/Serifu/Hoshi4/Serifu_Hoshi4.DDS"
#endif // !RESOURCE_Serifu_Hoshi4_DDS
#ifndef RESOURCE_Serifu_Hoshi4_1_DDS
#define RESOURCE_Serifu_Hoshi4_1_DDS "resource/texture/Main_Screen_v3/Serifu/Hoshi4/Serifu_Hoshi4_1.DDS"
#endif // !RESOURCE_Serifu_Hoshi4_1_DDS
#ifndef RESOURCE_Serifu_Hoshi4_2_DDS
#define RESOURCE_Serifu_Hoshi4_2_DDS "resource/texture/Main_Screen_v3/Serifu/Hoshi4/Serifu_Hoshi4_2.DDS"
#endif // !RESOURCE_Serifu_Hoshi4_2_DDS
#ifndef RESOURCE_Serifu_Hoshi4_3_DDS
#define RESOURCE_Serifu_Hoshi4_3_DDS "resource/texture/Main_Screen_v3/Serifu/Hoshi4/Serifu_Hoshi4_3.DDS"
#endif // !RESOURCE_Serifu_Hoshi4_3_DDS
#ifndef RESOURCE_Serifu_Hoshi4_Lose_DDS
#define RESOURCE_Serifu_Hoshi4_Lose_DDS "resource/texture/Main_Screen_v3/Serifu/Hoshi4/Serifu_Hoshi4_Lose.DDS"
#endif // !RESOURCE_Serifu_Hoshi4_Lose_DDS
#ifndef RESOURCE_Serifu_Hoshi4_Win_DDS
#define RESOURCE_Serifu_Hoshi4_Win_DDS "resource/texture/Main_Screen_v3/Serifu/Hoshi4/Serifu_Hoshi4_Win.DDS"
#endif // !RESOURCE_Serifu_Hoshi4_Win_DDS
#ifndef RESOURCE_Serifu_Hoshi5_DDS
#define RESOURCE_Serifu_Hoshi5_DDS "resource/texture/Main_Screen_v3/Serifu/Hoshi5/Serifu_Hoshi5.DDS"
#endif // !RESOURCE_Serifu_Hoshi5_DDS
#ifndef RESOURCE_Serifu_Hoshi5_1_DDS
#define RESOURCE_Serifu_Hoshi5_1_DDS "resource/texture/Main_Screen_v3/Serifu/Hoshi5/Serifu_Hoshi5_1.DDS"
#endif // !RESOURCE_Serifu_Hoshi5_1_DDS
#ifndef RESOURCE_Serifu_Hoshi5_2_DDS
#define RESOURCE_Serifu_Hoshi5_2_DDS "resource/texture/Main_Screen_v3/Serifu/Hoshi5/Serifu_Hoshi5_2.DDS"
#endif // !RESOURCE_Serifu_Hoshi5_2_DDS
#ifndef RESOURCE_Serifu_Hoshi5_3_DDS
#define RESOURCE_Serifu_Hoshi5_3_DDS "resource/texture/Main_Screen_v3/Serifu/Hoshi5/Serifu_Hoshi5_3.DDS"
#endif // !RESOURCE_Serifu_Hoshi5_3_DDS
#ifndef RESOURCE_Serifu_Hoshi5_Lose_DDS
#define RESOURCE_Serifu_Hoshi5_Lose_DDS "resource/texture/Main_Screen_v3/Serifu/Hoshi5/Serifu_Hoshi5_Lose.DDS"
#endif // !RESOURCE_Serifu_Hoshi5_Lose_DDS
#ifndef RESOURCE_Serifu_Hoshi5_Win_DDS
#define RESOURCE_Serifu_Hoshi5_Win_DDS "resource/texture/Main_Screen_v3/Serifu/Hoshi5/Serifu_Hoshi5_Win.DDS"
#endif // !RESOURCE_Serifu_Hoshi5_Win_DDS
#ifndef RESOURCE_Company_display_DDS
#define RESOURCE_Company_display_DDS "resource/texture/display/Company_display.DDS"
#endif // !RESOURCE_Company_display_DDS
#ifndef RESOURCE_display_DDS
#define RESOURCE_display_DDS "resource/texture/display/display.DDS"
#endif // !RESOURCE_display_DDS
#ifndef RESOURCE_Rirekisyo_display_DDS
#define RESOURCE_Rirekisyo_display_DDS "resource/texture/display/Rirekisyo_display.DDS"
#endif // !RESOURCE_Rirekisyo_display_DDS
#ifndef RESOURCE_TransferAnimation_display_DDS
#define RESOURCE_TransferAnimation_display_DDS "resource/texture/display/TransferAnimation_display.DDS"
#endif // !RESOURCE_TransferAnimation_display_DDS
#ifndef RESOURCE_TenkinCutin_001_DDS
#define RESOURCE_TenkinCutin_001_DDS "resource/texture/Main_Screen_v3/Sinful_image/TenkinCutin_001.DDS"
#endif // !RESOURCE_TenkinCutin_001_DDS
#ifndef RESOURCE_TenkinCutin_002_DDS
#define RESOURCE_TenkinCutin_002_DDS "resource/texture/Main_Screen_v3/Sinful_image/TenkinCutin_002.DDS"
#endif // !RESOURCE_TenkinCutin_002_DDS
#ifndef RESOURCE_TenkinCutin_003_DDS
#define RESOURCE_TenkinCutin_003_DDS "resource/texture/Main_Screen_v3/Sinful_image/TenkinCutin_003.DDS"
#endif // !RESOURCE_TenkinCutin_003_DDS
#ifndef RESOURCE_TenkinCutin_004_DDS
#define RESOURCE_TenkinCutin_004_DDS "resource/texture/Main_Screen_v3/Sinful_image/TenkinCutin_004.DDS"
#endif // !RESOURCE_TenkinCutin_004_DDS
#ifndef RESOURCE_TenkinCutin_005_DDS
#define RESOURCE_TenkinCutin_005_DDS "resource/texture/Main_Screen_v3/Sinful_image/TenkinCutin_005.DDS"
#endif // !RESOURCE_TenkinCutin_005_DDS
#ifndef RESOURCE_TenkinCutin_006_DDS
#define RESOURCE_TenkinCutin_006_DDS "resource/texture/Main_Screen_v3/Sinful_image/TenkinCutin_006.DDS"
#endif // !RESOURCE_TenkinCutin_006_DDS
#ifndef RESOURCE_TenkinCutin_007_DDS
#define RESOURCE_TenkinCutin_007_DDS "resource/texture/Main_Screen_v3/Sinful_image/TenkinCutin_007.DDS"
#endif // !RESOURCE_TenkinCutin_007_DDS
#ifndef RESOURCE_TenkinCutin_008_DDS
#define RESOURCE_TenkinCutin_008_DDS "resource/texture/Main_Screen_v3/Sinful_image/TenkinCutin_008.DDS"
#endif // !RESOURCE_TenkinCutin_008_DDS
#ifndef RESOURCE_TenkinCutin_009_DDS
#define RESOURCE_TenkinCutin_009_DDS "resource/texture/Main_Screen_v3/Sinful_image/TenkinCutin_009.DDS"
#endif // !RESOURCE_TenkinCutin_009_DDS
#ifndef RESOURCE_TenkinCutin_010_DDS
#define RESOURCE_TenkinCutin_010_DDS "resource/texture/Main_Screen_v3/Sinful_image/TenkinCutin_010.DDS"
#endif // !RESOURCE_TenkinCutin_010_DDS
#ifndef RESOURCE_TenkinCutin_011_DDS
#define RESOURCE_TenkinCutin_011_DDS "resource/texture/Main_Screen_v3/Sinful_image/TenkinCutin_011.DDS"
#endif // !RESOURCE_TenkinCutin_011_DDS
#ifndef RESOURCE_TenkinCutin_012_DDS
#define RESOURCE_TenkinCutin_012_DDS "resource/texture/Main_Screen_v3/Sinful_image/TenkinCutin_012.DDS"
#endif // !RESOURCE_TenkinCutin_012_DDS
#ifndef RESOURCE_TenkinCutin_013_DDS
#define RESOURCE_TenkinCutin_013_DDS "resource/texture/Main_Screen_v3/Sinful_image/TenkinCutin_013.DDS"
#endif // !RESOURCE_TenkinCutin_013_DDS
#ifndef RESOURCE_TenkinCutin_014_DDS
#define RESOURCE_TenkinCutin_014_DDS "resource/texture/Main_Screen_v3/Sinful_image/TenkinCutin_014.DDS"
#endif // !RESOURCE_TenkinCutin_014_DDS
#ifndef RESOURCE_TenkinCutin_015_DDS
#define RESOURCE_TenkinCutin_015_DDS "resource/texture/Main_Screen_v3/Sinful_image/TenkinCutin_015.DDS"
#endif // !RESOURCE_TenkinCutin_015_DDS
#ifndef RESOURCE_TenkinCutin_016_DDS
#define RESOURCE_TenkinCutin_016_DDS "resource/texture/Main_Screen_v3/Sinful_image/TenkinCutin_016.DDS"
#endif // !RESOURCE_TenkinCutin_016_DDS
#ifndef RESOURCE_TenkinCutin_017_DDS
#define RESOURCE_TenkinCutin_017_DDS "resource/texture/Main_Screen_v3/Sinful_image/TenkinCutin_017.DDS"
#endif // !RESOURCE_TenkinCutin_017_DDS
#ifndef RESOURCE_TenkinCutin_018_DDS
#define RESOURCE_TenkinCutin_018_DDS "resource/texture/Main_Screen_v3/Sinful_image/TenkinCutin_018.DDS"
#endif // !RESOURCE_TenkinCutin_018_DDS
#ifndef RESOURCE_TenkinCutin_019_DDS
#define RESOURCE_TenkinCutin_019_DDS "resource/texture/Main_Screen_v3/Sinful_image/TenkinCutin_019.DDS"
#endif // !RESOURCE_TenkinCutin_019_DDS
#ifndef RESOURCE_Tutorial01_DDS
#define RESOURCE_Tutorial01_DDS "resource/texture/Tutorial/Tutorial01.DDS"
#endif // !RESOURCE_Tutorial01_DDS
#ifndef RESOURCE_Tutorial02_DDS
#define RESOURCE_Tutorial02_DDS "resource/texture/Tutorial/Tutorial02.DDS"
#endif // !RESOURCE_Tutorial02_DDS
#ifndef RESOURCE_Tutorial03_DDS
#define RESOURCE_Tutorial03_DDS "resource/texture/Tutorial/Tutorial03.DDS"
#endif // !RESOURCE_Tutorial03_DDS
#ifndef RESOURCE_Tutorial04_DDS
#define RESOURCE_Tutorial04_DDS "resource/texture/Tutorial/Tutorial04.DDS"
#endif // !RESOURCE_Tutorial04_DDS
#ifndef RESOURCE_Tutorial05_DDS
#define RESOURCE_Tutorial05_DDS "resource/texture/Tutorial/Tutorial05.DDS"
#endif // !RESOURCE_Tutorial05_DDS
#ifndef RESOURCE_Tutorial06_DDS
#define RESOURCE_Tutorial06_DDS "resource/texture/Tutorial/Tutorial06.DDS"
#endif // !RESOURCE_Tutorial06_DDS
#ifndef RESOURCE_Tutorial07_DDS
#define RESOURCE_Tutorial07_DDS "resource/texture/Tutorial/Tutorial07.DDS"
#endif // !RESOURCE_Tutorial07_DDS
#ifndef RESOURCE_Tutorial08_DDS
#define RESOURCE_Tutorial08_DDS "resource/texture/Tutorial/Tutorial08.DDS"
#endif // !RESOURCE_Tutorial08_DDS
#ifndef RESOURCE_Tutorial09_DDS
#define RESOURCE_Tutorial09_DDS "resource/texture/Tutorial/Tutorial09.DDS"
#endif // !RESOURCE_Tutorial09_DDS
#ifndef RESOURCE_Tutorial10_DDS
#define RESOURCE_Tutorial10_DDS "resource/texture/Tutorial/Tutorial10.DDS"
#endif // !RESOURCE_Tutorial10_DDS
#ifndef RESOURCE_Tutorial11_DDS
#define RESOURCE_Tutorial11_DDS "resource/texture/Tutorial/Tutorial11.DDS"
#endif // !RESOURCE_Tutorial11_DDS
#ifndef RESOURCE_Tutorial12_DDS
#define RESOURCE_Tutorial12_DDS "resource/texture/Tutorial/Tutorial12.DDS"
#endif // !RESOURCE_Tutorial12_DDS
#ifndef RESOURCE_Tutorial13_DDS
#define RESOURCE_Tutorial13_DDS "resource/texture/Tutorial/Tutorial13.DDS"
#endif // !RESOURCE_Tutorial13_DDS
#ifndef RESOURCE_Tutorial14_DDS
#define RESOURCE_Tutorial14_DDS "resource/texture/Tutorial/Tutorial14.DDS"
#endif // !RESOURCE_Tutorial14_DDS
#ifndef RESOURCE_Tutorial15_DDS
#define RESOURCE_Tutorial15_DDS "resource/texture/Tutorial/Tutorial15.DDS"
#endif // !RESOURCE_Tutorial15_DDS

//=============================================================================
// 拡張子 [.x]
//=============================================================================

#ifndef RESOURCE_Aus_map_x
#define RESOURCE_Aus_map_x "resource/3DModel/Australia/Aus_map.x"
#endif // !RESOURCE_Aus_map_x
#ifndef RESOURCE_build_x
#define RESOURCE_build_x "resource/3DModel/Building/build.x"
#endif // !RESOURCE_build_x
#ifndef RESOURCE_Sub_build_x
#define RESOURCE_Sub_build_x "resource/3DModel/Building/Sub_build.x"
#endif // !RESOURCE_Sub_build_x
#ifndef RESOURCE_Hawaii_map_x
#define RESOURCE_Hawaii_map_x "resource/3DModel/Hawaii/Hawaii_map.x"
#endif // !RESOURCE_Hawaii_map_x
#ifndef RESOURCE_japan_3d_map_x
#define RESOURCE_japan_3d_map_x "resource/3DModel/Japan/japan_3d_map.x"
#endif // !RESOURCE_japan_3d_map_x
#ifndef RESOURCE_USA_map_x
#define RESOURCE_USA_map_x "resource/3DModel/USA/USA_map.x"
#endif // !RESOURCE_USA_map_x
#ifndef RESOURCE_koma_x
#define RESOURCE_koma_x "resource/3DModel/koma/koma.x"
#endif // !RESOURCE_koma_x
#ifndef RESOURCE_keiro_x
#define RESOURCE_keiro_x "resource/3DModel/keiro/keiro.x"
#endif // !RESOURCE_keiro_x

//=============================================================================
// 拡張子 [.bin]
//=============================================================================

#ifndef RESOURCE_Australia_bin
#define RESOURCE_Australia_bin "resource/data/Australia.bin"
#endif // !RESOURCE_Australia_bin
#ifndef RESOURCE_Hawaii_bin
#define RESOURCE_Hawaii_bin "resource/data/Hawaii.bin"
#endif // !RESOURCE_Hawaii_bin
#ifndef RESOURCE_Japan_bin
#define RESOURCE_Japan_bin "resource/data/Japan.bin"
#endif // !RESOURCE_Japan_bin
#ifndef RESOURCE_USA_bin
#define RESOURCE_USA_bin "resource/data/USA.bin"
#endif // !RESOURCE_USA_bin
#ifndef RESOURCE_log_bin
#define RESOURCE_log_bin "resource/system/log.bin"
#endif // !RESOURCE_log_bin
#ifndef RESOURCE_result_bin
#define RESOURCE_result_bin "resource/system/result.bin"
#endif // !RESOURCE_result_bin
#ifndef RESOURCE_select_bin
#define RESOURCE_select_bin "resource/system/select.bin"
#endif // !RESOURCE_select_bin
#ifndef RESOURCE_stage_bin
#define RESOURCE_stage_bin "resource/system/stage.bin"
#endif // !RESOURCE_stage_bin

//=============================================================================
// 拡張子 [.csv]
//=============================================================================

#ifndef RESOURCE_ディレクトリ指定_csv
#define RESOURCE_ディレクトリ指定_csv "/ディレクトリ指定.csv"
#endif // !RESOURCE_ディレクトリ指定_csv
#ifndef RESOURCE_拡張子指定_csv
#define RESOURCE_拡張子指定_csv "/拡張子指定.csv"
#endif // !RESOURCE_拡張子指定_csv
#ifndef RESOURCE_Australia_csv
#define RESOURCE_Australia_csv "resource/data/Australia.csv"
#endif // !RESOURCE_Australia_csv
#ifndef RESOURCE_Country_csv
#define RESOURCE_Country_csv "resource/data/Country.csv"
#endif // !RESOURCE_Country_csv
#ifndef RESOURCE_Hawaii_csv
#define RESOURCE_Hawaii_csv "resource/data/Hawaii.csv"
#endif // !RESOURCE_Hawaii_csv
#ifndef RESOURCE_Japan_csv
#define RESOURCE_Japan_csv "resource/data/Japan.csv"
#endif // !RESOURCE_Japan_csv
#ifndef RESOURCE_USA_csv
#define RESOURCE_USA_csv "resource/data/USA.csv"
#endif // !RESOURCE_USA_csv
#ifndef RESOURCE_SoundList_csv
#define RESOURCE_SoundList_csv "resource/Sound/SoundList.csv"
#endif // !RESOURCE_SoundList_csv

//=============================================================================
// 拡張子 [.wav]
//=============================================================================

#ifndef RESOURCE_GameCon_America_wav
#define RESOURCE_GameCon_America_wav "resource/Sound/BGM/GameCon_America.wav"
#endif // !RESOURCE_GameCon_America_wav
#ifndef RESOURCE_GameCon_Hawaii_wav
#define RESOURCE_GameCon_Hawaii_wav "resource/Sound/BGM/GameCon_Hawaii.wav"
#endif // !RESOURCE_GameCon_Hawaii_wav
#ifndef RESOURCE_GameCon_JP_wav
#define RESOURCE_GameCon_JP_wav "resource/Sound/BGM/GameCon_JP.wav"
#endif // !RESOURCE_GameCon_JP_wav
#ifndef RESOURCE_GameCon_JPnew_wav
#define RESOURCE_GameCon_JPnew_wav "resource/Sound/BGM/GameCon_JPnew.wav"
#endif // !RESOURCE_GameCon_JPnew_wav
#ifndef RESOURCE_GameCon_lose_wav
#define RESOURCE_GameCon_lose_wav "resource/Sound/BGM/GameCon_lose.wav"
#endif // !RESOURCE_GameCon_lose_wav
#ifndef RESOURCE_GameCon_Raria_wav
#define RESOURCE_GameCon_Raria_wav "resource/Sound/BGM/GameCon_Raria.wav"
#endif // !RESOURCE_GameCon_Raria_wav
#ifndef RESOURCE_GameCon_title_wav
#define RESOURCE_GameCon_title_wav "resource/Sound/BGM/GameCon_title.wav"
#endif // !RESOURCE_GameCon_title_wav
#ifndef RESOURCE_GameCon_win_wav
#define RESOURCE_GameCon_win_wav "resource/Sound/BGM/GameCon_win.wav"
#endif // !RESOURCE_GameCon_win_wav
#ifndef RESOURCE_R_ページ_wav
#define RESOURCE_R_ページ_wav "resource/Sound/SE/R_ページ.wav"
#endif // !RESOURCE_R_ページ_wav
#ifndef RESOURCE_R_引き出し_wav
#define RESOURCE_R_引き出し_wav "resource/Sound/SE/R_引き出し.wav"
#endif // !RESOURCE_R_引き出し_wav
#ifndef RESOURCE_ストライキ_wav
#define RESOURCE_ストライキ_wav "resource/Sound/SE/ストライキ.wav"
#endif // !RESOURCE_ストライキ_wav
#ifndef RESOURCE_ターン_wav
#define RESOURCE_ターン_wav "resource/Sound/SE/ターン.wav"
#endif // !RESOURCE_ターン_wav
#ifndef RESOURCE_戻る_wav
#define RESOURCE_戻る_wav "resource/Sound/SE/戻る.wav"
#endif // !RESOURCE_戻る_wav
#ifndef RESOURCE_決定_wav
#define RESOURCE_決定_wav "resource/Sound/SE/決定.wav"
#endif // !RESOURCE_決定_wav
#ifndef RESOURCE_移動_wav
#define RESOURCE_移動_wav "resource/Sound/SE/移動.wav"
#endif // !RESOURCE_移動_wav

//=============================================================================
// 拡張子 [.bmp]
//=============================================================================

#ifndef RESOURCE_TitleLogo_bmp
#define RESOURCE_TitleLogo_bmp "/TitleLogo.bmp"
#endif // !RESOURCE_TitleLogo_bmp

//=============================================================================
// 拡張子 [.ico]
//=============================================================================

#ifndef RESOURCE_ICO_3_ico
#define RESOURCE_ICO_3_ico "/ICO_3.ico"
#endif // !RESOURCE_ICO_3_ico
