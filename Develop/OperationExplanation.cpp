//==========================================================================
// 操作説明/制御[OperationExplanation.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "OperationExplanation.h"
#include "Game_Syokuin.h"
#include "Game_Company_Date.h"
#include "GameCamera.h"
#include "GamePlayerEmployee.h"
#include "Game_Start.h"
#include "Game_Sound.h"
#include "resource_list.h"

//==========================================================================
// 演算子を与える
//==========================================================================
template<> struct enum_system::calculation<COperationExplanation::E_Key> : std::true_type {};

//==========================================================================
// 初期化
bool COperationExplanation::Init(void)
{
    const char * p_tex[] = {
        RESOURCE_MainScreen_Operation_Company_Date_tab_DDS,
        RESOURCE_MainScreen_Operation_Syokuin_Date_tab_DDS,
        RESOURCE_MainScreen_Operation_Tenkin_DDS,
    };
    auto *p_obj = this->_2DObject()->Create();

    // テクスチャ読み込み
    if (this->_2DPolygon()->Init(p_tex, this->Helper()->Sizeof_(p_tex), true))
    {
        return true;
    }

    this->_2DPolygon()->ObjectInput(p_obj);

    p_obj->Init(0);
    p_obj->SetPos((float)this->GetWinSize().m_Width - this->_2DPolygon()->GetTexSize(0)->w, (float)this->GetWinSize().m_Height - this->_2DPolygon()->GetTexSize(0)->h);

    this->XInput()->Init(1);

    this->m_control_company = this->GetObjects(ID::Polygon2D, Type::Game_Company_Date);
    this->m_control_syokuin = this->GetObjects(ID::Polygon2D, Type::Game_Syokuin);
    this->m_camera = this->GetObjects(ID::Camera, Type::Game_Camera);
    this->m_PlayerEmployee = this->GetObjects(ID::Default, Type::Game_GamePlayerEmployee);
    this->m_start_script = this->GetObjects(ID::Polygon2D, Type::Game_Start_Object);
    this->m_Game_Sound = this->GetObjects(ID::Default, Type::Game_Game_Sound);

    auto * p_company = (CGame_Company_Date*)this->m_control_company;
    if (p_company != nullptr)
    {
        p_company->Game_Company_Data_Open();
    }

    return false;
}

//==========================================================================
// 解放
void COperationExplanation::Uninit(void)
{
}

//==========================================================================
// 更新
void COperationExplanation::Update(void)
{
    if (this->m_control_company == nullptr)
    {
        this->m_control_company = this->GetObjects(ID::Polygon2D, Type::Game_Company_Date);
    }
    if (this->m_control_syokuin == nullptr)
    {
        this->m_control_syokuin = this->GetObjects(ID::Polygon2D, Type::Game_Syokuin);
    }
    if (this->m_camera == nullptr)
    {
        this->m_camera = this->GetObjects(ID::Camera, Type::Game_Camera);
    }
    if (this->m_PlayerEmployee == nullptr)
    {
        this->m_PlayerEmployee = this->GetObjects(ID::Default, Type::Game_GamePlayerEmployee);
    }
    if (this->m_Game_Sound == nullptr)
    {
        this->m_Game_Sound = this->GetObjects(ID::Default, Type::Game_Game_Sound);
    }
    if (this->m_start_script == nullptr)
    {
        this->m_start_script = this->GetObjects(ID::Polygon2D, Type::Game_Start_Object);
        auto * p_company = (CGame_Company_Date*)this->m_control_company;
        if (p_company != nullptr)
        {
            p_company->Game_Company_Data_Open();
        }
    }

    // オブジェクトがあるとき
    if (this->m_start_script != nullptr)
    {
        auto * p_start = (CGame_Start*)this->m_start_script;
        auto * p_sound = (CGame_Sound*)this->m_Game_Sound;

        if (p_start->Start())
        {
            auto * p_company = (CGame_Company_Date*)this->m_control_company;
            auto * p_syokuin = (CGame_Syokuin*)this->m_control_syokuin;
            auto * p_camera = (CGameCamera*)this->m_camera;
            auto * p_PlayerEmployee = (CGamePlayerEmployee*)this->m_PlayerEmployee;

            // コントローラーチェック
            if (this->XInput()->Check(0))
            {
                // タブ切り替え
                if (this->XInput()->Trigger(CXInput::EButton::B, 0))
                {
                    // オブジェクトがあるとき
                    if (p_company != nullptr&&p_syokuin != nullptr)
                    {
                        // 加算
                        ++this->m_control_tab_key;

                        // シシャタブ表示/シャインタブ非表示
                        if (E_Key::Company == this->m_control_tab_key)
                        {
                            if (p_sound != nullptr)
                            {
                                p_sound->tab_se();
                            }
                            this->m_control_menu_key = this->m_control_tab_key;
                            p_company->Game_Company_Data_Open();
                            p_syokuin->Game_Syokuin_Data_Clause();
                        }

                        // シシャタブ非表示/シャインタブ表示
                        if (E_Key::Employee == this->m_control_tab_key)
                        {
                            if (p_sound != nullptr)
                            {
                                p_sound->tab_se();
                            }

                            this->m_control_menu_key = this->m_control_tab_key;
                            p_syokuin->Game_Syokuin_Data_Open();
                            p_company->Game_Company_Data_Clause();
                        }

                        // 転勤UIと一致/超えた場合
                        // シシャタブ非表示/シャインタブ非表示
                        if (E_Key::Transfer <= this->m_control_tab_key)
                        {
                            // Companyに戻す
                            if (p_sound != nullptr)
                            {
                                p_sound->tab_se();
                            }

                            this->m_control_tab_key = E_Key::Company;
                            this->m_control_menu_key = this->m_control_tab_key;
                            p_company->Game_Company_Data_Open();
                            p_syokuin->Game_Syokuin_Data_Clause();
                        }
                    }
                }

                // 転勤対象を選んだ
                if (this->XInput()->Trigger(CXInput::EButton::X, 0))
                {
                    if (p_camera != nullptr&&p_PlayerEmployee != nullptr)
                    {
                        // 転勤キーが選択済み/転勤キーが押された時のIDではないとき
                        if (this->m_control_menu_key == E_Key::Transfer&&this->m_old_serect_id != p_camera->GetSerectID())
                        {
                            // メニュー情報をタブ情報に合わせる
                            this->m_control_menu_key = this->m_control_tab_key;
                            this->m_old_serect_id = -1;
                            this->m_lock = false;
                        }

                        // 転勤キーが未選択
                        else if (this->m_control_menu_key != E_Key::Transfer&& this->Key_Syokuin() == true && p_PlayerEmployee->lookData() != nullptr)
                        {
                            this->m_old_serect_id = p_camera->GetSerectID();
                            this->m_control_menu_key = E_Key::Transfer;
                            this->m_lock = true;
                        };
                    }
                }
            }

            if (this->m_lock == true)
            {
                this->m_control_menu_key = E_Key::Transfer;
            }
            this->_2DObject()->Get(0)->Setindex((int)this->m_control_menu_key);
            this->XInput()->Update();
        }
    }
    this->_2DPolygon()->Update();
}

//==========================================================================
// 描画
void COperationExplanation::Draw(void)
{
    this->_2DPolygon()->Draw();
}
