//==========================================================================
// GameCamera[GameCamera.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _GameCamera_H_
#define _GameCamera_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "FieldEditor.h"

//==========================================================================
//
// class  : CGameCamera
// Content: カメラオブジェクト
//
//==========================================================================
class CGameCamera : public CObject
{
public:
    enum class E_Type
    {
        Neet, // ニート
        Kuroneko, // クロネコ
        VIPtan, // VIPタン
    };
public:
    CGameCamera() :CObject(CObject::ID::Camera) { 
        this->SetType(CObject::Type::Game_Camera);
        this->m_ObjectSerect = 0;
        this->m_key_zoom = false;
        this->m_min_limit_pos = 0.0f;
        this->m_vew_pos = 0.0f;
        this->m_LT_RT = false;
    }
    ~CGameCamera() {
        this->m_CObjectData.Create();
    }
    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;

    // 選択ID
    int GetSerectID(void) {
        return m_ObjectSerect;
    }
private:
    // カメラの移動先の選択
    void MoveSerect(void);

    // カメラの移動処理
    void Move(void);

    // ズーム/IN/OUT
    void Zoom_IN_OUT(void);
private:
    CObjectVectorManager<CFieldData>m_CObjectData; // フィールド管理コンテナ
    int m_ObjectSerect; // オブジェクト選択
    bool m_key_zoom; // ズーム切り替え
    float m_min_limit_pos; // 始点との距離の制御キー
    float m_vew_pos; // カメラ座標
    const float m_max_limit_pos = 10.0f;
    bool m_LT_RT;
};

#endif // !_GameCamera_H_
