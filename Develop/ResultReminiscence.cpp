#include "ResultReminiscence.h"
#include "resource_list.h"
#include "resource_link.hpp"

#include "Screen.h"


CResultReminiscence::CResultReminiscence() : CObject(ID::Polygon2D)
{
    this->SetType(Type::Result_ResultReminiscence);
    this->m_key = false;
}


CResultReminiscence::~CResultReminiscence()
{
}

//==========================================================================
// 初期化
bool CResultReminiscence::Init(void)
{
    int __container = 0;
    this->m_sin_list[__container] = RESOURCE_TenkinCutin_001_DDS;
    __container++;
    this->m_sin_list[__container] = RESOURCE_TenkinCutin_002_DDS;
    __container++;
    this->m_sin_list[__container] = RESOURCE_TenkinCutin_003_DDS;
    __container++;
    this->m_sin_list[__container] = RESOURCE_TenkinCutin_004_DDS;
    __container++;
    this->m_sin_list[__container] = RESOURCE_TenkinCutin_005_DDS;
    __container++;
    this->m_sin_list[__container] = RESOURCE_TenkinCutin_006_DDS;
    __container++;
    this->m_sin_list[__container] = RESOURCE_TenkinCutin_007_DDS;
    __container++;
    this->m_sin_list[__container] = RESOURCE_TenkinCutin_008_DDS;
    __container++;
    this->m_sin_list[__container] = RESOURCE_TenkinCutin_009_DDS;
    __container++;
    this->m_sin_list[__container] = RESOURCE_TenkinCutin_010_DDS;
    __container++;
    this->m_sin_list[__container] = RESOURCE_TenkinCutin_011_DDS;
    __container++;
    this->m_sin_list[__container] = RESOURCE_TenkinCutin_012_DDS;
    __container++;
    this->m_sin_list[__container] = RESOURCE_TenkinCutin_013_DDS;
    __container++;
    this->m_sin_list[__container] = RESOURCE_TenkinCutin_014_DDS;
    __container++;
    this->m_sin_list[__container] = RESOURCE_TenkinCutin_015_DDS;
    __container++;
    this->m_sin_list[__container] = RESOURCE_TenkinCutin_016_DDS;
    __container++;
    this->m_sin_list[__container] = RESOURCE_TenkinCutin_017_DDS;
    __container++;
    this->m_sin_list[__container] = RESOURCE_TenkinCutin_018_DDS;
    __container++;
    this->m_sin_list[__container] = RESOURCE_TenkinCutin_019_DDS;
    __container++;
    this->m_sin_list[__container] = RESOURCE_film_DDS;

    // ここは、ログから取得した、データを元に初期化を行います。
    this->Load();

    __container = 0;
    // ログを元にデータを登録
    for (auto itr_ = this->m_log.begin(); itr_ != this->m_log.end(); ++itr_)
    {
        auto itr = this->m_sin_list.find((*itr_));

        // パス情報が存在するか判定
        if (itr != this->m_sin_list.end())
        {
            if (this->_2DPolygon()->Init(RESOURCE_film_DDS, true))
            {
                return true;
            }
            // 存在するパスの読み込み
            if (this->_2DPolygon()->Init(itr->second.c_str(), true))
            {
                return true;
            }
            auto * p_obj = this->_2DObject()->Create();
            p_obj->Init(__container);
            this->_2DPolygon()->ObjectInput(p_obj);
            __container++;
            p_obj = this->_2DObject()->Create();
            p_obj->Init(__container);
            this->_2DPolygon()->ObjectInput(p_obj);
            __container++;
        }
    }

    float f_pos = (float)this->GetWinSize().m_Width;
    for (int i = 0; i < this->_2DObject()->Size(); i++)
    {
        auto * p_obj = this->_2DObject()->Get(i);

        p_obj->SetPos({ f_pos,0.0f });
        p_obj->SetColor({ 255,255,255,0 });
        i++;
        p_obj = this->_2DObject()->Get(i);
        p_obj->SetPos({ f_pos,0.0f });
        p_obj->SetColor({ 255,255,255,0 });
        f_pos += (float)this->GetWinSize().m_Width;
    }

    this->m_color = 255;
    this->m_color.a = 0;

    return false;
}

//==========================================================================
// 解放
void CResultReminiscence::Uninit(void)
{
    this->m_log.clear();
}

//==========================================================================
// 更新
void CResultReminiscence::Update(void)
{
    if (this->m_key == true)
    {
        auto * p_objct = this->_2DObject()->Get(this->_2DObject()->Size() - 1);

        if (this->m_color.a != 255)
        {
            this->m_color.a += 2;
            if (255<this->m_color.a)
            {
                if (p_objct == nullptr)
                {
                    CScreen::screenchange(CScreen::scenelist_t::Title);
                }
                this->m_color.a = 255;
            }
        }

        for (int i = 0; i < this->_2DObject()->Size(); i++)
        {
            auto * p_obj = this->_2DObject()->Get(i);

            p_obj->SetXPlus(-20.0f);
            p_obj->SetColor(this->m_color);
        }
        if (p_objct != nullptr)
        {
            if (p_objct->GetPos()->x<this->GetWinSize().m_Width)
            {
                CScreen::screenchange(CScreen::scenelist_t::Title);
            }
        }
    }

    this->_2DPolygon()->Update();
}

//==========================================================================
// 描画
void CResultReminiscence::Draw(void)
{
    this->_2DPolygon()->Draw();
}

//==========================================================================
// 開始
void CResultReminiscence::Start(void)
{
    this->m_key = true;
}

//==========================================================================
// 読み込み
void CResultReminiscence::Load(void)
{
    std::string str_link = resource_link::data::system_file;

    this->m_log.clear();

    str_link += "/";
    str_link += resource_link::data::system_file_log;
    FILE *pFile = fopen(str_link.c_str(), "rb");
    if (pFile)
    {
        int n_size = 0;
        int n_push = 0;
        fread(&n_size, sizeof(n_size), 1, pFile);

        for (int i = 0; i < n_size; i++)
        {
            fread(&n_push, sizeof(n_push), 1, pFile);
            this->m_log.push_back(n_push);
        }

        fclose(pFile);
    }
}
