//==========================================================================
// 制作用シーン[make.h]
// author: tatsuya ogawa
//==========================================================================
#include "make.h"


CMake::CMake()
{
}

CMake::~CMake()
{
}

bool CMake::Init(void)
{
    return CObject::InitAll();
}

void CMake::Uninit(void)
{
	CObject::ReleaseAll();
}

void CMake::Update(void)
{
	CObject::UpdateAll();
}

void CMake::Draw(void)
{
	CObject::DrawAll();
}
