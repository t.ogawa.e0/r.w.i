//==========================================================================
// EmployeeParamSystem[EmployeeParamSystem.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "min_max.hpp"

//==========================================================================
//
// class  : CEmployeeParamSystem
// Content: 社員パラメータ生成システム
//
//==========================================================================
class CEmployeeParamSystem
{
private:
    // データ
    template <typename _Ty>
    class CData_ {
    public:
        CData_() {
            this->id = -1;
        }
        ~CData_() {}
        CData_(int _id, _Ty _data) {
            this->id = _id;
            this->data = _data;
        }
    public:
        int id;
        _Ty data;
    };
public:
    // パラメータセット
    class CParam
    {
    public:
        CParam() {
            this->m_ico_textureID = -1;
            this->m_name_textureID = -1;
            this->m_ico_frameID = -1;
        }
        ~CParam() {}
        // 年齢のゲッター
        const CData_<std::string> * GetAge(void)const {
            return &this->m_age;
        }
        // 国籍のゲッター
        const CData_<std::string> * GetCountry(void)const {
            return &this->m_country;
        }
        // 性格のゲッター
        const CData_<std::string> * GetPersonality(void)const {
            return &this->m_personality;
        }
        // 名前のゲッター
        const CData_ <std::string> * GetName(void)const {
            return &this->m_Name;
        }
        // タイプのゲッター
        const CData_ <std::string> * GetTypeID(void)const {
            return &this->m_TypeID;
        }
        // 特徴のゲッター
        const CData_ <std::string> * GetChaID(void)const {
            return &this->m_ChaID;
        }
        // パートナーのゲッター
        const CData_<std::string> * GetPartner(void)const {
            return &this->m_Partner;
        }
        // アイコンのテクスチャID
        int GetIcoTextureID(void) {
            return this->m_ico_textureID;
        }
        // アイコンのテクスチャframe
        int GetIcoFrameID(void) {
            return this->m_ico_frameID;
        }
        // 名前のテクスチャID
        int GetNameTextureID(void) {
            return this->m_name_textureID;
        }
        // 年齢
        void SetAge(const CData_<std::string> & _this) {
            this->m_age = _this;
        }
        // 国籍
        void SetCountry(const CData_<std::string> & _this) {
            this->m_country = _this;
        }
        // 性格
        void SetPersonality(const CData_<std::string> & _this) {
            this->m_personality = _this;
        }
        // 名前のセット
        void SetName(const CData_ <std::string> & _this) {
            this->m_Name = _this;
        }
        // タイプのセット
        void SetTypeID(const CData_ <std::string> & _this) {
            this->m_TypeID = _this;
        }
        // 特徴のセット
        void SetChaID(const CData_ <std::string> & _this) {
            this->m_ChaID = _this;
        }
        // パートナーのセット
        void SetPartner(const CData_<std::string> & _this) {
            this->m_Partner = _this;
        }
        // アイコンテクスチャIDのセット
        void SetIcoTextureID(const int _this) {
            this->m_ico_textureID = _this;
        }
        // アイコンテクスチャのframeセット
        void SetIcoFrameID(const int _this) {
            this->m_ico_frameID = _this;
        }
        // 名前テクスチャIDのセット
        void SetNameTextureID(const int _this) {
            this->m_name_textureID = _this;
        }
    private:
        CData_<std::string> m_age; // 年齢
        CData_<std::string> m_country; // 国籍
        CData_<std::string> m_personality; // 性格
        CData_<std::string> m_Name; // 名前
        CData_<std::string> m_TypeID; // タイプのID
        CData_<std::string> m_ChaID; // 特徴のID
        CData_<std::string> m_Partner; // パートナー判定
        int m_ico_textureID; // テクスチャID 
        int m_ico_frameID; // フレームのID
        int m_name_textureID; // テクスチャID
    };
protected:
    CEmployeeParamSystem() {
        //=============================================================================
        // 乱数生成
        //=============================================================================
        C_min_max<std::string> inputter; // 確率表設計用
        std::random_device rnd; // 非決定的な乱数生成器を生成
        std::mt19937 mt(rnd());  //  メルセンヌ・ツイスタの32ビット版、引数は初期シード値
        float next = 1.0f; // 次の確立
        this->m_mt = mt;

        //==========================================================================
        // タイプの定義
        //==========================================================================
        this->m_type["新卒"] = 0;
        this->m_type["中途"] = 1;
        this->m_type["派遣"] = 2;
        //==========================================================================
        // タイプの確立表
        //==========================================================================
        next = 1.0f;
        next = inputter.set(next, 20.0f, "新卒");
        this->m_type_table.emplace_back(inputter);
        next = inputter.set(next, 20.0f, "中途");
        this->m_type_table.emplace_back(inputter);
        next = inputter.set(next, 20.0f, "派遣");
        this->m_type_table.emplace_back(inputter);
        this->m_type_rand = std::uniform_real_distribution<float>(1.0f, next);
        //==========================================================================
        // 名前の設定
        //==========================================================================
        {
            int namecpunt = -1;
            namecpunt++;
            this->m_name[namecpunt] = "クラスィ";
            namecpunt++;
            this->m_name[namecpunt] = "アルマニ";
            namecpunt++;
            this->m_name[namecpunt] = "ツカサ";
            namecpunt++;
            this->m_name[namecpunt] = "ヒロミ";
            namecpunt++;
            this->m_name[namecpunt] = "ホリス";
            namecpunt++;
            this->m_name[namecpunt] = "ジョメル";
            namecpunt++;
            this->m_name[namecpunt] = "サンタナ";
            namecpunt++;
            this->m_name[namecpunt] = "ツバサ";
            namecpunt++;
            this->m_name[namecpunt] = "メグム";
            namecpunt++;
            this->m_name[namecpunt] = "ロビィ";
            namecpunt++;
            this->m_name[namecpunt] = "クレオ";
            namecpunt++;
            this->m_name[namecpunt] = "イブキ";
            namecpunt++;
            this->m_name[namecpunt] = "ナギサ";
            namecpunt++;
            this->m_name[namecpunt] = "ユウヒ";
            namecpunt++;
            this->m_name[namecpunt] = "ルリィ";
            namecpunt++;
            this->m_name[namecpunt] = "ノエル";
            namecpunt++;
            this->m_name[namecpunt] = "カヲル";
            namecpunt++;
            this->m_name[namecpunt] = "ヒナタ";
            namecpunt++;
            this->m_name[namecpunt] = "ユヅキ";
            namecpunt++;
            this->m_name[namecpunt] = "シャリン";
            namecpunt++;
            this->m_name[namecpunt] = "ドリアン";
            namecpunt++;
            this->m_name[namecpunt] = "カズサ";
            namecpunt++;
            this->m_name[namecpunt] = "ユーリ";
            namecpunt++;
            this->m_name[namecpunt] = "ノイシュ";
            namecpunt++;
            this->m_name[namecpunt] = "エリシャ";
            namecpunt++;
            this->m_name[namecpunt] = "オリィ";
            namecpunt++;
            this->m_name[namecpunt] = "シオン";
            namecpunt++;
            this->m_name[namecpunt] = "リョウ";
            namecpunt++;
            this->m_name[namecpunt] = "ミズキ";
            namecpunt++;
            this->m_name[namecpunt] = "バンディ";
            namecpunt++;
            this->m_name[namecpunt] = "アリオン";
            namecpunt++;
            this->m_name[namecpunt] = "スバル";
            namecpunt++;
            this->m_name[namecpunt] = "アヒロ";
            namecpunt++;
            this->m_name[namecpunt] = "エマール";
            namecpunt++;
            this->m_name[namecpunt] = "デヴィン";
            namecpunt++;
            this->m_name[namecpunt] = "エモリー";
            namecpunt++;
            this->m_name[namecpunt] = "チアキ";
            namecpunt++;
            this->m_name[namecpunt] = "ミスト";
            namecpunt++;
            this->m_name[namecpunt] = "レミィ";
            namecpunt++;
            this->m_name[namecpunt] = "キャシェ";
            namecpunt++;
            this->m_name[namecpunt] = "レイリー";
            namecpunt++;
            this->m_name[namecpunt] = "ランディ";
            namecpunt++;
            this->m_name[namecpunt] = "アルト";
            namecpunt++;
            this->m_name[namecpunt] = "アティス";
            namecpunt++;
            this->m_name[namecpunt] = "ジョディ";
            namecpunt++;
            this->m_name[namecpunt] = "ジェシー";
            namecpunt++;
            this->m_name[namecpunt] = "アキラ";
            namecpunt++;
            this->m_name[namecpunt] = "カミーユ";
            namecpunt++;
            this->m_name[namecpunt] = "メリル";
            namecpunt++;
            this->m_name[namecpunt] = "アルコ";
            //==========================================================================
            // 名前の乱数
            //==========================================================================
            this->m_name_rand = std::uniform_int_distribution<int>(0, namecpunt);
        }

        //==========================================================================
        // 年齢
        //==========================================================================
        {
            int n_age = 0;
            this->m_age[n_age] = "20歳";
            n_age++;
            this->m_age[n_age] = "50歳";
            n_age++;
            this->m_age[n_age] = "25歳";
            n_age++;
            this->m_age[n_age] = "55歳";
            n_age++;
            this->m_age[n_age] = "30歳";
            n_age++;
            this->m_age[n_age] = "60歳";
            n_age++;
            this->m_age[n_age] = "35歳";
            n_age++;
            this->m_age[n_age] = "65歳";
            n_age++;
            this->m_age[n_age] = "40歳";
            n_age++;
            this->m_age[n_age] = "ヒミツ";
            n_age++;
            this->m_age[n_age] = "45歳";
            n_age++;
            this->m_age[n_age] = "17歳";
            n_age++;
            this->m_age[n_age] = "3歳";
            this->m_age_rand = std::uniform_int_distribution<int>(0, n_age);
        }
        //==========================================================================
        // 性格
        //==========================================================================
        int personality = 0;
        this->m_personality[personality] = "優しい";
        personality++;
        this->m_personality[personality] = "物静か";
        personality++;
        this->m_personality[personality] = "怒りっぽい";
        personality++;
        this->m_personality[personality] = "のんびり";
        personality++;
        this->m_personality[personality] = "気分屋";
        personality++;
        this->m_personality[personality] = "無し";
        personality++;
        this->m_personality[personality] = "素直";
        personality++;
        this->m_personality[personality] = "まじめ";
        personality++;
        this->m_personality[personality] = "よめん。";
        personality++;
        this->m_personality_rand = std::uniform_int_distribution<int>(0, personality);
        //==========================================================================
        // 国籍
        //==========================================================================
        {
            int country = 0;
            this->m_country[country] = "日本";
            country++;
            this->m_country[country] = "アメリカ";
            country++;
            this->m_country[country] = "イタリア";
            country++;
            this->m_country[country] = "イギリス";
            country++;
            this->m_country[country] = "ブラジル";
            country++;
            this->m_country[country] = "シーランド";
            country++;
            this->m_country[country] = "ロシア";
            country++;
            this->m_country[country] = "月";
            this->m_country_rand = std::uniform_int_distribution<int>(0, country);
        }
        //==========================================================================
        // 特徴の定義
        //==========================================================================
        this->m_cha["高齢"] = 0;
        this->m_cha["没個性"] = 1;
        this->m_cha["ムードメーカー"] = 2;
        this->m_cha["職人気質"] = 3;
        this->m_cha["人工知能"] = 4;
        this->m_cha["病弱"] = 5;
        this->m_cha["旅好き"] = 6;
        this->m_cha["トラブルメーカー"] = 7;
        //==========================================================================
        // 特徴の確立表
        //==========================================================================
        next = 1.0f;
        next = inputter.set(next, 3.0f, "ムードメーカー");
        this->m_cha_table.emplace_back(inputter);
        next = inputter.set(next, 10.0f, "職人気質");
        this->m_cha_table.emplace_back(inputter);
        next = inputter.set(next, 2.0f, "病弱");
        this->m_cha_table.emplace_back(inputter);
        next = inputter.set(next, 10.0f, "高齢");
        this->m_cha_table.emplace_back(inputter);
        next = inputter.set(next, 5.0f, "トラブルメーカー");
        this->m_cha_table.emplace_back(inputter);
        next = inputter.set(next, 1.0f, "人工知能");
        this->m_cha_table.emplace_back(inputter);
        next = inputter.set(next, 5.0f, "旅好き");
        this->m_cha_table.emplace_back(inputter);
        next = inputter.set(next, 50.0f, "なし");
        this->m_cha_table.emplace_back(inputter);
        this->m_cha_rand = std::uniform_real_distribution<float>(1.0f, next);
        //==========================================================================
        // パートナー
        //==========================================================================
        this->m_partner["既婚者"] = true;
        this->m_partner["独身"] = false;
        //==========================================================================
        // パートナーの確率表
        //==========================================================================
        next = 1.0f;
        next = inputter.set(next, 50.0f, "既婚者");
        this->m_partner_table.emplace_back(inputter);
        next = inputter.set(next, 50.0f, "独身");
        this->m_partner_table.emplace_back(inputter);
        this->m_partner_rand = std::uniform_real_distribution<float>(1.0f, next);

        this->m_partner_font[this->m_partner["既婚者"]] = "既婚者";
        this->m_partner_font[this->m_partner["独身"]] = "独身";

        //==========================================================================
        // アイコンの乱数
        //==========================================================================
        this->m_ico_rand = std::uniform_int_distribution<int>(0, 18);
    }
    virtual ~CEmployeeParamSystem() {
        this->m_age.clear();
        this->m_country.clear();
        this->m_personality.clear();
        this->m_type.clear();
        this->m_cha.clear();
        this->m_partner.clear();
        this->m_name.clear();
        this->m_cha_table.clear();
        this->m_partner_table.clear();
        this->m_type_table.clear();
        this->m_partner_font.clear();
    }
    // タイプの生成
    void CreateType(CParam * _this);
    // 年齢の生成
    void CreateAge(CParam * _this);
    // 国籍の生成
    void CreateCountry(CParam * _this);
    // 性格の生成
    void CreatePersonality(CParam * _this);
    // 特徴の生成
    void CreateCha(CParam * _this);
    // パートナーの生成
    void CreatePartner(CParam * _this);
    // アイコン生成
    void CreateICO(CParam * _this);
    // デバッグ用
    void SetText(CParam * _this);
private:
    std::mt19937 m_mt; // 乱数

    std::unordered_map<int, std::string> m_age; // 年齢
    std::uniform_int_distribution<int> m_age_rand; // 年齢の乱数

    std::unordered_map<int, std::string> m_country; // 国籍
    std::uniform_int_distribution<int> m_country_rand; // 国籍の乱数

    std::unordered_map<int, std::string> m_personality; // 性格
    std::uniform_int_distribution<int> m_personality_rand; // 性格の乱数

    std::unordered_map<int, std::string> m_name; // 名前
    std::uniform_int_distribution<int> m_name_rand; // 名前の乱数

    // タイプ
    std::unordered_map<std::string, int> m_type; // タイプ
    std::list<C_min_max<std::string>> m_type_table; // 確率表
    std::uniform_real_distribution<float> m_type_rand; // 乱数

    // 特徴
    std::unordered_map<std::string, int> m_cha; // 特徴
    std::uniform_real_distribution<float> m_cha_rand; // 特徴乱数
    std::list<C_min_max<std::string>> m_cha_table; // 確率表

    // パートナー
    std::unordered_map<std::string, bool> m_partner; // パートナー判定
    std::unordered_map<bool, std::string> m_partner_font; // パートナー判定フォント
    std::list<C_min_max<std::string>> m_partner_table; // 確率表
    std::uniform_real_distribution<float> m_partner_rand; // パートナー乱数

    std::uniform_int_distribution<int> m_ico_rand; // アイコン乱数

    CImGui_Dx9 m_ImGui; // IMGUI
};

using EmployeeParam = CEmployeeParamSystem::CParam;