//==========================================================================
// タイトル画面選択データ管理クラス[TitleData.h]
// author: yoji watanabe
//==========================================================================
#ifndef _TitleData_H_
#define _TitleData_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "Stage.h"
#include "resource_link.hpp"

//==========================================================================
// 定義
//==========================================================================
using Title_Stage_ID = StageData_ID; // ステージID

//==========================================================================
//
// class  : CTitleData
// Content: タイトル画面選択データ管理クラス
//
//==========================================================================
class CTitleData
{
public:
	enum class Enemy_Level_ID
	{
		begin = -1,
		TESHI,
		CUROUS,
		YOSHIDAYA,
		KENG,
		OZAKIGUMI,
		end
	};

	CTitleData() { m_Term = 1; m_EnemyLevel = Enemy_Level_ID::TESHI; m_StageID = Title_Stage_ID::Waneha; };
	~CTitleData() {};

	//セーブ
	void SavaData()
	{
        if (_mkdir(resource_link::data::system_file) == 0) {}

        std::string str_link = resource_link::data::system_file;
        str_link += "/";
        str_link += resource_link::data::system_file_select;
        FILE* fp;
		fopen_s(&fp, str_link.c_str(), "wb");
		fwrite(&m_Term, sizeof(int), 1, fp);
		fwrite(&m_EnemyLevel, sizeof(Enemy_Level_ID), 1, fp);
		fclose(fp);

        std::string strFilename = "";
        strFilename += resource_link::data::system_file;
        strFilename += "/";
        strFilename += resource_link::data::system_file_stage;

        // フォルダ生成
        FILE *pFile = fopen(strFilename.c_str(), "wb");
        if (pFile)
        {
            fwrite(&this->m_StageID, sizeof(this->m_StageID), 1, pFile); // データ数の書き込み
            fclose(pFile);
        }
	};

	//ローダー
	void LoadData()
	{
        // フォルダ生成
        if (_mkdir(resource_link::data::system_file) == 0) {}

        std::string str_link = resource_link::data::system_file;
        str_link += "/";
        str_link += resource_link::data::system_file_select;
        FILE* fp;
		fopen_s(&fp, str_link.c_str(), "rb");
		fread(&m_Term, sizeof(int), 1, fp);
		fread(&m_EnemyLevel, sizeof(Enemy_Level_ID), 1, fp);
		fclose(fp);

        std::string strFilename = "";
        strFilename += resource_link::data::system_file;
        strFilename += "/";
        strFilename += resource_link::data::system_file_stage;
        FILE *pFile = fopen(strFilename.c_str(), "rb");
        if (pFile)
        {
            fread(&m_StageID, sizeof(m_StageID), 1, fp);
            fclose(pFile);
        }
    };

	//任期ロード
	static int LoadTerm()
	{
        std::string str_link = resource_link::data::system_file;
        str_link += "/";
        str_link += resource_link::data::system_file_select;
        CTitleData data;
		FILE* fp;
		fopen_s(&fp, str_link.c_str(), "rb");
		fread(&data.m_Term, sizeof(int), 1, fp);
		fclose(fp);

		return data.m_Term;
	};

	//敵レベルロード
	static Enemy_Level_ID LoadEnemyLevel()
	{
        std::string str_link = resource_link::data::system_file;
        str_link += "/";
        str_link += resource_link::data::system_file_select;
        CTitleData data;
		FILE* fp;
		fopen_s(&fp, str_link.c_str(), "rb");
		fread(&data.m_Term, sizeof(int), 1, fp);
		fread(&data.m_EnemyLevel, sizeof(Enemy_Level_ID), 1, fp);
		fclose(fp);

		return data.m_EnemyLevel;
	};

	//ステージIDロード
	static Title_Stage_ID LoadStageID()
	{
        Title_Stage_ID id = Title_Stage_ID::begin;
        std::string str_link = resource_link::data::system_file;

        str_link += "/";
        str_link += resource_link::data::system_file_stage;
        FILE *pFile = fopen(str_link.c_str(), "rb");
        if (pFile)
        {
            fread(&id, sizeof(id), 1, pFile);
            fclose(pFile);
        }

		return id;
	};

protected:
	//任期
	int m_Term;
	//敵会社レベル
	Enemy_Level_ID m_EnemyLevel;
	//ステージID
	Title_Stage_ID m_StageID;
};

#endif // !_TitleUI_H_