//==========================================================================
// 子会社の処理[Subsidiary.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _Subsidiary_H_
#define _Subsidiary_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "SubsidiaryParam.h"
#include "CharList.h"
#include <string>
#include <vector>

#include "CreateSubsidiary.h"
#include "FieldEditor.h"

//==========================================================================
//
// class  : CSubsidiaryManager
// Content: 子会社のマネージャー
//
//==========================================================================
class CSubsidiaryManager : private CCharList
{
protected:
	//==========================================================================
	//
	// class  : CTransfer
	// Content: 転勤
	//
	//==========================================================================
	class CTransfer
	{
	public:
		CTransfer()
		{
			this->m_this = nullptr;
			this->m_num = 0;
			this->m_relay_point = 0;
		}
		CTransfer(CSubsidiaryParam * _this, int num, int relay_point)
		{
			this->m_this = _this;
			this->m_num = num;
			this->m_relay_point = relay_point;
		}
		~CTransfer() {}
	public:
		CSubsidiaryParam * m_this; // 移動先アドレス
		int m_num; // 人数
		int m_relay_point; // 中継地点
		int m_relay_point_set; // 中継地点数のセット
	};
public:
	CSubsidiaryManager() 
	{
		this->m_playname = "";
		this->m_tag = (CharList)-1;
	}
	~CSubsidiaryManager() 
	{
		this->Release();
	}

	// 解放
	void Release(void)
	{
        this->m_subparam.Release();
		this->ReleaseTransfer();
	}

	// 移動
	void SetTransfer(CSubsidiaryParam * Input, int num, int relay_point);

	// 移動の更新
	void UpdateTransfer(void);

	// 移動の解放
	void ReleaseTransfer(void);

	// 自社の総社員数のゲッター
	int GetMaxNemployee(void);

	// 利益
	int GetAsset(void);

	// テキスト
	void Text(void);

    // 子会社生成
    // pRegion 地域
    // pRegionProfit 賃金
    // pRegionEmployee 社員数
    // pcapital_city 首都判定
    // num データ数
    // tag_ キャラタグ
    void Create(CFieldData * FieldData, int num, CharList tag_);

    // 子会社生成
    // pRegion 地域
    // pRegionProfit 賃金
    // pRegionEmployee 社員数
    // pcapital_city 首都判定
    // tag_ キャラタグ
    void Create(CFieldData & FieldData, CharList tag_);

	// 子会社データのゲッター
	CSubsidiaryParam * GetSub(int num) { return this->m_subparam.Get(num); }

	// 誰の会社かのセッター
	void SetTag(const std::string & name) { this->m_playname = name; }

	// 子会社数のゲッター
	int GetNumSub(void) { return this->m_subparam.Size(); }
protected:
	CTemplates m_temp; // template
	CImGui_Dx9 m_imgui; // imgui
    CObjectVectorManager<CSubsidiaryParam>m_subparam; // 子会社管理
	std::string m_playname; // play側の名前 
	std::list<CTransfer*> m_TransferData; // 移動先のデータ管理
	CharList m_tag; // tag
};

//==========================================================================
//
// class  : CSubsidiary
// Content: 子会社
//
//==========================================================================
class CSubsidiary : public CObject , private CCharList
{
public:
	CSubsidiary() :CObject(CObject::ID::Default) {
        this->m_WorldTime = nullptr;
        this->m_start_script = nullptr;
    }
	~CSubsidiary() {
        this->Uninit();
    }
    // 初期化
	bool Init(void);

	// 解放
	void Uninit(void);

	// 更新
	void Update(void);

	// 描画
	void Draw(void);

	// 子会社のゲッター
	static CSubsidiaryManager * Get(CharList play) { return m_play_sub[(int)play]; }
private:
	static std::vector<CSubsidiaryManager*> m_play_sub; // プレイヤーの子会社群
    void * m_WorldTime; // 時間へのアクセスルート
	int m_numsub; // 子会社数
    void * m_start_script; // スタート制御
};

#endif // !_Subsidiary_H_
