//==========================================================================
// シーン遷移[SceneChange.cpp]
// author: tatuya ogawa
//==========================================================================
#include "SceneChange.h"
#include "make.h"
#include "test.h"
#include "Editor.h"
#include "DataCheck.h"
#include "Title.h"
#include "Home.h"
#include "Result.h"
#include "Game.h"
#include "Music.h"
#include "EmployeeSystem.h"
#include "make.h"

//==========================================================================
// シーンの切り替え
void CSceneManager::ChangeScene(SceneName Name) 
{
	// 解放
	this->Uninit();

	switch (Name)
	{
	case SceneName::NOME:
        CDeviceManager::GetDXDevice()->ErrorMessage("Scene Ellor : -1");
		break;

    case SceneName::Title:
        this->m_pScene = new CTitle;
        break;

	case SceneName::Home:
		this->m_pScene = new CHome;
		break;

    case SceneName::Game:
        this->m_pScene = new CGame;
        break;

    case SceneName::Result:
        this->m_pScene = new CResult;
        break;

	case SceneName::Screen_Saver:
		this->m_pScene = nullptr;
		break;

	case SceneName::Practice:
		this->m_pScene = nullptr;
		break;

	case SceneName::Load:
		this->m_pScene = nullptr;
		break;

    case SceneName::Test:
        this->m_pScene = new CTest;
        break;

    case SceneName::Editor:
        this->m_pScene = new CEditor;
        break;

    case SceneName::EmployeeSystem:
        this->m_pScene = new CEmployeeSystem;
        break;

    case SceneName::DataCheck:
        this->m_pScene = new CDataCheck;
        break;

    case SceneName::Music:
        this->m_pScene = new CMusic;
        break;

    case SceneName::Default:
        this->m_pScene = new CMake;
        break;

	default:
        CDeviceManager::GetDXDevice()->ErrorMessage("Scene Ellor : -1");
		break;
	}
}


//==========================================================================
// 初期化 初期化終了時にtrue
bool CSceneManager::Init(void)
{
    if (this->m_pScene != nullptr)
    {
        return this->m_pScene->Init();
    }
    return true;
}

//==========================================================================
// 解放
void CSceneManager::Uninit(void)
{
    if (this->m_pScene != nullptr)
    {
        this->m_pScene->Uninit();
        delete this->m_pScene;
        this->m_pScene = nullptr;
    }
}

//==========================================================================
// 更新
void CSceneManager::Update(void)
{
    if (this->m_pScene != nullptr)
    {
        this->m_pScene->Update();
    }
}

//==========================================================================
// 描画
void CSceneManager::Draw(void)
{
    if (this->m_pScene != nullptr)
    {
        this->m_pScene->Draw();
    }
}