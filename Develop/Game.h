//==========================================================================
// ゲーム[Game.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _Game_H_
#define _Game_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"
#include "SceneChange.h"
#include "Stage.h"


class CBool4_ss
{
public:
    CBool4_ss() { this->m_1 = this->m_2 = this->m_3 = this->m_4 = false; }
    ~CBool4_ss() {}
public:
    bool m_1, m_2, m_3, m_4;
};

//==========================================================================
//
// class  : CGame
// Content: ゲーム
//
//==========================================================================
class CGame : public CBaseScene
{
private:
public:
    CGame();
    ~CGame();
    // 初期化
    bool Init(void)override;
    // 解放
    void Uninit(void)override;
    // 更新
    void Update(void)override;
    // 描画
    void Draw(void)override;
private:
    void Load(void);
    void Save(void);
private:
    CImGui_Dx9 m_ImGui;
    StageData_ID m_stage;
    CBool4_ss m_bool4;
    CStageData m_stage_data;
};


#endif // !_Game_H_
