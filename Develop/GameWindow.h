//==========================================================================
// ゲームウィンドウ[GameWindow.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _GameWindow_H_
#define _GameWindow_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <stdio.h>
#include "dxlib.h"
#include "Screen.h"

//==========================================================================
//
// class  : CGameWindow
// Content: ゲームウィンドウ
//
//==========================================================================
class CGameWindow
{
private:
	const char *Class_Name = "R.W.I";
	const char *Window_Name = "R.W.I";
private:
	class CWinSize
	{
	public:
		CWinSize()
		{
			this->w = 0;
			this->h = 0;
		};
		CWinSize(int _w, int _h)
		{
			this->w = _w;
			this->h = _h;
		}
		~CWinSize() {}
	public:
		int w;
		int h;
	};
	class CData
	{
	public:
		CData() 
		{
			this->m_serect = 0;
			this->m_key = false;
		};
		CData(int serect, bool key)
		{
			this->m_serect = serect;
			this->m_key = key;
		}
		~CData() {}
	public:
		int m_serect;
		bool m_key;
	};
public:
	CGameWindow() { this->m_WinMood = false; }
	~CGameWindow() {}
	// ウィンドウ生成
	bool Window(WNDCLASSEX & wcex, HINSTANCE hInstance, const CVector2<int> & data, bool Mode);
	// ウィンドウ更新
	int WindowUpdate(WNDCLASSEX &wcex, int nCmdShow);
private:
	// ウィンドウプロシージャ
	static LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
	// ゲームループ
	int GameLoop(HINSTANCE hInstance, HWND hWnd);
	bool Init(HINSTANCE hInstance, HWND hWnd); //初期化
	void Uninit(void);//終了処理
	bool Update(void);//更新
	void Draw(void);//描画
private:
	CScreen m_screne;
    CAspectRatio::data_t m_WinSize; // ウィンドウサイズ
    bool m_WinMood; // ウィンドウモード
};

#endif // !_GameWindow_H_
