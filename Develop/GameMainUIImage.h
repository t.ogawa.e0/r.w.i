//==========================================================================
// メインUIイメージ[GameMainUIImage.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _GameMainUIImage_H_
#define _GameMainUIImage_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CGameMainUIImage
// Content: メインUIイメージ
//
//==========================================================================
class CGameMainUIImage : public CObject
{
public:
    CGameMainUIImage() :CObject(CObject::ID::Polygon2D) {
        this->m_MasterUI = false;
    }
    ~CGameMainUIImage() {}
    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;
private:
    bool m_MasterUI;
};

#endif // !_GameMainUIImage_H_
