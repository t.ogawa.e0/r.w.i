//==========================================================================
//  海[Game_Ocean.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CGame_Ocean
// Content: 海
//
//==========================================================================
class CGame_Ocean : public CObject
{
public:
    CGame_Ocean() : CObject(ID::Mesh) {
        this->m_camera = nullptr;
    }
    ~CGame_Ocean() {}
    // 初期化
    bool Init(void)override;

    // 解放
    void Uninit(void)override;

    // 更新
    void Update(void)override;

    // 描画
    void Draw(void)override;
private:
    void * m_camera; // カメラへのアクセスルート
};
