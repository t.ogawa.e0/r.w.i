//==========================================================================
// 各地域の子会社生成[CreateSubsidiary.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "CreateSubsidiary.h"
#include "RelayPoint.h"

//==========================================================================
// 定数宣言
//==========================================================================
constexpr float fPowor = 0.5f;
constexpr float fScale = 2.0f;

//==========================================================================
// 初期化
bool CCreateSubsidiary::Init(void)
{
    this->m_LimitRegionEmployee = CMinMax(1, 50);
    this->m_LimitRegionProfit = CMinMax(1, 3000);

    this->m_RelayPoint = this->GetObjects(CObject::ID::Xmodel, CObject::Type::Game_RelayPoint);

    return false;
}

//==========================================================================
// 解放
void CCreateSubsidiary::Uninit(void)
{
    this->Release();
}

//==========================================================================
// 更新
void CCreateSubsidiary::Update(void)
{
    if (this->m_RelayPoint == nullptr)
    {
        this->m_RelayPoint = this->GetObjects(CObject::ID::Xmodel, CObject::Type::Game_RelayPoint);
    }

    // 編集モードがアクティブな時
    if (this->m_ActivationKey)
    {
        // 生成
        this->Create();

        // エディット
        this->Edit();

        // 消去内容がある場合消去
        this->Delete();

        // 中継地点のセッティング
        this->SetRelayPoint();
    }
    else
    {
        this->m_bool4_1.reset();
        this->m_bool4_2.reset();
        this->m_bool4_3.reset();
        this->m_bool4_4.reset();
        this->m_ObjePoly.ObjectDelete(this->m_data->m_pos);
        this->m_DataCase.PinpointRelease(this->m_data);
    }
    this->m_ObjePoly.Update();
}

//==========================================================================
// 描画
void CCreateSubsidiary::Draw(void)
{
    // 現在制作中のデータの描画
    if (this->m_bool4_1.b1)
    {
        this->m_data->m_pos.Draw(20, this->GetDirectX9Device());
    }

    if (this->m_Edit != nullptr)
    {
        this->m_Edit->m_pos.Draw(20, this->GetDirectX9Device());
    }

    this->m_ObjePoly.Draw();
}

//==========================================================================
// 座標のセット
void CCreateSubsidiary::SetPos(bool bopen, CFieldData * Input)
{
    // 座標修正キーが有効時
    if (bopen)
    {
        if (this->ImGui()->NewMenu("座標の調整"))
        {
            CFloatCase fpowor;

            this->ImGui()->Text("移動");

            // X軸の移動
            this->XAxisMovement(Input);

            // Y軸の移動
            this->YAxisMovement(Input);

            // Z軸の移動
            this->ZAxisMovement(Input);

            this->ImGui()->Separator();
            this->ImGui()->Text("回転");

            // X軸回転
            fpowor.f4 = this->XAxisRot(Input);

            // Y軸回転
            this->YAxisRot(Input);

            this->ImGui()->Separator();
            this->ImGui()->Text("サイズ");

            // スケーリング
            this->Scaling(Input);

            this->ImGui()->Separator();

            const D3DXVECTOR3 * pos = Input->m_pos.GetMatInfoPos();
            const D3DXVECTOR3 * rot = Input->m_pos.GetVecFront();
            const D3DXVECTOR3 * sca = Input->m_pos.GetMatInfoSca();

            // 各パラメータの描画
            this->ImGui()->Text("座標");
            this->ImGui()->Text("X = %.2f, Y = %.2f , Z = %.2f", pos->x, pos->y, pos->z);
            this->ImGui()->Text("回転");
            this->ImGui()->Text("X = %.2f, Y = %.2f , Z = %.2f", rot->x, rot->y, rot->z);
            this->ImGui()->Text("サイズ");
            this->ImGui()->Text("X = %.2f, Y = %.2f , Z = %.2f", sca->x, sca->y, sca->z);
            this->ImGui()->EndMenu();
        }
    }
}

//==========================================================================
// X軸の移動
float CCreateSubsidiary::XAxisMovement(CFieldData * Input)
{
    float fpo = 0.0f;

    // 階層構造
    if (this->ImGui()->NewTreeNode("移動 : X軸", false))
    {
        // スクロールバー
        if (this->ImGui()->SliderFloat("パワー : X", &fpo, -fPowor, fPowor))
        {
            Input->m_pos.MoveX(fpo);
        }

        // リセットボタン
        if (this->ImGui()->Button("移動 : X軸のリセット"))
        {
            D3DXVECTOR3 Set = *Input->m_pos.GetMatInfoPos();
            Set.x = 0.0f;
            Input->m_pos.SetMatInfoPos(Set);
        }
        this->ImGui()->EndTreeNode();
        this->ImGui()->Separator();
    }

    return fpo;
}

//==========================================================================
// Y軸の移動
float CCreateSubsidiary::YAxisMovement(CFieldData * Input)
{
    float fpo = 0.0f;

    // 階層構造
    if (this->ImGui()->NewTreeNode("移動 : Y軸", false))
    {
        // スクロールバー
        if (this->ImGui()->SliderFloat("パワー : Y", &fpo, -fPowor, fPowor))
        {
            Input->m_pos.MoveY(fpo);
        }

        // リセットボタン
        if (this->ImGui()->Button("移動 : Y軸のリセット"))
        {
            D3DXVECTOR3 Set = *Input->m_pos.GetMatInfoPos();
            Set.y = 0.70f;
            Input->m_pos.SetMatInfoPos(Set);
        }
        this->ImGui()->EndTreeNode();
        this->ImGui()->Separator();
    }

    return fpo;
}

//==========================================================================
// Z軸の移動
float CCreateSubsidiary::ZAxisMovement(CFieldData * Input)
{
    float fpo = 0.0f;

    // 階層構造
    if (this->ImGui()->NewTreeNode("移動 : Z軸", false))
    {
        // スクロールバー
        if (this->ImGui()->SliderFloat("パワー : Z", &fpo, -fPowor, fPowor))
        {
            Input->m_pos.MoveZ(fpo);
        }

        // リセットボタン
        if (this->ImGui()->Button("移動 : Z軸のリセット"))
        {
            D3DXVECTOR3 Set = *Input->m_pos.GetMatInfoPos();
            Set.z = 0.0f;
            Input->m_pos.SetMatInfoPos(Set);
        }
        this->ImGui()->EndTreeNode();
        this->ImGui()->Separator();
    }

    return fpo;
}

//==========================================================================
// X軸回転
float CCreateSubsidiary::XAxisRot(CFieldData * Input)
{
    float fpo = 0.0f;

    // 階層構造
    if (this->ImGui()->NewTreeNode("回転 : X軸", false))
    {
        // リセットボタン
        if (this->ImGui()->SliderFloat("パワー : X", &fpo, -fPowor, fPowor))
        {
            Input->m_pos.RotX(fpo);
        }
        this->ImGui()->EndTreeNode();
        this->ImGui()->Separator();
    }

    return fpo;
}

//==========================================================================
// Y軸回転
float CCreateSubsidiary::YAxisRot(CFieldData * Input)
{
    float fpo = 0.0f;

    // 階層構造
    if (this->ImGui()->NewTreeNode("回転 : Y軸", false))
    {
        // スクリュー
        if (this->ImGui()->SliderFloat("パワー : Y", &fpo, -fPowor, fPowor))
        {
            Input->m_pos.RotY(fpo);
        }

        // リセットボタン
        if (this->ImGui()->Button("回転 : Y軸のリセット"))
        {
            D3DXVECTOR3 Set = D3DXVECTOR3(); // 向きベクトルの記録用
            C3DObject pos = Input->m_pos; // データの複製

            // 向きベクトルの複製
            Set = D3DXVECTOR3(pos.GetVecFront()->x, 0, pos.GetVecFront()->z);

            // 初期化
            Input->m_pos.Init(Input->m_pos.getindex());

            // 複製した向きベクトルの方向に向かせる
            for (int i = 0; i < 50; i++) { Input->m_pos.RotX(Set, 1.0f); }

            // 複製したデータより各パラメータでの上書き
            Input->m_pos.SetMatInfoPos(*pos.GetMatInfoPos());
            Input->m_pos.SetMatInfoRot(*pos.GetMatInfoRot());
            Input->m_pos.SetMatInfoSca(*pos.GetMatInfoSca());
        }
        this->ImGui()->EndTreeNode();
        this->ImGui()->Separator();
    }

    return fpo;
}

//==========================================================================
// スケーリング
float CCreateSubsidiary::Scaling(CFieldData * Input)
{
    float fpo = 0.0f;

    // 階層構造
    if (this->ImGui()->NewTreeNode("拡大縮小", false))
    {
        // スクロールバー
        if (this->ImGui()->SliderFloat("パワー", &fpo, -fPowor, fPowor))
        {
            Input->m_pos.Scale(fpo);
        }

        // リセットボタン
        if (this->ImGui()->Button("拡大縮小のリセット"))
        {
            // スケール情報の宣言
            D3DXVECTOR3 Set = D3DXVECTOR3(fScale, fScale, fScale);
            Input->m_pos.SetMatInfoSca(Set);
        }
        this->ImGui()->EndTreeNode();
        this->ImGui()->Separator();
    }

    return fpo;
}

//==========================================================================
// 生成
void CCreateSubsidiary::Create(void)
{
    // 格納済みデータと生成可能なデータ数が一致しない限り処理
    if ((int)this->m_NameList.size() - 1 != this->m_DataCase.Size())
    {
        // 生成ボタン
        if (this->ImGui()->NewMenu("生成"))
        {
            if (this->ImGui()->Button("社の生成"))
            {
                // 各操作キーの初期化
                this->m_bool4_1.reset();
                this->m_bool4_2.reset();
                this->m_bool4_3.reset();
                this->m_bool4_1.b1 = true;
                this->m_Edit = nullptr;

                // 古いデータがある場合破棄
                this->m_ObjePoly.ObjectDelete(this->m_data->m_pos);
                this->m_DataCase.PinpointRelease(this->m_data);

                // インスタンス生成
                this->m_data = this->m_DataCase.GetDelete(this->m_DataCase.Create());

                // 初期化
                this->m_data->m_pos.Init(0);
                this->m_data->m_pos.Scale(fScale);
                this->m_ObjePoly.ObjectInput(this->m_data->m_pos);
            }

            // 生成モードがアクティブな時
            if (this->m_bool4_1.b1)
            {
                // チェックボックスの設置
                this->m_bool4_1.b2 = true;
                this->m_bool4_1.b3 = true;

                // データ登録ボタン
                if (this->ImGui()->Button("データの登録"))
                {
                    this->m_bool4_1.reset(); // 非アクティブ化
                    this->m_DataCase.PushBack(this->m_data);// データの登録
                    this->m_data = nullptr;
                }
            }

            // 生成内容の出力
            this->SetPos(this->m_bool4_1.b2, this->m_data);

            // パラメータのセット
            this->SetRegionalData();
            this->ImGui()->EndMenu();
        }
    }
}

//==========================================================================
// パラメータのセット
void CCreateSubsidiary::SetRegionalData(void)
{
    // 生成時の処理キーがアクティブな時
    if (this->m_bool4_1.b3)
    {
        if (this->ImGui()->NewMenu("パラメータのセット"))
        {
            // 階層構造
            if (this->ImGui()->NewTreeNode("地域の選択", false))
            {
                // 設定してある地域の未選択地域を検出する
                for (int i = 0; i < (int)this->m_NameList.size(); i++)
                {
                    bool bkey = false; // 使用済み判定キー

                    // 既に使われている地域IDの検出

                    for (int s = 0; s < this->m_DataCase.Size(); s++)
                    {
                        CFieldData * fil = this->m_DataCase.Get(s);
                        
                        // 使用済み地域IDの検出
                        if (fil->m_param.m_textID == i)
                        {
                            bkey = true; // 判定
                            break;
                        }
                    }

                    // 未使用の時、未設定ではないとき
                    if (i != 0 && bkey == false)
                    {
                        // ボタンをセット
                        if (this->ImGui()->Button(this->m_NameList[i].c_str()))
                        {
                            this->m_data->m_param.m_textID = i; // 押された時のIDを代入
                        }
                    }
                }
                this->ImGui()->EndTreeNode();
            }
            this->ImGui()->Separator();

            //bool Capital_City = false; // 首都判定用

            //// 首都が設定されているかの検出
            //for (int i = 0; i < this->m_DataCase.Size(); i++)
            //{
            //    CFieldData * fil = this->m_DataCase.Get(i);
            //    // 検出時
            //    if (fil->m_param.m_capital_city)
            //    {
            //        Capital_City = true; // 判定フラグを立てる
            //        break;
            //    }
            //}

            //// 検出されなかった場合チェックボックスの表示
            //if (Capital_City == false)
            //{
            //    this->ImGui()->Checkbox("首都判定", &this->m_data->m_param.m_capital_city);
            //}

            // スクロールバーによる各種パラメーター処理
            this->ImGui()->SliderInt("社員数", &this->m_data->m_param.m_employee, this->m_LimitRegionEmployee.min_(), this->m_LimitRegionEmployee.max_());
            this->ImGui()->SliderInt("売り上げ", &this->m_data->m_param.m_income, this->m_LimitRegionProfit.min_(), this->m_LimitRegionProfit.max_());
            this->ImGui()->Separator();

            // パラメーターの描画
            this->ImGui()->Text("首都名 : %s", this->m_NameList[this->m_data->m_param.m_textID].c_str());
            this->ImGui()->Text("首都判定 : %d", this->m_data->m_param.m_capital_city);
            this->ImGui()->Text("社員数 : %d", this->m_data->m_param.m_employee);
            this->ImGui()->Text("売り上げ : %d", this->m_data->m_param.m_income);
            this->ImGui()->EndMenu();
        }
    }
}

//==========================================================================
// エディト
void CCreateSubsidiary::Edit(void)
{
    int ncount = 0; // カウンタ

    // 配列番号を入れる処理
    for (int i = 0; i < this->m_DataCase.Size(); i++)
    {
        CFieldData * fil = this->m_DataCase.Get(i);
        fil->m_param.m_id = ncount;
        ncount++;
    }

    // データがある時のみ処理を行う
    if (this->m_DataCase.Size() != 0)
    {
        ncount = 0;

        if (this->ImGui()->NewMenu("登録済みデータの編集"))
        {
            // 編集する地域の選択処理
            for (int i = 0; i < this->m_DataCase.Size(); i++)
            {
                CFieldData * fil = this->m_DataCase.Get(i);

                // テキストを取り出す
                std::string stddata = this->m_NameList[fil->m_param.m_textID];
                ncount++;

                // 未設定時のみ
                stddata += this->Numbering(fil, ncount);

                // オブジェクト名が一致しないときにナンバリングされたデータを入れる
                if (fil->m_ObjName != stddata)
                {
                    fil->m_ObjName = stddata;
                }

                // ボタンが押されたとき
                if (this->ImGui()->NewMenu(fil->m_ObjName.c_str()))
                {
                    // 各フラグのリセット
                    this->m_bool4_1.reset();
                    this->m_bool4_2.reset();
                    this->m_bool4_3.reset();
                    this->m_bool4_4.reset();
                    this->m_bool4_2.b1 = true;
                    this->m_bool4_2.b2 = true;
                    this->m_Edit = fil;

                    // 修正
                    this->SetPos(this->m_bool4_2.b1, this->m_Edit);

                    // エディットデータを入れる
                    this->EditRegionalData();
                    this->ImGui()->EndMenu();
                }
            }
            this->ImGui()->EndMenu();
        }
    }
}

//==========================================================================
// パラメータのセット
void CCreateSubsidiary::EditRegionalData(void)
{
    // エディットモードのパラメータ処理有効時
    if (this->m_bool4_2.b2)
    {
        if (this->ImGui()->NewMenu("編集"))
        {
            if (this->ImGui()->NewTreeNode("地域の選択", false))
            {
                // 設定してある地域の未選択地域を検出する
                for (int i = 0; i < (int)this->m_NameList.size(); i++)
                {
                    bool bkey = false;

                    // 既に使われている地域IDの検出
                    for (int s = 0; s < this->m_DataCase.Size(); s++)
                    {
                        CFieldData * fil = this->m_DataCase.Get(s);

                        // 使用済み地域IDの検出
                        if (fil->m_param.m_textID == i&&i != 0) { bkey = true;break; }
                    }

                    // 検出されなかった地域IDのセット
                    if (bkey == false)
                    {
                        // ボタンをセット
                        if (this->ImGui()->Button(this->m_NameList[i].c_str()))
                        {
                            this->m_Edit->m_param.m_textID = i;
                        }
                    }
                }
                this->ImGui()->EndTreeNode();
            }

            this->ImGui()->Separator();

            if (this->ImGui()->NewTreeNode("パラメータの編集", false))
            {
                // 首都判定の変更処理
                if (this->ImGui()->Checkbox("首都判定", &this->m_Edit->m_param.m_capital_city))
                {
                    // 変更結果の記録
                    bool bkey = this->m_Edit->m_param.m_capital_city;

                    // 全ての首都判定を初期化
                    for (int i = 0; i < this->m_DataCase.Size(); i++)
                    {
                        CFieldData * fil = this->m_DataCase.Get(i);
                        fil->m_param.m_capital_city = false;
                    }
                    // 変更結果をもとに戻す
                    this->m_Edit->m_param.m_capital_city = bkey;
                }

                if (this->ImGui()->Checkbox("エネミー:本社判定", &this->m_Edit->m_head_office_enemy))
                {
                    // 変更結果の記録
                    bool bkey = this->m_Edit->m_head_office_enemy;

                    // 全ての首都判定を初期化
                    for (int i = 0; i < this->m_DataCase.Size(); i++)
                    {
                        CFieldData * fil = this->m_DataCase.Get(i);
                        fil->m_head_office_enemy = false;
                    }
                    // 変更結果をもとに戻す
                    m_Edit->m_head_office_enemy = bkey;
                }
                if (this->ImGui()->Checkbox("プレイヤー:本社判定", &this->m_Edit->m_head_office_player))
                {
                    // 変更結果の記録
                    bool bkey = this->m_Edit->m_head_office_player;

                    // 全ての首都判定を初期化
                    for (int i = 0; i < this->m_DataCase.Size(); i++)
                    {
                        CFieldData * fil = this->m_DataCase.Get(i);
                        fil->m_head_office_player = false;
                    }
                    // 変更結果をもとに戻す
                    m_Edit->m_head_office_player = bkey;
                }

                // 各種パラメーター処理
                this->ImGui()->SliderInt("社員数", &this->m_Edit->m_param.m_employee, this->m_LimitRegionEmployee.min_(), this->m_LimitRegionEmployee.max_());
                this->ImGui()->SliderInt("売り上げ", &this->m_Edit->m_param.m_income, this->m_LimitRegionProfit.min_(), this->m_LimitRegionProfit.max_());
                this->ImGui()->EndTreeNode();
            }

            this->ImGui()->Separator();

            this->LinkSystem();

            // パラメーターの描画
            this->ImGui()->Text("首都名 : %s", this->m_NameList[this->m_Edit->m_param.m_textID].c_str());
            this->ImGui()->Text("首都判定 : %d", this->m_Edit->m_param.m_capital_city);
            this->ImGui()->Text("社員数 : %d", this->m_Edit->m_param.m_employee);
            this->ImGui()->Text("売り上げ : %d", this->m_Edit->m_param.m_income);

            // 双方向の地域の情報
            std::string StrPrevObjName = "";
            std::string StrNextObjName = "";

            // 次のIDがセットされていない
            if (this->m_Edit->m_param.m_prev == -1)
            {
                StrPrevObjName = "No Data";
            }
            else
            {
                StrPrevObjName = this->m_DataCase.Get(this->m_Edit->m_param.m_prev)->m_ObjName;
            }

            // 前のIDがないとき
            if (this->m_Edit->m_param.m_next == -1)
            {
                StrNextObjName = "No Data";
            }
            else
            {
                StrNextObjName = this->m_DataCase.Get(this->m_Edit->m_param.m_next)->m_ObjName;
            }

            this->ImGui()->Text("%s >> %s >> %s", StrPrevObjName.c_str(), this->m_Edit->m_ObjName.c_str(), StrNextObjName.c_str());

            // 消去
            if (this->ImGui()->Button("消去"))
            {
                this->m_Edit->m_key = true;
                this->m_Edit = nullptr;
                this->m_bool4_1.reset();
                this->m_bool4_2.reset();
                this->m_bool4_3.reset();
                this->m_bool4_4.reset();
            }
            this->ImGui()->EndMenu();
        }
    }
}

//==========================================================================
// ナンバリング
std::string CCreateSubsidiary::Numbering(CFieldData * Input, int num)
{
    std::string stddata;

    stddata = "";

    // 未設定時のみナンバリングを行う。
    if (Input->m_param.m_textID == 0)
    {
        char cBuf[256] = { 0 };
        sprintf(cBuf, "%d", num);
        stddata = cBuf;
    }

    return stddata;
}

//==========================================================================
// リンクシステム
void CCreateSubsidiary::LinkSystem(void)
{
    if (this->ImGui()->NewTreeNode("首都同士の関連付け", false))
    {
        if (this->ImGui()->NewTreeNode("前の地域", false))
        {
            for (int i = 0; i < this->m_DataCase.Size(); i++)
            {
                CFieldData * fil = this->m_DataCase.Get(i);
                if (this->ImGui()->Button(fil->m_ObjName.c_str()))
                {
                    // 破棄する対象がアクセスする情報に対しての関連情報の破棄
                    if (fil->m_param.m_next != -1)
                    {
                        this->m_DataCase.Get(fil->m_param.m_next)->m_param.m_prev = -1;
                    }
                    if (this->m_Edit->m_param.m_prev != -1)
                    {
                        this->m_DataCase.Get(this->m_Edit->m_param.m_prev)->m_param.m_next = -1;
                    }

                    // IDを渡す
                    this->m_Edit->m_param.m_prev = fil->m_param.m_id;
                    fil->m_param.m_next = this->m_Edit->m_param.m_id;
                }
            }
            this->ImGui()->EndTreeNode();
        }
        if (this->ImGui()->NewTreeNode("次の地域", false))
        {
            for (int i = 0; i < this->m_DataCase.Size(); i++)
            {
                CFieldData * fil = this->m_DataCase.Get(i);
                if (this->ImGui()->Button(fil->m_ObjName.c_str()))
                {
                    // 破棄する対象がアクセスする情報に対しての関連情報の破棄
                    if (this->m_Edit->m_param.m_next != -1)
                    {
                        this->m_DataCase.Get(this->m_Edit->m_param.m_next)->m_param.m_prev = -1;
                    }
                    if (fil->m_param.m_prev != -1)
                    {
                        this->m_DataCase.Get(fil->m_param.m_prev)->m_param.m_next = -1;
                    }

                    // IDを渡す
                    this->m_Edit->m_param.m_next = fil->m_param.m_id;
                    fil->m_param.m_prev = this->m_Edit->m_param.m_id;
                }
            }
            this->ImGui()->EndTreeNode();
        }
        this->ImGui()->EndTreeNode();
    }
    this->ImGui()->Separator();
}

//==========================================================================
// データの破棄
void CCreateSubsidiary::Delete(void)
{
    for (int i = 0; i < this->m_DataCase.Size(); i++)
    {
        CFieldData * fil = this->m_DataCase.Get(i);
        if (fil->m_key == true)
        {
            // 破棄する対象がアクセスする情報に対しての関連情報の破棄
            if (fil->m_param.m_next != -1)
            {
                this->m_DataCase.Get(fil->m_param.m_next)->m_param.m_prev = -1;
            }
            if (fil->m_param.m_prev != -1)
            {
                this->m_DataCase.Get(fil->m_param.m_prev)->m_param.m_next = -1;
            }

            this->m_ObjePoly.ObjectDelete(fil->m_pos);
            // イテレータを破棄し、仮のイテレータを返す
            fil->Release();
            this->m_DataCase.PinpointRelease(fil);
            break;
        }
    }
}

//==========================================================================
// 中継中継地点をセット
void CCreateSubsidiary::SetRelayPoint(void)
{
    auto * point = (CRelayPoint*)this->m_RelayPoint;

    if (point != nullptr)
    {
        point->Reset();
        for (int i = 0; i < this->m_DataCase.Size(); i++)
        {
            CFieldData * fil = this->m_DataCase.Get(i);
            if (this->m_DataCase.Get(fil->m_param.m_next) != nullptr)
            {
                const C3DObject &pos1 = fil->m_pos;
                const C3DObject &pos2 = this->m_DataCase.Get(fil->m_param.m_next)->m_pos;

                point->Set(pos1, pos2);
            }
        }
    }
}

//==========================================================================
// 読み込み
void CCreateSubsidiary::Load(StageData_ID Input)
{
    auto * point = (CRelayPoint*)this->m_RelayPoint;

    this->m_CSV.CSVSetData(this->m_stage_data.Get_Pass_CSV(Input));

    // データがある場合
    if (this->m_CSV.m_input.size() != 0 && point != nullptr)
    {
        this->RegionalRelease();
        point->Reset();
        this->Release();
        this->RegionalReset();
        this->m_ObjePoly.Release();

        this->m_ObjePoly.Init(RESOURCE_build_x, CXmodel::LightMoad::System);
        this->m_ObjePoly.Init(RESOURCE_Sub_build_x, CXmodel::LightMoad::System);
        this->Load(this->m_stage_data.Get_Pass_BIN(Input));

        // 地域の登録
        for (auto itr = this->m_CSV.m_input.begin(); itr != this->m_CSV.m_input.end(); ++itr)
        {
            this->RegionalSet((*itr));
        }

        // 解放
        this->m_CSV.CSVReleaseData();
    }
}

//==========================================================================
// セーブ
void CCreateSubsidiary::Save(StageData_ID Input)
{
    this->Save(this->m_stage_data.Get_Pass_BIN(Input));
}

//==========================================================================
// 読み込み
void CCreateSubsidiary::Load(const std::string Input)
{
    FILE *pFile = fopen(Input.c_str(), "rb");

    if (pFile)
    {
        // 終わりまで回す
        int nsize = 0; // サイズの取得
        fread(&nsize, sizeof(nsize), 1, pFile);

        // 取得したデータ数回す
        for (int i = 0; i < nsize; i++)
        {
            CFieldData data; // データ格納
            CFieldData *pdata = nullptr; // コピー先
            char cBuf[512] = { 0 }; // バフ

            // データの読み取り
            fread(&data.m_pos, sizeof(data.m_pos), 1, pFile);
            fread(&data.m_param, sizeof(data.m_param), 1, pFile);
            fread(cBuf, sizeof(cBuf), 1, pFile);
            fread(&data.m_head_office_enemy, sizeof(data.m_head_office_enemy), 1, pFile);
            fread(&data.m_head_office_player, sizeof(data.m_head_office_player), 1, pFile);

            // インスタンス生成
            pdata = this->m_DataCase.Create();

            // データの移植
            pdata->m_pos.Init(data.m_pos.getindex());
            pdata->m_pos.SetLockAt(*data.m_pos.GetLockAt());
            pdata->m_pos.SetLockEye(*data.m_pos.GetLockEye());
            pdata->m_pos.SetLockUp(*data.m_pos.GetLockUp());
            pdata->m_pos.SetMatInfoPos(*data.m_pos.GetMatInfoPos());
            pdata->m_pos.SetMatInfoRot(*data.m_pos.GetMatInfoRot());
            pdata->m_pos.SetMatInfoSca(*data.m_pos.GetMatInfoSca());
            pdata->m_pos.SetVecFront(*data.m_pos.GetVecFront());
            pdata->m_pos.SetVecRight(*data.m_pos.GetVecRight());
            pdata->m_pos.SetVecUp(*data.m_pos.GetVecUp());
            pdata->m_pos.SetMatrixType(data.m_pos.GetMatrixType());
            pdata->m_param = data.m_param;
            pdata->m_ObjName = cBuf;
            pdata->m_head_office_enemy = data.m_head_office_enemy;
            pdata->m_head_office_player = data.m_head_office_player;

            // データの登録
            this->m_ObjePoly.ObjectInput(pdata->m_pos);
        }
        fclose(pFile);
    }
}

//==========================================================================
// セーブ
void CCreateSubsidiary::Save(const std::string Input)
{
    FILE *pFile = fopen(Input.c_str(), "wb");

    if (pFile)
    {
        int nsize = this->m_DataCase.Size(); // サイズの取得
        fwrite(&nsize, sizeof(nsize), 1, pFile); // データ数の書き込み

        for (int i = 0; i < this->m_DataCase.Size(); i++)
        {
            CFieldData * fil = this->m_DataCase.Get(i);
            char cBuf[512] = { 0 }; // バフ

            // バフに登録
            sprintf(cBuf, "%s", fil->m_ObjName.c_str());

            // データを書き込む
            fwrite(&fil->m_pos, sizeof(fil->m_pos), 1, pFile);
            fwrite(&fil->m_param, sizeof(fil->m_param), 1, pFile);
            fwrite(cBuf, sizeof(cBuf), 1, pFile);
            fwrite(&fil->m_head_office_enemy, sizeof(fil->m_head_office_enemy), 1, pFile);
            fwrite(&fil->m_head_office_player, sizeof(fil->m_head_office_player), 1, pFile);
        }
        fclose(pFile);
    }
}