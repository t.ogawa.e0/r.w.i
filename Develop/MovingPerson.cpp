//==========================================================================
// 動く人[MovingPerson.h]
// author: tatsuya ogawa
//==========================================================================
#include "MovingPerson.h"
#include "LoadGameData.h"
#include "Worldtime.h"
#include "GamePlayerEmployee.h"
#include "Subsidiary.h"
#include "Game_Start.h"
#include "resource_list.h"

//==========================================================================
// 初期化
bool CMovingPerson::Init(void)
{
    // オブジェクトのインスタンス生成
    for (int i = 0; i < CLoadGameData::Size(); i++)
    {
        CFieldData * fie = this->m_CObjectData.Create();
        *fie = CLoadGameData::GetData(i);
    }

    // オブジェクトの先行呼び出し
    this->m_WorldTime = this->GetObjects(CObject::ID::Default, CObject::Type::Game_WorldTime);
    this->m_Employee = this->GetObjects(CObject::ID::Default, CObject::Type::Game_GamePlayerEmployee);
    this->m_start_script = this->GetObjects(ID::Polygon2D, Type::Game_Start_Object);

    return this->_3DXmodel()->Init(RESOURCE_koma_x);
}

//==========================================================================
// 解放
void CMovingPerson::Uninit(void)
{
    this->m_CObjectData.Create();
}

//==========================================================================
// 更新
void CMovingPerson::Update(void)
{
    if (this->m_WorldTime == nullptr)
    {
        this->m_WorldTime = this->GetObjects(CObject::ID::Default, CObject::Type::Game_WorldTime);
    }

    if (this->m_Employee == nullptr)
    {
        this->m_Employee = this->GetObjects(CObject::ID::Default, CObject::Type::Game_GamePlayerEmployee);
    }

    if (this->m_start_script == nullptr)
    {
        this->m_start_script = this->GetObjects(ID::Polygon2D, Type::Game_Start_Object);
    }

    auto * p_employee = (CGamePlayerEmployee*)this->m_Employee;
    auto * pTime = (CWorldTime*)this->m_WorldTime;
    auto * p_start = (CGame_Start*)this->m_start_script;

    // オブジェクトが存在するとき
    if (p_start != nullptr&&pTime != nullptr&&p_employee != nullptr)
    {
        if (p_start->Start())
        {
            // 終了した移動タスクの処理
            for (auto itr = p_employee->GetEndTaskID().begin(); itr != p_employee->GetEndTaskID().end(); ++itr)
            {
                auto itr2 = this->m_inputlist.find((*itr));

                // 存在するとき
                if (itr2 != this->m_inputlist.end())
                {
                    // 登録済みオブジェクトのデータの破棄
                    this->_3DXmodel()->ObjectDelete(itr2->second.m_obj);

                    // オブジェクトの破棄
                    this->_3DObject()->PinpointRelease(itr2->second.m_obj);

                    // データを破棄
                    this->m_inputlist.erase(itr2);
                }

                // 終了キーをセット
                (*itr) = -1;
            }

            // 移動オブジェクト設定
            for (auto itr = p_employee->GetTaskData()->begin(); itr != p_employee->GetTaskData()->end(); ++itr)
            {
                // データIDの検索
                auto itr2 = this->m_inputlist.find(itr->m_data_id);
                // 存在しない場合
                if (itr2 == this->m_inputlist.end())
                {
                    // 移動数0のオブジェクトの時
                    if (itr->m_now == 0)
                    {
                        auto * p_obj = this->_3DObject()->Create();
                        auto * p_fild = this->m_CObjectData.Get(itr->m_id_old);

                        // データの初期化
                        p_obj->Init(0);
                        p_obj->Scale(6.0f);
                        p_obj->SetMatInfoPos(*p_fild->m_pos.GetMatInfoPos());

                        // オブジェクト登録
                        this->_3DXmodel()->ObjectInput(p_obj);

                        // 登録
                        this->m_inputlist[itr->m_data_id] = CHuman(p_obj, 0.0f, itr->m_now);
                    }
                }
                // データが存在する場合
                else if (itr2 != this->m_inputlist.end())
                {
                    // ターゲットの更新
                    // 前回とターンが違うときと最大移動回数と同じではないとき
                    if (itr2->second.m_old_turn != itr->m_now&&itr2->second.m_old_turn != itr->m_turn)
                    {
                        // ルートポインタからルートの取得
                        auto * p_sub_data = (CSubsidiaryParam*)itr->m_root_pt[itr->m_now];

                        // 移動距離の計算
                        float distance = this->_Hit()->Distance(*itr2->second.m_obj, p_sub_data->m_pos);

                        // 移動速度の計算
                        itr2->second.m_speed = distance / (float)(pTime->Geturntime() * 60);

                        // 向きの変更
                        for (int cmove = 0; cmove < 10; cmove++)
                        {
                            itr2->second.m_obj->LockOn(p_sub_data->m_pos, 0.6f);
                        }

                        // ターンの更新
                        itr2->second.m_old_turn = itr->m_now;
                    }

                    // オブジェクトの移動
                    itr2->second.m_obj->MoveZ(itr2->second.m_speed);
                }
            }
        }
    }

    //==========================================================================
    // リソースの更新
    //==========================================================================
    this->_3DXmodel()->Update();
}

//==========================================================================
// 描画
void CMovingPerson::Draw(void)
{
    this->_3DXmodel()->Draw();
}
