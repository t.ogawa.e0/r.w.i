//==========================================================================
// ImGuiデバッグエネミー[ImGui_Enemy.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "ImGui_Enemy.h"
#include "ImGui_Player.h"
#include "worldtime.h"
#include "Subsidiary.h"

//==========================================================================
// 初期化
bool CImGui_Enemy::Init(void)
{
    this->SetCharacterType(CharList::Enemy);
    this->m_psub = CSubsidiary::Get(CharList::Enemy);

    // mainデータ初期化
    this->m_param = CPlayerParam(20 * this->m_psub->GetMaxNemployee(), this->m_psub->GetNumSub(), 20);

    // 総従業員に加算
    this->m_param.m_total_employee = this->m_psub->GetMaxNemployee();

    //移動時間カウンター初期化
    for (int i = 0; i < m_psub->GetNumSub(); i++)
    {
        this->m_waitCounter[i] = 0;
    }

    this->m_actionID[0] = CImGui_Enemy::actionID::NONE;
    this->m_actionID[1] = CImGui_Enemy::actionID::NONE;

    // オブジェクトの先行呼び出し
    this->m_WorldTime = (CWorldTime*)this->GetObjects(CObject::ID::Default, CObject::Type::Game_WorldTime);

    return false;
}

//==========================================================================
// 解放
void CImGui_Enemy::Uninit(void)
{
}

//==========================================================================
// 更新
void CImGui_Enemy::Update(void)
{
    if (this->m_WorldTime == nullptr)
    {
        this->m_WorldTime = (CWorldTime*)this->GetObjects(CObject::ID::Default, CObject::Type::Game_WorldTime);
    }

    auto*pTime = (CWorldTime*)this->m_WorldTime;

    if (pTime != nullptr)
    {
        // ワールド時間の更新が入ったときに処理が発生する。
        this->UpdateParam(pTime->getturns());

        // 不満度の処理
        this->Dissatisfied(pTime->getturns2()->GetTime());

        // エネミーの行動
        this->action(pTime->getturns());
    }
    // 社員一揆の処理
    this->Strike();

    // テキストセット
    this->Text("エネミーの情報");
}

//==========================================================================
// 描画
void CImGui_Enemy::Draw(void)
{
}

// エネミーの行動
void CImGui_Enemy::action(const CGameManager::TURNS_t & InputTime)
{
    if (InputTime.m_current != InputTime.m_old)
    {
        //思考関数呼び出し
        this->selectTarget();

        switch (this->m_actionID[0])
        {
        case actionID::NONE:
            break;

        case actionID::FIRE:
            //不満度オーバー防止
            if (this->m_param.m_dissatisfied + this->m_numemployment * 5 >= this->m_param.m_wage)
            {
                this->m_numemployment = (this->m_param.m_wage - 1 - this->m_param.m_dissatisfied) / 5;
            }
            this->Fire(this->m_psub->GetSub(this->m_targetNum), this->m_numemployment);
            break;

        case actionID::EMPLOYMENT:
            this->Employment(this->m_psub->GetSub(this->m_targetNum), this->m_numemployment);
            break;

        case actionID::TRANSFERRED:
            //移動指示後のウェイト設定（テストで5、移動ターン数取得したい
            this->m_waitCounter[this->m_targetNum] = 5;
            //不満度オーバー防止
            if (this->m_param.m_dissatisfied + this->m_numemployment * 2 >= this->m_param.m_wage)
            {
                this->m_numemployment = (this->m_param.m_wage - 1 - this->m_param.m_dissatisfied) / 2;
            }
            this->Transferred(this->m_psub->GetSub(this->m_sourceTargetNum), this->m_psub->GetSub(this->m_targetNum), this->m_psub, this->m_numemployment);
            break;

        case actionID::WAGE:
            break;

        default:
            break;
        }
    }

    //タスク更新
    this->m_actionID[0] = this->m_actionID[1];
    this->m_actionID[1] = CImGui_Enemy::actionID::NONE;
}

void CImGui_Enemy::selectTarget(void)
{
    // 子会社数取得
    int NumSub = this->m_psub->GetNumSub();

    // ウェイトカウンター更新
    for (int i = 0; i < NumSub; i++)
    {
        if (this->m_waitCounter[i] > 0)
        {
            this->m_waitCounter[i]--;
        }
    }

    //最適人数の計算
    std::vector<int> optimumNumEmployee(NumSub);//エリアごとの最適人数
    int totalOptimumNumEmployee = 0;			//全体での最適人数
    for (int i = 0; i < NumSub; i++)
    {
        //エリア売り上げ÷２÷ベース賃金
        optimumNumEmployee[i] = this->m_psub->GetSub(i)->m_income / this->m_param.m_wage / 2;
        totalOptimumNumEmployee += optimumNumEmployee[i];
    }

    //次アクションが登録されている = 本社からターゲット不足分ジャストの移動なので人数設定してリターン
    if (this->m_actionID[0] != CImGui_Enemy::actionID::NONE)
    {
        this->m_numemployment = optimumNumEmployee[this->m_targetNum] - this->m_psub->GetSub(this->m_targetNum)->m_employee;
        return;
    }

    //解雇判断
    //解雇すべきケース：全体最適人数＜社員数
    //ターゲット：最もオーバーしているエリア
    //人数：全体でのオーバー数とそのエリアのオーバー数とで小さいほう
    if (totalOptimumNumEmployee < this->m_param.m_total_employee)
    {
        this->m_targetNum = 0;
        for (int i = 1; i < NumSub; i++)
        {
            if (this->m_psub->GetSub(i)->m_employee - optimumNumEmployee[i] > this->m_psub->GetSub(this->m_targetNum)->m_employee - optimumNumEmployee[this->m_targetNum])
            {
                this->m_targetNum = i;
            }
        }

        if (this->m_param.m_total_employee - totalOptimumNumEmployee > this->m_psub->GetSub(this->m_targetNum)->m_employee - optimumNumEmployee[this->m_targetNum])
        {
            this->m_numemployment = this->m_psub->GetSub(this->m_targetNum)->m_employee - optimumNumEmployee[this->m_targetNum];
        }
        else
        {
            this->m_numemployment = this->m_param.m_total_employee - totalOptimumNumEmployee;
        }

        this->m_actionID[0] = actionID::FIRE;
        return;
    }

    //増員ターゲット判断
    //ウェイト中じゃないエリアで最もロス指標(不足人数×エリア売り上げ)が大きい
    this->m_targetNum = 0;
    int loss = 0;
    for (int i = 1; i < NumSub; i++)
    {
        if ((this->m_waitCounter[i] == 0) && ((optimumNumEmployee[i] - this->m_psub->GetSub(i)->m_employee) * this->m_psub->GetSub(i)->m_income) > loss)
        {
            this->m_targetNum = i;
            loss = (optimumNumEmployee[i] - this->m_psub->GetSub(i)->m_employee) * this->m_psub->GetSub(i)->m_income;
        }
    }
    //タゲ本社かつ本社最適なら全エリア最適なのでアクションなし
    if ((this->m_targetNum == 0) && (this->m_psub->GetSub(0)->m_employee >= optimumNumEmployee[0]))
    {
        this->m_actionID[0] = actionID::NONE;
        return;
    }

    //移動元判断
    //理想人数を最もオーバーしているエリアを対象
    this->m_sourceTargetNum = 0;
    for (int i = 1; i < NumSub; i++)
    {
        if ((this->m_psub->GetSub(i)->m_employee > optimumNumEmployee[i]) && (this->m_psub->GetSub(i)->m_employee - optimumNumEmployee[i] > this->m_psub->GetSub(this->m_sourceTargetNum)->m_employee - optimumNumEmployee[this->m_sourceTargetNum]))
        {
            this->m_sourceTargetNum = i;
        }
    }
    //移動元本社(初期設定)かつ移動の結果本社が最適人数以下になるなら不足人数を雇用→移動になる
    if (this->m_sourceTargetNum == 0 && optimumNumEmployee[this->m_targetNum] - this->m_psub->GetSub(this->m_targetNum)->m_employee > this->m_psub->GetSub(0)->m_employee - optimumNumEmployee[0])
    {
        this->m_actionID[0] = actionID::EMPLOYMENT;
        this->m_numemployment = (optimumNumEmployee[this->m_targetNum] - this->m_psub->GetSub(this->m_targetNum)->m_employee) - (this->m_psub->GetSub(0)->m_employee - optimumNumEmployee[0]);

        //タゲが本社以外なら次ターン行動に移動を設定
        if (this->m_targetNum != 0)
        {
            this->m_actionID[1] = actionID::TRANSFERRED;
        }
    }
    else
    {
        this->m_actionID[0] = actionID::TRANSFERRED;
        if (this->m_psub->GetSub(this->m_sourceTargetNum)->m_employee - optimumNumEmployee[this->m_sourceTargetNum] > optimumNumEmployee[this->m_targetNum] - this->m_psub->GetSub(this->m_targetNum)->m_employee)
        {
            this->m_numemployment = optimumNumEmployee[this->m_targetNum] - this->m_psub->GetSub(this->m_targetNum)->m_employee;
        }
        else
        {
            this->m_numemployment = this->m_psub->GetSub(this->m_sourceTargetNum)->m_employee - optimumNumEmployee[this->m_sourceTargetNum];
        }
    }
}