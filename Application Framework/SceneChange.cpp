//==========================================================================
// シーン遷移[SceneChange.cpp]
// author: tatuya ogawa
//==========================================================================
#include "SceneChange.h"
#include "make.h"

//==========================================================================
// シーンの切り替え
void CSceneManager::ChangeScene(SceneName Name) 
{
	// 解放
	this->Uninit();

	switch (Name)
	{
	case SceneName::NOME:
        CDeviceManager::GetDXDevice()->ErrorMessage("Scene Ellor : -1");
		break;

	case SceneName::Load:
		this->m_pScene = nullptr;
		break;

    case SceneName::Default:
        this->m_pScene = new CMake;
        break;

	default:
        CDeviceManager::GetDXDevice()->ErrorMessage("Scene Ellor : -1");
		break;
	}
}


//==========================================================================
// 初期化 初期化終了時にtrue
bool CSceneManager::Init(void)
{
    if (this->m_pScene != nullptr)
    {
        return this->m_pScene->Init();
    }
    return true;
}

//==========================================================================
// 解放
void CSceneManager::Uninit(void)
{
    if (this->m_pScene != nullptr)
    {
        this->m_pScene->Uninit();
        delete this->m_pScene;
        this->m_pScene = nullptr;
    }
}

//==========================================================================
// 更新
void CSceneManager::Update(void)
{
    if (this->m_pScene != nullptr)
    {
        this->m_pScene->Update();
    }
}

//==========================================================================
// 描画
void CSceneManager::Draw(void)
{
    if (this->m_pScene != nullptr)
    {
        this->m_pScene->Draw();
    }
}