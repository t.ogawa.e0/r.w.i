//=============================================================================
// リソースのファイルパス
// 更新日 [2019/02/11]
// 更新時間 [20:50:08]
//=============================================================================
#pragma once

//=============================================================================
// 拡張子 [.DDS]
//=============================================================================

#ifndef RESOURCE_LoadTex_DDS
#define RESOURCE_LoadTex_DDS "resource/texture/LoadTex.DDS"
#endif // !RESOURCE_LoadTex_DDS
#ifndef RESOURCE_NowLoading_DDS
#define RESOURCE_NowLoading_DDS "resource/texture/NowLoading.DDS"
#endif // !RESOURCE_NowLoading_DDS

//=============================================================================
// 拡張子 [.csv]
//=============================================================================

#ifndef RESOURCE_ディレクトリ指定_csv
#define RESOURCE_ディレクトリ指定_csv "/ディレクトリ指定.csv"
#endif // !RESOURCE_ディレクトリ指定_csv
#ifndef RESOURCE_拡張子指定_csv
#define RESOURCE_拡張子指定_csv "/拡張子指定.csv"
#endif // !RESOURCE_拡張子指定_csv

//=============================================================================
// 拡張子 [.bmp]
//=============================================================================

#ifndef RESOURCE_TitleLogo_bmp
#define RESOURCE_TitleLogo_bmp "/TitleLogo.bmp"
#endif // !RESOURCE_TitleLogo_bmp

//=============================================================================
// 拡張子 [.ico]
//=============================================================================

#ifndef RESOURCE_ICO_3_ico
#define RESOURCE_ICO_3_ico "/ICO_3.ico"
#endif // !RESOURCE_ICO_3_ico
