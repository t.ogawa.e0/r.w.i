//==========================================================================
// ObjectVectorManager[ObjectVectorManager.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _ObjectVectorManager_H_
#define _ObjectVectorManager_H_

//==========================================================================
// include
//==========================================================================
#include <vector>

//==========================================================================
//
// class  : CObjectVectorManager
// Content: ObjectVectorManager
//
//==========================================================================
template <typename _Ty>
class CObjectVectorManager
{
private:
    // コピー禁止 (C++11)
    CObjectVectorManager(const CObjectVectorManager &) = delete;
    CObjectVectorManager &operator=(const CObjectVectorManager &) = delete;
public:
    CObjectVectorManager() {}
    ~CObjectVectorManager() {
        this->Release();
    }

    // Objectの破棄
    void Release(void) {
        for (auto itr = this->m_list.begin(); itr != this->m_list.end(); ++itr) {
            this->Delete((*itr));
        }
        this->m_list.clear();

        for (auto itr = this->m_list_sub.begin(); itr != this->m_list_sub.end(); ++itr) {
            this->Delete((*itr));
        }
        this->m_list_sub.clear();
    }

    // インスタンスの生成
    // 戻り値 : インスタンスのアドレス
    _Ty * Create(void) {
        _Ty * Object = nullptr;
        this->m_list.emplace_back(this->New(Object));
        return Object;
    }

    // 指定したアドレスの管理権限をメインメモリから破棄
    // サブメモリに管理権限を移します。
    // 失敗時 nullが返ります
    _Ty * GetDelete(_Ty * Object) {
        if (Object != nullptr) {
            bool hit = false; // 管理範囲外のデータが来た場合スルーするためのフラグ
                              // 指定アドレスの管理権限を破棄する
            for (auto itr = this->m_list.begin(); itr != this->m_list.end(); ++itr) {
                if ((*itr) == Object) {
                    hit = true; // 管理範囲内の場合
                    this->m_list.erase(itr);
                    break;
                }
            }
            if (hit)
            {
                // サブメモリに管理権限を持たせる
                this->m_list_sub.emplace_back(Object);
            }
        }
        return Object; // 戻り値として返す
    }

    // 指定した領域の管理権限をメインメモリから破棄
    // サブメモリに管理権限を移します。
    // 失敗時 nullが返ります
    _Ty * GetDelete(int label) {
        return this->GetDelete(this->Get(label));
    }

    // メインメモリに管理権限を戻す
    // GetDelete() で取得したデータを入れてください。
    void PushBack(_Ty * Object) {
        if (Object != nullptr) {
            // サブメモリから管理権限を破棄
            bool hit = false; // 管理範囲外のデータが来た場合スルーするためのフラグ
            for (auto itr = this->m_list_sub.begin(); itr != this->m_list_sub.end(); ++itr) {
                if ((*itr) == Object) {
                    hit = true; // ヒット
                    this->m_list_sub.erase(itr);
                    break;
                }
            }
            if (hit)
            {
                // メインメモリに管理権限を持たせる
                this->m_list.emplace_back(Object);
            }
        }
    }

    // 特定のObjectの破棄
    // 破棄するオブジェクトのアドレスを入れてください
    void PinpointRelease(_Ty * Object) {
        if (Object != nullptr) {
            // メインメモリ
            for (auto itr = this->m_list.begin(); itr != this->m_list.end(); ++itr) {
                if ((*itr) == Object) {
                    this->Delete((*itr));
                    this->m_list.erase(itr);
                    return;
                }
            }
            // サブメモリ
            for (auto itr = this->m_list_sub.begin(); itr != this->m_list_sub.end(); ++itr) {
                if ((*itr) == Object) {
                    this->Delete((*itr));
                    this->m_list_sub.erase(itr);
                    return;
                }
            }
        }
    }

    // 特定のObjectの破棄
    // 破棄する領域を入れてください
    void PinpointRelease(int label) {
        this->PinpointRelease(this->Get(label));
    }

    // 管理しているObject数
    int Size(void) {
        return (int)this->m_list.size();
    }

    // Objectのゲッター
    // 失敗時 null
    _Ty * Get(int label) {
        // メモリ領域内の時
        if (0 <= label&&label<this->Size()) {
            return this->m_list[label];
        }
        return nullptr;
    }
private:
    // インスタンス生成
    // アドレスが返ります
    _Ty * New(_Ty *& Object) {
        Object = new _Ty;
        return Object;
    }

    // インスタンスの破棄
    // nullが返ります
    _Ty * Delete(_Ty *& Object) {
        // 破棄メモリがある時
        if (Object != nullptr) {
            delete Object;
            Object = nullptr;
        }
        return Object;
    }
private:
    std::vector<_Ty*>m_list; // メインメモリ(アドレスで登録)
    std::vector<_Ty*>m_list_sub; // サブメモリ(アドレスで登録)
};

#endif // !_ObjectVectorManager_H_
