//==========================================================================
// キューブ[Cube.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _Cube_H_
#define _Cube_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "Rectangle.h"
#include "Vertex3D.h"
#include "3DObject.h"
#include "Template.h"
#include "DataType.h"
#include "TextureLoader.h"
#include "DeviceManager.h"
#include "Create.h"
#include "ObjectInput.h"

//==========================================================================
//
// class  : CCube
// Content: キューブ 
//
//==========================================================================
class CCube : private CRectangle, private VERTEX_3D, private CCreate, public CObjectInput
{
private:
    // コピー禁止 (C++11)
    CCube(const CCube &) = delete;
    CCube &operator=(const CCube &) = delete;
private:
	// テクスチャ情報格納
	class TEXTURE
	{
	public:
		TEXTURE()
		{
			this->Direction1 = 0;
			this->Direction2 = 0;
			this->Pattern = 0;
			this->texsize = 0;
			this->texcutsize = 0;
		}
		~TEXTURE(){}
	public:
		int Direction1; // 一行の画像数
		int Direction2; // 一列の画像数
		int Pattern; // 面の数
		CTexvec<float> texsize; // テクスチャのサイズ
		CTexvec<float> texcutsize; // テクスチャの切り取りサイズ
	};
public:
    CCube() {
        this->m_pVertexBuffer = nullptr;
        this->m_pIndexBuffer = nullptr;
        this->m_Lock = false;
        this->m_Device = nullptr;
    }
	~CCube() {
        this->Release();
    }

	// 初期化 失敗時true
	// Input = 使用するテクスチャのパス ダブルポインタに対応
	// size = 要素数 
	bool Init(const char** Input, int size);

	// 初期化 失敗時true
	// Input = 使用するテクスチャのパス
	bool Init(const char* Input);

    // 更新
    void Update(void);

	// 解放
	void Release(void);

    // 描画
    // DrawType = ワールド行列の種類の指定をします
    void Draw(void);

	// デバイスのセット
	void SetDevice(const LPDIRECT3DDEVICE9 Input) { this->m_Device = Input; }
private:
	// キューブ生成
	void SetCube(VERTEX_4 * Output, const CUv<float> * _this);

	// UV生成
	void SetUV(const TEXTURE * Input, CUv<float> * Output, const int nNum);
private: // 格納
	CTextureLoader m_texture; // テクスチャの格納
	D3DMATERIAL9 m_aMat; // マテリアルの情報
	LPDIRECT3DVERTEXBUFFER9 m_pVertexBuffer; // バッファ
	LPDIRECT3DINDEXBUFFER9 m_pIndexBuffer; // インデックスバッファ
	CTemplates m_temp; // テンプレート
	bool m_Lock; // バッファロック
	LPDIRECT3DDEVICE9 m_Device; // デバイス
};


#endif // !_Cube_H_
