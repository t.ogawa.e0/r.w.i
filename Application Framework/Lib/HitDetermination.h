//==========================================================================
// 当たり判定[HitDetermination.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _HitDetermination_H_
#define _HitDetermination_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "3DObject.h"
#include "2DObject.h"
#include "DataType.h"

//==========================================================================
//
// class  : CHitDetermination
// Content: 当たり判定専用クラス
//
//==========================================================================
class CHitDetermination
{
public:
	CHitDetermination() {}
	~CHitDetermination() {}

	// 球当たり判定 当たった場合true
	bool Ball(const C3DObject & TargetA, const C3DObject & TargetB, float Scale);

	// シンプルな当たり判定 当たった場合true
	bool Simple(const C3DObject & TargetA, const C3DObject & TargetB, float Scale);

    // 距離の算出
    // C2DObject or C3DObject
    float Distance(const C3DObject & TargetA, const C3DObject & TargetB);

    // 距離の算出
    // C2DObject or C3DObject
    float Distance(const C2DObject & TargetA, const C2DObject & TargetB);

    // 距離の算出
    // CVector4<float>
    float Distance(const CVector4<float> & TargetA, const CVector4<float> & TargetB);
private:
	// シンプルな当たり判定の内部 
	bool Sinple2(const float & TargetA, const float & TargetB, const float & TargetC, float Scale);
};

#endif // !_HitDetermination_H_
