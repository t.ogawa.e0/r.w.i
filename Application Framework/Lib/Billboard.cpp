//==========================================================================
// ビルボード[Billboard.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Billboard.h"

//==========================================================================
// 定数定義
//==========================================================================
constexpr float VCRCT = 0.5f;

//==========================================================================
// 初期化 失敗時true
// Input = 使うテクスチャのパス
// plate = 板ポリの種類
// bCenter = 中心座標を下の真ん中にします
bool CBillboard::Init(const char* Input, PlateList plate, bool bCenter)
{
	// テクスチャの格納
	if (this->m_tex.init(Input)) { return true; }

    // バッファの生成
	if (this->CreateBuffer(plate, bCenter)) { return true; }

	return false;
}

//==========================================================================
// アニメーション用初期化 失敗時true
// Input = 使うテクスチャのパス
// UpdateFrame = 更新フレーム
// Pattern = アニメーション数
// Direction = 横一列のアニメーション数
// plate = 板ポリの種類
// bCenter = 中心座標を下の真ん中にします
bool CBillboard::Init(const char* Input, int UpdateFrame, int Pattern, int Direction, PlateList plate, bool bCenter)
{
	// テクスチャの格納
	if (this->m_tex.init(Input)) { return true; }

    // バッファの生成
	if (this->CreateBuffer(plate, bCenter)) { return true; }

	// アニメーションの生成
	this->CreateAnimation(UpdateFrame, Pattern, Direction);

	return false;
}

//==========================================================================
// 初期化 失敗時true
// Input = 使うテクスチャのパス ダブルポインタ用
// numdata = 要素数
// plate = 板ポリの種類
// bCenter = 中心座標を下の真ん中にします
bool CBillboard::Init(const char ** Input, int numdata, PlateList plate, bool bCenter)
{
	for (int i = 0; i < numdata; i++)
	{
		if (this->Init(Input[i], plate, bCenter))
		{
			return true;
		}
	}
	return false;
}

//==========================================================================
// アニメーション用初期化 失敗時true
// Input = 使うテクスチャのパス ダブルポインタ用
// numdata = 要素数
// UpdateFrame = 更新フレーム
// Pattern = アニメーション数
// Direction = 横一列のアニメーション数
// plate = 板ポリの種類
// bCenter = 中心座標を下の真ん中にします
bool CBillboard::Init(const char ** Input, int numdata, int UpdateFrame, int Pattern, int Direction, PlateList plate, bool bCenter)
{
	for (int i = 0; i < numdata; i++)
	{
		if (this->Init(Input[i], UpdateFrame, Pattern, Direction, plate, bCenter))
		{
			return true;
		}
	}
	return false;
}

//==========================================================================
// 更新
void CBillboard::Update(D3DXMATRIX * MtxView, int * AnimationCount)
{
    // 登録済みオブジェクトのデータの更新
    for (auto itr = this->m_ObjectData.begin(); itr != this->m_ObjectData.end(); ++itr)
    {
        CAnimationParam *buf = this->m_buffer_path.Get(itr->m_3dobj->getindex());

        // アニメーション用カウンタとアニメーション情報が存在するとき
        if (AnimationCount != nullptr&&buf->m_pAnimation != nullptr)
        {
            this->AnimationPalam(buf, AnimationCount);
            this->UV(buf);
        }

        // 板ポリの種類
        this->BillboardType(buf, itr->m_3dobj, itr->MtxWorld, MtxView);
    }
}

//==========================================================================
// 解放
void CBillboard::Release(void)
{
    this->m_tex.Release();
    this->m_buffer_origin.Release();
    this->m_buffer_path.Release();
    this->ObjectRelease();
}

//==========================================================================
// アニメーション更新
// AnimationCount = アニメーション用変数を入れるところ
void CBillboard::UpdateAnimation(int & AnimationCount)
{
	AnimationCount++;
}

//==========================================================================
// 描画
void CBillboard::BaseDraw(bool bADD)
{
    if (this->m_buffer_path.Size() != 0 && this->m_ObjectData.size() != 0)
    {
        LPDIRECT3DDEVICE9 pDevice = CDeviceManager::GetDXDevice()->GetD3DDevice();

        // FVFの設定
        pDevice->SetFVF(this->FVF_VERTEX_4);

        // 表示モード操作がある場合
        if (bADD)
        {
            this->SetRenderALPHAREF_END(pDevice);

            // 全加算合成
            this->SetRenderSUB(pDevice);
            this->SetRenderZWRITEENABLE_START(pDevice);
        }

        // 登録済みオブジェクトのデータを元に描画
        for (auto itr = this->m_ObjectData.begin(); itr != this->m_ObjectData.end(); ++itr)
        {
            CAnimationParam *buf = this->m_buffer_path.Get(itr->m_3dobj->getindex());

            // バッファ
            this->VertexUpdate(buf, itr->m_3dobj);

            // パイプライン
            pDevice->SetStreamSource(0, buf->m_buffer.m_pVertexBuffer, 0, sizeof(VERTEX_4));

            // インデックス登録
            pDevice->SetIndices(buf->m_buffer.m_pIndexBuffer);

            // テクスチャの登録
            pDevice->SetTexture(0, nullptr);
            pDevice->SetTexture(0, this->m_tex.get(itr->m_3dobj->getindex())->gettex());

            // トランスフォーム
            pDevice->SetTransform(D3DTS_WORLD, &itr->MtxWorld);

            // 描画設定
            pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2);
        }

        // 元に戻す
        if (bADD)
        {
            this->SetRenderADD(pDevice);
            this->SetRenderZWRITEENABLE_END(pDevice);
            this->SetRenderALPHAREF_START(pDevice, 100);
        }
    }
}

//==========================================================================
// 頂点の更新
void CBillboard::VertexUpdate(CAnimationParam *ptexture, C3DObject * pInput)
{
    ptexture->m_buffer.m_pVertexBuffer->Lock(0, 0, (void**)&ptexture->m_buffer.m_pPseudo, D3DLOCK_DISCARD);
    ptexture->m_buffer.m_pPseudo[0].Tex = D3DXVECTOR2(ptexture->m_uv.u0, ptexture->m_uv.v0);
    ptexture->m_buffer.m_pPseudo[1].Tex = D3DXVECTOR2(ptexture->m_uv.u1, ptexture->m_uv.v0);
    ptexture->m_buffer.m_pPseudo[2].Tex = D3DXVECTOR2(ptexture->m_uv.u1, ptexture->m_uv.v1);
    ptexture->m_buffer.m_pPseudo[3].Tex = D3DXVECTOR2(ptexture->m_uv.u0, ptexture->m_uv.v1);

    // 色が設定されているとき
    if (pInput->GetCollar() != nullptr)
    {
        ptexture->m_buffer.m_pPseudo[0].color = pInput->GetCollar()->get();
        ptexture->m_buffer.m_pPseudo[1].color = pInput->GetCollar()->get();
        ptexture->m_buffer.m_pPseudo[2].color = pInput->GetCollar()->get();
        ptexture->m_buffer.m_pPseudo[3].color = pInput->GetCollar()->get();
    }
    ptexture->m_buffer.m_pVertexBuffer->Unlock();	// ロック解除
}

//==========================================================================
// ビルボードの種類
void CBillboard::BillboardType(CAnimationParam *ptexture, C3DObject * pInput, D3DXMATRIX & MtxWorld, D3DXMATRIX * MtxView)
{
    // 板ポリの種類
    switch (ptexture->m_buffer.m_plate)
    {
    case PlateList::Vertical:
        if (MtxView != nullptr)
        {
            pInput->SetMtxView(MtxView);
            pInput->SetMatrixType(C3DObject::EMatrixType::MirrorVector);
            pInput->CreateMtxWorld(MtxWorld);
        }
        else if (MtxView == nullptr)
        {
            pInput->SetMatrixType(C3DObject::EMatrixType::Vector1);
            pInput->CreateMtxWorld(MtxWorld);
        }
        break;
    case PlateList::Up:
        if (MtxView != nullptr)
        {
            pInput->SetMtxView(MtxView);
            pInput->SetMatrixType(C3DObject::EMatrixType::MirrorNotVector);
            pInput->CreateMtxWorld(MtxWorld);
        }
        else if (MtxView == nullptr)
        {
            pInput->SetMatrixType(C3DObject::EMatrixType::Vector1);
            pInput->CreateMtxWorld(MtxWorld);
        }
        break;
    default:
        break;
    }
}

//==========================================================================
// 頂点バッファの生成
void CBillboard::CreateVertex(CAnimationParam *ptexture, bool bCenter)
{
	// 板ポリの種類
	switch (ptexture->m_buffer.m_plate)
	{
	case PlateList::Vertical:
	{
		if (bCenter == true)
		{
			VERTEX_4 vVertex[] =
			{
				// 手前
				{ D3DXVECTOR3(-VCRCT, VCRCT,0.0f),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(ptexture->m_uv.u0,ptexture->m_uv.v0) }, // 左上
				{ D3DXVECTOR3(VCRCT, VCRCT,0.0f),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(ptexture->m_uv.u1,ptexture->m_uv.v0) }, // 右上
				{ D3DXVECTOR3(VCRCT,-VCRCT,0.0f),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(ptexture->m_uv.u1,ptexture->m_uv.v1) }, // 右下
				{ D3DXVECTOR3(-VCRCT,-VCRCT,0.0f),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(ptexture->m_uv.u0,ptexture->m_uv.v1) }, // 左下
			};

			for (int i = 0; i < (int)this->m_temp.Sizeof_(vVertex); i++)
			{
                ptexture->m_buffer.m_pPseudo[i] = vVertex[i];
			}
		}
		else if (bCenter == false)
		{
			VERTEX_4 vVertex[] =
			{
				// 手前
				{ D3DXVECTOR3(-VCRCT, 1.0f,0.0f),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(ptexture->m_uv.u0,ptexture->m_uv.v0) }, // 左上
				{ D3DXVECTOR3(VCRCT, 1.0f,0.0f),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(ptexture->m_uv.u1,ptexture->m_uv.v0) }, // 右上
				{ D3DXVECTOR3(VCRCT,0.0f,0.0f),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(ptexture->m_uv.u1,ptexture->m_uv.v1) }, // 右下
				{ D3DXVECTOR3(-VCRCT,0.0f,0.0f),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(ptexture->m_uv.u0,ptexture->m_uv.v1) }, // 左下
			};

			for (int i = 0; i < (int)this->m_temp.Sizeof_(vVertex); i++)
			{
                ptexture->m_buffer.m_pPseudo[i] = vVertex[i];
			}
		}
	}
	break;
	case PlateList::Up:
	{
		VERTEX_4 vVertex[] =
		{
			// 上
			{ D3DXVECTOR3(-VCRCT,0.0f, VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(ptexture->m_uv->u0,ptexture->m_uv->v0) }, // 左上
			{ D3DXVECTOR3(VCRCT,0.0f, VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(ptexture->m_uv->u1,ptexture->m_uv->v0) }, // 右上
			{ D3DXVECTOR3(VCRCT,0.0f,-VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(ptexture->m_uv->u1,ptexture->m_uv->v1) }, // 右下
			{ D3DXVECTOR3(-VCRCT,0.0f,-VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(ptexture->m_uv->u0,ptexture->m_uv->v1) }, // 左下
		};
		for (int i = 0; i < (int)this->m_temp.Sizeof_(vVertex); i++)
		{
            ptexture->m_buffer.m_pPseudo[i] = vVertex[i];
		}
	}
	break;
	default:
		break;
	}
}

//==========================================================================
// アニメーション情報のセット
void CBillboard::AnimationPalam(CAnimationParam *ptexture, int * AnimationCount)
{
	int PattanNum = ((*AnimationCount) / ptexture->m_pAnimation->Frame) % ptexture->m_pAnimation->Pattern;	//パターン数
	int patternV = PattanNum % ptexture->m_pAnimation->Direction; // 横方向のパターン
	int patternH = PattanNum / ptexture->m_pAnimation->Direction; // 縦方向のパターン

	ptexture->m_pAnimation->m_Cut.x = patternV * ptexture->m_pAnimation->m_Cut.w; // 切り取り座標X
	ptexture->m_pAnimation->m_Cut.y = patternH * ptexture->m_pAnimation->m_Cut.h; // 切り取り座標Y
}

//==========================================================================
// アニメーション切り替わり時の判定 切り替え時true 
// index = 判定路調べたい板ポリの番号
// AnimationCount = アニメーション用変数を入れるところ
bool CBillboard::GetPattanNum(int index, int * AnimationCount)
{
    CAnimationParam *buf = this->m_buffer_path.Get(index);

    // セットされているとき
    if (AnimationCount != nullptr)
    {
        // フレームが一致したとき
        if ((*AnimationCount) == (buf->m_pAnimation->Frame*buf->m_pAnimation->Pattern) - 1)
        {
            this->AnimationCountInit(AnimationCount);
            return true;
        }
    }
    return false;
}

//==========================================================================
// アニメーション用のカウンタの初期化 引数はカウンタ用変数
// AnimationCount = アニメーション用変数を入れるところ
int *CBillboard::AnimationCountInit(int * AnimationCount)
{
	if (AnimationCount != nullptr)
	{
		(*AnimationCount) = -1;
	}

	return AnimationCount;
}

//==========================================================================
// UVの生成
void CBillboard::UV(CAnimationParam *ptexture)
{
	// 左上
	ptexture->m_uv.u0 = (float)ptexture->m_pAnimation->m_Cut.x / ptexture->m_pAnimation->m_TexSize.w;
	ptexture->m_uv.v0 = (float)ptexture->m_pAnimation->m_Cut.y / ptexture->m_pAnimation->m_TexSize.h;

	// 左下
	ptexture->m_uv.u1 = (float)(ptexture->m_pAnimation->m_Cut.x + ptexture->m_pAnimation->m_Cut.w) / ptexture->m_pAnimation->m_TexSize.w;
	ptexture->m_uv.v1 = (float)(ptexture->m_pAnimation->m_Cut.y + ptexture->m_pAnimation->m_Cut.h) / ptexture->m_pAnimation->m_TexSize.h;
}

//==========================================================================
// 重複検索
bool CBillboard::OverlapSearch(PlateList plate)
{
    for (int i = 0; i < this->m_buffer_origin.Size(); i++)
    {
        CAnimationParam * buf = this->m_buffer_origin.Get(i);

        // 板ポリの種類が同じとき
        if (buf->m_buffer.m_plate == plate)
        {
            CAnimationParam * bufsub = this->m_buffer_path.Create();
            bufsub->m_buffer.m_pVertexBuffer = buf->m_buffer.m_pVertexBuffer;
            bufsub->m_buffer.m_pIndexBuffer = buf->m_buffer.m_pIndexBuffer;
            bufsub->m_buffer.m_plate = buf->m_buffer.m_plate;
            bufsub->m_buffer.m_pPseudo = buf->m_buffer.m_pPseudo;
            bufsub->m_buffer.m_origin = false;
            return true;
        }
    }

	return false;
}

//==========================================================================
// バッファの生成
bool CBillboard::CreateBuffer(PlateList plate, bool bCenter)
{
	WORD* pIndex = nullptr;

	// 重複判定が出なかったとき
	if (!this->OverlapSearch(plate))
	{
		// メモリ確保
        CAnimationParam * buf = this->m_buffer_origin.Create();

        buf->m_buffer.m_plate = plate;

		if (this->CreateVertexBuffer(sizeof(VERTEX_4) * 4, D3DUSAGE_WRITEONLY, FVF_VERTEX_4, D3DPOOL_MANAGED, &buf->m_buffer.m_pVertexBuffer, nullptr))
		{
			return true;
		}

		if (this->CreateIndexBuffer(sizeof(WORD) * 6, D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &buf->m_buffer.m_pIndexBuffer, nullptr))
		{
			return true;
		}

        buf->m_buffer.m_pVertexBuffer->Lock(0, 0, (void**)&buf->m_buffer.m_pPseudo, D3DLOCK_DISCARD);
		this->CreateVertex(buf, bCenter);
        buf->m_buffer.m_pVertexBuffer->Unlock();	// ロック解除

        buf->m_buffer.m_pIndexBuffer->Lock(0, 0, (void**)&pIndex, D3DLOCK_DISCARD);
		this->RectangleIndex(pIndex, 1);
        buf->m_buffer.m_pIndexBuffer->Unlock();	// ロック解除

        buf->m_buffer.m_origin = true;

        CAnimationParam * bufsub = this->m_buffer_path.Create();
        bufsub->m_buffer.m_pVertexBuffer = buf->m_buffer.m_pVertexBuffer;
        bufsub->m_buffer.m_pIndexBuffer = buf->m_buffer.m_pIndexBuffer;
        bufsub->m_buffer.m_plate = buf->m_buffer.m_plate;
        bufsub->m_buffer.m_pPseudo = buf->m_buffer.m_pPseudo;
        bufsub->m_buffer.m_origin = false;
	}

	return false;
}

//==========================================================================
// アニメーション情報の生成
void CBillboard::CreateAnimation(int UpdateFrame, int Pattern, int Direction)
{
	int nCount = 0;

	// メモリ確保
    CAnimationParam * buf = this->m_buffer_path.Get(this->m_buffer_path.Size() - 1);
	this->m_temp.New_(buf->m_pAnimation);

	*buf->m_pAnimation = CAnimation(UpdateFrame, Pattern, Direction, CTexvec<float>(0.0f, 0.0f, 0.0f, 0.0f), *this->m_tex.get(this->m_tex.size() - 1)->getsize());

    buf->m_pAnimation->m_Cut.w = (float)buf->m_pAnimation->m_TexSize.w / buf->m_pAnimation->Direction;

	// 切り取る縦幅を計算
	for (int i = 0;; i += buf->m_pAnimation->Direction)
	{
		if (buf->m_pAnimation->Pattern <= i) { break; }
		nCount++;
	}

    buf->m_pAnimation->m_Cut.h = (float)buf->m_pAnimation->m_TexSize.h / nCount;
    buf->m_pAnimation->m_Cut.x = buf->m_pAnimation->m_Cut.y = 0.0f;
}

//==========================================================================
// 解放
void CBillboard::CBuffer::Release(void)
{
    if (this->m_origin)
    {
        // バッファ解放
        this->m_temp.Release_(this->m_pVertexBuffer);

        // インデックスバッファ解放
        this->m_temp.Release_(this->m_pIndexBuffer);
    }

	this->m_pPseudo = nullptr;
}

//==========================================================================
// 解放
void CBillboard::CAnimationParam::Release(void)
{
    this->m_temp.Delete_(this->m_pAnimation);
}