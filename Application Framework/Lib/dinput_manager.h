//==========================================================================
// ダイレクインプットマネージャー[dinput_manager.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _dinput_manager_h_
#define _dinput_manager_h_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#define DIRECTINPUT_VERSION (0x0800) // DirectInputのバージョン指定
#include<dinput.h>
#pragma comment(lib,"dinput8.lib")
#pragma comment(lib,"dxguid.lib")
#include <list>

//==========================================================================
//
// class  : CDirectInputManager
// Content: DirectInputManager
//
//==========================================================================
class CDirectInputManager
{
private:
    // コピー禁止 (C++11)
    CDirectInputManager(const CDirectInputManager &) = delete;
    CDirectInputManager &operator=(const CDirectInputManager &) = delete;
protected:
    CDirectInputManager() {
        this->m_DirectInput.emplace_back(this);
    }
    virtual ~CDirectInputManager() {}
    // 更新
    virtual void Update(void) = 0;
public:
    // 登録済みデバイスの更新
    static void UpdateAll(void);

    // 登録済みデバイスの破棄 
    static void ReleaseALL(void);

    // デバイスの生成
    // p = new T();
    template <typename T> static T* NewDirectInput(T*& p);
private:
    static std::list<CDirectInputManager*> m_DirectInput; // デバイスの格納
};

//==========================================================================
// p = new T;
template<typename T>
inline T * CDirectInputManager::NewDirectInput(T *& p)
{
    p = nullptr;
    p = new T;

    return p;
}

//==========================================================================
//
// class  : CDirectInput
// Content: DirectInput
//
//==========================================================================
class CDirectInput
{
private:
    // コピー禁止 (C++11)
    CDirectInput(const CDirectInput &) = delete;
    CDirectInput &operator=(const CDirectInput &) = delete;
public:
    CDirectInput() {
        this->m_DInput = nullptr;
        this->m_DIDevice = nullptr;
    }
    virtual ~CDirectInput() {
        this->ReleaseDevice();
    }
private:
    // 解放
    void ReleaseDevice(void) {
        if (this->m_DIDevice != nullptr)
        {// 入力デバイス(キーボード)の開放
         // キーボードへのアクセス権を開放(入力制御終了)
            this->m_DIDevice->Unacquire();

            this->m_DIDevice->Release();
            this->m_DIDevice = nullptr;
        }

        if (this->m_DInput != nullptr)
        {// DirectInputオブジェクトの開放
            this->m_DInput->Release();
            this->m_DInput = nullptr;
        }
    }
protected:
    LPDIRECTINPUT8 m_DInput; // DirectInputオブジェクトへのポインタ
    LPDIRECTINPUTDEVICE8 m_DIDevice; // 入力デバイス(キーボード)へのポインタ
};

#endif // !_dinput_manager_h_
