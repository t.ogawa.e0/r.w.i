//==========================================================================
// カメラ[Camera.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _Camera_H_
#define _Camera_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>

//==========================================================================
//
// class  : CCamera
// Content: カメラ
//
//==========================================================================
class CCamera // カメラ
{
public:
	enum class VectorList
	{
		VEYE = 0, // 注視点
		VAT, // カメラ座標
		VUP, // ベクター
		VECUP, // 上ベクトル
		VECFRONT, // 前ベクトル
		VECRIGHT, //  右ベクトル
	};
private:
	// 平行処理
	void CameraMoveXYZ(D3DXVECTOR3 * pVec, D3DXVECTOR3 *pOut1, D3DXVECTOR3 *pOut2, const float *pSpeed);

	// X軸回転処理
	void CameraRangX(D3DXVECTOR3 *pDirection, D3DXMATRIX *pRot, D3DXVECTOR3 *pOut1, D3DXVECTOR3 *pOut2, const float *pRang);

	// Y軸回転処理
	void CameraRangY(D3DXVECTOR3 *pDirection, D3DXMATRIX *pRot, D3DXVECTOR3 *pOut1, D3DXVECTOR3 *pOut2, const float *pRang);

	// 視点変更 戻り値,視点とカメラの距離
	float ViewPos(D3DXVECTOR3 *pVec, D3DXVECTOR3 *pOut1, D3DXVECTOR3 *pOut2, const float *pSpeed);

	// 内積
	bool Restriction(D3DXMATRIX pRot, const float* pRang);
public:
	CCamera()
	{
		this->m_Eye = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		this->m_At = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		this->m_Eye2 = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		this->m_At2 = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		this->m_Up = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		this->m_VecUp = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		this->m_VecFront = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		this->m_VecRight = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		this->m_ViewPort = nullptr;
	}
	~CCamera()
	{
		if (this->m_ViewPort != nullptr)
		{
			delete[]this->m_ViewPort;
			this->m_ViewPort = nullptr;
		}
	}

	// 初期化
	void Init(void);

	// 初期化
	// pEye = 注視点
	// pAt = カメラ座標
	void Init(const D3DXVECTOR3 & pEye, const D3DXVECTOR3 & pAt);

	// 解放
	void Release(void);

    // 更新
    // この関数は一か所に設置すれば全て自動で処理されます
    // MtxView = ビュー行列を入れてください
    // fWidth = 画面の幅を入れてください
    // fHeight = 画面の高さを入れてください
    // pDevice = デバイスを入れてください
    void Update(int fWidth, int fHeight, LPDIRECT3DDEVICE9 pDevice);

    // 更新 ビューポートmode
    // この関数は一か所に設置すれば全て自動で処理されます
    // MtxView = ビュー行列を入れてください
    // ViewPort = ビューポートの設定
    // fWidth = 画面の幅を入れてください
    // fHeight = 画面の高さを入れてください
    // pDevice = デバイスを入れてください
    void UpdateViewPort(const D3DXMATRIX * MtxView, const D3DVIEWPORT9 * ViewPort, int fWidth, int fHeight, LPDIRECT3DDEVICE9 pDevice);

    // ビュー行列生成
    D3DXMATRIX *CreateView(void);

    // 視点中心にX軸回転
	// Rang = 入れた値が加算されます
	void RotViewX(float Rang);

	// 視点中心にY軸回転
	// Rang = 入れた値が加算されます
	void RotViewY(float Rang);

	// カメラ中心にX軸回転
	// Rang = 入れた値が加算されます
	void RotCameraX(float Rang);

	// カメラ中心にY軸回転
	// Rang = 入れた値が加算されます
	void RotCameraY(float Rang);

	// X軸平行移動
	// Speed = 入れた値が加算されます
	void MoveX(float Speed);

	// Y軸平行移動
	// Speed = 入れた値が加算されます
	void MoveY(float Speed);

	// Z軸平行移動
	// Speed = 入れた値が加算されます
	void MoveZ(float Speed);

	// 視点変更 戻り値,視点とカメラの距離
	// Distance = 入れた値が加算されます
	float DistanceFromView(float Distance);

	// ベクターの取得
	// List = カメラベクトルの種類
	D3DXVECTOR3 GetVECTOR(VectorList List);

	// カメラY回転情報
	float GetRestriction(void);

	// カメラ座標をセット
	// Eye = 注視点
	// At = カメラ座標
	// Up = ベクター
	void SetCameraPos(const D3DXVECTOR3 & Eye, const D3DXVECTOR3 & At, const D3DXVECTOR3 & Up) { this->m_Eye = Eye; this->m_At = At; this->m_Up = Up; }

	// カメラ座標
	// At = カメラ座標
	void SetAt(const D3DXVECTOR3 & At) { this->m_At2 = At; }

	// 注視点
	// Eye = 注視点
	void SetEye(const D3DXVECTOR3 & Eye) { this->m_Eye2 = Eye; }
public:
	D3DVIEWPORT9 *m_ViewPort; // ビューポート
private:
	D3DXMATRIX m_aMtxView; // ビュー行列
	D3DXVECTOR3 m_Eye; // 注視点
	D3DXVECTOR3 m_At; // カメラ座標
	D3DXVECTOR3 m_Eye2; // 注視点
	D3DXVECTOR3 m_At2; // カメラ座標
	D3DXVECTOR3 m_Up; // ベクター
	D3DXVECTOR3 m_VecUp; // 上ベクトル
	D3DXVECTOR3 m_VecFront; // 前ベクトル
	D3DXVECTOR3 m_VecRight; //  右ベクトル
};

#endif // !_Camera_H_
