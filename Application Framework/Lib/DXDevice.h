//==========================================================================
// デバイス[DXDevice.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _DXDevice_H_
#define _DXDevice_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "AspectRatio.h"

//==========================================================================
//
// class  : CDirectXDevice
// Content: デバイス 
//
//==========================================================================
class CDirectXDevice
{
private:
    // コピー禁止 (C++11)
    CDirectXDevice(const CDirectXDevice &) = delete;
    CDirectXDevice &operator=(const CDirectXDevice &) = delete;
public:
    //==========================================================================
    //
    // class  : CWindowsSize
    // Content: ウィンドウサイズ 
    //
    //==========================================================================
    template<typename types>
    class CWindowsSize
    {
    public:
        CWindowsSize() { this->m_Width = (types)0;this->m_Height = (types)0; }
        CWindowsSize(types Width, types Height) { this->m_Width = Width; this->m_Height = Height; }
        ~CWindowsSize() {}
    public:
        types m_Width;
        types m_Height;
    };
public:
    CDirectXDevice() {
        this->m_pD3D = nullptr;
        this->m_pD3DDevice = nullptr;
        this->m_hwnd = nullptr;
        this->m_screenbuffscale = 1.0f;
    }
    ~CDirectXDevice() {
        this->Release();
    }

    // 初期化 失敗時true
    bool Init(void);

    // 解放
    void Release(void);

    // ウィンドウモードの生成
    bool CreateWindowMode(const CAspectRatio::data_t & data, bool Mode);

    // デバイスの生成
    bool CreateDevice(void);

    // デバイスの習得
    LPDIRECT3DDEVICE9 GetD3DDevice(void) { return this->m_pD3DDevice; };

    // デバイスの習得
    D3DPRESENT_PARAMETERS Getd3dpp(void) { return this->m_d3dpp; };

    // ウィンドウサイズ
    CWindowsSize<int> GetWindowsSize(void) { return this->m_WindowsSize; }

    // ウィンドウハンドルの記録
    void SetHwnd(HWND hWnd) { this->m_hwnd = hWnd; }

    // ウィンドウハンドルのゲッター
    HWND GetHwnd(void) { return this->m_hwnd; }

    // スクリーンバッファ誤差修正
    float screenbuffscale(void) { return this->m_screenbuffscale; }

    // 描画開始
    bool DrawBegin(void);

    // 描画終了
    HRESULT STDMETHODCALLTYPE DrawEnd(const RECT* SourceRect, const RECT* DestRect, HWND hDestWindowOverride, const RGNDATA* DirtyRegion);

    // 描画終了
    HRESULT STDMETHODCALLTYPE DrawEnd(void) { return this->DrawEnd(nullptr, nullptr, nullptr, nullptr); }

    // エラーメッセージ
    template <typename ... Args>
    void ErrorMessage(const char * text, Args const & ... args);
private:
    LPDIRECT3D9 m_pD3D; //ダイレクト3Dインターフェース
    LPDIRECT3DDEVICE9 m_pD3DDevice;//ダイレクト3Dデバイス
    CWindowsSize<int> m_WindowsSize; // ウィンドウサイズ
    D3DPRESENT_PARAMETERS m_d3dpp;
    D3DDISPLAYMODE m_d3dpm;
    HWND m_hwnd; // ウィンドウハンドル
    std::vector<D3DDISPLAYMODE> m_d3dspMode; // ディスプレイモード管理
    float m_screenbuffscale; // 推奨サイズとの描画サイズの誤差
};

//==========================================================================
// エラーメッセージ
template<typename ...Args>
inline void CDirectXDevice::ErrorMessage(const char * text, Args const & ...args)
{
    char pfon[1024] = { 0 };

    sprintf(pfon, text, args ...);

    MessageBox(this->m_hwnd, pfon, "ERROR", MB_OK | MB_ICONEXCLAMATION | MB_ICONWARNING);
}

#endif // !_DXDevice_H_
