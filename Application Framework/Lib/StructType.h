//==========================================================================
// structを使った定数(よろしくない)[StructType.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _StructType_H_
#define _StructType_H_

//==========================================================================
//
// struct : ANGLE
// Content: 回転情報のテンプレート
//
//==========================================================================
struct ANGLE_t
{
	static constexpr float	_ANGLE_000 = 0.00000000f; // 0度
	static constexpr float	_ANGLE_045 = 0.785398185f; // 45度
	static constexpr float	_ANGLE_090 = 1.57079637f; // 90度
	static constexpr float	_ANGLE_135 = 2.35619450f; // 135度
	static constexpr float	_ANGLE_180 = 3.14159274f; // 180度
	static constexpr float	_ANGLE_225 = -2.35619450f; // 225度
	static constexpr float	_ANGLE_270 = -1.57079637f; // 270度
	static constexpr float	_ANGLE_315 = -0.785398185f; // 315度
};

#endif // !_StructType_H_
