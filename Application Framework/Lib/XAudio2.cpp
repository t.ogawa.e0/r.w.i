//==========================================================================
// サウンド[XAudio2.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "XAudio2.h"

//==========================================================================
// 実体
//==========================================================================
IXAudio2 *CXAudio2Device::m_pXAudio2 = nullptr; // XAudio2オブジェクトへのインターフェイス
HWND CXAudio2Device::m_hWnd = nullptr; // ウィンドハンドル

//==========================================================================
// 初期化
HRESULT CXAudio2Device::Init(HWND hWnd)
{
	m_pXAudio2 = nullptr;
	m_pMasteringVoice = nullptr;
	m_hWnd = hWnd;

	// COMライブラリの初期化
    if (FAILED(CoInitializeEx(nullptr, COINIT_MULTITHREADED)))
    {
        MessageBox(hWnd, "COMライブラリの初期化に失敗しました", "error", MB_ICONWARNING);

        // COMライブラリの終了処理
        CoUninitialize();

        return E_FAIL;
    }

	// XAudio2オブジェクトの作成
	if (FAILED(XAudio2Create(&m_pXAudio2, 0)))
	{
		MessageBox(hWnd, "XAudio2オブジェクトの作成に失敗しました", "error", MB_ICONWARNING);

		// COMライブラリの終了処理
		CoUninitialize();

		return E_FAIL;
	}

	// マスターボイスの生成
	if (FAILED(m_pXAudio2->CreateMasteringVoice(&m_pMasteringVoice)))
	{
		MessageBox(hWnd, "マスターボイスの生成に失敗しました", "error", MB_ICONWARNING);

		// XAudio2オブジェクトの開放
        if (m_pXAudio2 != nullptr)
        {
            m_pXAudio2->Release();
            m_pXAudio2 = nullptr;
        }

		// COMライブラリの終了処理
		CoUninitialize();

		return E_FAIL;
	}

	return S_OK;
}

//==========================================================================
// 解放
void CXAudio2Device::Release(void)
{
	// マスターボイスの破棄
	DestroyVoice();

    // XAudio2オブジェクトの開放
    if (m_pXAudio2 != nullptr)
    {
        m_pXAudio2->Release();
        m_pXAudio2 = nullptr;
    }

	// COMライブラリの終了処理
	CoUninitialize();
}

//==========================================================================
// ボイスデータの破棄
void CXAudio2Device::DestroyVoice(void)
{
	if (m_pMasteringVoice != nullptr)
	{
		m_pMasteringVoice->DestroyVoice();
		m_pMasteringVoice = nullptr;
	}
}

//==========================================================================
// 初期化
HRESULT CXAudio2::Init(const SoundLabel & Input, int & OutLabel)
{
    HANDLE hFile;
    DWORD dwChunkSize = 0;
    DWORD dwChunkPosition = 0;
    DWORD dwFiletype;
    WAVEFORMATEXTENSIBLE wfx;
    XAUDIO2_BUFFER buffer;
    CParam *pSound = nullptr;
    HWND phWnd = CXAudio2Device::GetWnd();
    IXAudio2 * pXAudio2 = CXAudio2Device::GetAudio2();

    // インスタンスの生成
    pSound = this->m_Sound.Create();

    // バッファのクリア
    memset(&wfx, 0, sizeof(WAVEFORMATEXTENSIBLE));
    memset(&buffer, 0, sizeof(XAUDIO2_BUFFER));

    // サウンドデータファイルの生成
    hFile = CreateFile(Input.m_strName.c_str(), GENERIC_READ, FILE_SHARE_READ, nullptr, OPEN_EXISTING, 0, nullptr);
    if (hFile == INVALID_HANDLE_VALUE)
    {
        MessageBox(phWnd, "サウンドデータファイルの生成に失敗(1)", "警告", MB_ICONWARNING);
        return HRESULT_FROM_WIN32(GetLastError());
    }

    // ファイルポインタを先頭に移動
    if (SetFilePointer(hFile, 0, nullptr, FILE_BEGIN) == INVALID_SET_FILE_POINTER)
    {
        MessageBox(phWnd, "サウンドデータファイルの生成に失敗(2)", "警告", MB_ICONWARNING);
        return HRESULT_FROM_WIN32(GetLastError());
    }

    // WAVEファイルのチェック
    if (FAILED(this->CheckChunk(hFile, 'FFIR', &dwChunkSize, &dwChunkPosition)))
    {
        MessageBox(phWnd, "WAVEファイルのチェックに失敗(1)", "警告", MB_ICONWARNING);
        return S_FALSE;
    }

    if (FAILED(this->ReadChunkData(hFile, &dwFiletype, sizeof(DWORD), dwChunkPosition)))
    {
        MessageBox(phWnd, "WAVEファイルのチェックに失敗(2)", "警告", MB_ICONWARNING);
        return S_FALSE;
    }

    if (dwFiletype != 'EVAW')
    {
        MessageBox(phWnd, "WAVEファイルのチェックに失敗(3)", "警告", MB_ICONWARNING);
        return S_FALSE;
    }

    // フォーマットチェック
    if (FAILED(this->CheckChunk(hFile, ' tmf', &dwChunkSize, &dwChunkPosition)))
    {
        MessageBox(phWnd, "フォーマットチェックに失敗(1)", "警告", MB_ICONWARNING);
        return S_FALSE;
    }

    if (FAILED(this->ReadChunkData(hFile, &wfx, dwChunkSize, dwChunkPosition)))
    {
        MessageBox(phWnd, "フォーマットチェックに失敗(2)", "警告", MB_ICONWARNING);
        return S_FALSE;
    }

    // オーディオデータ読み込み
    if (FAILED(this->CheckChunk(hFile, 'atad', &pSound->m_SizeAudio, &dwChunkPosition)))
    {
        MessageBox(phWnd, "オーディオデータ読み込みに失敗(1)", "警告", MB_ICONWARNING);
        return S_FALSE;
    }

    // メモリ確保
    this->m_temp.New_(pSound->m_pDataAudio, pSound->m_SizeAudio);

    if (FAILED(this->ReadChunkData(hFile, pSound->m_pDataAudio, pSound->m_SizeAudio, dwChunkPosition)))
    {
        MessageBox(phWnd, "オーディオデータ読み込みに失敗(2)", "警告", MB_ICONWARNING);
        return S_FALSE;
    }

    // ソースボイスの生成
    if (FAILED(pXAudio2->CreateSourceVoice(&pSound->m_pSourceVoice, &(wfx.Format))))
    {
        MessageBox(phWnd, "ソースボイスの生成に失敗！", "警告！", MB_ICONWARNING);
        return S_FALSE;
    }

    //// バッファの値設定
    //memset(&buffer, 0, sizeof(XAUDIO2_BUFFER));
    //buffer.AudioBytes = pSound->m_SizeAudio;
    //buffer.pAudioData = pSound->m_pDataAudio;
    //buffer.Flags = XAUDIO2_END_OF_STREAM;
    //buffer.LoopCount = Input.m_CntLoop;

    // カウント記憶
    pSound->m_CntLoop = Input.m_CntLoop;

    // ボリューム記憶
    pSound->m_Volume = Input.m_Volume;

    this->SetVolume(pSound, Input.m_Volume);

    //// オーディオバッファの登録
    //pSound->m_pSourceVoice->SubmitSourceBuffer(&buffer);

    // ラベルの登録
    OutLabel = (int)this->m_Sound.Size() - 1;

    return S_OK;
}

//==========================================================================
// 解放
void CXAudio2::Release(void)
{
    // 登録済みデータの破棄
    this->m_Sound.Release();
}

//==========================================================================
// 特定のサウンドの破棄
void CXAudio2::PinpointRelease(int label)
{
    this->m_Sound.PinpointRelease(this->m_Sound.Get(label));
}

//==========================================================================
// 再生
HRESULT CXAudio2::Play(int label)
{
	XAUDIO2_BUFFER buffer;
    CParam *pSound = this->m_Sound.Get(label);

    if (pSound != nullptr)
    {
        // バッファの値設定
        memset(&buffer, 0, sizeof(XAUDIO2_BUFFER));
        buffer.AudioBytes = pSound->m_SizeAudio;
        buffer.pAudioData = pSound->m_pDataAudio;
        buffer.Flags = XAUDIO2_END_OF_STREAM;
        buffer.LoopCount = pSound->m_CntLoop;

        this->Stop(pSound);

        pSound->m_pSourceVoice->SetVolume(pSound->m_Volume);

        // オーディオバッファの登録
        pSound->m_pSourceVoice->SubmitSourceBuffer(&buffer);

        // 再生
        pSound->m_pSourceVoice->Start(0);
    }

	return S_OK;
}

//==========================================================================
// 特定のサウンド停止
void CXAudio2::Stop(CParam * pSound)
{
    if (pSound != nullptr)
    {
        XAUDIO2_VOICE_STATE xa2state;

        // 状態取得
        pSound->m_pSourceVoice->GetState(&xa2state);
        if (xa2state.BuffersQueued != 0)
        {// 再生中
         // 一時停止
            pSound->m_pSourceVoice->Stop(0);

            // オーディオバッファの削除
            pSound->m_pSourceVoice->FlushSourceBuffers();
        }
    }
}

//==========================================================================
// 格納してある全てのサウンド停止
void CXAudio2::Stop(void)
{
    for (int i = 0; i < this->m_Sound.Size(); i++)
    {
        this->Stop(this->m_Sound.Get(i));
    }
}

//==========================================================================
// 特定のサウンドのボリューム設定
// volume 最大=1.0f
// volume 無音=0.0f
void CXAudio2::SetVolume(int label, float volume)
{
    CParam * par = this->m_Sound.Get(label);

    if (par != nullptr)
    {
        par->m_Volume = volume;
        par->m_pSourceVoice->SetVolume(par->m_Volume);
    }
}

//==========================================================================
// 全てのサウンドのボリューム設定
// volume 最大=1.0f
// volume 無音=0.0f
void CXAudio2::SetVolume(float volume)
{
    for (int i = 0; i < this->m_Sound.Size(); i++)
    {
        CParam * par = this->m_Sound.Get(i);

        if (par != nullptr)
        {
            par->m_Volume = volume;
            par->m_pSourceVoice->SetVolume(par->m_Volume);
        }
    }
}

//==========================================================================
// 全てのサウンドのボリューム自動設定
void CXAudio2::SetVolume(void)
{
    for (int i = 0; i < this->m_Sound.Size(); i++)
    {
        CParam * par = this->m_Sound.Get(i);

        if (par != nullptr)
        {
            if (par->m_CntLoop == 0)
            {
                par->m_Volume = 1.0f;
                par->m_pSourceVoice->SetVolume(par->m_Volume);
            }
            else if (par->m_CntLoop == -1)
            {
                par->m_Volume = 0.5f;
                par->m_pSourceVoice->SetVolume(par->m_Volume);
            }
        }
	}
}

//==========================================================================
// 特定のサウンドのボリューム設定
// volume 最大=1.0f
// volume 無音=0.0f
void CXAudio2::SetVolume(CParam * pSound, float volume)
{
    if (pSound != nullptr)
    {
        pSound->m_Volume = volume;
        pSound->m_pSourceVoice->SetVolume(volume);
    }
}

//==========================================================================
// 特定のサウンドのボリュームの取得
const float CXAudio2::GetVolume(int label)
{
    CParam * par = this->m_Sound.Get(label);
    float Volume = 0.0;

    if (par != nullptr)
    {
        par->m_pSourceVoice->GetVolume(&Volume);
    }
    return Volume;
}

//==========================================================================
// ボイスデータの破棄
void CXAudio2::CParam::Release(void)
{
    XAUDIO2_VOICE_STATE xa2state;

    // サウンドが再生中の場合
    if (this->m_pSourceVoice != nullptr)
    {
        this->m_pSourceVoice->GetState(&xa2state);
        if (xa2state.BuffersQueued != 0)
        {// 再生中
         // 一時停止
            this->m_pSourceVoice->Stop(0);

            // オーディオバッファの削除
            this->m_pSourceVoice->FlushSourceBuffers();
        }

        // ソースボイスの破棄
        this->m_pSourceVoice->DestroyVoice();
        this->m_pSourceVoice = nullptr;

        // オーディオデータの開放
        this->m_temp.Delete_(this->m_pDataAudio);
    }
}

//==========================================================================
// チャンクのチェック
HRESULT CXAudio2::CheckChunk(HANDLE hFile, DWORD format, DWORD * pChunkSize, DWORD * pChunkDataPosition)
{
	HRESULT hr = S_OK;
	DWORD dwRead;
	DWORD dwChunkType;
	DWORD dwChunkDataSize;
	DWORD dwRIFFDataSize = 0;
	DWORD dwFileType;
	DWORD dwBytesRead = 0;
	DWORD dwOffset = 0;

	// ファイルポインタを先頭に移動
	if (SetFilePointer(hFile, 0, nullptr, FILE_BEGIN) == INVALID_SET_FILE_POINTER)
	{
		return HRESULT_FROM_WIN32(GetLastError());
	}

	while (hr == S_OK)
	{
		// チャンクの読み込み
		if (ReadFile(hFile, &dwChunkType, sizeof(DWORD), &dwRead, nullptr) == 0)
		{
			hr = HRESULT_FROM_WIN32(GetLastError());
		}

		// チャンクデータの読み込み
		if (ReadFile(hFile, &dwChunkDataSize, sizeof(DWORD), &dwRead, nullptr) == 0)
		{
			hr = HRESULT_FROM_WIN32(GetLastError());
		}

		switch (dwChunkType)
		{
		case 'FFIR':
			dwRIFFDataSize = dwChunkDataSize;
			dwChunkDataSize = 4;

			// ファイルタイプの読み込み
			if (ReadFile(hFile, &dwFileType, sizeof(DWORD), &dwRead, nullptr) == 0)
			{
				hr = HRESULT_FROM_WIN32(GetLastError());
			}
			break;
		default:
			// ファイルポインタをチャンクデータ分移動
			if (SetFilePointer(hFile, dwChunkDataSize, nullptr, FILE_CURRENT) == INVALID_SET_FILE_POINTER)
			{
				return HRESULT_FROM_WIN32(GetLastError());
			}
		}

		dwOffset += sizeof(DWORD) * 2;
		if (dwChunkType == format)
		{
			*pChunkSize = dwChunkDataSize;
			*pChunkDataPosition = dwOffset;

			return S_OK;
		}

		dwOffset += dwChunkDataSize;
		if (dwBytesRead >= dwRIFFDataSize)
		{
			return S_FALSE;
		}
	}

	return S_OK;
}

//==========================================================================
// チャンクデータの読み込み
HRESULT CXAudio2::ReadChunkData(HANDLE hFile, void * pBuffer, DWORD dwBuffersize, DWORD dwBufferoffset)
{
	DWORD dwRead;

	// ファイルポインタを指定位置まで移動
	if (SetFilePointer(hFile, dwBufferoffset, nullptr, FILE_BEGIN) == INVALID_SET_FILE_POINTER)
	{
		return HRESULT_FROM_WIN32(GetLastError());
	}

	// データの読み込み
	if (ReadFile(hFile, pBuffer, dwBuffersize, &dwRead, nullptr) == 0)
	{
		return HRESULT_FROM_WIN32(GetLastError());
	}

	return S_OK;
}
