//==========================================================================
// 数字処理[Number.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Number.h"

//==========================================================================
// 定数定義
//==========================================================================
constexpr int Pusyu_Score = 10; // 繰り上げ定数

//==========================================================================
// 初期化
// AutoTextureResize = テクスチャのサイズ自動調節機能 trueで有効化
bool CNumber::Init(const std::string strTexName, bool AutoTextureResize)
{
    this->m_Data.Create();
    return this->m_poly->Init(strTexName.c_str(), AutoTextureResize);
}

//==========================================================================
// Digit 桁
// LeftAlignment 左寄せするかしないか
// Zero ゼロ描画判定
void CNumber::Set(int index, int Digit, bool LeftAlignment, bool Zero, const CVector2<float> v2pos)
{
    CData * pda = this->m_Data.Get(index);
    *pda->m_masterv2pos = v2pos;

    int MaxScore = Pusyu_Score;

    for (int i = 1; i < Digit; i++)
    {
        MaxScore *= Pusyu_Score;
    }
    for (int i = 0; i < Digit; i++)
    {
        pda->m_param->Create();
    }
    MaxScore--;

    *pda->m_list = CList(Digit, MaxScore, Zero, LeftAlignment);
}

//==========================================================================
// アニメーション用初期化
// index = テクスチャ番号
// Frame = 更新フレーム 
// Pattern = アニメーション数
// Direction = 横一列のアニメーション数
void CNumber::SetAnim(int index, int Frame, int Pattern, int Direction)
{
    CData * pdata = this->m_Data.Get(index);
    pdata->index = index;
    pdata->Frame = Frame;
    pdata->Pattern = Pattern;
    pdata->Direction = Direction;

    for (int i = 0; i < pdata->m_param->Size(); i++)
    {
        pdata->m_param->Get(i)->m_2DPos.Init(index, Frame, Pattern, Direction);
    }
}

//==========================================================================
// 解放
void CNumber::Release(void)
{
    this->m_poly->Release();
    this->m_Data.Release();
    delete this->m_poly;
}

//==========================================================================
// 更新
CVector2<float> CNumber::Update(int index, int SetNumber)
{
    CData * pda = this->m_Data.Get(index);

    // nullではないとき
    if (pda != nullptr)
    {
        float fWidht = (float)this->m_poly->GetTexSize(pda->index)->w / pda->Direction;
        int nDigit = pda->m_list->m_Digit;
        CVector2<float> pluspos = *pda->m_masterv2pos;

        //左詰め対応
        if (pda->m_list->m_LeftAlignment == true)
        {
            int Set = SetNumber;

            nDigit = 1;
            for (;;)
            {
                Set /= Pusyu_Score;
                if (Set == 0)
                {
                    break;
                }
                nDigit++;
                if (nDigit == pda->m_list->m_Digit)
                {
                    break;
                }
            }
        }

        SetNumber = this->Min(pda->m_list->m_Max, SetNumber);

        for (int i = nDigit; i >= 0; i--)
        {
            CParam * ppar = pda->m_param->Get(i - 1);

            this->m_poly->ObjectInput(ppar->m_2DPos);

            pluspos.x = pda->m_masterv2pos->x + (i - 1)*fWidht;

            ppar->m_2DPos.SetAnimationCount(SetNumber%Pusyu_Score);

            // 123456
            ppar->m_2DPos.SetPos(CVector4<float>(pluspos.x, pluspos.y));

            // 123456
            SetNumber /= Pusyu_Score;

            // 12345.6
            if (!pda->m_list->m_Zero && SetNumber == 0)
            {
                break;
            }
        }

        this->m_poly->Update();
        return pluspos;
    }
    return CVector2<float>();
}

//==========================================================================
// 描画
void CNumber::Draw(bool ADD)
{
    this->m_poly->Draw(ADD);
    this->m_poly->ObjectRelease();
}