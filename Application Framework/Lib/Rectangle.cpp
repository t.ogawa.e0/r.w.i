//==========================================================================
// 矩形[Rectangle.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Rectangle.h"

//==========================================================================
// 定数定義
//==========================================================================
constexpr int NumDfaltIndex = 6;

//==========================================================================
// Index
void CRectangle::RectangleIndex(WORD * Output, int NumRectangle)
{
	for (int i = 0, s = 0, ncount = 0; i < NumDfaltIndex*NumRectangle; i++, s++, ncount++)
	{
		switch (ncount)
		{
		case 3:
			s -= 3;
			break;
		case 4:
			s += 1;
			break;
		case 6:
			ncount = 0;
			break;
		default:
			break;
		}
		Output[i] = (WORD)s;
	}
}
