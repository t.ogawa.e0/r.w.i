//==========================================================================
// 球体[Screen.cpp]
// author: tatuya ogawa
//==========================================================================
#include "Sphere.h"

//==========================================================================
// 初期化
// Input = 使用するテクスチャのパス
// SubdivisionRatio = 品質 20あれば十分綺麗な球体になる
bool CSphere::Init(const char * ptex, int SubdivisionRatio)
{
	LPDIRECT3DDEVICE9 pDevice = CDeviceManager::GetDXDevice()->GetD3DDevice();
	CSphereData* matdata = nullptr;

	if (this->m_texture.init(ptex)) { return true; }

	if (SubdivisionRatio <= 2)
	{
		SubdivisionRatio = 2;
	}

    if (this->OverlapSearch(matdata, SubdivisionRatio) == false)
    {
        matdata = this->m_matdata.Create();

        if (matdata->CreateSphere(SubdivisionRatio, pDevice))
        {
            return true;
        }

		matdata->convert();

		matdata->setmat();

        matdata->m_origin = true;
	}

    CSphereData* psps = this->m_matdata_path.Create();
    psps->copy(matdata);

	return false;
}

//==========================================================================
// 初期化
// Input = 使用するテクスチャのパス ダブルポインタに対応
// num = 要素数
// SubdivisionRatio = 品質 20あれば十分綺麗な球体になる
bool CSphere::Init(const char ** ptex, int num, int SubdivisionRatio)
{
	for (int i = 0; i < num; i++)
	{
		if (this->Init(ptex[i], SubdivisionRatio))
		{
			return true;
		}
	}

	return false;
}

//==========================================================================
// 初期化
// Input = 使用するテクスチャのパス ダブルポインタに対応
// num = 要素数
// SubdivisionRatio = 品質 20あれば十分綺麗な球体になる
bool CSphere::Init(const char ** ptex, int num, int *SubdivisionRatio)
{
	for (int i = 0; i < num; i++)
	{
		if (this->Init(ptex[i], SubdivisionRatio[i]))
		{
			return true;
		}
	}

	return false;
}

//==========================================================================
// 更新
void CSphere::Update(void)
{
    // 登録済みオブジェクトのデータの更新
    for (auto itr = this->m_ObjectData.begin(); itr != this->m_ObjectData.end(); ++itr)
    {
        // 行列がデフォルトの指定の時
        if (itr->m_3dobj->GetMatrixType() == C3DObject::EMatrixType::Default)
        {
            itr->m_3dobj->SetMatrixType(C3DObject::EMatrixType::Vector1);
        }

        itr->m_3dobj->CreateMtxWorld(itr->MtxWorld);
    }
}

//==========================================================================s
// 解放
void CSphere::Release(void)
{
    this->m_matdata.Release();
    this->m_matdata_path.Release();
	this->m_texture.Release();
    this->ObjectRelease();
}

//==========================================================================
// 描画
void CSphere::Draw(void)
{
    if (this->m_texture.size() != 0 && this->m_ObjectData.size() != 0)
    {
        LPDIRECT3DDEVICE9 pDevice = CDeviceManager::GetDXDevice()->GetD3DDevice();

        // 登録済みオブジェクトのデータを元に描画
        for (auto itr = this->m_ObjectData.begin(); itr != this->m_ObjectData.end(); ++itr)
        {
            CSphereData* matdata = this->m_matdata_path.Get(itr->m_3dobj->getindex());

            pDevice->SetFVF(matdata->m_mesh2->GetFVF());

            // マテリアルの設定
            pDevice->SetMaterial(&matdata->m_mat);

            pDevice->SetTexture(0, nullptr);
            pDevice->SetTexture(0, this->m_texture.get(itr->m_3dobj->getindex())->gettex());

            pDevice->SetTransform(D3DTS_WORLD, &itr->MtxWorld);
            matdata->m_mesh2->DrawSubset(0);
        }
    }
}

//==========================================================================
// 重複検索
bool CSphere::OverlapSearch(CSphereData *& pOut, int SubdivisionRatio)
{
    for (int i = 0; i < this->m_matdata.Size(); i++)
    {
        CSphereData * pinp = this->m_matdata.Get(i);

        if (SubdivisionRatio == pinp->m_SubdivisionRatio)
        {
            pOut = pinp;
            return true;
        }
    }

	return false;
}

//==========================================================================
// 解放
void CSphere::CSphereData::Release(void)
{
	if (this->m_origin)
	{
		this->m_temp.Release_(this->m_vertexbuffer);
		this->m_temp.Release_(this->m_mesh2);
		this->m_temp.Release_(this->m_mesh1);
		this->m_pseudo = nullptr;
        this->m_SubdivisionRatio = 0;
	}
}

//==========================================================================
// クリエイト
bool CSphere::CSphereData::CreateSphere(int SubdivisionRatio, LPDIRECT3DDEVICE9 pDevice)
{
	this->m_SubdivisionRatio = SubdivisionRatio;
	if (FAILED(D3DXCreateSphere(pDevice, 1.0f, this->m_SubdivisionRatio * 2, this->m_SubdivisionRatio, &this->m_mesh1, nullptr)))
	{
		return true;
	}

	this->m_mesh1->CloneMeshFVF(0, this->FVF_VERTEX_4, pDevice, &this->m_mesh2);
	this->m_mesh2->GetVertexBuffer(&this->m_vertexbuffer);

	return false;
}

//==========================================================================
// コンバート
void CSphere::CSphereData::convert(void)
{
	float r = 1.0f;
	this->m_vertexbuffer->Lock(0, 0, (void**)&this->m_pseudo, 0);
	for (DWORD i = 0; i < this->m_mesh2->GetNumVertices(); i++)
	{
		float q = 0.0f;
		float q2 = 0.0f;
		VERTEX_4* pPseudo = &this->m_pseudo[i];

		q = atan2(pPseudo->pos.z, pPseudo->pos.x);

		pPseudo->Tex.x = q / (2.0f * 3.1415f);
		q2 = asin(pPseudo->pos.y / r);
		pPseudo->Tex.y = (1.0f - q2 / (3.1415f / 2.0f)) / 2.0f;
		if (pPseudo->Tex.x > 1.0f)
		{
			pPseudo->Tex.x = 1.0f;
		}

        pPseudo->Tex = -pPseudo->Tex;
		pPseudo->color = D3DCOLOR_RGBA(255, 255, 255, 255);
		pPseudo->Normal = D3DXVECTOR3(1, 1, 1);
	}
	this->m_vertexbuffer->Unlock();
}

//==========================================================================
// マテリアル情報のセット
void CSphere::CSphereData::setmat(void)
{
	// マテリアルの設定
	ZeroMemory(&this->m_mat, sizeof(this->m_mat));
	this->m_mat.Diffuse.r = 1.0f; // ディレクショナルライト
	this->m_mat.Diffuse.g = 1.0f; // ディレクショナルライト
	this->m_mat.Diffuse.b = 1.0f; // ディレクショナルライト
	this->m_mat.Diffuse.a = 1.0f; // ディレクショナルライト
	this->m_mat.Ambient.r = 0.7f; // アンビエントライト
	this->m_mat.Ambient.g = 0.7f; // アンビエントライト
	this->m_mat.Ambient.b = 0.7f; // アンビエントライト
	this->m_mat.Ambient.a = 1.0f; // アンビエントライト
}

//==========================================================================
// コピー
void CSphere::CSphereData::copy(CSphereData * pinp)
{
	this->m_SubdivisionRatio = pinp->m_SubdivisionRatio;
	this->m_mat = pinp->m_mat;
	this->m_mesh1 = pinp->m_mesh1;
	this->m_mesh2 = pinp->m_mesh2;
	this->m_vertexbuffer = pinp->m_vertexbuffer;
	this->m_pseudo = pinp->m_pseudo;
	this->m_origin = false;
}
