//==========================================================================
// 影[Shadow.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Shadow.h"

//==========================================================================
// 定数定義
//==========================================================================
constexpr float VCRCT = 0.5f;
constexpr int DefaltColor = 80;
constexpr int ColorBuf = 8;

//==========================================================================
// 初期化 失敗時true
// Input = 使用するテクスチャのパス
bool CShadow::Init(const char * Input)
{
	CUv<float> uUV;
	WORD* pIndex;

	if (this->m_Lock == false)
	{
		if (this->m_texture.init(Input))
		{
			return true;
		}

		this->m_pIndexBuffer = nullptr;
		this->m_pVertexBuffer = nullptr;

		uUV = CUv<float>(0.0f, 0.0f, 1.0f, 1.0f);

		if (this->CreateVertexBuffer(sizeof(VERTEX_4) * 4, D3DUSAGE_WRITEONLY, FVF_VERTEX_4, D3DPOOL_MANAGED, &this->m_pVertexBuffer, nullptr))
		{
			return true;
		}

		if (this->CreateIndexBuffer(sizeof(WORD) * 6, D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &this->m_pIndexBuffer, nullptr))
		{
			return true;
		}

		this->m_pVertexBuffer->Lock(0, 0, (void**)&this->m_pPseudo, D3DLOCK_DISCARD);
		this->VertexBuffer(this->m_pPseudo, &uUV);
		this->m_pVertexBuffer->Unlock(); // ロック解除

		this->m_pIndexBuffer->Lock(0, 0, (void**)&pIndex, D3DLOCK_DISCARD);
		this->RectangleIndex(pIndex, 1);
		this->m_pIndexBuffer->Unlock();	// ロック解除
	}

	return false;
}

//==========================================================================
// 更新
void CShadow::Update(void)
{
    // 登録済みオブジェクトのデータの更新
    for (auto itr = this->m_ObjectData.begin(); itr != this->m_ObjectData.end(); ++itr)
    {
        C3DObject aObject = *itr->m_3dobj; // 複製
        const D3DXVECTOR3 *pos = aObject.GetMatInfoPos(); // 座標の取得

        // 距離から色を出す
        int nColor = DefaltColor - (int)((pos->y * ColorBuf) + 0.5f);

        // 回転行列初期化
        aObject.SetMatInfoRot({ 0, 0, 0 });

        // サイズの設定
        aObject.SetMatInfoSca({ pos->x*(pos->y + 1.0f) ,pos->y*(pos->y + 1.0f) ,pos->z*(pos->y + 1.0f) });

        // マイナスの値回避
        if (nColor <= 0) { nColor = 0; }
        if (pos->y <= 0) { nColor = 0; }

        // 色を登録
        itr->color = CColor<int>(255, 255, 255, nColor);

        // 各種行列の設定
        aObject.SetMatInfoPos({ pos->x,0.0f,pos->z });

        // 行列がデフォルトの指定の時
        if (aObject.GetMatrixType() == C3DObject::EMatrixType::Default)
        {
            aObject.SetMatrixType(C3DObject::EMatrixType::NotVector);
        }

        // 行列を生成
        aObject.CreateMtxWorld(itr->MtxWorld);
    }
}

//==========================================================================
// 解放
void CShadow::Release(void)
{
	// バッファ解放
	this->m_temp.Release_(this->m_pVertexBuffer);

	// インデックスバッファ解放
	this->m_temp.Release_(this->m_pIndexBuffer);

	this->m_texture.Release();

	this->m_Lock = false;

    this->ObjectRelease();
}

//==========================================================================
// 描画
void CShadow::Draw(void)
{
    if (this->m_texture.size() != 0 && this->m_ObjectData.size() != 0)
    {
        LPDIRECT3DDEVICE9 pDevice = CDeviceManager::GetDXDevice()->GetD3DDevice();

        // パイプライン
        pDevice->SetStreamSource(0, this->m_pVertexBuffer, 0, sizeof(VERTEX_4));

        pDevice->SetIndices(this->m_pIndexBuffer);

        // FVFの設定
        pDevice->SetFVF(this->FVF_VERTEX_4);

        // バッファを開く
        this->m_pVertexBuffer->Lock(0, 0, (void**)&this->m_pPseudo, D3DLOCK_DISCARD);

        // 登録済みオブジェクトのデータを元に描画
        for (auto itr = this->m_ObjectData.begin(); itr != this->m_ObjectData.end(); ++itr)
        {
            this->m_pPseudo[0].color = itr->color.get();
            this->m_pPseudo[1].color = itr->color.get();
            this->m_pPseudo[2].color = itr->color.get();
            this->m_pPseudo[3].color = itr->color.get();

            pDevice->SetTexture(0, nullptr);
            pDevice->SetTexture(0, this->m_texture.get(0)->gettex());

            // 各種行列の設定
            pDevice->SetTransform(D3DTS_WORLD, &itr->MtxWorld);

            // 描画設定
            pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2);
        }
        this->m_pVertexBuffer->Unlock(); // ロック解除
    }
}

//==========================================================================
// 頂点バッファの生成
void CShadow::VertexBuffer(VERTEX_4 * Output, CUv<float> * uv)
{
	VERTEX_4 vVertex[] =
	{
		// 上
		{ D3DXVECTOR3(-VCRCT,0.0f, VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,80),D3DXVECTOR2(uv->u0,uv->v0) }, // 左上
		{ D3DXVECTOR3(VCRCT,0.0f, VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,80),D3DXVECTOR2(uv->u1,uv->v0) }, // 右上
		{ D3DXVECTOR3(VCRCT,0.0f,-VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,80),D3DXVECTOR2(uv->u1,uv->v1) }, // 右下
		{ D3DXVECTOR3(-VCRCT,0.0f,-VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,80),D3DXVECTOR2(uv->u0,uv->v1) }, // 左下
	};

	for (int i = 0; i < (int)(sizeof(vVertex) / sizeof(VERTEX_4)); i++)
	{
		Output[i] = vVertex[i];
	}
}
