//==========================================================================
// ObjectInput[ObjectInput.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _ObjectInput_H_
#define _ObjectInput_H_

//==========================================================================
// include
//==========================================================================
#include "2DObject.h"
#include "3DObject.h"
#include "TextureLoader.h"
#include "DataType.h"
#include <list>

//==========================================================================
//
// class  : CObjectInput
// Content: オブジェクトの一括管理
//
//==========================================================================
class CObjectInput
{
private:
    // コピー禁止 (C++11)
    CObjectInput(const CObjectInput &) = delete;
    CObjectInput &operator=(const CObjectInput &) = delete;
private:
    class CObjectAddress : public VERTEX_3D
    {
    public:
        CObjectAddress() {
            this->m_2dobj = nullptr;
            this->m_3dobj = nullptr;
            this->MtxWorld = D3DXMATRIX(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        }
        ~CObjectAddress() {}
    public:
        C2DObject*m_2dobj; // 2Dオブジェクト
        C3DObject*m_3dobj; // 3Dオブジェクト
        D3DXMATRIX MtxWorld; // ワールド行列
        CColor<int> color; // 色
        VERTEX_3D::VERTEX_3 vertex_3[4]; // バーテックス3
    };
protected:
    CObjectInput() {}
    virtual ~CObjectInput() {
        this->m_ObjectData.clear();
    }
public:
    // オブジェクトのインプット
    void ObjectInput(C3DObject&Input) {
        this->ObjectInput(&Input);
    }

    // オブジェクトのインプット
    void ObjectInput(C3DObject*Input) {
        CObjectAddress obj;
        obj.m_3dobj = Input;
        this->m_ObjectData.emplace_back(obj);
    }

    // オブジェクトのインプット
    void ObjectInput(C2DObject&Input) {
        this->ObjectInput(&Input);
    }

    // オブジェクトのインプット
    void ObjectInput(C2DObject*Input) {
        CObjectAddress obj;
        obj.m_2dobj = Input;
        this->m_ObjectData.emplace_back(obj);
    }

    // 消去するオブジェクトの情報を教える
    void ObjectDelete(C2DObject&Input) {
        this->ObjectDelete(&Input);
    }

    // 消去するオブジェクトの情報を教える
    void ObjectDelete(C2DObject*Input) {
        for (auto itr = this->m_ObjectData.begin(); itr != this->m_ObjectData.end(); ++itr) {
            if (Input == itr->m_2dobj) {
                this->m_ObjectData.erase(itr);
                break;
            }
        }
    }

    // 消去するオブジェクトの情報を教える
    void ObjectDelete(C3DObject&Input) {
        this->ObjectDelete(&Input);
    }

    // 消去するオブジェクトの情報を教える
    void ObjectDelete(C3DObject*Input) {
        for (auto itr = this->m_ObjectData.begin(); itr != this->m_ObjectData.end(); ++itr) {
            if (Input == itr->m_3dobj) {
                this->m_ObjectData.erase(itr);
                break;
            }
        }
    }

    // 登録してあるデータ情報の破棄
    void ObjectRelease(void) {
        this->m_ObjectData.clear();
    }
protected:
    std::list<CObjectAddress>m_ObjectData; // オブジェクト
};

#endif // !_ObjectInput_H_
