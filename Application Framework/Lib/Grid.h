//==========================================================================
// グリッド[Grid.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _Grid_H_
#define _Grid_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "Vertex3D.h"
#include "Template.h"
#include "DeviceManager.h"

//==========================================================================
//
// class  : CGrid
// Content: グリッド
//
//==========================================================================
class CGrid : private VERTEX_3D
{
private:
    // コピー禁止 (C++11)
    CGrid(const CGrid &) = delete;
    CGrid &operator=(const CGrid &) = delete;
public:
    CGrid() {
        this->m_pos = nullptr;
        this->m_Device = nullptr;
        this->m_num = 0;
        this->m_scale = 0;
    }
	~CGrid() {
        this->Release();
    }

	// 初期化
	// scale = グリッドの表示範囲の指定
	void Init(int scale);

	// 解放
	void Release(void);

	// 描画
	void Draw(void);

	// デバイスのセット
	void SetDevice(const LPDIRECT3DDEVICE9 Input) { this->m_Device = Input; }
private:
	VERTEX_2 *m_pos; // 頂点の作成
	CTemplates m_temp; // テンプレート
	int m_num; // 線の本数管理
	int m_scale; // サイズの記録
	LPDIRECT3DDEVICE9 m_Device; // デバイス
};

#endif // !_Grid_H_
