//==========================================================================
// オブジェクト[3DObject.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _3DObject_H_
#define _3DObject_H_

//==========================================================================
// #define _SET3DObjectDrawKey_
// release時に向きベクトル視覚化の有効化
//==========================================================================
//#define _SET3DObjectDrawKey_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "DataType.h"

//==========================================================================
//
// class  : C3DObject
// Content: オブジェクト
//
//==========================================================================
class C3DObject
{
public:
	// 使うベクトルの種類の選択
	enum class EMatrixType
	{
		Default = 0, // デフォルト
		Vector1, // ベクトルの使用
		NotVector, // ベクトルを使用しません
		MirrorVector, // 逆ベクトルを使用
		MirrorNotVector, // 逆ベクトルを使用し、ベクトルを使用しません
	};
private:
	//==========================================================================
	//
	// class  : CMatLook
	// Content: 視覚情報
	//
	//==========================================================================
	class CMatLook
	{
	public:
		CMatLook()
		{
			this->Up = this->Eye = this->At = D3DXVECTOR3(0, 0, 0);
		}
		~CMatLook() {}
	public:
		D3DXVECTOR3 Eye; // 視点
		D3DXVECTOR3 At; // 座標
		D3DXVECTOR3 Up; // ベクター
	};

	//==========================================================================
	//
	// class  : CVec3
	// Content: ベクトル
	//
	//==========================================================================
	class CVec3
	{
	public:
		CVec3()
		{
			this->Up = this->Front = this->Right = D3DXVECTOR3(0, 0, 0);
		}
		~CVec3() {}
	public:
		D3DXVECTOR3 Up; // 上ベクトル
		D3DXVECTOR3 Front; // 前ベクトル
		D3DXVECTOR3 Right; //  右ベクトル
	};

	//==========================================================================
	//
	// class  : CObjInfo
	// Content: オブジェクトの情報
	//
	//==========================================================================
	class CObjInfo
	{
	public:
		CObjInfo()
		{
			this->pos = this->rot = this->sca = D3DXVECTOR3(0, 0, 0);
		}
		~CObjInfo() {}
	public:
		D3DXVECTOR3 pos; // 平行
		D3DXVECTOR3 rot; // 回転
		D3DXVECTOR3 sca; // スケール
	};

	//==========================================================================
	//
	// class  : CVertex
	// Content: 向きベクトル情報
	//
	//==========================================================================
	class CVertex
	{
	public:
		CVertex()
		{
			this->pos = D3DXVECTOR3(0, 0, 0);
			this->color = (DWORD)0;
		}
		~CVertex() {}
	public:
		D3DXVECTOR3 pos; // 座標変換が必要
		D3DCOLOR color; // ポリゴンの色
	};
public:
	C3DObject()
	{
		this->Collar = nullptr;
		this->m_MatType = EMatrixType::Default;
		this->m_MtxView = nullptr;
		this->m_Device = nullptr;
		this->Init(0);
	}
	~C3DObject()
	{
		this->DeleteCollar();
	}

	// 初期化
	// index = 使用するオブジェクトの番号
	void Init(int index);

	// 解放
	void Release(void);

	// 描画
	// NumLine = 線の細かさ
	// pDevice = デバイスをセットしてください
	void Draw(int NumLine, LPDIRECT3DDEVICE9 pDevice);

	// 色格納領域の初期化
	void NewCollar(void);

	// 色格納領域の初期化
	// Input = CColor<int>型で色指定してください
	void NewCollar(const CColor<int> & Input);

	const CColor<int> * GetCollar(void) const { return this->Collar; }

	// 色のセット
	// Input = CColor<int>型で色指定してください
	void SetCollar(const CColor<int> & Input);

	// 色の解放
	void DeleteCollar(void);

	// X軸回転
	// rot = 入力した値が加算されます
	void RotX(float rot);

    // X軸回転
    // rot = 入力したベクトルが入ります
    // power = 回転にかけるパワー
    // 戻り値 = 回転した角度
    float RotX(D3DXVECTOR3 & rot, float power);

	// Y軸回転
	// rot = 入力した値が加算されます
	void RotY(float rot);

	// 平行移動
	// speed = 入力した値が加算されます
	void MoveZ(float speed);

	// 平行移動
	// speed = 入力した値が加算されます
	void MoveX(float speed);

	// 平行移動
	// speed = 入力した値が加算されます
	void MoveY(float speed);

	// スケール
	// speed = 入力した値が加算されます
	void Scale(float speed);

	// ワールド行列の生成
	// 失敗時にnullが返る
	const D3DXMATRIX *CreateMtxWorld(D3DXMATRIX & pInOut);

	// ラジコン回転
	// vecRight = 向きベクトル
	// speed = 入力した値が加算されます
	void RadioControl(const D3DXVECTOR3 & vecRight, float speed);

	// 前ベクトルのセット
	// Input = 向きベクトル
	void SetVecFront(const D3DXVECTOR3 & Input) { this->Vec.Front = Input; }

	// 前ベクトルのゲッター
	const D3DXVECTOR3 * GetVecFront(void) const { return &this->Vec.Front; }

	// 上ベクトルのセット
	// Input = 向きベクトル
	void SetVecUp(const D3DXVECTOR3 & Input) { this->Vec.Up = Input; }

	// 上ベクトルのゲッター
	const D3DXVECTOR3 * GetVecUp(void) const { return &this->Vec.Up; }

	//  右ベクトルのセット
	// Input = 向きベクトル
	void SetVecRight(const D3DXVECTOR3 & Input) { this->Vec.Right = Input; }

	// 右ベクトルのゲッター
	const D3DXVECTOR3 * GetVecRight(void) const { return &this->Vec.Right; }

	// 上のどの方向を見ているかのベクトルのセット
	// Input = 向きベクトル
	void SetLockUp(const D3DXVECTOR3 & Input) { this->Look.Up = Input; }

	// 上のどの方向を見ているかのベクトルのゲッター
	const D3DXVECTOR3 * GetLockUp(void) const { return &this->Look.Up; }

	// 目の座標ベクトルセット
	// Input = 向きベクトル
	void SetLockAt(const D3DXVECTOR3 & Input) { this->Look.At = Input; }

	// 目の座標ベクトルのゲッター
	const D3DXVECTOR3 * GetLockAt(void) const { return &this->Look.At; }

	// 視線ベクトルのセット
	// Input = 向きベクトル
	void SetLockEye(const D3DXVECTOR3 & Input) { this->Look.Eye = Input; }

	// 視線ベクトルのゲッター
	const D3DXVECTOR3 * GetLockEye(void) const { return &this->Look.Eye; }

	// 座標行列のセット
	// Input = 向きベクトル
	void SetMatInfoPos(const D3DXVECTOR3 & Input) { this->Info.pos = Input; }

	// 座標行列のゲッター
	const D3DXVECTOR3 * GetMatInfoPos(void) const { return &this->Info.pos; }

	// 回転行列のセット
	// Input = 向きベクトル
	void SetMatInfoRot(const D3DXVECTOR3 & Input) { this->Info.rot = Input; }

	// 回転行列のゲッター
	const D3DXVECTOR3 * GetMatInfoRot(void) const { return &this->Info.rot; }

	// スケール行列のセット
	// Input = 向きベクトル
	void SetMatInfoSca(const D3DXVECTOR3 & Input) { this->Info.sca = Input; }

	// スケール行列のゲッター
	const D3DXVECTOR3 * GetMatInfoSca(void) const { return &this->Info.sca; }

	// 対象の方向に向かせる
	// Input = ターゲットのC3DObject
	// power = 回転にかけるパワー
	void LockOn(const C3DObject & Input, float power);

	// インデックス情報の取得 
	int getindex(void) const { return m_index; }

	// 使うベクトルのタイプ指定
	void SetMatrixType(const EMatrixType & type) { this->m_MatType = type; }

	// 使っているベクトルゲッター
	EMatrixType GetMatrixType(void) const { return this->m_MatType; }

	// ビュー行列の登録
	void SetMtxView(D3DXMATRIX * Input) { this->m_MtxView = Input; }

	// デバイスのセット
	void SetDevice(const LPDIRECT3DDEVICE9 Input) { this->m_Device = Input; }

    void SetIndex(int label) { this->m_index = label; }

	// 向きベクトルの補正
	// Out = 補正対象
	// Parent = 補正に使う親
	D3DXVECTOR3 * CorrectionVec3(D3DXVECTOR3 & Out, D3DXVECTOR3 & Parent);
private:
	// ベクトルを使った行列
	const D3DXMATRIX *CreateMtxWorld1(D3DXMATRIX & pInOut);

	// 直接値を入れる行列
	const D3DXMATRIX *CreateMtxWorld2(D3DXMATRIX & pInOut);

	// ベクトルを使ったカメラの方向を向かせる行列
	const D3DXMATRIX *CreateMtxWorld4(D3DXMATRIX & pInOut, D3DXMATRIX & pMtxView);

	// 直接値を入れるカメラの方向を向かせる行列
	const D3DXMATRIX *CreateMtxWorld5(D3DXMATRIX & pInOut, D3DXMATRIX & pMtxView);

	// 回転の制限
	bool Restriction(D3DXMATRIX pRot, const float * pRang);

	// 回転行列
	const D3DXMATRIX* CalcLookAtMatrix(D3DXMATRIX & pOut);

	// 回転行列
	const D3DXMATRIX* CalcLookAtMatrixAxisFix(D3DXMATRIX & pOut);
private:
	CMatLook Look; // 視覚情報
	CVec3 Vec; // ベクトル
	CObjInfo Info; // オブジェクトの情報
	CColor<int> * Collar = nullptr; // 色
	CVertex *m_RoundX;
	CVertex *m_RoundY;
	CVertex *m_lineX;
	CVertex *m_lineY;
	CVertex *m_lineZ;
	EMatrixType m_MatType;
	D3DXMATRIX *m_MtxView; // ビュー行列の登録
	LPDIRECT3DDEVICE9 m_Device; // デバイス
	int m_index; // データ
};

#endif // !_3DObject_H_
