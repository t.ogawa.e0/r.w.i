//==========================================================================
// オブジェクト管理[Object.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _CObject_H_
#define _CObject_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <list>
#include <string>

//==========================================================================
// 描画方法include
//==========================================================================
#include "SetSampler.h"
#include "SetRender.h"

//==========================================================================
// 座標
//==========================================================================
#include "ObjectVectorManager.h"
#include "2DObject.h"
#include "3DObject.h"

//==========================================================================
// 2D描画include
//==========================================================================
#include "2DPolygon.h"
#include "Number.h"

//==========================================================================
// 3D描画include
//==========================================================================
#include "Billboard.h"
#include "Cube.h"
#include "Mesh.h"
#include "Shadow.h"
#include "Sphere.h"
#include "Model.h"
#include "Grid.h"
#include "text.h"

//==========================================================================
// ヘルパー関数
//==========================================================================
#include "Template.h"
#include "DXDevice.h"
#include "ImGui_Dx9.h"
#include "HitDetermination.h"

//==========================================================================
// 入力処理
//==========================================================================
#include "XInput.h"

//==========================================================================
// サウンド系
//==========================================================================
#include "XAudio2.h"

//==========================================================================
// カメラ
//==========================================================================
#include "Camera.h"

//==========================================================================
//
// class  : CObject
// Content: オブジェクト管理クラス
//
//==========================================================================
class CObject : private CSetSampler, private CSetRender
{
public:
    // CObjectID
    enum class ID {
        Default, // default
        Camera, // カメラ
        Grid, // グリッド
        Field, // フィールド
        Cube, // キューブ
        Mesh, // メッシュ
        Shadow, // 影
        Sphere, // 球体
        Xmodel, // Xモデル
        Billboard, // ビルボード
        Polygon2D, // 2D
        Text, // テキスト
        SpecialBlock, // オブジェクトに認識されない領域
        MAX, // Object数
    };

    // オブジェクトのタイプ
    enum class Type {
        None,
        Game_Camera, // カメラ
        Game_Object, // ゲームオブジェクト
        Game_Human, // 人
        Game_Player, // プレイヤー
        Game_Enemy, // エネミー
        Game_WorldTime, // ゲームの世界時間
        Game_RelayPoint, // 中継地点
        Game_EmployeeManager, // 社員マネージャー
        Game_GamePlayerEmployee, // プレイヤーの社員データ
        Game_GameEnemyEmployee, // プレイヤーの社員データ
        Game_OperationExplanation, // 操作説明UI
        Game_Syokuin, // シャイン
        Game_GachaUI, // ガチャUI
        Game_Company_Date, // シシャ
        Game_EmployeeGatya, // シャイン表示
        Game_Start_Object, // ゲーム開始オブジェクト
        Game_End, // ゲーム終わり判定
        Game_Manager, // ゲームマネ,ージャー
        Game_Strike, // ストライキ演出
        Game_CharacterAnimation, // 転勤アニメージョン
        Game_Cursor, // 
        Game_Sinful_Image, // 社長の罪
        Result_ResultReminiscence, // リザルト回想
        Game_Game_Sound, // ゲームサウンド
        Title_InterruptToTitle, // タイトルへの割り込みプログラム
        Title_Warning, // 警告内容
        MAX,
    };
private:
    // コピー禁止 (C++11)
    CObject(const CObject &) = delete;
    CObject &operator=(const CObject &) = delete;
protected:
    CObject(ID ObjectID = ID::Default)
    {
        this->m_Type = Type::None;
        this->m_3DPos = nullptr;
        this->m_2DPos = nullptr;
        this->m_2DPolygon = nullptr;
        this->m_Billboard = nullptr;
        this->m_Cube = nullptr;
        this->m_Mesh = nullptr;
        this->m_A_CircleShadow = nullptr;
        this->m_Xmodel = nullptr;
        this->m_Sphere = nullptr;
        this->m_Grid = nullptr;
        this->m_Text = nullptr;
        this->m_XInput = nullptr;
        this->m_XAudio = nullptr;
        this->m_Camera = nullptr;
        this->m_Number = nullptr;
        this->m_initializer = false;
        if (ObjectID != ID::SpecialBlock)
        {
            this->m_object[(int)ObjectID].emplace_back(this);
        }
    }
    virtual ~CObject() {
        this->Delete(this->m_3DPos);
        this->Delete(this->m_2DPos);
        this->Delete(this->m_2DPolygon);
        this->Delete(this->m_Billboard);
        this->Delete(this->m_Cube);
        this->Delete(this->m_Mesh);
        this->Delete(this->m_A_CircleShadow);
        this->Delete(this->m_Xmodel);
        this->Delete(this->m_Sphere);
        this->Delete(this->m_Grid);
        this->Delete(this->m_Text);
        this->Delete(this->m_XInput);
        this->Delete(this->m_XAudio);
        this->Delete(this->m_Camera);
        this->Delete(this->m_Number);
    }

    // 継承初期化
    virtual bool Init(void) = 0;

    // 継承解放
    virtual void Uninit(void) = 0;

    // 継承更新
    virtual void Update(void) = 0;

    // 継承描画
    virtual void Draw(void) = 0;
public:
    // オブジェクトの取得
    CObject * GetObjects(const ID ObjectID, const Type ObjectType = Type::None);

    // 3Dオブジェクトインスタンスの取得
    CObjectVectorManager<C3DObject>*_3DObject(void) {
        return this->New(this->m_3DPos);
    }

    // 2Dオブジェクトインスタンスの取得
    CObjectVectorManager<C2DObject>*_2DObject(void) {
        return this->New(this->m_2DPos);
    }

    // 自分のオブジェクトタイプ
    Type GetType(void) { return this->m_Type; }

    // カメラのview行列
    D3DXMATRIX * GetMtxView(void) {
        return this->New(this->m_Camera)->CreateView();
    }

    // カメラへ追従情報を渡す
    void SetCameraLockOn(const D3DXVECTOR3 & vpos) {
        this->New(this->m_Camera)->SetAt(vpos);
        this->New(this->m_Camera)->SetEye(vpos);
    }
protected:
    // オブジェクトタイプ
    void SetType(const Type type) { this->m_Type = type; }

    // 2Dポリゴン
    C2DPolygon * _2DPolygon(void) {
        return this->New(this->m_2DPolygon);
    }

    // 数字
    CNumber * _2DNumber(void) {
        return this->New(this->m_Number);
    }

    // ビルボード
    CBillboard *_3DBillboard(void) {
        return this->New(this->m_Billboard);
    }

    // キューブ
    CCube *_3DCube(void) {
        return this->New(this->m_Cube);
    }

    // メッシュ
    CMesh *_3DMesh(void) {
        return this->New(this->m_Mesh);
    }

    // 丸影
    CShadow *_3DA_CircleShadow(void) {
        return this->New(this->m_A_CircleShadow);
    }

    // Xモデル
    CXmodel *_3DXmodel(void) {
        return this->New(this->m_Xmodel);
    }

    // 球体
    CSphere *_3DSphere(void) {
        return this->New(this->m_Sphere);
    }

    // グリッド
    CGrid *_3DGrid(void) {
        return this->New(this->m_Grid);
    }

    // テキスト
    CText *_2DText(void) {
        return this->New(this->m_Text);
    }

    // ヘルパー
    CTemplates * Helper(void) {
        return &this->m_Temp;
    }

    // ImGui
    CImGui_Dx9 * ImGui(void) {
        return &this->m_ImGui;
    }

    // XInput
    CXInput *XInput(void) {
        return this->New(this->m_XInput);
    }

    // XAudio
    CXAudio2 *XAudio(void) {
        return this->New(this->m_XAudio);
    }

    // カメラ
    CCamera *Camera(void) {
        return this->New(this->m_Camera);
    }

    // 当たり判定
    CHitDetermination * _Hit(void) {
        return &this->m_Hit;
    }

    // ダイレクトインプットkeyboard
    // 存在しない場合はnull
    static CKeyboard * Dinput_Keyboard(void) {
        return CDeviceManager::GetKeyboard();
    }

    // ダイレクトインプットマウス
    // 存在しない場合はnull
    static CMouse * Dinput_Mouse(void) {
        return CDeviceManager::GetMouse();
    }

    // ダイレクトインプットコントローラー(PS4)
    // 存在しない場合はnull
    static  CController * Dinput_Controller(void) {
        return CDeviceManager::GetController();
    }

    // ウィンドウサイズの取得
    static const CDirectXDevice::CWindowsSize<int> GetWinSize(void) {
        return CDeviceManager::GetDXDevice()->GetWindowsSize();
    }

    // DirectX9デバイスの取得
    static LPDIRECT3DDEVICE9 GetDirectX9Device(void) {
        return CDeviceManager::GetDXDevice()->GetD3DDevice();
    }
public:
    // オブジェクトの生成
    // p = new T();
    template <typename T> static T* NewObject(T*& p);

    // オブジェクトの生成
    // p = new T(...);
    template<typename T, typename ... _Valty> static T* NewObject(T *& p, _Valty && ..._Val);

    // 登録済みオブジェクトの初期化
    static bool InitAll(void);

    // 登録済みオブジェクトの解放
    static void ReleaseAll(void);

    // 登録済みオブジェクトの更新
    static void UpdateAll(void);

    // 登録済みオブジェクトの描画
    static void DrawAll(void);
private:
    // 描画の準備
    static void DrawPreparation(ID ObjectID);

    // 描画の後処理
    static void DrawPostProcessing(ID ObjectID);

    // delete
    template <typename T> void Delete(T*& p);

    // new
    template <typename T> T* New(T*& p);
private:
    static std::list<CObject*> m_object[(int)ID::MAX]; // オブジェクト格納
private:
    CObjectVectorManager<C3DObject>*m_3DPos; // 3Dオブジェクト座標インスタンス管理
    CObjectVectorManager<C2DObject>*m_2DPos; // 2Dオブジェクト座標インスタンス管理
    C2DPolygon *m_2DPolygon; // 2Dポリゴン
    CBillboard *m_Billboard; // ビルボード
    CCube *m_Cube; // キューブ
    CMesh *m_Mesh; // メッシュ
    CShadow *m_A_CircleShadow; // 丸影
    CXmodel *m_Xmodel; // Xモデル
    CSphere *m_Sphere; // 球体
    CGrid *m_Grid; // グリッド
    CText *m_Text; // テキスト
    CXInput *m_XInput; // XInput
    CXAudio2 *m_XAudio; // XAudio
    CCamera *m_Camera; // カメラ
    CNumber *m_Number; // 数字
    CTemplates m_Temp; // ヘルパー
    CImGui_Dx9 m_ImGui; // ImGui
    CHitDetermination m_Hit; // 当たり判定
    Type m_Type; // オブジェクトタイプ
    bool m_initializer; // 初期化フラグ
};

//==========================================================================
// p = new T;
template<typename T>
inline T * CObject::NewObject(T*& p)
{
    p = nullptr;
    p = new T;

    return p;
}

//==========================================================================
// p = new T(...);
template<typename T, typename ..._Valty>
inline T * CObject::NewObject(T *& p, _Valty && ..._Val)
{
    p = nullptr;
    p = new T((_Val)...);

    return p;
}

//==========================================================================
// p = new T;
template<typename T>
inline T * CObject::New(T *& p)
{
    // メモリが未確保時
    if (p == nullptr)
    {
        p = new T;
    }

    return p;
}

//==========================================================================
// メモリ解放 delete
template<typename T>
inline void CObject::Delete(T *& p)
{
    if (p != nullptr)
    {
        delete p;
        p = nullptr;
    }
}

#endif // !_CObject_H_
