//==========================================================================
// コントローラー[dinput_controller.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _dinput_controller_h_
#define _dinput_controller_h_

//==========================================================================
// include
//==========================================================================
#include "dinput_manager.h"

//==========================================================================
//
// class  : CController
// Content: コントローラー
//
//==========================================================================
class CController : public CDirectInput, public CDirectInputManager
{
public:
    // 方向キー2
    enum class CDirecyionkey
    {
        LeftButtonUp = 1,
        LeftButtonUpRight,
        LeftButtonRight,
        LeftButtonUnderRight,
        LeftButtonUnder,
        LeftButtonUnderLeft,
        LeftButtonLeft,
        LeftButtonUpLeft,
        MAX
    };

    // 方向キー1
    enum class CDirectionCkey
    {
        LeftButtonUp,
        LeftButtonUpRight,
        LeftButtonRight,
        LeftButtonUnderRight,
        LeftButtonUnder,
        LeftButtonUnderLeft,
        LeftButtonLeft,
        LeftButtonUpLeft,
        MAX
    };

    // ボタン
    enum class CButtonCkey
    {
        RightButton1,	// □ シカク
        RightButton2,	// Ｘ バツ
        RightButton3,	// 〇 マル
        RightButton4,	// △ サンカク
        L1Button,		// L1
        R1Button,		// R1
        L2Button,		// L2
        R2Button,		// R2
        SHAREButton,	// SHARE
        SOPTIONSButton,	// OPTIONS
        L3Button,		// L3
        R3Button,		// R3
        PSButton,		// PS
        TouchPad,		// TouchPad
        MAX
    };
private:
    //==========================================================================
    //
    // class  : CPS4
    // Content: PS4コントローラー
    //
    //==========================================================================
    class CStick
    {
    public:
        CStick() {
            this->m_LeftRight = (LONG)0;
            this->m_UpUnder = (LONG)0;
        }
        ~CStick() {}
    public:
        LONG m_LeftRight;
        LONG m_UpUnder;
    };
private:
    // コピー禁止 (C++11)
    CController(const CController &) = delete;
    CController &operator=(const CController &) = delete;
public:
    CController() {}
    ~CController() {}

    // 初期化
    bool Init(HINSTANCE hInstance, HWND hWnd);

    // 更新
    void Update(void)override;

    // 左スティック
    CStick LeftStick(void);

    // 右スティック
    CStick RightStick(void);

    // L2
    LONG L2(void);

    // R2
    LONG R2(void);

    // PS4 方向キー Num=Max3
    bool DirectionKey(CDirecyionkey Key);

    // 方向キー 
    // プレス
    bool Press(CDirectionCkey key);

    // トリガー
    bool Trigger(CDirectionCkey key);

    // リピート
    bool Repeat(CDirectionCkey key);

    // リリ−ス
    bool Release(CDirectionCkey key);

    // プレス
    bool Press(CButtonCkey key);

    // トリガー
    bool Trigger(CButtonCkey key);

    // リピート
    bool Repeat(CButtonCkey key);

    // リリ−ス
    bool Release(CButtonCkey key);

private:
    LONG Adjustment(LONG Set);

    CStick Stick(LONG Stick1, LONG Stick2);
private:
    static BOOL CALLBACK EnumJoysticksCallback(const DIDEVICEINSTANCE *pdidInstance, void *pContext);
    static BOOL CALLBACK EnumAxesCallback(const DIDEVICEOBJECTINSTANCE *pdidoi, void *pContext);
private:
    DIJOYSTATE m_State; // 入力情報ワーク
    BYTE m_StateTrigger[(int)sizeof(DIJOYSTATE::rgbButtons)]; // トリガー情報ワーク
    BYTE m_StateRelease[(int)sizeof(DIJOYSTATE::rgbButtons)]; // リリース情報ワーク
    BYTE m_StateRepeat[(int)sizeof(DIJOYSTATE::rgbButtons)]; // リピート情報ワーク
    int m_StateRepeatCnt[(int)sizeof(DIJOYSTATE::rgbButtons)]; // リピートカウンタ

    BYTE m_POVState[(int)CDirectionCkey::MAX]; // POV入力情報ワーク
    BYTE m_POVTrigger[(int)CDirectionCkey::MAX]; // トリガー情報ワーク
    BYTE m_POVRelease[(int)CDirectionCkey::MAX]; // リリース情報ワーク
    BYTE m_POVRepeat[(int)CDirectionCkey::MAX]; // リピート情報ワーク
    int m_POVRepeatCnt[(int)CDirectionCkey::MAX]; // リピートカウンタ
};

#endif // !_dinput_controller_h_
