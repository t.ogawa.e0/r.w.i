//==========================================================================
// 球体[Screen.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _Sphere_H_
#define _Sphere_H_

//==========================================================================
// Include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "Template.h"
#include "3DObject.h"
#include "DeviceManager.h"
#include "SetRender.h"
#include "Vertex3D.h"
#include "TextureLoader.h"
#include "ObjectInput.h"
#include "ObjectVectorManager.h"

//==========================================================================
//
// class  : CSphere
// Content: 球体
//
//==========================================================================
class CSphere : private CSetRender, private VERTEX_3D, public CObjectInput
{
private:
    // コピー禁止 (C++11)
    CSphere(const CSphere &) = delete;
    CSphere &operator=(const CSphere &) = delete;
private:
	//==========================================================================
	//
	// class  : CSphereData
	// Content: 球体のデータ管理
	//
	//==========================================================================
	class CSphereData : private VERTEX_3D
	{
	public:
        CSphereData() {
            this->m_origin = false;
            this->m_SubdivisionRatio = 0;
            this->m_mesh1 = nullptr;
            this->m_mesh2 = nullptr;
            this->m_vertexbuffer = nullptr;
            this->m_pseudo = nullptr;
            ZeroMemory(&this->m_mat, sizeof(this->m_mat));
        }
        ~CSphereData() {
            this->Release();
        }

		// 解放
		void Release(void);
		// クリエイト
		bool CreateSphere(int SubdivisionRatio, LPDIRECT3DDEVICE9 pDevice);
		// コンバート
		void convert(void);
		// マテリアル情報のセット
		void setmat(void);
		// コピー
		void copy(CSphereData* pinp);
	public:
		int m_SubdivisionRatio; // 球体の品質
		D3DMATERIAL9 m_mat; // マテリアルの情報
		LPD3DXMESH m_mesh1; // メッシュ
		LPD3DXMESH m_mesh2; // メッシュ
		LPDIRECT3DVERTEXBUFFER9 m_vertexbuffer; // バッファ
		VERTEX_4* m_pseudo;// 頂点バッファのロック
		bool m_origin; // 親データかの判定
	private:
		CTemplates m_temp; // テンプレート
	};
public:
	CSphere() {}
	~CSphere() {
        this->Release();
    }

	// 初期化
	// Input = 使用するテクスチャのパス
	// SubdivisionRatio = 品質 20あれば十分綺麗な球体になる
	bool Init(const char * ptex, int SubdivisionRatio);

	// 初期化
	// Input = 使用するテクスチャのパス ダブルポインタに対応
	// num = 要素数
	// SubdivisionRatio = 品質 20あれば十分綺麗な球体になる
	bool Init(const char ** ptex, int num, int SubdivisionRatio);

	// 初期化
	// Input = 使用するテクスチャのパス ダブルポインタに対応
	// num = 要素数
	// SubdivisionRatio = 品質 20あれば十分綺麗な球体になる
	bool Init(const char ** ptex, int num, int *SubdivisionRatio);

    // 更新
    void Update(void);

	// 解放
	void Release(void);

    // 描画
    void Draw(void);
private:
	// 重複検索
	bool OverlapSearch(CSphereData *& pOut, int SubdivisionRatio);
private:
	CTextureLoader m_texture; // テクスチャの格納
    CObjectVectorManager<CSphereData> m_matdata; // オリジナルデータ
    CObjectVectorManager<CSphereData> m_matdata_path; // コピーデータ
	CTemplates m_temp; // テンプレート
};

#endif // !_Sphere_H_
