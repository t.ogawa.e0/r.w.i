//==========================================================================
// キューブ[Cube.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Cube.h"

//==========================================================================
// 定数定義
//==========================================================================
constexpr int nBite = 256; // バイト数
constexpr int nMaxIndex = 36; // 
constexpr int nNumVertex = 24; // 頂点数
constexpr int nNumTriangle = 12; // 三角形の数
constexpr float VCRCT = 0.5f;
constexpr int NumDfaltIndex = 6;

//==========================================================================
// 初期化 失敗時true
// Input = 使用するテクスチャのパス ダブルポインタに対応
// size = 要素数 
bool CCube::Init(const char** Input, int size)
{
	for (int i = 0; i < size; i++)
	{
		if (this->Init(Input[i]))
		{
			return true;
		}
	}

	return false;
}

//==========================================================================
// 初期化 失敗時true
// Input = 使用するテクスチャのパス
bool CCube::Init(const char * Input)
{
	if (this->m_texture.init(Input))
	{
		return true;
	}

	if (!this->m_Lock)
	{
		CUv<float> aUv[6]; // 各面のUV
		VERTEX_4* pPseudo = nullptr;// 頂点バッファのロック
		WORD* pIndex = nullptr;
		TEXTURE texparam;

		texparam.Direction1 = 4;
		texparam.Direction2 = 4;
		texparam.Pattern = 6;

		// 画像データの格納
		texparam.texsize = CTexvec<float>(0, 0, 1.0f, 1.0f);
		texparam.texcutsize = CTexvec<float>(0, 0, 0, 0);
		texparam.texcutsize.w = texparam.texsize.w / texparam.Direction1;
		texparam.texcutsize.h = texparam.texsize.h / texparam.Direction2;

		for (int s = 0; s < texparam.Pattern; s++)
		{
			this->SetUV(&texparam, &aUv[s], s);
		}

		if (this->CreateVertexBuffer(sizeof(VERTEX_4) * nNumVertex, D3DUSAGE_WRITEONLY, this->FVF_VERTEX_4, D3DPOOL_MANAGED, &this->m_pVertexBuffer, nullptr))
		{
			return true;
		}

		if (this->CreateIndexBuffer(sizeof(WORD) * nMaxIndex, D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &this->m_pIndexBuffer, nullptr))
		{
			return true;
		}

		this->m_pVertexBuffer->Lock(0, 0, (void**)&pPseudo, D3DLOCK_DISCARD);
		this->SetCube(pPseudo, aUv);
		this->m_pVertexBuffer->Unlock();	// ロック解除

		this->m_pIndexBuffer->Lock(0, 0, (void**)&pIndex, D3DLOCK_DISCARD);
		this->RectangleIndex(pIndex, nMaxIndex / NumDfaltIndex);
		this->m_pIndexBuffer->Unlock();	// ロック解除

		// マテリアルの設定
		ZeroMemory(&this->m_aMat, sizeof(this->m_aMat));
		this->m_aMat.Diffuse.r = 1.0f; // ディレクショナルライト
		this->m_aMat.Diffuse.g = 1.0f; // ディレクショナルライト
		this->m_aMat.Diffuse.b = 1.0f; // ディレクショナルライト
		this->m_aMat.Diffuse.a = 1.0f; // ディレクショナルライト
		this->m_aMat.Ambient.r = 0.7f; // アンビエントライト
		this->m_aMat.Ambient.g = 0.7f; // アンビエントライト
		this->m_aMat.Ambient.b = 0.7f; // アンビエントライト
		this->m_aMat.Ambient.a = 1.0f; // アンビエントライト

		this->m_Lock = true;
	}

	return false;
}

//==========================================================================
// 更新
void CCube::Update(void)
{
    // 登録済みオブジェクトのデータの更新
    for (auto itr = this->m_ObjectData.begin(); itr != this->m_ObjectData.end(); ++itr)
    {
        // 行列がデフォルトの指定の時
        if (itr->m_3dobj->GetMatrixType() == C3DObject::EMatrixType::Default)
        {
            itr->m_3dobj->SetMatrixType(C3DObject::EMatrixType::NotVector);
        }

        // 行列を生成
        itr->m_3dobj->CreateMtxWorld(itr->MtxWorld);
    }
}

//==========================================================================
// 解放
void CCube::Release(void)
{
    // バッファ解放
    this->m_temp.Release_(this->m_pVertexBuffer);

    // インデックスバッファ解放
    this->m_temp.Release_(this->m_pIndexBuffer);

    this->m_texture.Release();

    this->ObjectRelease();
}

//==========================================================================
// 描画
// DrawType = ワールド行列の種類の指定をします
void CCube::Draw(void)
{
    if (this->m_texture.size() != 0 && this->m_ObjectData.size() != 0)
    {
        LPDIRECT3DDEVICE9 pDevice = CDeviceManager::GetDXDevice()->GetD3DDevice();

        // サイズ
        pDevice->SetStreamSource(0, this->m_pVertexBuffer, 0, sizeof(VERTEX_4)); // パイプライン

        pDevice->SetIndices(this->m_pIndexBuffer);

        // FVFの設定
        pDevice->SetFVF(this->FVF_VERTEX_4);

        // マテリアルの設定
        pDevice->SetMaterial(&this->m_aMat);

        // 登録済みオブジェクトのデータを元に描画
        for (auto itr = this->m_ObjectData.begin(); itr != this->m_ObjectData.end(); ++itr)
        {
            pDevice->SetTexture(0, nullptr);
            pDevice->SetTexture(0, this->m_texture.get(itr->m_3dobj->getindex())->gettex());

            // 行列をセット
            pDevice->SetTransform(D3DTS_WORLD, &itr->MtxWorld);

            // 描画設定
            pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, nNumVertex, 0, nNumTriangle);
        }
    }
}

//==========================================================================
// キューブ生成	
void CCube::SetCube(VERTEX_4 * Output, const CUv<float> * _this)
{
	VERTEX_4 vVertex[] =
	{
		// 手前
		{ D3DXVECTOR3(-VCRCT, VCRCT,-VCRCT),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[0].u0,_this[0].v0) }, // 左上
		{ D3DXVECTOR3(VCRCT, VCRCT,-VCRCT),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[0].u1,_this[0].v0) }, // 右上
		{ D3DXVECTOR3(VCRCT,-VCRCT,-VCRCT),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[0].u1,_this[0].v1) }, // 右下
		{ D3DXVECTOR3(-VCRCT,-VCRCT,-VCRCT),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[0].u0,_this[0].v1) }, // 左下

		// 奥
		{ D3DXVECTOR3(-VCRCT,-VCRCT,VCRCT),D3DXVECTOR3(0,0,1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[1].u0,_this[1].v0) }, // 左上
		{ D3DXVECTOR3(VCRCT,-VCRCT,VCRCT),D3DXVECTOR3(0,0,1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[1].u1,_this[1].v0) }, // 右上
		{ D3DXVECTOR3(VCRCT, VCRCT,VCRCT),D3DXVECTOR3(0,0,1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[1].u1,_this[1].v1) }, // 右下
		{ D3DXVECTOR3(-VCRCT, VCRCT,VCRCT),D3DXVECTOR3(0,0,1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[1].u0,_this[1].v1) }, // 左下

		// 右
		{ D3DXVECTOR3(-VCRCT,-VCRCT,-VCRCT),D3DXVECTOR3(-1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[2].u0,_this[2].v0) }, // 左上
		{ D3DXVECTOR3(-VCRCT,-VCRCT, VCRCT),D3DXVECTOR3(-1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[2].u1,_this[2].v0) }, // 右上
		{ D3DXVECTOR3(-VCRCT, VCRCT, VCRCT),D3DXVECTOR3(-1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[2].u1,_this[2].v1) }, // 右下
		{ D3DXVECTOR3(-VCRCT, VCRCT,-VCRCT),D3DXVECTOR3(-1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[2].u0,_this[2].v1) }, // 左下

		// 左
		{ D3DXVECTOR3(VCRCT, VCRCT,-VCRCT),D3DXVECTOR3(1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[3].u0,_this[3].v0) }, // 左上
		{ D3DXVECTOR3(VCRCT, VCRCT, VCRCT),D3DXVECTOR3(1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[3].u1,_this[3].v0) }, // 右上
		{ D3DXVECTOR3(VCRCT,-VCRCT, VCRCT),D3DXVECTOR3(1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[3].u1,_this[3].v1) }, // 右下
		{ D3DXVECTOR3(VCRCT,-VCRCT,-VCRCT),D3DXVECTOR3(1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[3].u0,_this[3].v1) }, // 左下

		// 上
		{ D3DXVECTOR3(-VCRCT,VCRCT, VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[4].u0,_this[4].v0) }, // 左上
		{ D3DXVECTOR3(VCRCT,VCRCT, VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[4].u1,_this[4].v0) }, // 右上
		{ D3DXVECTOR3(VCRCT,VCRCT,-VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[4].u1,_this[4].v1) }, // 右下
		{ D3DXVECTOR3(-VCRCT,VCRCT,-VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[4].u0,_this[4].v1) }, // 左下

		// 下
		{ D3DXVECTOR3(-VCRCT,-VCRCT,-VCRCT),D3DXVECTOR3(0,-1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[5].u0,_this[5].v0) }, // 左上
		{ D3DXVECTOR3(VCRCT,-VCRCT,-VCRCT),D3DXVECTOR3(0,-1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[5].u1,_this[5].v0) }, // 右上
		{ D3DXVECTOR3(VCRCT,-VCRCT, VCRCT),D3DXVECTOR3(0,-1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[5].u1,_this[5].v1) }, // 右下
		{ D3DXVECTOR3(-VCRCT,-VCRCT, VCRCT),D3DXVECTOR3(0,-1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[5].u0,_this[5].v1) }, // 左下
	};

	for (int i = 0; i < (int)(sizeof(vVertex) / sizeof(VERTEX_4)); i++) { Output[i] = vVertex[i]; }
}

//==========================================================================
// UV生成
void CCube::SetUV(const TEXTURE * Input, CUv<float> * Output, const int nNum)
{
	int patternNum = (nNum / 1) % Input->Pattern; //2フレームに１回	%10=パターン数
	int patternV = patternNum % Input->Direction1; //横方向のパターン
	int patternH = patternNum / Input->Direction2; //縦方向のパターン
	float fTcx = patternV * Input->texcutsize.w; //切り取りサイズ
	float fTcy = patternH * Input->texcutsize.h; //切り取りサイズ

	Output->u0 = fTcx;// 左
	Output->v0 = fTcy;// 上
	Output->u1 = fTcx + Input->texcutsize.w;// 右
	Output->v1 = fTcy + Input->texcutsize.h;//下
}
