//==========================================================================
// Xインプット[XInput.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _XInput_H_
#define _XInput_H_

//==========================================================================
// include
//==========================================================================
#pragma comment(lib,"xinput.lib")
#include <Windows.h>
#include <XInput.h>
#include <d3d9.h>
#include <d3dx9.h>

//==========================================================================
//
// class  : CXInput
// Content: Xインプット
//
//==========================================================================
class CXInput
{ 
private:
    // コピー禁止 (C++11)
    CXInput(const CXInput &) = delete;
    CXInput &operator=(const CXInput &) = delete;
private:
    //==========================================================================
    //
    // class  : CBool4
    // Content: bool4
    //
    //==========================================================================
    class CBool4
    {
    public:
        CBool4() { this->m_up = this->m_down = this->m_left = this->m_right = false; }
        ~CBool4() {}
    public:
        bool m_up, m_down, m_left, m_right;
    };

    //==========================================================================
    //
    // class  : CAnalogTrigger
    // Content: アナログスティックのトリガー
    //
    //==========================================================================
    class CAnalogTrigger
    {
    public:
        CAnalogTrigger()
        {
            this->m_Ltrigger = CBool4();
            this->m_LtriggerOld = CBool4();
            this->m_Rtrigger = CBool4();
            this->m_RtriggerOld = CBool4();
        }
        ~CAnalogTrigger() {}
    public:
        // 更新
        void update(void)
        {
            this->m_LtriggerOld = this->m_Ltrigger;
            this->m_RtriggerOld = this->m_Rtrigger;
        }

        // 切り替え
        void ifset(bool input, bool * Out) { (*Out) = ((input == true) ? true : false); }

        // 比較
        // in1 < in2
        bool ifbool(float in1, float in2) { return (in1<in2) ? true : false; }
    public:
        CBool4 m_Ltrigger; // 左アナログのトリガー
        CBool4 m_LtriggerOld; // 左アナログの古いトリガー
        CBool4 m_Rtrigger; // 右アナログのトリガー
        CBool4 m_RtriggerOld; // 右アナログの古いトリガー
    };
public:
    // アナログのボタン
    enum class EAnalog
    {
        UP, // 上
        DOWN, // 下
        LEFT, // 左
        RIGHT, // 右
    };
    // xBoxの押せるボタン
    enum class EButton
    {
        DPAD_UP = XINPUT_GAMEPAD_DPAD_UP, // 十字ボタン 上
        DPAD_DOWN = XINPUT_GAMEPAD_DPAD_DOWN, // 十字ボタン 下
        DPAD_LEFT = XINPUT_GAMEPAD_DPAD_LEFT, // 十字ボタン 左
        DPAD_RIGHT = XINPUT_GAMEPAD_DPAD_RIGHT, // 十字ボタン 右
        START = XINPUT_GAMEPAD_START, // STARTボタン
        BACK = XINPUT_GAMEPAD_BACK, // BACKボタン
        LEFT_THUMB = XINPUT_GAMEPAD_LEFT_THUMB, // 左アナログスティックのボタン
        RIGHT_THUMB = XINPUT_GAMEPAD_RIGHT_THUMB, // 右アナログスティックのボタン
        LEFT_LB = XINPUT_GAMEPAD_LEFT_SHOULDER, // LBボタン
        RIGHT_RB = XINPUT_GAMEPAD_RIGHT_SHOULDER, // RBボタン
        A = XINPUT_GAMEPAD_A, // Aボタン
        B = XINPUT_GAMEPAD_B, // Bボタン
        X = XINPUT_GAMEPAD_X, // Xボタン
        Y = XINPUT_GAMEPAD_Y, // Yボタン
    };
public:
    CXInput() {
        this->m_state = nullptr;
        this->m_stateOld = nullptr;
        this->m_statedat = nullptr;
        this->m_trigger = nullptr;
        this->m_numdata = 0;
    }
    ~CXInput() {
        this->Release();
    }
    // 初期化
    // Num 取るコントローラーの数
    bool Init(int Num);
    // 解放
    void Release(void);
    // 更新
    void Update(void);

    // プレス
    bool Press(EButton button, int index);
    // トリガー
    bool Trigger(EButton button, int index);
    // リリース
    bool Release(EButton button, int index);

    // 左アナログスティック
    bool AnalogL(int index, D3DXVECTOR3 * Out);
    // 左アナログスティック
    bool AnalogL(int index);
    // 右アナログスティック
    bool AnalogR(int index, D3DXVECTOR3 * Out);
    // 右アナログスティック
    bool AnalogR(int index);

    // 左アナログスティックのトリガー
    bool AnalogLTrigger(EAnalog key, int index);
    // 左アナログスティックのトリガー
    bool AnalogLTrigger(EAnalog key, int index, D3DXVECTOR3 * Out);
    // 右アナログスティックのトリガー
    bool AnalogRTrigger(EAnalog key, int index);
    // 右アナログスティックのトリガー
    bool AnalogRTrigger(EAnalog key, int index, D3DXVECTOR3 * Out);

    // 左トリガーボタン
    bool LT(int index);
    // 右トリガーボタン
    bool RT(int index);

    // コントローラーの存在の確認
    bool Check(int index) { return m_statedat[index]; }

    // コントローラーの現在の状態を取得
    XINPUT_STATE* GetState(int index) { return &m_state[index]; }
private:
    // コントローラのキートリガー
    bool KeyTrigger(bool bNew, bool bOld) { return (bOld ^ bNew) & bNew; }
    // コントローラのキーリリース
    bool KeyRelease(bool bNew, bool bOld) { return bOld ^ bNew & bOld; }
private:
    XINPUT_STATE * m_state; // コントローラーのステータス
    XINPUT_STATE * m_stateOld; // 古い情報
    bool * m_statedat; //パッドの有無
    CAnalogTrigger * m_trigger; // アナログのトリガー
    int m_numdata; // 登録されたデータ数
};

#endif // !_XInput_H_
