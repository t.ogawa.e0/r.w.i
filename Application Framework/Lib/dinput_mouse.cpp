//==========================================================================
// ダイレクトインプットのマウス[dinput_mouse.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "dinput_mouse.h"

//==========================================================================
// 初期化
bool CMouse::Init(HINSTANCE hInstance, HWND hWnd)
{
    this->m_hWnd = hWnd;
    this->m_mousePos.x = this->m_mousePos.y = (LONG)0;

	if (FAILED(DirectInput8Create(hInstance, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&this->m_DInput, nullptr)))
	{
		MessageBox(hWnd, "DirectInputオブジェクトが作れませんでした", "警告", MB_OK);
		return true;
	}

	if (FAILED(this->m_DInput->CreateDevice(GUID_SysMouse, &this->m_DIDevice, nullptr)))
	{
		MessageBox(hWnd, "マウスがありませんでした。", "警告", MB_OK);
		return true;
	}

	if (this->m_DIDevice != nullptr)
	{
		// マウス用のデータ・フォーマットを設定
		if (FAILED(this->m_DIDevice->SetDataFormat(&c_dfDIMouse2)))
		{
			MessageBox(hWnd, "マウスのデータフォーマットを設定できませんでした。", "警告", MB_ICONWARNING);
			return true;
		}
		
		// 協調モードを設定（フォアグラウンド＆非排他モード）
		if (FAILED(this->m_DIDevice->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_FOREGROUND)))
		{
			MessageBox(hWnd, "マウスの協調モードを設定できませんでした。", "警告", MB_ICONWARNING);
			return true;
		}

		if (FAILED(this->m_DIDevice->EnumObjects(EnumAxesCallback, hWnd, DIDFT_AXIS)))
		{
			MessageBox(hWnd, "プロパティを設定できません", "警告", MB_OK);
			return true;
		}

		if (FAILED(this->m_DIDevice->Poll()))
		{
			while (this->m_DIDevice->Acquire() == DIERR_INPUTLOST)
			{
                this->m_DIDevice->Acquire();
			}
		}
	}

	return false;
}

//==========================================================================
// 軸のコールバック
BOOL CALLBACK CMouse::EnumAxesCallback(const DIDEVICEOBJECTINSTANCE *pdidoi, void *pContext)
{
    DIPROPDWORD diprop;

    pContext;
    diprop.diph.dwSize = sizeof(DIPROPDWORD);
    diprop.diph.dwHeaderSize = sizeof(DIPROPHEADER);
    diprop.diph.dwHow = DIPH_DEVICE;
    diprop.diph.dwObj = pdidoi->dwType;
    diprop.dwData = DIPROPAXISMODE_REL; // 相対値モードで設定（絶対値はDIPROPAXISMODE_ABS）    

    return DIENUM_CONTINUE;
}

//==========================================================================
// 更新処理
void CMouse::Update(void)
{
	DIMOUSESTATE2 aState;

	GetCursorPos(&this->m_mousePos);
	ScreenToClient(this->m_hWnd, &this->m_mousePos);

    if (this->m_DIDevice == nullptr)
    {
        return;
    }

    // デバイスからデータを取得
    if (SUCCEEDED(this->m_DIDevice->GetDeviceState(sizeof(aState), &aState)))
    {
        for (int i = 0; i < (int)sizeof(DIMOUSESTATE2::rgbButtons); i++)
        {
            // トリガー・リリース情報を生成
            this->m_StateTrigger[i] = (this->m_State.rgbButtons[i] ^ aState.rgbButtons[i]) & aState.rgbButtons[i];
            this->m_StateRelease[i] = (this->m_State.rgbButtons[i] ^ aState.rgbButtons[i]) & this->m_State.rgbButtons[i];

            // リピート情報を生成
            if (aState.rgbButtons[i])
            {
                if (this->m_StateRepeatCnt[i] < 20)
                {
                    this->m_StateRepeatCnt[i]++;
                    if (this->m_StateRepeatCnt[i] == 1 || this->m_StateRepeatCnt[i] >= 20)
                    {// 押し始めた最初のフレーム、または一定時間経過したらリピート情報ON
                        this->m_StateRepeat[i] = aState.rgbButtons[i];
                    }
                    else
                    {
                        this->m_StateRepeat[i] = 0;
                    }
                }
            }
            else
            {
                this->m_StateRepeatCnt[i] = 0;
                this->m_StateRepeat[i] = 0;
            }
            // プレス情報を保存
            this->m_State.rgbButtons[i] = aState.rgbButtons[i];
        }
        this->m_State.lX = aState.lX;
        this->m_State.lY = aState.lY;
        this->m_State.lZ = aState.lZ;
    }
    else
    {
        // アクセス権を取得
        this->m_DIDevice->Acquire();
    }
}

//==========================================================================
// プレス
bool CMouse::Press(ButtonKey key)
{
	if (this->m_DIDevice == nullptr)
	{
		return false;
	}

	return (this->m_State.rgbButtons[(int)key] & 0x80) ? true : false;
}

//==========================================================================
// トリガー
bool CMouse::Trigger(ButtonKey key)
{
	if (this->m_DIDevice == nullptr)
	{
		return false;
	}

	return (this->m_StateTrigger[(int)key] & 0x80) ? true : false;
}

//==========================================================================
// リピート
bool CMouse::Repeat(ButtonKey key)
{
	if (this->m_DIDevice == nullptr)
	{
		return false;
	}

	return (this->m_StateRelease[(int)key] & 0x80) ? true : false;
}

//==========================================================================
// リリ−ス
bool CMouse::Release(ButtonKey key)
{
	if (this->m_DIDevice == nullptr)
	{
		return false;
	}

	return (this->m_StateRelease[(int)key] & 0x80) ? true : false;
}

//==========================================================================
// マウスの速度
CMouse::CSpeed CMouse::Speed(void)
{
	CSpeed speed;

	speed.m_lX = this->m_State.lX;
	speed.m_lY = this->m_State.lY;
	speed.m_lZ = this->m_State.lZ;

	return speed;
}

//==========================================================================
// カーソルの座標
POINT CMouse::WIN32Cursor(void)
{
	return this->m_mousePos;
}

//==========================================================================
// 左クリック
SHORT CMouse::WIN32LeftClick(void)
{
	return GetAsyncKeyState(VK_LBUTTON);
}

//==========================================================================
// 右クリック
SHORT CMouse::WIN32RightClick(void)
{
	return GetAsyncKeyState(VK_RBUTTON);
}

//==========================================================================
// マウスホイールホールド
SHORT CMouse::WIN32WheelHold(void)
{
	return GetAsyncKeyState(VK_MBUTTON);
}