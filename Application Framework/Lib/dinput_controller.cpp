//==========================================================================
// コントローラー[dinput_controller.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "dinput_controller.h"

//-----------------------------------------------------------------
// コントローラーの初期化 DirectInput初期化
bool CController::Init(HINSTANCE hInstance, HWND hWnd)
{
	HRESULT hr;
	DIDEVCAPS diDevCaps; // デバイス機能

    if (FAILED(DirectInput8Create(hInstance, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&this->m_DInput, nullptr)))
    {
        MessageBox(hWnd, "DirectInputオブジェクトが作れませんでした", "警告", MB_OK);
        return true;
    }

	hr = this->m_DInput->EnumDevices(DI8DEVCLASS_GAMECTRL, EnumJoysticksCallback, nullptr, DIEDFL_FORCEFEEDBACK | DIEDFL_ATTACHEDONLY);
	if (FAILED(hr) || this->m_DIDevice == nullptr)
	{
        this->m_DInput->EnumDevices(DI8DEVCLASS_GAMECTRL, EnumJoysticksCallback, nullptr, DIEDFL_ATTACHEDONLY);
	}

	if (this->m_DIDevice != nullptr)
	{
		if (FAILED(this->m_DIDevice->SetDataFormat(&c_dfDIJoystick)))
		{
			MessageBox(hWnd, "コントローラーの初期化に失敗.", "警告", MB_OK);
			return true;
		}

		if (FAILED(this->m_DIDevice->SetCooperativeLevel(hWnd, DISCL_EXCLUSIVE | DISCL_FOREGROUND)))
		{
			MessageBox(hWnd, "協調モードを設定できません", "警告", MB_OK);
			return true;
		}

		diDevCaps.dwSize = sizeof(DIDEVCAPS);
		if (FAILED(this->m_DIDevice->GetCapabilities(&diDevCaps)))
		{
			MessageBox(hWnd, "コントローラー機能を作成できません", "警告", MB_OK);
			return true;
		}

		if (FAILED(this->m_DIDevice->EnumObjects(EnumAxesCallback, (void*)hWnd, DIDFT_AXIS)))
		{
			MessageBox(hWnd, "プロパティを設定できません", "警告", MB_OK);
			return true;
		}

		if (FAILED(this->m_DIDevice->Poll()))
		{
			hr = this->m_DIDevice->Acquire();
			while (hr == DIERR_INPUTLOST)
			{
				hr = this->m_DIDevice->Acquire();
			}
		}
	}

	return false;
}

//-----------------------------------------------------------------
// コントローラーの更新
void CController::Update(void)
{
	DIJOYSTATE aState;
	BYTE aPOVState[(int)CDirectionCkey::MAX];

    if (this->m_DIDevice == nullptr)
    {
        return;
    }

    if (SUCCEEDED(this->m_DIDevice->GetDeviceState(sizeof(aState), &aState)))
    {
        // ボタン
        for (int i = 0; i < (int)sizeof(DIJOYSTATE::rgbButtons); i++)
        {
            // トリガー・リリース情報を生成
            this->m_StateTrigger[i] = (this->m_State.rgbButtons[i] ^ aState.rgbButtons[i]) & aState.rgbButtons[i];
            this->m_StateRelease[i] = (this->m_State.rgbButtons[i] ^ aState.rgbButtons[i]) & this->m_State.rgbButtons[i];

            // リピート情報を生成
            if (aState.rgbButtons[i])
            {
                if (this->m_StateRepeatCnt[i] < 20)
                {
                    this->m_StateRepeatCnt[i]++;
                    if (this->m_StateRepeatCnt[i] == 1 || this->m_StateRepeatCnt[i] >= 20)
                    {// 押し始めた最初のフレーム、または一定時間経過したらリピート情報ON
                        this->m_StateRepeat[i] = aState.rgbButtons[i];
                    }
                    else
                    {
                        this->m_StateRepeat[i] = 0;
                    }
                }
            }
            else
            {
                this->m_StateRepeatCnt[i] = 0;
                this->m_StateRepeat[i] = 0;
            }
            // プレス情報を保存
            this->m_State.rgbButtons[i] = aState.rgbButtons[i];
        }

        // 方向キー
        for (int i = 0; i < (int)CDirectionCkey::MAX; i++)
        {
            // 入力情報生成
            if (((aState.rgdwPOV[0] / (DWORD)4500) + (DWORD)1) == (DWORD)(i + 1))
            {
                aPOVState[i] = 0x80;
            }
            else
            {
                aPOVState[i] = 0x00;
            }

            // トリガー・リリース情報を生成
            this->m_POVTrigger[i] = (this->m_POVState[i] ^ aPOVState[i]) & aPOVState[i];
            this->m_POVRelease[i] = (this->m_POVState[i] ^ aPOVState[i]) & this->m_POVState[i];

            // リピート情報を生成
            if (aPOVState[i])
            {
                if (this->m_POVRepeatCnt[i] < 20)
                {
                    this->m_POVRepeatCnt[i]++;
                    if (this->m_POVRepeatCnt[i] == 1 || this->m_POVRepeatCnt[i] >= 20)
                    {// 押し始めた最初のフレーム、または一定時間経過したらリピート情報ON
                        this->m_POVRepeat[i] = aPOVState[i];
                    }
                    else
                    {
                        this->m_POVRepeat[i] = 0x00;
                    }
                }
            }
            else
            {
                this->m_POVRepeatCnt[i] = 0x00;
                this->m_POVRepeat[i] = 0x00;
            }
            // プレス情報を保存
            this->m_POVState[i] = aPOVState[i];
        }

        this->m_State.lX = aState.lX;
        this->m_State.lY = aState.lY;
        this->m_State.lZ = aState.lZ;
        this->m_State.lRx = aState.lRx;
        this->m_State.lRy = aState.lRy;
        this->m_State.lRz = aState.lRz;
    }
    else
    {
        // アクセス権を取得
        this->m_DIDevice->Acquire();
    }
}

//------------------------------------------------------------------------------
// ジョイスティックのコールバック
BOOL CALLBACK CController::EnumJoysticksCallback(const DIDEVICEINSTANCE *pdidInstance, void *pContext)
{
    DIDEVCAPS	diDevCaps;			// デバイス情報
    CController *pThis = reinterpret_cast<CController*>(pContext);

    // ジョイスティック用デバイスオブジェクトを作成
    if (FAILED(pThis->m_DInput->CreateDevice(pdidInstance->guidInstance, &pThis->m_DIDevice, nullptr)))
    {
        return DIENUM_CONTINUE; // 列挙を続ける
    }

    // ジョイスティックの能力を調べる
    diDevCaps.dwSize = sizeof(DIDEVCAPS);
    if (FAILED(pThis->m_DIDevice->GetCapabilities(&diDevCaps)))
    {
        if (pThis->m_DIDevice)
        {
            pThis->m_DIDevice->Release();
            pThis->m_DIDevice = nullptr;
        }
        return DIENUM_CONTINUE;		// 列挙を続ける
    }

	return DIENUM_STOP; // このデバイスを使うので列挙を終了する
}

//------------------------------------------------------------------------------
// 軸のコールバック
BOOL CALLBACK CController::EnumAxesCallback(const DIDEVICEOBJECTINSTANCE *pdidoi, void *pContext)
{
    CController *pThis = reinterpret_cast<CController*>(pContext);

    // 軸の値の範囲を設定（-1000〜1000）
    DIPROPRANGE diprg;
    ZeroMemory(&diprg, sizeof(diprg));
    diprg.diph.dwSize = sizeof(diprg);
    diprg.diph.dwHeaderSize = sizeof(diprg.diph);
    diprg.diph.dwObj = pdidoi->dwType;
    diprg.diph.dwHow = DIPH_BYID;
    diprg.lMin = -1000;
    diprg.lMax = +1000;

    if (FAILED(pThis->m_DIDevice->SetProperty(DIPROP_RANGE, &diprg.diph)))
    {
        return DIENUM_STOP;
    }

    return DIENUM_CONTINUE;
}

//-----------------------------------------------------------------
// PS4 左スティック
CController::CStick CController::LeftStick(void)
{
	return this->Stick(this->m_State.lX, this->m_State.lY);
}

//-----------------------------------------------------------------
// PS4 右スティック
CController::CStick CController::RightStick(void)
{
	return this->Stick(this->m_State.lZ, this->m_State.lRz);
}

//-----------------------------------------------------------------
// PS4 L2
LONG CController::L2(void)
{
	if (this->m_DIDevice == nullptr)
	{
		return (LONG)0;
	}

	return this->m_State.lRx;
}

//-----------------------------------------------------------------
// PS4 R2
LONG CController::R2(void)
{
	if (this->m_DIDevice == nullptr)
	{
		return (LONG)0;
	}

	return this->m_State.lRy;
}

//-----------------------------------------------------------------
// PS4 方向キー
bool CController::DirectionKey(CDirecyionkey Key)
{
	int Num = 0;

	if (this->m_DIDevice == nullptr)
	{
		return false;
	}

	if ((int)(sizeof(this->m_State.rgdwPOV) / 4) < Num)
	{
		return false;
	}

	if ((LONG)4294967295 == this->m_State.rgdwPOV[Num])
	{
		return false;
	}

	if ((CDirecyionkey)((this->m_State.rgdwPOV[Num] / (LONG)4500) + (LONG)1) == Key)
	{
		return true;
	}

	return false;
}

LONG CController::Adjustment(LONG Set)
{
	LONG Stick;

	if (Set >= (LONG)50 || -(LONG)50 >= Set)
	{
		Stick = Set;
	}
	else
	{
		Stick = (LONG)0;
	}

	return Stick;
}

CController::CStick CController::Stick(LONG Stick1, LONG Stick2)
{
	CStick Stick;

	if (this->m_DIDevice != nullptr)
	{
		Stick.m_LeftRight = this->Adjustment(Stick1);
		Stick.m_UpUnder = this->Adjustment(Stick2);
	}
	else
	{
		Stick.m_LeftRight = (LONG)0;
		Stick.m_UpUnder = (LONG)0;
	}

	return Stick;
}

//==========================================================================
// プレス
bool CController::Press(CButtonCkey key)
{
	if (this->m_DIDevice == nullptr)
	{
		return false;
	}

	return (this->m_State.rgbButtons[(int)key] & 0x80) ? true : false;
}

//==========================================================================
// トリガー
bool CController::Trigger(CButtonCkey key)
{
	if (this->m_DIDevice == nullptr)
	{
		return false;
	}

	return (this->m_StateTrigger[(int)key] & 0x80) ? true : false;
}

//==========================================================================
// リピート
bool CController::Repeat(CButtonCkey key)
{
	if (this->m_DIDevice == nullptr)
	{
		return false;
	}

	return (this->m_StateRelease[(int)key] & 0x80) ? true : false;
}

//==========================================================================
// リリ−ス
bool CController::Release(CButtonCkey key)
{
	if (this->m_DIDevice == nullptr)
	{
		return false;
	}

	return (this->m_StateRelease[(int)key] & 0x80) ? true : false;
}

//==========================================================================
// プレス
bool CController::Press(CDirectionCkey key)
{
	if (this->m_DIDevice == nullptr)
	{
		return false;
	}

	return (this->m_POVState[(int)key] & 0x80) ? true : false;
}

//==========================================================================
// トリガー
bool CController::Trigger(CDirectionCkey key)
{
	if (this->m_DIDevice == nullptr)
	{
		return false;
	}

	return (this->m_POVTrigger[(int)key] & 0x80) ? true : false;
}

//==========================================================================
// リピート
bool CController::Repeat(CDirectionCkey key)
{
	if (this->m_DIDevice == nullptr)
	{
		return false;
	}

	return (this->m_POVRepeat[(int)key] & 0x80) ? true : false;
}

//==========================================================================
// リリ−ス
bool CController::Release(CDirectionCkey key)
{
	if (this->m_DIDevice == nullptr)
	{
		return false;
	}

	return (this->m_POVRelease[(int)key] & 0x80) ? true : false;
}
