//==========================================================================
// Xインプット[XInput.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "XInput.h"

//==========================================================================
// 初期化
// NumPlayer 取るコントローラーの数
bool CXInput::Init(int Num)
{
	DWORD dwResult;
	if (this->m_state != nullptr)
	{
		delete[]this->m_state;
        this->m_state = nullptr;
	}
	if (this->m_stateOld != nullptr)
	{
		delete[]this->m_stateOld;
        this->m_stateOld = nullptr;
	}
	if (this->m_statedat != nullptr)
	{
		delete[]this->m_statedat;
        this->m_statedat = nullptr;
	}
	if (this->m_trigger != nullptr)
	{
		delete[]this->m_trigger;
        this->m_trigger = nullptr;
	}

	// 登録数の代入
    this->m_numdata = Num;
    this->m_statedat = new bool[m_numdata];
    this->m_state = new XINPUT_STATE[m_numdata];
    this->m_stateOld = new XINPUT_STATE[m_numdata];
    this->m_trigger = new CAnalogTrigger[m_numdata];

	for (int i = 0; i < this->m_numdata; i++)
	{
		// 中身をきれいにする
		ZeroMemory(&this->m_state[i], sizeof(XINPUT_STATE));

		// コントローラーの情報の取得
		dwResult = XInputGetState((DWORD)i, &this->m_state[i]);
		if (dwResult != ERROR_SUCCESS)
		{
            this->m_statedat[i] = false;
			break;
		}
		else
		{
            this->m_statedat[i] = true;
		}
	}

	return false;
}

//==========================================================================
// 解放
void CXInput::Release(void)
{
	if (this->m_state != nullptr)
	{
		delete[]this->m_state;
        this->m_state = nullptr;
	}
	if (this->m_stateOld != nullptr)
	{
		delete[]this->m_stateOld;
        this->m_stateOld = nullptr;
	}
	if (this->m_statedat != nullptr)
	{
		delete[]this->m_statedat;
        this->m_statedat = nullptr;
	}
	if (this->m_trigger != nullptr)
	{
		delete[]this->m_trigger;
        this->m_trigger = nullptr;
	}
}

//==========================================================================
// 更新
void CXInput::Update(void)
{
	XINPUT_STATE * pnew = nullptr;
	XINPUT_STATE * pold = nullptr;
	CAnalogTrigger * panatri = nullptr;

	for (int i = 0; i < this->m_numdata; i++)
	{
		// アクセス速度の向上
		pnew = &this->m_state[i];
		pold = &this->m_stateOld[i];
		panatri = &this->m_trigger[i];

		// コントローラ情報の複製
		*pold = *pnew;

		// コントローラーの情報の取得
		XInputGetState(i, pnew);

		// 左スティックのデッドゾーン
		if ((pnew->Gamepad.sThumbLX < XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE && pnew->Gamepad.sThumbLX > -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE) &&
			(pnew->Gamepad.sThumbLY < XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE && pnew->Gamepad.sThumbLY > -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE))
		{
			pnew->Gamepad.sThumbLX = 0;
			pnew->Gamepad.sThumbLY = 0;
		}

		// 右スティックのデッドゾーン
		if ((pnew->Gamepad.sThumbRX < XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE && pnew->Gamepad.sThumbRX > -XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE) &&
			(pnew->Gamepad.sThumbRY < XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE && pnew->Gamepad.sThumbRY > -XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE))
		{
			pnew->Gamepad.sThumbRX = 0;
			pnew->Gamepad.sThumbRY = 0;
		}

		// アナログスティックトリガー用更新
		panatri->update();

		// 左アナログのトリガー処理
		if (pnew->Gamepad.sThumbLY > /*XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE*/(SHORT)31000 ||
			pnew->Gamepad.sThumbLY < -/*XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE*/(SHORT)31000 ||
			pnew->Gamepad.sThumbLX < -/*XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE*/(SHORT)31000 ||
			pnew->Gamepad.sThumbLX > /*XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE*/(SHORT)31000)
		{
			panatri->ifset(panatri->ifbool((1.41421356f / 2.0f), (float)pnew->Gamepad.sThumbLY), &panatri->m_Ltrigger.m_up);
			panatri->ifset(panatri->ifbool((float)pnew->Gamepad.sThumbLY, -(1.41421356f / 2.0f)), &panatri->m_Ltrigger.m_down);
			panatri->ifset(panatri->ifbool((float)pnew->Gamepad.sThumbLX, -(1.41421356f / 2.0f)), &panatri->m_Ltrigger.m_left);
			panatri->ifset(panatri->ifbool((1.41421356f / 2.0f), (float)pnew->Gamepad.sThumbLX), &panatri->m_Ltrigger.m_right);
		}
		else
		{
			panatri->m_Ltrigger = CBool4();
		}

		// 右アナログのトリガー処理
		if (pnew->Gamepad.sThumbRY > /*XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE*/(SHORT)31000 ||
			pnew->Gamepad.sThumbRY < -/*XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE*/(SHORT)31000 ||
			pnew->Gamepad.sThumbRX < -/*XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE*/(SHORT)31000 ||
			pnew->Gamepad.sThumbRX > /*XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE*/(SHORT)31000)
		{
			panatri->ifset(panatri->ifbool((1.41421356f / 2.0f), (float)pnew->Gamepad.sThumbRY), &panatri->m_Rtrigger.m_up);
			panatri->ifset(panatri->ifbool((float)pnew->Gamepad.sThumbRY, -(1.41421356f / 2.0f)), &panatri->m_Rtrigger.m_down);
			panatri->ifset(panatri->ifbool((float)pnew->Gamepad.sThumbRX, -(1.41421356f / 2.0f)), &panatri->m_Rtrigger.m_left);
			panatri->ifset(panatri->ifbool((1.41421356f / 2.0f), (float)pnew->Gamepad.sThumbRX), &panatri->m_Rtrigger.m_right);
		}
		else
		{
			panatri->m_Rtrigger = CBool4();
		}
	}
}

//==========================================================================
// プレス
bool CXInput::Press(EButton button, int index)
{
	return (this->m_state[index].Gamepad.wButtons & (WORD)button) ? true : false;
}

//==========================================================================
// トリガー
bool CXInput::Trigger(EButton button, int index)
{
	return this->KeyTrigger((bool)((this->m_state[index].Gamepad.wButtons & (WORD)button) ? true : false), (bool)((this->m_stateOld[index].Gamepad.wButtons & (WORD)button) ? true : false));
}

//==========================================================================
// リリース
bool CXInput::Release(EButton button, int index)
{
	return this->KeyRelease((this->m_state[index].Gamepad.wButtons & (WORD)button) ? true : false, (this->m_stateOld[index].Gamepad.wButtons & (WORD)button) ? true : false);
}

//==========================================================================
// 左アナログスティック
bool CXInput::AnalogL(int index, D3DXVECTOR3 * Out)
{
	XINPUT_STATE * pstate = &this->m_state[index];

	*Out = D3DXVECTOR3(pstate->Gamepad.sThumbLX, 0.0f, pstate->Gamepad.sThumbLY);
	D3DXVec3Normalize(Out, Out);

	return this->AnalogL(index);
}

//==========================================================================
// 左アナログスティック
bool CXInput::AnalogL(int index)
{
	XINPUT_STATE * pstate = &this->m_state[index];

	if (pstate->Gamepad.sThumbLY > XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE ||
		pstate->Gamepad.sThumbLY < -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE ||
		pstate->Gamepad.sThumbLX < -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE ||
		pstate->Gamepad.sThumbLX > XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE)
	{
		return true;
	}

	return false;
}

//==========================================================================
// 右アナログスティック
bool CXInput::AnalogR(int index, D3DXVECTOR3 * Out)
{
	XINPUT_STATE * pstate = &this->m_state[index];

	*Out = D3DXVECTOR3(pstate->Gamepad.sThumbRX, 0.0f, pstate->Gamepad.sThumbRY);
	D3DXVec3Normalize(Out, Out);

	return this->AnalogR(index);
}

//==========================================================================
// 右アナログスティック
bool CXInput::AnalogR(int index)
{
	XINPUT_STATE * pstate = &this->m_state[index];

	if (pstate->Gamepad.sThumbRY > XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE ||
		pstate->Gamepad.sThumbRY < -XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE ||
		pstate->Gamepad.sThumbRX < -XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE ||
		pstate->Gamepad.sThumbRX > XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE)
	{
		return true;
	}

	return false;
}

//==========================================================================
// 左アナログスティックのトリガー
bool CXInput::AnalogLTrigger(EAnalog key, int index)
{
	switch (key)
	{
	case EAnalog::UP:
		return this->KeyTrigger(this->m_trigger[index].m_Ltrigger.m_up, this->m_trigger[index].m_LtriggerOld.m_up);
		break;
	case EAnalog::DOWN:
		return this->KeyTrigger(this->m_trigger[index].m_Ltrigger.m_down, this->m_trigger[index].m_LtriggerOld.m_down);
		break;
	case EAnalog::LEFT:
		return this->KeyTrigger(this->m_trigger[index].m_Ltrigger.m_left, this->m_trigger[index].m_LtriggerOld.m_left);
		break;
	case EAnalog::RIGHT:
		return this->KeyTrigger(this->m_trigger[index].m_Ltrigger.m_right, this->m_trigger[index].m_LtriggerOld.m_right);
		break;
	default:
		break;
	}
	return false;
}

//==========================================================================
// 左アナログスティックのトリガー
bool CXInput::AnalogLTrigger(EAnalog key, int index, D3DXVECTOR3 * Out)
{
	XINPUT_STATE * pstate = &this->m_state[index];

	*Out = D3DXVECTOR3(pstate->Gamepad.sThumbLX, 0.0f, pstate->Gamepad.sThumbLY);
	D3DXVec3Normalize(Out, Out);

	return this->AnalogLTrigger(key, index);
}

//==========================================================================
// 右アナログスティックのトリガー
bool CXInput::AnalogRTrigger(EAnalog key, int index)
{
	switch (key)
	{
	case EAnalog::UP:
		return this->KeyTrigger(this->m_trigger[index].m_Rtrigger.m_up, this->m_trigger[index].m_RtriggerOld.m_up);
		break;
	case EAnalog::DOWN:
		return this->KeyTrigger(this->m_trigger[index].m_Rtrigger.m_down, this->m_trigger[index].m_RtriggerOld.m_down);
		break;
	case EAnalog::LEFT:
		return this->KeyTrigger(this->m_trigger[index].m_Rtrigger.m_left, this->m_trigger[index].m_RtriggerOld.m_left);
		break;
	case EAnalog::RIGHT:
		return this->KeyTrigger(this->m_trigger[index].m_Rtrigger.m_right, this->m_trigger[index].m_RtriggerOld.m_right);
		break;
	default:
		break;
	}
	return false;
}

//==========================================================================
// 右アナログスティックのトリガー
bool CXInput::AnalogRTrigger(EAnalog key, int index, D3DXVECTOR3 * Out)
{
	XINPUT_STATE * pstate = &this->m_state[index];

	*Out = D3DXVECTOR3(pstate->Gamepad.sThumbRX, 0.0f, pstate->Gamepad.sThumbRY);
	D3DXVec3Normalize(Out, Out);

	return this->AnalogRTrigger(key, index);
}

//==========================================================================
// 左トリガーボタン
bool CXInput::LT(int index)
{
	return (this->m_state[index].Gamepad.bLeftTrigger > XINPUT_GAMEPAD_TRIGGER_THRESHOLD) ? true : false;
}

//==========================================================================
// 右トリガーボタン
bool CXInput::RT(int index)
{
	return (this->m_state[index].Gamepad.bRightTrigger > XINPUT_GAMEPAD_TRIGGER_THRESHOLD) ? true : false;
}
