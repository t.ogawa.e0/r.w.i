//==========================================================================
// 2Dポリゴン[2DPolygon.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _2DPolygon_H_
#define _2DPolygon_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "Vertex3D.h"
#include "SetRender.h"
#include "2DObject.h"
#include "Template.h"
#include "DataType.h"
#include "TextureLoader.h"
#include "Create.h"
#include "DeviceManager.h"
#include "SetSampler.h"
#include "ObjectInput.h"

//==========================================================================
//
// class  : C2DPolygon
// Content: 2Dポリゴン 
//
//==========================================================================
class C2DPolygon : private VERTEX_3D, private CSetRender, private CSetSampler, private CCreate, public CObjectInput
{
private:
    // コピー禁止 (C++11)
    C2DPolygon(const C2DPolygon &) = delete;
    C2DPolygon &operator=(const C2DPolygon &) = delete;
private:
    typedef CTextureLoader::CTexture Tex_t;
public:
    C2DPolygon() {
        this->m_Device = nullptr;
        this->m_pVertexBuffer = nullptr;
        this->m_Lock = false;
    }
	~C2DPolygon() {
        this->Release();
    }

	// 初期化 失敗時true
	// Input = 使用するテクスチャのパス ダブルポインタに対応
	// NumData = 要素数
	// AutoTextureResize = テクスチャのサイズ自動調節機能 trueで有効化
	bool Init(const char ** Input, int NumData, bool AutoTextureResize = false);

	// 初期化 失敗時true
	// Input = 使用するテクスチャのパス
	// AutoTextureResize = テクスチャのサイズ自動調節機能 trueで有効化
	bool Init(const char * Input, bool AutoTextureResize = false);

	// 初期化 失敗時true
    bool Init(void) { return this->Init(nullptr); }

    // 更新
    void Update(void);

	// 解放
	void Release(void);

    // 描画
    // ADD = 加算合成の有無
    void Draw(bool ADD = false);

	// テクスチャの情報
	// index = テクスチャの番号
	CTexvec<int> * GetTexSize(int index) { return this->m_texture.get(index)->getsize(); }

	// テクスチャの枚数
	int GetNumTex(void) { return this->m_texture.size(); }

	// テクスチャのサイズ変更
	// index = テクスチャの番号
	// Widht Height = 幅と高さ
	void SetTexSize(int index, int Widht, int Height) { this->m_texture.get(index)->setsize(CTexvec<int>(0, 0, Widht, Height)); }

	// テクスチャのスケール変更
	// index = テクスチャの番号
	// scale = 入力した値が加算されます
	void SetTexScale(int index, float scale);

	// テクスチャのサイズリセット
	// index = テクスチャの番号
	void ResetTexSize(int index) { this->m_texture.get(index)->resetsize(); }

	// アニメーション切り替わり時の判定 切り替え時true 
	// Input = アニメーションの初期化をしたC2DObject
	// AnimationCount = アニメーションカウンタ
	bool GetPattanNum(C2DObject & Input, int * AnimationCount);

	// アニメーション切り替わり時の判定 切り替え時true 
	// Input = アニメーションの初期化をしたC2DObject
	bool GetPattanNum(C2DObject & Input);

	// アニメーション用のカウンタの初期化
	// AnimationCount = アニメーションカウンタ
	int *AnimationCountInit(int * AnimationCount);

	// デバイスのセット
	void SetDevice(const LPDIRECT3DDEVICE9 Input) { this->m_Device = Input; }
private:
	CTemplates m_temp; // テンプレート

	LPDIRECT3DVERTEXBUFFER9 m_pVertexBuffer; // バッファ
	CTextureLoader m_texture; // テクスチャの格納
	bool m_Lock; // バッファ登録判定
	LPDIRECT3DDEVICE9 m_Device; // デバイス
};

#endif // !_2DPolygon_H_
