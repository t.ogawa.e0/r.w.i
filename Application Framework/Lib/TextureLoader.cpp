//==========================================================================
// テクスチャローダー[TextureLoader.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "TextureLoader.h"


//==========================================================================
// 読み込み
bool CTextureLoader::init(const char ** Input, int Num)
{
	for (int i = 0; i < Num; i++)
	{
		if (this->init(Input[i]))
		{
			return true;
		}
	}

	return false;
}

//==========================================================================
// 読み込み
bool CTextureLoader::init(const char * Input)
{
	CTexture *tex = nullptr;
	bool bkey = false; // 鍵

	// ファイルパスがある場合
	if (Input != nullptr)
	{
		// 重複読み込みのチェック
        for (int i = 0; i < this->m_texture.Size(); i++)
        {
            tex = this->m_texture.Get(i);
            // 重複しているとき
            if (tex->check(Input))
            {
                bkey = true;
                break;
            }
        }

		// 重複していないとき
		if (bkey == false)
		{
			this->create(tex, Input);

			// テクスチャの読み込み
			if (FAILED(tex->load()))
			{
                CDeviceManager::GetDXDevice()->ErrorMessage("テクスチャが存在しません \n %s", tex->gettag());
				return true;
			}
		}
	}
	else if (Input == nullptr)
	{
		this->create(tex, Input);
	}

    CTexture *path = this->m_path.Create();
    path->path(tex);

	return false;
}

//==========================================================================
// 解放
void CTextureLoader::Release(void)
{
	this->m_texture.Release();
    this->m_path.Release();
}

//==========================================================================
// データのゲッター
CTextureLoader::CTexture * CTextureLoader::get(int nID)
{
    if (0 <= nID && nID < this->m_path.Size())
    {
        return this->m_path.Get(nID);
    }
    return nullptr;
}

//==========================================================================
// 生成
CTextureLoader::CTexture * CTextureLoader::create(CTexture *& pinp, const char * Input)
{
    pinp = this->m_texture.Create();

	pinp->settag(Input);

	return pinp;
}

//==========================================================================
// サイズリセット
void CTextureLoader::CTexture::resetsize(void)
{
	this->m_texparam = this->m_mastersize;
}

//==========================================================================
// サイズのセット
void CTextureLoader::CTexture::setsize(CTexvec<int> Input)
{
	this->m_texparam = Input;
}

//==========================================================================
// 読み込み
HRESULT CTextureLoader::CTexture::load(void)
{
	LPDIRECT3DDEVICE9 pDevice = CDeviceManager::GetDXDevice()->GetD3DDevice();
	D3DXIMAGE_INFO dil; // 画像データの管理
	HRESULT hr = D3DXCreateTextureFromFile(pDevice, this->m_strName.c_str(), &this->m_texture);

	if (!FAILED(hr))
	{
		// 画像データの格納
		D3DXGetImageInfoFromFile(this->m_strName.c_str(), &dil);
		this->m_texparam = CTexvec<int>(0, 0, dil.Width, dil.Height);
		this->m_mastersize = CTexvec<int>(0, 0, dil.Width, dil.Height);
	}
    else
    {
        this->m_texture = nullptr;
    }

	return hr;
}

//==========================================================================
// 解放
void CTextureLoader::CTexture::Release(void)
{
    if (this->m_original == true)
    {
        if (this->m_texture != nullptr)
        {
            this->m_texture->Release();
        }
    }
    this->m_texture = nullptr;
    this->m_strName.clear();
}

//==========================================================================
// tagのセット
void CTextureLoader::CTexture::settag(const char * ptag)
{
	if (ptag != nullptr)
	{
		this->m_strName = ptag;
	}
	else
	{
		this->m_strName = "";
	}

    this->m_original = true;
}

//==========================================================================
// 同じテクスチャのパスが入ったらtrueが返る
bool CTextureLoader::CTexture::check(const char * Input)
{
	if (Input != nullptr)
	{
		if (this->m_strName == Input)
		{
			return true;
		}
	}
	else if (Input == nullptr)
	{
		if (this->m_strName == "")
		{
			return true;
		}
	}
	return false;
}

//==========================================================================
// 複製データ
void CTextureLoader::CTexture::path(CTexture * pinp)
{
    this->m_texture = pinp->m_texture;
    this->m_texparam = pinp->m_texparam;
    this->m_mastersize = pinp->m_mastersize;
    this->m_strName = pinp->m_strName;
    this->m_original = false;
}