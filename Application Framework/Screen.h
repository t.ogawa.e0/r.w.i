//==========================================================================
// スクリーン[Screen.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _Screen_H_
#define _Screen_H_

//==========================================================================
// Include
//==========================================================================
#include "dxlib.h"
#include "SceneChange.h"
#include "LoadScreen.h"
#include "Fade.h"

//==========================================================================
//
// class  : CScreen
// Content: スクリーン
//
//==========================================================================
class CScreen : private CSetRender, private CSetSampler
{
public:
    typedef CSceneManager::SceneName scenelist_t;
public:
    CScreen()
    {
        this->m_fade = nullptr;
        this->m_load = nullptr;
        this->m_change = false;
        this->m_count = 0;
        this->m_end_count = 0;
        this->m_start = false;
        this->m_debugkey = false;
        this->m_bFillmode = false;
        this->m_bFittering = false;
        this->m_bUpdatestop = false;
        this->m_bstartinitdata = false; // 初期時のみ読み込もデータがあるときtrueに
        this->m_bstartdraw = false;
        this->m_initializer = false;
    }
    ~CScreen() {}
    // 初期化
    bool Init(HINSTANCE hInstance, HWND hWnd);
    // 解放
    void Uninit(void);
    // 更新
    bool Update(void);
    // 描画
    void Draw(void);
    // シーン変更キー
    static void screenchange(scenelist_t Name);
private:
    // スクリーンの切り替え
    void change(scenelist_t Name);
    // 最初の初期化
    bool startinit(void);
    // 最初の初期化時の描画
    void startdraw(void);
    // 最初の初期化データの破棄
    void startdatarelease(void);
    // スクリーンの初期化
    bool screeninit(void);
    // フェード
    void fade(void);

    // ワイヤーフレームに切り替える
    void SetFillmode(void);

    // 描画モード
    void Fillmode(LPDIRECT3DDEVICE9 pDevice);

    // 描画モード
    void EndFillmode(LPDIRECT3DDEVICE9 pDevice);

    // フィルタリングのセット
    void SetFittering(void);

    // フィルタリング
    void Fittering(LPDIRECT3DDEVICE9 pDevice);

    // 更新の停止
    bool Updatestop(void);

    // デバッグ用シーンchange
    void DebugSceneChange(void);
private:
    CFade *m_fade; // フェード
    CLoadScreen *m_load; // ロード画面
    CSceneManager m_scene; // スクリーンセット
    CImGui_Dx9 m_ImGui;
    bool m_debugkey; // デバッグキー
    bool m_start; // 初期化フラグ
    bool m_change; // 切り替えフラグ
    int m_count; // カウンタ
    int m_end_count; // カウンタ
    bool m_bFillmode;
    bool m_bFittering;
    bool m_bUpdatestop;
    bool m_bstartinitdata; // 初期時格納データを設定する場合このフラグをtrueに
    bool m_bstartdraw;
    bool m_initializer;
    CTemplates m_temp; // テンプレート

    static bool m_screenchange;
    static scenelist_t m_name; // name
};

#endif // !_Screen_H_
