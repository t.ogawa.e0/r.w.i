//==========================================================================
// システムウィンドウ[SystemWindow.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _SystemWindow_H_
#define _SystemWindow_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <stdio.h>
#include <vector>
#include "dxlib.h"
#include "resource.h"

//==========================================================================
//
// class  : CSystemWindow
// Content: システムウィンドウ
//
//==========================================================================
class CSystemWindow
{
private:
	const char *Class_Name = "Config";
	const char *Window_Name = "Config";
private:
	class CWinSize
	{
	public:
		CWinSize() 
		{
			this->w = 0;
			this->h = 0;
		};
		CWinSize(int _w, int _h)
		{
			this->w = _w;
			this->h = _h;
		}
	public:
		int w;
		int h;
	};
private:
	class CData
	{
	public:
		CData() 
		{
			this->m_serect = 0;
			this->m_key = 0;
		};
        CData(int serect, bool key)
        {
            this->m_serect = serect;
            this->m_key = key;
        }
	public:
		int m_serect;
		bool m_key;
	};
public:
	CSystemWindow() {}
	~CSystemWindow() {}

	// ウィンドウ生成
	void Window(WNDCLASSEX & wcex, HINSTANCE hInstance);
	// 更新
    bool WindowUpdate(WNDCLASSEX & wcex, int nCmdShow);

    // true の時はフルスクリーン
    bool GetWindowMode(void) {
        if (this->m_asp.Size() == this->m_data.m_serect) {
            return true;
        }
        return false;
    }

    // ウィンドウサイズの取得
    CAspectRatio::data_t GetWindowSize(void) {
        if (this->m_asp.Size() != this->m_data.m_serect) {
            return this->m_asp.Get(this->m_data.m_serect).size;
        }
        return CAspectRatio::data_t(0, 0);
    }

    // ウィンドウモードのセット
    void SetAspectRatio(const CAspectRatio::List & Input) {
        this->m_asp.Search(Input.size, Input.asp);
    }
private:
	static void newtex(void);
	// ボタン
	static void Button(HWND hWnd, LPARAM *lParam, int posx, int posy);
	// 設定
	static void Config(HDC * hDC, CTexvec<int> * vpos);
	// テキスト描画
	static void Text(HWND hWnd, HDC * hDC, LPCTSTR lpszStr, int posx, int posy);
	// イメージデータ表示場所
	static void ImgeData(HDC * hDC, CTexvec<int> * vpos);
	// テクスチャ読み込み
	static void LoadTex(LPARAM * lp);
	// テクスチャ描画
	static void DrawTex(HWND hWnd, HDC * hDC, CTexvec<int> * vpos);
	// ウインドウプロシージャ
	static LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

    static BOOL CALLBACK MonitorEnumProc(HMONITOR hMonitor, HDC hdcMonitor, LPRECT lprcMonitor, LPARAM dwData);
private:
	static HWND m_hWnd;
	static HWND m_combo;
	static HDC m_BackBufferDC;
	static HBITMAP m_BackBufferBMP;
	static HWND m_hWndButton10000;
	static HWND m_hWndButton10001;
	static HWND m_check;

	//ビットマップ
	static HBITMAP m_hBitmap;
	static BITMAP m_Bitmap;

	static CData m_data;

    static CAspectRatio m_asp;
};
#endif // !_SystemWindow_H_
