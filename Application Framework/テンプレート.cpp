//==========================================================================
// ファイルの説明[ファイル名.cpp]
// author: 制作者名
//==========================================================================
#include "テンプレート.h"

//==========================================================================
// 初期化
bool Cクラス名::Init(void)
{
    /*
    ●CObjectを継承した際に使用可能な機能の説明
        ※機能の呼び出しには this-> を使用してください
    
    ●オブジェクトの座標機能の説明
    ・this->_2DObject()/this->_3DObject()
        2D/3Dオブジェクトの座標を決める機能です。
        ->Create()でインスタンスを生成し使用してください。
        ※Create()を呼び出した回数だけインスタンスが生成され中で自動的に管理されます。

    ●描画処理の説明
        これは全ての描画機能の共通のルールです。
    
    ・オブジェクトの登録
        this->描画機能()->ObjectInput()で描画したい対象(_2DObject()/_3DObject())のインスタンスを登録してください。

    ・特定オブジェクトの描画の終了
        this->描画機能()->ObjectDelete()で描画を止めたい対象(_2DObject()/_3DObject())のインスタンスを指定してください。

    ・描画オブジェクト情報の破棄
        this->描画機能()->ObjectRelease()で全ての描画を停止させることができます。

    ・描画に必要なパラメータの生成
        this->描画機能()->Update()で登録済みオブジェクトから描画に必要なデータの生成を行い記録します。

    ・描画
        this->描画機能()->Draw()で登録済みオブジェクトの一括描画を行います。

    ●各描画機能の説明
    ・this->_2DPolygon()
        2Dオブジェクトの描画に必要な機能です。
        this->_2DPolygon()->Init()に描画するテクスチャのパスを入れてください。
        描画するにはオブジェクトの登録が必要です。

    ・this->_2DNumber()
        2Dの数字描画に必要な機能です。
        呼び出し順番を守ってください。
        1:this->_2DNumber()->Init()に描画するテクスチャのパスを入れてください。
        2:this->_2DNumber()->Set()数字をどのように描画するかの指定を行ってください。
        3:this->_2DNumber()->SetAnim()数字のアニメーション情報を入れてください。
        特殊オブジェクトのため、オブジェクトの登録は不要です。

    ・this->_3DBillboard()
        ビルボードの描画に必要な機能です。
        this->_3DBillboard()->Init()に描画するテクスチャのパスを入れてください。
        描画するにはオブジェクトの登録が必要です。

    ・this->_3DCube()
        キューブの描画に必要な機能です。
        this->_3DCube()->Init()に描画するテクスチャのパスを入れてください。
        描画するにはオブジェクトの登録が必要です。

    ・this->_3DMesh()
        メッシュの描画に必要な機能です。
        this->_3DMesh()->Init()に描画するテクスチャのパスを入れてください。
        描画するにはオブジェクトの登録が必要です。

    ・this->_3DA_CircleShadow()
        丸影の描画に必要な機能です。
        this->_3DA_CircleShadow()->Init()に描画するテクスチャのパスを入れてください。
        描画するにはオブジェクトの登録が必要です。

    ・this->_3DXmodel()
        Xモデルの描画に必要な機能です。
        this->_3DXmodel()->Init()に描画するテクスチャのパスを入れてください。
        描画するにはオブジェクトの登録が必要です。

    ・this->_3DSphere()
        球体の描画に必要な機能です。
        this->_3DSphere()->Init()に描画するテクスチャのパスを入れてください。
        描画するにはオブジェクトの登録が必要です。

    ・this->_3DGrid()
        グリッド描画に必要な機能です。
        描画するにはオブジェクトの登録が必要です。
        特殊オブジェクトのため、オブジェクトの登録は不要です。

    ・this->_2DText()
        テキストの描画に必要な機能です。
        特殊オブジェクトのため、オブジェクトの登録は不要です。
        
    ●その他の機能説明
    ・this->Helper()
        ヘルパー関数が入っています。

    ・this->ImGui()
        ImGuiの機能が入っています。

    ・this->XInput()
        コントローラーでの操作に必要な機能が入っています。

    ・this->XAudio()
        音楽再生の機能が入っています。

    ・this->Camera()
        カメラの機能が入っています。

        2018/05/24
        */


    return false;
}

//==========================================================================
// 解放
void Cクラス名::Uninit(void)
{
    /*何も書かなくてよい*/
}

//==========================================================================
// 更新
void Cクラス名::Update(void)
{
    /*this->描画機能()->Update();*/
}

//==========================================================================
// 描画
void Cクラス名::Draw(void)
{
    /*this->描画機能()->Draw();*/
}