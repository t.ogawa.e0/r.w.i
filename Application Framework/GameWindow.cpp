//==========================================================================
// ゲームウィンドウ[GameWindow.cpp]
// author: tatuya ogawa
//==========================================================================
#include "GameWindow.h"
#include "resource.h"

//==========================================================================
// 定数定義
//==========================================================================
constexpr DWORD WINDOW_STYLE = (WS_OVERLAPPEDWINDOW& ~(WS_THICKFRAME | WS_MAXIMIZEBOX | WS_MINIMIZEBOX));

//==========================================================================
// ウィンドウ生成
bool CGameWindow::Window(WNDCLASSEX & wcex, HINSTANCE hInstance, const CVector2<int> & data, bool Mode)
{
	//変数の初期化
	wcex.cbSize = sizeof(WNDCLASSEX);					// 構造体のサイズ
	wcex.style = CS_VREDRAW | CS_HREDRAW;				// ウインドウスタイル
	wcex.lpfnWndProc = (WNDPROC)this->WndProc;			// そのウインドウのメッセージを処理するコールバック関数へのポインタ
	wcex.cbClsExtra = 0L;								// ウインドウクラス構造体の後ろに割り当てる補足バイト数．普通0．
	wcex.cbWndExtra = 0L;								// ウインドウインスタンスの後ろに割り当てる補足バイト数．普通0．
	wcex.hInstance = hInstance;						    // このクラスのためのウインドウプロシージャがあるインスタンスハンドル．
	wcex.hIcon = LoadIcon(hInstance, (LPCSTR)IDI_ICON2);          // アイコンのハンドル LoadIcon(hInstance, (LPCSTR)IDI_ICON1)
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);		// マウスカーソルのハンドル．LoadCursor(nullptr, IDC_ARROW )とか．
	wcex.hbrBackground = (HBRUSH)(COLOR_BACKGROUND + 1);	// ウインドウ背景色(背景描画用ブラシのハンドル)．
	wcex.lpszMenuName = nullptr;						// デフォルトメニュー名(MAKEINTRESOURCE(メニューID))
	wcex.lpszClassName = this->Class_Name;	            // このウインドウクラスにつける名前
	wcex.hIconSm = nullptr;        // 16×16の小さいサイズのアイコン LoadIcon(hInstance, (LPCSTR)IDI_ICON1)

	//	ウインドウクラスを登録します。
	RegisterClassEx(&wcex);

    this->m_WinSize = data;
    this->m_WinMood = Mode;

    return false;
}

//==========================================================================
// ウィンドウ更新
int CGameWindow::WindowUpdate(WNDCLASSEX & wcex, int nCmdShow)
{
	HWND hWnd = nullptr;
	RECT wr, dr; // 画面サイズ

    // マウスカーソル表示設定
    ShowCursor(true);

	//デスクトップサイズ習得
	GetWindowRect(GetDesktopWindow(), &dr);

	wr = dr;

	// メイン・ウインドウ作成
	AdjustWindowRect(&wr, wcex.style, TRUE);

    //Direct3Dオブジェクトの作成 に失敗時終了 
    if (CDeviceManager::Init(CDeviceManager::DeviceInitList::DXDevice, wcex.hInstance, nullptr))
    {
        return -1;
    }

    // 色々失敗した時終了
    if (CDeviceManager::GetDXDevice()->CreateWindowMode(this->m_WinSize, this->m_WinMood))
    {
        return -1;
    }

    wr.right = CDeviceManager::GetDXDevice()->GetWindowsSize().m_Width - (wr.top * 2);
    wr.bottom = CDeviceManager::GetDXDevice()->GetWindowsSize().m_Height - (wr.top * 2);
    wr.left = 0;
    wr.top = 0;

	//	ウインドウを作成します。
	hWnd = CreateWindow(this->Class_Name, this->Window_Name, WINDOW_STYLE, wr.left, wr.top, wr.right, wr.bottom, nullptr, nullptr, wcex.hInstance, nullptr);
    CDeviceManager::GetDXDevice()->SetHwnd(hWnd);

	//	ウインドウを表示します。
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return this->GameLoop(wcex.hInstance, hWnd);
}

//==========================================================================
// ウィンドウプロシージャ
LRESULT CALLBACK CGameWindow::WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	LPDIRECT3DDEVICE9 pDevice = CDeviceManager::GetDXDevice()->GetD3DDevice();	//デバイス渡し	
	D3DPRESENT_PARAMETERS pd3dpp = CDeviceManager::GetDXDevice()->Getd3dpp();
	CImGui_Dx9 imgui;

	if (CDeviceManager::GetDXDevice()->GetHwnd() != nullptr)
	{
		hWnd = CDeviceManager::GetDXDevice()->GetHwnd();
	}

	if (imgui.ImGui_WndProcHandler(hWnd, uMsg, wParam, lParam))
	{
		return true;
	}

	// メッセージの種類に応じて処理を分岐します。
	switch (uMsg)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	case WM_KEYDOWN:	// キー入力
		switch (wParam)
		{
		case VK_ESCAPE:
			// [ESC]キーが押されたら
			if (MessageBox(hWnd, "終了しますか？", "終了メッセージ", MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
			{
				DestroyWindow(hWnd);	// ウィンドウを破棄
			}
			else
			{
				return 0;	// いいえの時
			}
		}
		break;
	case WM_LBUTTONDOWN:
		SetFocus(hWnd);
		break;
	case WM_CLOSE:	// ×ボタン押した時
		if (MessageBox(hWnd, "終了しますか？", "終了メッセージ", MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
		{
			DestroyWindow(hWnd);	// ウィンドウを破棄
		}
		else
		{
			return 0;	// いいえの時
		}
		break;
	default:
		break;
	}

	//デフォルトの処理
	return imgui.SetMenu(hWnd, uMsg, wParam, lParam, pDevice, &pd3dpp);
}

//==========================================================================
// ゲームループ
int CGameWindow::GameLoop(HINSTANCE hInstance, HWND hWnd)
{
	MSG msg; // メッセージ構造体
	DWORD DNewTime = 0, DOldTime = 0; //時間格納

	if (this->Init(hInstance, hWnd))
	{
        CDeviceManager::GetDXDevice()->ErrorMessage("初期化に失敗しました");
		return-1;
	}

	timeBeginPeriod(1);//時間を正確に

	// メッセージループ
	for (;;)
	{
		// 何があってもスルー
		if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				break;
			}
			else
			{
				//メッセージ処理
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			DNewTime = timeGetTime();
			if ((DNewTime - DOldTime) >= (1000 / 60))
			{
				if (this->Update())
				{
                    CDeviceManager::GetDXDevice()->ErrorMessage("初期化に失敗しました");
					return-1;
				}

				//描画
				this->Draw();

				//時間渡し
				DOldTime = DNewTime;
			}
		}
	}
	timeEndPeriod(1);
	this->Uninit();

	return (int)msg.wParam;
}

//==========================================================================
//	初期化
bool CGameWindow::Init(HINSTANCE hInstance, HWND hWnd)
{
	return this->m_screne.Init(hInstance, hWnd);
}

//==========================================================================
//	終了処理
void CGameWindow::Uninit(void)
{
	this->m_screne.Uninit();
}

//==========================================================================
//	更新処理
bool CGameWindow::Update(void)
{
	return this->m_screne.Update();
}

//==========================================================================
//	描画処理
void CGameWindow::Draw(void)
{
	this->m_screne.Draw();
}
