//==========================================================================
// メイン関数[main.cpp]
// author: tatuya ogawa
//==========================================================================
#define _CRTDBG_MAP_ALLOC
#include <cstdlib>
#include <crtdbg.h>
#define _GLIBCXX_DEBUG

#include "dxlib.h"
#include "GameWindow.h"
#include "SystemWindow.h"

//==========================================================================
//	メイン関数
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
    // CRTメモリリーク箇所検出
 //   _CrtSetBreakAlloc(35556);
	hPrevInstance;
	lpCmdLine; 

    CAspectRatio asp; 
    CAspectRatio::data_t Aspect(16, 9); // アスペクト比

    // 指定した数のウィンドウサイズの生成
    for (int i = 0; i < 10; i++)
    {
        if (3 < i)
        {
            // 指定したアスペクト比かどうかのセット&登録
            asp.Search((Aspect * 12)*(i + 1), Aspect);
        }
    }

#if defined(_DEBUG) || defined(DEBUG)
	WNDCLASSEX gamewcex; // ウインドウクラス構造体
	CGameWindow gamewin;

	// ゲームウィンドウ登録成功時
    gamewin.Window(gamewcex, hInstance, asp.Get(3).size, false);

    // ゲームウィンドウの更新
    return gamewin.WindowUpdate(gamewcex, nCmdShow);
#else
	WNDCLASSEX gamewcex; // ウインドウクラス構造体
	WNDCLASSEX syswcex; // ウインドウクラス構造体
	CGameWindow gamewin;
	CSystemWindow syswin;

    // 生成されたウィンドウサイズの情報の入力
    for (int i = 0; i < asp.Size(); i++)
    {
        syswin.SetAspectRatio(asp.Get(i));
    }

    // システムウィンドウ登録
    syswin.Window(syswcex, hInstance);

    // ウィンドウの更新
    if (syswin.WindowUpdate(syswcex, nCmdShow))
    {
        gamewin.Window(gamewcex, hInstance, syswin.GetWindowSize(), syswin.GetWindowMode());

        // ゲームウィンドウの更新
        return gamewin.WindowUpdate(gamewcex, nCmdShow);
    }
#endif
    return 0;
}
