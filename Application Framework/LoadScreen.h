//==========================================================================
// ロードスクリーン[LoadScreen.h]
// author: tatsuya ogawa
//==========================================================================
#ifndef _LoadScreen_H_
#define _LoadScreen_H_

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CLoadScreen
// Content: ロードスクリーン
//
//==========================================================================
class CLoadScreen : public CObject
{
private:
    // パラメータ
    class CParam
    {
    public:
        CParam()
        {
            this->m_a = 0;
            this->m_Change = false;
        }
        ~CParam() {}
    public:
        int m_a; // α
        bool m_Change; // change
    };
public:
    CLoadScreen() :CObject(CObject::ID::SpecialBlock) {}
    ~CLoadScreen() {}

    // 初期化
    bool Init(void);
    // 解放
    void Uninit(void);
    // 更新
    void Update(void);
    // 描画
    void Draw(void);
private:
    // αチェンジ
    void Change(int Speed);
private:
    CParam m_paramload; // パラメータ
    DWORD m_NewTime = 0, m_OldTime = 0; //時間格納
};

#endif // !_LoadScreen_H_
